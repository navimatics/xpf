#ifndef MYMEMORYSTREAM_HPP_INCLUDED
#define MYMEMORYSTREAM_HPP_INCLUDED

#include <Base/Base.hpp>

class MyMemoryStream : public Base::Stream
{
public:
    MyMemoryStream(const void *buf = 0, size_t size = -1);
    ~MyMemoryStream();
    ssize_t read(void *buf, size_t size);
    ssize_t write(const void *buf, size_t size);
    ssize_t seek(ssize_t offset, int whence = SEEK_SET);
    ssize_t close();
    const char *buffer();
private:
    const char *_buf;
    ssize_t _offset;
};

#endif // MYMEMORYSTREAM_HPP_INCLUDED
