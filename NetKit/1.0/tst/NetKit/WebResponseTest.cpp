/*
 * NetKit/WebResponseTest.cpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#include <NetKit/WebResponse.hpp>
#include <gtest/gtest.h>

TEST(WebResponseTest, headers)
{
    mark_and_collect
    {
        NetKit::WebResponse *response = new (Base::collect) NetKit::WebResponse;

        response->setHeader("A", "1", true);
        response->setHeader("a", "2", true);
        response->setHeader("A", "3", true);
        ASSERT_STREQ("1, 2, 3", response->header("A"));
        response->setHeader("A", "4");
        ASSERT_STREQ("4", response->header("A"));
        response->setHeader("A", 0);
        ASSERT_EQ((void *)0, response->header("A"));
    }
}
TEST(WebResponseTest, cookies)
{
    mark_and_collect
    {
        NetKit::WebResponse *response = new (Base::collect) NetKit::WebResponse;
        response->setUri("http://www.example.com");

        response->addCookie("A", "aaa");
        response->addCookie("BB", "bbbbbb");
        ASSERT_STREQ("A=aaa, BB=bbbbbb", response->header("Set-Cookie"));
        ASSERT_STREQ("aaa", response->cookie("A"));
        ASSERT_STREQ("bbbbbb", response->cookie("BB"));
        ASSERT_EQ((void *)0, response->cookie("NonExistantCookie"));
        response->addCookie("CCC", "ccccccccc", 2451911);
        ASSERT_EQ((void *)0, response->cookie("CCC"));
        ASSERT_STREQ("A=aaa, BB=bbbbbb, CCC=ccccccccc; Expires=Mon, 01 Jan 2001 12:00:00 GMT",
            response->header("Set-Cookie"));
        response->addCookie("D", "d", Base::None(), "foo.example.com");
        ASSERT_EQ((void *)0, response->cookie("D"));
        ASSERT_STREQ("A=aaa, BB=bbbbbb, CCC=ccccccccc; Expires=Mon, 01 Jan 2001 12:00:00 GMT, D=d; Domain=foo.example.com",
            response->header("Set-Cookie"));
        response->addCookie("E", "e", Base::None(), "example.com", "/foo");
        ASSERT_STREQ("e", response->cookie("E"));
        ASSERT_STREQ("A=aaa, BB=bbbbbb, CCC=ccccccccc; Expires=Mon, 01 Jan 2001 12:00:00 GMT, D=d; Domain=foo.example.com, E=e; Domain=example.com; Path=/foo",
            response->header("Set-Cookie"));
        response->addCookie("F", "f", Base::DateTime::currentJulianDate() + 1);
        ASSERT_STREQ("f", response->cookie("F"));

        ASSERT_STREQ("aaa", response->cookie("A"));
        ASSERT_STREQ("bbbbbb", response->cookie("BB"));
        ASSERT_EQ((void *)0, response->cookie("CCC"));
        ASSERT_EQ((void *)0, response->cookie("D"));
        ASSERT_STREQ("e", response->cookie("E"));
        ASSERT_STREQ("f", response->cookie("F"));
    }
}
