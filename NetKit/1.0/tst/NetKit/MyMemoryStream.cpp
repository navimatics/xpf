#include "MyMemoryStream.hpp"

MyMemoryStream::MyMemoryStream(const void *buf, size_t size)
{
    _buf = Base::cstr_new((const char *)buf, size);
}
MyMemoryStream::~MyMemoryStream()
{
    close();
}
ssize_t MyMemoryStream::read(void *buf, size_t size)
{
    if (0 == _buf)
        return -1;
    ssize_t remain = Base::cstr_length(_buf) - _offset;
    if (size > remain)
        size = remain;
    memcpy(buf, _buf + _offset, size);
    _offset += size;
    return size;
}
ssize_t MyMemoryStream::write(const void *buf, size_t size)
{
    if (0 == _buf)
        return -1;
    _buf = Base::cstr_concat(_buf, (const char *)buf, size);
    _offset = Base::cstr_length(_buf);
    return size;
}
ssize_t MyMemoryStream::seek(ssize_t offset, int whence)
{
    if (0 == _buf)
        return -1;
    ssize_t newofst = _offset;
    switch (whence)
    {
    default:
    case SEEK_SET:
        newofst = offset;
        break;
    case SEEK_CUR:
        newofst += offset;
        break;
    case SEEK_END:
        newofst = Base::cstr_length(_buf) + offset;
        break;
    }
    if (newofst < 0 || Base::cstr_length(_buf) < newofst)
        return -1;
    return _offset = newofst;
}
ssize_t MyMemoryStream::close()
{
    if (0 == _buf)
        return -1;
    Base::cstr_release(_buf);
    _buf = 0;
    return 0;
}
const char *MyMemoryStream::buffer()
{
    return _buf;
}
