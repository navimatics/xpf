/*
 * NetKit/WebRequestTest.cpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#include <NetKit/WebRequest.hpp>
#include <gtest/gtest.h>

TEST(WebRequestTest, headers)
{
    mark_and_collect
    {
        NetKit::WebRequest *request = new (Base::collect) NetKit::WebRequest;

        request->setHeader("A", "1", true);
        request->setHeader("a", "2", true);
        request->setHeader("A", "3", true);
        ASSERT_STREQ("1, 2, 3", request->header("A"));
        request->setHeader("A", "4");
        ASSERT_STREQ("4", request->header("A"));
        request->setHeader("A", 0);
        ASSERT_EQ((void *)0, request->header("A"));

        request->setHeader("Cookie", "1", true);
        request->setHeader("Cookie", "2", true);
        request->setHeader("Cookie", "3", true);
        ASSERT_STREQ("1; 2; 3", request->header("Cookie"));
        request->setHeader("Cookie", "4");
        ASSERT_STREQ("4", request->header("Cookie"));
        request->setHeader("Cookie", 0);
        ASSERT_EQ((void *)0, request->header("Cookie"));
    }
}
TEST(WebRequestTest, cookies)
{
    mark_and_collect
    {
        NetKit::WebRequest *request = new (Base::collect) NetKit::WebRequest;

        request->addCookie("A", "aaa");
        request->addCookie("BB", "bbbbbb");
        request->addCookie("CCC", "ccccccccc");
        ASSERT_STREQ("A=aaa; BB=bbbbbb; CCC=ccccccccc", request->header("Cookie"));
        ASSERT_STREQ("aaa", request->cookie("A"));
        ASSERT_STREQ("bbbbbb", request->cookie("BB"));
        ASSERT_STREQ("ccccccccc", request->cookie("CCC"));
        ASSERT_EQ((void *)0, request->cookie("DDDD"));
    }
}
