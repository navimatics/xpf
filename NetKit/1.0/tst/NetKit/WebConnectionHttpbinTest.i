TEST(HTTPBIN_TEST, ReuseConnection)
{
    mark_and_collect
    {
        NetKit::WebConnection *connection;
        NetKit::WebRequest *request;
        NetKit::WebResponse *response;

        connection = new (Base::collect) NetKit::WebConnection;
        request = connection->request();
        response = connection->response();

        request->setUri(HTTPBIN "/get");
        ASSERT_TRUE(connection->sendrecv());

        request->setUri(HTTPBIN "/get");
        ASSERT_TRUE(connection->sendrecv());
    }
}

void BASE_CONCAT(HTTPBIN_TEST, _AsyncReuseConnection1)(NetKit::WebConnection *connection, int event);
void BASE_CONCAT(HTTPBIN_TEST, _AsyncReuseConnection2)(NetKit::WebConnection *connection, int event);
void BASE_CONCAT(HTTPBIN_TEST, _AsyncReuseConnection1)(NetKit::WebConnection *connection, int event)
{
    switch (event)
    {
    case NetKit::WebConnection::EventReceiveHeader:
        connection->request()->setUri(HTTPBIN "/get");
        connection->setDelegate(make_delegate(BASE_CONCAT(HTTPBIN_TEST, _AsyncReuseConnection2)));
        ASSERT_TRUE(connection->sendrecv(true));
        break;
    case NetKit::WebConnection::EventFailure:
        FAIL();
        break;
    }
}
void BASE_CONCAT(HTTPBIN_TEST, _AsyncReuseConnection2)(NetKit::WebConnection *connection, int event)
{
    switch (event)
    {
    case NetKit::WebConnection::EventReceiveHeader:
        Base::ThreadLoopStop();
        break;
    case NetKit::WebConnection::EventFailure:
        FAIL();
        break;
    }
}
TEST(HTTPBIN_TEST, AsyncReuseConnection)
{
    mark_and_collect
    {
        Base::ThreadLoopInit();
        NetKit::WebConnection *connection = new (Base::collect) NetKit::WebConnection;
        connection->request()->setUri(HTTPBIN "/get");
        connection->setDelegate(make_delegate(BASE_CONCAT(HTTPBIN_TEST, _AsyncReuseConnection1)));
        ASSERT_TRUE(connection->sendrecv(true));
        Base::ThreadLoopRun();
    }
}

TEST(HTTPBIN_TEST, Status)
{
    mark_and_collect
    {
        NetKit::WebConnection *connection;
        NetKit::WebRequest *request;
        NetKit::WebResponse *response;

        connection = new (Base::collect) NetKit::WebConnection;
        request = connection->request();
        response = connection->response();

        request->setUri(HTTPBIN "/status/500");
        ASSERT_TRUE(connection->sendrecv());
        ASSERT_EQ(500, response->statusCode());

        request->setUri(HTTPBIN "/status/204");
        ASSERT_TRUE(connection->sendrecv());
        ASSERT_EQ(204, response->statusCode());
    }
}

void BASE_CONCAT(HTTPBIN_TEST, _AsyncStatus1)(NetKit::WebConnection *connection, int event);
void BASE_CONCAT(HTTPBIN_TEST, _AsyncStatus2)(NetKit::WebConnection *connection, int event);
void BASE_CONCAT(HTTPBIN_TEST, _AsyncStatus1)(NetKit::WebConnection *connection, int event)
{
    switch (event)
    {
    case NetKit::WebConnection::EventReceiveHeader:
        ASSERT_EQ(500, connection->response()->statusCode());
        connection->setDelegate(make_delegate(BASE_CONCAT(HTTPBIN_TEST, _AsyncStatus2)));
        connection->request()->setUri(HTTPBIN "/status/204");
        ASSERT_TRUE(connection->sendrecv(true));
        break;
    case NetKit::WebConnection::EventFailure:
        FAIL();
        break;
    }
}
void BASE_CONCAT(HTTPBIN_TEST, _AsyncStatus2)(NetKit::WebConnection *connection, int event)
{
    switch (event)
    {
    case NetKit::WebConnection::EventReceiveHeader:
        ASSERT_EQ(204, connection->response()->statusCode());
        Base::ThreadLoopStop();
        break;
    case NetKit::WebConnection::EventFailure:
        FAIL();
        break;
    }
}
TEST(HTTPBIN_TEST, AsyncStatus)
{
    mark_and_collect
    {
        Base::ThreadLoopInit();
        NetKit::WebConnection *connection = new (Base::collect) NetKit::WebConnection;
        connection->request()->setUri(HTTPBIN "/status/500");
        connection->setDelegate(make_delegate(BASE_CONCAT(HTTPBIN_TEST, _AsyncStatus1)));
        ASSERT_TRUE(connection->sendrecv(true));
        Base::ThreadLoopRun();
    }
}

TEST(HTTPBIN_TEST, RequestHeaders)
{
    mark_and_collect
    {
        NetKit::WebConnection *connection;
        NetKit::WebRequest *request;
        NetKit::WebResponse *response;
        Base::Object *obj;
        Base::Map *map;

        connection = new (Base::collect) NetKit::WebConnection;
        request = connection->request();
        response = connection->response();

        request->setUri(HTTPBIN "/get");
        request->setHeader("MyHeader", "MyValue");
        request->setHeader("MyHeader2", "MyValue2");
        request->setHeader("MyHeader2", "MyValue3", true);
        ASSERT_TRUE(connection->sendrecv());
        ASSERT_EQ(200, response->statusCode());
        obj = responseObject(response);
        map = dynamic_cast<Base::Map *>(obj);
        ASSERT_NE((void *)0, map);
        map = dynamic_cast<Base::Map *>(map->object(BASE_ALLOCA_OBJECT(Base::TransientCString, "headers")));
        ASSERT_NE((void *)0, map);
        Base::Map *headersMap = new (Base::collect) Base::Map(Base::CString::caseInsensitiveComparer());
        headersMap->addObjects(map);
        ASSERT_STREQ("MyValue", headersMap->object(BASE_ALLOCA_OBJECT(Base::TransientCString, "MyHeader"))->strrepr());
        ASSERT_STREQ("MyValue2, MyValue3", headersMap->object(BASE_ALLOCA_OBJECT(Base::TransientCString, "MyHeader2"))->strrepr());
    }
}
TEST(HTTPBIN_TEST, ResponseHeaders)
{
    mark_and_collect
    {
        NetKit::WebConnection *connection;
        NetKit::WebRequest *request;
        NetKit::WebResponse *response;

        connection = new (Base::collect) NetKit::WebConnection;
        request = connection->request();
        response = connection->response();

        request->setUri(HTTPBIN "/response-headers?MyHeader=%09MyValue%09&MyHeader2=%20MyValue2%20");
        ASSERT_TRUE(connection->sendrecv());
        ASSERT_EQ((void *)0, response->header("HeaderDoesNotExist-89B9834CCC684D83A5BC454D055626D0"));
        ASSERT_STREQ("MyValue", response->header("MyHeader"));
        ASSERT_STREQ("MyValue2", response->header("MyHeader2"));
    }
}
TEST(HTTPBIN_TEST, Redirects)
{
    mark_and_collect
    {
        NetKit::WebConnection *connection;
        NetKit::WebRequest *request;
        NetKit::WebResponse *response;

        connection = new (Base::collect) NetKit::WebConnection;
        request = connection->request();
        response = connection->response();

        request->setUri(HTTPBIN "/redirect/5");
        ASSERT_TRUE(connection->sendrecv());
        ASSERT_EQ(200, response->statusCode());
        ASSERT_STREQ(HTTPBIN "/get", response->uri());

        request->setUri(HTTPBIN "/relative-redirect/5");
        ASSERT_TRUE(connection->sendrecv());
        ASSERT_EQ(200, response->statusCode());
        ASSERT_STREQ(HTTPBIN "/get", response->uri());
    }
}
TEST(HTTPBIN_TEST, Cookies)
{
    mark_and_collect
    {
        NetKit::WebConnection *connection;
        NetKit::WebRequest *request;
        NetKit::WebResponse *response;

        connection = new (Base::collect) NetKit::WebConnection;
        request = connection->request();
        response = connection->response();

        request->setUri(HTTPBIN "/cookies/set?k1=v1&k2=v2");
        request->setRedirectsAllowed(false);
        ASSERT_TRUE(connection->sendrecv());
        const char *SetCookie = response->header("Set-Cookie");
        ASSERT_TRUE(
            0 == strcmp("k1=v1; Path=/, k2=v2; Path=/", SetCookie) ||
            0 == strcmp("k2=v2; Path=/, k1=v1; Path=/", SetCookie));
        ASSERT_STREQ("v1", response->cookie("k1"));
        ASSERT_STREQ("v2", response->cookie("k2"));
    }
}

TEST(HTTPBIN_TEST, Post)
{
    mark_and_collect
    {
        NetKit::WebConnection *connection;
        NetKit::WebRequest *request;
        NetKit::WebResponse *response;
        MyMemoryStream *requestStream;
        Base::Object *obj;
        Base::Map *map;

        connection = new (Base::collect) NetKit::WebConnection;
        request = connection->request();
        response = connection->response();

        request->setUri(HTTPBIN "/post");
        request->setMethod("POST");
        request->setBodyStream(new (Base::collect) Base::MemoryStream("hello, world"));
        ASSERT_TRUE(connection->sendrecv());
        ASSERT_EQ(200, response->statusCode());
        obj = responseObject(response);
        map = dynamic_cast<Base::Map *>(obj);
        ASSERT_NE((void *)0, map);
        ASSERT_STREQ("hello, world", map->object(BASE_ALLOCA_OBJECT(Base::TransientCString, "data"))->strrepr());

        requestStream = new (Base::collect) MyMemoryStream;
        for (size_t i = 0; 1000 > i; i++)
            requestStream->write("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz", 26 * 2);
        requestStream->seek(0);

        request->reset();
        request->setUri(HTTPBIN "/post");
        request->setMethod("POST");
        request->setBodyStream(requestStream);
        ASSERT_TRUE(connection->sendrecv());
        ASSERT_EQ(200, response->statusCode());
        obj = responseObject(response);
        map = dynamic_cast<Base::Map *>(obj);
        ASSERT_NE((void *)0, map);
        ASSERT_STREQ(requestStream->buffer(), map->object(BASE_ALLOCA_OBJECT(Base::TransientCString, "data"))->strrepr());
    }
}

class BASE_CONCAT(HTTPBIN_TEST, _AsyncPost_Obj) : public Base::Object
{
public:
    BASE_CONCAT(HTTPBIN_TEST, _AsyncPost_Obj)()
    {
        _requestStream = new MyMemoryStream;
        for (size_t i = 0; 1000 > i; i++)
            _requestStream->write("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz", 26 * 2);
        _requestStream->seek(0);
        _reader = new Base::ObjectReader;
    }
    ~BASE_CONCAT(HTTPBIN_TEST, _AsyncPost_Obj)()
    {
        _reader->release();
        _requestStream->release();
    }
    void delegate1(NetKit::WebConnection *connection, int event)
    {
        switch (event)
        {
        case NetKit::WebConnection::EventReceiveHeader:
            ASSERT_EQ(200, connection->response()->statusCode());
            break;
        case NetKit::WebConnection::EventReceiveData:
            {
                char buf[4096];
                ssize_t bytes;
                size_t loopCount = 0;
                while (0 < (bytes = connection->response()->bodyStream()->read(buf, sizeof buf)))
                {
                    ASSERT_NE(-1, _reader->parse(buf, bytes));
                    loopCount++;
                }
                ASSERT_LT((size_t)0, loopCount);
                if (-1 == bytes)
                {
                    if (Base::Error *error = Base::Error::lastError())
                        if (EAGAIN == error->code() && 0 == strcmp("errno", error->domain()))
                            break;
                    FAIL();
                }
            }
            break;
        case NetKit::WebConnection::EventSuccess:
            {
                Base::Object *obj = _reader->readObject();
                Base::Map *map = dynamic_cast<Base::Map *>(obj);
                ASSERT_NE((void *)0, map);
                ASSERT_STREQ("hello, world", map->object(BASE_ALLOCA_OBJECT(Base::TransientCString, "data"))->strrepr());
                _reader->reset();
            }
            connection->request()->reset();
            connection->request()->setUri(HTTPBIN "/post");
            connection->request()->setMethod("POST");
            connection->request()->setBodyStream(_requestStream);
            connection->setDelegate(make_delegate(&BASE_CONCAT(HTTPBIN_TEST, _AsyncPost_Obj)::delegate2, this));
            ASSERT_TRUE(connection->sendrecv(true));
            break;
        case NetKit::WebConnection::EventFailure:
            FAIL();
            break;
        }
    }
    void delegate2(NetKit::WebConnection *connection, int event)
    {
        switch (event)
        {
        case NetKit::WebConnection::EventReceiveHeader:
            ASSERT_EQ(200, connection->response()->statusCode());
            break;
        case NetKit::WebConnection::EventReceiveData:
            {
                char buf[4096];
                ssize_t bytes;
                size_t loopCount = 0;
                while (0 < (bytes = connection->response()->bodyStream()->read(buf, sizeof buf)))
                {
                    ASSERT_NE(-1, _reader->parse(buf, bytes));
                    loopCount++;
                }
                ASSERT_LT((size_t)0, loopCount);
                if (-1 == bytes)
                {
                    if (Base::Error *error = Base::Error::lastError())
                        if (EAGAIN == error->code() && 0 == strcmp("errno", error->domain()))
                            break;
                    FAIL();
                }
            }
            break;
        case NetKit::WebConnection::EventSuccess:
            {
                Base::Object *obj = _reader->readObject();
                Base::Map *map = dynamic_cast<Base::Map *>(obj);
                ASSERT_NE((void *)0, map);
                ASSERT_STREQ(_requestStream->buffer(), map->object(BASE_ALLOCA_OBJECT(Base::TransientCString, "data"))->strrepr());
            }
            Base::ThreadLoopStop();
            break;
        case NetKit::WebConnection::EventFailure:
            FAIL();
            break;
        }
    }
private:
    MyMemoryStream *_requestStream;
    Base::ObjectReader *_reader;
};
TEST(HTTPBIN_TEST, AsyncPost)
{
    mark_and_collect
    {
        Base::ThreadLoopInit();
        BASE_CONCAT(HTTPBIN_TEST, _AsyncPost_Obj) *asyncObj = new (Base::collect) BASE_CONCAT(HTTPBIN_TEST, _AsyncPost_Obj);
        NetKit::WebConnection *connection = new (Base::collect) NetKit::WebConnection;
        connection->request()->setUri(HTTPBIN "/post");
        connection->request()->setMethod("POST");
        connection->request()->setBodyStream(new (Base::collect) Base::MemoryStream("hello, world"));
        connection->setDelegate(make_delegate(&BASE_CONCAT(HTTPBIN_TEST, _AsyncPost_Obj)::delegate1, asyncObj));
        ASSERT_TRUE(connection->sendrecv(true));
        Base::ThreadLoopRun();
    }
}

#if 0 // httpbin.org does not seem to properly support chunked
TEST(HTTPBIN_TEST, PostChunked)
{
    mark_and_collect
    {
        NetKit::WebConnection *connection;
        NetKit::WebRequest *request;
        NetKit::WebResponse *response;
        MyMemoryStream *requestStream;
        Base::Object *obj;
        Base::Map *map;

        connection = new (Base::collect) NetKit::WebConnection;
        request = connection->request();
        response = connection->response();

        request->setUri(HTTPBIN "/post");
        request->setMethod("POST");
        request->setBodyStream(new (Base::collect) MyMemoryStream("hello, world"));
        request->setRedirectsAllowed(false);
        ASSERT_TRUE(connection->sendrecv());
        ASSERT_EQ(200, response->statusCode());
        obj = responseObject(response);
        map = dynamic_cast<Base::Map *>(obj);
        ASSERT_NE((void *)0, map);
        ASSERT_STREQ("hello, world", map->object(BASE_ALLOCA_OBJECT(Base::TransientCString, "data"))->strrepr());

        requestStream = new (Base::collect) MyMemoryStream;
        for (size_t i = 0; 1000 > i; i++)
            requestStream->write("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz", 26 * 2);
        requestStream->seek(0);

        request->reset();
        request->setUri(HTTPBIN "/post");
        request->setMethod("POST");
        request->setBodyStream(requestStream);
        request->setRedirectsAllowed(false);
        ASSERT_TRUE(connection->sendrecv());
        ASSERT_EQ(200, response->statusCode());
        obj = responseObject(response);
        map = dynamic_cast<Base::Map *>(obj);
        ASSERT_NE((void *)0, map);
        ASSERT_STREQ(requestStream->buffer(), map->object(BASE_ALLOCA_OBJECT(Base::TransientCString, "data"))->strrepr());
    }
}
#endif
TEST(HTTPBIN_TEST, SessionUserAgent)
{
    mark_and_collect
    {
        NetKit::WebSession *session;
        NetKit::WebConnection *connection;
        NetKit::WebRequest *request;
        NetKit::WebResponse *response;
        Base::Object *obj;
        Base::Map *map;
        Base::Map *headersMap;

        session = new (Base::collect) NetKit::WebSession;
        session->setUserAgent("NetKitTests Session Test User Agent");
        connection = new (Base::collect) NetKit::WebConnection(session);
        request = connection->request();
        response = connection->response();

        request->setUri(HTTPBIN "/redirect/1");
        ASSERT_TRUE(connection->sendrecv());
        obj = responseObject(response);
        map = dynamic_cast<Base::Map *>(obj);
        ASSERT_NE((void *)0, map);
        map = dynamic_cast<Base::Map *>(map->object(BASE_ALLOCA_OBJECT(Base::TransientCString, "headers")));
        ASSERT_NE((void *)0, map);
        headersMap = new (Base::collect) Base::Map(Base::CString::caseInsensitiveComparer());
        headersMap->addObjects(map);
        ASSERT_STREQ("NetKitTests Session Test User Agent", headersMap->object(BASE_ALLOCA_OBJECT(Base::TransientCString, "User-Agent"))->strrepr());

        request->reset();
        request->setUri(HTTPBIN "/get");
        ASSERT_TRUE(connection->sendrecv());
        obj = responseObject(response);
        map = dynamic_cast<Base::Map *>(obj);
        ASSERT_NE((void *)0, map);
        map = dynamic_cast<Base::Map *>(map->object(BASE_ALLOCA_OBJECT(Base::TransientCString, "headers")));
        ASSERT_NE((void *)0, map);
        headersMap = new (Base::collect) Base::Map(Base::CString::caseInsensitiveComparer());
        headersMap->addObjects(map);
        ASSERT_STREQ("NetKitTests Session Test User Agent", headersMap->object(BASE_ALLOCA_OBJECT(Base::TransientCString, "User-Agent"))->strrepr());
    }
}
TEST(HTTPBIN_TEST, SessionCookiesWithoutRedirect)
{
    mark_and_collect
    {
        NetKit::WebSession *session;
        NetKit::Cookie *cookie;
        NetKit::WebConnection *connection;
        NetKit::WebRequest *request;
        NetKit::WebResponse *response;
        Base::Object *obj;
        Base::Map *map;
        Base::Map *headersMap;

        session = new (Base::collect) NetKit::WebSession;
        cookie = new (Base::collect) NetKit::Cookie("MyCookie", "MyValue", Base::None(), true, HTTPBIN_HOST, "/");
        session->addCookie(cookie);
        cookie = new (Base::collect) NetKit::Cookie("MyCookie2", "MyValue2", Base::None(), true, "www" HTTPBIN_HOST, "/");
            /* this cookie SHOULD NOT be sent to httpbin.org */
        session->addCookie(cookie);
        connection = new (Base::collect) NetKit::WebConnection(session);
        request = connection->request();
        response = connection->response();

        request->setUri(HTTPBIN "/get");
        ASSERT_TRUE(connection->sendrecv());
        obj = responseObject(response);
        map = dynamic_cast<Base::Map *>(obj);
        ASSERT_NE((void *)0, map);
        map = dynamic_cast<Base::Map *>(map->object(BASE_ALLOCA_OBJECT(Base::TransientCString, "headers")));
        ASSERT_NE((void *)0, map);
        headersMap = new (Base::collect) Base::Map(Base::CString::caseInsensitiveComparer());
        headersMap->addObjects(map);
        ASSERT_STREQ("MyCookie=MyValue", headersMap->object(BASE_ALLOCA_OBJECT(Base::TransientCString, "Cookie"))->strrepr());

        request->reset();
        request->setUri(HTTPBIN "/cookies/set?k1=v1&k2=v2");
        request->setRedirectsAllowed(false);
        ASSERT_TRUE(connection->sendrecv());
        const char *SetCookie = response->header("Set-Cookie");
        ASSERT_TRUE(
            0 == strcmp("k1=v1; Path=/, k2=v2; Path=/", SetCookie) ||
            0 == strcmp("k2=v2; Path=/, k1=v1; Path=/", SetCookie));
        ASSERT_STREQ("v1", response->cookie("k1"));
        ASSERT_STREQ("v2", response->cookie("k2"));

        request->reset();
        request->setUri(HTTPBIN "/get");
        ASSERT_TRUE(connection->sendrecv());
        obj = responseObject(response);
        map = dynamic_cast<Base::Map *>(obj);
        ASSERT_NE((void *)0, map);
        map = dynamic_cast<Base::Map *>(map->object(BASE_ALLOCA_OBJECT(Base::TransientCString, "headers")));
        ASSERT_NE((void *)0, map);
        headersMap = new (Base::collect) Base::Map(Base::CString::caseInsensitiveComparer());
        headersMap->addObjects(map);
        const char *Cookie = headersMap->object(BASE_ALLOCA_OBJECT(Base::TransientCString, "Cookie"))->strrepr();
        ASSERT_TRUE(
            0 == strcmp("MyCookie=MyValue; k1=v1; k2=v2", Cookie) ||
            0 == strcmp("MyCookie=MyValue; k2=v2; k1=v1", Cookie));
    }
}
TEST(HTTPBIN_TEST, SessionCookiesWithRedirect)
{
    mark_and_collect
    {
        NetKit::WebSession *session;
        NetKit::Cookie *cookie;
        NetKit::WebConnection *connection;
        NetKit::WebRequest *request;
        NetKit::WebResponse *response;
        Base::Object *obj;
        Base::Map *map;
        Base::Map *headersMap;

        session = new (Base::collect) NetKit::WebSession;
        cookie = new (Base::collect) NetKit::Cookie("MyCookie", "MyValue", Base::None(), true, HTTPBIN_HOST, "/");
        session->addCookie(cookie);
        cookie = new (Base::collect) NetKit::Cookie("MyCookie2", "MyValue2", Base::None(), true, "www" HTTPBIN_HOST, "/");
            /* this cookie SHOULD NOT be sent to httpbin.org */
        session->addCookie(cookie);
        connection = new (Base::collect) NetKit::WebConnection(session);
        request = connection->request();
        response = connection->response();

        request->setUri(HTTPBIN "/get");
        ASSERT_TRUE(connection->sendrecv());
        obj = responseObject(response);
        map = dynamic_cast<Base::Map *>(obj);
        ASSERT_NE((void *)0, map);
        map = dynamic_cast<Base::Map *>(map->object(BASE_ALLOCA_OBJECT(Base::TransientCString, "headers")));
        ASSERT_NE((void *)0, map);
        headersMap = new (Base::collect) Base::Map(Base::CString::caseInsensitiveComparer());
        headersMap->addObjects(map);
        ASSERT_STREQ("MyCookie=MyValue", headersMap->object(BASE_ALLOCA_OBJECT(Base::TransientCString, "Cookie"))->strrepr());

        request->reset();
        request->setUri(HTTPBIN "/cookies/set?k1=v1&k2=v2");
        ASSERT_TRUE(connection->sendrecv());

        request->reset();
        request->setUri(HTTPBIN "/get");
        ASSERT_TRUE(connection->sendrecv());
        obj = responseObject(response);
        map = dynamic_cast<Base::Map *>(obj);
        ASSERT_NE((void *)0, map);
        map = dynamic_cast<Base::Map *>(map->object(BASE_ALLOCA_OBJECT(Base::TransientCString, "headers")));
        ASSERT_NE((void *)0, map);
        headersMap = new (Base::collect) Base::Map(Base::CString::caseInsensitiveComparer());
        headersMap->addObjects(map);
        const char *Cookie = headersMap->object(BASE_ALLOCA_OBJECT(Base::TransientCString, "Cookie"))->strrepr();
        ASSERT_TRUE(
            0 == strcmp("MyCookie=MyValue; k1=v1; k2=v2", Cookie) ||
            0 == strcmp("MyCookie=MyValue; k2=v2; k1=v1", Cookie));
    }
}
