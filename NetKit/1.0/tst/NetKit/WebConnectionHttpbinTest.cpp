/*
 * NetKit/WebConnectionHttpbinTest.cpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#include <NetKit/WebConnection.hpp>
#include <gtest/gtest.h>
#include "MyMemoryStream.hpp"

static inline Base::Object *responseObject(NetKit::WebResponse *response)
{
    Base::ObjectReader *reader = new Base::ObjectReader(response->bodyStream());
    Base::Object *obj = reader->readObject();
    if (0 != obj)
        obj->autocollect();
    reader->release();
    return obj;
}

#define HTTPBIN                         "http://httpbin.org"
#define HTTPBIN_HOST                    "httpbin.org"
#define HTTPBIN_TEST                    WebConnectionHttpbinTest
#include "WebConnectionHttpbinTest.i"
#undef HTTPBIN_TEST
#undef HTTPBIN_HOST
#undef HTTPBIN

#define HTTPBIN                         "https://httpbin.org"
#define HTTPBIN_HOST                    "httpbin.org"
#define HTTPBIN_TEST                    WebConnectionHttpsbinTest
#include "WebConnectionHttpbinTest.i"
#undef HTTPBIN_TEST
#undef HTTPBIN_HOST
#undef HTTPBIN
