/*
 * NetKit/CookieJarTest.cpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#include <NetKit/CookieJar.hpp>
#include <gtest/gtest.h>

TEST(CookieJarTest, netscape1)
{
    mark_and_collect
    {
        NetKit::CookieJar *cookieJar = new (Base::collect) NetKit::CookieJar;
        double now = 2451484/* 1999-11-01 */;
        Base::cstr_uri_parts uriparts;

        Base::cstr_uri_parse("http://www.example.com", &uriparts);
        cookieJar->addCookieSpec("CUSTOMER=WILE_E_COYOTE; path=/; expires=Wednesday, 09-Nov-99 23:12:40 GMT",
            now, &uriparts);
        ASSERT_STREQ("CUSTOMER=WILE_E_COYOTE", cookieJar->cookieSpec(now, &uriparts));
        cookieJar->addCookieSpec("PART_NUMBER=ROCKET_LAUNCHER_0001; path=/",
            now, &uriparts);
        ASSERT_STREQ("CUSTOMER=WILE_E_COYOTE; PART_NUMBER=ROCKET_LAUNCHER_0001", cookieJar->cookieSpec(now, &uriparts));
        cookieJar->addCookieSpec("SHIPPING=FEDEX; path=/foo",
            now, &uriparts);
        ASSERT_STREQ("CUSTOMER=WILE_E_COYOTE; PART_NUMBER=ROCKET_LAUNCHER_0001", cookieJar->cookieSpec(now, &uriparts));
        Base::cstr_uri_parse("http://www.example.com/foo", &uriparts);
        ASSERT_STREQ("SHIPPING=FEDEX; CUSTOMER=WILE_E_COYOTE; PART_NUMBER=ROCKET_LAUNCHER_0001", cookieJar->cookieSpec(now, &uriparts));
            /*
             * The cookie order in the Netscape cookie document is wrong. The one above is the correct one:
             *     - SHIPPING=FEDEX has path=/foo, which is more specific than /
             *     - CUSTOMER=WILE_E_COYOTE was created before PART_NUMBER=ROCKET_LAUNCHER_0001
             */
    }
}
TEST(CookieJarTest, netscape2)
{
    mark_and_collect
    {
        NetKit::CookieJar *cookieJar = new (Base::collect) NetKit::CookieJar;
        double now = 2451484/* 1999-11-01 */;
        Base::cstr_uri_parts uriparts;

        Base::cstr_uri_parse("http://www.example.com", &uriparts);
        cookieJar->addCookieSpec("PART_NUMBER=ROCKET_LAUNCHER_0001; path=/",
            now, &uriparts);
        ASSERT_STREQ("PART_NUMBER=ROCKET_LAUNCHER_0001", cookieJar->cookieSpec(now, &uriparts));
        cookieJar->addCookieSpec("PART_NUMBER=RIDING_ROCKET_0023; path=/ammo",
            now, &uriparts);
        Base::cstr_uri_parse("http://www.example.com/ammo", &uriparts);
        ASSERT_STREQ("PART_NUMBER=RIDING_ROCKET_0023; PART_NUMBER=ROCKET_LAUNCHER_0001", cookieJar->cookieSpec(now, &uriparts));
    }
}
TEST(CookieJarTest, rfc6265)
{
    mark_and_collect
    {
        NetKit::CookieJar *cookieJar = new (Base::collect) NetKit::CookieJar;
        double now = 2451484/* 1999-11-01 */;
        Base::cstr_uri_parts uriparts;

        Base::cstr_uri_parse("http://www.example.com", &uriparts);
        cookieJar->addCookieSpec("SID=31d4d96e407aad42",
            now, &uriparts);
        ASSERT_STREQ("SID=31d4d96e407aad42", cookieJar->cookieSpec(now, &uriparts));
        cookieJar->addCookieSpec("SID=31d4d96e407aad42; Path=/; Domain=example.com",
            now, &uriparts);
        ASSERT_STREQ("SID=31d4d96e407aad42; SID=31d4d96e407aad42", cookieJar->cookieSpec(now, &uriparts));
            /*
             * RFC 6265 section "5.4. The Cookie Header" suggests that sending the identical cookie
             * name/value pair is indeed correct!
             *
             * Safari 6.0.2 on Mac seems to agree.
             */
        cookieJar->addCookieSpec("SID=31d4d96e407aad42; Path=/; Secure; HttpOnly, lang=en-US; Path=/; Domain=example.com",
            now, &uriparts);
            /* mark one of the "SID" cookies Secure; the other "SID" cookie still remains */
        ASSERT_STREQ("SID=31d4d96e407aad42; lang=en-US", cookieJar->cookieSpec(now, &uriparts));
        cookieJar->addCookieSpec("lang=en-US; Path=/; Domain=example.com; Expires=Sun, 06 Nov 1994 08:49:37 GMT",
            now, &uriparts);
            /* expire the "lang" cookie */
        ASSERT_STREQ("SID=31d4d96e407aad42", cookieJar->cookieSpec(now, &uriparts));
    }
}
