/*
 * NetKit/WebConnectionTest.cpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#include <NetKit/WebConnection.hpp>
#include <gtest/gtest.h>

TEST(WebConnectionTest, SyncBasic)
{
    mark_and_collect
    {
        NetKit::WebConnection *connection;
        NetKit::WebRequest *request;
        NetKit::WebResponse *response;
        Base::Stream *istream;
        char buf[4096];

        connection = new (Base::collect) NetKit::WebConnection;
        request = connection->request();

        ASSERT_FALSE(connection->sendrecv());
        request->setUri("protocol-does-not-exist:");
        ASSERT_FALSE(connection->sendrecv());

        request->setUri("http://host-does-not-exist");
        ASSERT_FALSE(connection->sendrecv());

        request->setUri("http://google.com/");
        ASSERT_TRUE(connection->sendrecv());
        response = connection->response();
        ASSERT_EQ(200, response->statusCode());
        ASSERT_STREQ("HTTP/1.1 200 OK", response->statusLine());
        ASSERT_STREQ("http://www.google.com/", response->uri());
        ASSERT_STREQ("gws", response->header("server"));
        foreach (Base::CString *key, response->headers())
        {
            (void)key;
        }
        istream = response->bodyStream();
        while (0 < istream->read(buf, sizeof buf))
        {
        }

        request->setUri("http://www.google.com/");
        ASSERT_TRUE(connection->sendrecv());
        response = connection->response();
        ASSERT_EQ(200, response->statusCode());
        ASSERT_STREQ("HTTP/1.1 200 OK", response->statusLine());
        ASSERT_STREQ("http://www.google.com/", response->uri());
        ASSERT_STREQ("gws", response->header("server"));
        foreach (Base::CString *key, response->headers())
        {
            (void)key;
        }
        istream = response->bodyStream();
        while (0 < istream->read(buf, sizeof buf))
        {
        }

        request->setUri("http://google.com/");
        request->setRedirectsAllowed(false);
        ASSERT_TRUE(connection->sendrecv());
        response = connection->response();
        ASSERT_EQ(301, response->statusCode());
    }
}
void WebConnectionDelegateFail(NetKit::WebConnection *connection, int event)
{
    switch (event)
    {
    case NetKit::WebConnection::EventReceiveHeader:
    case NetKit::WebConnection::EventReceiveData:
    case NetKit::WebConnection::EventSuccess:
        FAIL();
        break;
    case NetKit::WebConnection::EventFailure:
        Base::ThreadLoopStop();
        break;
    }
}
void WebConnectionDelegate200(NetKit::WebConnection *connection, int event)
{
    NetKit::WebResponse *response = connection->response();
    switch (event)
    {
    case NetKit::WebConnection::EventReceiveHeader:
        ASSERT_EQ(200, response->statusCode());
        ASSERT_STREQ("HTTP/1.1 200 OK", response->statusLine());
        ASSERT_STREQ("http://www.google.com/", response->uri());
        ASSERT_STREQ("gws", response->header("server"));
        break;
    case NetKit::WebConnection::EventReceiveData:
        {
            char buf[4096];
            ASSERT_TRUE(0 <= response->bodyStream()->read(buf, sizeof buf));
        }
        break;
    case NetKit::WebConnection::EventSuccess:
        Base::ThreadLoopStop();
        break;
    case NetKit::WebConnection::EventFailure:
        FAIL();
        break;
    }
}
void WebConnectionDelegate301(NetKit::WebConnection *connection, int event)
{
    NetKit::WebResponse *response = connection->response();
    switch (event)
    {
    case NetKit::WebConnection::EventReceiveHeader:
        ASSERT_EQ(301, response->statusCode());
        break;
    case NetKit::WebConnection::EventReceiveData:
        {
            char buf[4096];
            ASSERT_TRUE(0 <= response->bodyStream()->read(buf, sizeof buf));
        }
        break;
    case NetKit::WebConnection::EventSuccess:
        Base::ThreadLoopStop();
        break;
    case NetKit::WebConnection::EventFailure:
        FAIL();
        break;
    }
}
TEST(WebConnectionTest, AsyncBasic)
{
    mark_and_collect
    {
        NetKit::WebConnection *connection;
        NetKit::WebRequest *request;

        Base::ThreadLoopInit();
        connection = new (Base::collect) NetKit::WebConnection;
        request = connection->request();

        request->setUri("http://host-does-not-exist");
        connection->setDelegate(make_delegate(WebConnectionDelegateFail));
        ASSERT_TRUE(connection->sendrecv(true));
        Base::ThreadLoopRun();

        request->setUri("http://google.com/");
        connection->setDelegate(make_delegate(WebConnectionDelegate200));
        ASSERT_TRUE(connection->sendrecv(true));
        Base::ThreadLoopRun();

        request->setUri("http://www.google.com/");
        connection->setDelegate(make_delegate(WebConnectionDelegate200));
        ASSERT_TRUE(connection->sendrecv(true));
        Base::ThreadLoopRun();

        request->setUri("http://google.com/");
        connection->setDelegate(make_delegate(WebConnectionDelegate301));
        request->setRedirectsAllowed(false);
        ASSERT_TRUE(connection->sendrecv(true));
        Base::ThreadLoopRun();
    }
}
