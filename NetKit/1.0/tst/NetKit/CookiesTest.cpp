/*
 * NetKit/CookiesTest.cpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#include <NetKit/Cookies.hpp>
#include <gtest/gtest.h>

TEST(CookiesTest, parse)
{
    const char *slash = "/";
    const char *myhost = "myhost";
    const char *mypath1 = "mypath";
    const char *mypath2 = "/mypath";
    const char *mypath3 = "/mypath/";
    const char *mypath4 = "/mypath/mypath";
    const char *mypath5 = "/mypath/mypath/";
    const char *mypath1res = "/";
    const char *mypath2res = "/";
    const char *mypath3res = "/mypath/";
    const char *mypath4res = "/mypath/";
    const char *mypath5res = "/mypath/mypath/";
    const char *specs[] =
    {
        /* netscape examples */
        "CUSTOMER=WILE_E_COYOTE; path=/; expires=Wednesday, 09-Nov-99 23:12:40 GMT",
        "CUSTOMER=WILE_E_COYOTE",
        "PART_NUMBER=ROCKET_LAUNCHER_0001; path=/",
        "CUSTOMER=WILE_E_COYOTE; PART_NUMBER=ROCKET_LAUNCHER_0001",
        "SHIPPING=FEDEX; path=/foo",
        "CUSTOMER=WILE_E_COYOTE; PART_NUMBER=ROCKET_LAUNCHER_0001",
        "CUSTOMER=WILE_E_COYOTE; PART_NUMBER=ROCKET_LAUNCHER_0001; SHIPPING=FEDEX",
        "PART_NUMBER=ROCKET_LAUNCHER_0001; path=/",
        "PART_NUMBER=ROCKET_LAUNCHER_0001",
        "PART_NUMBER=RIDING_ROCKET_0023; path=/ammo",
        "PART_NUMBER=RIDING_ROCKET_0023; PART_NUMBER=ROCKET_LAUNCHER_0001",
        /* rfc 2109 examples */
        "Customer=\"WILE_E_COYOTE\"; Version=\"1\"; Path=\"/acme\"",
        "$Version=\"1\"; Customer=\"WILE_E_COYOTE\"; $Path=\"/acme\"",
        "Part_Number=\"Rocket_Launcher_0001\"; Version=\"1\"; Path=\"/acme\"",
        "$Version=\"1\"; Customer=\"WILE_E_COYOTE\"; $Path=\"/acme\"; Part_Number=\"Rocket_Launcher_0001\"; $Path=\"/acme\"",
        "Shipping=\"FedEx\"; Version=\"1\"; Path=\"/acme\"",
        "$Version=\"1\"; Customer=\"WILE_E_COYOTE\"; $Path=\"/acme\"; Part_Number=\"Rocket_Launcher_0001\"; $Path=\"/acme\"; Shipping=\"FedEx\"; $Path=\"/acme\"",
        "Part_Number=\"Rocket_Launcher_0001\"; Version=\"1\"; Path=\"/acme\"",
        "Part_Number=\"Riding_Rocket_0023\"; Version=\"1\"; Path=\"/acme/ammo\"",
        "$Version=\"1\"; Part_Number=\"Riding_Rocket_0023\"; $Path=\"/acme/ammo\"; Part_Number=\"Rocket_Launcher_0001\"; $Path=\"/acme\"",
        "$Version=\"1\"; Part_Number=\"Rocket_Launcher_0001\"; $Path=\"/acme\"",
        /* rfc 6265 examples */
        "SID=31d4d96e407aad42",
        "SID=31d4d96e407aad42",
        "SID=31d4d96e407aad42; Path=/; Domain=example.com",
        "SID=31d4d96e407aad42; Path=/; Secure; HttpOnly",
        "lang=en-US; Path=/; Domain=example.com",
        "SID=31d4d96e407aad42; lang=en-US",
        "lang=en-US; Expires=Wed, 09 Jun 2021 10:18:14 GMT",
        "lang=; Expires=Sun, 06 Nov 1994 08:49:37 GMT",
        /* misc */
        "session-id=1234567; max-age=86400; domain=example.com; path=/;",
        "  session-id = 1234567  ; max-age = 86400  ; domain = example.com ; path = / ;",
        "CUSTOMER=WILE_E_COYOTE; expires=Sun Nov  6 08:49:37 1994; path=/",
        "CUSTOMER=WILE_E_COYOTE; expires=Sun Nov  6 08:49:37 1994; path=/   , ",
        "CUSTOMER=WILE_E_COYOTE; path=/; expires=Wednesday, 09-Nov-99 23:12:40 GMT  ,  ",
        "SID=31d4d96e407aad42; Path=/; Domain=.com",
        "SID=31d4d96e407aad42; Path=/; Domain=.co.uk",
        "SID=31d4d96e407aad42",
        "SID=31d4d96e407aad42",
        "SID=31d4d96e407aad42",
        "SID=31d4d96e407aad42",
        "SID=31d4d96e407aad42",
        "SID=31d4d96e407aad42; Domain=example.com",
        "SID=31d4d96e407aad42; Domain=fxample.com",
        "SID=31d4d96e407aad42; Domain=example.com",
        "SID=31d4d96e407aad42; Domain=example.com",
        "SID=31d4d96e407aad42; Domain=foo.example.com",
        "SID=31d4d96e407aad42; Domain=127.0.0.1",
        "SID=31d4d96e407aad42; Domain=0.1",
        "SID=31d4d96e407aad42; Domain=2001:0db8:85a3:0000:0000:8a2e:0370:7334",
        "SID=31d4d96e407aad42; Domain=::1",
        "SID=31d4d96e407aad42; Domain=1.com",
        "",
        "",
        "  \t",
        "  \t",
    };
    struct
    {
        const char *spec;
        bool isSetCookie;
        double now;
        const char *host;
        const char *path;
        const char *endp;
        NetKit::cookie_parts parts;
    } tests[] =
    {
        /* netscape examples */
        { specs[ 0], 1, 0, 0, 0, specs[ 0]+73, { specs[0], specs[0]+8, specs[0]+9, specs[0]+22, 2451492.4671296296, 1, 0, 0, specs[0]+29, specs[0]+30 } },
        { specs[ 1], 0, 0, 0, 0, specs[ 1]+22, { specs[1], specs[1]+8, specs[1]+9, specs[1]+22 } },
        { specs[ 2], 1, 0, 0, 0, specs[ 2]+40, { specs[2], specs[2]+11, specs[2]+12, specs[2]+32, 0, 1, 0, 0, specs[2]+39, specs[2]+40 } },
        { specs[ 3], 0, 0, 0, 0, specs[ 3]+23, { specs[3], specs[3]+8, specs[3]+9, specs[3]+22 } },
        { specs[ 4], 1, 0, 0, 0, specs[ 4]+25, { specs[4], specs[4]+8, specs[4]+9, specs[4]+14, 0, 1, 0, 0, specs[4]+21, specs[4]+25 } },
        { specs[ 5], 0, 0, 0, 0, specs[ 5]+23, { specs[5], specs[5]+8, specs[5]+9, specs[5]+22 } },
        { specs[ 6], 0, 0, 0, 0, specs[ 6]+23, { specs[6], specs[6]+8, specs[6]+9, specs[6]+22 } },
        { specs[ 7], 1, 0, 0, 0, specs[ 7]+40, { specs[7], specs[7]+11, specs[7]+12, specs[7]+32, 0, 1, 0, 0, specs[7]+39, specs[7]+40 } },
        { specs[ 8], 0, 0, 0, 0, specs[ 8]+32, { specs[8], specs[8]+11, specs[8]+12, specs[8]+32 } },
        { specs[ 9], 1, 0, 0, 0, specs[ 9]+42, { specs[9], specs[9]+11, specs[9]+12, specs[9]+30, 0, 1, 0, 0, specs[9]+37, specs[9]+42 } },
        { specs[10], 0, 0, 0, 0, specs[10]+31, { specs[10], specs[10]+11, specs[10]+12, specs[10]+30 }},
        /* rfc 2109 examples */
        { specs[11], 1, 0, 0, 0, specs[11]+51, { specs[11], specs[11]+8, specs[11]+10, specs[11]+23, 0, 1, 0, 0, specs[11]+45, specs[11]+50 } },
        { specs[12], 0, 0, 0, 0, specs[12]+39, { specs[12]+14, specs[12]+22, specs[12]+24, specs[12]+37 } },
        { specs[13], 1, 0, 0, 0, specs[13]+61, { specs[13], specs[13]+11, specs[13]+13, specs[13]+33, 0, 1, 0, 0, specs[13]+55, specs[13]+60 } },
        { specs[14], 0, 0, 0, 0, specs[14]+39, { specs[14]+14, specs[14]+22, specs[14]+24, specs[14]+37 } },
        { specs[15], 1, 0, 0, 0, specs[15]+43, { specs[15], specs[15]+8, specs[15]+10, specs[15]+15, 0, 1, 0, 0, specs[15]+37, specs[15]+42 } },
        { specs[16], 0, 0, 0, 0, specs[16]+39, { specs[16]+14, specs[16]+22, specs[16]+24, specs[16]+37 } },
        { specs[17], 1, 0, 0, 0, specs[17]+61, { specs[17], specs[17]+11, specs[17]+13, specs[17]+33, 0, 1, 0, 0, specs[17]+55, specs[17]+60 } },
        { specs[18], 1, 0, 0, 0, specs[18]+64, { specs[18], specs[18]+11, specs[18]+13, specs[18]+31, 0, 1, 0, 0, specs[18]+53, specs[18]+63 } },
        { specs[19], 0, 0, 0, 0, specs[19]+47, { specs[19]+14, specs[19]+25, specs[19]+27, specs[19]+45 } },
        { specs[20], 0, 0, 0, 0, specs[20]+49, { specs[20]+14, specs[20]+25, specs[20]+27, specs[20]+47 } },
        /* rfc 6265 examples */
        { specs[21], 1, 0, 0, 0, specs[21]+20, { specs[21], specs[21]+3, specs[21]+4, specs[21]+20, 0, 1, 0, 0, slash, slash+1 } },
        { specs[22], 0, 0, 0, 0, specs[22]+20, { specs[22], specs[22]+3, specs[22]+4, specs[22]+20 } },
        { specs[23], 1, 0, 0, 0, specs[23]+48, { specs[23], specs[23]+3, specs[23]+4, specs[23]+20, 0, 0, specs[23]+37, specs[23]+48, specs[23]+27, specs[23]+28 } },
        { specs[24], 1, 0, 0, 0, specs[24]+46, { specs[24], specs[24]+3, specs[24]+4, specs[24]+20, 0, 1, 0, 0, specs[24]+27, specs[24]+28, true, true } },
        { specs[25], 1, 0, 0, 0, specs[25]+38, { specs[25], specs[25]+4, specs[25]+5, specs[25]+10, 0, 0, specs[25]+27, specs[25]+38, specs[25]+17, specs[25]+18 } },
        { specs[26], 0, 0, 0, 0, specs[26]+21, { specs[26], specs[26]+3, specs[26]+4, specs[26]+20 } },
        { specs[27], 1, 0, 0, 0, specs[27]+49, { specs[27], specs[27]+4, specs[27]+5, specs[27]+10, 2459374.9293287038, 1, 0, 0, slash, slash+1 } },
        { specs[28], 1, 0, 0, 0, specs[28]+44, { specs[28], specs[28]+4, specs[28]+5, specs[28]+5, 2449662.8677893518, 1, 0, 0, slash, slash+1 } },
        /* misc */
        { specs[29], 1, 2456279.5, 0, 0, specs[29]+62, { specs[29], specs[29]+10, specs[29]+11, specs[29]+18, 2456280.5, 0, specs[29]+42, specs[29]+53, specs[29]+60, specs[29]+61 } },
        { specs[30], 1, 2456279.5, 0, 0, specs[30]+78, { specs[30]+2, specs[30]+12, specs[30]+15, specs[30]+22, 2456280.5, 0, specs[30]+54, specs[30]+65, specs[30]+75, specs[30]+76 } },
        { specs[31], 1, 0, 0, 0, specs[31]+64, { specs[31], specs[31]+8, specs[31]+9, specs[31]+22, 2449662.8677893518, 1, 0, 0, specs[31]+63, specs[31]+64 } },
        { specs[32], 1, 0, 0, 0, specs[32]+68, { specs[32], specs[32]+8, specs[32]+9, specs[32]+22, 2449662.8677893518, 1, 0, 0, specs[32]+63, specs[32]+64 } },
        { specs[33], 1, 0, 0, 0, specs[33]+76, { specs[33], specs[33]+8, specs[33]+9, specs[33]+22, 2451492.4671296296, 1, 0, 0, specs[33]+29, specs[33]+30 } },
        { specs[34], 1, 0, 0, 0, specs[34]+41, { 0 } },
        { specs[35], 1, 0, 0, 0, specs[35]+43, { 0 } },
        { specs[36], 1, 0, myhost, mypath1, specs[36]+20, { specs[36], specs[36]+3, specs[36]+4, specs[36]+20, 0, 1, myhost, myhost+strlen(myhost), mypath1res, mypath1res+strlen(mypath1res) } },
        { specs[37], 1, 0, myhost, mypath2, specs[37]+20, { specs[37], specs[37]+3, specs[37]+4, specs[37]+20, 0, 1, myhost, myhost+strlen(myhost), mypath2res, mypath2res+strlen(mypath2res) } },
        { specs[38], 1, 0, myhost, mypath3, specs[38]+20, { specs[38], specs[38]+3, specs[38]+4, specs[38]+20, 0, 1, myhost, myhost+strlen(myhost), mypath3res, mypath3res+strlen(mypath3res) } },
        { specs[39], 1, 0, myhost, mypath4, specs[39]+20, { specs[39], specs[39]+3, specs[39]+4, specs[39]+20, 0, 1, myhost, myhost+strlen(myhost), mypath4res, mypath4res+strlen(mypath4res) } },
        { specs[40], 1, 0, myhost, mypath5, specs[40]+20, { specs[40], specs[40]+3, specs[40]+4, specs[40]+20, 0, 1, myhost, myhost+strlen(myhost), mypath5res, mypath5res+strlen(mypath5res) } },
        { specs[41], 1, 0, "example.com", 0, specs[41]+40, { specs[41], specs[41]+3, specs[41]+4, specs[41]+20, 0, 0, specs[41]+29, specs[41]+40, slash, slash+1 } },
        { specs[42], 1, 0, "example.com", 0, specs[42]+40, { 0 } },
        { specs[43], 1, 0, "foo.example.com", 0, specs[43]+40, { specs[43], specs[43]+3, specs[43]+4, specs[43]+20, 0, 0, specs[43]+29, specs[43]+40, slash, slash+1 } },
        { specs[44], 1, 0, "fooexample.com", 0, specs[44]+40, { 0 } },
        { specs[45], 1, 0, "example.com", 0, specs[45]+44, { 0 } },
        { specs[46], 1, 0, 0, 0, specs[46]+38, { 0 } },
        { specs[47], 1, 0, 0, 0, specs[47]+32, { 0 } },
        { specs[48], 1, 0, 0, 0, specs[48]+68, { 0 } },
        { specs[49], 1, 0, 0, 0, specs[49]+32, { 0 } },
        { specs[50], 1, 0, 0, 0, specs[50]+34, { specs[50], specs[50]+3, specs[50]+4, specs[50]+20, 0, 0, specs[50]+29, specs[50]+34, slash, slash+1 } },
        { specs[51], 0, 0, 0, 0, specs[51]+0, { 0 } },
        { specs[52], 1, 0, 0, 0, specs[52]+0, { 0 } },
        { specs[53], 0, 0, 0, 0, specs[53]+3, { 0 } },
        { specs[54], 1, 0, 0, 0, specs[54]+3, { 0 } },
    };
    BASE_COMPILE_ASSERT(NELEMS(specs) == NELEMS(tests));
    for (size_t i = 0; NELEMS(tests) > i; i++)
    {
        Base::cstr_uri_parts uriparts = { 0 };
        uriparts.host = tests[i].host;
        uriparts.path = tests[i].path;
        NetKit::cookie_parts parts;
        const char *endp;
        if (!tests[i].isSetCookie)
            NetKit::cookie_parse_req(tests[i].spec, &endp, &parts);
        else
            NetKit::cookie_parse_rsp(tests[i].spec, tests[i].now, &uriparts, &endp, &parts);
#define ASSERT_MEMRNGCMP(p1, q1, p2, q2) ASSERT_TRUE((q1) - (p1) == (q2) - (p2) && 0 == memcmp(p1, p2, (q1) - (p1)))
        ASSERT_STREQ(tests[i].endp, endp);
        ASSERT_EQ(tests[i].parts.name, parts.name);
        ASSERT_EQ(tests[i].parts.nameEnd, parts.nameEnd);
        ASSERT_EQ(tests[i].parts.value, parts.value);
        ASSERT_EQ(tests[i].parts.valueEnd, parts.valueEnd);
        ASSERT_TRUE(0 != tests[i].parts.expires ? tests[i].parts.expires == parts.expires : Base::isNone(parts.expires));
        ASSERT_EQ(tests[i].parts.hostOnly, parts.hostOnly);
        ASSERT_MEMRNGCMP(tests[i].parts.domain, tests[i].parts.domainEnd, parts.domain, parts.domainEnd);
        ASSERT_MEMRNGCMP(tests[i].parts.path, tests[i].parts.pathEnd, parts.path, parts.pathEnd);
        ASSERT_EQ(tests[i].parts.secure, parts.secure);
        ASSERT_EQ(tests[i].parts.httpOnly, parts.httpOnly);
#undef ASSERT_MEMRNGCMP
    }
}
TEST(CookiesTest, concat)
{
    const char *spec = 0;
    spec = NetKit::cookie_cstr_concat_req(spec, "SID", "31d4d96e407aad42");
    spec = NetKit::cookie_cstr_concat_req(spec, "lang", "en-US");
    ASSERT_STREQ("SID=31d4d96e407aad42; lang=en-US", spec);
    Base::cstr_release(spec);
    spec = 0;
    spec = NetKit::cookie_cstr_concat_rsp(spec, "CUSTOMER", "WILE_E_COYOTE", 2451492.4671296296, 0, "/");
    spec = NetKit::cookie_cstr_concat_rsp(spec, "SID", "31d4d96e407aad42", Base::None(), "example.com", "/", true, true);
    spec = NetKit::cookie_cstr_concat_rsp(spec, "lang", "en-US");
    ASSERT_STREQ(
        "CUSTOMER=WILE_E_COYOTE; Expires=Tue, 09 Nov 1999 23:12:40 GMT; Path=/, "
        /* the original Netscape cookie document got this wrong: 09 Nov 1999 was a Tuesday! */
        "SID=31d4d96e407aad42; Domain=example.com; Path=/; Secure; HttpOnly, "
        "lang=en-US", spec);
    Base::cstr_release(spec);
}
