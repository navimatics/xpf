/*
 * NetKit/WebSession.hpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef NETKIT_WEBSESSION_HPP_INCLUDED
#define NETKIT_WEBSESSION_HPP_INCLUDED

#include <NetKit/Defines.hpp>
#include <NetKit/CookieJar.hpp>
#include <NetKit/impl/WebSession.hpp>

namespace NetKit {

using NetKit::impl::WebSession;

}

#endif // NETKIT_WEBSESSION_HPP_INCLUDED
