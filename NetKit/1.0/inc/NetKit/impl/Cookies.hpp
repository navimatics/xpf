/*
 * NetKit/impl/Cookies.hpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef NETKIT_IMPL_COOKIES_HPP_INCLUDED
#define NETKIT_IMPL_COOKIES_HPP_INCLUDED

#include <NetKit/impl/Defines.hpp>

namespace NetKit {
namespace impl {

struct NETKIT_APISYM cookie_parts
{
    const char *name, *nameEnd;
    const char *value, *valueEnd;
    double expires; /* julian date */
    bool hostOnly;
    const char *domain, *domainEnd;
    const char *path, *pathEnd;
    bool secure;
    bool httpOnly;
};
void cookie_parse_req(const char *spec, const char **endp, cookie_parts *parts);
void cookie_parse_rsp(const char *spec, double now, const Base::cstr_uri_parts *uriparts,
    const char **endp, cookie_parts *parts);

/* cookie_cstr_concat_*() returns non-autoreleased strings! */
const char *cookie_cstr_concat_req(const char *spec, const char *name, const char *value);
const char *cookie_cstr_concat_rsp(const char *spec, const char *name, const char *value,
    double expires = Base::None(), const char *domain = 0, const char *path = 0,
    bool secure = false, bool httpOnly = false);

}
}

#endif // NETKIT_IMPL_COOKIES_HPP_INCLUDED
