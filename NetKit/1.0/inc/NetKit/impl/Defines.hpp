/*
 * NetKit/impl/Defines.hpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef NETKIT_IMPL_DEFINES_HPP_INCLUDED
#define NETKIT_IMPL_DEFINES_HPP_INCLUDED

#include <Base/Base.hpp>

#if defined(NETKIT_CONFIG_INTERNAL)
#define NETKIT_APISYM                   BASE_EXPORTSYM
#else
#define NETKIT_APISYM                   BASE_IMPORTSYM
#endif

namespace NetKit {
namespace impl {
}
}

#endif // NETKIT_IMPL_DEFINES_HPP_INCLUDED
