/*
 * NetKit/impl/WebSession.hpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef NETKIT_IMPL_WEBSESSION_HPP_INCLUDED
#define NETKIT_IMPL_WEBSESSION_HPP_INCLUDED

#include <NetKit/impl/Defines.hpp>
#include <NetKit/impl/CookieJar.hpp>

namespace NetKit {
namespace impl {

class NETKIT_APISYM WebSession : public Base::Object
{
public:
    WebSession();
    ~WebSession();
    Base::Lock *lock();
    const char *userAgent();
    void setUserAgent(const char *value);
    Cookie **cookies(double now, const Base::cstr_uri_parts *uriparts);
    void addCookie(Cookie *cookie, bool remove = false);
    const char *cookieSpec(double now, const Base::cstr_uri_parts *uriparts);
    void addCookieSpec(const char *spec, double now, const Base::cstr_uri_parts *uriparts);
private:
    const char *_userAgent;
    CookieJar *_cookieJar;
};

}
}

#endif // NETKIT_IMPL_WEBSESSION_HPP_INCLUDED
