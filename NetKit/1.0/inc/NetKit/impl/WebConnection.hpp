/*
 * NetKit/impl/WebConnection.hpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef NETKIT_IMPL_WEBCONNECTION_HPP_INCLUDED
#define NETKIT_IMPL_WEBCONNECTION_HPP_INCLUDED

#include <NetKit/impl/Defines.hpp>
#include <NetKit/impl/WebRequest.hpp>
#include <NetKit/impl/WebResponse.hpp>
#include <NetKit/impl/WebSession.hpp>

namespace NetKit {
namespace impl {

class NETKIT_APISYM WebConnection : public Base::Object
{
public:
    enum
    {
        EventReceiveHeader = 1,
        EventReceiveData,
        EventSuccess,
        EventFailure,
    };
    enum
    {
        ErrorMisuse = 1,
        ErrorConnection,
        ErrorRequest,
        ErrorResponse,
        ErrorMax = 1000,
    };
    typedef Base::Delegate<void (WebConnection *, int)> Delegate;
public:
    static const char *PropertyCellularData();
    static const char *errorDomain();
    static const char *defaultUserAgent();
    WebConnection(WebSession *session = 0);
    ~WebConnection();
    Delegate delegate();
    void setDelegate(Delegate value);
    WebSession *session();
    WebRequest *request();
    WebResponse *response();
    bool sendrecv(bool async = false);
    void close();
    virtual void onEvent(int event);
    virtual const char *_errorDomain();
    /* internal use only */
    void *__state();
    void __setState(void *value);
private:
    static const char *defaultUserAgentComment();
    bool openConnection(const Base::cstr_uri_parts *uriparts, bool async);
    void closeConnection();
    Base::Stream *createResponseStream(const Base::cstr_uri_parts *uriparts, bool async);
    void closeResponseStream();
private:
    Delegate _delegate;
    WebSession *_session;
    WebRequest *_request;
    WebResponse *_response;
    void *_state;
};

inline WebConnection::Delegate WebConnection::delegate()
{
    return _delegate;
}
inline void WebConnection::setDelegate(WebConnection::Delegate value)
{
    _delegate = value;
}
inline WebSession *WebConnection::session()
{
    return _session;
}
inline WebRequest *WebConnection::request()
{
    return _request;
}
inline WebResponse *WebConnection::response()
{
    return _response;
}
inline void *WebConnection::__state()
{
    return _state;
}
inline void WebConnection::__setState(void *value)
{
    _state = value;
}

/* internal use only */
void __WebConnectionSetRequestHeader(Base::Map *&headers, const char *name, const char *value,
    bool append = false);
Base::Map *__WebConnectionCreateExtraRequestHeaders(WebConnection *connection,
    const Base::cstr_uri_parts *uriparts);

}
}

#endif // NETKIT_IMPL_WEBCONNECTION_HPP_INCLUDED
