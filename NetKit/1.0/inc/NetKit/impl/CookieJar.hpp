/*
 * NetKit/impl/CookieJar.hpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef NETKIT_IMPL_COOKIEJAR_HPP_INCLUDED
#define NETKIT_IMPL_COOKIEJAR_HPP_INCLUDED

#include <NetKit/impl/Defines.hpp>
#include <NetKit/impl/Cookies.hpp>

namespace NetKit {
namespace impl {

class NETKIT_APISYM Cookie : public Base::Object
{
public:
    Cookie(const cookie_parts *ckparts);
    Cookie(const char *name, const char *value, double expires,
        bool hostOnly, const char *domain, const char *path,
        bool secure = false, bool httpOnly = false);
    ~Cookie();
    int cmp(Object *obj0);
    const char *name();
    const char *value();
    double expires(); /* julian date */
    bool hostOnly();
    const char *domain();
    const char *path();
    bool secure();
    bool httpOnly();
private:
    unsigned _cookieOrd;
    const char *_name;
    const char *_value;
    double _expires;
    bool _hostOnly;
    const char *_domain;
    const char *_path;
    bool _secure;
    bool _httpOnly;
};
inline const char *Cookie::name()
{
    return _name;
}
inline const char *Cookie::value()
{
    return _value;
}
inline double Cookie::expires()
{
    return _expires;
}
inline bool Cookie::hostOnly()
{
    return _hostOnly;
}
inline const char *Cookie::domain()
{
    return _domain;
}
inline const char *Cookie::path()
{
    return _path;
}
inline bool Cookie::secure()
{
    return _secure;
}
inline bool Cookie::httpOnly()
{
    return _httpOnly;
}

class NETKIT_APISYM CookieJar : public Base::Object
{
public:
    CookieJar();
    ~CookieJar();
    Base::Lock *lock();
    Cookie **cookies(double now, const Base::cstr_uri_parts *uriparts);
    void addCookie(Cookie *cookie, bool remove = false);
    const char *cookieSpec(double now, const Base::cstr_uri_parts *uriparts);
    void addCookieSpec(const char *spec, double now, const Base::cstr_uri_parts *uriparts);
    void evictCookies(double now);
private:
    Base::Lock *_lock;
    Base::Map *_cookies;
};

}
}

#endif // NETKIT_IMPL_COOKIEJAR_HPP_INCLUDED
