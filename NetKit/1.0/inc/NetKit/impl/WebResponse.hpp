/*
 * NetKit/impl/WebResponse.hpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef NETKIT_IMPL_WEBRESPONSE_HPP_INCLUDED
#define NETKIT_IMPL_WEBRESPONSE_HPP_INCLUDED

#include <NetKit/impl/Defines.hpp>

namespace NetKit {
namespace impl {

class NETKIT_APISYM WebResponse : public Base::Object
{
public:
    WebResponse();
    ~WebResponse();
    void reset();
    void _resetHeader();
    const char *uri();
    void setUri(const char *value);
    int statusCode();
    void setStatusCode(int value);
    const char *statusLine();
    void setStatusLine(const char *value);
    const char *header(const char *name);
    void setHeader(const char *name, const char *value, bool append = false);
    Base::Map *headers();
    const char *cookie(const char *name);
    void addCookie(const char *name, const char *value,
        double expires = Base::None(), const char *domain = 0, const char *path = 0,
        bool secure = false, bool httpOnly = false);
    Base::Stream *bodyStream();
    void setBodyStream(Base::Stream *value);
private:
    void initCookieQueryData();
    void finiCookieQueryData();
private:
    const char *_uri;
    int _statusCode;
    const char *_statusLine;
    Base::Map *_headers;
    Base::Stream *_bodyStream;
    /* cookie query data */
    bool _ckqValid;
    Base::cstr_uri_parts _ckqUriparts;
    double _ckqTimestamp;
};

inline const char *WebResponse::uri()
{
    return _uri;
}
inline int WebResponse::statusCode()
{
    return _statusCode;
}
inline const char *WebResponse::statusLine()
{
    return _statusLine;
}
inline Base::Map *WebResponse::headers()
{
    return _headers;
}
inline Base::Stream *WebResponse::bodyStream()
{
    return _bodyStream;
}

}
}

#endif // NETKIT_IMPL_WEBRESPONSE_HPP_INCLUDED
