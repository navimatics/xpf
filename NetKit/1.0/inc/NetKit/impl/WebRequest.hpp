/*
 * NetKit/impl/WebRequest.hpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef NETKIT_IMPL_WEBREQUEST_HPP_INCLUDED
#define NETKIT_IMPL_WEBREQUEST_HPP_INCLUDED

#include <NetKit/impl/Defines.hpp>

namespace NetKit {
namespace impl {

class NETKIT_APISYM WebRequest : public Base::Object
{
public:
    WebRequest();
    ~WebRequest();
    void reset();
    const char *uri();
    void setUri(const char *value);
    const char *method();
    void setMethod(const char *value);
    const char *header(const char *name);
    void setHeader(const char *name, const char *value, bool append = false);
    Base::Map *headers();
    const char *cookie(const char *name);
    void addCookie(const char *name, const char *value);
    Base::Stream *bodyStream();
    void setBodyStream(Base::Stream *value);
    bool redirectsAllowed();
    void setRedirectsAllowed(bool value);
private:
    const char *_uri;
    const char *_method;
    Base::Map *_headers;
    Base::Stream *_bodyStream;
    bool _redirectsNotAllowed;
};

inline const char *WebRequest::uri()
{
    return _uri;
}
inline const char *WebRequest::method()
{
    return _method;
}
inline Base::Map *WebRequest::headers()
{
    return _headers;
}
inline Base::Stream *WebRequest::bodyStream()
{
    return _bodyStream;
}
inline bool WebRequest::redirectsAllowed()
{
    return !_redirectsNotAllowed;
}

}
}

#endif // NETKIT_IMPL_WEBREQUEST_HPP_INCLUDED
