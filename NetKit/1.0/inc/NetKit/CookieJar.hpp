/*
 * NetKit/CookieJar.hpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef NETKIT_COOKIEJAR_HPP_INCLUDED
#define NETKIT_COOKIEJAR_HPP_INCLUDED

#include <NetKit/Defines.hpp>
#include <NetKit/Cookies.hpp>
#include <NetKit/impl/CookieJar.hpp>

namespace NetKit {

using NetKit::impl::Cookie;
using NetKit::impl::CookieJar;

}

#endif // NETKIT_COOKIEJAR_HPP_INCLUDED
