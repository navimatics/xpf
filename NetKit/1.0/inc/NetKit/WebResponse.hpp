/*
 * NetKit/WebResponse.hpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef NETKIT_WEBRESPONSE_HPP_INCLUDED
#define NETKIT_WEBRESPONSE_HPP_INCLUDED

#include <NetKit/Defines.hpp>
#include <NetKit/impl/WebResponse.hpp>

namespace NetKit {

using NetKit::impl::WebResponse;

}

#endif // NETKIT_WEBRESPONSE_HPP_INCLUDED
