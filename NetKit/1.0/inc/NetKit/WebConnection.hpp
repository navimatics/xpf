/*
 * NetKit/WebConnection.hpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef NETKIT_WEBCONNECTION_HPP_INCLUDED
#define NETKIT_WEBCONNECTION_HPP_INCLUDED

#include <NetKit/Defines.hpp>
#include <NetKit/WebRequest.hpp>
#include <NetKit/WebResponse.hpp>
#include <NetKit/WebSession.hpp>
#include <NetKit/impl/WebConnection.hpp>

namespace NetKit {

using NetKit::impl::WebConnection;

}

#endif // NETKIT_WEBCONNECTION_HPP_INCLUDED
