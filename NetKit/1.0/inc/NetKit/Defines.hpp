/*
 * NetKit/Defines.hpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef NETKIT_DEFINES_HPP_INCLUDED
#define NETKIT_DEFINES_HPP_INCLUDED

#include <NetKit/impl/Defines.hpp>

namespace NetKit {
}

#endif // NETKIT_DEFINES_HPP_INCLUDED
