/*
 * NetKit/WebRequest.hpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef NETKIT_WEBREQUEST_HPP_INCLUDED
#define NETKIT_WEBREQUEST_HPP_INCLUDED

#include <NetKit/Defines.hpp>
#include <NetKit/impl/WebRequest.hpp>

namespace NetKit {

using NetKit::impl::WebRequest;

}

#endif // NETKIT_WEBREQUEST_HPP_INCLUDED
