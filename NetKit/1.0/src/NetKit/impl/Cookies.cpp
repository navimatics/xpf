/*
 * NetKit/impl/Cookies.cpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#include <NetKit/impl/Cookies.hpp>

namespace NetKit {
namespace impl {

static const char *wkdays[] = { "Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat" };
static const char *months[] = { "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" };
static const char *tlds[] = { "COM", "EDU", "NET", "ORG", "GOV", "MIL", "INT" };

static inline bool strmem_iequals(const char *s, const void *p, size_t size)
{
    if (strlen(s) != size)
        return false;
    return 0 == memicmp(s, p, size);
}
static inline void ltrim(const char *&p)
{
    while (' ' == *p || '\t' == *p)
        p++;
}
static inline void ltrim(const char *&p, const char *q)
{
    while (p < q && (' ' == *p || '\t' == *p))
        p++;
}
static inline void rtrim(const char *p, const char *&q)
{
    if (p < q)
    {
        q--;
        while (p <= q && (' ' == *q || '\t' == *q))
            q--;
        q++;
    }
}
static inline unsigned long parse_ulong(const char *&p, const char *q)
{
    unsigned long v = 0;
    while (p < q && isdigit(*p))
        v = 10 * v + (*p++ - '0');
    return v;
}
static inline void cookie_attn(const char *&p, const char *&q)
{
    ltrim(p);
    q = p;
    while ('\0' != *q && ',' != *q && ';' != *q && '=' != *q)
        q++;
    rtrim(p, q);
}
static inline char cookie_attn_end(const char *&p)
{
    ltrim(p);
    char c = *p;
    if (',' == c || ';' == c || '=' == c)
        p++;
    return c;
}
static inline void cookie_attv(const char *&p, const char *&q, bool allowOneComma)
{
    ltrim(p);
    if ('\"' == *p)
    {
        p++;
        q = p;
        while ('\0' != *q && '\"' != *q)
        {
            if ('\\' == *q && '\0' != q[1])
                q++;
            q++;
        }
    }
    else
    {
        q = p;
    loop:
        while ('\0' != *q && ',' != *q && ';' != *q)
            q++;
        if (',' == *q && allowOneComma && q - p <= 9/* max distance for ',': "Expires: Wednesday, ..." */)
        {
            q++;
            allowOneComma = false;
            goto loop;
        }
        rtrim(p, q);
    }
}
static inline char cookie_attv_end(const char *&p)
{
    if ('\"' == *p)
    {
        p++;
        while ('\0' != *p && ',' != *p && ';' != *p)
            p++;
    }
    else
        ltrim(p);
    char c = *p;
    if (',' == c || ';' == c)
        p++;
    return c;
}
static double cookie_parse_date(const char *p, const char *q)
{
    Base::DateTime::Components c = { 0 };
    size_t i;
    /* skip day of week */
    while (p < q && ' ' != *p && '\t' != *p)
        p++;
    ltrim(p, q);
    if (p < q && isdigit(*p))
    {
        /* RFC 1123/850 date  */
        c.day = parse_ulong(p, q);
        if (0 == c.day)
            return Base::None();
        if (' ' != *p && '-' != *p)
            return Base::None();
        p++;
        if (p + 3 > q)
            return Base::None();
        for (i = 0; NELEMS(months) > i; i++)
            if (0 == memicmp(months[i], p, 3))
            {
                p += 3;
                break;
            }
        if (NELEMS(months) == i)
            return Base::None();
        c.month = i + 1;
        if (' ' != *p && '-' != *p)
            return Base::None();
        p++;
        c.year = parse_ulong(p, q);
        if (90 > c.year)
            c.year += 2000;
        else if (100 > c.year)
            c.year += 1900;
        ltrim(p, q);
        c.hour = parse_ulong(p, q);
        if (':' != *p++)
            return Base::None();
        c.min = parse_ulong(p, q);
        if (':' != *p++)
            return Base::None();
        c.sec = parse_ulong(p, q);
        ltrim(p, q);
        if (p < q)
            if (p + 3 > q || 0 != memicmp("GMT", p, 3))
                return Base::None();
    }
    else
    {
        /* asctime date */
        if (p + 3 > q)
            return Base::None();
        for (i = 0; NELEMS(months) > i; i++)
            if (0 == memicmp(months[i], p, 3))
            {
                p += 3;
                break;
            }
        if (NELEMS(months) == i)
            return Base::None();
        c.month = i + 1;
        ltrim(p, q);
        c.day = parse_ulong(p, q);
        if (0 == c.day)
            return Base::None();
        ltrim(p, q);
        c.hour = parse_ulong(p, q);
        if (':' != *p++)
            return Base::None();
        c.min = parse_ulong(p, q);
        if (':' != *p++)
            return Base::None();
        c.sec = parse_ulong(p, q);
        ltrim(p, q);
        c.year = parse_ulong(p, q);
    }
    return Base::DateTime::julianDateFromComponents(c);
}
void cookie_parse_req(const char *spec, const char **endp, cookie_parts *outparts)
{
    memset(outparts, 0, sizeof *outparts);
    outparts->expires = Base::None();
    cookie_parts ckparts = { 0 };
    ckparts.expires = Base::None();
    const char *p = spec;
    char c;
    do
    {
        const char *attnp = 0, *attnq = 0;
        const char *attvp = 0, *attvq = 0;
        attnp = p;
        cookie_attn(attnp, attnq);
        p = attnq;
        c = cookie_attn_end(p);
        if ('=' == c)
        {
            attvp = p;
            cookie_attv(attvp, attvq, false);
            p = attvq;
            c = cookie_attv_end(p);
        }
        if (attnp == attnq)
            continue;
        if (strmem_iequals("$domain", attnp, attnq - attnp) ||
            strmem_iequals("$path", attnp, attnq - attnp) ||
            strmem_iequals("$version", attnp, attnq - attnp))
        {
            /* ignore */
        }
        else
        {
            /* NAME=VALUE pair */
            ckparts.name = attnp;
            ckparts.nameEnd = attnq;
            ckparts.value = attvp;
            ckparts.valueEnd = attvq;
            break;
        }
    } while (';' == c);
    if (0 != endp)
        *endp = p;
    *outparts = ckparts;
}
void cookie_parse_rsp(const char *spec, double now, const Base::cstr_uri_parts *uriparts,
    const char **endp, cookie_parts *outparts)
{
    memset(outparts, 0, sizeof *outparts);
    outparts->expires = Base::None();
    const char *p = spec;
    char c;
retry:
    cookie_parts ckparts = { 0 };
    ckparts.expires = Base::None();
    do
    {
        const char *attnp = 0, *attnq = 0;
        const char *attvp = 0, *attvq = 0;
        attnp = p;
        cookie_attn(attnp, attnq);
        p = attnq;
        c = cookie_attn_end(p);
        if ('=' == c)
        {
            attvp = p;
            cookie_attv(attvp, attvq,
                0 != ckparts.name && strmem_iequals("expires", attnp, attnq - attnp));
            p = attvq;
            c = cookie_attv_end(p);
        }
        if (attnp == attnq)
            continue;
        if (0 == ckparts.name)
        {
            /* NAME=VALUE pair */
            ckparts.name = attnp;
            ckparts.nameEnd = attnq;
            ckparts.value = attvp;
            ckparts.valueEnd = attvq;
        }
        else if (strmem_iequals("expires", attnp, attnq - attnp))
        {
            ckparts.expires = cookie_parse_date(attvp, attvq);
        }
        else if (strmem_iequals("max-age", attnp, attnq - attnp))
        {
            ckparts.expires = now + (double)parse_ulong(attvp, attvq) / 86400;
            if (attvp < attvq)
                ckparts.expires = Base::None();
        }
        else if (strmem_iequals("domain", attnp, attnq - attnp))
        {
            if (attvp < attvq && '.' == *attvp)
                attvp++;
            ckparts.domain = attvp;
            ckparts.domainEnd = attvq;
        }
        else if (strmem_iequals("path", attnp, attnq - attnp))
        {
            ckparts.path = attvp;
            ckparts.pathEnd = attvq;
        }
        else if (strmem_iequals("secure", attnp, attnq - attnp))
        {
            ckparts.secure = true;
        }
        else if (strmem_iequals("httponly", attnp, attnq - attnp))
        {
            ckparts.httpOnly = true;
        }
        else
            ; /* ignore */
    } while (';' == c);
    if (0 != ckparts.name)
    {
        if (0 == ckparts.domain)
        {
            /* set the host-only flag and the domain to uriparts->host */
            ckparts.hostOnly = true;
            ckparts.domain = uriparts->host;
            ckparts.domainEnd = uriparts->host ? uriparts->host + strlen(uriparts->host) : 0;
        }
        else
        {
            /* any leading dots were already stripped */
            const char *tld = 0;
            size_t dotcount = 0;
            bool nondigit = false;
            for (const char *p = ckparts.domain; ckparts.domainEnd > p; p++)
                if ('.' == *p)
                {
                    dotcount++;
                    tld = p + 1;
                }
                else if (':' == *p)
                    goto retry; /* possible IPv6 address */
                else if (!isdigit(*p))
                    nondigit = true;
            if (1 > dotcount || !nondigit/* possible IPv4 address */)
                goto retry;
            size_t tldlen = ckparts.domainEnd - tld;
            size_t i;
            for (i = 0; NELEMS(tlds) > i; i++)
                if (strmem_iequals(tlds[i], tld, tldlen))
                    break;
            if (NELEMS(tlds) == i)
            {
                if (2 > dotcount)
                    goto retry; /* not one of the special TLDs: required dotcount >= 2 */
            }
            if (0 != uriparts->host)
            {
                size_t cdomlen = ckparts.domainEnd - ckparts.domain;
                size_t udomlen = strlen(uriparts->host);
                if (cdomlen == udomlen)
                {
                    if (0 != memicmp(uriparts->host, ckparts.domain, cdomlen)) /* case-insensitive */
                        goto retry;
                }
                else if (cdomlen < udomlen)
                {
                    if (0 != memicmp(uriparts->host + udomlen - cdomlen, ckparts.domain, cdomlen) ||
                        '.' != uriparts->host[udomlen - cdomlen - 1]) /* case-insensitive */
                        goto retry;
                }
                else
                    goto retry;
            }
        }
        if (0 == ckparts.path || '/' != ckparts.path[0])
        {
            /* set to the default path */
            if (0 == uriparts->path || '/' != uriparts->path[0])
            {
                ckparts.path = "/";
                ckparts.pathEnd = ckparts.path + 1;
            }
            else
            {
                ckparts.path = uriparts->path;
                const char *p = ckparts.path;
                const char *q = p + strlen(p);
                q--;
                while (p <= q && '/' != *q)
                    q--;
                q++;
                ckparts.pathEnd = q;
            }
        }
    }
    if (0 != endp)
        *endp = p;
    *outparts = ckparts;
}

const char *cookie_cstr_concat_req(const char *spec, const char *name, const char *value)
{
    if (0 < Base::cstr_length(spec))
        spec = Base::cstr_concat(spec, "; ", 2);
    spec = Base::cstr_concat(spec, name);
    spec = Base::cstr_concat(spec, "=", 1);
    spec = Base::cstr_concat(spec, value);
    return spec;
}
const char *cookie_cstr_concat_rsp(const char *spec, const char *name, const char *value,
    double expires, const char *domain, const char *path,
    bool secure, bool httpOnly)
{
    if (0 < Base::cstr_length(spec))
        spec = Base::cstr_concat(spec, ", ", 2);
    spec = Base::cstr_concat(spec, name);
    spec = Base::cstr_concat(spec, "=", 1);
    spec = Base::cstr_concat(spec, value);
    if (!Base::isNone(expires))
    {
        Base::DateTime::Components c = Base::DateTime::componentsFromJulianDate(expires);
        char buf[128];
        size_t len=portable_snprintf(buf, sizeof buf, "; Expires=%s, %02d %s %04d %02d:%02d:%02d GMT",
            wkdays[c.dayOfWeek], c.day, months[c.month - 1], c.year, c.hour, c.min, c.sec);
        spec = Base::cstr_concat(spec, buf, len);
    }
    if (0 != domain)
    {
        spec = Base::cstr_concat(spec, "; Domain=", 9);
        spec = Base::cstr_concat(spec, domain);
    }
    if (0 != path)
    {
        spec = Base::cstr_concat(spec, "; Path=", 7);
        spec = Base::cstr_concat(spec, path);
    }
    if (secure)
        spec = Base::cstr_concat(spec, "; Secure", 8);
    if (httpOnly)
        spec = Base::cstr_concat(spec, "; HttpOnly", 10);
    return spec;
}

}
}
