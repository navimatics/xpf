/*
 * NetKit/impl/WebConnectionCFNetwork.cpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#include <NetKit/impl/WebConnection.hpp>
#if defined(BASE_CONFIG_DARWIN)
#if defined(BASE_CONFIG_DARWIN_MAC)
#include <CoreServices/CoreServices.h>
#else
#include <CFNetwork/CFNetwork.h>
#endif

namespace NetKit {
namespace impl {

/*
 * WebResponseStream
 */
#define IOBUFSIZ                        8192
#define MAXHDRSIZ                       8192
#define MAXREDIRECTS                    5
static CFStringRef PrivateRunLoopMode = CFSTR("NetKit::impl::WebResponseStream");
class WebResponseStream : public Base::Stream
{
public:
    static Base::Stream *create(WebConnection *connection,
        const Base::cstr_uri_parts *uriparts, bool async);
    WebResponseStream(WebConnection *connection, bool async);
    ~WebResponseStream();
    ssize_t read(void *buf, size_t size);
    ssize_t close();
private:
    bool initRequest();
    void finiRequest();
    bool open(const Base::cstr_uri_parts *uriparts);
    int readResponseHeader();
    bool onEvent(int event);
    void setLastError(int code, const char *message);
    bool isAsync();
    void enableRequestWStreamCallback();
    void disableRequestWStreamCallback();
    void enableResponseStreamCallback();
    void disableResponseStreamCallback();
    static void setRequestHeaders(CFHTTPMessageRef cfrequest, Base::Map *headers);
    static void requestWStreamCallback(CFWriteStreamRef cfstream, CFStreamEventType event, void *data);
    static void responseStreamCallback(CFReadStreamRef cfstream, CFStreamEventType event, void *data);
private:
    WebConnection *_connection; /* weak */
    CFStringRef _runLoopMode; /* weak */
    int _redirectCount;
    CFDataRef _cfrequestData;
    CFWriteStreamRef _cfrequestWStream;
    CFReadStreamRef _cfrequestRStream;
    CFReadStreamRef _cfresponseStream;
    bool _responseHeadersRead;
};
inline bool WebResponseStream::isAsync()
{
    return PrivateRunLoopMode != _runLoopMode;
}
inline void WebResponseStream::enableRequestWStreamCallback()
{
    CFStreamClientContext clientContext = { 0, this };
    CFWriteStreamSetClient(_cfrequestWStream,
        kCFStreamEventOpenCompleted | kCFStreamEventCanAcceptBytes | kCFStreamEventErrorOccurred,
        requestWStreamCallback, &clientContext);
    CFWriteStreamScheduleWithRunLoop(_cfrequestWStream, CFRunLoopGetCurrent(), _runLoopMode);
}
inline void WebResponseStream::disableRequestWStreamCallback()
{
    CFWriteStreamUnscheduleFromRunLoop(_cfrequestWStream, CFRunLoopGetCurrent(), _runLoopMode);
    CFWriteStreamSetClient(_cfrequestWStream, kCFStreamEventNone, NULL, NULL);
}
inline void WebResponseStream::enableResponseStreamCallback()
{
    CFStreamClientContext clientContext = { 0, this };
    CFReadStreamSetClient(_cfresponseStream,
        kCFStreamEventHasBytesAvailable | kCFStreamEventEndEncountered | kCFStreamEventErrorOccurred,
        responseStreamCallback, &clientContext);
    CFReadStreamScheduleWithRunLoop(_cfresponseStream, CFRunLoopGetCurrent(), _runLoopMode);
}
inline void WebResponseStream::disableResponseStreamCallback()
{
    CFReadStreamUnscheduleFromRunLoop(_cfresponseStream, CFRunLoopGetCurrent(), _runLoopMode);
    CFReadStreamSetClient(_cfresponseStream, kCFStreamEventNone, NULL, NULL);
}
Base::Stream *WebResponseStream::create(WebConnection *connection,
    const Base::cstr_uri_parts *uriparts, bool async)
{
    WebResponseStream *self = new WebResponseStream(connection, async);
    if (!self->initRequest())
        goto error;
    if (!self->open(uriparts))
    {
        self->finiRequest();
        goto error;
    }
    if (WebResponseStream *previousResponseStream = (WebResponseStream *)connection->__state())
    {
        /*
         * The ''self'' stream is open, we can now safely close the previous response stream (if any).
         * See comments in WebConnection::closeResponseStream() for explanation.
         */
        previousResponseStream->close();
        previousResponseStream->release();
        connection->__setState(0);
    }
    if (!async)
    {
    retry:
        CFRunLoopRunInMode(PrivateRunLoopMode, 1e+20, false);
        if (kCFStreamStatusError == CFReadStreamGetStatus(self->_cfresponseStream))
        {
            self->setLastError(WebConnection::ErrorResponse, "cannot open response stream");
            goto error;
        }
        switch (self->readResponseHeader())
        {
        case -1:
            goto error;
        case 0:
            break;
        case +1:
            /* redirect */
            goto retry;
        default:
            assert(false);
            goto error;
        }
    }
    return self;
error:
    self->release();
    return 0;
}
WebResponseStream::WebResponseStream(WebConnection *connection, bool async)
{
    threadsafe();
    _connection = connection;
    _runLoopMode = async ? kCFRunLoopCommonModes : PrivateRunLoopMode;
}
WebResponseStream::~WebResponseStream()
{
    if (NULL != _cfresponseStream)
        close();
}
ssize_t WebResponseStream::read(void *buf, size_t size)
{
    if (NULL == _cfresponseStream)
    {
        setLastError(WebConnection::ErrorMisuse, "stream has been closed");
        return -1;
    }
    if (isAsync() && !CFReadStreamHasBytesAvailable(_cfresponseStream))
        Base::Error::setLastErrorFromCErrno(EAGAIN);
    CFIndex bytes = CFReadStreamRead(_cfresponseStream, (UInt8 *)buf, size);
    if (-1 == bytes)
    {
        setLastError(WebConnection::ErrorResponse, "cannot read response stream");
        return -1;
    }
    return bytes;
}
ssize_t WebResponseStream::close()
{
    if (NULL == _cfresponseStream)
    {
        setLastError(WebConnection::ErrorMisuse, "stream has been closed");
        return -1;
    }
    CFReadStreamClose(_cfresponseStream);
    CFRelease(_cfresponseStream);
    _cfresponseStream = NULL;
    finiRequest();
    return 0;
}
bool WebResponseStream::initRequest()
{
    WebRequest *request = _connection->request();
    if (Base::Stream *stream = request->bodyStream())
        if (Base::MemoryStream *memoryStream = dynamic_cast<Base::MemoryStream *>(stream))
        {
            _cfrequestData = CFDataCreateWithBytesNoCopy(NULL,
                (const UInt8 *)memoryStream->buffer(), memoryStream->size(), kCFAllocatorNull);
            if (NULL == _cfrequestData)
                Base::MemoryAllocationError();
        }
        else if (request->redirectsAllowed())
        {
            _cfrequestData = CFDataCreateMutable(NULL, 0);
            if (NULL == _cfrequestData)
                Base::MemoryAllocationError();
            char buf[IOBUFSIZ];
            ssize_t bytes;
            while (0 < (bytes = stream->read(buf, sizeof buf)))
                CFDataAppendBytes((CFMutableDataRef)_cfrequestData, (UInt8 *)buf, bytes);
            if (-1 == bytes)
            {
                setLastError(WebConnection::ErrorRequest, "cannot read request stream");
                CFRelease(_cfrequestData);
                _cfrequestData = NULL;
                return false;
            }
        }
        else
        {
            /* See http://lists.apple.com/archives/macnetworkprog/2012/Feb/msg00008.html */
            CFStreamCreateBoundPair(NULL, &_cfrequestRStream, &_cfrequestWStream, IOBUFSIZ);
            enableRequestWStreamCallback();
            if (!CFWriteStreamOpen(_cfrequestWStream))
            {
                setLastError(WebConnection::ErrorRequest, "cannot read request stream");
                CFRelease(_cfrequestWStream);
                _cfrequestWStream = NULL;
                CFRelease(_cfrequestRStream);
                _cfrequestRStream = NULL;
                return false;
            }
        }
    return true;
}
void WebResponseStream::finiRequest()
{
    if (NULL != _cfrequestWStream)
    {
        CFWriteStreamClose(_cfrequestWStream);
        CFRelease(_cfrequestWStream);
        _cfrequestWStream = NULL;
    }
    if (NULL != _cfrequestRStream)
    {
        CFReadStreamClose(_cfrequestRStream);
        CFRelease(_cfrequestRStream);
        _cfrequestRStream = NULL;
    }
    if (NULL != _cfrequestData)
    {
        CFRelease(_cfrequestData);
        _cfrequestData = NULL;
    }
}
bool WebResponseStream::open(const Base::cstr_uri_parts *uriparts)
{
    WebRequest *request = _connection->request();
    const char *method = request->method();
    if (0 == method)
        method = "GET";
    if (CFStringRef cfmethod = CFStringCreateWithCString(NULL, method, kCFStringEncodingUTF8))
    {
        const char *uri = Base::cstr_uri_composeraw(uriparts);
        if (CFURLRef cfurl = CFURLCreateAbsoluteURLWithBytes(NULL,
            (const UInt8 *)uri, Base::cstr_length(uri), kCFStringEncodingUTF8, NULL, false))
        {
            if (CFHTTPMessageRef cfrequest = CFHTTPMessageCreateRequest(NULL,
                cfmethod, cfurl, kCFHTTPVersion1_1))
            {
                setRequestHeaders(cfrequest, request->headers());
                Base::Map *extraRequestHeaders =
                    __WebConnectionCreateExtraRequestHeaders(_connection, uriparts);
                setRequestHeaders(cfrequest, extraRequestHeaders);
                if (0 != extraRequestHeaders)
                    extraRequestHeaders->release();
                if (NULL != _cfrequestRStream)
                    _cfresponseStream = CFReadStreamCreateForStreamedHTTPRequest(NULL,
                        cfrequest, _cfrequestRStream);
                else
                {
                    if (NULL != _cfrequestData)
                        CFHTTPMessageSetBody(cfrequest, _cfrequestData);
                    _cfresponseStream = CFReadStreamCreateForHTTPRequest(NULL, cfrequest);
                }
                if (NULL != _cfresponseStream)
                {
                    CFReadStreamSetProperty(_cfresponseStream,
                        kCFStreamPropertyHTTPShouldAutoredirect, kCFBooleanFalse);
                    CFDictionaryRef proxies = CFNetworkCopySystemProxySettings();
                    CFReadStreamSetProperty(_cfresponseStream, kCFStreamPropertyHTTPProxy, proxies);
                    CFRelease(proxies);
                    CFReadStreamSetProperty(_cfresponseStream,
                        kCFStreamPropertyHTTPAttemptPersistentConnection, kCFBooleanTrue);
                    if (Base::Value *value = dynamic_cast<Base::Value *>(
                        Base::Environment::standardEnvironment()->object(
                            Base::cstr_obj(WebConnection::PropertyCellularData()))))
                        if (!value->booleanValue())
                        {
                            CFReadStreamSetProperty(_cfresponseStream,
                                kCFStreamPropertyNoCellular, kCFBooleanTrue);
                        }
                    /*
                     * The following "tags" the response stream, thus allowing to maintain
                     * multiple connections to the same server.
                     *
                     * See http://lists.apple.com/archives/macnetworkprog/2009/Jul/msg00037.html
                     * See http://lists.apple.com/archives/macnetworkprog/2008/Dec/msg00001.html
                     */
                    long long connPtr = (intptr_t)_connection;
                    CFNumberRef connNum = CFNumberCreate(NULL, kCFNumberLongLongType, &connPtr);
                    CFReadStreamSetProperty(_cfresponseStream, CFSTR("NetKit.Connection"), connNum);
                    CFRelease(connNum);
                    enableResponseStreamCallback();
                }
                CFRelease(cfrequest);
            }
            CFRelease(cfurl);
        }
        CFRelease(cfmethod);
    }
    if (NULL == _cfresponseStream || !CFReadStreamOpen(_cfresponseStream))
    {
        setLastError(WebConnection::ErrorResponse, "cannot open response stream");
        if (NULL != _cfresponseStream)
        {
            CFRelease(_cfresponseStream);
            _cfresponseStream = NULL;
        }
        return false;
    }
    return true;
}
int WebResponseStream::readResponseHeader()
{
    CFHTTPMessageRef cfresponse = (CFHTTPMessageRef)CFReadStreamCopyProperty(_cfresponseStream,
        kCFStreamPropertyHTTPResponseHeader);
    if (NULL == cfresponse)
    {
        setLastError(WebConnection::ErrorResponse, "cannot read response header");
        return -1;
    }
    if (!CFHTTPMessageIsHeaderComplete(cfresponse))
    {
        setLastError(WebConnection::ErrorResponse, "response header is not complete");
        CFRelease(cfresponse);
        return -1;
    }
    int statusCode = CFHTTPMessageGetResponseStatusCode(cfresponse);
    WebResponse *response = _connection->response();
    response->setStatusCode(statusCode);
    if (CFStringRef cfdesc = CFHTTPMessageCopyResponseStatusLine(cfresponse))
    {
        char desc[MAXHDRSIZ];
        if (CFStringGetCString(cfdesc, desc, sizeof desc, kCFStringEncodingUTF8))
            response->setStatusLine(desc);
        CFRelease(cfdesc);
    }
    if (CFDictionaryRef cfheaders = CFHTTPMessageCopyAllHeaderFields(cfresponse))
    {
        CFIndex count = CFDictionaryGetCount(cfheaders);
        const void **keys = (const void **)(128 < count ?
            Base::Malloc(2 * count * sizeof(void *)) : alloca(2 * count * sizeof(void *)));
        const void **vals = keys + count;
        CFDictionaryGetKeysAndValues(cfheaders, keys, vals);
        for (CFIndex i = 0; count > i; i++)
        {
            char key[MAXHDRSIZ];
            char val[MAXHDRSIZ];
            if (CFStringGetCString((CFStringRef)keys[i], key, sizeof key, kCFStringEncodingUTF8) &&
                CFStringGetCString((CFStringRef)vals[i], val, sizeof val, kCFStringEncodingUTF8))
            {
                response->setHeader(key, val, true);
            }
        }
        128 < count ? free(keys) : (void)0;
        CFRelease(cfheaders);
    }
    CFRelease(cfresponse);
    if (CFURLRef cfurl = (CFURLRef)CFReadStreamCopyProperty(_cfresponseStream,
        kCFStreamPropertyHTTPFinalURL))
    {
        if (CFStringRef cfuri = CFURLGetString(cfurl))
        {
            char uri[MAXHDRSIZ];
            if (CFStringGetCString(cfuri, uri, sizeof uri, kCFStringEncodingUTF8))
                response->setUri(uri);
        }
        CFRelease(cfurl);
    }
    if (WebSession *session = _connection->session())
        if (const char *uri = response->uri())
            if (const char *spec = response->header("Set-Cookie"))
            {
                Base::cstr_uri_parts uriparts;
                Base::cstr_uri_parseraw(uri, &uriparts);
                session->addCookieSpec(spec, Base::DateTime::currentJulianDate(), &uriparts);
            }
    if (300 <= statusCode && statusCode < 400 && _connection->request()->redirectsAllowed())
    {
        if (MAXREDIRECTS == _redirectCount++)
        {
            setLastError(WebConnection::ErrorResponse, "too many redirections");
            return -1;
        }
        if (const char *uri = response->header("Location"))
        {
            /* close the response stream */
            CFReadStreamClose(_cfresponseStream);
            CFRelease(_cfresponseStream);
            _cfresponseStream = NULL;
            /* reopen with new uri */
            if (const char *p = strchr(uri, ','))
                uri = Base::cstr_slice(uri, p - uri);
            if (const char *responseUri = response->uri())
                uri = Base::cstr_uri_concat(responseUri, uri);
            Base::cstr_uri_parts uriparts;
            Base::cstr_uri_parseraw(uri, &uriparts);
            if (0 == uriparts.scheme ||
                (0 != strcmp("http", uriparts.scheme) && 0 != strcmp("https", uriparts.scheme)))
            {
                setLastError(WebConnection::ErrorResponse, "redirection to unsupported uri scheme");
                return -1;
            }
            response->_resetHeader();
            if (open(&uriparts))
                return +1;
            return -1;
        }
        else
        {
            setLastError(WebConnection::ErrorResponse, "redirection without Location header");
            return -1;
        }
    }
    finiRequest();
    _responseHeadersRead = true;
    return 0;
}
bool WebResponseStream::onEvent(int event)
{
    retain();
    _connection->onEvent(event);
    bool res = NULL != _cfresponseStream;
        /* check whether stream was closed during event notification */
    release();
    return res;
}
void WebResponseStream::setLastError(int code, const char *message)
{
    CFErrorRef cferror = WebConnection::ErrorResponse && NULL != _cfresponseStream ?
        CFReadStreamCopyError(_cfresponseStream) : NULL;
    if (NULL == cferror)
        Base::Error::setLastError(_connection->_errorDomain(), code, message);
    else
    {
        CFStringRef cfdesc = CFErrorCopyDescription(cferror);
        char desc[1024];
        if (CFStringGetCString(cfdesc, desc, sizeof desc, kCFStringEncodingUTF8))
        {
            const char *fullmsg = Base::cstr_newf("%s: %s", message, desc);
            Base::Error::setLastError(_connection->_errorDomain(), code, fullmsg);
            Base::cstr_release(fullmsg);
        }
        else
            Base::Error::setLastError(_connection->_errorDomain(), code, message);
        CFRelease(cfdesc);
        CFRelease(cferror);
    }
}
void WebResponseStream::setRequestHeaders(CFHTTPMessageRef cfrequest, Base::Map *headers)
{
    foreach (Base::CString *key, headers)
    {
        if (CFStringRef cfkey = CFStringCreateWithCString(NULL,
            key->chars(), kCFStringEncodingUTF8))
        {
            if (CFStringRef cfval = CFStringCreateWithCString(NULL,
                ((Base::CString *)headers->object(key))->chars(), kCFStringEncodingUTF8))
            {
                CFHTTPMessageSetHeaderFieldValue(cfrequest, cfkey, cfval);
                CFRelease(cfval);
            }
            CFRelease(cfkey);
        }
    }
}
void WebResponseStream::requestWStreamCallback(CFWriteStreamRef cfstream,
    CFStreamEventType event, void *data)
{
    WebResponseStream *self = (WebResponseStream *)data;
    assert(self->_cfrequestWStream == cfstream);
    switch (event)
    {
    case kCFStreamEventOpenCompleted:
    case kCFStreamEventCanAcceptBytes:
        if (Base::Stream *stream = self->_connection->request()->bodyStream())
            if (CFWriteStreamCanAcceptBytes(self->_cfrequestWStream))
            {
                char buf[IOBUFSIZ];
                ssize_t bytes;
                if (0 < (bytes = stream->read(buf, sizeof buf)))
                    bytes = CFWriteStreamWrite(self->_cfrequestWStream, (UInt8 *)buf, bytes);
                if (-1 == bytes)
                {
                    self->setLastError(WebConnection::ErrorRequest, "cannot read request stream");
                    self->disableRequestWStreamCallback();
                    CFWriteStreamClose(self->_cfrequestWStream);
                    CFReadStreamClose(self->_cfrequestRStream); /* signal error */
                }
                else if (0 == bytes)
                {
                    self->disableRequestWStreamCallback();
                    CFWriteStreamClose(self->_cfrequestWStream); /* signal eof */
                }
            }
        break;
    case kCFStreamEventErrorOccurred:
        self->setLastError(WebConnection::ErrorRequest, "cannot read request stream");
        self->disableRequestWStreamCallback();
        CFWriteStreamClose(self->_cfrequestWStream);
        CFReadStreamClose(self->_cfrequestRStream); /* signal error */
        break;
    }
}
void WebResponseStream::responseStreamCallback(CFReadStreamRef cfstream,
    CFStreamEventType event, void *data)
{
    WebResponseStream *self = (WebResponseStream *)data;
    assert(self->_cfresponseStream == cfstream);
    if (!self->isAsync())
    {
        self->disableResponseStreamCallback();
        return;
    }
    switch (event)
    {
    case kCFStreamEventHasBytesAvailable:
    case kCFStreamEventEndEncountered:
        if (!self->_responseHeadersRead)
        {
            switch (self->readResponseHeader())
            {
            case -1:
                self->disableResponseStreamCallback();
                self->onEvent(WebConnection::EventFailure);
                goto outerBreak;
            case 0:
                if (!self->onEvent(WebConnection::EventReceiveHeader))
                    goto outerBreak;
                break;
            case +1:
                /* redirect */
                goto outerBreak;
            default:
                assert(false);
                goto outerBreak;
            }
        }
        self->onEvent(kCFStreamEventHasBytesAvailable == event ?
            WebConnection::EventReceiveData : WebConnection::EventSuccess);
        break;
    case kCFStreamEventErrorOccurred:
        self->setLastError(WebConnection::ErrorResponse, "response stream error");
        self->disableResponseStreamCallback();
        self->onEvent(WebConnection::EventFailure);
        break;
    }
outerBreak:;
}

/*
 * WebConnection
 */
#define _previousResponseStream         (*((Base::Stream **)&_state))
const char *WebConnection::defaultUserAgentComment()
{
    static const char *comment = Base::CStringConstant("(CFNetwork)")->chars();
    return comment;
}
bool WebConnection::openConnection(const Base::cstr_uri_parts *uriparts, bool async)
{
    if (0 == uriparts->scheme ||
        (0 != strcmp("http", uriparts->scheme) && 0 != strcmp("https", uriparts->scheme)))
    {
        Base::Error::setLastError(_errorDomain(), ErrorMisuse, "unsupported uri scheme");
        return false;
    }
    return true;
}
void WebConnection::closeConnection()
{
    if (0 != _previousResponseStream)
    {
        _previousResponseStream->close();
        _previousResponseStream->release();
        _previousResponseStream = 0;
    }
}
Base::Stream *WebConnection::createResponseStream(const Base::cstr_uri_parts *uriparts, bool async)
{
    return WebResponseStream::create(this, uriparts, async);
}
void WebConnection::closeResponseStream()
{
    /*
     * We do not actually close the response stream here, but in closeConnection()
     * or openResponseStream() instead. The reason is that CFNetwork closes a connection
     * when the last read stream on it has been closed. Since we want to support persistent
     * connections, we only close the response stream either when a new response stream
     * has been successfully opened [openResponseStream()] or when the connection itself
     * is closed [closeConnection()].
     *
     * See http://lists.apple.com/archives/macnetworkprog/2010/Jan/msg00008.html
     * See http://lists.apple.com/archives/macnetworkprog/2008/Dec/msg00001.html
     */
    if (0 == _previousResponseStream)
    {
        _previousResponseStream = _response->bodyStream();
        if (0 != _previousResponseStream)
            _previousResponseStream->retain();
    }
    _response->setBodyStream(0);
}

}
}
#endif
