/*
 * NetKit/impl/WebConnectionWinHttp.cpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#include <NetKit/impl/WebConnection.hpp>
#if defined(BASE_CONFIG_WINDOWS)
#include <winhttp.h>

namespace NetKit {
namespace impl {

struct _WebConnectionHandle
{
    HINTERNET hconnection;
    const char *server;
};
static inline WCHAR *trimLWS(WCHAR *s)
{
    while (L' ' == *s || L'\t' == *s)
        s++;
    WCHAR *r = s;
    while (*s)
        s++;
    if (r < s)
    {
        s--;
        while (r <= s && (L' ' == *s || L'\t' == *s))
            s--;
        s[1] = L'\0';
    }
    return r;
}

/*
 * WebResponseStream
 */
#define WriteData(self, buf, siz)\
    (WinHttpWriteData(self->_hrequest, buf, siz, NULL) ||\
        (self->setLastError(WebConnection::ErrorRequest, "cannot write request"), 0))
#define ReadData(self, buf, siz)\
    (WinHttpReadData(self->_hrequest, buf, siz, NULL) ||\
        (self->setLastError(WebConnection::ErrorResponse, "cannot read response"), 0))
#define ReceiveResponse(self)\
    (WinHttpReceiveResponse(self->_hrequest, NULL) ||\
        (self->setLastError(WebConnection::ErrorResponse, "cannot receive response"), 0))
#define CheckIO(self)\
    (0 == self->_ioerr || (Base::Error::setLastError(self->_ioerr), 0))
#define SIGNAL_RSPHEAD                  0x0001
#define SIGNAL_RSPDATA                  0x0002
#define SIGNAL_SUCCESS                  0x0004
#define SIGNAL_FAILURE                  0x0008
#define SIGNAL_PENDING                  0x8000
#define IOBUFSIZ                        8192
#define MAXHDRSIZ                       8192
class WebResponseStream : public Base::Stream
{
public:
    static Base::Stream *create(WebConnection *connection,
        const Base::cstr_uri_parts *uriparts, bool async);
    WebResponseStream(WebConnection *connection, bool async);
    ~WebResponseStream();
    ssize_t read(void *buf, size_t size);
    ssize_t close();
private:
    void finiRequest();
    bool open(const Base::cstr_uri_parts *uriparts);
    bool readResponseHeader();
    bool onEvent(int event);
    void handleRedirectNoLock(WCHAR *wuri);
    void signalEventNoLock();
    void signalErrorNoLock();
    void setLastError(int code, const char *message);
    static void setRequestHeaders(HINTERNET hrequest, Base::Map *headers);
    static void signalEventOnCurrentThread(Base::Object *obj);
    static void CALLBACK statusCallback(HINTERNET h,
        DWORD_PTR dwContext, DWORD code, void *info, DWORD length);
private:
    struct Context;
    WebConnection *_connection;
    HANDLE _ioevt;
    Base::Thread *_iothr;
    Context *_context;
    char _iobuf[IOBUFSIZ];
    /* everything below is protected by _context->lock */
    size_t _ioidx, _iosiz; bool _rdeof;
    Base::Error *_ioerr;
    unsigned _signalFlags;
    Base::MemoryStream *_requestStream;
    const char *_requestCookieSpec;
    HINTERNET _hrequest;
};
struct WebResponseStream::Context : Base::Object
{
    CRITICAL_SECTION lock;
    WebResponseStream *self;
    Context()
    {
        threadsafe();
        InitializeCriticalSection(&lock);
    }
    ~Context()
    {
        DeleteCriticalSection(&lock);
    }
};
Base::Stream *WebResponseStream::create(WebConnection *connection,
    const Base::cstr_uri_parts *uriparts, bool async)
{
    WebResponseStream *self = new WebResponseStream(connection, async);
    if (!self->open(uriparts))
        goto error;
    if (!async)
    {
        /* wait for response headers available */
        WaitForSingleObject(self->_ioevt, INFINITE);
        EnterCriticalSection(&self->_context->lock);
        self->finiRequest();
        bool ok = CheckIO(self) && self->readResponseHeader();
        LeaveCriticalSection(&self->_context->lock);
        if (!ok)
            goto error;
    }
    return self;
error:
    self->release();
    return 0;
}
WebResponseStream::WebResponseStream(WebConnection *connection, bool async)
{
    threadsafe();
    _connection = connection;
    if (!async)
    {
        _ioevt = CreateEvent(NULL, FALSE/* auto-reset */, FALSE/* non-signaled */, NULL);
        if (NULL == _ioevt)
            Base::MemoryAllocationError();
    }
    else
        _iothr = Base::Thread::currentThread();
    _context = new Context;
}
WebResponseStream::~WebResponseStream()
{
    if (NULL != _hrequest)
        close();
    _context->release();
    if (NULL != _ioevt)
        CloseHandle(_ioevt);
}
ssize_t WebResponseStream::read(void *buf, size_t size)
{
    /* !!!: Because read() uses _iobuf, it is an error to issue overlapping read() calls. */
    ssize_t result = -1;
    EnterCriticalSection(&_context->lock);
    if (NULL == _hrequest)
    {
        setLastError(WebConnection::ErrorMisuse, "stream has been closed");
        goto unlock_and_return;
    }
    if (NULL != _ioevt)
    {
        /* !async */
        while (_ioidx == _iosiz)
        {
            if (_rdeof) /* EOF */
            {
                result = 0;
                goto unlock_and_return;
            }
            LeaveCriticalSection(&_context->lock);
            if (!ReadData(this, _iobuf, IOBUFSIZ))
                return result;
            WaitForSingleObject(_ioevt, INFINITE);
            EnterCriticalSection(&_context->lock);
            if (!CheckIO(this))
                goto unlock_and_return;
        }
        size_t remain = _iosiz - _ioidx;
        if (size > remain)
            size = remain;
        memcpy(buf, _iobuf + _ioidx, size);
        _ioidx += size;
        result = size;
        goto unlock_and_return;
    }
    else
    {
        /* async */
        if (_ioidx == _iosiz)
        {
            if (_rdeof)
                result = 0;
            else
                Base::Error::setLastErrorFromCErrno(EAGAIN);
            goto unlock_and_return;
        }
        size_t remain = _iosiz - _ioidx;
        if (size > remain)
            size = remain;
        memcpy(buf, _iobuf + _ioidx, size);
        _ioidx += size;
        result = size;
        if (size == remain)
        {
            LeaveCriticalSection(&_context->lock);
            if (!ReadData(this, _iobuf, IOBUFSIZ))
                result = -1;
            return result;
        }
        goto unlock_and_return;
    }
unlock_and_return:
    LeaveCriticalSection(&_context->lock);
    return result;
}
ssize_t WebResponseStream::close()
{
    ssize_t result = -1;
    EnterCriticalSection(&_context->lock);
    if (NULL == _hrequest)
    {
        setLastError(WebConnection::ErrorMisuse, "stream has been closed");
        goto unlock_and_return;
    }
    _context->self = 0;
    WinHttpCloseHandle(_hrequest);
    _hrequest = NULL;
    finiRequest();
    if (0 != _ioerr)
    {
        _ioerr->release();
        _ioerr = 0;
    }
    result = 0;
unlock_and_return:
    LeaveCriticalSection(&_context->lock);
    return result;
}
void WebResponseStream::finiRequest()
{
    if (0 != _requestStream)
    {
        _requestStream->release();
        _requestStream = 0;
    }
    if (0 != _requestCookieSpec)
    {
        Base::cstr_release(_requestCookieSpec);
        _requestCookieSpec = 0;
    }
}
bool WebResponseStream::open(const Base::cstr_uri_parts *uriparts)
{
    WebRequest *request = _connection->request();
    bool redirectsAllowed = request->redirectsAllowed();
    Base::MemoryStream *requestStream = 0;
    const void *requestBuffer = NULL;
    DWORD requestLength = 0;
    DWORD contentLength = 0;
    _requestCookieSpec = request->header("Cookie");
    _requestCookieSpec = 0 != _requestCookieSpec ?
        Base::cstr_new(_requestCookieSpec, Base::cstr_length(_requestCookieSpec)) : 0;
    if (Base::Stream *stream = request->bodyStream())
        if (Base::MemoryStream *memoryStream = dynamic_cast<Base::MemoryStream *>(stream))
        {
            requestStream = memoryStream;
            requestStream->retain();
            requestBuffer = requestStream->buffer();
            requestLength = contentLength = requestStream->size();
        }
        else if (redirectsAllowed || 6 > LOBYTE(LOWORD(GetVersion())))
        {
            /*
             * Windows versions before Windows Vista or Windows Server 2008 do not support
             * WINHTTP_IGNORE_REQUEST_TOTAL_LENGTH and chunked transfer encoding
             */
            requestStream = new Base::MemoryStream;
            ssize_t bytes;
            while (0 < (bytes = stream->read(_iobuf, IOBUFSIZ)))
                requestStream->write(_iobuf, bytes);
            if (-1 == bytes)
            {
                setLastError(WebConnection::ErrorRequest, "cannot read request stream");
                requestStream->release();
                return false;
            }
            requestBuffer = requestStream->buffer();
            requestLength = contentLength = requestStream->size();
        }
        else
            contentLength = WINHTTP_IGNORE_REQUEST_TOTAL_LENGTH;
    WCHAR wmethod[128] = L"GET";
    WCHAR wpath[MAXHDRSIZ] = L"/";
    if (const char *method = request->method())
        if (0 == MultiByteToWideChar(CP_UTF8, 0, method, Base::cstr_length(method) + 1, wmethod, NELEMS(wmethod)))
            goto error;
    {
        Base::cstr_uri_parts parts = { 0 };
        parts.path = uriparts->path && uriparts->path[0] ? uriparts->path : "/";
        parts.query = uriparts->query;
        const char *path = Base::cstr_uri_composeraw(&parts);
        if (0 == MultiByteToWideChar(CP_UTF8, 0, path, Base::cstr_length(path) + 1, wpath, NELEMS(wpath)))
            goto error;
    }
    HINTERNET hconnection = ((_WebConnectionHandle *)_connection->__state())->hconnection;
    if (HINTERNET hrequest = WinHttpOpenRequest(
        hconnection,
        wmethod, wpath, NULL, /* HTTP/1.1 */
        WINHTTP_NO_REFERER,
        WINHTTP_DEFAULT_ACCEPT_TYPES,
        0 == strcmp("https", uriparts->scheme)
            ? WINHTTP_FLAG_ESCAPE_DISABLE | WINHTTP_FLAG_SECURE
            : WINHTTP_FLAG_ESCAPE_DISABLE))
    {
        WinHttpSetStatusCallback(
            hrequest,
            statusCallback,
            0 |
                WINHTTP_CALLBACK_FLAG_REDIRECT |
                WINHTTP_CALLBACK_FLAG_SENDREQUEST_COMPLETE |
                WINHTTP_CALLBACK_FLAG_WRITE_COMPLETE |
                WINHTTP_CALLBACK_FLAG_HEADERS_AVAILABLE |
                WINHTTP_CALLBACK_FLAG_READ_COMPLETE |
                WINHTTP_CALLBACK_FLAG_REQUEST_ERROR |
                WINHTTP_CALLBACK_FLAG_HANDLES,
            0 /* reserved */);
        DWORD disableFeature = WINHTTP_DISABLE_COOKIES;
        if (!redirectsAllowed)
            disableFeature |= WINHTTP_DISABLE_REDIRECTS;
        WinHttpSetOption(hrequest,
            WINHTTP_OPTION_DISABLE_FEATURE, &disableFeature, sizeof disableFeature);
        setRequestHeaders(hrequest, request->headers());
        Base::Map *extraRequestHeaders =
            __WebConnectionCreateExtraRequestHeaders(_connection, uriparts);
        setRequestHeaders(hrequest, extraRequestHeaders);
        if (0 != extraRequestHeaders)
            extraRequestHeaders->release();
        EnterCriticalSection(&_context->lock);
        _hrequest = hrequest;
        _requestStream = requestStream;
        _context->retain();
        _context->self = this;
        BOOL ok = WinHttpSendRequest(
            hrequest,
            WINHTTP_NO_ADDITIONAL_HEADERS, 0,
            (LPVOID)requestBuffer, requestLength, contentLength,
            (DWORD_PTR)_context);
        if (!ok)
        {
            _context->self = 0;
            WinHttpCloseHandle(_hrequest);
            _hrequest = NULL;
            _requestStream = NULL;
        }
        LeaveCriticalSection(&_context->lock);
        if (!ok)
            goto error;
        return true;
    }
error:
    setLastError(WebConnection::ErrorResponse, "cannot open response stream");
    if (0 != requestStream)
        requestStream->release();
    return false;
}
bool WebResponseStream::readResponseHeader()
{
    WebResponse *response = _connection->response();
    WCHAR *headerBuffer = 0;
    DWORD headerLength;
    WinHttpQueryHeaders(_hrequest, WINHTTP_QUERY_RAW_HEADERS,
        WINHTTP_HEADER_NAME_BY_INDEX, NULL, &headerLength, WINHTTP_NO_HEADER_INDEX);
    if (ERROR_INSUFFICIENT_BUFFER == GetLastError())
    {
        assert(0 == headerLength % 2);
        headerBuffer = (WCHAR *)Base::Malloc(headerLength);
        if (!WinHttpQueryHeaders(_hrequest, WINHTTP_QUERY_RAW_HEADERS,
            WINHTTP_HEADER_NAME_BY_INDEX, headerBuffer, &headerLength, WINHTTP_NO_HEADER_INDEX))
            goto error;
        if (headerBuffer[0])
        {
            WCHAR *hdr = headerBuffer, *nexthdr;
            /* parse status line */
            {
                nexthdr = hdr + wcslen(hdr) + 1;
                char hdrstr[MAXHDRSIZ];
                if (WideCharToMultiByte(CP_UTF8, 0, hdr, -1, hdrstr, sizeof(hdrstr), NULL, NULL))
                    response->setStatusLine(hdrstr);
                while (*hdr && L' ' != *hdr)
                    hdr++;
                while (L' ' == *hdr)
                    hdr++;
                response->setStatusCode(wcstoul(hdr, 0, 10));
            }
            /* parse headers */
            for (hdr = nexthdr; *hdr; hdr = nexthdr)
            {
                nexthdr = hdr + wcslen(hdr) + 1;
                WCHAR *val = wcschr(hdr, ':');
                if (0 == val)
                    continue; /* invalid header */
                *val++ = L'\0';
                hdr = trimLWS(hdr);
                val = trimLWS(val);
                char hdrstr[MAXHDRSIZ];
                char valstr[MAXHDRSIZ];
                if (WideCharToMultiByte(CP_UTF8, 0, hdr, -1, hdrstr, sizeof(hdrstr), NULL, NULL) &&
                    WideCharToMultiByte(CP_UTF8, 0, val, -1, valstr, sizeof(valstr), NULL, NULL))
                {
                    response->setHeader(hdrstr, valstr, true);
                }
            }
        }
        free(headerBuffer);
        {
            WCHAR urlBuffer[MAXHDRSIZ];
            DWORD urlLength = sizeof(urlBuffer);
            if (WinHttpQueryOption(_hrequest, WINHTTP_OPTION_URL, urlBuffer, &urlLength))
            {
                char urlstr[MAXHDRSIZ];
                if (WideCharToMultiByte(CP_UTF8, 0, urlBuffer, -1, urlstr, sizeof(urlstr), NULL, NULL))
                    response->setUri(urlstr);
            }
        }
        if (WebSession *session = _connection->session())
            if (const char *uri = response->uri())
                if (const char *spec = response->header("Set-Cookie"))
                {
                    Base::cstr_uri_parts uriparts;
                    Base::cstr_uri_parseraw(uri, &uriparts);
                    session->addCookieSpec(spec, Base::DateTime::currentJulianDate(), &uriparts);
                }
        return true;
    }
error:
    setLastError(WebConnection::ErrorResponse, "cannot read response header");
    free(headerBuffer);
    return false;
}
bool WebResponseStream::onEvent(int event)
{
    retain();
    _connection->onEvent(event);
    bool res = NULL != _hrequest;
        /* check whether stream was closed during event notification */
    release();
    return res;
}
void WebResponseStream::handleRedirectNoLock(WCHAR *wuri)
{
    /*
     * It is thread safe to access _connection and _connection->session();
     * they are both read-only for the lifetime of this WebResponseStream.
     */
    if (WebSession *session = _connection->session())
        mark_and_collect
        {
            double now = Base::DateTime::currentJulianDate();
            Base::cstr_uri_parts uriparts;
            char buf[MAXHDRSIZ];
            if (0 == WideCharToMultiByte(CP_UTF8, 0, wuri, -1, buf, sizeof(buf), NULL, NULL))
                return;
            Base::cstr_uri_parseraw(buf, &uriparts);
            if (0 == uriparts.scheme ||
                (0 != strcmp("http", uriparts.scheme) && 0 != strcmp("https", uriparts.scheme)))
            {
                //setLastError(WebConnection::ErrorResponse, "redirection to unsupported uri scheme");
                return; /* WinHttp only knows about HTTP(S), so this will get ignored anyway */
            }
            WCHAR wspec[MAXHDRSIZ];
            DWORD wspeclen = sizeof wspec;
            for (DWORD i = 0; WinHttpQueryHeaders(_hrequest,
                WINHTTP_QUERY_SET_COOKIE, WINHTTP_HEADER_NAME_BY_INDEX, wspec, &wspeclen, &i); wspeclen = sizeof wspec)
            {
                if (0 == WideCharToMultiByte(CP_UTF8, 0, wspec, -1, buf, sizeof(buf), NULL, NULL))
                    continue;
                session->addCookieSpec(buf, now, &uriparts);
            }
            Base::Map *extraRequestHeaders = 0;
            if (const char *sessionCookieSpec = session->cookieSpec(now, &uriparts))
            {
                if (0 != _requestCookieSpec)
                    __WebConnectionSetRequestHeader(extraRequestHeaders, "Cookie", _requestCookieSpec);
                __WebConnectionSetRequestHeader(extraRequestHeaders, "Cookie", sessionCookieSpec, true);
            }
            setRequestHeaders(_hrequest, extraRequestHeaders);
            if (0 != extraRequestHeaders)
                extraRequestHeaders->release();
        }
}
void WebResponseStream::signalEventNoLock()
{
    if (NULL != _ioevt)
        SetEvent(_ioevt);
    else if (!(_signalFlags & SIGNAL_PENDING))
    {
        Base::ThreadLoopInvoke(_iothr, signalEventOnCurrentThread, this);
        _signalFlags |= SIGNAL_PENDING;
    }
}
void WebResponseStream::signalErrorNoLock()
{
    if (0 == _ioerr)
        mark_and_collect
        {
            if (Base::Error *error = Base::Error::lastError())
                _ioerr = new Base::Error(error->domain(), error->code(), error->message());
            else
                _ioerr = new Base::Error(_connection->_errorDomain(),
                    WebConnection::ErrorResponse, "internal error");
            _ioerr->threadsafe();
        }
    signalEventNoLock();
}
void WebResponseStream::setLastError(int code, const char *message)
{
    Base::Error::setLastError(_connection->_errorDomain(), code, message);
}
void WebResponseStream::setRequestHeaders(HINTERNET hrequest, Base::Map *headers)
{
    foreach (Base::CString *key, headers)
    {
        Base::CString *val = (Base::CString *)headers->object(key);
        WCHAR wheader[MAXHDRSIZ];
        if (int count0 = MultiByteToWideChar(CP_UTF8, 0, key->chars(), key->length(),
            wheader, NELEMS(wheader)))
        {
            wheader[count0++] = L':';
            wheader[count0++] = L' ';
            if (int count1 = MultiByteToWideChar(CP_UTF8, 0, val->chars(), val->length(),
                wheader + count0, NELEMS(wheader) - count0))
            {
                WinHttpAddRequestHeaders(
                    hrequest,
                    wheader, count0 + count1,
                    WINHTTP_ADDREQ_FLAG_ADD | WINHTTP_ADDREQ_FLAG_REPLACE);
            }
        }
    }
}
void WebResponseStream::signalEventOnCurrentThread(Base::Object *obj)
{
    WebResponseStream *self = (WebResponseStream *)obj;
    EnterCriticalSection(&self->_context->lock);
    int event = Base::None();
    if (NULL == self->_hrequest)
        ;
    else if (self->_signalFlags & (SIGNAL_SUCCESS | SIGNAL_FAILURE))
        ;
    else if (!CheckIO(self))
    {
        event = WebConnection::EventFailure;
        self->_signalFlags |= SIGNAL_FAILURE;
    }
    else if (!(self->_signalFlags & SIGNAL_RSPHEAD))
    {
        if (self->readResponseHeader())
            event = WebConnection::EventReceiveHeader;
        else
            event = WebConnection::EventFailure;
        self->_signalFlags |= SIGNAL_RSPHEAD;
    }
    else if (self->_ioidx < self->_iosiz)
    {
        event = WebConnection::EventReceiveData;
        self->_signalFlags |= SIGNAL_RSPDATA;
    }
    else if (self->_rdeof)
    {
        event = WebConnection::EventSuccess;
        self->_signalFlags |= SIGNAL_SUCCESS;
    }
    self->_signalFlags &= ~SIGNAL_PENDING;
    if (Base::isNone(event))
    {
        LeaveCriticalSection(&self->_context->lock);
        return;
    }
    self->finiRequest();
    LeaveCriticalSection(&self->_context->lock);
    if (!self->onEvent(event))
        return;
    if (WebConnection::EventReceiveData == event)
    {
        EnterCriticalSection(&self->_context->lock);
        if (self->_ioidx < self->_iosiz)
            self->signalEventNoLock();
        LeaveCriticalSection(&self->_context->lock);
    }
    else if (WebConnection::EventReceiveHeader == event)
    {
        if (!ReadData(self, self->_iobuf, IOBUFSIZ))
        {
            EnterCriticalSection(&self->_context->lock);
            self->signalErrorNoLock();
            LeaveCriticalSection(&self->_context->lock);
        }
    }
}
void CALLBACK WebResponseStream::statusCallback(HINTERNET h,
    DWORD_PTR dwContext, DWORD code, void *info, DWORD length)
{
    WebResponseStream::Context *context = (WebResponseStream::Context *)dwContext;
    if (WINHTTP_CALLBACK_STATUS_HANDLE_CLOSING == code)
    {
        /* guaranteed to be last callback; release context */
        context->release();
        return;
    }
    else if (WINHTTP_CALLBACK_STATUS_HANDLE_CREATED == code)
        return;
    EnterCriticalSection(&context->lock);
    WebResponseStream *self = context->self;
    if (0 == self)
        goto unlock_and_return;
    switch (code)
    {
    case WINHTTP_CALLBACK_STATUS_REDIRECT:
        self->handleRedirectNoLock((LPWSTR)info);
        break;
    case WINHTTP_CALLBACK_STATUS_SENDREQUEST_COMPLETE:
        if (0 != self->_requestStream)
        {
            if (!ReceiveResponse(self))
                goto signal_error;
            break;
        }
        /* fall through */
    case WINHTTP_CALLBACK_STATUS_WRITE_COMPLETE:
        if (Base::Stream *stream = self->_connection->request()->bodyStream())
        {
            length = info ? *(DWORD *)info : 0;
            self->_ioidx += length;
            if (self->_ioidx == self->_iosiz)
            {
                self->_ioidx = 0;
                self->_iosiz = 0;
                ssize_t bytes = stream->read(self->_iobuf, IOBUFSIZ);
                if (-1 == bytes)
                {
                    self->setLastError(WebConnection::ErrorRequest, "cannot read request stream");
                    goto signal_error;
                }
                self->_iosiz = bytes;
            }
            if (0 < self->_iosiz)
            {
                if (!WriteData(self, self->_iobuf + self->_ioidx, self->_iosiz - self->_ioidx))
                    goto signal_error;
            }
            else
            {
                if (!ReceiveResponse(self))
                    goto signal_error;
            }
        }
        else
        {
            if (!ReceiveResponse(self))
                goto signal_error;
        }
        break;
    case WINHTTP_CALLBACK_STATUS_HEADERS_AVAILABLE:
        self->signalEventNoLock();
        break;
    case WINHTTP_CALLBACK_STATUS_READ_COMPLETE:
        self->_ioidx = 0;
        self->_iosiz = length;
        self->_rdeof = 0 == length;
        self->signalEventNoLock();
        break;
    case WINHTTP_CALLBACK_STATUS_REQUEST_ERROR:
        self->setLastError(WebConnection::ErrorResponse, "response stream error");
        goto signal_error;
    }
unlock_and_return:
    LeaveCriticalSection(&context->lock);
    return;
signal_error:
    self->signalErrorNoLock();
    LeaveCriticalSection(&context->lock);
}

/*
 * WebConnection
 */
#define MAXHOSTSIZ                      256
#define _connectionHandle               (*((_WebConnectionHandle **)&_state))
static inline HINTERNET GetWinHttpSession()
{
    static HINTERNET hsession;
    execute_once
    {
        hsession = WinHttpOpen(
            0, /* no user agent */
            WINHTTP_ACCESS_TYPE_DEFAULT_PROXY,
            WINHTTP_NO_PROXY_NAME,
            WINHTTP_NO_PROXY_BYPASS,
            WINHTTP_FLAG_ASYNC);
        DWORD dwValue;
        dwValue = 1;
        WinHttpSetOption(hsession,
            WINHTTP_OPTION_CONNECT_RETRIES, &dwValue, sizeof dwValue);
        dwValue = 5;
        WinHttpSetOption(hsession,
            WINHTTP_OPTION_MAX_HTTP_AUTOMATIC_REDIRECTS, &dwValue, sizeof dwValue);
        dwValue = WINHTTP_OPTION_REDIRECT_POLICY_ALWAYS;
        WinHttpSetOption(hsession,
            WINHTTP_OPTION_REDIRECT_POLICY, &dwValue, sizeof dwValue);
    }
    return hsession;
}
const char *WebConnection::defaultUserAgentComment()
{
    static const char *comment = Base::CStringConstant("(WinHttp)")->chars();
    return comment;
}
bool WebConnection::openConnection(const Base::cstr_uri_parts *uriparts, bool async)
{
    if (0 == uriparts->scheme ||
        (0 != strcmp("http", uriparts->scheme) && 0 != strcmp("https", uriparts->scheme)))
    {
        Base::Error::setLastError(_errorDomain(), ErrorMisuse, "unsupported uri scheme");
        return false;
    }
    Base::cstr_uri_parts parts = { 0 };
    parts.scheme = uriparts->scheme;
    parts.host = uriparts->host;
    parts.port = uriparts->port;
    const char *server = Base::cstr_uri_composeraw(&parts);
    if (0 == _connectionHandle || 0 != strcmp(server, _connectionHandle->server))
    {
        HINTERNET hconnection = NULL;
        WCHAR whost[MAXHOSTSIZ];
        if (0 != MultiByteToWideChar(CP_UTF8, 0, uriparts->host, Base::cstr_length(uriparts->host) + 1, whost, NELEMS(whost)))
            hconnection = WinHttpConnect(GetWinHttpSession(),
                whost, uriparts->port ? strtoul(uriparts->port, 0, 0) : INTERNET_DEFAULT_PORT,
                0 /* reserved */);
        if (NULL == hconnection)
        {
            Base::Error::setLastError(_errorDomain(), WebConnection::ErrorConnection, "cannot open connection");
            return false;
        }
        closeConnection();
        _connectionHandle = (_WebConnectionHandle *)Base::Malloc(sizeof(_WebConnectionHandle));
        _connectionHandle->hconnection = hconnection;
        _connectionHandle->server = server;
        Base::cstr_retain(server);
    }
    return true;
}
void WebConnection::closeConnection()
{
    if (_connectionHandle)
    {
        WinHttpCloseHandle(_connectionHandle->hconnection);
        Base::cstr_release(_connectionHandle->server);
        free(_connectionHandle);
        _connectionHandle = 0;
    }
}
Base::Stream *WebConnection::createResponseStream(const Base::cstr_uri_parts *uriparts, bool async)
{
    return WebResponseStream::create(this, uriparts, async);
}
void WebConnection::closeResponseStream()
{
    Base::Stream *stream = _response->bodyStream();
    if (0 != stream)
    {
        stream->close();
        _response->setBodyStream(0);
    }
}

}
}
#endif
