/*
 * NetKit/impl/WebRequest.cpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#include <NetKit/impl/WebRequest.hpp>
#include <NetKit/impl/Cookies.hpp>

namespace NetKit {
namespace impl {

static inline bool strmem_iequals(const char *s, const void *p, size_t size)
{
    if (strlen(s) != size)
        return false;
    return 0 == memicmp(s, p, size);
}

WebRequest::WebRequest()
{
}
WebRequest::~WebRequest()
{
    if (0 != _bodyStream)
        _bodyStream->release();
    if (0 != _headers)
        _headers->release();
    Base::cstr_release(_method);
    Base::cstr_release(_uri);
}
void WebRequest::reset()
{
    if (0 != _bodyStream)
        _bodyStream->release();
    if (0 != _headers)
        _headers->release();
    Base::cstr_release(_method);
    Base::cstr_release(_uri);
    _uri = 0;
    _method = 0;
    _headers = 0;
    _bodyStream = 0;
    _redirectsNotAllowed = false;
}
void WebRequest::setUri(const char *value)
{
    if (0 != value)
        value = Base::cstr_new(value);
    Base::cstr_release(_uri);
    _uri = value;
}
void WebRequest::setMethod(const char *value)
{
    if (0 != value)
        value = Base::cstr_new(value);
    Base::cstr_release(_method);
    _method = value;
}
const char *WebRequest::header(const char *name)
{
    if (0 == _headers)
        return 0;
    if (Base::CString *str = (Base::CString *)_headers->object(BASE_ALLOCA_OBJECT(Base::TransientCString, name)))
        return str->chars();
    else
        return 0;
}
void WebRequest::setHeader(const char *name, const char *value, bool append)
{
    if (0 == _headers)
        _headers = new Base::Map(Base::CString::caseInsensitiveComparer());
    if (0 != value)
    {
        Base::CString *keyobj = Base::CString::create(name);
        Base::CString *valobj;
        if (append && 0 != (valobj = (Base::CString *)_headers->object(keyobj)))
        {
            valobj->retain();
            if (0 == stricmp("Cookie", name))
                /* special case Cookie header */
                valobj = valobj->_concat("; ", 2);
            else
                valobj = valobj->_concat(", ", 2);
            valobj = valobj->_concat(value);
        }
        else
            valobj = Base::CString::create(value);
        _headers->setObject(keyobj, valobj);
        valobj->release();
        keyobj->release();
    }
    else
        _headers->removeObject(BASE_ALLOCA_OBJECT(Base::TransientCString, name));
}
const char *WebRequest::cookie(const char *name)
{
    if (0 == _headers)
        return 0;
    static Base::CString *keyobj = Base::CStringConstant("Cookie");
    if (Base::CString *str = (Base::CString *)_headers->object(keyobj))
    {
        for (const char *p = str->chars(), *endp; *p; p = endp)
        {
            cookie_parts ckparts;
            cookie_parse_req(p, &endp, &ckparts);
            if (0 == ckparts.name)
                break;
            if (strmem_iequals(name, ckparts.name, ckparts.nameEnd - ckparts.name))
                return Base::cstring(ckparts.value, ckparts.valueEnd - ckparts.value);
        }
    }
    return 0;
}
void WebRequest::addCookie(const char *name, const char *value)
{
    if (0 == _headers)
        _headers = new Base::Map(Base::CString::caseInsensitiveComparer());
    static Base::CString *keyobj = Base::CStringConstant("Cookie");
    Base::CString *valobj;
    if (0 != (valobj = (Base::CString *)_headers->object(keyobj)))
    {
        valobj->retain();
        valobj = Base::cstr_obj(cookie_cstr_concat_req(valobj->chars(), name, value));
    }
    else
        valobj = Base::cstr_obj(cookie_cstr_concat_req(0, name, value));
    _headers->setObject(keyobj, valobj);
    valobj->release();
}
void WebRequest::setBodyStream(Base::Stream *value)
{
    if (0 != value)
        value->retain();
    if (0 != _bodyStream)
        _bodyStream->release();
    _bodyStream = value;
}
void WebRequest::setRedirectsAllowed(bool value)
{
    _redirectsNotAllowed = !value;
}

}
}
