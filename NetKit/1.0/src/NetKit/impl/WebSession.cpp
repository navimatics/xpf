/*
 * NetKit/impl/WebSession.cpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#include <NetKit/impl/WebSession.hpp>

namespace NetKit {
namespace impl {

WebSession::WebSession()
{
    threadsafe();
    _cookieJar = new CookieJar;
}
WebSession::~WebSession()
{
    _cookieJar->release();
    Base::cstr_release(_userAgent);
}
Base::Lock *WebSession::lock()
{
    return _cookieJar->lock();
}
const char *WebSession::userAgent()
{
    synchronized (_cookieJar->lock())
    {
        if (0 != _userAgent)
            Base::cstr_obj(_userAgent)->autocollect();
        return _userAgent;
    }
}
void WebSession::setUserAgent(const char *value)
{
    synchronized (_cookieJar->lock())
    {
        if (0 != value)
            value = Base::cstr_new(value);
        Base::cstr_release(_userAgent);
        _userAgent = value;
    }
}
Cookie **WebSession::cookies(double now, const Base::cstr_uri_parts *uriparts)
{
    //synchronized (_cookieJar->lock())
    return _cookieJar->cookies(now, uriparts);
}
void WebSession::addCookie(Cookie *cookie, bool remove)
{
    //synchronized (_cookieJar->lock())
    _cookieJar->addCookie(cookie, remove);
}
const char *WebSession::cookieSpec(double now, const Base::cstr_uri_parts *uriparts)
{
    //synchronized (_cookieJar->lock())
    return _cookieJar->cookieSpec(now, uriparts);
}
void WebSession::addCookieSpec(const char *spec, double now, const Base::cstr_uri_parts *uriparts)
{
    //synchronized (_cookieJar->lock())
    _cookieJar->addCookieSpec(spec, now, uriparts);
}

}
}
