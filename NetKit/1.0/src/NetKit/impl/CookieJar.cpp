/*
 * NetKit/impl/CookieJar.cpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#include <NetKit/impl/CookieJar.hpp>

namespace NetKit {
namespace impl {

#define MAX_DOMAIN_COOKIES              50

static unsigned cookieOrd = 0;
    /*
     * We use ''cookieOrd'' to track the creation order of Cookie objects.
     * This is important because of RFC 6265 Cookie header ordering requirements [see cmp() below].
     *
     * Currently there is no provision to persist CookieJar's and the contained Cookie's.
     * If CookieJar persistence is implemented we may or may not want to persist a Cookie's
     * ''cookieOrd''.
     */
Cookie::Cookie(const cookie_parts *ckparts)
{
    threadsafe();
    _cookieOrd = Base::atomic_inc(cookieOrd);
    _name = Base::cstr_new(ckparts->name, ckparts->nameEnd - ckparts->name);
    _value = Base::cstr_new(ckparts->value, ckparts->valueEnd - ckparts->value);
    _expires = ckparts->expires;
    _hostOnly = ckparts->hostOnly;
    _domain = Base::cstr_new(ckparts->domain, ckparts->domainEnd - ckparts->domain);
    _path = Base::cstr_new(ckparts->path, ckparts->pathEnd - ckparts->path);
    _secure = ckparts->secure;
    _httpOnly = ckparts->httpOnly;
}
Cookie::Cookie(const char *name, const char *value, double expires,
    bool hostOnly, const char *domain, const char *path,
    bool secure, bool httpOnly)
{
    _cookieOrd = Base::atomic_inc(cookieOrd);
    _name = Base::cstr_new(name);
    _value = Base::cstr_new(value);
    _expires = expires;
    _hostOnly = hostOnly;
    _domain = Base::cstr_new(domain);
    _path = Base::cstr_new(path);
    _secure = secure;
    _httpOnly = httpOnly;
}
Cookie::~Cookie()
{
    Base::cstr_release(_name);
    Base::cstr_release(_value);
    Base::cstr_release(_domain);
    Base::cstr_release(_path);
}
int Cookie::cmp(Object *obj0)
{
    if (Cookie *obj = dynamic_cast<Cookie *>(obj0))
    {
        /*
         * RFC 6265 states in "5.4. The Cookie Header":
         *
         * 2.  The user agent SHOULD sort the cookie-list in the following
         *     order:
         *
         *     *  Cookies with longer paths are listed before cookies with
         *        shorter paths.
         *
         *     *  Among cookies that have equal-length path fields, cookies with
         *        earlier creation-times are listed before cookies with later
         *        creation-times.
         */
        int llen = Base::cstr_length(_path), rlen = Base::cstr_length(obj->_path);
        if (int r = rlen - llen)
            return r;
        return _cookieOrd - obj->_cookieOrd;
    }
    return (Object *)this - obj0;
}

CookieJar::CookieJar()
{
    threadsafe();
    _lock = new Base::Lock;
    _cookies = new Base::Map(Base::CString::caseInsensitiveComparer());
    _cookies->autocollectObjects(false);
}
CookieJar::~CookieJar()
{
    _cookies->release();
    _lock->release();
}
Base::Lock *CookieJar::lock()
{
    return _lock;
}
Cookie **CookieJar::cookies(double now, const Base::cstr_uri_parts *uriparts)
{
    synchronized (_lock)
    {
        Cookie **cookies = (Cookie **)Base::carr_new(0, 0, sizeof(Cookie *));
        bool host_equ_domain = true;
        const char *path = 0 == uriparts->path || '/' != uriparts->path[0] ? "/" : uriparts->path;
        size_t pathlen = strlen(path);
        bool https = 0 != uriparts->scheme && 0 == strcmp("https", uriparts->scheme);
        for (const char *domain = uriparts->host; *domain;)
        {
            Base::CacheMap *domainCookies = (Base::CacheMap *)_cookies->object(BASE_ALLOCA_OBJECT(Base::TransientCString, domain));
            foreach (Base::CString *name, domainCookies)
            {
                Base::Array *nameCookies = (Base::Array *)domainCookies->object(name);
                foreach (Cookie *cookie, nameCookies)
                {
                    assert(0 == strcmp(cookie->domain(), domain));
                    assert(0 == strcmp(cookie->name(), name->chars()));
                    if (!Base::isNone(cookie->expires()) && cookie->expires() < now)
                        continue;
                    if (cookie->hostOnly() && !host_equ_domain)
                        continue;
                    const char *cpath = cookie->path();
                    size_t cpathlen = Base::cstr_length(cpath);
                    assert(0 < cpathlen);
                    if (cpathlen == pathlen)
                    {
                        if (0 != memcmp(path, cpath, cpathlen)) /* case-sensitive */
                            continue;
                    }
                    else if (cpathlen < pathlen)
                    {
                        if (0 != memcmp(path, cpath, cpathlen) ||
                            ('/' != cpath[cpathlen - 1] && '/' != path[cpathlen])) /* case-sensitive */
                            continue;
                    }
                    else
                        continue;
                    if (cookie->secure() && !https)
                        continue;
                    cookies = (Cookie **)Base::carr_concat(cookies, &cookie, 1, sizeof(Cookie *));
                    cookie->autocollect();
                }
            }
            while ('\0' != *domain && '.' != *domain)
                domain++;
            if ('.' == *domain)
                domain++;
            host_equ_domain = false;
        }
        qsort(cookies, Base::carr_length(cookies), sizeof(Cookie *), Base::Object::qsort_cmp);
        Base::carr_obj(cookies)->autorelease();
        return cookies;
    }
}
void CookieJar::addCookie(Cookie *cookie, bool remove)
{
    synchronized (_lock)
    {
        Base::CacheMap *domainCookies;
        Base::Array *nameCookies;
        domainCookies = (Base::CacheMap *)_cookies->object(Base::cstr_obj(cookie->domain()));
        if (0 == domainCookies)
        {
            if (remove)
                return;
            domainCookies = new Base::CacheMap(Base::CString::caseInsensitiveComparer());
            domainCookies->autocollectObjects(false);
            domainCookies->setMaxCount(MAX_DOMAIN_COOKIES);
            nameCookies = new Base::Array;
            nameCookies->autocollectObjects(false);
            nameCookies->addObject(cookie);
            domainCookies->setObject(Base::cstr_obj(cookie->name()), nameCookies);
            _cookies->setObject(Base::cstr_obj(cookie->domain()), domainCookies);
            nameCookies->release();
            domainCookies->release();
        }
        else
        {
            nameCookies = (Base::Array *)domainCookies->object(Base::cstr_obj(cookie->name()));
            if (0 == nameCookies)
            {
                if (remove)
                    return;
                nameCookies = new Base::Array;
                nameCookies->autocollectObjects(false);
                nameCookies->addObject(cookie);
                domainCookies->setObject(Base::cstr_obj(cookie->name()), nameCookies);
                nameCookies->release();
            }
            else
            {
                size_t i, n;
                for (i = 0, n = nameCookies->count(); n > i; i++)
                {
                    Cookie *storedCookie = (Cookie *)nameCookies->object(i);
                    if (0 == strcmp(storedCookie->path(), cookie->path()))
                    {
                        if (remove)
                            nameCookies->removeObject(i);
                        else
                            nameCookies->replaceObject(i, cookie);
                        break;
                    }
                }
                if (n == i)
                    nameCookies->addObject(cookie);
            }
        }
    }
}
const char *CookieJar::cookieSpec(double now, const Base::cstr_uri_parts *uriparts)
{
    synchronized (_lock)
    {
        const char *spec = 0;
        foreach (Cookie *cookie, (Base::Object **)cookies(now, uriparts))
            spec = cookie_cstr_concat_req(spec, cookie->name(), cookie->value());
        if (0 != spec)
            Base::cstr_obj(spec)->autorelease();
        return spec;
    }
}
void CookieJar::addCookieSpec(const char *spec, double now, const Base::cstr_uri_parts *uriparts)
{
    synchronized (_lock)
    {
        for (const char *p = spec, *endp; *p; p = endp)
        {
            cookie_parts ckparts;
            cookie_parse_rsp(p, now, uriparts, &endp, &ckparts);
            if (0 == ckparts.name)
                break;
            Cookie *cookie = new Cookie(&ckparts);
            addCookie(cookie, !Base::isNone(cookie->expires()) && cookie->expires() < now);
            cookie->release();
        }
    }
}
void CookieJar::evictCookies(double now)
{
    synchronized (_lock)
    {
        /* not implemented */
    }
}

}
}
