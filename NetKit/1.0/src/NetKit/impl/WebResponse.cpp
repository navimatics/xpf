/*
 * NetKit/impl/WebResponse.cpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#include <NetKit/impl/WebResponse.hpp>
#include <NetKit/impl/Cookies.hpp>

namespace NetKit {
namespace impl {

static inline bool strmem_iequals(const char *s, const void *p, size_t size)
{
    if (strlen(s) != size)
        return false;
    return 0 == memicmp(s, p, size);
}

WebResponse::WebResponse()
{
    _statusCode = Base::None();
}
WebResponse::~WebResponse()
{
    if (0 != _bodyStream)
        _bodyStream->release();
    if (0 != _headers)
        _headers->release();
    Base::cstr_release(_statusLine);
    Base::cstr_release(_uri);
    finiCookieQueryData();
}
void WebResponse::reset()
{
    if (0 != _bodyStream)
        _bodyStream->release();
    if (0 != _headers)
        _headers->release();
    Base::cstr_release(_statusLine);
    Base::cstr_release(_uri);
    finiCookieQueryData();
    _uri = 0;
    _statusCode = Base::None();
    _statusLine = 0;
    _headers = 0;
    _bodyStream = 0;
}
void WebResponse::_resetHeader()
{
    if (0 != _headers)
        _headers->release();
    Base::cstr_release(_statusLine);
    Base::cstr_release(_uri);
    finiCookieQueryData();
    _uri = 0;
    _statusCode = Base::None();
    _statusLine = 0;
    _headers = 0;
}
void WebResponse::setUri(const char *value)
{
    finiCookieQueryData();
    if (0 != value)
        value = Base::cstr_new(value);
    Base::cstr_release(_uri);
    _uri = value;
}
void WebResponse::setStatusCode(int value)
{
    _statusCode = value;
}
void WebResponse::setStatusLine(const char *value)
{
    if (0 != value)
        value = Base::cstr_new(value);
    Base::cstr_release(_statusLine);
    _statusLine = value;
}
const char *WebResponse::header(const char *name)
{
    if (0 == _headers)
        return 0;
    if (Base::CString *str = (Base::CString *)_headers->object(BASE_ALLOCA_OBJECT(Base::TransientCString, name)))
        return str->chars();
    else
        return 0;
}
void WebResponse::setHeader(const char *name, const char *value, bool append)
{
    if (0 == _headers)
        _headers = new Base::Map(Base::CString::caseInsensitiveComparer());
    if (0 != value)
    {
        Base::CString *keyobj = Base::CString::create(name);
        Base::CString *valobj;
        if (append && 0 != (valobj = (Base::CString *)_headers->object(keyobj)))
        {
            valobj->retain();
            valobj = valobj->_concat(", ", 2);
            valobj = valobj->_concat(value);
        }
        else
            valobj = Base::CString::create(value);
        _headers->setObject(keyobj, valobj);
        valobj->release();
        keyobj->release();
    }
    else
        _headers->removeObject(BASE_ALLOCA_OBJECT(Base::TransientCString, name));
}
const char *WebResponse::cookie(const char *name)
{
    if (0 == _headers)
        return 0;
    static Base::CString *keyobj = Base::CStringConstant("Set-Cookie");
    if (Base::CString *str = (Base::CString *)_headers->object(keyobj))
    {
        initCookieQueryData();
        if (0 != _ckqUriparts.host)
            for (const char *p = str->chars(), *endp; *p; p = endp)
            {
                cookie_parts ckparts;
                cookie_parse_rsp(p, _ckqTimestamp, &_ckqUriparts, &endp, &ckparts);
                if (0 == ckparts.name)
                    break;
                if (!Base::isNone(ckparts.expires))
                    if (ckparts.expires < _ckqTimestamp)
                        continue;
                if (strmem_iequals(name, ckparts.name, ckparts.nameEnd - ckparts.name))
                    return Base::cstring(ckparts.value, ckparts.valueEnd - ckparts.value);
            }
    }
    return 0;
}
void WebResponse::addCookie(const char *name, const char *value,
    double expires, const char *domain, const char *path,
    bool secure, bool httpOnly)
{
    if (0 == _headers)
        _headers = new Base::Map(Base::CString::caseInsensitiveComparer());
    static Base::CString *keyobj = Base::CStringConstant("Set-Cookie");
    Base::CString *valobj;
    if (0 != (valobj = (Base::CString *)_headers->object(keyobj)))
    {
        valobj->retain();
        valobj = Base::cstr_obj(cookie_cstr_concat_rsp(valobj->chars(), name, value,
            expires, domain, path, secure, httpOnly));
    }
    else
        valobj = Base::cstr_obj(cookie_cstr_concat_rsp(0, name, value,
            expires, domain, path, secure, httpOnly));
    _headers->setObject(keyobj, valobj);
    valobj->release();
}
void WebResponse::setBodyStream(Base::Stream *value)
{
    if (0 != value)
        value->retain();
    if (0 != _bodyStream)
        _bodyStream->release();
    _bodyStream = value;
}
void WebResponse::initCookieQueryData()
{
    if (0 == _uri || _ckqValid)
        return;
    mark_and_collect
    {
        Base::cstr_uri_parseraw(_uri, &_ckqUriparts);
        Base::cstr_retain(_ckqUriparts.scheme);
        Base::cstr_retain(_ckqUriparts.userinfo);
        Base::cstr_retain(_ckqUriparts.host);
        Base::cstr_retain(_ckqUriparts.port);
        Base::cstr_retain(_ckqUriparts.path);
        Base::cstr_retain(_ckqUriparts.query);
        Base::cstr_retain(_ckqUriparts.fragment);
    }
    _ckqTimestamp = Base::DateTime::currentJulianDate();
    _ckqValid = true;
}
void WebResponse::finiCookieQueryData()
{
    if (!_ckqValid)
        return;
    Base::cstr_release(_ckqUriparts.scheme);
    Base::cstr_release(_ckqUriparts.userinfo);
    Base::cstr_release(_ckqUriparts.host);
    Base::cstr_release(_ckqUriparts.port);
    Base::cstr_release(_ckqUriparts.path);
    Base::cstr_release(_ckqUriparts.query);
    Base::cstr_release(_ckqUriparts.fragment);
    _ckqUriparts.scheme = 0;
    _ckqUriparts.userinfo = 0;
    _ckqUriparts.host = 0;
    _ckqUriparts.port = 0;
    _ckqUriparts.path = 0;
    _ckqUriparts.query = 0;
    _ckqUriparts.fragment = 0;
    _ckqValid = false;
}

}
}
