/*
 * NetKit/impl/WebConnection.cpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#include <NetKit/impl/WebConnection.hpp>

namespace NetKit {
namespace impl {

const char *WebConnection::PropertyCellularData()
{
    static const char *s = Base::CStringConstant("NetKit::impl::WebConnection::PropertyCellularData")->chars();
    return s;
}
const char *WebConnection::errorDomain()
{
    static const char *s = Base::CStringConstant("NetKit::impl::WebConnection")->chars();
    return s;
}
const char *WebConnection::defaultUserAgent()
{
    static const char *agent;
    execute_once
    {
        if (const char *comment = defaultUserAgentComment())
            agent = Base::cstr_newf("%s NetKit/1.0 %s", Base::GetApplicationName(), comment);
        else
            agent = Base::cstr_newf("%s NetKit/1.0", Base::GetApplicationName());
    }
    return agent;
}
WebConnection::WebConnection(WebSession *session)
{
    if (0 != (_session = session))
        _session->retain();
    _request = new WebRequest;
    _response = new WebResponse;
}
WebConnection::~WebConnection()
{
    close();
    _response->release();
    _request->release();
    if (0 != _session)
        _session->release();
}
bool WebConnection::sendrecv(bool async)
{
    closeResponseStream();
    _response->reset();
    const char *uri = _request->uri();
    if (0 == uri)
    {
        Base::Error::setLastError(_errorDomain(), ErrorMisuse, "unspecified request uri");
        return false;
    }
    Base::cstr_uri_parts uriparts; Base::cstr_uri_parseraw(uri, &uriparts);
    if (!openConnection(&uriparts, async))
        return false;
    Base::Stream *stream = createResponseStream(&uriparts, async);
    if (0 == stream)
        return false;
    _response->setBodyStream(stream);
    stream->release();
    return true;
}
void WebConnection::close()
{
    closeResponseStream();
    closeConnection();
}
void WebConnection::onEvent(int event)
{
    if (_delegate)
        _delegate(this, event);
}
const char *WebConnection::_errorDomain()
{
    return WebConnection::errorDomain();
}

void __WebConnectionSetRequestHeader(Base::Map *&headers, const char *name, const char *value,
    bool append)
{
    if (0 == headers)
    {
        headers = new Base::Map(Base::CString::caseInsensitiveComparer());
        headers->autocollectObjects(false);
    }
    if (0 != value)
    {
        Base::CString *keyobj = Base::CString::create(name);
        Base::CString *valobj;
        if (append && 0 != (valobj = (Base::CString *)headers->object(keyobj)))
        {
            valobj->retain();
            if (0 == stricmp("Cookie", name))
                /* special case Cookie header */
                valobj = valobj->_concat("; ", 2);
            else
                valobj = valobj->_concat(", ", 2);
            valobj = valobj->_concat(value);
        }
        else
            valobj = Base::CString::create(value);
        headers->setObject(keyobj, valobj);
        valobj->release();
        keyobj->release();
    }
    else
        headers->removeObject(BASE_ALLOCA_OBJECT(Base::TransientCString, name));
}
Base::Map *__WebConnectionCreateExtraRequestHeaders(WebConnection *connection,
    const Base::cstr_uri_parts *uriparts)
{
    Base::Map *extraRequestHeaders = 0;
    WebSession *session = connection->session();
    WebRequest *request = connection->request();
    if (0 == request->header("User-Agent"))
    {
        const char *userAgent = session ? session->userAgent() : 0;
        __WebConnectionSetRequestHeader(extraRequestHeaders,
            "User-Agent", userAgent ? userAgent : WebConnection::defaultUserAgent());
    }
    if (0 != session)
        if (const char *sessionCookieSpec = session->cookieSpec(Base::DateTime::currentJulianDate(), uriparts))
        {
            if (const char *requestCookieSpec = request->header("Cookie"))
                __WebConnectionSetRequestHeader(extraRequestHeaders, "Cookie", requestCookieSpec);
            __WebConnectionSetRequestHeader(extraRequestHeaders, "Cookie", sessionCookieSpec, true);
        }
    return extraRequestHeaders;
}

}
}
