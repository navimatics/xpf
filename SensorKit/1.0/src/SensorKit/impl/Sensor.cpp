/*
 * SensorKit/impl/Sensor.cpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#include <SensorKit/impl/Sensor.hpp>

namespace SensorKit {
namespace impl {

typedef Base::GenericDelegateMap<Base::Object, Sensor *(Base::Map *)> CreatorMap;

static Base::Lock *GetCreatorMapAndLock(CreatorMap *&mapref)
{
    static CreatorMap *map;
    static Base::Lock *lock;
    execute_once
    {
        map = new CreatorMap;
        lock = new Base::Lock;
    }
    mapref = map;
    return lock;
}

void Sensor::registerCreator(const char *name,
    Base::Delegate<Sensor *(Base::Map *)> create)
{
    CreatorMap *map;
    synchronized (GetCreatorMapAndLock(map))
    {
        Base::CString *nameobj = Base::CString::create(name);
        map->setDelegate(nameobj, create);
        nameobj->release();
    }
}
Sensor *Sensor::create(const char *name, Base::Map *params)
{
    Base::Delegate<Sensor *(Base::Map *)> create;
    CreatorMap *map;
    synchronized (GetCreatorMapAndLock(map))
        create = map->delegate(BASE_ALLOCA_OBJECT(Base::TransientCString, name));
    return create ? create(params) : 0;
}
Sensor::Sensor()
{
    threadsafe();
}
Sensor::kind_t Sensor::kind()
{
    return KindUnknown;
}
SensorHandle *Sensor::createHandle()
{
    return 0;
}
const char *Sensor::description()
{
    return className();
}

const char *SensorHandle::errorDomain()
{
    static const char *s = Base::CStringConstant("SensorKit::impl::SensorHandle")->chars();
    return s;
}
SensorHandle::SensorHandle(Sensor *sensor)
{
    threadsafe();
    sensor->retain();
    _sensor = sensor;
}
SensorHandle::~SensorHandle()
{
    close(Sensor::KindUnknown);
    _sensor->release();
}
Sensor::kind_t SensorHandle::openKind()
{
    return _openKind;
}
int SensorHandle::open(Sensor::kind_t kind)
{
    Sensor::kind_t defaultKind = sensor()->kind();
    if (Sensor::KindUnknown == kind)
        kind = defaultKind;
    else
        kind &= defaultKind;
    if (0 == kind)
        return -1;
    for (Sensor::kind_t mask = kind, bit = 1; mask; mask &= ~bit, bit <<= 1)
        if ((mask & bit) && !(_openKind & bit))
        {
            if (-1 == _open(bit))
            {
                close();
                return -1;
            }
            _openKind |= bit;
        }
    return 0;
}
int SensorHandle::close(Sensor::kind_t kind)
{
    if (Sensor::KindUnknown == kind)
        kind = _openKind;
    else
        kind &= _openKind;
    if (0 == kind)
        return -1;
    for (Sensor::kind_t mask = kind, bit = 1; mask; mask &= ~bit, bit <<= 1)
        if ((mask & bit) && (_openKind & bit))
        {
            _openKind &= ~bit;
            _close(bit);
        }
    return 0;
}
int SensorHandle::_open(Sensor::kind_t kind)
{
    return -1;
}
int SensorHandle::_close(Sensor::kind_t kind)
{
    return -1;
}
Base::Object *SensorHandle::parameter(const char *name)
{
    return 0;
}
void SensorHandle::setParameter(const char *name, Base::Object *value)
{
}

}
}
