/*
 * SensorKit/impl/SensorRegistration.cpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#include <SensorKit/impl/Sensor.hpp>
#include <SensorKit/impl/sensors/CoreLocation/CLSensor.hpp>
#include <SensorKit/impl/sensors/dr/DrSensor.hpp>
#include <SensorKit/impl/sensors/gpsais/GpsAisSensor.hpp>

namespace SensorKit {
namespace impl {

void Sensor::registerAllCreators()
{
    execute_once
    {
#if defined(SENSORKIT_CONFIG_CORELOCATION)
        Sensor::registerCreator("CoreLocation", make_delegate(sensors::CoreLocation::CLSensor::create));
#endif
#if defined(SENSORKIT_CONFIG_NMEA)
        Sensor::registerCreator("GPS-NMEA", make_delegate(sensors::gpsais::GpsAisSensor::createNmea));
        Sensor::registerCreator("AIS-NMEA", make_delegate(sensors::gpsais::GpsAisSensor::createNmea));
        Sensor::registerCreator("GPSAIS-NMEA", make_delegate(sensors::gpsais::GpsAisSensor::createNmea));
#endif
#if defined(SENSORKIT_CONFIG_GPSD)
        Sensor::registerCreator("GPS-GPSD", make_delegate(sensors::gpsais::GpsAisSensor::createGpsd));
        Sensor::registerCreator("AIS-GPSD", make_delegate(sensors::gpsais::GpsAisSensor::createGpsd));
        Sensor::registerCreator("GPSAIS-GPSD", make_delegate(sensors::gpsais::GpsAisSensor::createGpsd));
#endif
        Sensor::registerCreator("GPS-DR", make_delegate(sensors::dr::DrSensor::create));
    }
}

}
}
