/*
 * SensorKit/impl/SensorManager.cpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#include <SensorKit/impl/SensorManager.hpp>

namespace SensorKit {
namespace impl {

SensorManager *SensorManager::sharedSensorManager()
{
    static SensorManager *manager;
    execute_once
    {
        manager = new SensorManager;
        manager->linger();
    }
    return manager;
}
SensorManager::SensorManager()
{
    threadsafe();
    _lock = new Base::Lock;
    _sensors = new SensorMap;
    _sensors->autocollectObjects(false);
}
SensorManager::~SensorManager()
{
    _sensors->release();
    _lock->release();
}
bool SensorManager::addNewSensor(const char *name, Base::Map *params)
{
    if (Sensor *sensor = Sensor::create(name, params))
    {
        addSensor(sensor);
        sensor->release();
        return true;
    }
    else
        return false;
}
void SensorManager::addSensor(Sensor *sensor)
{
    Sensor::kind_t kind = sensor->kind();
    synchronized (_lock)
    {
        for (Sensor::kind_t mask = kind, bit = 1; mask; mask &= ~bit, bit <<= 1)
            if (mask & bit)
            {
                SensorSet *set = _sensors->object(bit);
                if (0 == set)
                {
                    set = new SensorSet;
                    set->autocollectObjects(false);
                    _sensors->setObject(bit, set);
                    set->release();
                }
                set->addObject(sensor);
            }
    }
}
void SensorManager::removeSensor(Sensor *sensor)
{
    Sensor::kind_t kind = sensor->kind();
    synchronized (_lock)
    {
        for (Sensor::kind_t mask = kind, bit = 1; mask; mask &= ~bit, bit <<= 1)
            if (mask & bit)
            {
                SensorSet *set = _sensors->object(bit);
                if (0 != set)
                    set->removeObject(sensor);
            }
    }
}
void SensorManager::removeAllSensors()
{
    synchronized (_lock)
        _sensors->removeAllObjects();
}
Sensor **SensorManager::sensors(Sensor::kind_t kind)
{
    synchronized (_lock)
    {
        for (Sensor::kind_t mask = kind, bit = 1; mask; mask &= ~bit, bit <<= 1)
            if (mask & bit)
            {
                SensorSet *set = _sensors->object(bit);
                if (0 != set)
                {
                    Sensor **sensors = set->objects();
                    Sensor **q = sensors;
                    for (Sensor **p = sensors, **endp = p + Base::carr_length(p); endp > p; p++)
                    {
                        Sensor *sensor = *p;
                        if ((sensor->kind() & kind) == kind)
                            *q++ = sensor;
                    }
                    Base::carr_obj(sensors)->setLength(q - sensors);
                    return sensors;
                }
                break;
            }
    }
    return (Sensor **)Base::CArray::empty()->elems();
}

}
}
