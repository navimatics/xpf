/*
 * SensorKit/impl/sensors/dr/DrSensor.cpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#include <SensorKit/impl/sensors/dr/DrSensor.hpp>

namespace SensorKit {
namespace impl {
namespace sensors {
namespace dr {

#define SENSOR_TIMER_PERIOD             1000 /* 1 sec */

/* adopted from GeoKit/impl/Navigation.cpp */
struct Point
{
    double x, y;
};
inline double rad(double a)
{
    return a * M_PI / 180.0;
}
inline double deg(double a)
{
    return a * 180.0 / M_PI;
}
static const double EarthMeanRadius = 6371000;
static Point GreatCircleDestination(Point point1, double dis, double brg)
{
    double phi1 = rad(point1.y);
    double lam1 = rad(point1.x);
    double cph1 = cos(phi1);
    double sph1 = sin(phi1);
    double rdis = dis / EarthMeanRadius;
    double cdis = cos(rdis);
    double sdis = sin(rdis);
    double cbrg = cos(rad(brg));
    double sbrg = sin(rad(brg));
    double phi2 = asin(sph1 * cdis + cph1 * sdis * cbrg);
    double sph2 = sin(phi2);
    double lam2 = lam1 + atan2(cph1 * sdis * sbrg, cdis - sph1 * sph2);
    Point point2;
    point2.y = deg(phi2);
    point2.x = fmod(deg(lam2) + 540, 360) - 180;
    return point2;
}

/*
 * DrSensorHandle
 */
class DrSensorHandle : public SensorHandle,
    public impl::GpsSensorHandle, public impl::ErrorSensorHandle
{
public:
    DrSensorHandle(DrSensor *sensor);
    int _open(Sensor::kind_t kind);
    int _close(Sensor::kind_t kind);
    void setParameter(const char *name, Base::Object *value);
    GpsData gpsData();
    Base::Error *error();
    void _triggerEvent(Sensor::kind_t evkind);
};
DrSensorHandle::DrSensorHandle(DrSensor *sensor) : SensorHandle(sensor)
{
}
int DrSensorHandle::_open(Sensor::kind_t kind)
{
    if (0 == openKind())
        if (!((DrSensor *)sensor())->addHandle(this))
            return -1;
    return 0;
}
int DrSensorHandle::_close(Sensor::kind_t kind)
{
    if (0 == openKind())
        ((DrSensor *)sensor())->removeHandle(this);
    return 0;
}
void DrSensorHandle::setParameter(const char *name, Base::Object *value)
{
    if (0 == strcmp("sensorParams", name))
    {
        if (Base::Map *params = dynamic_cast<Base::Map *>(value))
        {
            static Base::CString *timestampKey = Base::CStringConstant("timestamp");
            static Base::CString *latitudeKey = Base::CStringConstant("latitude");
            static Base::CString *longitudeKey = Base::CStringConstant("longitude");
            static Base::CString *trackKey = Base::CStringConstant("track");
            static Base::CString *speedKey = Base::CStringConstant("speed");
            GpsData gpsData = GpsDataNone();
            if (Base::Value *v = dynamic_cast<Base::Value *>(params->object(timestampKey)))
                gpsData.timestamp = v->realValue();
            if (Base::Value *v = dynamic_cast<Base::Value *>(params->object(latitudeKey)))
                gpsData.lat = v->realValue();
            if (Base::Value *v = dynamic_cast<Base::Value *>(params->object(longitudeKey)))
                gpsData.lon = v->realValue();
            if (Base::Value *v = dynamic_cast<Base::Value *>(params->object(trackKey)))
                gpsData.track = v->realValue();
            if (Base::Value *v = dynamic_cast<Base::Value *>(params->object(speedKey)))
                gpsData.speed = v->realValue();
            ((DrSensor *)sensor())->setGpsData(&gpsData);
        }
    }
}
GpsData DrSensorHandle::gpsData()
{
    return ((DrSensor *)sensor())->gpsData();
}
Base::Error *DrSensorHandle::error()
{
    return ((DrSensor *)sensor())->error();
}
void DrSensorHandle::_triggerEvent(Sensor::kind_t evkind)
{
    if (Sensor::kind_t kind = evkind & openKind())
        if (Delegate delegate = this->delegate())
            delegate(this, (evkind & Sensor::KindError) | kind);
}

/*
 * DrSensor
 */
Sensor *DrSensor::create(Base::Map *params)
{
    return new DrSensor;
}
DrSensor::DrSensor()
{
    _startData = GpsDataNone();
    _startData.mode = GpsData::ModeEstimated;
}
DrSensor::~DrSensor()
{
    assert(0 == _handles && 0 == _timer);
}
Sensor::kind_t DrSensor::kind()
{
    return KindGps;
}
SensorHandle *DrSensor::createHandle()
{
    return new DrSensorHandle(this);
}
const char *DrSensor::description()
{
    return Base::cstringf("GPS-DR: %f %f,%f trk=%.0f spd=%.1f",
        _startData.timestamp, _startData.lat, _startData.lon, _startData.track, _startData.speed);
}
bool DrSensor::addHandle(DrSensorHandle *handle)
{
    if (0 == _handles)
    {
        _time = 0;
        _timer = Base::ThreadLoopSetTimer(
            SENSOR_TIMER_PERIOD, make_delegate(&DrSensor::timerEvent, this), 0);
    }
    _handles = (DrSensorHandle **)Base::carr_concat(_handles, &handle, 1, sizeof(DrSensorHandle *));
    return true;
}
void DrSensor::removeHandle(DrSensorHandle *handle)
{
    size_t count = Base::carr_length(_handles);
    DrSensorHandle **p = _handles, **endp = p + count;
    /* find and remove handle */
    for (; endp > p; p++)
        if (*p == handle)
            break;
    if (endp > p)
    {
        /* if found (and removed) handle */
        memmove(p, p + 1, (endp - p - 1) * sizeof(DrSensorHandle *));
        Base::carr_obj(_handles)->setLength(count - 1);
        if (1 == count)
        {
            /* this was the last handle */
            Base::ThreadLoopCancelTimer(_timer);
            _timer = 0;
            Base::carr_release(_handles);
            _handles = 0;
        }
    }
}
void DrSensor::setGpsData(const GpsData *data)
{
    _startData = *data;
    _startData.mode = GpsData::ModeEstimated;
}
GpsData DrSensor::gpsData()
{
    if (Base::isNone(_startData.lat) || Base::isNone(_startData.lon))
        return GpsDataNone();
    if (Base::isNone(_startData.timestamp))
        return _startData;
    GpsData gpsData = _startData;
    gpsData.timestamp += _time / 86400;
    if (!Base::isNone(_startData.track) && !Base::isNone(_startData.speed))
    {
        Point destination = GreatCircleDestination((Point){ gpsData.lon, gpsData.lat },
            gpsData.speed * _time, gpsData.track);
        gpsData.lon = destination.x;
        gpsData.lat = destination.y;
    }
    return gpsData;
}
Base::Error *DrSensor::error()
{
    return 0;
}
void DrSensor::timerEvent(Base::Object *obj)
{
    if (Base::isNone(_startData.lat) || Base::isNone(_startData.lon))
        return;
    if (Base::isNone(_startData.timestamp))
        _time = 0;
    else
        _time = (Base::DateTime::currentJulianDate() - _startData.timestamp) * 86400;
    for (DrSensorHandle **p = _handles, **endp = p + Base::carr_length(_handles); endp > p; p++)
    {
        DrSensorHandle *handle = *p;
        handle->_triggerEvent(KindGps);
    }
}

}
}
}
}
