/*
 * SensorKit/impl/sensors/dr/DrSensor.hpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef SENSORKIT_IMPL_SENSORS_DR_DRSENSOR_HPP_INCLUDED
#define SENSORKIT_IMPL_SENSORS_DR_DRSENSOR_HPP_INCLUDED

#include <SensorKit/impl/Defines.hpp>
#include <SensorKit/impl/Sensor.hpp>

namespace SensorKit {
namespace impl {
namespace sensors {
namespace dr {

class DrSensorHandle;
class DrSensor : public Sensor
{
public:
    static Sensor *create(Base::Map *params = 0);
    DrSensor();
    ~DrSensor();
    kind_t kind();
    SensorHandle *createHandle();
    const char *description();
private:
    /* DrSensorHandle helpers */
    friend class DrSensorHandle;
    bool addHandle(DrSensorHandle *handle);
    void removeHandle(DrSensorHandle *handle);
    void setGpsData(const GpsData *data);
    GpsData gpsData();
    Base::Error *error();
    void timerEvent(Base::Object *obj);
private:
    GpsData _startData;
    DrSensorHandle **_handles;
    void *_timer;
    double _time;
};

}
}
}
}
#endif // SENSORKIT_IMPL_SENSORS_DR_DRSENSOR_HPP_INCLUDED
