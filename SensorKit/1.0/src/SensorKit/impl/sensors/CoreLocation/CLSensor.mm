/*
 * SensorKit/impl/sensors/CoreLocation/CLSensor.mm
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#include <SensorKit/impl/sensors/CoreLocation/CLSensor.hpp>
#if defined(SENSORKIT_CONFIG_CORELOCATION)
#import <CoreLocation/CoreLocation.h>

namespace SensorKit {
namespace impl {
namespace sensors {
namespace CoreLocation {

class CLSensorHandle : public SensorHandle,
    public GpsSensorHandle, public CompassSensorHandle, public ErrorSensorHandle
{
public:
    CLSensorHandle(Sensor *sensor);
    ~CLSensorHandle();
    int _open(Sensor::kind_t kind);
    int _close(Sensor::kind_t kind);
    GpsData gpsData();
    CompassData compassData();
    Base::Error *error();
    void _triggerEvent(Sensor::kind_t kind);
    void _triggerErrorEvent(Sensor::kind_t kind, int code, const char *message);
private:
    id _objcHandle;
    CLLocationManager *_locationManager;
    Base::Error *_error;
};

}
}
}
}

@interface SensorKit_impl_sensors_CoreLocation_CLSensorHandle : NSObject <CLLocationManagerDelegate>
{
    SensorKit::impl::sensors::CoreLocation::CLSensorHandle *_cppHandle; /* weak */
}
- (id)initWithCppHandle:(SensorKit::impl::sensors::CoreLocation::CLSensorHandle *)cppHandle;
@end
@implementation SensorKit_impl_sensors_CoreLocation_CLSensorHandle
- (id)initWithCppHandle:(SensorKit::impl::sensors::CoreLocation::CLSensorHandle *)cppHandle
{
    self = [super init];
    if (nil != self)
        _cppHandle = cppHandle;
    return self;
}
/* CLLocationManagerDelegate */
- (void)locationManager:(CLLocationManager *)manager
    didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    _cppHandle->_triggerEvent(SensorKit::impl::Sensor::KindGps);
}
- (void)locationManager:(CLLocationManager *)manager
    didUpdateHeading:(CLHeading *)newHeading
{
    _cppHandle->_triggerEvent(SensorKit::impl::Sensor::KindCompass);
}
- (void)locationManager:(CLLocationManager *)manager
    didFailWithError:(NSError *)error
{
    switch (error.code)
    {
    case kCLErrorLocationUnknown:
        _cppHandle->_triggerErrorEvent(SensorKit::impl::Sensor::KindGps,
            SensorKit::impl::SensorHandle::ErrorInformational, [[error localizedDescription] UTF8String]);
        break;
    case kCLErrorHeadingFailure:
        _cppHandle->_triggerErrorEvent(SensorKit::impl::Sensor::KindCompass,
            SensorKit::impl::SensorHandle::ErrorInformational, [[error localizedDescription] UTF8String]);
        break;
    case kCLErrorNetwork:
        _cppHandle->_triggerErrorEvent(SensorKit::impl::Sensor::KindGps | SensorKit::impl::Sensor::KindCompass,
            SensorKit::impl::SensorHandle::ErrorFunctionality, [[error localizedDescription] UTF8String]);
        break;
    case kCLErrorDenied:
        _cppHandle->_triggerErrorEvent(SensorKit::impl::Sensor::KindGps | SensorKit::impl::Sensor::KindCompass,
            SensorKit::impl::SensorHandle::ErrorUserCorrectable, [[error localizedDescription] UTF8String]);
        break;
    default:
        DEBUGLOG("%s", [[error localizedDescription] UTF8String]);
        break;
    }
}
- (void)locationManager:(CLLocationManager *)manager
    didChangeAuthorizationStatus:(CLAuthorizationStatus)status
{
    switch (status)
    {
    case kCLAuthorizationStatusNotDetermined:
        DEBUGLOG("kCLAuthorizationStatusNotDetermined");
        break;
    case kCLAuthorizationStatusRestricted:
        DEBUGLOG("kCLAuthorizationStatusRestricted");
        break;
    case kCLAuthorizationStatusDenied:
        DEBUGLOG("kCLAuthorizationStatusDenied");
        break;
    case kCLAuthorizationStatusAuthorized:
        DEBUGLOG("kCLAuthorizationStatusAuthorized");
        break;
    default:
        DEBUGLOG("UNKNOWN");
        break;
    }
}
- (BOOL)locationManagerShouldDisplayHeadingCalibration:(CLLocationManager *)manager
{
    return YES;
}
- (void)locationManager:(CLLocationManager *)manager
    didEnterRegion:(CLRegion *)region
{
    DEBUGLOG_HERE();
}
- (void)locationManager:(CLLocationManager *)manager
    didExitRegion:(CLRegion *)region
{
    DEBUGLOG_HERE();
}
- (void)locationManager:(CLLocationManager *)manager
    monitoringDidFailForRegion:(CLRegion *)region withError:(NSError *)error
{
    DEBUGLOG_HERE();
}
- (void)locationManager:(CLLocationManager *)manager
    didStartMonitoringForRegion:(CLRegion *)region
{
    DEBUGLOG_HERE();
}
@end

namespace SensorKit {
namespace impl {
namespace sensors {
namespace CoreLocation {

CLSensorHandle::CLSensorHandle(Sensor *sensor) : SensorHandle(sensor)
{
    _objcHandle = [[SensorKit_impl_sensors_CoreLocation_CLSensorHandle alloc] initWithCppHandle:this];
}
CLSensorHandle::~CLSensorHandle()
{
    close(Sensor::KindUnknown);
    assert(0 == _locationManager);
    [_objcHandle release];
    if (0 != _error)
        _error->release();
}
int CLSensorHandle::_open(Sensor::kind_t kind)
{
    if (0 == openKind())
    {
        assert(0 == _locationManager);
        _locationManager = [[CLLocationManager alloc] init];
        _locationManager.delegate = _objcHandle;
    }
    if (kind & Sensor::KindGps)
    {
        _locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation;
        _locationManager.distanceFilter = kCLDistanceFilterNone;
        [_locationManager startUpdatingLocation];
    }
    if (kind & Sensor::KindCompass)
    {
        [(id)_locationManager setHeadingFilter:kCLHeadingFilterNone];
        [(id)_locationManager startUpdatingHeading];
    }
    return 0;
}
int CLSensorHandle::_close(Sensor::kind_t kind)
{
    if (kind & Sensor::KindCompass)
        [(id)_locationManager stopUpdatingHeading];
    if (kind & Sensor::KindGps)
        [_locationManager stopUpdatingLocation];
    if (0 == openKind())
    {
        assert(0 != _locationManager);
        _locationManager.delegate = nil;
        [_locationManager release];
        _locationManager = nil;
    }
    return 0;
}
GpsData CLSensorHandle::gpsData()
{
    GpsData gpsData;
    NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
    if (CLLocation *location = (openKind() & Sensor::KindGps) ? [_locationManager location] : nil)
    {
        NSDate *timestamp = location.timestamp;
        CLLocationCoordinate2D coordinate = location.coordinate;
        CLLocationDistance altitude = location.altitude;
        CLLocationAccuracy horizontalAccuracy = location.horizontalAccuracy;
        CLLocationAccuracy verticalAccuracy = location.verticalAccuracy;
        CLLocationDirection course = location.course;
        CLLocationSpeed speed = location.speed;
        if (0 > horizontalAccuracy)
        {
            coordinate.latitude = coordinate.longitude = Base::None();
            horizontalAccuracy = Base::None();
        }
        if (0 > verticalAccuracy)
        {
            altitude = Base::None();
            verticalAccuracy = Base::None();
        }
        if (0 > course)
            course = Base::None();
        if (0 > speed)
            speed = Base::None();
        gpsData = (GpsData)
        {
            0 < horizontalAccuracy ? GpsData::ModeAutonomous : GpsData::ModeNoFix,
            2440587.5 + [timestamp timeIntervalSince1970] / 86400.0,
            Base::None(),               /* e_timestamp */
            coordinate.latitude, coordinate.longitude,
            altitude,
            horizontalAccuracy, horizontalAccuracy,
            verticalAccuracy,
            course,
            speed,
            Base::None(),               /* climb */
            Base::None(),               /* e_track */
            Base::None(),               /* e_speed */
            Base::None(),               /* e_climb */
        };
    }
    else
        gpsData = GpsDataNone();
    [pool drain];
    return gpsData;
}
CompassData CLSensorHandle::compassData()
{
    CompassData compassData;
    NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
    if (CLHeading *heading = (openKind() & Sensor::KindCompass) ? [(id)_locationManager heading] : nil)
    {
        NSDate *timestamp = heading.timestamp;
        CLLocationDirection magneticHeading = heading.magneticHeading;
        CLLocationAccuracy headingAccuracy = heading.headingAccuracy;
        if (0 > headingAccuracy)
        {
            magneticHeading = Base::None();
            headingAccuracy = Base::None();
        }
        compassData = (CompassData)
        {
            2440587.5 + [timestamp timeIntervalSince1970] / 86400.0,
            Base::None(),               /* e_timestamp */
            Base::None(),
            magneticHeading,
            headingAccuracy,
        };
    }
    else
        compassData = CompassDataNone();
    [pool drain];
    return compassData;
}
Base::Error *CLSensorHandle::error()
{
    if (0 != _error)
        _error->autocollect();
    return _error;
}
void CLSensorHandle::_triggerEvent(Sensor::kind_t evkind)
{
    if (Sensor::kind_t kind = evkind & openKind())
        if (Delegate delegate = this->delegate())
            delegate(this, kind);
}
void CLSensorHandle::_triggerErrorEvent(Sensor::kind_t evkind, int code, const char *message)
{
    if (0 != _error)
    {
        _error->release();
        _error = 0;
    }
    _error = new Base::Error(errorDomain(), code, message);
    if (Sensor::kind_t kind = evkind & openKind())
        if (Delegate delegate = this->delegate())
            delegate(this, Sensor::KindError | kind);
}

Sensor *CLSensor::create(Base::Map *params)
{
    return new CLSensor;
}
Sensor::kind_t CLSensor::kind()
{
    kind_t kind = KindGps;
    if ([CLLocationManager respondsToSelector:@selector(headingAvailable)] &&
        [CLLocationManager headingAvailable])
        kind |= KindCompass;
    return kind;
}
SensorHandle *CLSensor::createHandle()
{
    return new CLSensorHandle(this);
}
const char *CLSensor::description()
{
    static const char *s = Base::CStringConstant("CoreLocation")->chars();
    return s;
}

}
}
}
}
#endif // SENSORKIT_CONFIG_CORELOCATION
