/*
 * SensorKit/impl/sensors/CoreLocation/CLSensor.hpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef SENSORKIT_IMPL_SENSORS_CORELOCATION_CLSENSOR_HPP_INCLUDED
#define SENSORKIT_IMPL_SENSORS_CORELOCATION_CLSENSOR_HPP_INCLUDED

#include <SensorKit/impl/Defines.hpp>
#if defined(SENSORKIT_CONFIG_CORELOCATION)
#include <SensorKit/impl/Sensor.hpp>

namespace SensorKit {
namespace impl {
namespace sensors {
namespace CoreLocation {

class CLSensor : public Sensor
{
public:
    static Sensor *create(Base::Map *params = 0);
    kind_t kind();
    SensorHandle *createHandle();
    const char *description();
};

}
}
}
}
#endif // SENSORKIT_CONFIG_CORELOCATION

#endif // SENSORKIT_IMPL_SENSORS_CORELOCATION_CLSENSOR_HPP_INCLUDED
