/*
 * SensorKit/impl/sensors/shared/SocketStream.cpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#include <SensorKit/impl/sensors/shared/SocketStream.hpp>
#if defined(BASE_CONFIG_WINDOWS)
#include <winsock.h>
#include <ws2tcpip.h>
#elif defined(BASE_CONFIG_POSIX)
#include <netdb.h>
#include <sys/socket.h>
#else
#error SensorKit/impl/sensors/shared/SocketStream.cpp not implemented for this platform
#endif

namespace SensorKit {
namespace impl {
namespace sensors {
namespace shared {

static inline void setLastErrorFromSockets()
{
#if defined(BASE_CONFIG_WINDOWS)
    static const char *domain = Base::CStringConstant("windows")->chars();
    Base::Error::setLastError(domain, WSAGetLastError());
#elif defined(BASE_CONFIG_POSIX)
    Base::Error::setLastErrorFromSystem();
#endif
}

SocketStream *SocketStream::create(const Base::cstr_uri_parts *parts)
{
    if (0 == parts->scheme || 0 != strcmp("tcp", parts->scheme) || 0 == parts->host || 0 == parts->port)
    {
        Base::Error::setLastErrorFromCErrno(EINVAL);
        return 0;
    }
    handle_t handle = InvalidHandle;
    struct addrinfo hints = { 0 };
    hints.ai_family = AF_UNSPEC;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_protocol = IPPROTO_TCP;
    //hints.ai_flags = AI_NUMERICSERV;
    struct addrinfo *info = 0;
    int res = getaddrinfo(parts->host, parts->port, &hints, &info);
    if (0 != res)
    {
        static const char *domain = Base::CStringConstant("getaddrinfo")->chars();
#if defined(BASE_CONFIG_WINDOWS)
        Base::Error::setLastError(domain, res);
            /* gai_strerror() is not thread-safe in Windows */
#elif defined(BASE_CONFIG_POSIX)
        Base::Error::setLastError(domain, res, gai_strerror(res));
#endif
        return 0;
    }
    for (struct addrinfo *p = info; 0 != p; p = info->ai_next)
        if (AF_INET == p->ai_family || AF_INET6 == p->ai_family)
        {
            handle = socket(p->ai_family, p->ai_socktype, p->ai_protocol);
            if (InvalidHandle == handle)
                goto cleanup_and_report_error;
            res = connect(handle, p->ai_addr, p->ai_addrlen);
            if (-1 == res)
                goto cleanup_and_report_error;
            break;
        }
    freeaddrinfo(info);
    return new SocketStream(handle);
cleanup_and_report_error:
    setLastErrorFromSockets();
    if (InvalidHandle != handle)
#if defined(BASE_CONFIG_WINDOWS)
        closesocket(handle);
#elif defined(BASE_CONFIG_POSIX)
        ::close(handle);
#endif
    freeaddrinfo(info);
    return 0;
}
SocketStream::SocketStream(handle_t handle) : inherited(handle)
{
}
SocketStream::~SocketStream()
{
    if (InvalidHandle != handle())
        close();
}
ssize_t SocketStream::_close(handle_t handle)
{
#if defined(BASE_CONFIG_WINDOWS)
    return 0 == closesocket(handle) ? 0 : -1;
#elif defined(BASE_CONFIG_POSIX)
    return ::close(handle);
#endif
}

}
}
}
}
