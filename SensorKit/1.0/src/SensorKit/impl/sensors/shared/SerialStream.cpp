/*
 * SensorKit/impl/sensors/shared/SerialStream.cpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#include <SensorKit/impl/sensors/shared/SerialStream.hpp>
#if defined(BASE_CONFIG_WINDOWS)
#include <windows.h>
#elif defined(BASE_CONFIG_POSIX)
#include <fcntl.h>
#include <sys/ioctl.h>
#include <termios.h>
#else
#error SensorKit/impl/sensors/shared/SerialStream.cpp not implemented for this platform
#endif

namespace SensorKit {
namespace impl {
namespace sensors {
namespace shared {

SerialStream *SerialStream::create(const Base::cstr_uri_parts *parts, const char *mode)
{
    if (0 == parts->scheme || 0 != strcmp("file", parts->scheme) || 0 == parts->path)
    {
        Base::Error::setLastErrorFromCErrno(EINVAL);
        return 0;
    }
    unsigned speed = 0;
    mark_and_collect
    {
        const char *query = parts->query;
        const char *key, *val;
        while (0 != (query = Base::cstr_uri_query_extract(query, &key, &val)))
            if (0 == strcmp("speed", key))
                speed = strtoul(val, 0, 10);
    }
    int a = 0, r = 0, w = 0, b = 0, plus = 0;
    while (*mode)
    {
        char c = *mode++;
        switch (c)
        {
        case 'a':
            a = 1;
            break;
        case 'r':
            r = 1;
            break;
        case 'w':
            w = 1;
            break;
        case 'b':
            b = 1;
            break;
        case '+':
            plus = 1;
            break;
        default:
            break;
        }
    }
#if defined(BASE_CONFIG_WINDOWS)
    DWORD iomode;
    if (r)
        iomode = plus ? GENERIC_READ | GENERIC_WRITE : GENERIC_READ;
    else if (w)
        iomode = plus ? GENERIC_READ | GENERIC_WRITE : GENERIC_WRITE;
    else if (a)
        iomode = plus ? GENERIC_READ | GENERIC_WRITE : GENERIC_WRITE;
    else
    {
        Base::Error::setLastErrorFromCErrno(EINVAL);
        return 0;
    }
    handle_t handle = (handle_t)CreateFileA(
        parts->path, iomode, 0, 0, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL | FILE_FLAG_OVERLAPPED, 0);
    if (InvalidHandle == handle)
    {
        Base::Error::setLastErrorFromSystem();
        return 0;
    }
    DCB dcb = { 0 }, *dcbp;
    dcb.DCBLength = sizeof dcb;
    if (GetCommState(handle, &dcb))
    {
        /* make a copy of the original COM attributes */
        dcbp = (DCB *)Base::Malloc(sizeof dcb);
        *dcbp = dcb;
        dcb.ByteSize = 8;
        dcb.StopBits = ONESTOPBIT;
        dcb.Parity = NOPARITY;
        if (0 != speed)
        {
            if (speed < 300)
                dcb.BaudRate = 0;
            else if (speed < 1200)
                dcb.BaudRate = CBR_300;
            else if (speed < 2400)
                dcb.BaudRate = CBR_1200;
            else if (speed < 4800)
                dcb.BaudRate = CBR_2400;
            else if (speed < 9600)
                dcb.BaudRate = CBR_4800;
            else if (speed < 19200)
                dcb.BaudRate = CBR_9600;
            else if (speed < 38400)
                dcb.BaudRate = CBR_19200;
            else
                dcb.BaudRate = CBR_38400;
        }
        if (!SetCommState(handle, &dcb))
            goto close_and_report_error;
    }
    DEBUGLOG("%s?speed=%u -> %p", parts->path, speed, handle);
    return new SerialStream(handle, dcbp);
close_and_report_error:
    Base::Error::setLastErrorFromSystem();
    if (0 != dcbp)
        free(dcbp);
    CloseHandle(handle);
    return 0;
#elif defined(BASE_CONFIG_POSIX)
    int iomode;
    if (r)
        iomode = plus ? O_RDWR : O_RDONLY;
    else if (w)
        iomode = plus ? O_RDWR : O_WRONLY;
    else if (a)
        iomode = plus ? O_RDWR : O_WRONLY;
    else
    {
        Base::Error::setLastErrorFromCErrno(EINVAL);
        return 0;
    }
    handle_t handle = ::open(parts->path, iomode | O_NOCTTY | O_NONBLOCK);
        /* O_NOCTTY: do not change controlling terminal; O_NONBLOCK: do not block on open */
    if (InvalidHandle == handle)
    {
        Base::Error::setLastErrorFromSystem();
        return 0;
    }
    struct termios tcattr, *tcattrp = 0;
    if (-1 != tcgetattr(handle, &tcattr))
    {
        /*
         * terminal device
         */
        /* make a copy of the original terminal attributes */
        tcattrp = (struct termios *)Base::Malloc(sizeof tcattr);
        *tcattrp = tcattr;
        /* set exclusive use of this terminal */
        if (-1 == ioctl(handle, TIOCEXCL))
            goto close_and_report_error;
        /* remove O_NONBLOCK */
        /* !!!:
         * In at least one instance the Prolific driver hang on tcsetattr() below,
         * when this call was moved before the tcgetattr() conditional above.
         */
        if (-1 == fcntl(handle, F_SETFL, 0))
            goto close_and_report_error;
        /* raw mode, 1 char at a time, no timeout */
        cfmakeraw(&tcattr);
        tcattr.c_cc[VMIN] = 1;
        tcattr.c_cc[VTIME] = 0;
        /* speed */
        if (0 != speed)
        {
            speed_t actual;
            if (speed < 300)
                actual = B0;
            else if (speed < 1200)
                actual = B300;
            else if (speed < 2400)
                actual = B1200;
            else if (speed < 4800)
                actual = B2400;
            else if (speed < 9600)
                actual = B4800;
            else if (speed < 19200)
                actual = B9600;
            else if (speed < 38400)
                actual = B19200;
            else
                actual = B38400;
            cfsetispeed(&tcattr, actual);
            cfsetospeed(&tcattr, actual);
        }
        /* set terminal attributes */
        if (-1 == tcsetattr(handle, TCSANOW, &tcattr))
            goto close_and_report_error;
    }
    else
    {
        /*
         * non-terminal device
         */
        /* remove O_NONBLOCK */
        if (-1 == fcntl(handle, F_SETFL, 0))
            goto close_and_report_error;
    }
    DEBUGLOG("%s?speed=%u -> %i", parts->path, speed, handle);
    return new SerialStream(handle, tcattrp);
close_and_report_error:
    Base::Error::setLastErrorFromSystem();
    if (0 != tcattrp)
        free(tcattrp);
    ioctl(handle, TIOCNXCL);
    ::close(handle);
    return 0;
#endif
}
SerialStream::SerialStream(handle_t handle, void *internal) : inherited(handle)
{
    _internal = internal;
}
SerialStream::~SerialStream()
{
    if (InvalidHandle != handle())
        close();
}
ssize_t SerialStream::_close(handle_t handle)
{
#if defined(BASE_CONFIG_WINDOWS)
    DEBUGLOG("%p", handle);
#elif defined(BASE_CONFIG_POSIX)
    DEBUGLOG("%i", handle);
#endif
    if (0 != _internal)
    {
#if defined(BASE_CONFIG_WINDOWS)
        /* reset original COM attributes */
        SetCommState(handle, (DCB *)_internal);
#elif defined(BASE_CONFIG_POSIX)
        /* reset original terminal attributes */
        tcsetattr(handle, TCSANOW, (struct termios *)_internal);
        ioctl(handle, TIOCNXCL);
#endif
        free(_internal);
        _internal = 0;
    }
    return inherited::_close(handle);
}

}
}
}
}
