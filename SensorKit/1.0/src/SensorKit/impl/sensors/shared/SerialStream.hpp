/*
 * SensorKit/impl/sensors/shared/SerialStream.hpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef SENSORKIT_IMPL_SENSORS_SHARED_SERIALSTREAM_HPP_INCLUDED
#define SENSORKIT_IMPL_SENSORS_SHARED_SERIALSTREAM_HPP_INCLUDED

#include <SensorKit/impl/Defines.hpp>

namespace SensorKit {
namespace impl {
namespace sensors {
namespace shared {

class SerialStream : public Base::SystemStream
{
public:
    static SerialStream *create(const Base::cstr_uri_parts *parts, const char *mode);
    ~SerialStream();
protected:
    ssize_t _close(handle_t handle);
private:
    SerialStream(handle_t handle, void *internal);
private:
    typedef SystemStream inherited;
    void *_internal;
};

}
}
}
}

#endif // SENSORKIT_IMPL_SENSORS_SHARED_SERIALSTREAM_HPP_INCLUDED
