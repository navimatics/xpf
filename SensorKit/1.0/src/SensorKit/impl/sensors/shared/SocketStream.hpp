/*
 * SensorKit/impl/sensors/shared/SocketStream.hpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef SENSORKIT_IMPL_SENSORS_SHARED_SOCKETSTREAM_HPP_INCLUDED
#define SENSORKIT_IMPL_SENSORS_SHARED_SOCKETSTREAM_HPP_INCLUDED

#include <SensorKit/impl/Defines.hpp>

namespace SensorKit {
namespace impl {
namespace sensors {
namespace shared {

class SocketStream : public Base::SystemStream
{
public:
    static SocketStream *create(const Base::cstr_uri_parts *parts);
    ~SocketStream();
protected:
    ssize_t _close(handle_t handle);
private:
    typedef SystemStream inherited;
    SocketStream(handle_t handle);
};

}
}
}
}

#endif // SENSORKIT_IMPL_SENSORS_SHARED_SOCKETSTREAM_HPP_INCLUDED
