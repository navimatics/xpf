/*
 * SensorKit/impl/sensors/gpsais/GpsNmeaProcessor.hpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef SENSORKIT_IMPL_SENSORS_GPSAIS_GPSNMEAPROCESSOR_HPP_INCLUDED
#define SENSORKIT_IMPL_SENSORS_GPSAIS_GPSNMEAPROCESSOR_HPP_INCLUDED

#include <SensorKit/impl/Defines.hpp>
#if defined(SENSORKIT_CONFIG_NMEA)
#include <SensorKit/impl/sensors/gpsais/Defines.hpp>
#include <SensorKit/impl/sensors/gpsais/NmeaParser.hpp>

namespace SensorKit {
namespace impl {
namespace sensors {
namespace gpsais {

class GpsNmeaProcessor : public Base::Object
{
public:
    typedef Base::Delegate<void (unsigned, const void *)> Delegate;
public:
    GpsNmeaProcessor(NmeaParser *parser, Delegate delegate);
    ~GpsNmeaProcessor();
private:
    enum
    {
        tagNone                         = 0x0000,
        tagGPGGA                        = 0x0001,
        tagGPGLL                        = 0x0002,
        tagGPGSA                        = 0x0004,
        tagGPGSV                        = 0x0008,
        tagGPRMC                        = 0x0010,
        tagGPVTG                        = 0x0020,
    };
    void maybeTriggerCycleEvent(unsigned tag);
    void parseFAA(const char *faaarg);
    void parseStatus(const char *statarg);
    void parseTimestamp(const char *timearg, const char *datearg = 0);
    void parseLatitude(const char *latarg, const char *hemiarg);
    void parseLongitude(const char *lonarg, const char *hemiarg);
    void parseAltitude(const char *altarg, const char *unitarg);
    void parseTrack(const char *trackarg);
    void parseSpeed(const char *speedarg, const char *unitarg);
    void parseGPGGA(const char *argv[], size_t argc);
    void parseGPGLL(const char *argv[], size_t argc);
    void parseGPGSA(const char *argv[], size_t argc);
    void parseGPGSV(const char *argv[], size_t argc);
    void parseGPRMC(const char *argv[], size_t argc);
    void parseGPVTG(const char *argv[], size_t argc);
private:
    NmeaParser *_parser;
    Delegate _delegate;
    GpsData _gpsData;
    GpsSkyData _gpsSkyData;
    unsigned _cycleTags, _lastTag;
    double _cycleJulianDate, _lastJulianDate;
};

}
}
}
}
#endif // SENSORKIT_CONFIG_NMEA

#endif // SENSORKIT_IMPL_SENSORS_GPSAIS_GPSNMEAPROCESSOR_HPP_INCLUDED
