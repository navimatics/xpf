/*
 * SensorKit/impl/sensors/gpsais/GpsAisSensor.hpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef SENSORKIT_IMPL_SENSORS_GPSAIS_GPSAISSENSOR_HPP_INCLUDED
#define SENSORKIT_IMPL_SENSORS_GPSAIS_GPSAISSENSOR_HPP_INCLUDED

#include <SensorKit/impl/Defines.hpp>
#if defined(SENSORKIT_CONFIG_NMEA)
#include <SensorKit/impl/sensors/gpsais/Defines.hpp>

namespace SensorKit {
namespace impl {
namespace sensors {
namespace gpsais {

class GpsAisSensorHandle;
class GpsAisSensorThread;
class GpsAisSensor : public Sensor
{
public:
    static Sensor *createNmea(Base::Map *params = 0);
    static Sensor *createGpsd(Base::Map *params = 0);
    GpsAisSensor(unsigned protocol, const char *uri);
    ~GpsAisSensor();
    const char *uri();
    kind_t kind();
    SensorHandle *createHandle();
    const char *description();
private:
    /* GpsAisSensorHandle helpers */
    friend class GpsAisSensorHandle;
    bool addHandle(GpsAisSensorHandle *handle);
    void removeHandle(GpsAisSensorHandle *handle);
    GpsData gpsData();
    GpsSkyData gpsSkyData();
    void aisData(const AisData **pDataArray, const AisVesselData **pVesselDataArray);
    Base::Error *error();
    void timerEvent(Base::Object *obj);
    /* GpsAisSensorThread helpers */
    friend class GpsAisSensorThread;
    void gpsaisEvent(GpsAisSensorThread *thread, unsigned structKind, const void *structBuf);
    void errorEvent(GpsAisSensorThread *thread, int code, const char *message);
private:
    unsigned _protocol;
    const char *_uri;
    Base::Lock *_lock;
    GpsData _cycleGpsData;
    GpsSkyData _cycleGpsSkyData;
    AisData *_aisDataArray, *_cycleAisDataArray;
    AisVesselData *_aisVesselDataArray, *_cycleAisVesselDataArray;
    Base::Error *_cycleError;
    GpsAisSensorHandle **_handles;
    GpsAisSensorThread *_thread;
    void *_timer;
};

}
}
}
}
#endif // SENSORKIT_CONFIG_NMEA

#endif // SENSORKIT_IMPL_SENSORS_GPSAIS_GPSAISSENSOR_HPP_INCLUDED
