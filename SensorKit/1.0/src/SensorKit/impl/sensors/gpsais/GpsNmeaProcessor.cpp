/*
 * SensorKit/impl/sensors/gpsais/GpsNmeaProcessor.cpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#include <SensorKit/impl/sensors/gpsais/GpsNmeaProcessor.hpp>
#if defined(SENSORKIT_CONFIG_NMEA)

namespace SensorKit {
namespace impl {
namespace sensors {
namespace gpsais {

static inline bool parseNatNum(const char *p, int &value)
{
    value = 0;
    while ('\0' != *p)
    {
        if (!isdigit(*p))
            return false;
        value *= 10;
        value += *p - '0';
        p++;
    }
    return true;
}
static inline bool parseNatNum(const char *p, size_t len, int &value)
{
    value = 0;
    const char *endp = p + len;
    while (endp > p)
    {
        if (!isdigit(*p))
            return false;
        value *= 10;
        value += *p - '0';
        p++;
    }
    return true;
}
static inline bool parseDouble(const char *p, double &value)
{
    value = 0;
    while ('\0' != *p)
    {
        if (!isdigit(*p))
            goto nondigit;
        value *= 10;
        value += *p - '0';
        p++;
    }
    return true;
nondigit:
    if ('.' != *p++)
        return false;
    double fraction = 1;
    while ('\0' != *p)
    {
        if (!isdigit(*p))
            return false;
        fraction /= 10;
        value += (*p - '0') * fraction;
        p++;
    }
    return true;
}

GpsNmeaProcessor::GpsNmeaProcessor(NmeaParser *parser, Delegate delegate)
{
    parser->retain();
    _parser = parser;
    _delegate = delegate;
    _gpsData = GpsDataNone();
    _gpsSkyData = GpsSkyDataNone();
    _cycleTags = _lastTag = 0;
    _cycleJulianDate = _lastJulianDate = 0;
    _parser->addDelegate("$GPGGA", make_delegate(&GpsNmeaProcessor::parseGPGGA, this));
    _parser->addDelegate("$GPGLL", make_delegate(&GpsNmeaProcessor::parseGPGLL, this));
    _parser->addDelegate("$GPGSA", make_delegate(&GpsNmeaProcessor::parseGPGSA, this));
    _parser->addDelegate("$GPGSV", make_delegate(&GpsNmeaProcessor::parseGPGSV, this));
    _parser->addDelegate("$GPRMC", make_delegate(&GpsNmeaProcessor::parseGPRMC, this));
    _parser->addDelegate("$GPVTG", make_delegate(&GpsNmeaProcessor::parseGPVTG, this));
}
GpsNmeaProcessor::~GpsNmeaProcessor()
{
    _parser->removeDelegate("$GPGGA");
    _parser->removeDelegate("$GPGLL");
    _parser->removeDelegate("$GPGSA");
    _parser->removeDelegate("$GPGSV");
    _parser->removeDelegate("$GPRMC");
    _parser->removeDelegate("$GPVTG");
    _parser->release();
}
void GpsNmeaProcessor::maybeTriggerCycleEvent(unsigned tag)
{
    if (Base::isNone(_gpsData.timestamp))
        return;
    double julianDate = _gpsData.timestamp;
    if (0.5 < (julianDate - _lastJulianDate) * 86400)
    {
        _cycleTags |= _lastTag;
        _cycleJulianDate = _lastJulianDate;
    }
    _lastTag = tag;
    _lastJulianDate = julianDate;
    if ((_cycleTags & tag) && 0.5 < (julianDate - _cycleJulianDate) * 86400)
    {
        _cycleJulianDate = julianDate;
        GpsDataAndSkyData data = { &_gpsData, &_gpsSkyData };
        _delegate(GpsDataAndSkyDataStruct, &data);
    }
}
void GpsNmeaProcessor::parseFAA(const char *faaarg)
{
    /*
     * FAA Mode Indicator
     *     A = Autonomous mode
     *     D = Differential Mode
     *     E = Estimated (dead-reckoning) mode
     *     M = Manual Input Mode
     *     S = Simulated Mode
     *     N = Data Not Valid
     */
    switch (_gpsData.mode = faaarg[0])
    {
    case GpsData::ModeAutonomous:   /* A */
    case GpsData::ModeDifferential: /* D */
    case GpsData::ModeEstimated:    /* E */
    case GpsData::ModeManualInput:  /* M */
    case GpsData::ModeSimulated:    /* S */
    case GpsData::ModeNoFix:        /* N */
        break;
    default:
        _gpsData.mode = GpsData::ModeUnknown;
        break;
    }
}
void GpsNmeaProcessor::parseStatus(const char *statarg)
{
    /* status */
    switch (statarg[0])
    {
    case 'A':
        if (GpsData::ModeUnknown == _gpsData.mode ||
            GpsData::ModeNoFix == _gpsData.mode)
            _gpsData.mode = GpsData::ModeAutonomous;
        break;
    case 'V':
        _gpsData.mode = GpsData::ModeNoFix;
        break;
    default:
        _gpsData.mode = GpsData::ModeUnknown;
        break;
    }
}
void GpsNmeaProcessor::parseTimestamp(const char *timearg, const char *datearg)
{
    Base::DateTime::Components components = { 0 };
    bool dateValid = false;
    if (0 != datearg && 6 == strlen(datearg))
    {
        int day, month, year;
        if (parseNatNum(datearg + 0, 2, day) &&
            parseNatNum(datearg + 2, 2, month) &&
            parseNatNum(datearg + 4, 2, year))
        {
            components.day = day;
            components.month = month;
            components.year = 2000 + year;
                /* !!!: it is currently 2012; will this code live to 2100? */
            dateValid = true;
        }
    }
    _gpsData.timestamp = Base::None();
    if (6 <= strlen(timearg))
    {
        int hour, min; double sec;
        if (parseNatNum(timearg + 0, 2, hour) &&
            parseNatNum(timearg + 2, 2, min) &&
            parseDouble(timearg + 4, sec))
        {
            double sfr = modf(sec, &sec);
            if (!dateValid)
            {
                double implicitDate = !Base::isNone(_gpsData.timestamp)
                    ? _gpsData.timestamp
                    : Base::DateTime::currentJulianDate();
                components = Base::DateTime::componentsFromJulianDate(implicitDate);
                components.hour = hour;
                components.min = min;
                components.sec = (int)sec;
                components.msec = 1000 * sfr;
                double computedJulianDate = Base::DateTime::julianDateFromComponents(components);
                if (computedJulianDate < implicitDate && 0.5 < fabs(computedJulianDate - implicitDate))
                {
                    /* Assuming that our clock is close to the GPS clock,
                     * the computedJulianDate cannot be significantly less than our time.
                     * If that was the case, the reported timestamp is actually tomorrow.
                     */
                    components = Base::DateTime::componentsFromJulianDate(implicitDate + 1.0);
                    components.hour = hour;
                    components.min = min;
                    components.sec = (int)sec;
                    components.msec = 1000 * sfr;
                }
            }
            else
            {
                components.hour = hour;
                components.min = min;
                components.sec = (int)sec;
                components.msec = 1000 * sfr;
            }
            _gpsData.timestamp = Base::DateTime::julianDateFromComponents(components);
        }
    }
}
void GpsNmeaProcessor::parseLatitude(const char *latarg, const char *hemiarg)
{
    /* latitude */
    _gpsData.lat = Base::None();
    if (4 <= strlen(latarg))
    {
        int deg; double min;
        if (parseNatNum(latarg + 0, 2, deg) &&
            parseDouble(latarg + 2, min))
        {
            /* latitude hemisphere */
            switch (hemiarg[0])
            {
            case 'N': case 'n':
                _gpsData.lat = deg + min / 60.0;
                break;
            case 'S': case 's':
                _gpsData.lat = -(deg + min / 60.0);
                break;
            }
        }
    }
}
void GpsNmeaProcessor::parseLongitude(const char *lonarg, const char *hemiarg)
{
    /* longitude */
    _gpsData.lon = Base::None();
    if (5 <= strlen(lonarg))
    {
        int deg; double min;
        if (parseNatNum(lonarg + 0, 3, deg) &&
            parseDouble(lonarg + 3, min))
        {
            /* longitude hemisphere */
            switch (hemiarg[0])
            {
            case 'E': case 'e':
                _gpsData.lon = deg + min / 60.0;
                break;
            case 'W': case 'w':
                _gpsData.lon = -(deg + min / 60.0);
                break;
            }
        }
    }
}
void GpsNmeaProcessor::parseAltitude(const char *altarg, const char *unitarg)
{
    /* altitude */
    _gpsData.alt = Base::None();
    if (altarg[0]/* 1 <= strlen(altarg) */)
    {
        double alt;
        if (parseDouble(altarg, alt))
        {
            /* altitude units */
            switch (unitarg[0])
            {
            case 'M': case 'm':
                _gpsData.alt = alt;
                break;
            }
        }
    }
}
void GpsNmeaProcessor::parseTrack(const char *trackarg)
{
    /* track */
    _gpsData.track = Base::None();
    if (trackarg[0]/* 1 <= strlen(trackarg) */)
    {
        double track;
        if (parseDouble(trackarg, track))
        {
            _gpsData.track = track;
        }
    }
}
void GpsNmeaProcessor::parseSpeed(const char *speedarg, const char *unitarg)
{
    /* speed */
    _gpsData.speed = Base::None();
    if (speedarg[0]/* 1 <= strlen(speedarg) */)
    {
        double speed;
        if (parseDouble(speedarg, speed))
        {
            /* speed units */
            switch (unitarg[0])
            {
            case 'N': case 'n':
                _gpsData.speed = speed * 0.514444;  /* kn -> m/s */
                break;
            case 'K': case 'k':
                _gpsData.speed = speed * 0.2777778; /* km / h -> m/s */
                break;
            }
        }
    }
}
void GpsNmeaProcessor::parseGPGGA(const char *argv[], size_t argc)
{
    /* === GGA - Global Positioning System Fix Data ===
     *
     * Time, Position and fix related data for a GPS receiver.
     *
     * ------------------------------------------------------------------------------
     *                                                       11
     *         1         2       3 4        5 6 7  8   9  10 |  12 13  14   15
     *         |         |       | |        | | |  |   |   | |   | |   |    |
     *  $--GGA,hhmmss.ss,llll.ll,a,yyyyy.yy,a,x,xx,x.x,x.x,M,x.x,M,x.x,xxxx*hh<CR><LF>
     * ------------------------------------------------------------------------------
     *
     * Field Number:
     *
     * 1. Universal Time Coordinated (UTC)
     * 2. Latitude
     * 3. N or S (North or South)
     * 4. Longitude
     * 5. E or W (East or West)
     * 6. GPS Quality Indicator,
     *      - 0 - fix not available,
     *      - 1 - GPS fix,
     *      - 2 - Differential GPS fix
     *            (values above 2 are 2.3 features)
     *      - 3 = Pulse Per Second (PPS) fix
     *      - 4 = Real Time Kinematic
     *      - 5 = Float RTK
     *      - 6 = estimated (dead reckoning)
     *      - 7 = Manual input mode
     *      - 8 = Simulation mode
     * 7. Number of satellites in view, 00 - 12
     * 8. Horizontal Dilution of precision (meters)
     * 9. Antenna Altitude above/below mean-sea-level (geoid) (in meters)
     * 10. Units of antenna altitude, meters
     * 11. Geoidal separation, the difference between the WGS-84 earth
     *      ellipsoid and mean-sea-level (geoid), "-" means mean-sea-level
     *      below ellipsoid
     * 12. Units of geoidal separation, meters
     * 13. Age of differential GPS data, time in seconds since last SC104
     *      type 1 or 9 update, null field when DGPS is not used
     * 14. Differential reference station ID, 0000-1023
     * 15. Checksum
     */
    if (13 > argc)
        return;
    /* quality indicator */
    switch (argv[6][0])
    {
    case '1':
        _gpsData.mode = GpsData::ModeAutonomous;
        break;
    case '2':
        _gpsData.mode = GpsData::ModeDifferential;
        break;
    case '3':
    case '4':
    case '5':
        _gpsData.mode = GpsData::ModeAutonomous;
        break;
    case '6':
        _gpsData.mode = GpsData::ModeEstimated;
        break;
    case '7':
        _gpsData.mode = GpsData::ModeManualInput;
        break;
    case '8':
        _gpsData.mode = GpsData::ModeSimulated;
        break;
    case '0':
        _gpsData.mode = GpsData::ModeNoFix;
        break;
    default:
        _gpsData.mode = GpsData::ModeUnknown;
        break;
    }
    /* timestamp, latitude, longitude, altitude */
    parseTimestamp(argv[1]);
    parseLatitude(argv[2], argv[3]);
    parseLongitude(argv[4], argv[5]);
    parseAltitude(argv[9], argv[10]);
    maybeTriggerCycleEvent(tagGPGGA);
}
void GpsNmeaProcessor::parseGPGLL(const char *argv[], size_t argc)
{
    /* === GLL - Geographic Position - Latitude/Longitude ===
     *
     * ------------------------------------------------------------------------------
     *         1       2 3        4 5         6 7  8
     *         |       | |        | |         | |  |
     *  $--GLL,llll.ll,a,yyyyy.yy,a,hhmmss.ss,a,m,*hh<CR><LF>
     * ------------------------------------------------------------------------------
     *
     * Field Number:
     *
     * 1. Latitude
     * 2. N or S (North or South)
     * 3. Longitude
     * 4. E or W (East or West)
     * 5. Universal Time Coordinated (UTC)
     * 6. Status A - Data Valid, V - Data Invalid
     * 7. FAA mode indicator (NMEA 2.3 and later)
     * 8. Checksum
     */
    if (7 > argc)
        return;
    if (8 <= argc)
        parseFAA(argv[7]);
    else
        parseStatus(argv[6]);
    /* timestamp, latitude, longitude */
    parseTimestamp(argv[5]);
    parseLatitude(argv[1], argv[2]);
    parseLongitude(argv[3], argv[4]);
    maybeTriggerCycleEvent(tagGPGLL);
}
void GpsNmeaProcessor::parseGPGSA(const char *argv[], size_t argc)
{
    /* === GSA - GPS DOP and active satellites ===
     *
     * ------------------------------------------------------------------------------
     *         1 2 3                        14 15  16  17  18
     *         | | |                         | |   |   |   |
     *  $--GSA,a,a,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x.x,x.x,x.x*hh<CR><LF>
     * ------------------------------------------------------------------------------
     *
     * Field Number:
     *
     * 1. Selection mode: M=Manual, forced to operate in 2D or 3D, A=Automatic, 3D/2D
     * 2. Mode (1 = no fix, 2 = 2D fix, 3 = 3D fix)
     * 3. ID of 1st satellite used for fix
     * 4. ID of 2nd satellite used for fix
     * 5. ID of 3rd satellite used for fix
     * 6. ID of 4th satellite used for fix
     * 7. ID of 5th satellite used for fix
     * 8. ID of 6th satellite used for fix
     * 9. ID of 7th satellite used for fix
     * 10. ID of 8th satellite used for fix
     * 11. ID of 9th satellite used for fix
     * 12. ID of 10th satellite used for fix
     * 13. ID of 11th satellite used for fix
     * 14. ID of 12th satellite used for fix
     * 15. PDOP
     * 16. HDOP
     * 17. VDOP
     * 18. Checksum
     */
    if (18 > argc)
        return;
    /* mode */
    switch (argv[2][0])
    {
    case '2':
    case '3':
        break;
    case '1':
    default:
        _gpsData.mode = GpsData::ModeNoFix;
        break;
    }
    _gpsData.e_lat = _gpsData.e_lon = Base::None();
    if (argv[16][0])
    {
        double hdop;
        if (parseDouble(argv[16], hdop))
        {
            switch (_gpsData.mode)
            {
            case GpsData::ModeAutonomous:
                _gpsData.e_lat = _gpsData.e_lon = 15.0 * hdop;
                break;
            case GpsData::ModeDifferential:
                _gpsData.e_lat = _gpsData.e_lon = 3.75 * hdop;
                break;
            }
        }
    }
    _gpsData.e_alt = Base::None();
    if (argv[17][0])
    {
        double vdop;
        if (parseDouble(argv[17], vdop))
        {
            switch (_gpsData.mode)
            {
            case GpsData::ModeAutonomous:
                _gpsData.e_alt = 23.0 * vdop;
                break;
            case GpsData::ModeDifferential:
                _gpsData.e_alt = 5.75 * vdop;
                break;
            }
        }
    }
    maybeTriggerCycleEvent(tagGPGSA);
}
void GpsNmeaProcessor::parseGPGSV(const char *argv[], size_t argc)
{
    /* === GSV - Satellites in view ===
     *
     * These sentences describe the sky position of a UPS satellite in view.
     * Typically they're shipped in a group of 2 or 3.
     *
     * ------------------------------------------------------------------------------
     *         1 2 3 4 5 6 7     n
     *         | | | | | | |     |
     *  $--GSV,x,x,x,x,x,x,x,...*hh<CR><LF>
     * ------------------------------------------------------------------------------
     *
     * Field Number:
     *
     * 1. total number of GSV messages to be transmitted in this group
     * 2. 1-origin number of this GSV message  within current group
     * 3. total number of satellites in view (leading zeros sent)
     * 4. satellite PRN number (leading zeros sent)
     * 5. elevation in degrees (00-90) (leading zeros sent)
     * 6. azimuth in degrees to true north (000-359) (leading zeros sent)
     * 7. SNR in dB (00-99) (leading zeros sent)
     *    more satellite info quadruples like 4-7
     * n. checksum
     */
    if (4 > argc)
        return;
    _gpsSkyData.timestamp = _gpsData.timestamp;
    if (Base::isNone(_gpsSkyData.timestamp))
        return;
    int count, index;
    if (parseNatNum(argv[1], count) &&
        parseNatNum(argv[2], index))
    {
        if (1 == index)
            _gpsSkyData.satelliteCount = 0;
        for (size_t argi = 4; argc > argi + 3; argi += 4)
        {
            if (_gpsSkyData.satelliteCount == NELEMS(_gpsSkyData.satellites))
                break;
            int prn; double elevation, azimuth, snr;
            if (parseNatNum(argv[argi + 0], prn) &&
                parseDouble(argv[argi + 1], elevation) &&
                parseDouble(argv[argi + 2], azimuth) &&
                parseDouble(argv[argi + 3], snr))
            {
                _gpsSkyData.satellites[_gpsSkyData.satelliteCount++] = (GpsSkyData::Satellite)
                {
                    prn,
                    elevation,
                    azimuth,
                    snr
                };
            }
        }
        if (count == index)
            maybeTriggerCycleEvent(tagGPGSV);
    }
}
void GpsNmeaProcessor::parseGPRMC(const char *argv[], size_t argc)
{
    /* === RMC - Recommended Minimum Navigation Information ===
     *
     * ------------------------------------------------------------------------------
     *                                                             12
     *         1         2 3       4 5        6  7   8   9    10 11|  13
     *         |         | |       | |        |  |   |   |    |  | |  |
     *  $--RMC,hhmmss.ss,A,llll.ll,a,yyyyy.yy,a,x.x,x.x,xxxx,x.x,a,m,*hh<CR><LF>
     * ------------------------------------------------------------------------------
     *
     * Field Number:
     *
     * 1. UTC Time
     * 2. Status, V=Navigation receiver warning A=Valid
     * 3. Latitude
     * 4. N or S
     * 5. Longitude
     * 6. E or W
     * 7. Speed over ground, knots
     * 8. Track made good, degrees true
     * 9. Date, ddmmyy
     * 10. Magnetic Variation, degrees
     * 11. E or W
     * 12. FAA mode indicator (NMEA 2.3 and later)
     * 13. Checksum
     */
    if (12 > argc)
        return;
    if (13 <= argc)
        parseFAA(argv[12]);
    else
        parseStatus(argv[2]);
    /* timestamp/date, latitude, longitude, track, speed */
    parseTimestamp(argv[1], argv[9]);
    parseLatitude(argv[3], argv[4]);
    parseLongitude(argv[5], argv[6]);
    parseTrack(argv[8]);
    parseSpeed(argv[7], "N");
    maybeTriggerCycleEvent(tagGPRMC);
}
void GpsNmeaProcessor::parseGPVTG(const char *argv[], size_t argc)
{
    /* === VTG - Track made good and Ground speed ===
     *
     * ------------------------------------------------------------------------------
     *          1  2  3  4  5	6  7  8 9   10
     *          |  |  |  |  |	|  |  | |   |
     *  $--VTG,x.x,T,x.x,M,x.x,N,x.x,K,m,*hh<CR><LF>
     * ------------------------------------------------------------------------------
     *
     * Field Number:
     *
     * 1. Track Degrees
     * 2. T = True
     * 3. Track Degrees
     * 4. M = Magnetic
     * 5. Speed Knots
     * 6. N = Knots
     * 7. Speed Kilometers Per Hour
     * 8. K = Kilometers Per Hour
     * 9. FAA mode indicator (NMEA 2.3 and later)
     * 10. Checksum
     *
     * Note: in some older versions of NMEA 0183, the sentence looks like this:
     *
     * ------------------------------------------------------------------------------
     *          1  2  3   4  5
     *          |  |  |   |  |
     *  $--VTG,x.x,x,x.x,x.x,*hh<CR><LF>
     * ------------------------------------------------------------------------------
     *
     * Field Number:
     *
     * 1. True course over ground (degrees) 000 to 359
     * 2. Magnetic course over ground 000 to 359
     * 3. Speed over ground (knots) 00.0 to 99.9
     * 4. Speed over ground (kilometers) 00.0 to 99.9
     * 5. Checksum
     *
     * The two forms can be distinguished by field 2, which will be
     * the fixed text 'T' in the newer form.  The new form appears
     * to have been introduced with NMEA 3.01 in 2002.
     */
    if (5 > argc)
        return;
    if (10 <= argc)
        parseFAA(argv[9]);
    if ('T' == argv[2][0])
    {
        if (9 > argc)
            return;
        /* new form: track, speed */
        parseTrack(argv[1]);
        parseSpeed(argv[7], argv[8]);
    }
    else
    {
        /* old form: track, speed */
        parseTrack(argv[1]);
        parseSpeed(argv[4], "K");
    }
    maybeTriggerCycleEvent(tagGPVTG);
}

}
}
}
}
#endif // SENSORKIT_CONFIG_NMEA
