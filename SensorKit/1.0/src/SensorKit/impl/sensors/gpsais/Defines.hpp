/*
 * SensorKit/impl/sensors/gpsais/Defines.hpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef SENSORKIT_IMPL_SENSORS_GPSAIS_DEFINES_HPP_INCLUDED
#define SENSORKIT_IMPL_SENSORS_GPSAIS_DEFINES_HPP_INCLUDED

#include <SensorKit/impl/Defines.hpp>
#include <SensorKit/impl/Sensor.hpp>

namespace SensorKit {
namespace impl {
namespace sensors {
namespace gpsais {

enum
{
    GpsDataStruct = 1,
    GpsSkyDataStruct,
    GpsDataAndSkyDataStruct,
    CompassDataStruct,
    AisDataStruct,
    AisVesselDataStruct,
};

struct GpsDataAndSkyData
{
    const GpsData *gpsData;
    const GpsSkyData *gpsSkyData;
};

}
}
}
}

#endif // SENSORKIT_IMPL_SENSORS_GPSAIS_DEFINES_HPP_INCLUDED
