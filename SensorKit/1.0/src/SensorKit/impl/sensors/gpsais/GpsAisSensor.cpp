/*
 * SensorKit/impl/sensors/gpsais/GpsAisSensor.cpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#include <SensorKit/impl/sensors/gpsais/GpsAisSensor.hpp>
#if defined(SENSORKIT_CONFIG_NMEA) || defined(SENSORKIT_CONFIG_GPSD)
#include <SensorKit/impl/sensors/gpsais/AisNmeaProcessor.hpp>
#include <SensorKit/impl/sensors/gpsais/GpsNmeaProcessor.hpp>
#include <SensorKit/impl/sensors/gpsais/GpsdParser.hpp>
#include <SensorKit/impl/sensors/gpsais/NmeaParser.hpp>
#include <SensorKit/impl/sensors/shared/SerialStream.hpp>
#include <SensorKit/impl/sensors/shared/SocketStream.hpp>

namespace SensorKit {
namespace impl {
namespace sensors {
namespace gpsais {

#define SENSOR_TIMER_PERIOD             2000 /* 2 sec */
#define AIS_REPORTS_MAX                 1000

/*
 * GpsAisSensorHandle
 */
class GpsAisSensorHandle : public SensorHandle,
    public impl::GpsSensorHandle, public impl::GpsSkySensorHandle,
    public impl::AisSensorHandle,
    public impl::ErrorSensorHandle
{
public:
    GpsAisSensorHandle(GpsAisSensor *sensor);
    int _open(Sensor::kind_t kind);
    int _close(Sensor::kind_t kind);
    GpsData gpsData();
    GpsSkyData gpsSkyData();
    void aisData(const AisData **pDataArray, const AisVesselData **pVesselDataArray);
    Base::Error *error();
    void _triggerEvent(Sensor::kind_t evkind);
private:
    void _triggerEventOnHandleThread(Object *obj);
    struct Event : public Base::Object
    {
        Event(Sensor::kind_t kind_)
        {
            threadsafe();
            kind = kind_;
        }
        Sensor::kind_t kind;
    };
private:
    Base::Thread *_thread; /* weak */
};
GpsAisSensorHandle::GpsAisSensorHandle(GpsAisSensor *sensor) : SensorHandle(sensor)
{
}
int GpsAisSensorHandle::_open(Sensor::kind_t kind)
{
    if (0 == openKind())
    {
        _thread = Base::Thread::currentThread();
        if (!((GpsAisSensor *)sensor())->addHandle(this))
        {
            _thread = 0;
            return -1;
        }
    }
    return 0;
}
int GpsAisSensorHandle::_close(Sensor::kind_t kind)
{
    if (0 == openKind())
    {
        ((GpsAisSensor *)sensor())->removeHandle(this);
        Base::ThreadLoopCancelInvoke(_thread, this);
        _thread = 0;
    }
    return 0;
}
GpsData GpsAisSensorHandle::gpsData()
{
    return ((GpsAisSensor *)sensor())->gpsData();
}
GpsSkyData GpsAisSensorHandle::gpsSkyData()
{
    return ((GpsAisSensor *)sensor())->gpsSkyData();
}
void GpsAisSensorHandle::aisData(const AisData **pDataArray, const AisVesselData **pVesselDataArray)
{
    return ((GpsAisSensor *)sensor())->aisData(pDataArray, pVesselDataArray);
}
Base::Error *GpsAisSensorHandle::error()
{
    return ((GpsAisSensor *)sensor())->error();
}
void GpsAisSensorHandle::_triggerEvent(Sensor::kind_t evkind)
{
    if (Base::Thread::currentThread() == _thread)
    {
        if (Sensor::kind_t kind = evkind & openKind())
            /*
             * We must do the openKind() test in the handle thread rather than the sensor thread
             * to avoid problems with open() and close() calls on the handle thread.
             */
            if (Delegate delegate = this->delegate())
                delegate(this, (evkind & Sensor::KindError) | kind);
    }
    else
    {
        Event *event = new Event(evkind);
        if (!Base::ThreadLoopInvoke(
            _thread, make_delegate(&GpsAisSensorHandle::_triggerEventOnHandleThread, this), event))
            LOG("no thread loop on thread %p; event will not be delivered", _thread);
        event->release();
    }
}
void GpsAisSensorHandle::_triggerEventOnHandleThread(Base::Object *obj)
{
    assert(Base::Thread::currentThread() == _thread);
    Event *event = (Event *)obj;
    _triggerEvent(event->kind);
}

/*
 * GpsAisSensorThread
 */
class GpsAisSensorThread : public Base::Thread
{
public:
    GpsAisSensorThread(GpsAisSensor *sensor);
    ~GpsAisSensorThread();
    void quit();
protected:
    void run();
private:
    void handleError(const char *scheme, unsigned *pattempts, const char *s);
    void reportError(const char *s);
    void gpsaisEvent(unsigned structKind, const void *structBuf);
private:
    GpsAisSensor *_sensor;
    Base::Lock *_lock;
    /* maintained in run() */
    Base::SystemStream *_stream;
};
GpsAisSensorThread::GpsAisSensorThread(GpsAisSensor *sensor)
{
    sensor->retain();
    _sensor = sensor;
    _lock = new Base::Lock;
}
GpsAisSensorThread::~GpsAisSensorThread()
{
    _lock->release();
    _sensor->release();
}
void GpsAisSensorThread::quit()
{
    Thread::quit();
    synchronized (_lock)
        if (0 != _stream)
            _stream->cancel();
}
void GpsAisSensorThread::run()
{
    mark_and_collect
    {
        assert('N'/*NMEA*/ == _sensor->_protocol || 'D'/*GPSD*/ == _sensor->_protocol);
        Base::cstr_uri_parts parts;
        Base::cstr_uri_parse(_sensor->uri(), &parts);
        assert(0 != parts.scheme);
        assert(0 == strcmp("file", parts.scheme) || 0 == strcmp("tcp", parts.scheme));
        assert(0 != parts.path || (0 != parts.host && 0 != parts.port));
        unsigned openAttempts = 0;
        while (!isQuitting())
        {
            /* open serial device or TCP endpoint */
            Base::SystemStream *stream = 0;
            switch (parts.scheme[0])
            {
            case 'f':
                stream = sensors::shared::SerialStream::create(&parts, "rb");
                break;
            case 't':
                stream = sensors::shared::SocketStream::create(&parts);
                break;
            default:
                assert(false);
                break;
            }
            if (0 == stream)
            {
                if (isQuitting())
                    break;
                handleError(parts.scheme, &openAttempts, "Cannot open");
                continue;
            }
            openAttempts = 0;
            synchronized (_lock)
                _stream = stream;
            switch (_sensor->_protocol)
            {
#if defined(SENSORKIT_CONFIG_NMEA)
            case 'N'/*NMEA*/:
                {
                    NmeaParser *parser = new NmeaParser(_stream);
                    GpsNmeaProcessor *gpsProcessor =
                        new GpsNmeaProcessor(parser, make_delegate(&GpsAisSensorThread::gpsaisEvent, this));
                    AisNmeaProcessor *aisProcessor =
                        new AisNmeaProcessor(parser, make_delegate(&GpsAisSensorThread::gpsaisEvent, this));
                    /* while we have not been asked to quit, keep parsing */
                    while (!isQuitting() && 0 < parser->parse())
                        ;
                    if (!isQuitting())
                        handleError(parts.scheme, 0, "Cannot read");
                    aisProcessor->release();
                    gpsProcessor->release();
                    parser->release();
                }
                break;
#endif
#if defined(SENSORKIT_CONFIG_GPSD)
            case 'D'/*GPSD*/:
                {
                    static const char *watchCommand = "?WATCH={\"enable\":true,\"json\":true}";
                    if (-1 == _stream->write(watchCommand, strlen(watchCommand)))
                    {
                        handleError(parts.scheme, 0, "Cannot write");
                        break;
                    }
                    GpsdParser *parser =
                        new GpsdParser(_stream, make_delegate(&GpsAisSensorThread::gpsaisEvent, this));
                    /* while we have not been asked to quit, keep parsing */
                    while (!isQuitting() && 0 < parser->parse())
                        ;
                    if (!isQuitting())
                        handleError(parts.scheme, 0, "Cannot read");
                    parser->release();
                }
                break;
#endif
            }
            synchronized (_lock)
                _stream = 0;
            stream->close();
            stream->release();
        }
    }
}
void GpsAisSensorThread::handleError(const char *scheme, unsigned *pattempts, const char *s)
{
    mark_and_collect
    {
        Base::Error *error = Base::Error::lastError();
        if (0 == pattempts || 0 == (*pattempts)++)
            reportError(s);
        unsigned sleep;
        switch (scheme[0])
        {
        case 'f':
            sleep = Base::Random::integerValue(1500, 3000);
            break;
        case 't':
            if (0 == strcmp(error->domain(), "getaddrinfo"))
                sleep = Base::Random::integerValue(30000, 60000);
            else
                sleep = Base::Random::integerValue(7000, 10000);
            break;
        default:
            assert(false);
            break;
        }
        while (!isQuitting() && 0 < sleep)
        {
            unsigned minisleep = 1000 > sleep ? sleep : 1000;
            Thread::sleep(minisleep);
            sleep -= minisleep;
        }
    }
}
void GpsAisSensorThread::reportError(const char *s)
{
    char message[1024];
    mark_and_collect
    {
        if (Base::Error *error = Base::Error::lastError())
            portable_snprintf(message, sizeof message,
                "%s %s: %s", s, _sensor->uri(), error->message());
        else
            portable_snprintf(message, sizeof message,
                "%s %s", s, _sensor->uri());
    }
    _sensor->errorEvent(this, GpsAisSensorHandle::ErrorInformational, message);
}
void GpsAisSensorThread::gpsaisEvent(unsigned structKind, const void *structBuf)
{
    _sensor->gpsaisEvent(this, structKind, structBuf);
}

/*
 * GpsAisSensor
 */
#if defined(SENSORKIT_CONFIG_NMEA)
Sensor *GpsAisSensor::createNmea(Base::Map *params)
{
    if (0 != params)
    {
        static Base::CString *uriKey = Base::CStringConstant("uri");
        if (Base::CString *value = dynamic_cast<Base::CString *>(params->object(uriKey)))
            return new GpsAisSensor('N'/*NMEA*/, value->chars());
    }
    return 0;
}
#endif
#if defined(SENSORKIT_CONFIG_GPSD)
Sensor *GpsAisSensor::createGpsd(Base::Map *params)
{
    if (0 != params)
    {
        static Base::CString *uriKey = Base::CStringConstant("uri");
        if (Base::CString *value = dynamic_cast<Base::CString *>(params->object(uriKey)))
            return new GpsAisSensor('D'/*GPSD*/, value->chars());
    }
    return 0;
}
#endif
GpsAisSensor::GpsAisSensor(unsigned protocol, const char *uri)
{
    _protocol = protocol;
    _uri = Base::cstr_new(uri);
    _lock = new Base::Lock;
    _cycleGpsData = GpsDataNone();
    _cycleGpsSkyData = GpsSkyDataNone();
}
GpsAisSensor::~GpsAisSensor()
{
    assert(0 == _handles && 0 == _thread && 0 == _timer);
    if (0 != _cycleError)
        _cycleError->release();
    Base::carr_release(_cycleAisVesselDataArray);
    Base::carr_release(_aisVesselDataArray);
    Base::carr_release(_cycleAisDataArray);
    Base::carr_release(_aisDataArray);
    _lock->release();
    Base::cstr_release(_uri);
}
const char *GpsAisSensor::uri()
{
    return _uri;
}
Sensor::kind_t GpsAisSensor::kind()
{
    return KindGps | KindGpsSky | KindAis;
}
SensorHandle *GpsAisSensor::createHandle()
{
    return new GpsAisSensorHandle(this);
}
const char *GpsAisSensor::description()
{
    return Base::cstringf("GPSAIS-NMEA: %s", _uri);
}
/* GpsAisSensorHandle helpers */
bool GpsAisSensor::addHandle(GpsAisSensorHandle *handle)
{
    synchronized (_lock)
    {
        if (0 == _handles)
        {
            Base::cstr_uri_parts parts;
            Base::cstr_uri_parse(_uri, &parts);
            if (0 == parts.scheme)
                return false;
            else if (0 == strcmp("file", parts.scheme))
            {
                if (0 == parts.path)
                    return false;
#if defined(BASE_CONFIG_WINDOWS)
                if (-1 == _access(parts.path, 04))
#elif defined(BASE_CONFIG_POSIX)
                if (-1 == access(parts.path, R_OK))
#endif
                    return false;
            }
            else if (0 == strcmp("tcp", parts.scheme))
            {
                if (0 == parts.host || 0 == parts.port)
                    return false;
            }
            else
                return false;
            _thread = new GpsAisSensorThread(this);
            _thread->start();
            _timer = Base::ThreadLoopSetTimer(
                SENSOR_TIMER_PERIOD, make_delegate(&GpsAisSensor::timerEvent, this), 0);
        }
        _handles = (GpsAisSensorHandle **)Base::carr_concat(_handles, &handle, 1, sizeof(GpsAisSensorHandle *));
    }
    return true;
}
void GpsAisSensor::removeHandle(GpsAisSensorHandle *handle)
{
    synchronized (_lock)
    {
        size_t count = Base::carr_length(_handles);
        GpsAisSensorHandle **p = _handles, **endp = p + count;
        /* find and remove handle */
        for (; endp > p; p++)
            if (*p == handle)
                break;
        if (endp > p)
        {
            /* if found (and removed) handle */
            memmove(p, p + 1, (endp - p - 1) * sizeof(GpsAisSensorHandle *));
            Base::carr_obj(_handles)->setLength(count - 1);
            if (1 == count)
            {
                /* this was the last handle; instruct the sensor thread to quit */
                Base::ThreadLoopCancelTimer(_timer);
                _timer = 0;
                _thread->quit();
                _thread->detach();
                _thread->release();
                _thread = 0;
                Base::carr_release(_handles);
                _handles = 0;
            }
        }
    }
}
GpsData GpsAisSensor::gpsData()
{
    synchronized (_lock)
        return _cycleGpsData;
}
GpsSkyData GpsAisSensor::gpsSkyData()
{
    synchronized (_lock)
        return _cycleGpsSkyData;
}
void GpsAisSensor::aisData(const AisData **pDataArray, const AisVesselData **pVesselDataArray)
{
    synchronized (_lock)
    {
        if (0 != pDataArray)
        {
            *pDataArray = _cycleAisDataArray;
            Base::carr_autocollect(_cycleAisDataArray);
        }
        if (0 != pVesselDataArray)
        {
            *pVesselDataArray = _cycleAisVesselDataArray;
            Base::carr_autocollect(_cycleAisVesselDataArray);
        }
    }
}
Base::Error *GpsAisSensor::error()
{
    synchronized (_lock)
    {
        if (0 != _cycleError)
            _cycleError->autocollect();
        return _cycleError;
    }
}
void GpsAisSensor::timerEvent(Base::Object *obj)
{
    /* executed in the context of the handle (open) thread */
    synchronized (_lock)
    {
        Base::carr_release(_cycleAisDataArray);
        _cycleAisDataArray = _aisDataArray;
        _aisDataArray = 0;
        Base::carr_release(_cycleAisVesselDataArray);
        _cycleAisVesselDataArray = _aisVesselDataArray;
        _aisVesselDataArray = 0;
        if (0 < Base::carr_length(_cycleAisDataArray) || 0 < Base::carr_length(_cycleAisVesselDataArray))
            for (GpsAisSensorHandle **p = _handles, **endp = p + Base::carr_length(_handles); endp > p; p++)
            {
                GpsAisSensorHandle *handle = *p;
                handle->_triggerEvent(KindAis);
            }
    }
}
/* GpsAisSensorThread helpers */
void GpsAisSensor::gpsaisEvent(GpsAisSensorThread *thread, unsigned structKind, const void *structBuf)
{
    /* executed in the context of the sensor thread */
    synchronized (_lock)
    {
        if (_thread != thread)
            return; /* stale thread; ignore */
        Sensor::kind_t evkind = 0;
        switch (structKind)
        {
        case GpsDataStruct:
            _cycleGpsData = *(GpsData *)structBuf;
            evkind = KindGps;
            break;
        case GpsSkyDataStruct:
            _cycleGpsSkyData = *(GpsSkyData *)structBuf;
            evkind = KindGpsSky;
            break;
        case GpsDataAndSkyDataStruct:
            _cycleGpsData = *((GpsDataAndSkyData *)structBuf)->gpsData;
            _cycleGpsSkyData = *((GpsDataAndSkyData *)structBuf)->gpsSkyData;
            evkind = KindGps | KindGpsSky;
            break;
        case AisDataStruct:
            if (AIS_REPORTS_MAX > Base::carr_length(_aisDataArray))
                _aisDataArray = (AisData *)Base::carr_concat(_aisDataArray,
                    structBuf, 1, sizeof(AisData));
            /* AIS events are triggered by timer */
            break;
        case AisVesselDataStruct:
            if (AIS_REPORTS_MAX > Base::carr_length(_aisVesselDataArray))
                _aisVesselDataArray = (AisVesselData *)Base::carr_concat(_aisVesselDataArray,
                    structBuf, 1, sizeof(AisVesselData));
            /* AIS events are triggered by timer */
            break;
        }
        if (0 != evkind)
            for (GpsAisSensorHandle **p = _handles, **endp = p + Base::carr_length(_handles); endp > p; p++)
            {
                GpsAisSensorHandle *handle = *p;
                handle->_triggerEvent(evkind);
            }
    }
}
void GpsAisSensor::errorEvent(GpsAisSensorThread *thread, int code, const char *message)
{
    /* executed in the context of the sensor thread */
    synchronized (_lock)
    {
        if (_thread != thread)
            return; /* stale thread; ignore */
        if (0 != _cycleError)
            _cycleError->release();
        _cycleError = new Base::Error(GpsAisSensorHandle::errorDomain(), code, message);
        _cycleError->threadsafe();
        for (GpsAisSensorHandle **p = _handles, **endp = p + Base::carr_length(_handles); endp > p; p++)
        {
            GpsAisSensorHandle *handle = *p;
            handle->_triggerEvent(KindError | kind());
        }
    }
}

}
}
}
}
#endif // defined(SENSORKIT_CONFIG_NMEA) || defined(SENSORKIT_CONFIG_GPSD)
