/*
 * SensorKit/impl/sensors/gpsais/GpsdParser.cpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#include <SensorKit/impl/sensors/gpsais/GpsdParser.hpp>
#if defined(SENSORKIT_CONFIG_GPSD)
#include "gps.h"
#include "gps_json.h"

namespace SensorKit {
namespace impl {
namespace sensors {
namespace gpsais {

GpsdParser::GpsdParser(Base::Stream *stream, Delegate delegate) :
    Base::TextParser(stream, GPS_JSON_RESPONSE_MAX * 2)
{
    _delegate = delegate;
}
void GpsdParser::processData(const void *buf)
{
    const gps_data_t *libgps_data = (const gps_data_t *)buf;
    if (libgps_data->set & STATUS_SET)
    {
        GpsData gpsData = GpsDataNone();
        if (libgps_data->set & MODE_SET)
            switch (libgps_data->fix.mode)
            {
            case MODE_NOT_SEEN:
            default:
                gpsData.mode = GpsData::ModeUnknown;
                break;
            case MODE_NO_FIX:
                gpsData.mode = GpsData::ModeNoFix;
                break;
            case MODE_2D:
                gpsData.mode = GpsData::ModeAutonomous;
                break;
            case MODE_3D:
                gpsData.mode = GpsData::ModeDifferential;
                break;
            }
        if (libgps_data->set & TIME_SET)
            gpsData.timestamp = 2440587.5 + libgps_data->fix.time / 86400.0;
        if (libgps_data->set & LATLON_SET)
        {
            gpsData.lat = libgps_data->fix.latitude;
            gpsData.lon = libgps_data->fix.longitude;
        }
        if (libgps_data->set & ALTITUDE_SET)
            gpsData.alt = libgps_data->fix.altitude;
        if (libgps_data->set & HERR_SET)
        {
            gpsData.e_lat = libgps_data->fix.epy;
            gpsData.e_lon = libgps_data->fix.epx;
        }
        if (libgps_data->set & VERR_SET)
            gpsData.e_alt = libgps_data->fix.epv;
        if (libgps_data->set & TRACK_SET)
            gpsData.track = libgps_data->fix.track;
        if (libgps_data->set & SPEED_SET)
            gpsData.speed = libgps_data->fix.speed;
        if (libgps_data->set & CLIMB_SET)
            gpsData.climb = libgps_data->fix.climb;
        if (libgps_data->set & TRACKERR_SET)
            gpsData.e_track = libgps_data->fix.epd;
        if (libgps_data->set & SPEEDERR_SET)
            gpsData.e_speed = libgps_data->fix.eps;
        if (libgps_data->set & CLIMBERR_SET)
            gpsData.e_climb = libgps_data->fix.epc;
        _delegate(GpsDataStruct, &gpsData);
    }
    if (libgps_data->set & SATELLITE_SET)
    {
        /* ignored for now */
    }
    if (libgps_data->set & ATTITUDE_SET)
    {
        /* ignored for now */
    }
    if (libgps_data->set & AIS_SET)
    {
        uint32_t type = libgps_data->ais.type;
        //uint32_t repeat = libgps_data->ais.repeat;
        uint32_t mmsi = libgps_data->ais.mmsi;
        switch (type)
        {
        case 1: case 2: case 3:
        case 18: case 19:
            {
                uint32_t status, speed, accuracy, course, heading, second;
                int32_t lon, lat, turn;
                if (1 == type || 2 == type || 3 == type)
                {
                    status = libgps_data->ais.type1.status;
                    turn = libgps_data->ais.type1.turn;
                    speed = libgps_data->ais.type1.speed;
                    accuracy = libgps_data->ais.type1.accuracy;
                    lon = libgps_data->ais.type1.lon;
                    lat = libgps_data->ais.type1.lat;
                    course = libgps_data->ais.type1.course;
                    heading = libgps_data->ais.type1.heading;
                    second = libgps_data->ais.type1.second;
                }
                else /* types 18, 19 */
                {
                    if (18 == type)
                    {
                        status = AisData::StatusUnknown;
                        turn = -128;
                        speed = libgps_data->ais.type18.speed;
                        accuracy = libgps_data->ais.type18.accuracy;
                        lon = libgps_data->ais.type18.lon;
                        lat = libgps_data->ais.type18.lat;
                        course = libgps_data->ais.type18.course;
                        heading = libgps_data->ais.type18.heading;
                        second = libgps_data->ais.type18.second;
                    }
                    else /* type 19 */
                    {
                        status = AisData::StatusUnknown;
                        turn = -128;
                        speed = libgps_data->ais.type19.speed;
                        accuracy = libgps_data->ais.type19.accuracy;
                        lon = libgps_data->ais.type19.lon;
                        lat = libgps_data->ais.type19.lat;
                        course = libgps_data->ais.type19.course;
                        heading = libgps_data->ais.type19.heading;
                        second = libgps_data->ais.type19.second;
                    }
                }
                AisData aisData = AisDataNone();
                aisData.mmsi = mmsi;
                aisData.status = status;
                if (15 <= aisData.status)
                    aisData.status = AisData::StatusUnknown;
                if (60 <= second)
                    aisData.timestamp = Base::DateTime::currentJulianDate();
                else
                {
                    double currentJulianDate = Base::DateTime::currentJulianDate();
                    Base::DateTime::Components components =
                        Base::DateTime::componentsFromJulianDate(currentJulianDate);
                    components.sec = second;
                    components.msec = 0;
                    double computedJulianDate = Base::DateTime::julianDateFromComponents(components);
                    if (computedJulianDate > currentJulianDate)
                        /* Assuming that all ship clocks are synchronized,
                         * the computedJulianDate cannot be later than our time.
                         * If that was the case, the reported second was in the
                         * previous minute.
                         */
                        computedJulianDate -= 1.0 / (24 * 60);
                    aisData.timestamp = computedJulianDate;
                }
                aisData.lat = lat / 600000.0;
                if (-90.0 > aisData.lat || aisData.lat > +90.0)
                    aisData.lat = Base::None();
                aisData.lon = lon / 600000.0;
                if (-180.0 > aisData.lon || aisData.lon > +180.0)
                    aisData.lon = Base::None();
                if (accuracy)
                    aisData.e_lat = aisData.e_lon = 3.75;
                else
                    aisData.e_lat = aisData.e_lon = 15.0;
                aisData.track = course / 10.0;
                if (+360.0 <= aisData.track)
                    aisData.track = Base::None();
                if (+1023 == speed)
                    aisData.speed = Base::None();
                else if (+1022 == speed)
                    aisData.speed = +INFINITY;
                else
                    aisData.speed = speed * (0.514444 / 10);  /* kn tenths -> m/s */
                if (-128 == turn)
                    aisData.turn = Base::None();
                else if (-127 == turn)
                    aisData.turn = -INFINITY;
                else if (+127 == turn)
                    aisData.turn = +INFINITY;
                else
                {
                    aisData.turn = turn / 4.733;
                    aisData.turn *= aisData.turn;
                    if (0 > turn)
                        aisData.turn = -aisData.turn;
                }
                aisData.trueHeading = heading;
                if (+360.0 <= aisData.trueHeading)
                    aisData.trueHeading = Base::None();
                _delegate(AisDataStruct, &aisData);
                if (19 == type)
                {
                    uint32_t shiptype = libgps_data->ais.type19.shiptype;
                    uint32_t to_bow = libgps_data->ais.type19.to_bow;
                    uint32_t to_stern = libgps_data->ais.type19.to_stern;
                    uint32_t to_port = libgps_data->ais.type19.to_port;
                    uint32_t to_starboard = libgps_data->ais.type19.to_starboard;
                    uint32_t epfd = libgps_data->ais.type19.epfd;
                    AisVesselData aisVesselData = AisVesselDataNone();
                    aisVesselData.mmsi = mmsi;
                    assert(sizeof aisVesselData.name == sizeof libgps_data->ais.type19.shipname);
                    memcpy(aisVesselData.name, libgps_data->ais.type19.shipname, sizeof aisVesselData.name);
                    aisVesselData.type = shiptype;
                    if (100 <= aisVesselData.type)
                        aisVesselData.type = AisVesselData::TypeUnknown;
                    aisVesselData.length = to_bow + to_stern;
                    aisVesselData.beam = to_port + to_starboard;
                    aisVesselData.antennaLongitudinalPosition = (double)to_bow / aisVesselData.length;
                    aisVesselData.antennaLateralPosition = (double)to_port / aisVesselData.beam;
                    aisVesselData.epfd = epfd;
                    if (9 <= aisVesselData.epfd)
                        aisVesselData.epfd = AisVesselData::EpfdUnknown;
                    _delegate(AisVesselDataStruct, &aisVesselData);
                }
                break;
            }
        case 5:
            {
                uint32_t imo = libgps_data->ais.type5.imo;
                uint32_t shiptype = libgps_data->ais.type5.shiptype;
                uint32_t to_bow = libgps_data->ais.type5.to_bow;
                uint32_t to_stern = libgps_data->ais.type5.to_stern;
                uint32_t to_port = libgps_data->ais.type5.to_port;
                uint32_t to_starboard = libgps_data->ais.type5.to_starboard;
                uint32_t epfd = libgps_data->ais.type5.epfd;
                uint32_t month = libgps_data->ais.type5.month;
                uint32_t day = libgps_data->ais.type5.day;
                uint32_t hour = libgps_data->ais.type5.hour;
                uint32_t minute = libgps_data->ais.type5.minute;
                uint32_t draft = libgps_data->ais.type5.draught;
                AisVesselData aisVesselData = AisVesselDataNone();
                aisVesselData.mmsi = mmsi;
                aisVesselData.imo = imo;
                assert(sizeof aisVesselData.callsign == sizeof libgps_data->ais.type5.callsign);
                memcpy(aisVesselData.callsign, libgps_data->ais.type5.callsign, sizeof aisVesselData.callsign);
                assert(sizeof aisVesselData.name == sizeof libgps_data->ais.type5.shipname);
                memcpy(aisVesselData.name, libgps_data->ais.type5.shipname, sizeof aisVesselData.name);
                aisVesselData.type = shiptype;
                if (100 <= aisVesselData.type)
                    aisVesselData.type = AisVesselData::TypeUnknown;
                aisVesselData.length = to_bow + to_stern;
                aisVesselData.beam = to_port + to_starboard;
                aisVesselData.draft = draft / 10.0;
                aisVesselData.antennaLongitudinalPosition = (double)to_bow / aisVesselData.length;
                aisVesselData.antennaLateralPosition = (double)to_port / aisVesselData.beam;
                aisVesselData.epfd = epfd;
                if (9 <= aisVesselData.epfd)
                    aisVesselData.epfd = AisVesselData::EpfdUnknown;
                assert(sizeof aisVesselData.destination == sizeof libgps_data->ais.type5.destination);
                memcpy(aisVesselData.destination, libgps_data->ais.type5.destination, sizeof aisVesselData.destination);
                if (0 == month || month > 12)
                    aisVesselData.eta = Base::None();
                else if (0 == day || day > 31)
                    aisVesselData.eta = Base::None();
                else
                {
                    double currentJulianDate = round(Base::DateTime::currentJulianDate());
                    Base::DateTime::Components components =
                        Base::DateTime::componentsFromJulianDate(currentJulianDate);
                    components.month = month;
                    components.day = day;
                    double computedJulianDate = Base::DateTime::julianDateFromComponents(components);
                    if (computedJulianDate < currentJulianDate)
                        /* Assuming that all ship clocks are synchronized,
                         * the computedJulianDate cannot be less than the
                         * current date (since it must be an ETA in the future).
                         * If that is the case the ETA must be next year.
                         */
                        components.year++;
                    if (23 >= hour)
                    {
                        components.hour = hour;
                        if (59 >= minute)
                            components.min = minute;
                    }
                    aisVesselData.eta = Base::DateTime::julianDateFromComponents(components);
                }
                _delegate(AisVesselDataStruct, &aisVesselData);
                break;
            }
        case 24:
            {
                AisVesselData aisVesselData = AisVesselDataNone();
                aisVesselData.mmsi = mmsi;
                aisVesselData.type = libgps_data->ais.type24.shiptype;
                if (100 <= aisVesselData.type)
                    aisVesselData.type = AisVesselData::TypeUnknown;
                assert(sizeof aisVesselData.callsign == sizeof libgps_data->ais.type24.callsign);
                memcpy(aisVesselData.callsign, libgps_data->ais.type24.callsign, sizeof aisVesselData.callsign);
                assert(sizeof aisVesselData.name == sizeof libgps_data->ais.type24.shipname);
                memcpy(aisVesselData.name, libgps_data->ais.type24.shipname, sizeof aisVesselData.name);
                if (mmsi / 10000000 != 98)
                {
                    /* MMSI is primary */
                    uint32_t to_bow = libgps_data->ais.type24.dim.to_bow;
                    uint32_t to_stern = libgps_data->ais.type24.dim.to_stern;
                    uint32_t to_port = libgps_data->ais.type24.dim.to_port;
                    uint32_t to_starboard = libgps_data->ais.type24.dim.to_starboard;
                    aisVesselData.length = to_bow + to_stern;
                    aisVesselData.beam = to_port + to_starboard;
                    aisVesselData.antennaLongitudinalPosition = (double)to_bow / aisVesselData.length;
                    aisVesselData.antennaLateralPosition = (double)to_port / aisVesselData.beam;
                }
                else
                {
                    /* MMSI is of a vessel associated with a parent ship */
                    /* nothing to do */
                }
                _delegate(AisVesselDataStruct, &aisVesselData);
                break;
            }
        }
    }
}
void GpsdParser::parseBuffer(bool eof)
{
    for (;;)
    {
        char *startp = _charptr();
        char *endp = startp;
        for (;;)
        {
            if (!_testptr(endp))
                raise();
            if ('\n' == *endp++)
            {
                endp[-1] = '\0';
                break;
            }
        }
        for (const char *p = startp; 0 != p && '\0' != *p;)
        {
            gps_data_t libgps_data = { 0 };
            if (libgps_json_unpack(p, &libgps_data, &p) == -1)
                break;
            processData(&libgps_data);
        }
        skipnext(endp - startp);
    }
}

}
}
}
}
#endif // SENSORKIT_CONFIG_GPSD
