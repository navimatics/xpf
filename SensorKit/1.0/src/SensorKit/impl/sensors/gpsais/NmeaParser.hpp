/*
 * SensorKit/impl/sensors/gpsais/NmeaParser.hpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef SENSORKIT_IMPL_SENSORS_GPSAIS_NMEAPARSER_HPP_INCLUDED
#define SENSORKIT_IMPL_SENSORS_GPSAIS_NMEAPARSER_HPP_INCLUDED

#include <SensorKit/impl/Defines.hpp>
#if defined(SENSORKIT_CONFIG_NMEA)
#include <SensorKit/impl/sensors/gpsais/Defines.hpp>

namespace SensorKit {
namespace impl {
namespace sensors {
namespace gpsais {

class NmeaParser : public Base::TextParser
{
public:
    typedef Base::Delegate<void (const char *argv[], size_t argc)> Delegate;
    NmeaParser(Base::Stream *stream = 0);
    ~NmeaParser();
    void addDelegate(const char *tag, Delegate delegate);
    void removeDelegate(const char *tag);
protected:
    virtual void line(const char *argv[], size_t argc);
    void parseBuffer(bool eof);
private:
    typedef Base::GenericDelegateMap<Base::Object, void (const char *argv[], size_t argc)> DelegateMap;
    DelegateMap *_delegates;
};

}
}
}
}
#endif // SENSORKIT_CONFIG_NMEA

#endif // SENSORKIT_IMPL_SENSORS_GPSAIS_NMEAPARSER_HPP_INCLUDED
