/*
 * SensorKit/impl/sensors/gpsais/GpsdParser.hpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef SENSORKIT_IMPL_SENSORS_GPSAIS_GPSDPARSER_HPP_INCLUDED
#define SENSORKIT_IMPL_SENSORS_GPSAIS_GPSDPARSER_HPP_INCLUDED

#include <SensorKit/impl/Defines.hpp>
#if defined(SENSORKIT_CONFIG_GPSD)
#include <SensorKit/impl/sensors/gpsais/Defines.hpp>

namespace SensorKit {
namespace impl {
namespace sensors {
namespace gpsais {

class GpsdParser : public Base::TextParser
{
public:
    typedef Base::Delegate<void (unsigned, const void *)> Delegate;
    GpsdParser(Base::Stream *stream, Delegate delegate);
protected:
    virtual void processData(const void *buf);
    void parseBuffer(bool eof);
private:
    Delegate _delegate;
};

}
}
}
}
#endif // SENSORKIT_CONFIG_GPSD

#endif // SENSORKIT_IMPL_SENSORS_GPSAIS_GPSDPARSER_HPP_INCLUDED
