/*
 * SensorKit/impl/sensors/gpsais/AisNmeaProcessor.hpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef SENSORKIT_IMPL_SENSORS_GPSAIS_AISNMEAPROCESSOR_HPP_INCLUDED
#define SENSORKIT_IMPL_SENSORS_GPSAIS_AISNMEAPROCESSOR_HPP_INCLUDED

#include <SensorKit/impl/Defines.hpp>
#if defined(SENSORKIT_CONFIG_NMEA)
#include <SensorKit/impl/sensors/gpsais/Defines.hpp>
#include <SensorKit/impl/sensors/gpsais/NmeaParser.hpp>

namespace SensorKit {
namespace impl {
namespace sensors {
namespace gpsais {

class AisNmeaProcessor : public Base::Object
{
public:
    typedef Base::Delegate<void (unsigned, const void *)> Delegate;
public:
    AisNmeaProcessor(NmeaParser *parser, Delegate delegate, bool aivdo = false);
    ~AisNmeaProcessor();
private:
    void parseAIVDM(const char *argv[], size_t argc);
    bool decodePayload(int channel);
private:
    struct chandata_t
    {
        uint8_t bitbuf[128]; /* 1024 bits: enough for most messages (except perhaps Type 26?) */
        int bitpos;
        int count, index, message;
    };
    struct type24_entry_t
    {
        unsigned mmsi;
        char name[NELEMS(((AisVesselData *)0)->name)];
    };
    NmeaParser *_parser;
    Delegate _delegate;
    chandata_t _chandata[2];
    type24_entry_t _type24_array[8];
    size_t _type24_index;
};

}
}
}
}
#endif // SENSORKIT_CONFIG_NMEA

#endif // SENSORKIT_IMPL_SENSORS_GPSAIS_AISNMEAPROCESSOR_HPP_INCLUDED
