/*
 * SensorKit/impl/sensors/gpsais/NmeaParser.cpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#include <SensorKit/impl/sensors/gpsais/NmeaParser.hpp>
#if defined(SENSORKIT_CONFIG_NMEA)

namespace SensorKit {
namespace impl {
namespace sensors {
namespace gpsais {

NmeaParser::NmeaParser(Base::Stream *stream) : Base::TextParser(stream, 1024)
{
    _delegates = new DelegateMap;
}
NmeaParser::~NmeaParser()
{
    _delegates->release();
}
void NmeaParser::addDelegate(const char *tag, Delegate delegate)
{
    Base::CString *tagobj = Base::CString::create(tag);
    _delegates->setDelegate(tagobj, delegate);
    tagobj->release();
}
void NmeaParser::removeDelegate(const char *tag)
{
    _delegates->removeDelegate(BASE_ALLOCA_OBJECT(Base::TransientCString, tag));
}
void NmeaParser::line(const char *argv[], size_t argc)
{
    assert(0 < argc);
    if (Delegate delegate = _delegates->delegate(BASE_ALLOCA_OBJECT(Base::TransientCString, argv[0])))
        delegate(argv, argc);
}
void NmeaParser::parseBuffer(bool eof)
{
    for (;;)
    {
        char *startp = _charptr();
        char *endp = startp;
        for (;;)
        {
            if (!_testptr(endp))
                raise();
            if ('\n' == *endp++)
                break;
        }
        const char *argv[32]; size_t argc;
        unsigned char chksum;
        char *p = startp;
        switch (*p)
        {
        case '$':
        case '!':
            chksum = 0;
            argv[0] = p++; argc = 1;
            break;
        default:
            goto skip;
        }
        for (;;)
        {
            switch (unsigned char c = *p)
            {
            default:
                chksum ^= c;
                p++;
                break;
            case ',':
                chksum ^= c;
                *p++ = '\0';
                if (NELEMS(argv) <= argc)
                    goto skip;
                argv[argc++] = p;
                break;
            case '*':
                *p++ = '\0';
                if (isxdigit(p[0]) && isxdigit(p[1]) && '\r' == p[2] && '\n' == p[3])
                {
                    unsigned char x0 = p[0] < 'A' ? p[0] - '0' : (p[0] & ~0x20) - 'A' + 10;
                    unsigned char x1 = p[1] < 'A' ? p[1] - '0' : (p[1] & ~0x20) - 'A' + 10;
                    if (chksum == ((x0 << 4) | x1))
                        goto done;
                }
                goto skip;
            case '\r':
                p++;
                if ('\n' != *p)
                    goto skip;
                /* fall through */
            case '\n':
                goto done;
            }
        }
    done:
        line(argv, argc);
    skip:
        skipnext(endp - startp);
    }
}

}
}
}
}
#endif // SENSORKIT_CONFIG_NMEA
