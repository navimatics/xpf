/*
 * SensorKit/impl/sensors/gpsais/AisNmeaProcessor.cpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#include <SensorKit/impl/sensors/gpsais/AisNmeaProcessor.hpp>
#if defined(SENSORKIT_CONFIG_NMEA)

namespace SensorKit {
namespace impl {
namespace sensors {
namespace gpsais {

static inline bool parseNatNum(const char *p, int &value)
{
    value = 0;
    while ('\0' != *p)
    {
        if (!isdigit(*p))
            return false;
        value *= 10;
        value += *p - '0';
        p++;
    }
    return true;
}
static inline uint32_t ubits32(const uint8_t *bitbuf, int bitpos, int bitlen)
{
    int bytidx = bitpos / 8;
    int bitidx = bitpos % 8;
    uint32_t mask = -(0x80000000U >> (bitlen - 1));
        /* ''mask'' now has the ''bitlen'' most-significant bits set to 1 */
    mask >>= bitidx;
    uint32_t result = 0;
    const uint8_t *startp = &bitbuf[bytidx], *p = startp;
    while (mask)
    {
        result <<= 8;
        result |= (*p++) & (mask >> 24);
        mask <<= 8;
    }
    result >>= (p - startp) * 8 - (bitidx + bitlen);
    return (uint32_t)result;
}
static inline int32_t sbits32(const uint8_t *bitbuf, int bitpos, int bitlen)
{
    uint32_t mask = 1U << (bitlen - 1);
    int32_t result = (int32_t)ubits32(bitbuf, bitpos, bitlen);
    result = (result ^ mask) - mask;
    return result;
}
static inline uint32_t ubits64(const uint8_t *bitbuf, int bitpos, int bitlen)
{
    int bytidx = bitpos / 8;
    int bitidx = bitpos % 8;
    uint64_t mask = -(0x8000000000000000ULL >> (bitlen - 1));
        /* ''mask'' now has the ''bitlen'' most-significant bits set to 1 */
    mask >>= bitidx;
    uint64_t result = 0;
    const uint8_t *startp = &bitbuf[bytidx], *p = startp;
    while (mask)
    {
        result <<= 8;
        result |= (*p++) & (mask >> 56);
        mask <<= 8;
    }
    result >>= (p - startp) * 8 - (bitidx + bitlen);
    return (uint32_t)result;
}
static inline int32_t sbits64(const uint8_t *bitbuf, int bitpos, int bitlen)
{
    uint32_t mask = 1U << (bitlen - 1);
    int32_t result = (int32_t)ubits64(bitbuf, bitpos, bitlen);
    result = (result ^ mask) - mask;
    return result;
}
static const char ctab[] = "@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^- !\"#$%&'()*+,-./0123456789:;<=>?";
static inline void cbits(const uint8_t *bitbuf, int bitpos, char *chrbuf, size_t chrlen)
{
    char *p = chrbuf, *endp = chrbuf + chrlen - 1;
    while (endp > p)
    {
        *p = ctab[ubits32(bitbuf, bitpos, 6)];
        if ('@' == *p)
            break;
        p++;
        bitpos += 6;
    }
    *p-- = '\0';
    while (chrbuf <= p)
    {
        if (' ' != *p)
            break;
        *p-- = '\0';
    }
}

AisNmeaProcessor::AisNmeaProcessor(NmeaParser *parser, Delegate delegate, bool aivdo)
{
    parser->retain();
    _parser = parser;
    _delegate = delegate;
    _chandata[0].bitpos = -1;
    _chandata[1].bitpos = -1;
    for (size_t i = 0; NELEMS(_type24_array) > i; i++)
        _type24_array[i].mmsi = Base::None();
    _parser->addDelegate("!AIVDM", make_delegate(&AisNmeaProcessor::parseAIVDM, this));
    if (aivdo)
        _parser->addDelegate("!AIVDO", make_delegate(&AisNmeaProcessor::parseAIVDM, this));
}
AisNmeaProcessor::~AisNmeaProcessor()
{
    _parser->removeDelegate("!AIVDM");
    _parser->removeDelegate("!AIVDO");
    _parser->release();
}
void AisNmeaProcessor::parseAIVDM(const char *argv[], size_t argc)
{
    /* === AIVDM/AIVDO ===
     *
     * AIVDM packets are reports from other ships.
     * AIVDO packets are reports from your own ship.
     *
     * ------------------------------------------------------------------------------
     *         1 2 3 4 5    6  7
     *         | | | | |    |  |
     *  !AIVDM,x,x,x,a,x...,x,*hh<CR><LF>
     * ------------------------------------------------------------------------------
     *
     * Field Number:
     *
     * 1. Count of fragments in the currently accumulating message
     * 2. Fragment number of this sentence
     * 3. Sequential message ID for multi-sentence messages
     * 4. Radio channel code (A or B)
     * 5. Data payload
     * 6. Number of fill bits requires to pad the data payload to a 6 bit boundary (0 to 5)
     * 7. Checksum
     */
    if (7 > argc)
        return;
    int channel = argv[4][0];
    switch (channel)
    {
    case '\0':
        if (0 != strcmp("!AIVDO", argv[0]))
            return;
        channel = 0;
        break;
    case 'A': case 'B':
        if ('\0' != argv[4][1])
            return;
        channel -= 'A';
        break;
    case '1': case '2':
        if ('\0' != argv[4][1])
            return;
        channel -= '1';
        break;
    default:
        return;
    }
    int count, index, message, pad;
    if (parseNatNum(argv[1], count) &&
        parseNatNum(argv[2], index) &&
        parseNatNum(argv[3], message) &&
        parseNatNum(argv[6], pad))
    {
        /*
         * In the following the formula (b + 7) / 8 calculates
         * how many bytes are required to store b bits:
         *     b=0 -> (0+7)/8 = 7/8 = 0 bytes
         *     b=1 -> (1+7)/8 = 8/8 = 1 bytes
         *     b=2 -> (2+7)/8 = 9/8 = 1 bytes
         *     b=8 -> (8+7)/8 = 15/8 = 1 bytes
         *     b=9 -> (9+7)/8 = 16/8 = 2 bytes
         *     ...
         */
        assert(NELEMS(_chandata) > channel);
        chandata_t *chandata = &_chandata[channel];
        if (1 == index)
        {
            if (0 <= chandata->bitpos)
                memset(chandata->bitbuf, 0, (chandata->bitpos + 7) / 8);
            else
                memset(chandata->bitbuf, 0, sizeof(chandata->bitbuf));
            chandata->bitpos = 0;
            chandata->count = count;
            chandata->index = index;
            chandata->message = message;
        }
        else if (chandata->count != count || chandata->index + 1 != index || chandata->message != message)
            chandata->bitpos = -1;
        else
            chandata->index = index;
        if (0 <= chandata->bitpos)
        {
            for (const char *p = argv[5]; *p; p++)
            {
                if (NELEMS(chandata->bitbuf) < (chandata->bitpos + 6 + 7) / 8)
                {
                    /* append of a 6-bit character would result in chandata->bitbuf overflow */
                    chandata->bitpos = -1;
                    return;
                }
                uint8_t c = (uint8_t)*p - 48;
                if (40 < c)
                    c -= 8;
                /* append 6-bit character to chandata->bitbuf */
                int bytidx = chandata->bitpos / 8;
                int bitidx = chandata->bitpos % 8;
                c <<= 2;
                chandata->bitbuf[bytidx] |= c >> bitidx;
                if (2 < bitidx)
                    chandata->bitbuf[bytidx + 1] = c << (8 - bitidx);
                chandata->bitpos += 6;
            }
            chandata->bitpos -= pad;
            if (count == index)
                decodePayload(channel);
        }
    }
}
bool AisNmeaProcessor::decodePayload(int channel)
{
#define UBITS(bitpos, bitlen)           \
    (32 < bitpos % 8 + bitlen ? ubits64(chandata->bitbuf, bitpos, bitlen) : ubits32(chandata->bitbuf, bitpos, bitlen))
#define SBITS(bitpos, bitlen)           \
    (32 < bitpos % 8 + bitlen ? sbits64(chandata->bitbuf, bitpos, bitlen) : sbits32(chandata->bitbuf, bitpos, bitlen))
#define CBITS(bitpos, chrbuf)           cbits(chandata->bitbuf, bitpos, chrbuf, sizeof chrbuf)
    chandata_t *chandata = &_chandata[channel];
    if (38 > chandata->bitpos)
        return false;
    uint32_t type = UBITS(0, 6);
    //uint32_t repeat = UBITS(6, 2);
    uint32_t mmsi = UBITS(8, 30);
    switch (type)
    {
    case 1: case 2: case 3:
    case 18: case 19:
        {
            uint32_t status, speed, accuracy, course, heading, second;
            int32_t lon, lat, turn;
            if (1 == type || 2 == type || 3 == type)
            {
                if (168 != chandata->bitpos)
                    return false;
                status = UBITS(38, 4);
                turn = SBITS(42, 8);
                speed = UBITS(50, 10);
                accuracy = UBITS(60, 1);
                lon = SBITS(61, 28);
                lat = SBITS(89, 27);
                course = UBITS(116, 12);
                heading = UBITS(128, 9);
                second = UBITS(137, 6);
            }
            else /* types 18, 19 */
            {
                if (18 == type)
                {
                    if (168 != chandata->bitpos)
                        return false;
                }
                else /* type 19 */
                {
                    if (312 != chandata->bitpos)
                        return false;
                }
                status = AisData::StatusUnknown;
                turn = -128;
                speed = UBITS(46, 10);
                accuracy = UBITS(56, 1);
                lon = SBITS(57, 28);
                lat = SBITS(85, 27);
                course = UBITS(112, 12);
                heading = UBITS(124, 9);
                second = UBITS(133, 6);
            }
            AisData aisData = AisDataNone();
            aisData.mmsi = mmsi;
            aisData.status = status;
            if (15 <= aisData.status)
                aisData.status = AisData::StatusUnknown;
            if (60 <= second)
                aisData.timestamp = Base::DateTime::currentJulianDate();
            else
            {
                double currentJulianDate = Base::DateTime::currentJulianDate();
                Base::DateTime::Components components =
                    Base::DateTime::componentsFromJulianDate(currentJulianDate);
                components.sec = second;
                components.msec = 0;
                double computedJulianDate = Base::DateTime::julianDateFromComponents(components);
                if (computedJulianDate > currentJulianDate)
                    /* Assuming that all ship clocks are synchronized,
                     * the computedJulianDate cannot be later than our time.
                     * If that was the case, the reported second was in the
                     * previous minute.
                     */
                    computedJulianDate -= 1.0 / (24 * 60);
                aisData.timestamp = computedJulianDate;
            }
            aisData.lat = lat / 600000.0;
            if (-90.0 > aisData.lat || aisData.lat > +90.0)
                aisData.lat = Base::None();
            aisData.lon = lon / 600000.0;
            if (-180.0 > aisData.lon || aisData.lon > +180.0)
                aisData.lon = Base::None();
            if (accuracy)
                aisData.e_lat = aisData.e_lon = 3.75;
            else
                aisData.e_lat = aisData.e_lon = 15.0;
            aisData.track = course / 10.0;
            if (+360.0 <= aisData.track)
                aisData.track = Base::None();
            if (+1023 == speed)
                aisData.speed = Base::None();
            else if (+1022 == speed)
                aisData.speed = +INFINITY;
            else
                aisData.speed = speed * (0.514444 / 10);  /* kn tenths -> m/s */
            if (-128 == turn)
                aisData.turn = Base::None();
            else if (-127 == turn)
                aisData.turn = -INFINITY;
            else if (+127 == turn)
                aisData.turn = +INFINITY;
            else
            {
                aisData.turn = turn / 4.733;
                aisData.turn *= aisData.turn;
                if (0 > turn)
                    aisData.turn = -aisData.turn;
            }
            aisData.trueHeading = heading;
            if (+360.0 <= aisData.trueHeading)
                aisData.trueHeading = Base::None();
            _delegate(AisDataStruct, &aisData);
            if (19 == type)
            {
                uint32_t shiptype = UBITS(263, 8);
                uint32_t to_bow = UBITS(271, 9);
                uint32_t to_stern = UBITS(280, 9);
                uint32_t to_port = UBITS(289, 6);
                uint32_t to_starboard = UBITS(295, 6);
                uint32_t epfd = UBITS(301, 4);
                AisVesselData aisVesselData = AisVesselDataNone();
                aisVesselData.mmsi = mmsi;
                CBITS(143, aisVesselData.name);
                aisVesselData.type = shiptype;
                if (100 <= aisVesselData.type)
                    aisVesselData.type = AisVesselData::TypeUnknown;
                aisVesselData.length = to_bow + to_stern;
                aisVesselData.beam = to_port + to_starboard;
                aisVesselData.antennaLongitudinalPosition = (double)to_bow / aisVesselData.length;
                aisVesselData.antennaLateralPosition = (double)to_port / aisVesselData.beam;
                aisVesselData.epfd = epfd;
                if (9 <= aisVesselData.epfd)
                    aisVesselData.epfd = AisVesselData::EpfdUnknown;
                _delegate(AisVesselDataStruct, &aisVesselData);
            }
            return true;
        }
    case 5:
        {
            if (424 != chandata->bitpos)
                return false;
            uint32_t imo = UBITS(40, 30);
            uint32_t shiptype = UBITS(232, 8);
            uint32_t to_bow = UBITS(240, 9);
            uint32_t to_stern = UBITS(249, 9);
            uint32_t to_port = UBITS(258, 6);
            uint32_t to_starboard = UBITS(264, 6);
            uint32_t epfd = UBITS(270, 4);
            uint32_t month = UBITS(274, 4);
            uint32_t day = UBITS(278, 5);
            uint32_t hour = UBITS(283, 5);
            uint32_t minute = UBITS(288, 6);
            uint32_t draft = UBITS(294, 8);
            AisVesselData aisVesselData = AisVesselDataNone();
            aisVesselData.mmsi = mmsi;
            aisVesselData.imo = imo;
            CBITS(70, aisVesselData.callsign);
            CBITS(112, aisVesselData.name);
            aisVesselData.type = shiptype;
            if (100 <= aisVesselData.type)
                aisVesselData.type = AisVesselData::TypeUnknown;
            aisVesselData.length = to_bow + to_stern;
            aisVesselData.beam = to_port + to_starboard;
            aisVesselData.draft = draft / 10.0;
            aisVesselData.antennaLongitudinalPosition = (double)to_bow / aisVesselData.length;
            aisVesselData.antennaLateralPosition = (double)to_port / aisVesselData.beam;
            aisVesselData.epfd = epfd;
            if (9 <= aisVesselData.epfd)
                aisVesselData.epfd = AisVesselData::EpfdUnknown;
            CBITS(302, aisVesselData.destination);
            if (0 == month || month > 12)
                aisVesselData.eta = Base::None();
            else if (0 == day || day > 31)
                aisVesselData.eta = Base::None();
            else
            {
                double currentJulianDate = round(Base::DateTime::currentJulianDate());
                Base::DateTime::Components components =
                    Base::DateTime::componentsFromJulianDate(currentJulianDate);
                components.month = month;
                components.day = day;
                double computedJulianDate = Base::DateTime::julianDateFromComponents(components);
                if (computedJulianDate < currentJulianDate)
                    /* Assuming that all ship clocks are synchronized,
                     * the computedJulianDate cannot be less than the
                     * current date (since it must be an ETA in the future).
                     * If that is the case the ETA must be next year.
                     */
                    components.year++;
                if (23 >= hour)
                {
                    components.hour = hour;
                    if (59 >= minute)
                        components.min = minute;
                }
                aisVesselData.eta = Base::DateTime::julianDateFromComponents(components);
            }
            _delegate(AisVesselDataStruct, &aisVesselData);
            return true;
        }
    case 24:
        {
            if (40 > chandata->bitpos)
                return false;
            uint32_t part = UBITS(38, 2);
            switch (part)
            {
            case 0:
                if (160 != chandata->bitpos)
                    return false;
                {
                    type24_entry_t *entry = &_type24_array[_type24_index];
                    _type24_index = (_type24_index + 1) % NELEMS(_type24_array);
                    entry->mmsi = mmsi;
                    CBITS(40, entry->name);
                }
                return true;
            case 1:
                if (168 != chandata->bitpos)
                    return false;
                for (size_t i = 0; NELEMS(_type24_array) > i; i++)
                {
                    type24_entry_t *entry = &_type24_array[i];
                    if (mmsi == entry->mmsi)
                    {
                        AisVesselData aisVesselData = AisVesselDataNone();
                        aisVesselData.mmsi = mmsi;
                        aisVesselData.type = UBITS(40, 8);
                        if (100 <= aisVesselData.type)
                            aisVesselData.type = AisVesselData::TypeUnknown;
                        CBITS(90, aisVesselData.callsign);
                        memcpy(aisVesselData.name, entry->name, sizeof(aisVesselData.name));
                        if (mmsi / 10000000 != 98)
                        {
                            /* MMSI is primary */
                            uint32_t to_bow = UBITS(132, 9);
                            uint32_t to_stern = UBITS(141, 9);
                            uint32_t to_port = UBITS(150, 6);
                            uint32_t to_starboard = UBITS(156, 6);
                            aisVesselData.length = to_bow + to_stern;
                            aisVesselData.beam = to_port + to_starboard;
                            aisVesselData.antennaLongitudinalPosition = (double)to_bow / aisVesselData.length;
                            aisVesselData.antennaLateralPosition = (double)to_port / aisVesselData.beam;
                        }
                        else
                        {
                            /* MMSI is of a vessel associated with a parent ship */
                            /* nothing to do */
                        }
                        _delegate(AisVesselDataStruct, &aisVesselData);
                        return true;
                    }
                }
                break;
            }
            return false;
        }
    default:
        return false;
    }
#undef CBITS
#undef SBITS
#undef UBITS
}

}
}
}
}
#endif // SENSORKIT_CONFIG_NMEA
