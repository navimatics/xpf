/*
 * Base/GpsAisSensorTest.cpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#include <SensorKit/Sensor.hpp>
#if defined(SENSORKIT_CONFIG_NMEA)
#include <gtest/gtest.h>

static inline const char *cstringFromJulianDate(double jd)
{
    return Base::DateTime::format(Base::DateTime::componentsFromJulianDate(jd));
}

#if defined(BASE_CONFIG_WINDOWS)
#define GPSDEV                          "file:/COM1"
#elif defined(BASE_CONFIG_POSIX)
#define GPSDEV                          "file:/dev/cu.usbserial?speed=4800"
#endif
#define GPSNET                          "tcp://localhost:11011"
#define GPSDNET                         "tcp://localhost:2947"

TEST(GpsAisSensorTest, OpenClose)
{
    mark_and_collect
    {
        SensorKit::Sensor::registerAllCreators();
        static Base::CString *uriKey = Base::CStringConstant("uri");
        static Base::CString *uriVal = Base::CStringConstant(GPSDEV);
        Base::Map *params = new Base::Map;
        params->setObject(uriKey, uriVal);
        SensorKit::Sensor *sensor = SensorKit::Sensor::create("GPSAIS-NMEA", params);
        params->release();
        ASSERT_EQ(SensorKit::Sensor::KindGps | SensorKit::Sensor::KindGpsSky | SensorKit::Sensor::KindAis, sensor->kind());
        ASSERT_STREQ("GPSAIS-NMEA: " GPSDEV, sensor->description());
        SensorKit::SensorHandle *handle = sensor->createHandle();
        ASSERT_EQ(sensor, handle->sensor());
        ASSERT_EQ(0, handle->open(SensorKit::Sensor::KindUnknown));
        ASSERT_EQ(SensorKit::Sensor::KindGps | SensorKit::Sensor::KindGpsSky | SensorKit::Sensor::KindAis, handle->openKind());
        ASSERT_EQ(0, handle->close(SensorKit::Sensor::KindUnknown));
        ASSERT_TRUE(0 == handle->openKind());
        handle->release();
        sensor->release();
    }
}
void GpsAisSensorTestDelegate(SensorKit::SensorHandle *handle, SensorKit::Sensor::kind_t kind)
{
    if (kind & SensorKit::Sensor::KindError)
    {
        Base::Error *error = dynamic_cast<SensorKit::ErrorSensorHandle *>(handle)->error();
        ASSERT_NE((void *)0, error);
        DEBUGLOG("%s", error->strrepr());
    }
    else
    {
        if (kind & SensorKit::Sensor::KindGps)
        {
            SensorKit::GpsData gpsData = dynamic_cast<SensorKit::GpsSensorHandle *>(handle)->gpsData();
            //ASSERT_FALSE(Base::isNone(gpsData.timestamp));
            LOG("GpsData: %s %f %f", cstringFromJulianDate(gpsData.timestamp), gpsData.lat, gpsData.lon);
        }
        if (kind & SensorKit::Sensor::KindGpsSky)
        {
            SensorKit::GpsSkyData gpsSkyData =
                dynamic_cast<SensorKit::GpsSkySensorHandle *>(handle)->gpsSkyData();
            //ASSERT_FALSE(Base::isNone(gpsSkyData.timestamp));
            LOG("GpsSkyData: %s %tu", cstringFromJulianDate(gpsSkyData.timestamp), gpsSkyData.satelliteCount);
        }
    }
    Base::ThreadLoopStop();
}
#if 1
TEST(GpsAisSensorTest, Serial)
{
    mark_and_collect
    {
        SensorKit::Sensor::registerAllCreators();
        Base::ThreadLoopInit();
        static Base::CString *uriKey = Base::CStringConstant("uri");
        static Base::CString *uriVal = Base::CStringConstant(GPSDEV);
        Base::Map *params = new Base::Map;
        params->setObject(uriKey, uriVal);
        SensorKit::Sensor *sensor = SensorKit::Sensor::create("GPSAIS-NMEA", params);
        params->release();
        SensorKit::SensorHandle *handle = sensor->createHandle();
        handle->setDelegate(make_delegate(GpsAisSensorTestDelegate));
        ASSERT_EQ(0, handle->open(SensorKit::Sensor::KindUnknown));
        ASSERT_TRUE(0 != (SensorKit::Sensor::KindGps & handle->openKind()));
        ASSERT_EQ(SensorKit::Sensor::KindGps | SensorKit::Sensor::KindGpsSky | SensorKit::Sensor::KindAis, sensor->kind());
        Base::ThreadLoopRun();
        ASSERT_EQ(0, handle->close(SensorKit::Sensor::KindUnknown));
        handle->release();
        sensor->release();
    }
}
#endif
#if 1
TEST(GpsAisSensorTest, Network)
{
    mark_and_collect
    {
        SensorKit::Sensor::registerAllCreators();
        Base::ThreadLoopInit();
        static Base::CString *uriKey = Base::CStringConstant("uri");
        static Base::CString *uriVal = Base::CStringConstant(GPSNET);
        Base::Map *params = new Base::Map;
        params->setObject(uriKey, uriVal);
        SensorKit::Sensor *sensor = SensorKit::Sensor::create("GPSAIS-NMEA", params);
        params->release();
        SensorKit::SensorHandle *handle = sensor->createHandle();
        handle->setDelegate(make_delegate(GpsAisSensorTestDelegate));
        ASSERT_EQ(0, handle->open(SensorKit::Sensor::KindUnknown));
        ASSERT_TRUE(0 != (SensorKit::Sensor::KindGps & handle->openKind()));
        ASSERT_EQ(SensorKit::Sensor::KindGps | SensorKit::Sensor::KindGpsSky | SensorKit::Sensor::KindAis, sensor->kind());
        Base::ThreadLoopRun();
        ASSERT_EQ(0, handle->close(SensorKit::Sensor::KindUnknown));
        handle->release();
        sensor->release();
    }
}
#endif
#if 1
TEST(GpsAisSensorTest, Gpsd)
{
    mark_and_collect
    {
        SensorKit::Sensor::registerAllCreators();
        Base::ThreadLoopInit();
        static Base::CString *uriKey = Base::CStringConstant("uri");
        static Base::CString *uriVal = Base::CStringConstant(GPSDNET);
        Base::Map *params = new Base::Map;
        params->setObject(uriKey, uriVal);
        SensorKit::Sensor *sensor = SensorKit::Sensor::create("GPSAIS-GPSD", params);
        params->release();
        SensorKit::SensorHandle *handle = sensor->createHandle();
        handle->setDelegate(make_delegate(GpsAisSensorTestDelegate));
        ASSERT_EQ(0, handle->open(SensorKit::Sensor::KindUnknown));
        ASSERT_TRUE(0 != (SensorKit::Sensor::KindGps & handle->openKind()));
        ASSERT_EQ(SensorKit::Sensor::KindGps | SensorKit::Sensor::KindGpsSky | SensorKit::Sensor::KindAis, sensor->kind());
        Base::ThreadLoopRun();
        ASSERT_EQ(0, handle->close(SensorKit::Sensor::KindUnknown));
        handle->release();
        sensor->release();
    }
}
#endif
#endif // SENSORKIT_CONFIG_NMEA
