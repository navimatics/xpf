/*
 * Base/AisNmeaProcessorTest.cpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#include <SensorKit/Sensor.hpp>
#include <SensorKit/impl/sensors/gpsais/AisNmeaProcessor.hpp>
#if defined(SENSORKIT_CONFIG_NMEA)
#include <gtest/gtest.h>

static void BasicTestDelegate(unsigned structKind, const void *structBuf)
{
    static int count = 0;
    switch (count++)
    {
    case 0:
        {
            const SensorKit::AisData *aisData = (SensorKit::AisData *)structBuf;
            ASSERT_EQ((unsigned)SensorKit::impl::sensors::gpsais::AisDataStruct, structKind);
            ASSERT_EQ(371798000, aisData->mmsi);
            ASSERT_EQ((int)SensorKit::AisData::StatusUnderwayUsingEngine, aisData->status);
            ASSERT_EQ(33, Base::DateTime::componentsFromJulianDate(aisData->timestamp).sec);
            ASSERT_NEAR(48.38163333333, aisData->lat, 1.0/60000);
            ASSERT_NEAR(-123.395383333, aisData->lon, 1.0/60000);
            ASSERT_GT(10, aisData->e_lat);
            ASSERT_GT(10, aisData->e_lon);
            ASSERT_EQ(224, aisData->track);
            ASSERT_EQ(12.3, aisData->speed);
            ASSERT_EQ(-INFINITY, aisData->turn);
            ASSERT_EQ(215, aisData->trueHeading);
        }
        break;
    case 1:
        {
            const SensorKit::AisData *aisData = (SensorKit::AisData *)structBuf;
            ASSERT_EQ((unsigned)SensorKit::impl::sensors::gpsais::AisDataStruct, structKind);
            ASSERT_EQ(440348000, aisData->mmsi);
            ASSERT_EQ((int)SensorKit::AisData::StatusUnderwayUsingEngine, aisData->status);
            ASSERT_EQ(13, Base::DateTime::componentsFromJulianDate(aisData->timestamp).sec);
            ASSERT_NEAR(43.08015, aisData->lat, 1.0/60000);
            ASSERT_NEAR(-70.7582, aisData->lon, 1.0/60000);
            ASSERT_LT(10, aisData->e_lat);
            ASSERT_LT(10, aisData->e_lon);
            ASSERT_EQ(93.4, aisData->track);
            ASSERT_EQ(0, aisData->speed);
            ASSERT_TRUE(Base::isNone(aisData->turn));
            ASSERT_TRUE(Base::isNone(aisData->trueHeading));
        }
        break;
    case 2:
        {
            const SensorKit::AisData *aisData = (SensorKit::AisData *)structBuf;
            ASSERT_EQ((unsigned)SensorKit::impl::sensors::gpsais::AisDataStruct, structKind);
            ASSERT_EQ(356302000, aisData->mmsi);
            ASSERT_EQ((int)SensorKit::AisData::StatusUnderwayUsingEngine, aisData->status);
            ASSERT_EQ(41, Base::DateTime::componentsFromJulianDate(aisData->timestamp).sec);
            ASSERT_NEAR(40.39235833333333333333333333, aisData->lat, 1.0/60000);
            ASSERT_NEAR(-71.62614333333333333333333333, aisData->lon, 1.0/60000);
            ASSERT_LT(10, aisData->e_lat);
            ASSERT_LT(10, aisData->e_lon);
            ASSERT_EQ(87.7, aisData->track);
            ASSERT_EQ(13.9, aisData->speed);
            ASSERT_EQ(+INFINITY, aisData->turn);
            ASSERT_EQ(91, aisData->trueHeading);
        }
        break;
    case 3:
        {
            const SensorKit::AisData *aisData = (SensorKit::AisData *)structBuf;
            ASSERT_EQ((unsigned)SensorKit::impl::sensors::gpsais::AisDataStruct, structKind);
            ASSERT_EQ(563808000, aisData->mmsi);
            ASSERT_EQ((int)SensorKit::AisData::StatusMoored, aisData->status);
            ASSERT_EQ(35, Base::DateTime::componentsFromJulianDate(aisData->timestamp).sec);
            ASSERT_NEAR(36.91, aisData->lat, 1.0/60000);
            ASSERT_NEAR(-76.32753333333333333333333333, aisData->lon, 1.0/60000);
            ASSERT_GT(10, aisData->e_lat);
            ASSERT_GT(10, aisData->e_lon);
            ASSERT_EQ(252, aisData->track);
            ASSERT_EQ(0, aisData->speed);
            ASSERT_EQ(0, aisData->turn);
            ASSERT_EQ(352, aisData->trueHeading);
        }
        break;
    case 4:
        {
            const SensorKit::AisVesselData *aisVesselData = (SensorKit::AisVesselData *)structBuf;
            ASSERT_EQ((unsigned)SensorKit::impl::sensors::gpsais::AisVesselDataStruct, structKind);
            ASSERT_EQ(351759000, aisVesselData->mmsi);
            ASSERT_EQ(9134270, aisVesselData->imo);
            ASSERT_STREQ("3FOF8", aisVesselData->callsign);
            ASSERT_STREQ("EVER DIADEM", aisVesselData->name);
            ASSERT_EQ((int)SensorKit::AisVesselData::TypeCargo, aisVesselData->type);
            ASSERT_EQ(225 + 70, aisVesselData->length);
            ASSERT_EQ(1 + 31, aisVesselData->beam);
            ASSERT_EQ(12.2, aisVesselData->draft);
            ASSERT_EQ(225.0/(225 + 70), aisVesselData->antennaLongitudinalPosition);
            ASSERT_EQ(1.0/(1 + 31), aisVesselData->antennaLateralPosition);
            ASSERT_EQ((int)SensorKit::AisVesselData::EpfdGps, aisVesselData->epfd);
            ASSERT_STREQ("NEW YORK", aisVesselData->destination);
            Base::DateTime::Components components = Base::DateTime::componentsFromJulianDate(aisVesselData->eta);
            ASSERT_EQ(5, components.month);
            ASSERT_EQ(15, components.day);
            ASSERT_EQ(14, components.hour);
            ASSERT_EQ(0, components.min);
        }
        break;
    case 5:
        {
            const SensorKit::AisData *aisData = (SensorKit::AisData *)structBuf;
            ASSERT_EQ((unsigned)SensorKit::impl::sensors::gpsais::AisDataStruct, structKind);
            ASSERT_EQ(338087471, aisData->mmsi);
            ASSERT_EQ((int)SensorKit::AisData::StatusUnknown, aisData->status);
            ASSERT_EQ(49, Base::DateTime::componentsFromJulianDate(aisData->timestamp).sec);
            ASSERT_NEAR(40.68454, aisData->lat, 1.0/60000);
            ASSERT_NEAR(-74.07213166666666666666666667, aisData->lon, 1.0/60000);
            ASSERT_LT(10, aisData->e_lat);
            ASSERT_LT(10, aisData->e_lon);
            ASSERT_EQ(79.6, aisData->track);
            ASSERT_EQ(0.1, aisData->speed);
            ASSERT_TRUE(Base::isNone(aisData->turn));
            ASSERT_TRUE(Base::isNone(aisData->trueHeading));
        }
        break;
    case 6:
        {
            const SensorKit::AisData *aisData = (SensorKit::AisData *)structBuf;
            ASSERT_EQ((unsigned)SensorKit::impl::sensors::gpsais::AisDataStruct, structKind);
            ASSERT_EQ(338088483, aisData->mmsi);
            ASSERT_EQ((int)SensorKit::AisData::StatusUnknown, aisData->status);
            ASSERT_EQ(20, Base::DateTime::componentsFromJulianDate(aisData->timestamp).sec);
            ASSERT_NEAR(43.11555833, aisData->lat, 1.0/60000);
            ASSERT_NEAR(-70.8111966, aisData->lon, 1.0/60000);
            ASSERT_LT(10, aisData->e_lat);
            ASSERT_LT(10, aisData->e_lon);
            ASSERT_EQ(171.6, aisData->track);
            ASSERT_EQ(0, aisData->speed);
            ASSERT_TRUE(Base::isNone(aisData->turn));
            ASSERT_TRUE(Base::isNone(aisData->trueHeading));
        }
        break;
    case 7:
        {
            const SensorKit::AisData *aisData = (SensorKit::AisData *)structBuf;
            ASSERT_EQ((unsigned)SensorKit::impl::sensors::gpsais::AisDataStruct, structKind);
            ASSERT_EQ(368161000, aisData->mmsi);
            ASSERT_EQ((int)SensorKit::AisData::StatusUnknown, aisData->status);
            ASSERT_EQ(17, Base::DateTime::componentsFromJulianDate(aisData->timestamp).sec);
            ASSERT_NEAR(39.480925, aisData->lat, 1.0/60000);
            ASSERT_NEAR(-72.2338483333, aisData->lon, 1.0/60000);
            ASSERT_GT(10, aisData->e_lat);
            ASSERT_GT(10, aisData->e_lon);
            ASSERT_EQ(34.9, aisData->track);
            ASSERT_EQ(5.1, aisData->speed);
            ASSERT_TRUE(Base::isNone(aisData->turn));
            ASSERT_TRUE(Base::isNone(aisData->trueHeading));
        }
        break;
    case 8:
        {
            const SensorKit::AisData *aisData = (SensorKit::AisData *)structBuf;
            ASSERT_EQ((unsigned)SensorKit::impl::sensors::gpsais::AisDataStruct, structKind);
            ASSERT_EQ(367059850, aisData->mmsi);
            ASSERT_EQ((int)SensorKit::AisData::StatusUnknown, aisData->status);
            ASSERT_EQ(46, Base::DateTime::componentsFromJulianDate(aisData->timestamp).sec);
            ASSERT_NEAR(29.543695, aisData->lat, 1.0/60000);
            ASSERT_NEAR(-88.8103916667, aisData->lon, 1.0/60000);
            ASSERT_LT(10, aisData->e_lat);
            ASSERT_LT(10, aisData->e_lon);
            ASSERT_EQ(335.9, aisData->track);
            ASSERT_EQ(8.7, aisData->speed);
            ASSERT_TRUE(Base::isNone(aisData->turn));
            ASSERT_TRUE(Base::isNone(aisData->trueHeading));
        }
        break;
    case 9:
        {
            const SensorKit::AisVesselData *aisVesselData = (SensorKit::AisVesselData *)structBuf;
            ASSERT_EQ((unsigned)SensorKit::impl::sensors::gpsais::AisVesselDataStruct, structKind);
            ASSERT_EQ(367059850, aisVesselData->mmsi);
            ASSERT_TRUE(Base::isNone(aisVesselData->imo));
            ASSERT_EQ('\0', aisVesselData->callsign[0]);
            ASSERT_STREQ("CAPT.J.RIMES", aisVesselData->name);
            ASSERT_EQ((int)SensorKit::AisVesselData::TypeCargo, aisVesselData->type);
            ASSERT_EQ(5 + 21, aisVesselData->length);
            ASSERT_EQ(4 + 4, aisVesselData->beam);
            ASSERT_TRUE(Base::isNone(aisVesselData->draft));
            ASSERT_EQ(5.0/(5 + 21), aisVesselData->antennaLongitudinalPosition);
            ASSERT_EQ(4.0/(4 + 4), aisVesselData->antennaLateralPosition);
            ASSERT_EQ((int)SensorKit::AisVesselData::EpfdGps, aisVesselData->epfd);
            ASSERT_EQ('\0', aisVesselData->destination[0]);
            ASSERT_TRUE(Base::isNone(aisVesselData->eta));
        }
        break;
    case 10:
        {
            const SensorKit::AisVesselData *aisVesselData = (SensorKit::AisVesselData *)structBuf;
            ASSERT_EQ((unsigned)SensorKit::impl::sensors::gpsais::AisVesselDataStruct, structKind);
            ASSERT_EQ(271041815, aisVesselData->mmsi);
            ASSERT_TRUE(Base::isNone(aisVesselData->imo));
            ASSERT_STREQ("TC6163", aisVesselData->callsign);
            ASSERT_STREQ("PROGUY", aisVesselData->name);
            ASSERT_EQ((int)SensorKit::AisVesselData::TypePassenger, aisVesselData->type);
            ASSERT_EQ(0 + 15, aisVesselData->length);
            ASSERT_EQ(0 + 5, aisVesselData->beam);
            ASSERT_TRUE(Base::isNone(aisVesselData->draft));
            ASSERT_EQ(0.0/(0 + 15), aisVesselData->antennaLongitudinalPosition);
            ASSERT_EQ(0.0/(0 + 5), aisVesselData->antennaLateralPosition);
            ASSERT_EQ((int)SensorKit::AisVesselData::EpfdUnknown, aisVesselData->epfd);
            ASSERT_EQ('\0', aisVesselData->destination[0]);
            ASSERT_TRUE(Base::isNone(aisVesselData->eta));
        }
        break;
    }
}
TEST(AisNmeaProcessorTest, BasicTest)
{
    SensorKit::impl::sensors::gpsais::NmeaParser *parser =
        new SensorKit::impl::sensors::gpsais::NmeaParser;
    SensorKit::impl::sensors::gpsais::AisNmeaProcessor *processor =
        new SensorKit::impl::sensors::gpsais::AisNmeaProcessor(parser, make_delegate(BasicTestDelegate));
#define PARSE(lit)                      ASSERT_EQ(sizeof(lit) + 1, parser->parse(lit "\r\n", sizeof(lit) + 1))
    /* taken from gpsd-3.9/test/sample.aivdm */
    PARSE("!AIVDM,1,1,,A,15RTgt0PAso;90TKcjM8h6g208CQ,0*4A");
    PARSE("!AIVDM,1,1,,A,16SteH0P00Jt63hHaa6SagvJ087r,0*42");
    PARSE("!AIVDM,1,1,,B,25Cjtd0Oj;Jp7ilG7=UkKBoB0<06,0*60");
    PARSE("!AIVDM,1,1,,A,38Id705000rRVJhE7cl9n;160000,0*40");
    PARSE("!AIVDM,2,1,1,A,55?MbV02;H;s<HtKR20EHE:0@T4@Dn2222222216L961O5Gf0NSQEp6ClRp8,0*1C");
    PARSE("!AIVDM,2,2,1,A,88888888880,2*25");
    PARSE("!AIVDM,1,1,,A,B52K>;h00Fc>jpUlNV@ikwpUoP06,0*4C");
    PARSE("!AIVDM,1,1,,A,B52KB8h006fu`Q6:g1McCwb5oP06,0*00");
    PARSE("!AIVDM,1,1,,B,B5O6hr00<veEKmUaMFdEow`UWP06,0*4F");
    PARSE("!AIVDM,1,1,,B,C5N3SRgPEnJGEBT>NhWAwwo862PaLELTBJ:V00000000S0D:R220,0*0B");
    PARSE("!AIVDM,1,1,,A,H42O55i18tMET00000000000000,2*6D");
    PARSE("!AIVDM,1,1,,A,H42O55lti4hhhilD3nink000?050,0*40");
#undef PARSE
    processor->release();
    parser->release();
}
#endif
