/*
 * Base/CLSensorTest.cpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#include <SensorKit/Sensor.hpp>
#if defined(SENSORKIT_CONFIG_CORELOCATION)
#include <gtest/gtest.h>

static inline const char *cstringFromJulianDate(double jd)
{
    return Base::DateTime::format(Base::DateTime::componentsFromJulianDate(jd));
}

TEST(CLSensorTest, OpenClose)
{
    mark_and_collect
    {
        SensorKit::Sensor::registerAllCreators();
        SensorKit::Sensor *sensor = SensorKit::Sensor::create("CoreLocation");
        ASSERT_TRUE(0 != (SensorKit::Sensor::KindGps & sensor->kind()));
        ASSERT_STREQ("CoreLocation", sensor->description());
        SensorKit::SensorHandle *handle = sensor->createHandle();
        ASSERT_EQ(sensor, handle->sensor());
        ASSERT_EQ(-1, handle->open(SensorKit::Sensor::KindAis));
        ASSERT_EQ(-1, handle->close(SensorKit::Sensor::KindAis));
        ASSERT_TRUE(0 == handle->openKind());
        ASSERT_EQ(0, handle->open(SensorKit::Sensor::KindUnknown));
        ASSERT_TRUE(0 != (SensorKit::Sensor::KindGps & handle->openKind()));
        ASSERT_EQ(0, handle->close(SensorKit::Sensor::KindUnknown));
        ASSERT_TRUE(0 == handle->openKind());
        handle->release();
        sensor->release();
    }
}
void CLSensorHandleDelegate(SensorKit::SensorHandle *handle, SensorKit::Sensor::kind_t kind)
{
    if (kind & SensorKit::Sensor::KindError)
    {
        Base::Error *error = dynamic_cast<SensorKit::ErrorSensorHandle *>(handle)->error();
        ASSERT_NE((void *)0, error);
        DEBUGLOG("%s", error->strrepr());
    }
    else
    {
        if (kind & SensorKit::Sensor::KindGps)
        {
            SensorKit::GpsData gpsData = dynamic_cast<SensorKit::GpsSensorHandle *>(handle)->gpsData();
            ASSERT_FALSE(Base::isNone(gpsData.timestamp));
            LOG("GpsData: %s %f %f", cstringFromJulianDate(gpsData.timestamp), gpsData.lat, gpsData.lon);
        }
        if (kind & SensorKit::Sensor::KindCompass)
        {
            SensorKit::CompassData compassData =
                dynamic_cast<SensorKit::CompassSensorHandle *>(handle)->compassData();
            ASSERT_FALSE(Base::isNone(compassData.timestamp));
            LOG("CompassData: %s %f", cstringFromJulianDate(compassData.timestamp), compassData.magneticHeading);
        }
    }
    Base::ThreadLoopStop();
}
TEST(CLSensorTest, Delegate)
{
    mark_and_collect
    {
        SensorKit::Sensor::registerAllCreators();
        Base::ThreadLoopInit();
        SensorKit::Sensor *sensor = SensorKit::Sensor::create("CoreLocation");
        ASSERT_TRUE(0 != (SensorKit::Sensor::KindGps & sensor->kind()));
        SensorKit::SensorHandle *handle = sensor->createHandle();
        handle->setDelegate(make_delegate(CLSensorHandleDelegate));
        ASSERT_EQ(0, handle->open(SensorKit::Sensor::KindUnknown));
        ASSERT_TRUE(0 != (SensorKit::Sensor::KindGps & handle->openKind()));
        Base::ThreadLoopRun();
        ASSERT_EQ(0, handle->close(SensorKit::Sensor::KindUnknown));
        handle->release();
        sensor->release();
    }
}
#endif // SENSORKIT_CONFIG_CORELOCATION
