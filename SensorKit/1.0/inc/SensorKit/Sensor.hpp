/*
 * SensorKit/Sensor.hpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef SENSORKIT_SENSOR_HPP_INCLUDED
#define SENSORKIT_SENSOR_HPP_INCLUDED

#include <SensorKit/Defines.hpp>
#include <SensorKit/impl/Sensor.hpp>

namespace SensorKit {

using SensorKit::impl::Sensor;
using SensorKit::impl::SensorHandle;

using SensorKit::impl::GpsData;
using SensorKit::impl::GpsDataNone;
using SensorKit::impl::GpsSensorHandle;

using SensorKit::impl::GpsSkyData;
using SensorKit::impl::GpsSkyDataNone;
using SensorKit::impl::GpsSkySensorHandle;

using SensorKit::impl::CompassData;
using SensorKit::impl::CompassDataNone;
using SensorKit::impl::CompassSensorHandle;

using SensorKit::impl::AisData;
using SensorKit::impl::AisDataNone;
using SensorKit::impl::AisVesselData;
using SensorKit::impl::AisVesselDataNone;
using SensorKit::impl::AisSensorHandle;

using SensorKit::impl::ErrorSensorHandle;

}

#endif // SENSORKIT_SENSOR_HPP_INCLUDED
