/*
 * SensorKit/SensorManager.hpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef SENSORKIT_SENSORMANAGER_HPP_INCLUDED
#define SENSORKIT_SENSORMANAGER_HPP_INCLUDED

#include <SensorKit/Defines.hpp>
#include <SensorKit/Sensor.hpp>
#include <SensorKit/impl/SensorManager.hpp>

namespace SensorKit {

using SensorKit::impl::SensorManager;

}

#endif // SENSORKIT_SENSORMANAGER_HPP_INCLUDED
