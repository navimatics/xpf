/*
 * SensorKit/SensorKit.hpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef SENSORKIT_SENSORKIT_HPP_INCLUDED
#define SENSORKIT_SENSORKIT_HPP_INCLUDED

#include <SensorKit/Defines.hpp>
#include <SensorKit/Sensor.hpp>
#include <SensorKit/SensorManager.hpp>

#endif // SENSORKIT_SENSORKIT_HPP_INCLUDED
