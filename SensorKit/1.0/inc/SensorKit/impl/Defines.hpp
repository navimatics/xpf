/*
 * SensorKit/impl/Defines.hpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef SENSORKIT_IMPL_DEFINES_HPP_INCLUDED
#define SENSORKIT_IMPL_DEFINES_HPP_INCLUDED

#include <Base/Base.hpp>

#if defined(SENSORKIT_CONFIG_INTERNAL)
#define SENSORKIT_APISYM                BASE_EXPORTSYM
#else
#define SENSORKIT_APISYM                BASE_IMPORTSYM
#endif

namespace SensorKit {
namespace impl {
}
}

#endif // SENSORKIT_IMPL_DEFINES_HPP_INCLUDED
