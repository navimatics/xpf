/*
 * SensorKit/impl/Sensor.hpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef SENSORKIT_IMPL_SENSOR_HPP_INCLUDED
#define SENSORKIT_IMPL_SENSOR_HPP_INCLUDED

#include <SensorKit/impl/Defines.hpp>

namespace SensorKit {
namespace impl {

class SensorHandle;

class SENSORKIT_APISYM Sensor : public Base::Object
{
public:
    enum
    {
        KindUnknown                     = 0x00000000,
        KindGps                         = 0x00000001,
        KindGpsSky                      = 0x00000002,
        KindCompass                     = 0x00000004,
        KindAis                         = 0x00000100,
        KindError                       = 0x80000000,
    };
    typedef unsigned kind_t;
public:
    static void registerAllCreators();
    static void registerCreator(const char *name,
        Base::Delegate<Sensor *(Base::Map *)> create);
    static Sensor *create(const char *name, Base::Map *params = 0);
    Sensor();
    virtual kind_t kind();
    virtual SensorHandle *createHandle();
    virtual const char *description();
};

class SENSORKIT_APISYM SensorHandle : public Base::Object
{
public:
    typedef Base::Delegate<void (SensorHandle *, Sensor::kind_t)> Delegate;
    enum
    {
        ErrorInformational = 1,         /* informational only */
        ErrorFunctionality,             /* sensor functionality affected */
        ErrorUserCorrectable,           /* sensor functionality affected; correctable by user */
        ErrorMax = 1000,
    };
public:
    static const char *errorDomain();
    SensorHandle(Sensor *sensor);
    ~SensorHandle();
    Sensor *sensor();
    Delegate delegate();
    void setDelegate(Delegate value);
    virtual Sensor::kind_t openKind();
    virtual int open(Sensor::kind_t kind = Sensor::KindUnknown);
    virtual int close(Sensor::kind_t kind = Sensor::KindUnknown);
    virtual int _open(Sensor::kind_t kind);
    virtual int _close(Sensor::kind_t kind);
    virtual Base::Object *parameter(const char *name);
    virtual void setParameter(const char *name, Base::Object *value);
private:
    Sensor *_sensor;
    Delegate _delegate;
    Sensor::kind_t _openKind;
};

inline Sensor *SensorHandle::sensor()
{
    return _sensor;
}
inline SensorHandle::Delegate SensorHandle::delegate()
{
    return _delegate;
}
inline void SensorHandle::setDelegate(Delegate value)
{
    _delegate = value;
}

struct SENSORKIT_APISYM GpsData
{
    enum
    {
        ModeUnknown = 0,
        ModeAutonomous = 'A',
        ModeDifferential = 'D',
        ModeEstimated = 'E',
        ModeManualInput = 'M',
        ModeSimulated = 'S',
        ModeNoFix = 'N'
    };
    int mode;                           /* FAA mode */
    double timestamp;                   /* timestamp: julian date in UTC */
    double e_timestamp;                 /* timestamp error: seconds */
    double lat, lon;                    /* latitude, longitude */
    double alt;                         /* altitude */
    double e_lat, e_lon;                /* latitude, longitude error: meters */
    double e_alt;                       /* altitude error: meters */
    double track;                       /* track over ground: true degrees */
    double speed;                       /* speed over ground: meters per second */
    double climb;                       /* positive/negative rate of climb: meters per second */
    double e_track;                     /* track error: degrees */
    double e_speed;                     /* speed error: meters per second */
    double e_climb;                     /* climb error: meters per second */
};
inline GpsData GpsDataNone()
{
    return (GpsData)
    {
        GpsData::ModeUnknown,           /* mode */
        Base::None(),                   /* timestamp */
        Base::None(),                   /* e_timestamp */
        Base::None(), Base::None(),     /* lat, lon */
        Base::None(),                   /* alt */
        Base::None(), Base::None(),     /* e_lat, e_lon */
        Base::None(),                   /* e_alt */
        Base::None(),                   /* track */
        Base::None(),                   /* speed */
        Base::None(),                   /* climb */
        Base::None(),                   /* e_track */
        Base::None(),                   /* e_speed */
        Base::None(),                   /* e_climb */
    };
}
struct SENSORKIT_APISYM GpsSensorHandle : Base::Interface
{
    virtual GpsData gpsData() = 0;
};

struct SENSORKIT_APISYM GpsSkyData
{
    double timestamp;                   /* timestamp: julian date in UTC */
    size_t satelliteCount;
    struct Satellite
    {
        unsigned prn;                   /* satellite PRN number (1 - 32) */
        double elevation;               /* elevation in degrees (0 - 90) */
        double azimuth;                 /* azimuth in degrees (0 - 360) */
        double snr;                     /* signal to noise ratio (0 - 99 dB Hz) */
    } satellites[12];
};
inline GpsSkyData GpsSkyDataNone()
{
    return (GpsSkyData)
    {
        Base::None(),                   /* timestamp */
        0,                              /* satelliteCount */
    };
}
struct SENSORKIT_APISYM GpsSkySensorHandle : Base::Interface
{
    virtual GpsSkyData gpsSkyData() = 0;
};

struct SENSORKIT_APISYM CompassData
{
    double timestamp;                   /* timestamp: julian date in UTC */
    double e_timestamp;                 /* timestamp error: seconds */
    double trueHeading;                 /* heading: true degrees */
    double magneticHeading;             /* heading: magnetic degrees */
    double e_heading;                   /* heading error: degrees */
};
inline CompassData CompassDataNone()
{
    return (CompassData)
    {
        Base::None(),                   /* timestamp */
        Base::None(),                   /* e_timestamp */
        Base::None(),                   /* trueHeading */
        Base::None(),                   /* magneticHeading */
        Base::None(),                   /* e_heading */
    };
}
struct SENSORKIT_APISYM CompassSensorHandle : Base::Interface
{
    virtual CompassData compassData() = 0;
};

struct SENSORKIT_APISYM AisData
{
    enum
    {
        StatusUnderwayUsingEngine = 0,
        StatusAtAnchor = 1,
        StatusNotUnderCommand = 2,
        StatusRestrictedManeuverability = 3,
        StatusConstrainedByHerDraft = 4,
        StatusMoored = 5,
        StatusAground = 6,
        StatusEngagedInFishing = 7,
        StatusUnderwaySailing = 8,
        StatusReserved9 = 9,
        StatusReserved10 = 10,
        StatusReserved11 = 11,
        StatusReserved12 = 12,
        StatusReserved13 = 13,
        StatusAisSart = 14,
        StatusUnknown = 15,
    };
    unsigned mmsi;                      /* MMSI: Mobile Marine Service Identifier */
    int status;                         /* navigation status */
    double timestamp;                   /* timestamp: julian date in UTC */
    double lat, lon;                    /* latitude, longitude */
    double e_lat, e_lon;                /* latitude, longitude error: meters */
    double track;                       /* track over ground: true degrees */
    double speed;                       /* speed over ground: meters per second */
    double turn;                        /* rate of turn: degrees per second */
    double trueHeading;                 /* heading: true degrees */
};
inline AisData AisDataNone()
{
    return (AisData)
    {
        Base::None(),                   /* MMSI: Mobile Marine Service Identifier */
        AisData::StatusUnknown,         /* navigation status */
        Base::None(),                   /* timestamp: julian date in UTC */
        Base::None(), Base::None(),     /* latitude, longitude */
        Base::None(), Base::None(),     /* latitude, longitude error: meters */
        Base::None(),                   /* track over ground: true degrees */
        Base::None(),                   /* speed over ground: meters per second */
        Base::None(),                   /* rate of turn: degrees per second */
        Base::None(),                   /* heading: true degrees */
    };
}
struct SENSORKIT_APISYM AisVesselData
{
    enum
    {
        TypeUnknown = 0,
        TypeWig = 20,
        TypeWigCategoryA = 21,
        TypeWigCategoryB = 22,
        TypeWigCategoryC = 23,
        TypeWigCategoryD = 24,
        TypeWigNoAdditionalInfo = 29,
        TypeFishing = 30,
        TypeTowing = 31,
        TypeTowingExceeds200m = 32,     /* Towing and length of tow exceeds 200m or breadth exceeds 25m */
        TypeDredgingOp = 33,
        TypeDivingOp = 34,
        TypeMilitaryOp = 35,
        TypeSailing = 36,
        TypePleasure = 37,
        TypeHsc = 40,
        TypeHscCategoryA = 41,
        TypeHscCategoryB = 42,
        TypeHscCategoryC = 43,
        TypeHscCategoryD = 44,
        TypeHscNoAdditionalInfo = 49,
        TypePilot = 50,
        TypeRescue = 51,
        TypeTug = 52,
        TypePortTender = 53,
        TypeAntiPolution = 54,
        TypeLawEnforcement = 55,
        TypeMedical = 58,
        TypeNonCombatant = 59,
        TypePassenger = 60,
        TypePassengerCategoryA = 61,
        TypePassengerCategoryB = 62,
        TypePassengerCategoryC = 63,
        TypePassengerCategoryD = 64,
        TypePassengerNoAdditionalInfo = 69,
        TypeCargo = 70,
        TypeCargoCategoryA = 71,
        TypeCargoCategoryB = 72,
        TypeCargoCategoryC = 73,
        TypeCargoCategoryD = 74,
        TypeCargoNoAdditionalInfo = 79,
        TypeTanker = 80,
        TypeTankerCategoryA = 81,
        TypeTankerCategoryB = 82,
        TypeTankerCategoryC = 83,
        TypeTankerCategoryD = 84,
        TypeTankerNoAdditionalInfo = 89,
        TypeOther = 90,
        TypeOtherCategoryA = 91,
        TypeOtherCategoryB = 92,
        TypeOtherCategoryC = 93,
        TypeOtherCategoryD = 94,
        TypeOtherNoAdditionalInfo = 99,
    };
    enum
    {
        EpfdUnknown = 0,
        EpfdGps = 1,
        EpfdGlonass = 2,
        EpfdGpsGlonass = 3,
        EpfdLoranC = 4,
        EpfdChayka = 5,
        EpfdIntegratedNavigationSystem = 6,
        EpfdSurveyed = 7,
        EpfdGalileo = 8,
    };
    unsigned mmsi;                      /* MMSI: Mobile Marine Service Identifier */
    unsigned imo;                       /* IMO: International Maritime Organization number */
    char callsign[7 + 1];               /* call sign */
    char name[20 + 1];                  /* vessel name */
    int type;                           /* vessel type */
    double length;                      /* vessel length: meters */
    double beam;                        /* vessel beam: meters */
    double draft;                       /* vessel draft: meters */
    double antennaLongitudinalPosition; /* vessel antenna longitudinal position: bow=0, stern=1 */
    double antennaLateralPosition;      /* vessel antenna lateral position: port=0, starboard=1 */
    int epfd;                           /* Electronic Position Fixing Device */
    char destination[20 + 1];           /* destination */
    double eta;                         /* Estimated Time of Arrival: julian date in UTC */
};
inline AisVesselData AisVesselDataNone()
{
    return (AisVesselData)
    {
        Base::None(),                   /* MMSI: Mobile Marine Service Identifier */
        Base::None(),                   /* IMO: International Maritime Organization number */
        { 0 },                          /* call sign */
        { 0 },                          /* vessel name */
        AisVesselData::TypeUnknown,     /* vessel type */
        Base::None(),                   /* vessel length: meters */
        Base::None(),                   /* vessel beam: meters */
        Base::None(),                   /* vessel draft: meters */
        Base::None(),                   /* vessel antenna longitudinal position: bow=0, stern=1 */
        Base::None(),                   /* vessel antenna lateral position: port=0, starboard=1 */
        AisVesselData::EpfdUnknown,     /* Electronic Position Fixing Device */
        { 0 },                          /* destination */
        Base::None(),                   /* Estimated Time of Arrival: julian date in UTC */
    };
}
struct SENSORKIT_APISYM AisSensorHandle : Base::Interface
{
    virtual void aisData(const AisData **pDataArray, const AisVesselData **pVesselDataArray) = 0;
};

struct SENSORKIT_APISYM ErrorSensorHandle : Base::Interface
{
    virtual Base::Error *error() = 0;
};

}
}

#endif // SENSORKIT_IMPL_SENSOR_HPP_INCLUDED
