/*
 * SensorKit/impl/SensorManager.hpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef SENSORKIT_IMPL_SENSORMANAGER_HPP_INCLUDED
#define SENSORKIT_IMPL_SENSORMANAGER_HPP_INCLUDED

#include <SensorKit/impl/Defines.hpp>
#include <SensorKit/impl/Sensor.hpp>

namespace SensorKit {
namespace impl {

class SENSORKIT_APISYM SensorManager : public Base::Object
{
public:
    static SensorManager *sharedSensorManager();
    SensorManager();
    ~SensorManager();
    bool addNewSensor(const char *name, Base::Map *params = 0);
    void addSensor(Sensor *sensor);
    void removeSensor(Sensor *sensor);
    void removeAllSensors();
    Sensor **sensors(Sensor::kind_t kind);
private:
    typedef Base::GenericSet<Sensor> SensorSet;
    typedef Base::GenericIndexMap<SensorSet> SensorMap;
    Base::Lock *_lock;
    SensorMap *_sensors;
};

}
}

#endif // SENSORKIT_IMPL_SENSORMANAGER_HPP_INCLUDED
