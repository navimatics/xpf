/*
 * SensorKit/Defines.hpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef SENSORKIT_DEFINES_HPP_INCLUDED
#define SENSORKIT_DEFINES_HPP_INCLUDED

#include <SensorKit/impl/Defines.hpp>

namespace SensorKit {
}

#endif // SENSORKIT_DEFINES_HPP_INCLUDED
