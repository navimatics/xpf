/*
 * Base/jimpl/NativeString.cpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#include <Base/Jvm.hpp>
#if defined(BASE_CONFIG_JVM)

namespace Base {
namespace jimpl {

static jstring JNICALL _toString(JNIEnv *env, jobject jself)
{
    mark_and_collect
    {
        CString *self = (CString *)JvmNativeObject(env, jself);
        return JvmJavaString(env, self->chars());
    }
}
static jstring JNICALL _create(JNIEnv *env, jclass jcls, jstring jstr)
{
    mark_and_collect
    {
        CString *self = cstr_obj(JvmCString(env, jstr));
        jstring result = 0;
        if (0 == self)
            JvmThrowException(env, "java/lang/NullPointerException", 0);
        else
            result = (jstring)JvmJavaObject(env, self);
        return result;
    }
}

static JNINativeMethod methods[] =
{
    BASE_JVM_NATIVEMETHOD(_toString, "()Ljava/lang/String;"),
    BASE_JVM_NATIVEMETHOD(_create, "(Ljava/lang/String;)L" BASE_JVM_BASEPACKAGE "/NativeString;"),
};
void NativeStringRegister()
{
    JvmRegisterNativeMethods(BASE_JVM_BASEPACKAGE "/NativeString", methods, NELEMS(methods));
    JvmRegisterPeerTypes(BASE_JVM_BASEPACKAGE "/NativeString", typeid(CString));
}

}
}
#endif
