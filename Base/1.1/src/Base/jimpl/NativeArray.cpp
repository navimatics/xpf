/*
 * Base/jimpl/NativeArray.cpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#include <Base/Jvm.hpp>
#if defined(BASE_CONFIG_JVM)
#include <Base/Array.hpp>

namespace Base {
namespace jimpl {

static jboolean JNICALL add1(JNIEnv *env, jobject jself, jobject jobj)
{
    mark_and_collect
    {
        Array *self = (Array *)JvmNativeObject(env, jself);
        Object *obj = JvmNativeObject(env, jobj);
        self->addObject(obj);
        return true;
    }
}
static void JNICALL add2(JNIEnv *env, jobject jself, jint index, jobject jobj)
{
    mark_and_collect
    {
        Array *self = (Array *)JvmNativeObject(env, jself);
        ssize_t count = self->count();
        if (0 > index || index > count)
            JvmThrowException(env, "java/lang/IndexOutOfBoundsException");
        else
        {
            Object *obj = JvmNativeObject(env, jobj);
            self->insertObject(index, obj);
        }
    }
}
static void JNICALL clear(JNIEnv *env, jobject jself)
{
    mark_and_collect
    {
        Array *self = (Array *)JvmNativeObject(env, jself);
        self->removeAllObjects();
    }
}
static jobject JNICALL get(JNIEnv *env, jobject jself, jint index)
{
    mark_and_collect
    {
        Array *self = (Array *)JvmNativeObject(env, jself);
        jobject result = 0;
        ssize_t count = self->count();
        if (0 > index || index >= count)
            JvmThrowException(env, "java/lang/IndexOutOfBoundsException");
        else
            result = JvmJavaObject(env, self->object(index));
        return result;
    }
}
static jobject JNICALL remove(JNIEnv *env, jobject jself, jint index)
{
    mark_and_collect
    {
        Array *self = (Array *)JvmNativeObject(env, jself);
        jobject result = 0;
        ssize_t count = self->count();
        if (0 > index || index >= count)
            JvmThrowException(env, "java/lang/IndexOutOfBoundsException");
        else
        {
            result = JvmJavaObject(env, self->object(index));
            self->removeObject(index);
        }
        return result;
    }
}
static jobject JNICALL set(JNIEnv *env, jobject jself, jint index, jobject jobj)
{
    mark_and_collect
    {
        Array *self = (Array *)JvmNativeObject(env, jself);
        jobject result = 0;
        ssize_t count = self->count();
        if (0 > index || index >= count)
            JvmThrowException(env, "java/lang/IndexOutOfBoundsException");
        else
        {
            Object *obj = JvmNativeObject(env, jobj);
            result = JvmJavaObject(env, self->object(index));
            self->replaceObject(index, obj);
        }
        return result;
    }
}
static jint JNICALL size(JNIEnv *env, jobject jself)
{
    mark_and_collect
    {
        Array *self = (Array *)JvmNativeObject(env, jself);
        return self->count();
    }
}
static jint JNICALL _indexOf(JNIEnv *env, jobject jself, jobject jobj, jint start, jint end)
{
    mark_and_collect
    {
        if (!env->IsInstanceOf(jobj, JvmJavaObjectBaseClass()))
            return -1;
        Array *self = (Array *)JvmNativeObject(env, jself);
        Object *obj = JvmNativeObject(env, jobj);
        Object **objs = self->objects();
        if (0 != obj)
            for (Object **p = objs + start, **endp = objs + end; endp > p; p++)
            {
                Object *o = *p;
                if (0 == obj->cmp(o))
                    return p - objs;
            }
        else
            for (Object **p = objs + start, **endp = objs + end; endp > p; p++)
            {
                Object *o = *p;
                if (0 == o)
                    return p - objs;
            }
        return -1;
    }
}
static jint JNICALL _lastIndexOf(JNIEnv *env, jobject jself, jobject jobj, jint start, jint end)
{
    mark_and_collect
    {
        if (!env->IsInstanceOf(jobj, JvmJavaObjectBaseClass()))
            return -1;
        Array *self = (Array *)JvmNativeObject(env, jself);
        Object *obj = JvmNativeObject(env, jobj);
        Object **objs = self->objects();
        if (0 != obj)
            for (Object **p = objs + end - 1; objs + start <= p; p--)
            {
                Object *o = *p;
                if (0 == obj->cmp(o))
                    return p - objs;
            }
        else
            for (Object **p = objs + end - 1; objs + start <= p; p--)
            {
                Object *o = *p;
                if (0 == o)
                    return p - objs;
            }
        return -1;
    }
}
static jobjectArray JNICALL _toArray(JNIEnv *env, jobject jself, jobjectArray jarray, jclass jelemCls,
    jint start, jint end)
{
    mark_and_collect
    {
        Array *self = (Array *)JvmNativeObject(env, jself);
        Object **objs = self->objects();
        ssize_t count = end - start;
        jobjectArray result = 0 == jarray || count > env->GetArrayLength(jarray) ?
            env->NewObjectArray(count, jelemCls, 0) :
            jarray;
        if (0 == result)
            return 0;
        for (ssize_t index = 0; count > index; index++)
            env->SetObjectArrayElement(result, index, JvmJavaObject(env, objs[start + index]));
        return result;
    }
}
static void JNICALL _ctor(JNIEnv *env, jobject jself)
{
    mark_and_collect
    {
        Array *self = new Array;
        JvmSetNativeObject(env, jself, self);
    }
}

static JNINativeMethod methods[] =
{
    BASE_JVM_NATIVEMETHOD_EX("add", "(L" BASE_JVM_JAVAOBJECTBASECLASS ";)Z", add1),
    BASE_JVM_NATIVEMETHOD_EX("add", "(IL" BASE_JVM_JAVAOBJECTBASECLASS ";)V", add2),
    BASE_JVM_NATIVEMETHOD(clear, "()V"),
    BASE_JVM_NATIVEMETHOD(get, "(I)L" BASE_JVM_JAVAOBJECTBASECLASS ";"),
    BASE_JVM_NATIVEMETHOD(remove, "(I)L" BASE_JVM_JAVAOBJECTBASECLASS ";"),
    BASE_JVM_NATIVEMETHOD(set, "(IL" BASE_JVM_JAVAOBJECTBASECLASS ";)L" BASE_JVM_JAVAOBJECTBASECLASS ";"),
    BASE_JVM_NATIVEMETHOD(size, "()I"),
    BASE_JVM_NATIVEMETHOD(_indexOf, "(Ljava/lang/Object;II)I"),
    BASE_JVM_NATIVEMETHOD(_lastIndexOf, "(Ljava/lang/Object;II)I"),
    BASE_JVM_NATIVEMETHOD(_toArray, "([Ljava/lang/Object;Ljava/lang/Class;II)[Ljava/lang/Object;"),
    BASE_JVM_NATIVEMETHOD(_ctor, "()V"),
};
void NativeArrayRegister()
{
    JvmRegisterNativeMethods(BASE_JVM_BASEPACKAGE "/NativeArray", methods, NELEMS(methods));
    JvmRegisterPeerTypes(BASE_JVM_BASEPACKAGE "/NativeArray", typeid(Array));
}

}
}
#endif
