/*
 * Base/jimpl/NativeSet.cpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#include <Base/Jvm.hpp>
#if defined(BASE_CONFIG_JVM)
#include <Base/CArray.hpp>
#include <Base/Set.hpp>

namespace Base {
namespace jimpl {

static jboolean JNICALL add(JNIEnv *env, jobject jself, jobject jobj)
{
    mark_and_collect
    {
        Set *self = (Set *)JvmNativeObject(env, jself);
        Object *obj = JvmNativeObject(env, jobj);
        jboolean result = false;
        if (0 == obj)
            JvmThrowException(env, "java/lang/NullPointerException", "element cannot be null");
        else
        {
            result = !self->containsObject(obj);
            if (result)
                self->addObject(obj);
        }
        return result;
    }
}
static void JNICALL clear(JNIEnv *env, jobject jself)
{
    mark_and_collect
    {
        Set *self = (Set *)JvmNativeObject(env, jself);
        self->removeAllObjects();
    }
}
static jboolean JNICALL contains(JNIEnv *env, jobject jself, jobject jobj)
{
    mark_and_collect
    {
        if (!env->IsInstanceOf(jobj, JvmJavaObjectBaseClass()))
            return false;
        Set *self = (Set *)JvmNativeObject(env, jself);
        Object *obj = JvmNativeObject(env, jobj);
        jboolean result = false;
        if (0 == obj)
            JvmThrowException(env, "java/lang/NullPointerException", "element cannot be null");
        else
            result = self->containsObject(obj);
        return result;
    }
}
static jboolean JNICALL remove(JNIEnv *env, jobject jself, jobject jobj)
{
    mark_and_collect
    {
        if (!env->IsInstanceOf(jobj, JvmJavaObjectBaseClass()))
            return false;
        Set *self = (Set *)JvmNativeObject(env, jself);
        Object *obj = JvmNativeObject(env, jobj);
        jboolean result = false;
        if (0 == obj)
            JvmThrowException(env, "java/lang/NullPointerException", "element cannot be null");
        else
        {
            result = self->containsObject(obj);
            if (result)
                self->removeObject(obj);
        }
        return result;
    }
}
static jint JNICALL size(JNIEnv *env, jobject jself)
{
    mark_and_collect
    {
        Set *self = (Set *)JvmNativeObject(env, jself);
        return self->count();
    }
}
static jobjectArray JNICALL _toArray(JNIEnv *env, jobject jself, jobjectArray jarray, jclass jelemCls)
{
    mark_and_collect
    {
        Set *self = (Set *)JvmNativeObject(env, jself);
        Object **objs = self->objects();
        ssize_t count = carr_length(objs);
        jobjectArray result = 0 == jarray || count > env->GetArrayLength(jarray) ?
            env->NewObjectArray(count, jelemCls, 0) :
            jarray;
        if (0 == result)
            return 0;
        for (ssize_t index = 0; count > index; index++)
            env->SetObjectArrayElement(result, index, JvmJavaObject(env, objs[index]));
        return result;
    }
}
static void JNICALL _ctor(JNIEnv *env, jobject jself)
{
    mark_and_collect
    {
        Set *self = new Set;
        JvmSetNativeObject(env, jself, self);
    }
}

static JNINativeMethod methods[] =
{
    BASE_JVM_NATIVEMETHOD(add, "(L" BASE_JVM_JAVAOBJECTBASECLASS ";)Z"),
    BASE_JVM_NATIVEMETHOD(clear, "()V"),
    BASE_JVM_NATIVEMETHOD(contains, "(Ljava/lang/Object;)Z"),
    BASE_JVM_NATIVEMETHOD(remove, "(Ljava/lang/Object;)Z"),
    BASE_JVM_NATIVEMETHOD(size, "()I"),
    BASE_JVM_NATIVEMETHOD(_toArray, "([Ljava/lang/Object;Ljava/lang/Class;)[Ljava/lang/Object;"),
    BASE_JVM_NATIVEMETHOD(_ctor, "()V"),
};
void NativeSetRegister()
{
    JvmRegisterNativeMethods(BASE_JVM_BASEPACKAGE "/NativeSet", methods, NELEMS(methods));
    JvmRegisterPeerTypes(BASE_JVM_BASEPACKAGE "/NativeSet", typeid(Set));
}

}
}
#endif
