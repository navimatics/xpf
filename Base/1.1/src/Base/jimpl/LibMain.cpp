/*
 * Base/jimpl/LibMain.cpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#include <Base/Jvm.hpp>
#if defined(BASE_CONFIG_JVM)

namespace Base {
namespace jimpl {
void DateTimeRegister();
void NativeArrayRegister();
void NativeMapRegister();
void NativeObjectRegister();
void NativeObjectUtilRegister();
void NativeSetRegister();
void NativeStringRegister();
}
}

extern "C"
JNIEXPORT jint JNICALL JNI_OnLoad(JavaVM *jvm, void *reserved)
{
    mark_and_collect
    {
        Base::impl::_JvmInitializePeerMapping();
        Base::jimpl::DateTimeRegister();
        Base::jimpl::NativeArrayRegister();
        Base::jimpl::NativeMapRegister();
        Base::jimpl::NativeObjectRegister();
        Base::jimpl::NativeObjectUtilRegister();
        Base::jimpl::NativeSetRegister();
        Base::jimpl::NativeStringRegister();
        return JNI_VERSION_1_2;
    }
}
#endif
