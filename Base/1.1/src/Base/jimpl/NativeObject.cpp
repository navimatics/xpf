/*
 * Base/jimpl/NativeObject.cpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#include <Base/Jvm.hpp>
#if defined(BASE_CONFIG_JVM)
#include <Base/Synchronization.hpp>

namespace Base {
namespace jimpl {

static jboolean JNICALL equals(JNIEnv *env, jobject jself, jobject jobj)
{
    mark_and_collect
    {
        if (0 == jobj)
            return false;
        if (env->IsSameObject(jself, jobj))
            return true;
        if (!env->IsInstanceOf(jobj, JvmJavaObjectBaseClass()))
            return false;
        Object *self = JvmNativeObject(env, jself);
        Object *obj = JvmNativeObject(env, jobj);
        return 0 == self->cmp(obj);
    }
}
static jint JNICALL hashCode(JNIEnv *env, jobject jself)
{
    mark_and_collect
    {
        Object *self = JvmNativeObject(env, jself);
        return self->hash();
    }
}
static jstring JNICALL toString(JNIEnv *env, jobject jself)
{
    mark_and_collect
    {
        Object *self = JvmNativeObject(env, jself);
        return JvmJavaString(env, cstringf("%s %s",
            JvmClassName(env, jself, true), self->strrepr()));
    }
}
static void JNICALL _ctor(JNIEnv *env, jobject jself)
{
    mark_and_collect
    {
        Object *self = new Object;
        JvmSetNativeObject(env, jself, self);
    }
}

static JNINativeMethod methods[] =
{
    BASE_JVM_NATIVEMETHOD(equals, "(Ljava/lang/Object;)Z"),
    BASE_JVM_NATIVEMETHOD(hashCode, "()I"),
    BASE_JVM_NATIVEMETHOD(toString, "()Ljava/lang/String;"),
    BASE_JVM_NATIVEMETHOD(_ctor, "()V"),
    BASE_JVM_NATIVEMETHOD_EX("dispose", "()V", impl::_JvmJavaObjectDispose),
};
static JNINativeMethod FinalReference_methods[] =
{
    BASE_JVM_NATIVEMETHOD_EX("dispose", "()V", impl::_JvmFinalReferenceDispose),
};
void NativeObjectRegister()
{
    JvmRegisterNativeMethods(BASE_JVM_JAVAOBJECTBASECLASS, methods, NELEMS(methods));
    JvmRegisterNativeMethods(BASE_JVM_JAVAOBJECTBASECLASS "$FinalReference",
        FinalReference_methods, NELEMS(FinalReference_methods));
    JvmRegisterPeerTypes(BASE_JVM_JAVAOBJECTBASECLASS, typeid(Object));
}

}
}
#endif
