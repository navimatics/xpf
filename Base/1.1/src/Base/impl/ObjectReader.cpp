/*
 * Base/impl/ObjectReader.cpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#include <Base/impl/ObjectReader.hpp>
#include <Base/impl/Value.hpp>

namespace Base {
namespace impl {

ObjectReader::ObjectReader(Stream *stream) : JsonParser(stream)
{
    _stack = new Array;
    _stack->autocollectObjects(false);
    _keystack = new Array;
    _keystack->autocollectObjects(false);
    _idmap = new Map;
    _idmap->autocollectObjects(false);
    _queue = new Array;
    _queue->autocollectObjects(false);
    setFlags(ObjectCodecExtensions, 1);
}
ObjectReader::~ObjectReader()
{
    _queue->release();
    _idmap->release();
    if (0 != _text)
        _text->release();
    _keystack->release();
    _stack->release();
    if (0 != _typeset)
        _typeset->release();
}
void ObjectReader::reset()
{
    _finished = false;
    if (0 != _typeset)
        _typeset->removeAllObjects();
    _stack->removeAllObjects();
    _keystack->removeAllObjects();
    if (0 != _text)
    {
        _text->release();
        _text = 0;
    }
    _idmap->removeAllObjects();
    _queue->removeAllObjects();
    _qindex = 0;
    JsonParser::reset();
    setFlags(ObjectCodecExtensions, 1);
}
void ObjectReader::addType(const char *type)
{
    if (0 == _typeset)
    {
        _typeset = new Set;
        _typeset->autocollectObjects(false);
    }
    CString *str = CString::create(type);
    _typeset->addObject(str);
    str->release();
}
void ObjectReader::removeType(const char *type)
{
    if (0 != _typeset)
        _typeset->removeObject(BASE_ALLOCA_OBJECT(TransientCString, type));
}
Object *ObjectReader::readObject()
{
    for (;;)
    {
        if (_qindex < _queue->count())
            return _queue->object(_qindex++);
        _queue->removeAllObjects();
        _qindex = 0;
        if (_finished)
            return 0;
        _finished = 0 >= parse();
    }
}
bool ObjectReader::finished()
{
    return _finished;
}
void ObjectReader::setFinished(bool value)
{
    _finished = value;
}
Object *ObjectReader::ancestorObject(ssize_t index)
{
    return _stack->object(index);
}
void ObjectReader::objectComplete(Object *obj)
{
    if (0 != _typeset)
    {
        if (!_typeset->containsObject(cstr_obj(obj->className())))
            return;
    }
    else if (1 < _stack->count())
        return;
    _queue->addObject(obj);
}
void ObjectReader::startArray()
{
    getCurrentObject();
        /* ensure parent exists */
    Object *obj = new Array;
    _stack->addObject(obj);
    obj->release();
    _keystack->addObject(0); /* codec */
    _keystack->addObject(0); /* key(s) */
}
void ObjectReader::endArray()
{
    _keystack->removeObject(-1);
    _keystack->removeObject(-1);
    Object *obj = getCurrentObject();
    Object *anc = _stack->object(-2);
    if (0 != anc)
    {
        ObjectCodec::PropertyDescr *prop = getCurrentPropertyDescr();
        if (0 != prop)
            ObjectCodec::setObjectProperty(anc, prop, obj);
        else
            dynamicAddObject(anc, obj);
    }
    objectComplete(obj);
    _stack->removeObject(-1);
}
void ObjectReader::startObject()
{
    getCurrentObject();
        /* ensure parent exists */
    _stack->addObject(0);
    _keystack->addObject(0); /* codec */
    _keystack->addObject(0); /* key(s) */
}
void ObjectReader::endObject()
{
    Object *obj = getCurrentObject();
    Object *anc = _stack->object(-2);
    if (ObjectCodec *codec = (ObjectCodec *)_keystack->object(-2))
        codec->postCreateObject(obj);
    _keystack->removeObject(-1);
    _keystack->removeObject(-1);
    if (0 != anc)
    {
        ObjectCodec::PropertyDescr *prop = getCurrentPropertyDescr();
        if (0 != prop)
            ObjectCodec::setObjectProperty(anc, prop, obj);
        else
            dynamicAddObject(anc, obj);
    }
    objectComplete(obj);
    _stack->removeObject(-1);
}
void ObjectReader::nullValue()
{
    Object *obj = getCurrentObject();
    ObjectCodec::PropertyDescr *prop = getCurrentPropertyDescr();
    if (0 != prop)
        ObjectCodec::setObjectProperty(obj, prop, 0);
    else
        dynamicAddObject(obj, 0);
}
void ObjectReader::booleanValue(bool B)
{
    Object *obj = getCurrentObject();
    ObjectCodec::PropertyDescr *prop = getCurrentPropertyDescr();
    if (0 != prop)
        ObjectCodec::setBooleanProperty(obj, prop, B);
    else
    {
        Object *val = Value::create(B);
        dynamicAddObject(obj, val);
        val->release();
    }
}
void ObjectReader::integerValue(long l)
{
    Object *obj = getCurrentObject();
    ObjectCodec::PropertyDescr *prop = getCurrentPropertyDescr();
    if (0 != prop)
        ObjectCodec::setIntegerProperty(obj, prop, l);
    else
    {
        Object *val = Value::create(l);
        dynamicAddObject(obj, val);
        val->release();
    }
}
void ObjectReader::realValue(double d)
{
    Object *obj = getCurrentObject();
    ObjectCodec::PropertyDescr *prop = getCurrentPropertyDescr();
    if (0 != prop)
        ObjectCodec::setRealProperty(obj, prop, d);
    else
    {
        Object *val = Value::create(d);
        dynamicAddObject(obj, val);
        val->release();
    }
}
void ObjectReader::characterData(const char *s, size_t len, bool isFinal, bool isKey)
{
    if (0 == _text)
        _text = CString::create(s, len);
    else
        _text = _text->_concat(s, len);
    if (isFinal)
    {
        if (isKey)
            _keystack->replaceObject(-1, _text);
        else
        {
            int specialKey = 0;
            if (flags(_ObjectCodecExtension))
            {
                CString *key = (CString *)_keystack->object(-1);
                if (0 != key)
                {
                    const char *k = key->chars();
                    if ('\\' == k[0] && '/' == k[1] && '/' == k[2])
                    {
                        if ('t' == k[3] && '\0' == k[4])
                            specialKey = 't';
                        else if ('i' == k[3] && '\0' == k[4])
                            specialKey = 'i';
                    }
                }
            }
            Object *obj = _stack->object(-1);
            if (0 == obj && 1 <= _stack->count())
            {
                if ('t' == specialKey)
                {
                    ObjectCodec *codec = ObjectCodec::getObjectCodecByName(_text);
                    if (0 != codec)
                    {
                        _keystack->replaceObject(-2, codec);
                        obj = codec->createObject();
                    }
                    else
                        specialKey = 0;
                }
                if (0 == obj)
                    obj = new Map;
                _stack->replaceObject(-1, obj);
                obj->release();
            }
            if ('i' == specialKey && 0 != obj)
                _idmap->setObject(_text, obj);
            if (0 == specialKey)
            {
                ObjectCodec::PropertyDescr *prop = getCurrentPropertyDescr();
                const char *t = _text->chars();
                if ('\\' == t[0] && '/' == t[1] && '/' == t[2])
                {
                    if (0 != prop)
                        ObjectCodec::setObjectProperty(obj, prop, _idmap->object(_text));
                    else
                        dynamicAddObject(obj, _idmap->object(_text));
                }
                else
                {
                    if (0 != prop)
                        ObjectCodec::setStringProperty(obj, prop, _text->chars());
                    else
                        dynamicAddObject(obj, _text);
                }
            }
        }
        _text->release();
        _text = 0;
    }
}
Object *ObjectReader::getCurrentObject()
{
    Object *obj = _stack->object(-1);
    if (0 == obj && 1 <= _stack->count())
    {
        obj = new Map;
        _stack->replaceObject(-1, obj);
        obj->release();
    }
    return obj;
}
ObjectCodec::PropertyDescr *ObjectReader::getCurrentPropertyDescr()
{
    ObjectCodec *codec = (ObjectCodec *)_keystack->object(-2);
    if (0 == codec)
        return 0;
    CString *key = (CString *)_keystack->object(-1);
    if (0 == key)
        return 0;
    return codec->getPropertyDescr(key->chars());
}
void ObjectReader::dynamicAddObject(Object *anc, Object *obj)
{
    if (Map *map = dynamic_cast<Map *>(anc))
    {
        Object *key = _keystack->object(-1);
        if (0 != key)
            map->setObject(key, obj);
    }
    else if (Array *array = dynamic_cast<Array *>(anc))
        array->addObject(obj);
}

}
}
