/*
 * Base/impl/Set.cpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#include <Base/impl/Set.hpp>
#include <Base/impl/CArray.hpp>
#include <Base/impl/CString.hpp>

namespace Base {
namespace impl {

Set::Set(ObjectComparer *comparer) :
    _comparerRef(0 == comparer ? 0 : comparer->self()),
    _set(Object_hash_t(comparer), Object_equal_t(comparer))
{
}
void Set::autocollectObjects(bool ac)
{
    _nac = !ac;
}
size_t Set::count()
{
    return _set.size();
}
bool Set::containsObject(Object *obj)
{
    set_t::iterator iter = _set.find(Ref<Object>(obj));
    return iter != _set.end();
}
Object *Set::anyObject()
{
    set_t::iterator iter = _set.begin();
    if (iter != _set.end())
    {
        Object *obj = *iter;
        if (0 == obj)
            return 0;
        return _nac ? obj : obj->autocollect();
    }
    else
        return 0;
}
void Set::addObject(Object *obj)
{
    _set.insert(Ref<Object>(obj));
}
void Set::removeObject(Object *obj)
{
    _set.erase(Ref<Object>(obj));
}
void Set::removeAllObjects()
{
    _set.clear();
}
Object **Set::objects()
{
    Object **objbuf = (Object **)carrayWithCapacity(_set.size(), sizeof(Object *));
    carr_obj(objbuf)->setLength(_set.size());
    Object **objs = objbuf;
    for (set_t::iterator p = _set.begin(), q = _set.end(); p != q; ++p, ++objs)
        *objs = *p;
    return objbuf;
}
const char *Set::strrepr()
{
    return cstringf("<%s %p; count=%u>", className(), this, count());
}
/* Iterable<Object *> */
void *Set::iterate(void *state, Object **bufp[2])
{
    if (0 == state)
        state = new (collect) Iterator(_set);
    Iterator *iter = (Iterator *)state;
    Object **curp = bufp[0], **endp = bufp[1];
    while (curp < endp && iter->p != iter->q)
    {
        *curp++ = *iter->p;
        ++iter->p;
    }
    bufp[1] = curp;
    return state;
}
/* ObjectCodec use */
Array *Set::__array()
{
    Array *array = new (collect) Array(_set.size());
    for (set_t::iterator p = _set.begin(), q = _set.end(); p != q; ++p)
        array->addObject(*p);
    return array;
}
void Set::__setArray(Array *array)
{
    foreach (Object *obj, array)
        _set.insert(Ref<Object>(obj));
}
ObjectCodecBegin(Set)
    ObjectCodecProperty("_", &Set::__array, &Set::__setArray)
ObjectCodecEnd(Set)

}
}
