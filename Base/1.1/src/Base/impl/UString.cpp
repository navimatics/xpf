/*
 * Base/impl/UString.cpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#include <Base/impl/UString.hpp>
#include <Base/impl/Synchronization.hpp>
#include <Base/impl/hash_map.hpp>

#define SIZE_T_HIGHBIT                  ((size_t)1 << (sizeof(size_t) * CHAR_BIT - 1))

#define METRIC_IS_CONST_STRING(m)       (!(SIZE_T_HIGHBIT & (m)))
#define HASH_TO_METRIC(h)               (~SIZE_T_HIGHBIT & (h))
#define METRIC_TO_HASH(m)               (m)
#define SIZE_TO_METRIC(s)               (SIZE_T_HIGHBIT | (s))
#define METRIC_TO_SIZE(m)               ((SIZE_T_HIGHBIT & (m)) ? (~SIZE_T_HIGHBIT & (m)) : 0)

namespace Base {
namespace impl {

static inline size_t hash_uchars(const unichar_t *s, size_t length)
{
    /* djb2: see http://www.cse.yorku.ca/~oz/hash.html */
    size_t h = 5381;
    for (const unichar_t *t = s + length; t > s; ++s)
        h = 33 * h + *s;
    return HASH_TO_METRIC(h);
}
static inline int cmp_uchars(const unichar_t *ls, size_t llen, const unichar_t *rs, size_t rlen)
{
    ssize_t dlen = llen - rlen;
    int res = memcmp(ls, rs, (0 > dlen ? llen : rlen) * sizeof(unichar_t));
    return 0 != res ? res : dlen;
}
static inline size_t ustrlen(const unichar_t *s)
{
    const unichar_t *p;
    p = s;
    while (*p)
        p++;
    return p - s;
}

UString *UString::empty()
{
    static UString *str = __empty();
        /* threadsafe: __forcecall_UString_empty */
    return str;
}
UString *UString::create(const unichar_t *s, size_t length)
{
    if ((size_t)-1 == length)
        length = 0 != s ? ustrlen(s) : 0;
    if (0 == length)
        return empty();
    UString *self = new ((length + 1) * sizeof(unichar_t)) UString;
    self->_length = length;
    unichar_t *uchars = (unichar_t *)((char *)self + sizeof(UString));
    if (0 != s)
    {
        memcpy(uchars, s, length * sizeof(unichar_t));
        uchars[length] = 0;
    }
    else
        memset(uchars, 0, (length + 1) * sizeof(unichar_t));
    return self;
}
UString *UString::createWithCapacity(size_t capacity)
{
    capacity++;
    UString *self = new (capacity * sizeof(unichar_t)) UString;
    //self->_length = 0;
    self->_metric = SIZE_TO_METRIC(capacity);
    unichar_t *uchars = (unichar_t *)((char *)self + sizeof(UString));
    uchars[0] = 0;
    return self;
}
size_t UString::hash()
{
    if (!METRIC_IS_CONST_STRING(_metric))
        return hash_uchars(uchars(), _length);
    if (0 == _metric)
        _metric = hash_uchars(uchars(), _length);
    return _metric;
}
int UString::cmp(Object *obj0)
{
    if (UString *obj = dynamic_cast<UString *>(obj0))
        return cmp_uchars(uchars(), length(), obj->uchars(), obj->length());
    return (Object *)this - obj0;
}
void UString::setLength(size_t value)
{
    _length = value;
    if (METRIC_IS_CONST_STRING(_metric))
        _metric = 0; /* reset hash value */
}
UString *UString::slice(ssize_t i, ssize_t j)
{
    if (0 > i)
    {
        i = _length + i;
        if (0 > i)
            i = 0;
    }
    else if (_length < (size_t)i)
        i = _length;
    if (0 >= j)
    {
        j = _length + j;
        if (0 > j)
            j = 0;
    }
    else if (_length < (size_t)j)
        j = _length;
    if (i > j)
        j = i;
    UString *str = UString::create(uchars() + i, j - i);
    str->autorelease();
    return str;
}
UString *UString::_concat(const unichar_t *s, size_t length)
{
    if ((size_t)-1 == length)
        length = 0 != s ? ustrlen(s) : 0;
    if (0 == length)
        return this;
    UString *self = this;
    unichar_t *t = (unichar_t *)((char *)this + sizeof(UString));
    size_t curlen = _length;
    size_t newlen = curlen + length;
    if (newlen >= METRIC_TO_SIZE(_metric))
    {
        size_t capacity = (newlen + 1) * 3 / 2;
        if (capacity < 16)
            capacity = 16;
        self = new (capacity * sizeof(unichar_t)) UString;
        self->_metric = SIZE_TO_METRIC(capacity);
        unichar_t *p = (unichar_t *)((char *)self + sizeof(UString));
        memcpy(p, t, curlen * sizeof(unichar_t));
        t = p;
        release();
    }
    if (0 != s)
    {
        memcpy(t + curlen, s, length * sizeof(unichar_t));
        t[newlen] = 0;
    }
    else
        memset(t + curlen, 0, (length + 1) * sizeof(unichar_t));
    self->_length = newlen;
    return self;
}
void UString::makeImmutable()
{
    if (!METRIC_IS_CONST_STRING(_metric))
        _metric = 0;
}
UString *UString::__empty()
{
    UString *self = new (1 * sizeof(unichar_t)) UString;
    unichar_t *uchars = (unichar_t *)((char *)self + sizeof(UString));
    uchars[0] = 0;
    self->linger();
    return self;
}
static UString *__forcecall_UString_empty = UString::empty();

const unichar_t *ustring(const unichar_t *s, size_t length)
{
    UString *self = UString::create(s, length);
    self->autorelease();
    return (unichar_t *)((char *)self + sizeof(UString));
}
const unichar_t *ustringWithCapacity(size_t capacity)
{
    UString *self = UString::createWithCapacity(capacity);
    self->autorelease();
    return (unichar_t *)((char *)self + sizeof(UString));
}
const unichar_t *ustr_new(const unichar_t *s, size_t length)
{
    UString *self = UString::create(s, length);
    return (unichar_t *)((char *)self + sizeof(UString));
}
const unichar_t *ustr_newWithCapacity(size_t capacity)
{
    UString *self = UString::createWithCapacity(capacity);
    return (unichar_t *)((char *)self + sizeof(UString));
}
const unichar_t *ustr_concat(const unichar_t *s, const unichar_t *t, size_t length)
{
    if (0 == s)
        return ustr_new(t, length);
    else
        return ((UString *)((char *)s - sizeof(UString)))->_concat(t, length)->uchars();
}

}
}
