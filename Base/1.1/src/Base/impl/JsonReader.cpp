/*
 * Base/impl/JsonReader.cpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#include <Base/impl/JsonReader.hpp>
#include <Base/impl/CArray.hpp>
#include <Base/impl/CString.hpp>

namespace Base {
namespace impl {

JsonEvent *JsonEvent::create(int kind)
{
    assert('[' == kind || ']' == kind || '{' == kind || '}' == kind || '0' == kind);
    JsonEvent *self = new JsonEvent;
    self->_kind = kind;
    return self;
}
JsonEvent *JsonEvent::create(int kind, bool B)
{
    assert('B' == kind);
    JsonEvent *self = new JsonEvent;
    self->_kind = kind;
    self->_b = B;
    return self;
}
JsonEvent *JsonEvent::create(int kind, long l)
{
    assert('l' == kind);
    JsonEvent *self = new JsonEvent;
    self->_kind = kind;
    self->_l = l;
    return self;
}
JsonEvent *JsonEvent::create(int kind, double d)
{
    assert('d' == kind);
    JsonEvent *self = new JsonEvent;
    self->_kind = kind;
    self->_d = d;
    return self;
}
JsonEvent *JsonEvent::create(int kind, const char *s, size_t len)
{
    assert('K' == kind || 'T' == kind);
    JsonEvent *self = new JsonEvent;
    self->_kind = kind;
    self->_text = cstr_new(s, len);
    return self;
}
JsonEvent::~JsonEvent()
{
    switch (_kind)
    {
    case '[': case ']':
    case '{': case '}':
    case '0':
    case 'B':
    case 'l':
    case 'd':
        break;
    case 'K':
    case 'T':
        cstr_release(_text);
        break;
    default:
        break;
    }
}
void JsonEvent::_concat(const char *s, size_t len)
{
    assert('K' == _kind || 'T' == _kind);
    _text = cstr_concat(_text, s, len);
}

JsonReader::JsonReader(Stream *stream) : JsonParser(stream)
{
    _queue = new Array;
    _queue->autocollectObjects(false);
}
JsonReader::~JsonReader()
{
    _queue->release();
    if (0 != _textev)
        _textev->release();
}
void JsonReader::reset()
{
    _finished = false;
    if (0 != _textev)
    {
        _textev->release();
        _textev = 0;
    }
    _queue->removeAllObjects();
    _qindex = 0;
    JsonParser::reset();
}
JsonEvent *JsonReader::read()
{
    for (;;)
    {
        if (_qindex < _queue->count())
            return (JsonEvent *)_queue->object(_qindex++);
        _queue->removeAllObjects();
        _qindex = 0;
        if (_finished)
            return 0;
        _finished = 0 >= parse();
    }
}
bool JsonReader::finished()
{
    return _finished;
}
void JsonReader::setFinished(bool value)
{
    _finished = value;
}
void JsonReader::startArray()
{
    JsonEvent *ev = JsonEvent::create('[');
    _queue->addObject(ev);
    ev->release();
}
void JsonReader::endArray()
{
    JsonEvent *ev = JsonEvent::create(']');
    _queue->addObject(ev);
    ev->release();
}
void JsonReader::startObject()
{
    JsonEvent *ev = JsonEvent::create('{');
    _queue->addObject(ev);
    ev->release();
}
void JsonReader::endObject()
{
    JsonEvent *ev = JsonEvent::create('}');
    _queue->addObject(ev);
    ev->release();
}
void JsonReader::nullValue()
{
    JsonEvent *ev = JsonEvent::create('0');
    _queue->addObject(ev);
    ev->release();
}
void JsonReader::booleanValue(bool B)
{
    JsonEvent *ev = JsonEvent::create('B', B);
    _queue->addObject(ev);
    ev->release();
}
void JsonReader::integerValue(long l)
{
    JsonEvent *ev = JsonEvent::create('l', l);
    _queue->addObject(ev);
    ev->release();
}
void JsonReader::realValue(double d)
{
    JsonEvent *ev = JsonEvent::create('d', d);
    _queue->addObject(ev);
    ev->release();
}
void JsonReader::characterData(const char *s, size_t len, bool isFinal, bool isKey)
{
    if (0 == _textev)
        _textev = JsonEvent::create(isKey ? 'K' : 'T', s, len);
    else
        _textev->_concat(s, len);
    if (isFinal)
    {
        _queue->addObject(_textev);
        _textev->release();
        _textev = 0;
    }
}

}
}
