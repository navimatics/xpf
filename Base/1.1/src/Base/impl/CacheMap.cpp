/*
 * Base/impl/CacheMap.cpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#include <Base/impl/CacheMap.hpp>
#include <Base/impl/CArray.hpp>
#include <Base/impl/CString.hpp>

namespace Base {
namespace impl {

CacheMap::CacheMap(ObjectComparer *comparer) :
    _comparerRef(0 == comparer ? 0 : comparer->self()),
    _map(Object_hash_t(comparer), Object_equal_t(comparer))
{
    _maxcnt = SIZE_T_MAX - 1;
}
void CacheMap::autocollectObjects(bool ac)
{
    _nac = !ac;
}
size_t CacheMap::count()
{
    return _map.size();
}
Object *CacheMap::object(Object *key)
{
    map_t::iterator iter = _map.find(Ref<Object>(key));
    if (iter != _map.end())
    {
        _list.splice(_list.begin(), _list, (*iter).second.second);
        Object *obj = iter->second.first;
        if (0 == obj)
            return 0;
        return _nac ? obj : obj->autocollect();
    }
    else
        return 0;
}
bool CacheMap::getObject(Object *key, Object *&obj)
{
    map_t::iterator iter = _map.find(Ref<Object>(key));
    if (iter != _map.end())
    {
        _list.splice(_list.begin(), _list, (*iter).second.second);
        obj = iter->second.first;
        if (0 == obj)
            return true;
        _nac ? obj : obj->autocollect();
        return true;
    }
    else
    {
        obj = 0;
        return false;
    }
}
void CacheMap::setObject(Object *key, Object *obj)
{
    Ref<Object> keyref(key);
    Ref<Object> objref(obj);
    std::pair<map_t::iterator, bool> res =
        _map.insert(map_t::value_type(keyref, entry_t()));
    if (res.second)
    {
        if (_map.size() == _maxcnt + 1)
        {
            Ref<Object> &tmpref = _list.back();
            _map.erase(tmpref);
            tmpref = keyref;
            list_t::iterator iter = _list.end();
            _list.splice(_list.begin(), _list, --iter);
        }
        else
            _list.push_front(keyref);
    }
    else
        _list.splice(_list.begin(), _list, (*res.first).second.second);
    (*res.first).second = entry_t(Ref<Object>(obj), _list.begin());
    assert(_map.size() == _list.size());
}
void CacheMap::removeObject(Object *key)
{
    map_t::iterator iter = _map.find(Ref<Object>(key));
    if (iter != _map.end())
    {
        _list.erase((*iter).second.second);
        _map.erase(iter);
    }
    assert(_map.size() == _list.size());
}
void CacheMap::removeAllObjects()
{
    _list.clear();
    _map.clear();
}
void CacheMap::addObjects(CacheMap *map)
{
    for (map_t::iterator p = map->_map.begin(), q = map->_map.end(); p != q; ++p)
        setObject(p->first, p->second.first);
}
Object **CacheMap::keys()
{
    Object **keybuf = (Object **)carrayWithCapacity(_map.size(), sizeof(Object *));
    carr_obj(keybuf)->setLength(_map.size());
    Object **keys = keybuf;
    for (list_t::iterator p = _list.begin(), q = _list.end(); p != q; ++p, ++keys)
        *keys = *p;
    return keybuf;
}
Object **CacheMap::objects()
{
    Object **objbuf = (Object **)carrayWithCapacity(_map.size(), sizeof(Object *));
    carr_obj(objbuf)->setLength(_map.size());
    Object **objs = objbuf;
    for (list_t::iterator p = _list.begin(), q = _list.end(); p != q; ++p, ++objs)
    {
        map_t::iterator iter = _map.find(*p);
        assert(iter != _map.end());
        *objs = iter->second.first;
    }
    return objbuf;
}
void CacheMap::getKeysAndObjects(Object **&keybuf, Object **&objbuf)
{
    keybuf = (Object **)carrayWithCapacity(_map.size(), sizeof(Object *));
    objbuf = (Object **)carrayWithCapacity(_map.size(), sizeof(Object *));
    carr_obj(keybuf)->setLength(_map.size());
    carr_obj(objbuf)->setLength(_map.size());
    Object **keys = keybuf, **objs = objbuf;
    for (list_t::iterator p = _list.begin(), q = _list.end(); p != q; ++p, ++keys, ++objs)
    {
        *keys = *p;
        map_t::iterator iter = _map.find(*p);
        assert(iter != _map.end());
        *objs = iter->second.first;
    }
}
size_t CacheMap::maxCount()
{
    return _maxcnt;
}
void CacheMap::setMaxCount(size_t value)
{
    _maxcnt = value;
    if (0 == _maxcnt)
        _maxcnt = SIZE_T_MAX - 1;
    else
        while (_map.size() > _maxcnt)
        {
            _map.erase(_list.back());
            _list.pop_back();
        }
}
const char *CacheMap::strrepr()
{
    return cstringf("<%s %p; count=%u>", className(), this, count());
}
/* Iterable<Object *> */
void *CacheMap::iterate(void *state, Object **bufp[2])
{
    if (0 == state)
        state = new (collect) Iterator(_list);
    Iterator *iter = (Iterator *)state;
    Object **curp = bufp[0], **endp = bufp[1];
    while (curp < endp && iter->p != iter->q)
    {
        *curp++ = *iter->p;
        ++iter->p;
    }
    bufp[1] = curp;
    return state;
}

}
}
