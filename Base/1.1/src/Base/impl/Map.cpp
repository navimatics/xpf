/*
 * Base/impl/Map.cpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#include <Base/impl/Map.hpp>
#include <Base/impl/CArray.hpp>
#include <Base/impl/CString.hpp>

namespace Base {
namespace impl {

Map::Map(ObjectComparer *comparer) :
    _comparerRef(0 == comparer ? 0 : comparer->self()),
    _map(Object_hash_t(comparer), Object_equal_t(comparer))
{
}
void Map::autocollectObjects(bool ac)
{
    _nac = !ac;
}
size_t Map::count()
{
    return _map.size();
}
Object *Map::object(Object *key)
{
    map_t::iterator iter = _map.find(Ref<Object>(key));
    if (iter != _map.end())
    {
        Object *obj = iter->second;
        if (0 == obj)
            return 0;
        return _nac ? obj : obj->autocollect();
    }
    else
        return 0;
}
bool Map::getObject(Object *key, Object *&obj)
{
    map_t::iterator iter = _map.find(Ref<Object>(key));
    if (iter != _map.end())
    {
        obj = iter->second;
        if (0 == obj)
            return true;
        _nac ? obj : obj->autocollect();
        return true;
    }
    else
    {
        obj = 0;
        return false;
    }
}
void Map::setObject(Object *key, Object *obj)
{
    _map[Ref<Object>(key)] = Ref<Object>(obj);
}
void Map::removeObject(Object *key)
{
    _map.erase(Ref<Object>(key));
}
void Map::removeAllObjects()
{
    _map.clear();
}
void Map::addObjects(Map *map)
{
    for (map_t::iterator p = map->_map.begin(), q = map->_map.end(); p != q; ++p)
        _map[p->first] = p->second;
}
Object **Map::keys()
{
    Object **keybuf = (Object **)carrayWithCapacity(_map.size(), sizeof(Object *));
    carr_obj(keybuf)->setLength(_map.size());
    Object **keys = keybuf;
    for (map_t::iterator p = _map.begin(), q = _map.end(); p != q; ++p, ++keys)
        *keys = p->first;
    return keybuf;
}
Object **Map::objects()
{
    Object **objbuf = (Object **)carrayWithCapacity(_map.size(), sizeof(Object *));
    carr_obj(objbuf)->setLength(_map.size());
    Object **objs = objbuf;
    for (map_t::iterator p = _map.begin(), q = _map.end(); p != q; ++p, ++objs)
        *objs = p->second;
    return objbuf;
}
void Map::getKeysAndObjects(Object **&keybuf, Object **&objbuf)
{
    keybuf = (Object **)carrayWithCapacity(_map.size(), sizeof(Object *));
    objbuf = (Object **)carrayWithCapacity(_map.size(), sizeof(Object *));
    carr_obj(keybuf)->setLength(_map.size());
    carr_obj(objbuf)->setLength(_map.size());
    Object **keys = keybuf, **objs = objbuf;
    for (map_t::iterator p = _map.begin(), q = _map.end(); p != q; ++p, ++keys, ++objs)
    {
        *keys = p->first;
        *objs = p->second;
    }
}
const char *Map::strrepr()
{
    return cstringf("<%s %p; count=%u>", className(), this, count());
}
/* Iterable<Object *> */
void *Map::iterate(void *state, Object **bufp[2])
{
    if (0 == state)
        state = new (collect) Iterator(_map);
    Iterator *iter = (Iterator *)state;
    Object **curp = bufp[0], **endp = bufp[1];
    while (curp < endp && iter->p != iter->q)
    {
        *curp++ = iter->p->first;
        ++iter->p;
    }
    bufp[1] = curp;
    return state;
}

}
}
