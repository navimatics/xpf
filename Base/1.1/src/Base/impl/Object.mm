/*
 * Base/impl/Object.mm
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#include <Base/impl/Defines.hpp>
#if defined(BASE_CONFIG_DARWIN)
#include <Base/impl/Object.hpp>
#import <Foundation/Foundation.h>
#include <objc/runtime.h>

static BOOL SwizzleInstanceMethod(Class cls, SEL oldsel, SEL newsel)
{
    Method oldmeth = class_getInstanceMethod(cls, oldsel);
    Method newmeth = class_getInstanceMethod(cls, newsel);
    if (NULL == oldmeth || NULL == newmeth)
        return NO;
    class_addMethod(cls, oldsel,
        method_getImplementation(oldmeth), method_getTypeEncoding(oldmeth));
    class_addMethod(cls, newsel,
        method_getImplementation(newmeth), method_getTypeEncoding(newmeth));
    method_exchangeImplementations(
        class_getInstanceMethod(cls, oldsel),
        class_getInstanceMethod(cls, newsel));
    return YES;
}

@interface Base_impl_Object_CollectionPool : NSObject
@end
@implementation Base_impl_Object_CollectionPool
- (id)init
{
    Base::impl::Object::Mark();
    return self;
}
- (void)dealloc
{
    Base::impl::Object::Collect();
    [super dealloc];
}
@end

@interface NSAutoreleasePool (Base_impl_Object_CollectionPool)
@end
@implementation NSAutoreleasePool (Base_impl_Object_CollectionPool)
+ (void)load
{
    SwizzleInstanceMethod(self, @selector(init), @selector(__swizzle__init));
}
- (id)__swizzle__init
{
    self = [self __swizzle__init];
    [[[Base_impl_Object_CollectionPool alloc] init] autorelease];
    return self;
}
@end

namespace Base {
namespace impl {

void Load_Object_mm() {}

}
}

#endif
