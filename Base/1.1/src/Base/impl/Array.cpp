/*
 * Base/impl/Array.cpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#include <Base/impl/Array.hpp>
#include <Base/impl/CArray.hpp>
#include <Base/impl/CString.hpp>

namespace Base {
namespace impl {

static inline ssize_t constrained_index(ssize_t index, ssize_t count)
{
    if (0 > index)
    {
        index += count;
        if (0 > index)
            index = None();
    }
    else if (count <= index)
        index = None();
    return index;
}

Array::Array(size_t capacity)
{
    _objs = (Object **)carr_newWithCapacity(capacity, sizeof(Object *));
}
Array::~Array()
{
    removeAllObjects();
    carr_release(_objs);
}
void Array::autocollectObjects(bool ac)
{
    _nac = !ac;
}
size_t Array::count()
{
    return carr_length(_objs);
}
Object *Array::object(ssize_t index)
{
    ssize_t count = carr_length(_objs);
    index = constrained_index(index, count);
    if (isNone(index))
        return 0;
    Object *obj = _objs[index];
    if (0 == obj)
        return 0;
    return _nac ? obj : obj->autocollect();
}
ssize_t Array::indexOfObject(Object *obj)
{
    ssize_t count = carr_length(_objs);
    if (0 != obj)
        for (Object **p = _objs, **endp = _objs + count; endp > p; p++)
        {
            Object *o = *p;
            if (0 == obj->cmp(o))
                return p - _objs;
        }
    else
        for (Object **p = _objs, **endp = _objs + count; endp > p; p++)
        {
            Object *o = *p;
            if (0 == o)
                return p - _objs;
        }
    return None();
}
ssize_t Array::lastIndexOfObject(Object *obj)
{
    ssize_t count = carr_length(_objs);
    if (0 != obj)
        for (Object **p = _objs + count - 1; _objs <= p; p--)
        {
            Object *o = *p;
            if (0 == obj->cmp(o))
                return p - _objs;
        }
    else
        for (Object **p = _objs + count - 1; _objs <= p; p--)
        {
            Object *o = *p;
            if (0 == o)
                return p - _objs;
        }
    return None();
}
void Array::addObject(Object *obj)
{
    _objs = (Object **)carr_concat(_objs, &obj, 1, sizeof(Object *));
    if (0 != obj)
        obj->retain();
}
void Array::addObjects(Object **objs, size_t n)
{
    if (0 == n)
        return;
    _objs = (Object **)carr_concat(_objs, objs, n, sizeof(Object *));
    if (0 != objs)
        for (Object **endp = objs + n; endp > objs;)
        {
            Object *obj = *objs++;
            if (0 != obj)
                obj->retain();
        }
}
void Array::insertObject(ssize_t index, Object *obj)
{
    ssize_t count = carr_length(_objs);
    if (index != count)
    {
        index = constrained_index(index, count);
        if (isNone(index))
            return;
    }
    _objs = (Object **)carr_concat(_objs, 0, 1, sizeof(Object *));
    Object **p = _objs + index;
    memmove(p + 1, p, (count - index) * sizeof(Object *));
    *p = obj;
    if (0 != obj)
        obj->retain();
}
void Array::insertObjects(ssize_t index, Object **objs, size_t n)
{
    if (0 == n)
        return;
    ssize_t count = carr_length(_objs);
    if (index != count)
    {
        index = constrained_index(index, count);
        if (isNone(index))
            return;
    }
    _objs = (Object **)carr_concat(_objs, 0, n, sizeof(Object *));
    Object **p = _objs + index;
    memmove(p + n, p, (count - index) * sizeof(Object *));
    if (0 != objs)
        for (Object **endp = p + n; endp > p; p++)
        {
            Object *obj = *objs++;
            *p = obj;
            if (0 != obj)
                obj->retain();
        }
    else
        memset(p, 0, n * sizeof(Object *));
}
void Array::replaceObject(ssize_t index, Object *obj)
{
    ssize_t count = carr_length(_objs);
    index = constrained_index(index, count);
    if (isNone(index))
        return;
    Object **p = _objs + index;
    Object *o = *p;
    if (0 != obj)
        obj->retain();
    if (0 != o)
        o->release();
    *p = obj;
}
void Array::replaceObjects(ssize_t index, Object **objs, size_t n)
{
    if (0 == n)
        return;
    ssize_t count = carr_length(_objs);
    index = constrained_index(index, count);
    if (isNone(index))
        return;
    if (count - index < (ssize_t)n)
        n = count - index;
    Object **p = _objs + index;
    if (0 != objs)
        for (Object **endp = p + n; endp > p; p++)
        {
            Object *obj = *objs++;
            Object *o = *p;
            if (0 != obj)
                obj->retain();
            if (0 != o)
                o->release();
            *p = obj;
        }
    else
        for (Object **endp = p + n; endp > p; p++)
        {
            Object *o = *p;
            if (0 != o)
                o->release();
            *p = 0;
        }
}
void Array::moveObject(ssize_t index, ssize_t fromIndex)
{
    ssize_t count = carr_length(_objs);
    index = constrained_index(index, count);
    if (isNone(index))
        return;
    fromIndex = constrained_index(fromIndex, count);
    if (isNone(fromIndex))
        return;
    ssize_t indexDiff = index - fromIndex;
    if (0 == indexDiff)
        return;
    Object *o = _objs[fromIndex];
    if (0 > indexDiff)
    {
        Object **p = _objs + index;
        memmove(p + 1, p, -indexDiff * sizeof(Object *));
    }
    else /* 0 < indexDiff */
    {
        Object **p = _objs + fromIndex;
        memmove(p, p + 1, +indexDiff * sizeof(Object *));
    }
    _objs[index] = o;
}
void Array::moveObjects(ssize_t index, ssize_t fromIndex, size_t n)
{
    if (0 == n)
        return;
    ssize_t count = carr_length(_objs);
    index = constrained_index(index, count);
    if (isNone(index))
        return;
    fromIndex = constrained_index(fromIndex, count);
    if (isNone(fromIndex))
        return;
    ssize_t indexDiff = index - fromIndex;
    if (0 == indexDiff)
        return;
    if (count - index < (ssize_t)n)
        n = count - index;
    if (count - fromIndex < (ssize_t)n)
        n = count - fromIndex;
    Object **objs = n <= 128 ?
        (Object **)alloca(n * sizeof(Object *)) :
        (Object **)Malloc(n * sizeof(Object *));
    memcpy(objs, _objs + fromIndex, n * sizeof(Object *));
    if (0 > indexDiff)
    {
        Object **p = _objs + index;
        memmove(p + n, p, -indexDiff * sizeof(Object *));
    }
    else /* 0 < indexDiff */
    {
        Object **p = _objs + fromIndex;
        memmove(p, p + n, +indexDiff * sizeof(Object *));
    }
    memcpy(_objs + index, objs, n * sizeof(Object *));
    n <= 128 ?
        (void)0 :
        free(objs);
}
void Array::removeObject(ssize_t index)
{
    ssize_t count = carr_length(_objs);
    index = constrained_index(index, count);
    if (isNone(index))
        return;
    Object **p = _objs + index;
    Object *o = *p;
    if (0 != o)
        o->release();
    memmove(p, p + 1, (count - index - 1) * sizeof(Object *));
    carr_obj(_objs)->setLength(count - 1);
}
void Array::removeObjects(ssize_t index, size_t n)
{
    if (0 == n)
        return;
    ssize_t count = carr_length(_objs);
    index = constrained_index(index, count);
    if (isNone(index))
        return;
    if (count - index < (ssize_t)n)
        n = count - index;
    Object **p = _objs + index;
    for (Object **endp = p + n; endp > p; p++)
    {
        Object *o = *p;
        if (0 != o)
            o->release();
    }
    p = _objs + index;
    memmove(p, p + n, (count - index - n) * sizeof(Object *));
    carr_obj(_objs)->setLength(count - n);
}
void Array::removeAllObjects()
{
    ssize_t count = carr_length(_objs);
    for (Object **p = _objs, **endp = _objs + count; endp > p; p++)
    {
        Object *o = *p;
        if (0 != o)
            o->release();
    }
    carr_obj(_objs)->setLength(0);
}
Object **Array::objects()
{
    /* If the user has requested autocollection of objects (the default)
     * we place the CArray result in the autocollection pool.
     * Note however that if "this" gets deleted, an autocollected CArray result
     * will contain garbage Object * pointers, unless care is taken to retain them
     * before releasing "this".
     *
     * This behavior is consistent with other containers (e.g. Map),
     * which do not maintain internal storage as CArray's.
     */
    if (!_nac)
        carr_autocollect(_objs);
    return _objs;
}
const char *Array::strrepr()
{
    return cstringf("<%s %p; count=%u>", className(), this, count());
}
/* Iterable<Object *> */
void *Array::iterate(void *state, Object **bufp[2])
{
    if (0 == state)
    {
        bufp[0] = _objs;
        bufp[1] = _objs + carr_length(_objs);
    }
    else
        bufp[1] = bufp[0];
    return this;
}

}
}
