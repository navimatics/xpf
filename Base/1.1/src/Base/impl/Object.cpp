/*
 * Base/impl/Object.cpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#include <Base/impl/Object.hpp>
#include <Base/impl/Atomic.hpp>
#include <Base/impl/CString.hpp>
#include <Base/impl/Log.hpp>
#include <Base/impl/Tls.hpp>
#include <Base/impl/TypeInfo.hpp>

namespace Base {
namespace impl {

collect_t collect;

struct _CollectionPage
{
    Object *objbuf[1020], **objnextp;
    _CollectionPage *nextpage;
    _CollectionPage *nextpool;
};
struct _CollectionData
{
    _CollectionPage *headpool;
    _CollectionPage *freepage;
};
static void _CollectionDataFree(void *p)
{
    _CollectionData *data = (_CollectionData *)p;
    if (0 != data->freepage)
        free(data->freepage);
    free(data);
}
static tlskey_t _CollectionDataTlsKey() BASE_FUNCCONST;
static tlskey_t _CollectionDataTlsKey()
{
    static tlskey_t tlskey = tlsalloc(_CollectionDataFree);
        /* threadsafe: __forcecall_CollectionDataTlsKey */
    return tlskey;
}
static tlskey_t __forcecall_CollectionDataTlsKey = _CollectionDataTlsKey();
static inline bool _CollectionAddObject(Object *obj)
{
    _CollectionData *data = (_CollectionData *)tlsget(_CollectionDataTlsKey());
    _CollectionPage *page = 0 != data ? data->headpool : 0;
    if (0 == page)
    {
        static unsigned logcnt = 0;
        int lc = atomic_inc(logcnt);
        if (4 >= lc) /* print the first few collection pool leaks only */
            LOG_CALLSTACK("no current collection pool; will leak object %p", obj);
        return true;
    }
    if (page->objbuf + NELEMS(page->objbuf) == page->objnextp)
    {
        _CollectionPage *p = page;
        page = (_CollectionPage *)malloc(sizeof(_CollectionPage));
        if (0 == page)
            return false;
        page->objnextp = page->objbuf;
        page->nextpage = p;
        page->nextpool = p->nextpool;
        data->headpool = page;
    }
    *page->objnextp++ = obj;
    return true;
}

void Object::Mark()
{
    _CollectionData *data = (_CollectionData *)tlsget(_CollectionDataTlsKey());
    if (0 == data)
    {
        data = (_CollectionData *)Malloc(sizeof(_CollectionData));
        data->headpool = 0;
        data->freepage = 0;
        tlsset(_CollectionDataTlsKey(), data);
    }
    _CollectionPage *page = data->freepage;
    if (0 != page)
        data->freepage = 0;
    else
        page = (_CollectionPage *)Malloc(sizeof(_CollectionPage));
    page->objnextp = page->objbuf;
    page->nextpage = 0;
    page->nextpool = data->headpool;
    data->headpool = page;
}
void Object::Collect()
{
    /* BUGFIX:
     * It is possible to have Collect() reentered within the same thread. This can happen
     * if some code does Mark()/Collect() during an object release().
     *
     * To avoid problems in such a scenario, we ensure that the collection pool data structures
     * are in a consistent state while invoking release() in the objects contained in our pool.
     */
    _CollectionData *data = (_CollectionData *)tlsget(_CollectionDataTlsKey());
    _CollectionPage *page = data->headpool;
    data->headpool = page->nextpool;
    do
    {
        for (Object **p = page->objbuf; page->objnextp > p; p++)
            (*p)->release();
        if (0 != data->freepage)
            free(data->freepage);
        data->freepage = page;
        page = page->nextpage;
    } while (0 != page);
}
void *Object::operator new(size_t size, size_t extra)
{
    void *p = Malloc(size + extra);
    memset(p, 0, size);
    return p;
}
void *Object::operator new(size_t size, const collect_t &, size_t extra)
{
    void *p = Malloc(size + extra);
    memset(p, 0, size);
    if (!_CollectionAddObject((Object *)p))
    {
        free(p);
        MemoryAllocationError();
    }
    return p;
}
void *Object::operator new(size_t size, const std::nothrow_t &, size_t extra) throw ()
{
    void *p = malloc(size + extra);
    if (0 == p)
        return 0;
    memset(p, 0, size);
    return p;
}
void *Object::operator new(size_t size, void *p) throw ()
{
    memset(p, 0, size);
    return p;
}
void Object::operator delete(void *p) throw ()
{
    free(p);
}
void Object::operator delete(void *p, size_t) throw ()
{
    free(p);
}
void Object::operator delete(void *p, const collect_t &, size_t) throw ()
{
    free(p);
}
void Object::operator delete(void *p, const std::nothrow_t &, size_t) throw ()
{
    free(p);
}
void Object::operator delete(void *p, void *) throw ()
{
    (void)p;
}
int Object::qsort_cmp(const void *p1, const void *p2)
{
    Object *obj1 = *(Object **)p1, *obj2 = *(Object **)p2;
    return obj1->cmp(obj2);
}
Object *Object::threadsafe()
{
    if (0 < _refcnt)
        _refcnt = -_refcnt;
    return this;
}
Object *Object::linger()
{
    _refcnt = 0;
    return this;
}
Object *Object::retain()
{
    if (0 < _refcnt)
        ++_refcnt;
    else if (0 > _refcnt)
        atomic_dec(_refcnt);
    return this;
}
void Object::release()
{
    if (0 < _refcnt)
    {
        refcnt_t rc = --_refcnt;
        if (0 == rc)
            delete this;
    }
    else if (0 > _refcnt)
    {
        refcnt_t rc = atomic_inc(_refcnt);
        if (0 == rc)
            delete this;
    }
}
Object *Object::autorelease()
{
    if (0 != _refcnt)
        if (!_CollectionAddObject(this))
        {
            release();
            MemoryAllocationError();
        }
    return this;
}
size_t Object::hash()
{
    return (size_t)this;
}
int Object::cmp(Object *obj)
{
    return this - obj;
}
const char *Object::strrepr()
{
    return cstringf("<%s %p>", className(), this);
}
const char *Object::className()
{
    return TypeName(typeid(*this));
}

Object *Interface::self()
{
    return dynamic_cast<Object *>(this);
}

#if defined(BASE_CONFIG_DARWIN)
void Load_Object_mm();
static int __forcecall_Load_Object_mm = (Load_Object_mm(), 1);
#endif

}
}
