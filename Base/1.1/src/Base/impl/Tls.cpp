/*
 * Base/impl/Tls.cpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#include <Base/impl/Tls.hpp>
#include <Base/impl/Atomic.hpp>

namespace Base {
namespace impl {

#if defined(BASE_CONFIG_WINDOWS)
struct _tlskey_info
{
    tlskey_t key;
    void (*fn)(void *);
};
static _tlskey_info _tlskey_infos[8];
static int _tlskey_nexti = 0, _tlskey_count = 0;
    /* !!!:
     * At the present time Base uses 4 tlskey's. If we ever have more than 8 tlskey's requested
     * we need to change this!
     */
static void NTAPI _tlskey_run_destructors(HINSTANCE h, DWORD reason, PVOID reserved)
{
    if (DLL_THREAD_DETACH == reason)
    {
        ssize_t n = _tlskey_count;
        atomic_rmb();
        for (ssize_t i = n - 1; 0 <= i; i--)
        {
            if (0 != _tlskey_infos[i].fn)
                if (void *p = tlsget(_tlskey_infos[i].key))
                    _tlskey_infos[i].fn(p);
        }
    }
}
/* see http://www.codeproject.com/kb/threads/tls.aspx */
#pragma data_seg(push)
#pragma data_seg(".CRT$XLB")
static void (NTAPI *p_tlskey_run_destructors)(HINSTANCE h, DWORD reason, PVOID reserved) =
    _tlskey_run_destructors;
#pragma data_seg(pop)
#pragma comment(linker, "/INCLUDE:__tls_used")
tlskey_t tlsalloc(void (*fn)(void *))
{
    tlskey_t key = TlsAlloc();
    if (TLS_OUT_OF_INDEXES != key)
    {
        int i = atomic_inc(_tlskey_nexti) - 1;
        assert(_tlskey_nexti <= NELEMS(_tlskey_infos));
        _tlskey_infos[i].key = key;
        _tlskey_infos[i].fn = fn;
        atomic_inc(_tlskey_count);
        // atomic_wmb(); // not needed as atomic_inc() above issues a full memory barrier
    }
    return key;
}
#endif

}
}
