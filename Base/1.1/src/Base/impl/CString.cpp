/*
 * Base/impl/CString.cpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#include <Base/impl/CString.hpp>
#include <Base/impl/Snprintf.hpp>
#include <Base/impl/Synchronization.hpp>
#include <Base/impl/hash_map.hpp>

#define SIZE_T_HIGHBIT                  ((size_t)1 << (sizeof(size_t) * CHAR_BIT - 1))

#define METRIC_IS_CONST_STRING(m)       (!(SIZE_T_HIGHBIT & (m)))
#define HASH_TO_METRIC(h)               (~SIZE_T_HIGHBIT & (h))
#define METRIC_TO_HASH(m)               (m)
#define SIZE_TO_METRIC(s)               (SIZE_T_HIGHBIT | (s))
#define METRIC_TO_SIZE(m)               ((SIZE_T_HIGHBIT & (m)) ? (~SIZE_T_HIGHBIT & (m)) : 0)

namespace Base {
namespace impl {

inline size_t hash_chars(const char *s, size_t length)
{
    /* djb2: see http://www.cse.yorku.ca/~oz/hash.html */
    size_t h = 5381;
    for (const char *t = s + length; t > s; ++s)
        h = 33 * h + *s;
    return HASH_TO_METRIC(h);
}
inline size_t hash_ichars(const char *s, size_t length)
{
    /* djb2: see http://www.cse.yorku.ca/~oz/hash.html */
    size_t h = 5381;
    for (const char *t = s + length; t > s; ++s)
        h = 33 * h + tolower(*s);
    return HASH_TO_METRIC(h);
}
inline int cmp_chars(const char *ls, size_t llen, const char *rs, size_t rlen)
{
    ssize_t dlen = llen - rlen;
    int res = memcmp(ls, rs, 0 > dlen ? llen : rlen);
    return 0 != res ? res : dlen;
}
inline int cmp_ichars(const char *ls, size_t llen, const char *rs, size_t rlen)
{
    ssize_t dlen = llen - rlen;
    int res = memicmp(ls, rs, 0 > dlen ? llen : rlen);
    return 0 != res ? res : dlen;
}

typedef hash_map<const char *, CString *> __constant_map_t;

CString *CString::empty()
{
    static CString *str = CString::__constant("", 0);
        /* threadsafe: __forcecall_CString_empty */
    return str;
}
CString *CString::create(const char *s, size_t length)
{
    if ((size_t)-1 == length)
        length = 0 != s ? strlen(s) : 0;
    if (0 == length)
        return empty();
    CString *self = new (length + 1) CString;
    self->_length = length;
    char *chars = (char *)self + sizeof(CString);
    if (0 != s)
    {
        memcpy(chars, s, length);
        chars[length] = '\0';
    }
    else
        memset(chars, 0, length + 1);
    return self;
}
CString *CString::createWithCapacity(size_t capacity)
{
    capacity++;
    CString *self = new (capacity) CString;
    //self->_length = 0;
    self->_metric = SIZE_TO_METRIC(capacity);
    char *chars = (char *)self + sizeof(CString);
    chars[0] = '\0';
    return self;
}
CString *CString::createf(const char *format, ...)
{
    va_list ap;
    va_start(ap, format);
    CString *self = CString::vcreatef(format, ap);
    va_end(ap);
    return self;
}
CString *CString::vcreatef(const char *format, va_list ap)
{
    ssize_t length;
    {
        va_list ap0;
        va_copy(ap0, ap);
        length = portable_vsnprintf(0, 0, format, ap0);
        va_end(ap0);
    }
    if (0 == length)
        return empty();
    CString *self = new (length + 1) CString;
    self->_length = length;
    char *chars = (char *)self + sizeof(CString);
    portable_vsnprintf(chars, length + 1, format, ap);
    return self;
}
CString *CString::__constant(const char *literal, size_t length)
{
    static __constant_map_t *constant_map = new __constant_map_t;
    static Lock *lock = new Lock;
        /* threadsafe: __forcecall_CString_empty */
    CString *self = 0;
    synchronized (lock)
    {
        __constant_map_t::iterator iter = constant_map->find(literal);
        if (iter != constant_map->end())
            self = iter->second;
        else
        {
            self = new (length + 1) CString;
            self->_length = length;
            char *chars = (char *)self + sizeof(CString);
            memcpy(chars, literal, length);
            chars[length] = '\0';
            self->linger();
            constant_map->insert(__constant_map_t::value_type(literal, self));
        }
    }
    return self;
}
size_t CString::hash()
{
    if (!METRIC_IS_CONST_STRING(_metric))
        return hash_chars(chars(), _length);
    if (0 == _metric)
        _metric = hash_chars(chars(), _length);
    return _metric;
}
int CString::cmp(Object *obj0)
{
    if (CString *obj = dynamic_cast<CString *>(obj0))
        return cmp_chars(chars(), length(), obj->chars(), obj->length());
    if (TransientCString *obj = dynamic_cast<TransientCString *>(obj0))
        return cmp_chars(chars(), length(), obj->chars(), obj->length());
    return (Object *)this - obj0;
}
const char *CString::strrepr()
{
    this->autocollect();
    return chars();
}
void CString::setLength(size_t value)
{
    _length = value;
    if (METRIC_IS_CONST_STRING(_metric))
        _metric = 0; /* reset hash value */
}
CString *CString::slice(ssize_t i, ssize_t j)
{
    if (0 > i)
    {
        i = _length + i;
        if (0 > i)
            i = 0;
    }
    else if (_length < (size_t)i)
        i = _length;
    if (0 >= j)
    {
        j = _length + j;
        if (0 > j)
            j = 0;
    }
    else if (_length < (size_t)j)
        j = _length;
    if (i > j)
        j = i;
    CString *str = CString::create(chars() + i, j - i);
    str->autorelease();
    return str;
}
CString *CString::_concat(const char *s, size_t length)
{
    if ((size_t)-1 == length)
        length = 0 != s ? strlen(s) : 0;
    if (0 == length)
        return this;
    CString *self = this;
    char *t = (char *)this + sizeof(CString);
    size_t curlen = _length;
    size_t newlen = curlen + length;
    if (newlen >= METRIC_TO_SIZE(_metric))
    {
        size_t capacity = (newlen + 1) * 3 / 2;
        if (capacity < 16)
            capacity = 16;
        self = new (capacity) CString;
        self->_metric = SIZE_TO_METRIC(capacity);
        char *p = (char *)self + sizeof(CString);
        memcpy(p, t, curlen);
        t = p;
        release();
    }
    if (0 != s)
    {
        memcpy(t + curlen, s, length);
        t[newlen] = '\0';
    }
    else
        memset(t + curlen, 0, length + 1);
    self->_length = newlen;
    return self;
}
void CString::makeImmutable()
{
    if (!METRIC_IS_CONST_STRING(_metric))
        _metric = 0;
}
static CString *__forcecall_CString_empty = CString::empty();

const char *cstring(const char *s, size_t length)
{
    CString *self = CString::create(s, length);
    self->autorelease();
    return (char *)self + sizeof(CString);
}
const char *cstringWithCapacity(size_t capacity)
{
    CString *self = CString::createWithCapacity(capacity);
    self->autorelease();
    return (char *)self + sizeof(CString);
}
const char *cstringf(const char *format, ...)
{
    va_list ap;
    va_start(ap, format);
    CString *self = CString::vcreatef(format, ap);
    va_end(ap);
    self->autorelease();
    return (char *)self + sizeof(CString);
}
const char *cstr_new(const char *s, size_t length)
{
    CString *self = CString::create(s, length);
    return (char *)self + sizeof(CString);
}
const char *cstr_newWithCapacity(size_t capacity)
{
    CString *self = CString::createWithCapacity(capacity);
    return (char *)self + sizeof(CString);
}
const char *cstr_newf(const char *format, ...)
{
    va_list ap;
    va_start(ap, format);
    CString *self = CString::vcreatef(format, ap);
    va_end(ap);
    return (char *)self + sizeof(CString);
}
const char *cstr_concat(const char *s, const char *t, size_t length)
{
    if (0 == s)
        return cstr_new(t, length);
    else
        return ((CString *)(s - sizeof(CString)))->_concat(t, length)->chars();
}
uintptr_t __cstr_inlines_hack()
{
    /* ensure inclusion of the following inline symbols in library */
    return (uintptr_t)&cstr_retain;
}

size_t TransientCString::hash()
{
    if (0 == _metric)
        _metric = hash_chars(chars(), _length);
    return _metric;
}
int TransientCString::cmp(Object *obj0)
{
    if (CString *obj = dynamic_cast<CString *>(obj0))
        return cmp_chars(chars(), length(), obj->chars(), obj->length());
    if (TransientCString *obj = dynamic_cast<TransientCString *>(obj0))
        return cmp_chars(chars(), length(), obj->chars(), obj->length());
    return (Object *)this - obj0;
}
const char *TransientCString::strrepr()
{
    return cstring(chars());
}
void TransientCString::setChars(const char *s)
{
    if (0 != s)
    {
        _length = strlen(s);
        _metric = 0;
        _chars = s;
    }
    else
    {
        _length = 0;
        _metric = 0;
        _chars = "";
    }
}

template <
    size_t HashChars(const char *, size_t),
    int CmpChars(const char *, size_t, const char *, size_t)>
struct CStringComparer : Object, ObjectComparer
{
    size_t object_hash(Object *obj)
    {
        if (CString *str = dynamic_cast<CString *>(obj))
            return HashChars(str->chars(), str->length());
        else
        if (TransientCString *str = dynamic_cast<TransientCString *>(obj))
            return HashChars(str->chars(), str->length());
        return obj->hash();
    }
    int object_cmp(Object *lobj, Object *robj)
    {
        if (CString *lstr = dynamic_cast<CString *>(lobj))
        {
            if (CString *rstr = dynamic_cast<CString *>(robj))
                return CmpChars(lstr->chars(), lstr->length(), rstr->chars(), rstr->length());
            else
            if (TransientCString *rstr = dynamic_cast<TransientCString *>(robj))
                return CmpChars(lstr->chars(), lstr->length(), rstr->chars(), rstr->length());
        }
        else
        if (TransientCString *lstr = dynamic_cast<TransientCString *>(lobj))
        {
            if (CString *rstr = dynamic_cast<CString *>(robj))
                return CmpChars(lstr->chars(), lstr->length(), rstr->chars(), rstr->length());
            else
            if (TransientCString *rstr = dynamic_cast<TransientCString *>(robj))
                return CmpChars(lstr->chars(), lstr->length(), rstr->chars(), rstr->length());
        }
        return lobj->cmp(robj);
    }
};

ObjectComparer *CString::comparer()
{
    static ObjectComparer *cmp = new CStringComparer<hash_chars, cmp_chars>;
        /* threadsafe: __forcecall_CString_comparer */
    return cmp;
}
ObjectComparer *CString::caseInsensitiveComparer()
{
    static ObjectComparer *cmp = new CStringComparer<hash_ichars, cmp_ichars>;
        /* threadsafe: __forcecall_CString_comparer */
    return cmp;
}
static int __forcecall_CString_comparer = (
    CString::comparer(),
    CString::caseInsensitiveComparer(),
    1);

}
}
