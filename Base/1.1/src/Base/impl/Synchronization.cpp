/*
 * Base/impl/Synchronization.cpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#include <Base/impl/Synchronization.hpp>

namespace Base {
namespace impl {

Lock::Lock()
{
    threadsafe();
    mutex_init(&_mutex);
}
Lock::~Lock()
{
    mutex_fini(&_mutex);
}
void Lock::lock()
{
    mutex_lock(&_mutex);
}
void Lock::unlock()
{
    mutex_unlock(&_mutex);
}

ReadWriteLock::ReadWriteLock(bool fair)
{
    threadsafe();
    rwlock_init(&_rwlock, fair);
}
ReadWriteLock::~ReadWriteLock()
{
    rwlock_fini(&_rwlock);
}
void ReadWriteLock::lock(bool read)
{
    if (read)
        rwlock_rdlock(&_rwlock);
    else
        rwlock_wrlock(&_rwlock);
}
void ReadWriteLock::unlock(bool read)
{
    if (read)
        rwlock_rdunlock(&_rwlock);
    else
        rwlock_wrunlock(&_rwlock);
}

Semaphore::Semaphore(int count)
{
    threadsafe();
    semaphore_init(&_semaphore, count);
}
Semaphore::~Semaphore()
{
    semaphore_fini(&_semaphore);
}
void Semaphore::wait()
{
    semaphore_wait(&_semaphore);
}
void Semaphore::notify()
{
    semaphore_notify(&_semaphore);
}

ConditionVariable::ConditionVariable()
{
    threadsafe();
    condvar_init(&_condvar);
}
ConditionVariable::~ConditionVariable()
{
    condvar_fini(&_condvar);
}
bool ConditionVariable::wait(Lock *lock, wait_timeout_t timeout)
{
    return condvar_wait(&_condvar, &lock->_mutex, timeout);
}
void ConditionVariable::notify()
{
    condvar_notify(&_condvar);
}
void ConditionVariable::notifyAll()
{
    condvar_notify_all(&_condvar);
}

}
}
