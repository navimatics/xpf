/*
 * Base/impl/MemoryStream.cpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#include <Base/impl/MemoryStream.hpp>
#include <Base/impl/CString.hpp>

namespace Base {
namespace impl {

MemoryStream::MemoryStream(const void *buf, size_t size)
{
    _buf = cstr_new((const char *)buf, size);
}
MemoryStream::~MemoryStream()
{
    close();
}
ssize_t MemoryStream::read(void *buf, size_t size)
{
    if (0 == _buf)
        return -1;
    ssize_t remain = cstr_length(_buf) - _offset;
    if (size > remain)
        size = remain;
    memcpy(buf, _buf + _offset, size);
    _offset += size;
    return size;
}
ssize_t MemoryStream::write(const void *buf, size_t size)
{
    if (0 == _buf)
        return -1;
    _buf = cstr_concat(_buf, (const char *)buf, size);
    _offset = cstr_length(_buf);
    return size;
}
ssize_t MemoryStream::seek(ssize_t offset, int whence)
{
    if (0 == _buf)
        return -1;
    ssize_t newofst = _offset;
    switch (whence)
    {
    default:
    case SEEK_SET:
        newofst = offset;
        break;
    case SEEK_CUR:
        newofst += offset;
        break;
    case SEEK_END:
        newofst = cstr_length(_buf) + offset;
        break;
    }
    if (newofst < 0 || cstr_length(_buf) < newofst)
        return -1;
    return _offset = newofst;
}
ssize_t MemoryStream::close()
{
    if (0 == _buf)
        return -1;
    cstr_release(_buf);
    _buf = 0;
    return 0;
}
const char *MemoryStream::buffer()
{
    return _buf;
}
size_t MemoryStream::size()
{
    return cstr_length(_buf);
}

}
}
