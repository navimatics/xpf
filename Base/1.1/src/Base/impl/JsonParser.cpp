/*
 * Base/impl/JsonParser.cpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#include <Base/impl/JsonParser.hpp>
#include <Base/impl/CString.hpp>
#include <Base/impl/Error.hpp>

namespace Base {
namespace impl {

enum
{
    StateValue = 1000,
    StateObjectKey,
    StateObjectColon,
    StateObjectValue,
    StateObjectComma,
    StateArrayValue,
    StateArrayComma,
    StateRestOfString,
};
inline void JsonParser::_pushstate(int state)
{
    if (_stackendp == _stackp)
    {
        size_t count = _stackp - _stack;
        size_t capacity = 0 == count ? 16 : count * 2; /* initial=16; grow-factor=2.0 */
        void *p = Realloc(_stack, capacity * sizeof(int));
        _stack = (int *)p;
        _stackp = _stack + count;
        _stackendp = _stack + capacity;
    }
    *_stackp++ = state;
}
inline void JsonParser::_popstate()
{
    --_stackp;
}
inline int JsonParser::_topstate()
{
    assert(_stackp > _stack);
    return _stackp[-1];
}
inline void JsonParser::_topstate(int state)
{
    assert(_stackp > _stack);
    _stackp[-1] = state;
}
inline bool JsonParser::_endstate()
{
    return _stackp == _stack;
}

const char *JsonParser::errorDomain()
{
    static const char *s = CStringConstant("Base::impl::JsonParser")->chars();
    return s;
}
JsonParser::JsonParser(Stream *stream) : TextParser(stream)
{
    _pushstate(StateValue);
    _flags = InfinityExtension;
}
JsonParser::~JsonParser()
{
    if (0 != _stack)
        free(_stack);
}
void JsonParser::reset()
{
    TextParser::reset();
    _stackp = _stack;
    _pushstate(StateValue);
    _flags = InfinityExtension;
}
unsigned JsonParser::flags(unsigned mask)
{
    return _flags & mask;
}
void JsonParser::setFlags(unsigned mask, unsigned value)
{
    if (value)
        _flags |= mask;
    else
        _flags &= ~mask;
}
void JsonParser::startArray()
{
}
void JsonParser::endArray()
{
}
void JsonParser::startObject()
{
}
void JsonParser::endObject()
{
}
void JsonParser::nullValue()
{
}
void JsonParser::booleanValue(bool B)
{
}
void JsonParser::integerValue(long l)
{
}
void JsonParser::realValue(double d)
{
}
void JsonParser::characterData(const char *s, size_t len, bool isFinal, bool isKey)
{
}
const char *JsonParser::_errorDomain()
{
    return JsonParser::errorDomain();
}
#define EXPECT(literal)                 expect(literal "", sizeof literal - 1)
void JsonParser::parseBuffer(bool eof)
{
    if (_endstate())
    {
        while (_nextp < _freep)
            if (32 < *_nextp++)
                raise("syntax error: extra data at end of file");
        return;
    }
    for (;;)
    {
        int c = _topstate();
        if (StateValue == c)
        {
            _skipws();
            c = peekchar();
        }
        switch (c)
        {
        default:
            raise("syntax error");
        case 'f':
            EXPECT("false");
            booleanValue(false);
            break;
        case 'n':
            EXPECT("null");
            nullValue();
            break;
        case 't':
            EXPECT("true");
            booleanValue(true);
            break;
        case 'N':
            if (!flags(InfinityExtension))
                raise("syntax error");
            EXPECT("NaN");
            realValue(NAN);
            break;
        case 'I':
            if (!flags(InfinityExtension))
                raise("syntax error");
            EXPECT("Infinity");
            realValue(+INFINITY);
            break;
        case '+': case '-':
            if ('I' == peeknext(1))
            {
                if (!flags(InfinityExtension))
                    raise("syntax error");
                if ('+' == c)
                {
                    EXPECT("+Infinity");
                    realValue(+INFINITY);
                    break;
                }
                else
                {
                    EXPECT("-Infinity");
                    if (!flags(FloatNoneCompatibility))
                        realValue(-INFINITY);
                    else /* if FloatNoneCompatibility convert -INFINITY to None */
                        realValue(None());
                    break;
                }
            }
            /* fall through */
        case '0': case '1': case '2': case '3': case '4':
        case '5': case '6': case '7': case '8': case '9':
            _parseNumber();
            break;
        case '\"':
            skipchar();
            _topstate(StateRestOfString);
            /* fall through */
        case StateRestOfString:
            _parseRestOfString();
            break;
        /* '{': object */
        case '{':
            skipchar();
            _topstate(StateObjectKey);
            startObject();
            /* fall through */
        case StateObjectKey:
            _skipws();
            c = peekchar();
            if ('}' == c)
            {
                skipchar();
                endObject();
                break;
            }
            EXPECT("\"");
            _topstate(StateObjectColon);
            _pushstate(StateRestOfString);
            continue;
        case StateObjectColon:
            _skipws();
            EXPECT(":");
            _topstate(StateObjectValue);
            /* fall through */
        case StateObjectValue:
            _skipws();
            _topstate(StateObjectComma);
            _pushstate(StateValue);
            continue;
        case StateObjectComma:
            _skipws();
            c = peekchar();
            if (',' == c)
                skipchar();
            else if ('}' != c)
                raise("syntax error: expected ',' or '}'");
            _topstate(StateObjectKey);
            continue;
        /* '[': array */
        case '[':
            skipchar();
            _topstate(StateArrayValue);
            startArray();
            /* fall through */
        case StateArrayValue:
            _skipws();
            c = peekchar();
            if (']' == c)
            {
                skipchar();
                endArray();
                break;
            }
            _topstate(StateArrayComma);
            _pushstate(StateValue);
            continue;
        case StateArrayComma:
            _skipws();
            c = peekchar();
            if (',' == c)
                skipchar();
            else if (']' != c)
                raise("syntax error: expected ',' or ']'");
            _topstate(StateArrayValue);
            continue;
        }
        _popstate();
        if (_endstate())
            break;
    }
}
void JsonParser::_skipws()
{
    for (;;)
    {
        char c = peekchar();
        if (32 < c)
            break;
        skipchar();
    }
}
void JsonParser::_parseNumber()
{
    char numbuf[64], *nump = numbuf, *endp = numbuf + sizeof numbuf - 1;
    char *nextp = _nextp;
    bool dot = false;
    while (endp > nump)
    {
        if (nextp >= _freep)
            raise();
        char c = *nextp;
        switch (c)
        {
        case '.': case 'e': case 'E':
            dot = true;
            /* fall through */
        case '+': case '-':
        case '0': case '1': case '2': case '3': case '4':
        case '5': case '6': case '7': case '8': case '9':
            *nump++ = c;
            nextp++;
            break;
        default:
            goto numloopend;
        }
    }
numloopend:
    *nump = '\0';
    _nextp = nextp;
    if (!dot)
    {
        long l = strtol(numbuf, &endp, 10);
        if ('\0' != *endp)
            raise("syntax error: invalid number \'%s\'", numbuf);
        integerValue(l);
    }
    else
    {
        double d = strtod(numbuf, &endp);
        if ('\0' != *endp)
            raise("syntax error: invalid number \'%s\'", numbuf);
        realValue(d);
    }
}
void JsonParser::_parseRestOfString()
{
    bool isKey = _stackp - 2 >= _stack && StateObjectColon == _stackp[-2];
    char *startp = _nextp;
    for (;;)
    {
        if (_nextp >= _freep)
        {
            characterData(startp, _nextp - startp, false, isKey);
            raise();
        }
        char c = *_nextp;
        switch (c)
        {
        case '\"':
            characterData(startp, _nextp - startp, true, isKey);
            _nextp++;
            return;
        case '\\':
            characterData(startp, _nextp - startp, false, isKey);
            if (_nextp + 2/* \X */ > _freep)
                raise();
            c = *++_nextp;
            switch (c)
            {
            case '/':
                if (flags(StringEscapeExtension))
                {
                    if (_nextp + 3/* \// */ > _freep)
                    {
                        _nextp--;
                        raise();
                    }
                    if ('/' == _nextp[1])
                    {
                        _nextp++;
                        characterData("\\//", 3, false, isKey);
                        goto skipCharacterData;
                    }
                }
                break;
            case '\"':
            case '\\':
                break;
            case 'b':
                c = '\b';
                break;
            case 'f':
                c = '\f';
                break;
            case 'n':
                c = '\n';
                break;
            case 'r':
                c = '\r';
                break;
            case 't':
                c = '\t';
                break;
            case 'u':
                {
                    if (_nextp + 5/* \uXXXX */ > _freep)
                    {
                        _nextp--;
                        raise();
                    }
                    char numbuf[5], *endp;
                    numbuf[0] = *++_nextp;
                    numbuf[1] = *++_nextp;
                    numbuf[2] = *++_nextp;
                    numbuf[3] = *++_nextp;
                    numbuf[4] = '\0';
                    unsigned w = strtoul(numbuf, &endp, 16);
                    if ('\0' != *endp)
                        raise("syntax error: invalid unicode escape \'%s\'", numbuf);
                    char utf8[3];
                    if (w < 0x0080)
                    {
                        utf8[0] = w;
                        characterData(utf8, 1, false, isKey);
                    }
                    else if (w < 0x0800)
                    {
                        utf8[0] = 0xc0 | (w >> 6);
                        utf8[1] = 0x80 | (w & 0x3f);
                        characterData(utf8, 2, false, isKey);
                    }
                    else
                    {
                        utf8[0] = 0xe0 | (w >> 12);
                        utf8[1] = 0x80 | ((w >> 6) & 0x3f);
                        utf8[2] = 0x80 | (w & 0x3f);
                        characterData(utf8, 3, false, isKey);
                    }
                    goto skipCharacterData;
                }
                break;
            default:
                break;
            }
            characterData(&c, 1, false, isKey);
        skipCharacterData:
            _nextp++;
            startp = _nextp;
            break;
        default:
            _nextp++;
            break;
        }
    }
}
#undef EXPECT

}
}
