/*
 * Base/impl/IndexSet.cpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#include <Base/impl/IndexSet.hpp>
#include <Base/impl/BitHacks.hpp>
#include <Base/impl/CArray.hpp>
#include <Base/impl/CString.hpp>
#include <Base/impl/Coroutine.hpp>

namespace Base {
namespace impl {

#define INDEX_BITS                      IMAX_BITS(UINTPTR_MAX)

static inline int lsb(index_t v)
{
    assert(0 != v);
    return (INDEX_BITS - 1) - bit_clz(v);
}

IndexSet::IndexSet(size_t capacity) : RadixTree(capacity)
{
}
size_t IndexSet::count()
{
    return _count;
}
bool IndexSet::containsIndex(index_t index)
{
    index_t mask = (index_t)1 << (index % INDEX_BITS);
    index /= INDEX_BITS;
    Node *node = lookup(index);
    if (node != nullnode() && node->index == index)
    {
        index_t value = node->value;
        return 0 != (value & mask);
    }
    else
        return false;
}
index_t IndexSet::anyIndex()
{
    if (0 == _count)
        return None();
    Node *node = nullnode()->child[0];
    index_t index = node->index * INDEX_BITS;
    index_t value = node->value;
    return index + lsb(value);
}
void IndexSet::addIndex(index_t index)
{
    index_t mask = (index_t)1 << (index % INDEX_BITS);
    index /= INDEX_BITS;
    Node *node = lookup(index);
    if (node != nullnode() && node->index == index)
    {
        index_t value = node->value;
        if (0 == (value & mask))
        {
            node->value = value | mask;
            _count++;
        }
    }
    else
    {
        insert(node, index, mask);
        _count++;
    }
}
void IndexSet::removeIndex(index_t index)
{
    index_t mask = (index_t)1 << (index % INDEX_BITS);
    index /= INDEX_BITS;
    Node *node = lookup(index);
    if (node != nullnode() && node->index == index)
    {
        index_t value = node->value;
        if (0 != (value & mask))
        {
            value &= ~mask;
            if (0 != value)
                node->value = value;
            else
                remove(node);
            _count--;
        }
    }
}
void IndexSet::removeAllIndexes()
{
    _count = 0;
    reset();
}
index_t *IndexSet::indexes()
{
    index_t *result = (index_t *)carrayWithCapacity(_count, sizeof(index_t));
    carr_obj(result)->setLength(_count);
    size_t i = 0;
    Iterable<index_t> *iter = this;
    foreach (index_t index, iter)
        result[i++] = index;
    return result;
}
const char *IndexSet::strrepr()
{
    return cstringf("<%s %p; count=%u>", className(), this, count());
}
/* Iterable<index_t> */
void *IndexSet::iterate(void *state, index_t *bufp[2])
{
    if (0 == state)
        state = new (collect) Iterator(this);
    Iterator *iter = (Iterator *)state;
    index_t *curp = bufp[0], *endp = bufp[1];
    coroblock (iter)
    {
        iter->bufp[0] = iter->bufp[1] = iter->buf;
        for (;;)
        {
            if (iter->bufp[0] >= iter->bufp[1])
            {
                iter->bufp[0] = iter->buf;
                iter->bufp[1] = iter->buf + NELEMS(iter->buf);
                state = iter->iterate(state, iter->bufp);
                if (iter->bufp[0] >= iter->bufp[1])
                    corofini (iter);
            }
            iter->index = ((Node *)(*iter->bufp[0]))->index * INDEX_BITS;
            iter->value = ((Node *)(*iter->bufp[0]))->value;
            iter->bufp[0]++;
            while (0 != iter->value)
            {
                if (curp >= endp)
                    coroyield (iter);
                index_t diff = iter->value & ((index_t)-(sindex_t)iter->value); /* find lsb */
                *curp++ = iter->index + lsb(diff);
                iter->value ^= diff;
            }
        }
    }
    bufp[1] = curp;
    return state;
}
/* ObjectCodec use */
Array *IndexSet::__array()
{
    Array *array = new (collect) Array(_count);
    Iterable<index_t> *iter = this;
    foreach (index_t index, iter)
    {
        CString *value = CString::createf("%llx", (unsigned long long)index);
        array->addObject(value);
        value->release();
    }
    return array;
}
void IndexSet::__setArray(Array *array)
{
    foreach (CString *value, array)
    {
        index_t index = strtoull(value->chars(), 0, 16);
        addIndex(index);
    }
}
ObjectCodecBegin(IndexSet)
    ObjectCodecProperty("_", &IndexSet::__array, &IndexSet::__setArray)
ObjectCodecEnd(IndexSet)

}
}
