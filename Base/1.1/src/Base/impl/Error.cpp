/*
 * Base/impl/Error.cpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#include <Base/impl/Error.hpp>
#include <Base/impl/CString.hpp>
#include <Base/impl/Log.hpp>
#include <Base/impl/Thread.hpp>

namespace Base {
namespace impl {

static Error::logger_t _logger;
Error::logger_t Error::logger()
{
    return _logger;
}
void Error::setLogger(Error::logger_t logger)
{
    _logger = logger;
}
void Error::standardLogger(Error *error)
{
    Log("Error: %s(%i): %s", error->_domain, error->_code, error->message());
}
void Error::enableLogging(bool flag)
{
    Thread *thread = Thread::currentThread();
    intptr_t enabled = (intptr_t)thread->_slot(Thread::_SlotErrorLogging);
    if (flag)
        enabled++;
    else
        enabled--;
    thread->_setSlot(Thread::_SlotErrorLogging, (void *)enabled);
}
Error *Error::lastError()
{
    Thread *thread = Thread::currentThread();
    Error *error = (Error *)thread->_slotObject(Thread::_SlotObjectError);
    if (0 != error)
        error->autocollect();
    return error;
}
void Error::setLastError(Error *error, bool restore)
{
    Thread *thread = Thread::currentThread();
    thread->_setSlotObject(Thread::_SlotObjectError, error);
    if (!restore && 0 != error && 0 != _logger && 0 <= (intptr_t)thread->_slot(Thread::_SlotErrorLogging))
        mark_and_collect
            _logger(error);
}
void Error::setLastError(const char *domain, int code, const char *message)
{
    Error *error = new Error(domain, code, message);
    setLastError(error);
    error->release();
}
void Error::setLastErrorFromCErrno()
{
    Error *error = createFromCErrno();
    setLastError(error);
    error->release();
}
void Error::setLastErrorFromCErrno(int code)
{
    Error *error = createFromCErrno(code);
    setLastError(error);
    error->release();
}
void Error::setLastErrorFromSystem()
{
    Error *error = createFromSystem();
    setLastError(error);
    error->release();
}
void Error::setLastErrorFromSystem(int code)
{
    Error *error = createFromSystem(code);
    setLastError(error);
    error->release();
}
Error *Error::createFromCErrno()
{
    return createFromCErrno(errno);
}
Error *Error::createFromCErrno(int code)
{
    static CString *domain = CStringConstant("errno");
    return new Error(domain, code);
}
Error *Error::createFromSystem()
{
#if defined(BASE_CONFIG_WINDOWS)
    return createFromSystem(GetLastError());
#else
    return createFromSystem(errno);
#endif
}
Error *Error::createFromSystem(int code)
{
#if defined(BASE_CONFIG_WINDOWS)
    static CString *domain = CStringConstant("windows");
#else
    static CString *domain = CStringConstant("errno");
#endif
    return new Error(domain, code);
}
Error::Error(const char *domain, int code, const char *message)
{
    if (0 != domain)
        _domain = cstr_new(domain);
    _code = code;
    if (0 != message)
        _message = cstr_new(message);
}
Error::Error(CString *domain, int code, CString *message)
{
    if (0 != domain)
    {
        domain->retain();
        _domain = domain->chars();
    }
    _code = code;
    if (0 != message)
    {
        message->retain();
        _message = message->chars();
    }
}
Error::~Error()
{
    cstr_release(_message);
    cstr_release(_domain);
}
Object *Error::threadsafe()
{
    mark_and_collect
        /* force _message creation; this makes this object's fields constant and therefore thread-safe */
        message();
    return Object::threadsafe();
}
const char *Error::domain()
{
    cstr_autocollect(_domain);
    return _domain;
}
int Error::code()
{
    return _code;
}
const char *Error::message()
{
    if (0 == _message)
    {
        static const char *errnoDomain = CStringConstant("errno")->chars();
#if defined(BASE_CONFIG_WINDOWS)
        static const char *win32Domain = CStringConstant("windows")->chars();
#endif
        if (_domain == errnoDomain)
        {
            char *message = (char *)cstr_new(0, 128);
#if defined(BASE_CONFIG_WINDOWS)
            strerror_s(message, 128, _code);
#elif defined(BASE_CONFIG_POSIX)
            strerror_r(_code, message, 128);
#else
            strncpy(message, strerror(_code), 128);
            message[127] = '\0';
#endif
            size_t len = strlen(message);
            if ('\n' == message[len - 1])
            {
                message[--len] = '\0';
                if ('\r' == message[len - 1])
                    message[--len] = '\0';
            }
            cstr_obj(message)->setLength(len);
            _message = message;
        }
#if defined(BASE_CONFIG_WINDOWS)
        else if (_domain == win32Domain)
        {
            char *message = (char *)cstr_new(0, 128);
            if (0 == FormatMessage(FORMAT_MESSAGE_FROM_SYSTEM, 0, _code, 0, message, 128, 0))
                message[0] = '\0';
            size_t len = strlen(message);
            if ('\n' == message[len - 1])
            {
                message[--len] = '\0';
                if ('\r' == message[len - 1])
                    message[--len] = '\0';
            }
            cstr_obj(message)->setLength(len);
            _message = message;
        }
#endif
    }
    cstr_autocollect(_message);
    return _message;
}
const char *Error::strrepr()
{
    return cstringf("<%s %p; %s(%i): %s>", className(), this, _domain, _code, message());
}

}
}
