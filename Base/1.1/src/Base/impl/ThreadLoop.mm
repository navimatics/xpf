/*
 * Base/impl/ThreadLoop.mm
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#include <Base/impl/Defines.hpp>
#if defined(BASE_CONFIG_DARWIN)
#import <Foundation/Foundation.h>

namespace Base { namespace impl {
void _ThreadLoopInvokeOnCurrentThread();
}}

static Class SignalerClass;
@interface Base_impl_ThreadLoop_Signaler : NSObject
@end
@implementation Base_impl_ThreadLoop_Signaler
+ (void)load
{
    SignalerClass = self;
}
+ (void)signal0
{
}
+ (void)signal1
{
    Base::impl::_ThreadLoopInvokeOnCurrentThread();
}
@end

namespace Base {
namespace impl {

bool _ThreadLoopSignal0(void *nsthread)
{
    [SignalerClass
        performSelector:@selector(signal0)
        onThread:(NSThread *)nsthread
        withObject:nil
        waitUntilDone:NO];
    return true;
}
bool _ThreadLoopSignal1(void *nsthread)
{
    [SignalerClass
        performSelector:@selector(signal1)
        onThread:(NSThread *)nsthread
        withObject:nil
        waitUntilDone:NO];
    return true;
}
void *_ThreadLoopSignalTarget()
{
    return [NSThread currentThread];
}

}
}
#endif
