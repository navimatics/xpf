/*
 * Base/impl/JvmPeerMapping.cpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#include <Base/impl/Jvm.hpp>
#if defined(BASE_CONFIG_JVM)
#include <Base/impl/CArray.hpp>
#include <Base/impl/Ref.hpp>
#include <Base/impl/Synchronization.hpp>
#include <Base/impl/hash_map.hpp>
#include <map>
#endif

namespace Base {
namespace impl {

/*
 * peer mapping
 */
#if defined(BASE_CONFIG_JVM)
#define fatal                           JvmFatal
class _JvmTypeInfo : public Object
{
public:
    static void registerPeerTypes(const char *clsname, const std::type_info &tinfo);
    static jobject newJavaObject(JNIEnv *env, const std::type_info &tinfo);
private:
    _JvmTypeInfo(const char *clsname);
    ~_JvmTypeInfo();
private:
    const char *_clsname;
    jclass _jcls;
    jmethodID _jcid;
};
struct _stdtypeinfo_less_t
{
    bool operator()(const std::type_info *a, const std::type_info *b) const
    {
        return a->before(*b);
    }
};
typedef std::map<const std::type_info *, Ref<_JvmTypeInfo>, _stdtypeinfo_less_t> typemap_t;
typedef hash_map<Object *, jweak> peermap_t;

static Lock *typelock;
static typemap_t *typemap;
static Lock *peerlock;
static peermap_t *peermap;
static jclass JavaObject_Class;
static jfieldID JavaObject_ObjectFieldID;
static jclass FinalReference_Class;
static jmethodID FinalReference_CtorMethodID;
static jmethodID FinalReference_GetMethodID;
static jfieldID FinalReference_ObjectFieldID;

void _JvmTypeInfo::registerPeerTypes(const char *clsname, const std::type_info &tinfo)
{
    _JvmTypeInfo *jinfo = new _JvmTypeInfo(clsname);
    synchronized (typelock)
        typemap->insert(typemap_t::value_type(&tinfo, Ref<_JvmTypeInfo>(jinfo)));
    jinfo->release();
}
jobject _JvmTypeInfo::newJavaObject(JNIEnv *env, const std::type_info &tinfo)
{
    _JvmTypeInfo *jinfo = 0;
    jclass jcls = 0;
    synchronized (typelock)
    {
        typemap_t::iterator iter = typemap->find(&tinfo);
        if (iter != typemap->end())
            jinfo = iter->second;
        else
        {
            typemap_t::iterator iter = typemap->find(&typeid(Object));
            if (iter != typemap->end())
                jinfo = iter->second;
        }
        assert(0 != jinfo);
        jcls = (jclass)env->NewLocalRef(jinfo->_jcls);
    }
    if (0 == jcls)
    {
        jcls = env->FindClass(jinfo->_clsname);
        if (0 == jcls)
            fatal(env, "JNIEnv.FindClass: %s", jinfo->_clsname);
        synchronized (typelock)
        {
            if (0 != jinfo->_jcls)
            {
                env->DeleteWeakGlobalRef(jinfo->_jcls);
                jinfo->_jcls = 0;
            }
            jinfo->_jcls = (jclass)env->NewWeakGlobalRef(jcls);
        }
    }
    if (0 == jinfo->_jcid)
    {
        jinfo->_jcid =
            env->GetMethodID(jcls, "<init>", "(L" BASE_JVM_JAVAOBJECTBASECLASS "$skip_ctor_t;)V");
            /* thread-safe to call method from multiple threads simultaneously */
        if (0 == jinfo->_jcid)
            fatal(env, "JNIEnv.GetMethodID: %s,%s", jinfo->_clsname, "<init>");
    }
    jobject jobj = env->NewObject(jcls, jinfo->_jcid, /*skip_ctor_t*/0);
    if (0 == jobj)
        fatal(env, "JNIEnv.NewObject: ", jinfo->_clsname);
    env->DeleteLocalRef(jcls);
    return jobj;
}
_JvmTypeInfo::_JvmTypeInfo(const char *clsname)
{
    threadsafe();
    _clsname = cstr_new(clsname);
}
_JvmTypeInfo::~_JvmTypeInfo()
{
    fatal(0, "%s", BASE_FUNCSIG);
}

#if defined(BASE_CONFIG_ARCH32)
static inline jfieldID getObjectFieldId(JNIEnv *env, jclass jcls)
{
    return env->GetFieldID(jcls, "_object", "I");
}
static inline Object *getObjectField(JNIEnv *env, jobject jobj, jfieldID fid)
{
    return (Object *)env->GetIntField(jobj, fid);
}
static inline void setObjectField(JNIEnv *env, jobject jobj, jfieldID fid, Object *obj)
{
    env->SetIntField(jobj, fid, (jint)obj);
}
#else
static inline jfieldID getObjectFieldId(JNIEnv *env, jclass jcls)
{
    return env->GetFieldID(jcls, "_object", "J");
}
static inline Object *getObjectField(JNIEnv *env, jobject jobj, jfieldID fid)
{
    return (Object *)env->GetLongField(jobj, fid);
}
static inline void setObjectField(JNIEnv *env, jobject jobj, jfieldID fid, Object *obj)
{
    env->SetLongField(jobj, fid, (jlong)obj);
}
#endif
void _JvmInitializePeerMapping()
{
    execute_once
    {
        typelock = new Lock;
        typemap = new typemap_t;
        peerlock = new Lock;
        peermap = new peermap_t;
        JNIEnv *env = JvmEnvironment();
        JavaObject_Class = JvmClassGlobalRef(env, BASE_JVM_JAVAOBJECTBASECLASS);
        if (0 == JavaObject_Class)
            fatal(env, "JvmClassGlobalRef: %s", BASE_JVM_JAVAOBJECTBASECLASS);
        JavaObject_ObjectFieldID = getObjectFieldId(env, JavaObject_Class);
        if (0 == JavaObject_ObjectFieldID)
            fatal(env, "JNIEnv.GetFieldID: %s,%s",
                BASE_JVM_JAVAOBJECTBASECLASS, "_object");
        FinalReference_Class = JvmClassGlobalRef(env, BASE_JVM_JAVAOBJECTBASECLASS "$FinalReference");
        if (0 == FinalReference_Class)
            fatal(env, "JvmClassGlobalRef: %s", BASE_JVM_JAVAOBJECTBASECLASS "$FinalReference");
        FinalReference_CtorMethodID = env->GetMethodID(FinalReference_Class,
            "<init>", "(L" BASE_JVM_JAVAOBJECTBASECLASS ";)V");
        if (0 == FinalReference_CtorMethodID)
            fatal(env, "JNIEnv.GetMethodID: %s,%s",
                BASE_JVM_JAVAOBJECTBASECLASS "$FinalReference", "<init>");
        FinalReference_GetMethodID = env->GetMethodID(FinalReference_Class,
            "get", "()Ljava/lang/Object;");
        if (0 == FinalReference_GetMethodID)
            fatal(env, "JNIEnv.GetMethodID: %s,%s",
                BASE_JVM_JAVAOBJECTBASECLASS "$FinalReference", "get");
        FinalReference_ObjectFieldID = getObjectFieldId(env, FinalReference_Class);
        if (0 == FinalReference_ObjectFieldID)
            fatal(env, "JNIEnv.GetFieldID: %s,%s",
                BASE_JVM_JAVAOBJECTBASECLASS "$FinalReference", "_object");
    }
}
void JvmRegisterPeerTypes(const char *clsname, const std::type_info &tinfo)
{
    _JvmTypeInfo::registerPeerTypes(clsname, tinfo);
}
jobject JvmJavaObject(JNIEnv *env, Object *obj)
{
    if (0 == obj)
        return 0;
    jobject jobj = 0;
    jobject jref = 0;
    /*
     * Attempt to get the peer Java Object for this Base::Object:
     *     - Find the peer FinalReference in the peer map.
     *         - If there is one then attempt to get a strong reference to the underlying
     *         Java Object by invoking FinalReference.get().
     *             - If this succeeds we are done.
     *             - If it fails it means that the Java Object was garbage collected.
     *             We must remove the FinalReference from the peer map and also delete it.
     *             [It is safe to delete the FinalReference outside the lock, because only
     *             the thread that is responsible for deleting it from the peer map can
     *             access it.]
     *         - If there isn't one it means that either there was never a peer Java Object
     *         or that any previous peer Java Objects has been garbage collected and their
     *         FinalReferences processed by our Finalizer thread.
     */
    synchronized (peerlock)
    {
        peermap_t::iterator iter = peermap->find(obj);
        if (iter != peermap->end())
        {
            jref = iter->second;
            jobj = env->CallObjectMethod(jref, FinalReference_GetMethodID);
            if (0 == jobj)
                peermap->erase(iter);
            else
                return jobj;
        }
    }
    if (0 != jref)
    {
        env->DeleteGlobalRef(jref);
        jref = 0;
    }
    assert(0 == jobj && 0 == jref);
    /*
     * Construct new Java object as a candidate to become a peer of this Base::Object.
     */
    jobject jnewobj = _JvmTypeInfo::newJavaObject(env, typeid(*obj));
    jobject jnewref = env->NewGlobalRef(
        env->NewObject(FinalReference_Class, FinalReference_CtorMethodID, jnewobj));
    if (0 == jnewref)
        fatal(env, "JNIEnv.{NewGlobalRef|NewObject}: %s", BASE_JVM_JAVAOBJECTBASECLASS "$FinalReference");
    obj->retain();
    obj->threadsafe();
    setObjectField(env, jnewobj, JavaObject_ObjectFieldID, obj);
    setObjectField(env, jnewref, FinalReference_ObjectFieldID, obj);
    /*
     * While we were constructing our candidate it is possible that another thread was faster
     * at constructing its own candidate and registering it as a peer of this Base::Object.
     *
     * We must therefore recheck the peer mapping using the steps described earlier.
     *     - Attempt to get the peer Java Object for this Base::Object:
     *         - If this succeeds we must first destroy our candidate and its FinalReference
     *         (outside the lock) and then return the found peer.
     *         - If this fails we insert the candidate's FinalReference into the peer mapping.
     *         We must also destroy the old FinalReference if there was one (in the rare case
     *         that another thread was able to construct its own candidate, register it as peer
     *         and have it garbage collected all while we were constructing our own candidate).
     */
    synchronized (peerlock)
    {
        peermap_t::iterator iter = peermap->find(obj);
        if (iter != peermap->end())
        {
            jref = iter->second;
            jobj = env->CallObjectMethod(jref, FinalReference_GetMethodID);
            if (0 == jobj)
                peermap->erase(iter);
        }
        if (0 == jobj)
            peermap->insert(peermap_t::value_type(obj, jnewref));
    }
    if (0 != jobj)
    {
        env->DeleteGlobalRef(jnewref);
        env->DeleteLocalRef(jnewobj);
        return jobj;
    }
    else
    {
        if (0 != jref)
            env->DeleteGlobalRef(jref);
        return jnewobj;
    }
}
Object *JvmNativeObject(JNIEnv *env, jobject jobj)
{
    if (0 == jobj)
        return 0;
    Object *obj = getObjectField(env, jobj, JavaObject_ObjectFieldID);
    if (0 == obj)
        fatal(0, "use of disposed object: %s", JvmClassName(env, jobj));
    return obj;
}
void JvmSetNativeObject(JNIEnv *env, jobject jobj, Object *obj)
{
    /*
     * This function is designed to be called from the constructor of a Java Object.
     * Ownership of the Base::Object is transfered to the Java Object.
     *
     * Construct a new FinalReference for this Java Object and register it with the peer mapping.
     */
    jobject jref = env->NewGlobalRef(
        env->NewObject(FinalReference_Class, FinalReference_CtorMethodID, jobj));
    if (0 == jref)
        fatal(env, "JNIEnv.{NewGlobalRef|NewObject}: %s", BASE_JVM_JAVAOBJECTBASECLASS "$FinalReference");
    obj->threadsafe();
    setObjectField(env, jobj, JavaObject_ObjectFieldID, obj);
    setObjectField(env, jref, FinalReference_ObjectFieldID, obj);
    bool inserted;
    synchronized (peerlock)
        inserted = peermap->insert(peermap_t::value_type(obj, jref)).second;
    if (!inserted)
        fatal(0, "native object already present in the peer mapping: %s", obj->strrepr());
}
void _JvmJavaObjectDispose(JNIEnv *env, jobject jobj)
{
    /*
     * This function is the implementation of NativeObject.dispose().
     *
     * Release the peer Base::Object. Remove any FinalReference in the peer mapping and
     * delete it.
     *
     * Because this function is called while the "Java Object" and its FinalReference
     * are still alive, care must be taken to zero out their "_object" fields to avoid
     * double disposal.
     */
    /* NOTE: no collection pool in place! */
    Object *obj = getObjectField(env, jobj, JavaObject_ObjectFieldID);
    if (0 == obj)
        return;
    setObjectField(env, jobj, JavaObject_ObjectFieldID, 0);
    jobject jref = 0;
    synchronized (peerlock)
    {
        peermap_t::iterator iter = peermap->find(obj);
        if (iter != peermap->end())
        {
            jref = iter->second;
            peermap->erase(iter);
        }
    }
    if (0 != jref)
    {
        setObjectField(env, jref, FinalReference_ObjectFieldID, 0);
        env->DeleteGlobalRef(jref);
    }
    obj->release();
}
void _JvmFinalReferenceDispose(JNIEnv *env, jobject jref)
{
    /*
     * This function is the implementation of FinalReference.dispose().
     *
     * Release the peer Base::Object. Remove any FinalReference in the peer mapping and
     * delete it.
     *
     * As an optimization do not zero out the "_object" fields of the Java Object and
     * its FinalReference. This is safe because this is the last code executed for a
     * particular Java Object.
     */
    /* NOTE: no collection pool in place! */
    Object *obj = getObjectField(env, jref, FinalReference_ObjectFieldID);
    if (0 == obj)
        return;
    jref = 0;
    synchronized (peerlock)
    {
        peermap_t::iterator iter = peermap->find(obj);
        if (iter != peermap->end())
        {
            jref = iter->second;
            peermap->erase(iter);
        }
    }
    if (0 != jref)
        env->DeleteGlobalRef(jref);
    obj->release();
}
jclass JvmJavaObjectBaseClass()
{
    return JavaObject_Class;
}
struct _JvmStructInfo : Object
{
    jclass jcls;
    jmethodID jcid;
    struct Field
    {
        char type;
        size_t offset;
        jfieldID jfid;
    } *fields;
};
void *JvmCreateStructMapping(const char *clsname, JvmNativeField *fields, size_t nfields)
{
    JNIEnv *env = JvmEnvironment();
    _JvmStructInfo *info = new _JvmStructInfo;
    info->jcls = JvmClassGlobalRef(env, clsname);
    info->jcid = env->GetMethodID(info->jcls, "<init>", "()V");
    if (0 == info->jcid)
        fatal(env, "JNIEnv.GetMethodID: %s,%s", clsname, "<init>");
    info->fields = (_JvmStructInfo::Field *)carr_new(0, nfields, sizeof(_JvmStructInfo::Field));
    for (size_t i = 0; nfields > i; i++)
    {
        JvmNativeField *p = fields + i;
        _JvmStructInfo::Field *q = info->fields + i;
        switch (p->signature[0])
        {
        case 'Z':
        case 'B':
        case 'S':
        case 'I':
        case 'J':
        case 'F':
        case 'D':
        case 'L':
            break;
        default:
            fatal(env, "JvmCreateStructMapping: unsupported field signature: %s", p->signature);
            break;
        }
        q->type = p->signature[0];
        if (0 == strcmp("Ljava/lang/String;", p->signature))
            q->type = 's';
        q->offset = p->offset;
        q->jfid = env->GetFieldID(info->jcls, p->name, p->signature);
        if (0 == q->jfid)
            fatal(env, "JNIEnv.GetFieldID: %s,%s", clsname, p->name);
    }
    return info;
}
void JvmStructFromJavaObject(JNIEnv *env, void *mapping, jobject jobj, void *buf)
{
    _JvmStructInfo *info = (_JvmStructInfo *)mapping;
    for (_JvmStructInfo::Field *p = info->fields, *endp = p + carr_length(p); endp > p; p++)
        switch (p->type)
        {
        case 'Z':
            *(bool *)((char *)buf + p->offset) = env->GetBooleanField(jobj, p->jfid);
            break;
        case 'B':
            *(int8_t *)((char *)buf + p->offset) = env->GetByteField(jobj, p->jfid);
            break;
        case 'S':
            *(short *)((char *)buf + p->offset) = env->GetShortField(jobj, p->jfid);
            break;
        case 'I':
            *(int *)((char *)buf + p->offset) = env->GetIntField(jobj, p->jfid);
            break;
        case 'J':
            *(int64_t *)((char *)buf + p->offset) = env->GetLongField(jobj, p->jfid);
            break;
        case 'F':
            *(float *)((char *)buf + p->offset) = env->GetFloatField(jobj, p->jfid);
            break;
        case 'D':
            *(double *)((char *)buf + p->offset) = env->GetDoubleField(jobj, p->jfid);
            break;
        case 'L':
            *(Object **)((char *)buf + p->offset) = JvmNativeObject(env,
                env->GetObjectField(jobj, p->jfid))->autocollect();
            break;
        case 's': /* shorthand for "Ljava/lang/String;" */
            *(const char **)((char *)buf + p->offset) = JvmCString(env,
                (jstring)env->GetObjectField(jobj, p->jfid));
            break;
        default:
            break;
        }
}
jobject JvmJavaObjectFromStruct(JNIEnv *env, void *mapping, const void *buf)
{
    _JvmStructInfo *info = (_JvmStructInfo *)mapping;
    jobject jobj = env->NewObject(info->jcls, info->jcid);
    if (0 == jobj)
        return 0;
    for (_JvmStructInfo::Field *p = info->fields, *endp = p + carr_length(p); endp > p; p++)
        switch (p->type)
        {
        case 'Z':
            env->SetBooleanField(jobj, p->jfid, *(bool *)((char *)buf + p->offset));
            break;
        case 'B':
            env->SetByteField(jobj, p->jfid, *(int8_t *)((char *)buf + p->offset));
            break;
        case 'S':
            env->SetShortField(jobj, p->jfid, *(short *)((char *)buf + p->offset));
            break;
        case 'I':
            env->SetIntField(jobj, p->jfid, *(int *)((char *)buf + p->offset));
            break;
        case 'J':
            env->SetLongField(jobj, p->jfid, *(int64_t *)((char *)buf + p->offset));
            break;
        case 'F':
            env->SetFloatField(jobj, p->jfid, *(float *)((char *)buf + p->offset));
            break;
        case 'D':
            env->SetDoubleField(jobj, p->jfid, *(double *)((char *)buf + p->offset));
            break;
        case 'L':
            env->SetObjectField(jobj, p->jfid,
                JvmJavaObject(env, *(Object **)((char *)buf + p->offset)));
            break;
        case 's': /* shorthand for "Ljava/lang/String;" */
            env->SetObjectField(jobj, p->jfid,
                JvmJavaString(env, *(const char **)((char *)buf + p->offset)));
            break;
        default:
            break;
        }
    return jobj;
}
#else
void _JvmInitializePeerMapping()
{
}
void JvmRegisterPeerTypes(const char *clsname, const std::type_info &tinfo)
{
}
jobject JvmJavaObject(JNIEnv *env, Object *obj)
{
    return 0;
}
Object *JvmNativeObject(JNIEnv *env, jobject jobj)
{
    return 0;
}
Object *JvmNativeObjectAllowNull(JNIEnv *env, jobject jobj)
{
    return 0;
}
void JvmSetNativeObject(JNIEnv *env, jobject jobj, Object *obj)
{
}
void _JvmJavaObjectDispose(JNIEnv *env, jobject jobj)
{
}
void _JvmFinalReferenceDispose(JNIEnv *env, jobject jref)
{
}
jclass JvmJavaObjectBaseClass()
{
    return 0;
}
void *JvmCreateStructMapping(const char *clsname, JvmNativeField *fields, size_t nfields)
{
    return 0;
}
void JvmStructFromJavaObject(JNIEnv *env, void *mapping, jobject jobj, void *buf)
{
}
jobject JvmJavaObjectFromStruct(JNIEnv *env, void *mapping, const void *buf)
{
    return 0;
}
#endif

}
}
