/*
 * Base/impl/StdioStream.cpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#include <Base/impl/StdioStream.hpp>

namespace Base {
namespace impl {

StdioStream *StdioStream::create(const char *path, const char *mode)
{
    FILE *file = fopen(path, mode);
    if (0 == file)
        return 0;
    return new StdioStream(file);
}
StdioStream::StdioStream(FILE *file)
{
    _file = file;
}
StdioStream::~StdioStream()
{
    close();
}
ssize_t StdioStream::read(void *buf, size_t size)
{
    if (0 == _file)
        return -1;
    ssize_t ret = fread(buf, 1, size, _file);
    if (ret != size && ferror(_file))
        return -1;
    return ret;
}
ssize_t StdioStream::write(const void *buf, size_t size)
{
    if (0 == _file)
        return -1;
    ssize_t ret = fwrite(buf, 1, size, _file);
    if (ret != size && ferror(_file))
        return -1;
    return ret;
}
ssize_t StdioStream::seek(ssize_t offset, int whence)
{
    if (0 == _file)
        return -1;
    if (-1 == fseek(_file, offset, whence))
        return -1;
    return ftell(_file);
}
ssize_t StdioStream::close()
{
    if (0 == _file)
        return -1;
    int result = fclose(_file);
    _file = 0;
    return EOF == result ? -1 : 0;
}

}
}
