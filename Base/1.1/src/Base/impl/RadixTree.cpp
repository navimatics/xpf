/*
 * Base/impl/RadixTree.cpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#include <Base/impl/RadixTree.hpp>
#include <Base/impl/BitHacks.hpp>
#include <Base/impl/CArray.hpp>
#include <Base/impl/Coroutine.hpp>

namespace Base {
namespace impl {

#define INDEX_BITS                      IMAX_BITS(UINTPTR_MAX)
#define BTEST(index, bitpos)            (((index) >> (INDEX_BITS - 1 - (bitpos))) & 1)

static inline int select(RadixTree::Node *&node, index_t index)
{
    int bitpos = node->bitpos;
    node = node->child[BTEST(index, bitpos)];
    return bitpos;
}

RadixTree::RadixTree(size_t capacity)
{
    _nodes = (Node *)carr_newWithCapacity(1 + capacity, sizeof(Node));
    carr_obj(_nodes)->setLength(1);
    _nodes->index = _nodes->value = 0;
    _nodes->bitpos = -1;
    _nodes->child[0] = _nodes->child[1] = _nodes;
}
RadixTree::~RadixTree()
{
    carr_release(_nodes);
}
void RadixTree::reset()
{
    carr_obj(_nodes)->setLength(1);
    _nodes->child[0] = _nodes->child[1] = _nodes;
}
RadixTree::Node *RadixTree::lookup(index_t index)
{
    Node *node = _nodes->child[0];
    for (int bitpos = -1; bitpos < node->bitpos;)
        bitpos = select(node, index);
    return node;
}
void RadixTree::insert(Node *node, index_t index, index_t value)
{
    /* alloc */
    Node *newnode = _nodes->child[1];
    if (_nodes != newnode)
        _nodes->child[1] = newnode->child[1];
    else
    {
        size_t n = carr_length(_nodes);
        Node *newnodes = (Node *)carr_concat(_nodes, 0, 1, sizeof(Node));
        if (ptrdiff_t ptrdiff = (char *)newnodes - (char *)_nodes)
        {
            _nodes = newnodes;
            for (Node *p = _nodes, *endp = _nodes + n; endp > p; p++)
            {
                p->child[0] = (Node *)((char *)p->child[0] + ptrdiff);
                p->child[1] = (Node *)((char *)p->child[1] + ptrdiff);
            }
            node = (Node *)((char *)node + ptrdiff);
        }
        newnode = _nodes + n;
    }
    newnode->index = index;
    newnode->value = value;
    /* insert */
    int bitdiff = bit_clz(node->index ^ index);
    Node *parent = _nodes;
    node = parent->child[0];
    for (int bitpos = -1; bitpos < node->bitpos && node->bitpos < bitdiff;)
    {
        parent = node;
        bitpos = select(node, index);
    }
    newnode->bitpos = bitdiff;
    if (0 == BTEST(index, bitdiff))
    {
        newnode->child[0] = newnode;
        newnode->child[1] = node;
    }
    else
    {
        newnode->child[0] = node;
        newnode->child[1] = newnode;
    }
    if (node == parent->child[0])
        parent->child[0] = newnode;
    else
        parent->child[1] = newnode;
}
void RadixTree::remove(Node *remnode)
{
    index_t index = remnode->index;
    Node *parent = _nodes;
    Node *node = parent->child[0];
    while (remnode != node)
    {
        parent = node;
        select(node, index);
    }
    Node *leaf;
    do
    {
        leaf = node;
        select(node, index);
    } while (remnode != node);
    if (remnode != leaf)
    {
        leaf->bitpos = remnode->bitpos;
        if (remnode == leaf->child[0])
        {
            if (leaf != leaf->child[1])
            {
                Node *leafParent = parent;
                node = remnode;
                while (leaf != node)
                {
                    leafParent = node;
                    select(node, index);
                }
                if (leaf == leafParent->child[0])
                    leafParent->child[0] = leaf->child[1];
                else
                    leafParent->child[1] = leaf->child[1];
            }
        }
        else
        {
            if (leaf != leaf->child[0])
            {
                Node *leafParent = parent;
                node = remnode;
                while (leaf != node)
                {
                    leafParent = node;
                    select(node, index);
                }
                if (leaf == leafParent->child[0])
                    leafParent->child[0] = leaf->child[0];
                else
                    leafParent->child[1] = leaf->child[0];
            }
        }
        leaf->child[0] = remnode->child[0];
        leaf->child[1] = remnode->child[1];
    }
    else
    {
        if (remnode == leaf->child[0])
            leaf = leaf->child[1];
        else
            leaf = leaf->child[0];
    }
    if (remnode == parent->child[0])
        parent->child[0] = leaf;
    else
        parent->child[1] = leaf;
    /* dealloc */
    remnode->bitpos = -1;
    remnode->child[0] = _nodes;
    remnode->child[1] = _nodes->child[1];
    _nodes->child[1] = remnode;
}
void RadixTree::__sanityCheck()
{
#if !defined(NDEBUG)
    int *refcnt = (int *)carr_new(0, carr_length(_nodes), sizeof(int));
    Node *endp = _nodes + carr_length(_nodes);
    refcnt[_nodes->child[0] - _nodes]++;
    Iterable<index_t> *iter = new (collect) NodeIterator(this);
    foreach (index_t inode, iter)
    {
        Node *node = (Node *)inode;
        assert(_nodes < node && node < endp);
        assert(_nodes <= node->child[0] && node->child[0] < endp);
        assert(_nodes <= node->child[1] && node->child[1] < endp);
        assert(0 <= node->bitpos);
        assert(
            node->bitpos <= node->child[0]->bitpos ||
            node->bitpos <= node->child[1]->bitpos);
        refcnt[node->child[0] - _nodes]++;
        refcnt[node->child[1] - _nodes]++;
    }
    for (Node *node = _nodes->child[1]; _nodes != node; node = node->child[1])
    {
        assert(_nodes < node && node < endp);
        assert(0 == refcnt[node - _nodes]);
        refcnt[node - _nodes] = -1;
    }
    for (index_t inode = 1, n = carr_length(_nodes); n > inode; inode++)
        assert(2 == refcnt[inode] || -1 == refcnt[inode]);
    assert(0 == _nodes->index);
    assert(0 == _nodes->value);
    carr_release(refcnt);
#endif
}
void *RadixTree::NodeIterator::iterate(void *state, index_t *bufp[2])
{
    index_t *curp = bufp[0], *endp = bufp[1];
    coroblock (this)
        for (;;)
        {
            do
            {
                bitpos = node->bitpos;
                *stkptr++ = node;
                node = node->child[0];
            } while (bitpos < node->bitpos);
            do
            {
                node = *--stkptr;
                if (stkbuf == stkptr)
                    corofini (this);
                bitpos = node->bitpos;
                left = node->child[0];
                if (bitpos >= left->bitpos && -1 != left->bitpos)
                {
                    if (curp >= endp)
                        coroyield (this);
                    *curp++ = (index_t)left;
                }
                right = node->child[1];
                if (bitpos >= right->bitpos && -1 != right->bitpos)
                {
                    if (curp >= endp)
                        coroyield (this);
                    *curp++ = (index_t)right;
                }
            } while (bitpos >= right->bitpos);
            node = right;
        }
    bufp[1] = curp;
    return this;
}

}
}
