/*
 * Base/impl/ApplicationGuard.cpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#include <Base/impl/ApplicationGuard.hpp>
#include <Base/impl/CString.hpp>
#include <Base/impl/Error.hpp>
#include <Base/impl/Synchronization.hpp>
#if defined(BASE_CONFIG_WINDOWS)
#include <fcntl.h>
#include <io.h>
#include <windows.h>
#elif defined(BASE_CONFIG_POSIX)
#include <fcntl.h>
#else
#error ApplicationGuard.cpp not implemented for this platform
#endif

namespace Base {
namespace impl {

#define LOCKBYTE                        1073741824 /* 1 GiB */

bool _ApplicationGuard(const char *path, FILE **filep)
{
#if defined(BASE_CONFIG_WINDOWS)
    HANDLE handle = CreateFileA(path,
        GENERIC_READ | GENERIC_WRITE, FILE_SHARE_READ | FILE_SHARE_WRITE | FILE_SHARE_DELETE,
        0/* no inherit */, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, 0);
    if (INVALID_HANDLE_VALUE == handle)
    {
        Error::setLastErrorFromSystem();
        if (0 != filep)
            *filep = 0;
        return false;
    }
    /*
     * Lock 1 byte exclusively at a high offset. If we are successful in getting this lock,
     * it means we are the primary instance.
     *
     * The OS cleans up locks abandoned by dead processes. Even if our process dies unexpectedly
     * the lock will be removed, thus allowing another process to set a new lock.
     *
     * On Windows LockFile() does not allow the same process that locks a file through one file
     * handle to lock it again through another file handle.
     */
    bool result = FALSE != LockFile(handle, LOCKBYTE, 0, 1, 0);
    if (result)
        /* if we have the lock on the file, truncate it to zero size */
        SetEndOfFile(handle);
    else
        Error::setLastErrorFromSystem();
    if (0 != filep)
    {
        /* open stdio FILE from OS file handle */
        int fd = _open_osfhandle((intptr_t)handle, result ? _O_RDWR | _O_BINARY : _O_RDONLY | _O_BINARY);
        *filep = -1 != fd ? _fdopen(fd, result ? "r+b" : "rb") : 0;
    }
    return result;
#elif defined(BASE_CONFIG_POSIX)
    int fd = open(path, O_CREAT | O_RDWR, 0777);
    if (-1 == fd)
    {
        Error::setLastErrorFromSystem();
        if (0 != filep)
            *filep = 0;
        return false;
    }
    int flags = fcntl(fd, F_GETFD);
    if (-1 != flags)
    {
        /* set the no inherit flag */
        flags |= FD_CLOEXEC;
        fcntl(fd, F_SETFD, flags);
    }
    /*
     * Lock 1 byte exclusively at a high offset. If we are successful in getting this lock,
     * it means we are the primary instance.
     *
     * The OS cleans up locks abandoned by dead processes. Even if our process dies unexpectedly
     * the lock will be removed, thus allowing another process to set a new lock.
     *
     * On POSIX F_SETLK allows the same process that locks a file through one file descriptor
     * to lock it again through another file descriptor.
     */
    struct flock flock;
    flock.l_type = F_WRLCK;
    flock.l_whence = SEEK_SET;
    flock.l_start = LOCKBYTE;
    flock.l_len = 1;
    bool result = -1 != fcntl(fd, F_SETLK, &flock);
    if (result)
        /* if we have the lock on the file, truncate it to zero size */
        ftruncate(fd, 0);
    else
        Error::setLastErrorFromSystem();
    if (0 != filep)
        /* open stdio FILE from file descriptor */
        *filep = fdopen(fd, result ? "r+b" : "rb");
    return result;
#endif
}
static const char *guardPath;
static bool guardResult;
static FILE *guardFile;
static void guardRemove()
{
    fclose(guardFile);
    remove(guardPath);
}
bool ApplicationGuard(const char *path, FILE **filep)
{
    execute_once
    {
        cstr_assign_copy(guardPath, path);
        guardResult = _ApplicationGuard(path, &guardFile);
        if (guardResult && 0 != guardFile)
            atexit(guardRemove);
    }
    assert(0 == strcmp(guardPath, path));
    if (0 != filep)
        *filep = guardFile;
    return guardResult;
}

}
}
