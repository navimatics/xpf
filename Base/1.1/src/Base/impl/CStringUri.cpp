/*
 * Base/impl/CStringUri.cpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#include <Base/impl/CString.hpp>

namespace Base {
namespace impl {

/*
 * Based on RFC 3986
 */

#define unreserved                      \
    "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-._~"
#define gen_delims                      ":/?#[]@"
#define sub_delims                      "!$&'()*+,;="

static inline char hexdig(uint8_t c)
{
    return "0123456789ABCDEF"[c];
}
static inline uint8_t hexval(char c)
{
    if ('9' >= c)
        return c - '0';
    else
        return (c | 0x20) + 10 - 'a';
}

struct uri_parser
{
    const char *src;
    const char *scheme, *schemep;
    const char *userinfo, *userinfop;
    const char *host, *hostp;
    const char *port, *portp;
    const char *path, *pathp;
    const char *query, *queryp;
    const char *fragment, *fragmentp;
    uri_parser(const char *src);
    void parse();
    void parse_scheme();
    void parse_authority();
    void parse_userinfo();
    void parse_hostspec();
    void parse_port();
    void parse_path();
    void parse_query();
    void parse_fragment();
};
inline uri_parser::uri_parser(const char *src)
{
    memset(this, 0, sizeof *this);
    this->src = src;
}
void uri_parser::parse()
{
    parse_scheme();
    parse_authority();
    parse_path();
    parse_query();
    parse_fragment();
}
void uri_parser::parse_scheme()
{
    for (scheme = schemep = src;; schemep++)
        switch (*schemep)
        {
        case '\0':
        case '/':
        case '?':
        case '#':
            scheme = schemep = 0;
            return;
        case ':':
            src = schemep + 1;
            return;
        default:
            break;
        }
}
void uri_parser::parse_authority()
{
    if ('/' != src[0] || '/' != src[1])
        return;
    src += 2;
    parse_userinfo();
    parse_hostspec();
}
void uri_parser::parse_userinfo()
{
    for (userinfo = userinfop = src;; userinfop++)
        switch (*userinfop)
        {
        case '\0':
        case '/':
        case '?':
        case '#':
            userinfo = userinfop = 0;
            return;
        case '@':
            src = userinfop + 1;
            return;
        default:
            break;
        }
}
void uri_parser::parse_hostspec()
{
    int bracket = 0;
    for (host = hostp = src;; hostp++)
        switch (*hostp)
        {
        case '\0':
        case '/':
        case '?':
        case '#':
            src = hostp;
            return;
        case '[':
            bracket++;
            break;
        case ']':
            bracket--;
            break;
        case ':':
            if (0 != bracket)
                break;
            src = hostp + 1;
            parse_port();
            return;
        default:
            break;
        }
}
void uri_parser::parse_port()
{
    for (port = portp = src;; portp++)
        switch (*portp)
        {
        case '\0':
        case '/':
        case '?':
        case '#':
            src = portp;
            return;
        default:
            break;
        }
}
void uri_parser::parse_path()
{
    for (path = pathp = src;; pathp++)
        switch (*pathp)
        {
        case '\0':
        case '?':
        case '#':
            src = pathp;
            return;
        default:
            break;
        }
}
void uri_parser::parse_query()
{
    if ('?' != src[0])
        return;
    src++;
    for (query = queryp = src;; queryp++)
        switch (*queryp)
        {
        case '\0':
        case '#':
            src = queryp;
            return;
        default:
            break;
        }
}
void uri_parser::parse_fragment()
{
    if ('#' != src[0])
        return;
    src++;
    for (fragment = fragmentp = src;; fragmentp++)
        switch (*fragmentp)
        {
        case '\0':
            src = fragmentp;
            return;
        default:
            break;
        }
}

/* unreserved chars bitmap */
static uint8_t unrestab[32] =
{
    /*          */ 0x00,
    /*          */ 0x00,
    /*          */ 0x00,
    /*          */ 0x00,
    /*  !"#$%&' */ 0x00,
    /* ()*+,-./ */ 0x06,
    /* 01234567 */ 0xff,
    /* 89:;<=>? */ 0xc0,
    /* @ABCDEFG */ 0x7f,
    /* HIJKLMNO */ 0xff,
    /* PQRSTUVW */ 0xff,
    /* XYZ[\]^_ */ 0xe1,
    /* `abcdefg */ 0x7f,
    /* hijklmno */ 0xff,
    /* pqrstuvw */ 0xff,
    /* xyz{|}~  */ 0xe2,
    /*          */ 0x00,
    /*          */ 0x00,
    /*          */ 0x00,
    /*          */ 0x00,
    /*          */ 0x00,
    /*          */ 0x00,
    /*          */ 0x00,
    /*          */ 0x00,
    /*          */ 0x00,
    /*          */ 0x00,
    /*          */ 0x00,
    /*          */ 0x00,
    /*          */ 0x00,
    /*          */ 0x00,
    /*          */ 0x00,
    /*          */ 0x00,
};
static const char *uri_concat_escape(
    const char *dst, const char *src, const char *exclude, bool lowercase, bool spaceToPlus = false)
{
    /* bitmap of chars excluded from escaping */
    uint8_t exctab[NELEMS(unrestab)];
    memcpy(exctab, unrestab, sizeof unrestab);
    if (const char *e = exclude)
        while (*e)
        {
            uint8_t c = *e++;
            if ('%' != c)
                exctab[c >> 3] |= 0x80 >> (c & 7);
        }
    size_t dstlen = 0;
    const char *srcp = src;
    for (;;)
    {
        uint8_t c = *srcp;
        if ('\0' == c)
            break;
        else if (exctab[c >> 3] & (0x80 >> (c & 7)))
            dstlen++;
        else if (spaceToPlus && ' ' == c)
            dstlen++;
        else
            dstlen += 3;
        srcp++;
    }
    dst = cstr_concat(dst, 0, dstlen);
    char *dstp = (char *)dst + cstr_length(dst) - dstlen;
    srcp = src;
    for (;;)
    {
        uint8_t c = *srcp;
        if ('\0' == c)
            break;
        else if (exctab[c >> 3] & (0x80 >> (c & 7)))
        {
            if (lowercase && ('A' <= c && c <= 'Z'))
                /* ASCII lowercase */
                c |= 0x20;
            *dstp++ = c;
        }
        else if (spaceToPlus && ' ' == c)
            *dstp++ = '+';
        else
        {
            *dstp++ = '%';
            *dstp++ = hexdig(c >> 4);
            *dstp++ = hexdig(c & 0x0f);
        }
        srcp++;
    }
    return dst;
}
const char *uri_unescape(const char *src, size_t srclen, bool lowercase, bool plusToSpace = false)
{
    size_t dstlen = 0;
    const char *srcp = src, *endp = src + srclen;
    while (endp > srcp)
    {
        uint8_t c = *srcp;
        if ('%' == c && endp >= srcp + 3 && isxdigit(srcp[1]) && isxdigit(srcp[2]))
            srcp += 3;
        else
            srcp++;
        dstlen++;
    }
    const char *dst = cstring(0, dstlen);
    char *dstp = (char *)dst;
    srcp = src;
    while (endp > srcp)
    {
        uint8_t c = *srcp;
        if ('%' == c && endp >= srcp + 3 && isxdigit(srcp[1]) && isxdigit(srcp[2]))
        {
            *dstp++ = (hexval(srcp[1]) << 4) | hexval(srcp[2]);
            srcp += 3;
        }
        else
        {
            if (lowercase && ('A' <= c && c <= 'Z'))
                /* ASCII lowercase */
                c |= 0x20;
            else if (plusToSpace && '+' == c)
                c = ' ';
            *dstp++ = c;
            srcp++;
        }
    }
    return dst;
}
static const char *uri_concat_norm(
    const char *dst, const char *src, size_t srclen, const char *exclude, bool lowercase)
{
    /* bitmap of chars excluded from escaping */
    uint8_t exctab[NELEMS(unrestab)];
    memcpy(exctab, unrestab, sizeof unrestab);
    if (const char *e = exclude)
        while (*e)
        {
            uint8_t c = *e++;
            if ('%' != c)
                exctab[c >> 3] |= 0x80 >> (c & 7);
        }
    size_t dstlen = 0;
    const char *srcp = src, *endp = src + srclen;
    while (endp > srcp)
    {
        uint8_t c = *srcp;
        if ('%' == c && endp >= srcp + 3 && isxdigit(srcp[1]) && isxdigit(srcp[2]))
        {
            c = (hexval(srcp[1]) << 4) | hexval(srcp[2]);
            if (exctab[c >> 3] & (0x80 >> (c & 7)))
                dstlen++;
            else
                dstlen += 3;
            srcp += 3;
        }
        else if (exctab[c >> 3] & (0x80 >> (c & 7)))
        {
            dstlen++;
            srcp++;
        }
        else
        {
            dstlen += 3;
            srcp++;
        }
    }
    dst = cstr_concat(dst, 0, dstlen);
    char *dstp = (char *)dst + cstr_length(dst) - dstlen;
    srcp = src;
    while (endp > srcp)
    {
        uint8_t c = *srcp;
        if ('%' == c && endp >= srcp + 3 && isxdigit(srcp[1]) && isxdigit(srcp[2]))
        {
            c = (hexval(srcp[1]) << 4) | hexval(srcp[2]);
            if (exctab[c >> 3] & (0x80 >> (c & 7)))
            {
                if (lowercase && ('A' <= c && c <= 'Z'))
                    /* ASCII lowercase */
                    c |= 0x20;
                *dstp++ = c;
            }
            else
            {
                /* ASCII uppercase */
                *dstp++ = '%';
                *dstp++ = 'a' <= srcp[1] ? srcp[1] & ~0x20 : srcp[1];
                *dstp++ = 'a' <= srcp[2] ? srcp[2] & ~0x20 : srcp[2];
            }
            srcp += 3;
        }
        else if (exctab[c >> 3] & (0x80 >> (c & 7)))
        {
            if (lowercase && ('A' <= c && c <= 'Z'))
                /* ASCII lowercase */
                c |= 0x20;
            *dstp++ = c;
            srcp++;
        }
        else
        {
            *dstp++ = '%';
            *dstp++ = hexdig(c >> 4);
            *dstp++ = hexdig(c & 0x0f);
            srcp++;
        }
    }
    return dst;
}
static char *uri_remove_dot_segments(char *dst, const char *src, bool preserve_root)
{
    const char *srcp = src;
    char *dstp = dst;
    while ('\0' != srcp[0])
    {
        if ('.' == srcp[0])
        {
            if (src == srcp || '/' == srcp[-1])
            {
                if ('\0' == srcp[1])
                {
                    srcp++;
                    continue;
                }
                else if ('/' == srcp[1])
                {
                    srcp += 2;
                    continue;
                }
                else if ('.' == srcp[1])
                {
                    bool parent = false;
                    if ('\0' == srcp[2])
                    {
                        srcp += 2;
                        parent = true;
                    }
                    else if ('/' == srcp[2])
                    {
                        srcp += 3;
                        parent = true;
                    }
                    if (parent)
                    {
                        if (dst < dstp)
                        {
                            if ('/' == dstp[-1])
                                dstp -= 2;
                            while (dst <= dstp && '/' != *dstp)
                                dstp--;
                            dstp++;
                            if (preserve_root && dst == dstp)
                                *dstp++ = '/';
                        }
                        continue;
                    }
                }
            }
        }
        *dstp++ = *srcp++;
    }
    return dstp;
}
static const char *uri_normalize(const uri_parser &parser, bool query_processed)
{
    bool authority = false;
    const char *dst = cstr_new(0);
    if (0 != parser.scheme)
    {
        dst = uri_concat_norm(dst, parser.scheme, parser.schemep - parser.scheme,
            "+", true);
        dst = cstr_concat(dst, ":", 1);
    }
    if (0 != parser.userinfo || 0 != parser.host || 0 != parser.port)
    {
        authority = true;
        dst = cstr_concat(dst, "//", 2);
        if (0 != parser.userinfo)
        {
            dst = uri_concat_norm(dst, parser.userinfo, parser.userinfop - parser.userinfo,
                sub_delims ":", false);
            dst = cstr_concat(dst, "@", 1);
        }
        if (0 != parser.host)
            if (parser.hostp != parser.host && '[' != parser.host[0])
                dst = uri_concat_norm(dst, parser.host, parser.hostp - parser.host,
                    sub_delims, true);
            else
                dst = uri_concat_norm(dst, parser.host, parser.hostp - parser.host,
                    sub_delims ":[]", true);
        if (0 != parser.port)
        {
            dst = cstr_concat(dst, ":", 1);
            dst = uri_concat_norm(dst, parser.port, parser.portp - parser.port,
                0, false);
        }
    }
    if (0 != parser.path)
    {
        size_t len = cstr_length(dst);
        dst = uri_concat_norm(dst, parser.path, parser.pathp - parser.path,
            sub_delims ":@/", false);
        char *dstp = (char *)dst + len;
        dstp = uri_remove_dot_segments(dstp, dstp, authority);
        *dstp = '\0'; cstr_obj(dst)->setLength(dstp - dst);
    }
    if (0 != parser.query)
    {
        dst = cstr_concat(dst, "?", 1);
        if (!query_processed)
            dst = cstr_concat(dst, parser.query, parser.queryp - parser.query);
        else
            dst = uri_concat_norm(dst, parser.query, parser.queryp - parser.query,
                sub_delims ":@/?", false);
    }
    if (0 != parser.fragment)
    {
        dst = cstr_concat(dst, "#", 1);
        dst = uri_concat_norm(dst, parser.fragment, parser.fragmentp - parser.fragment,
            sub_delims ":@/?", false);
    }
    cstr_autorelease(dst);
    return dst;
}

void cstr_uri_parseraw(const char *src, cstr_uri_parts *parts)
{
    memset(parts, 0, sizeof *parts);
    uri_parser parser(src);
    parser.parse();
    if (0 != parser.scheme)
        parts->scheme = cstring(parser.scheme, parser.schemep - parser.scheme);
    if (0 != parser.userinfo)
        parts->userinfo = cstring(parser.userinfo, parser.userinfop - parser.userinfo);
    if (0 != parser.host)
        parts->host = cstring(parser.host, parser.hostp - parser.host);
    if (0 != parser.port)
        parts->port = cstring(parser.port, parser.portp - parser.port);
    if (0 != parser.path)
        parts->path = cstring(parser.path, parser.pathp - parser.path);
    if (0 != parser.query)
        parts->query = cstring(parser.query, parser.queryp - parser.query);
    if (0 != parser.fragment)
        parts->fragment = cstring(parser.fragment, parser.fragmentp - parser.fragment);
}
const char *cstr_uri_composeraw(const cstr_uri_parts *parts)
{
    bool authority = false;
    const char *dst = cstr_new(0);
    if (0 != parts->scheme)
    {
        dst = cstr_concat(dst, parts->scheme);
        dst = cstr_concat(dst, ":", 1);
    }
    if (0 != parts->userinfo || 0 != parts->host || 0 != parts->port)
    {
        authority = true;
        dst = cstr_concat(dst, "//", 2);
        if (0 != parts->userinfo)
        {
            dst = cstr_concat(dst, parts->userinfo);
            dst = cstr_concat(dst, "@", 1);
        }
        if (0 != parts->host)
            dst = cstr_concat(dst, parts->host);
        if (0 != parts->port)
        {
            dst = cstr_concat(dst, ":", 1);
            dst = cstr_concat(dst, parts->port);
        }
    }
    if (0 != parts->path)
    {
        if (authority && '\0' != parts->path[0] && '/' != parts->path[0])
            dst = cstr_concat(dst, "/", 1);
        dst = cstr_concat(dst, parts->path);
    }
    if (0 != parts->query)
    {
        dst = cstr_concat(dst, "?", 1);
        dst = cstr_concat(dst, parts->query);
    }
    if (0 != parts->fragment)
    {
        dst = cstr_concat(dst, "#", 1);
        dst = cstr_concat(dst, parts->fragment);
    }
    cstr_autorelease(dst);
    return dst;
}
void cstr_uri_parse(const char *src, cstr_uri_parts *parts, bool query_processed)
{
    memset(parts, 0, sizeof *parts);
    uri_parser parser(src);
    parser.parse();
    if (0 != parser.scheme)
        parts->scheme = uri_unescape(parser.scheme, parser.schemep - parser.scheme, true);
    if (0 != parser.userinfo)
        parts->userinfo = uri_unescape(parser.userinfo, parser.userinfop - parser.userinfo, false);
    if (0 != parser.host)
        parts->host = uri_unescape(parser.host, parser.hostp - parser.host, true);
    if (0 != parser.port)
        parts->port = uri_unescape(parser.port, parser.portp - parser.port, false);
    if (0 != parser.path)
        parts->path = uri_unescape(parser.path, parser.pathp - parser.path, false);
    if (0 != parser.query)
        if (!query_processed)
            parts->query = cstring(parser.query, parser.queryp - parser.query);
        else
            parts->query = uri_unescape(parser.query, parser.queryp - parser.query, false);
    if (0 != parser.fragment)
        parts->fragment = uri_unescape(parser.fragment, parser.fragmentp - parser.fragment, false);
}
const char *cstr_uri_compose(const cstr_uri_parts *parts, bool query_processed)
{
    bool authority = false;
    const char *dst = cstr_new(0);
    if (0 != parts->scheme)
    {
        dst = uri_concat_escape(dst, parts->scheme, "+", true);
        dst = cstr_concat(dst, ":", 1);
    }
    if (0 != parts->userinfo || 0 != parts->host || 0 != parts->port)
    {
        authority = true;
        dst = cstr_concat(dst, "//", 2);
        if (0 != parts->userinfo)
        {
            dst = uri_concat_escape(dst, parts->userinfo, sub_delims ":", false);
            dst = cstr_concat(dst, "@", 1);
        }
        if (0 != parts->host)
            if ('[' != parts->host[0])
                dst = uri_concat_escape(dst, parts->host, sub_delims, true);
            else
                dst = uri_concat_escape(dst, parts->host, sub_delims ":[]", true);
        if (0 != parts->port)
        {
            dst = cstr_concat(dst, ":", 1);
            dst = uri_concat_escape(dst, parts->port, 0, false);
        }
    }
    if (0 != parts->path)
    {
        if (authority && '\0' != parts->path[0] && '/' != parts->path[0])
            dst = cstr_concat(dst, "/", 1);
        dst = uri_concat_escape(dst, parts->path, sub_delims ":@/", false);
    }
    if (0 != parts->query)
    {
        dst = cstr_concat(dst, "?", 1);
        if (!query_processed)
            dst = cstr_concat(dst, parts->query);
        else
            dst = uri_concat_escape(dst, parts->query, sub_delims ":@/?", false);
    }
    if (0 != parts->fragment)
    {
        dst = cstr_concat(dst, "#", 1);
        dst = uri_concat_escape(dst, parts->fragment, sub_delims ":@/?", false);
    }
    cstr_autorelease(dst);
    return dst;
}
const char *cstr_uri_normalize(const char *src, bool query_processed)
{
    uri_parser parser(src);
    parser.parse();
    return uri_normalize(parser, query_processed);
}
const char *cstr_uri_concat(const char *src1, const char *src2, bool query_processed)
{
    uri_parser parserB(src1);
    parserB.parse();
    uri_parser parserR(src2);
    parserR.parse();
    uri_parser parser(0);
    if (0 != parserR.scheme)
    {
        parser.scheme = parserR.scheme; parser.schemep = parserR.schemep;
        parser.userinfo = parserR.userinfo; parser.userinfop = parserR.userinfop;
        parser.host = parserR.host; parser.hostp = parserR.hostp;
        parser.port = parserR.port; parser.portp = parserR.portp;
        parser.path = parserR.path; parser.pathp = parserR.pathp;
        parser.query = parserR.query; parser.queryp = parserR.queryp;
    }
    else
    {
        if (0 != parserR.userinfo || 0 != parserR.host || 0 != parserR.port)
        {
            parser.userinfo = parserR.userinfo; parser.userinfop = parserR.userinfop;
            parser.host = parserR.host; parser.hostp = parserR.hostp;
            parser.port = parserR.port; parser.portp = parserR.portp;
            parser.path = parserR.path; parser.pathp = parserR.pathp;
            parser.query = parserR.query; parser.queryp = parserR.queryp;
        }
        else
        {
            if (0 == parserR.path || parserR.pathp == parserR.path)
            {
                parser.path = parserB.path; parser.pathp = parserB.pathp;
                if (0 != parserR.query)
                {
                    parser.query = parserR.query; parser.queryp = parserR.queryp;
                }
                else
                {
                    parser.query = parserB.query; parser.queryp = parserB.queryp;
                }
            }
            else
            {
                if ('/' == parserR.path[0])
                {
                    parser.path = parserR.path; parser.pathp = parserR.pathp;
                }
                else
                {
                    if ((0 != parserB.userinfo || 0 != parserB.host || 0 != parserB.port) &&
                        (0 == parserB.path || parserB.pathp == parserB.path))
                    {
                        parser.path = cstr_new("/", 1);
                        parser.path = cstr_concat(parser.path, parserR.path, parserR.pathp - parserR.path);
                        parser.pathp = parser.path + cstr_length(parser.path);
                        cstr_autorelease(parser.path);
                    }
                    else
                    {
                        const char *p = parserB.pathp - 1;
                        while (parserB.path <= p && '/' != *p)
                            p--;
                        p++;
                        parser.path = cstr_new(parserB.path, p - parserB.path);
                        parser.path = cstr_concat(parser.path, parserR.path, parserR.pathp - parserR.path);
                        parser.pathp = parser.path + cstr_length(parser.path);
                        cstr_autorelease(parser.path);
                    }
                }
                parser.query = parserR.query; parser.queryp = parserR.queryp;
            }
            parser.userinfo = parserB.userinfo; parser.userinfop = parserB.userinfop;
            parser.host = parserB.host; parser.hostp = parserB.hostp;
            parser.port = parserB.port; parser.portp = parserB.portp;
        }
        parser.scheme = parserB.scheme; parser.schemep = parserB.schemep;
    }
    parser.fragment = parserR.fragment; parser.fragmentp = parserR.fragmentp;
    return uri_normalize(parser, query_processed);
}
const char *cstr_uri_escape(const char *src, const char *exclude)
{
    const char *res = uri_concat_escape(0, src, exclude, false);
    cstr_autorelease(res);
    return res;
}
const char *cstr_uri_unescape(const char *src)
{
    return uri_unescape(src, strlen(src), false);
}
const char *cstr_uri_query_extract(const char *query, const char **keyp, const char **valp)
{
    if (0 == query)
    {
        *keyp = 0;
        *valp = 0;
        return 0;
    }
    for (const char *p = query;; p++)
        if ('\0' == *p)
        {
            if (p == query)
            {
                *keyp = 0;
                *valp = 0;
                return 0;
            }
            else
            {
                *keyp = uri_unescape(query, p - query, false, true);
                *valp = cstring(0);
                return p;
            }
        }
        else if ('&' == *p)
        {
            *keyp = uri_unescape(query, p - query, false, true);
            *valp = cstring(0);
            return p + 1;
        }
        else if ('=' == *p)
        {
            *keyp = uri_unescape(query, p - query, false, true);
            query = p + 1;
            break;
        }
    for (const char *p = query;; p++)
        if ('\0' == *p)
        {
            *valp = uri_unescape(query, p - query, false, true);
            return p;
        }
        else if ('&' == *p)
        {
            *valp = uri_unescape(query, p - query, false, true);
            return p + 1;
        }
}
const char *cstr_uri_query_concat(const char *query, const char **components, size_t ncomponents)
{
    static const char *sep[2] = { "&", "=" };
    query = cstr_new(query);
    if (0 < ncomponents)
    {
        if (0 < cstr_length(query))
            query = cstr_concat(query, sep[0], 1);
        query = uri_concat_escape(query, components[0], 0, false, true);
        for (size_t i = 1; ncomponents > i; i++)
        {
            query = cstr_concat(query, sep[i % 2], 1);
            query = uri_concat_escape(query, components[i], 0, false, true);
        }
    }
    cstr_autorelease(query);
    return query;
}

}
}
