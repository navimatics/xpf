/*
 * Base/impl/Jvm.cpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#include <Base/impl/Jvm.hpp>
#if defined(BASE_CONFIG_JVM)
#include <Base/impl/Log.hpp>
#include <Base/impl/Synchronization.hpp>
#include <dlfcn.h>
#endif

namespace Base {
namespace impl {

/*
 * utility
 */
#if defined(BASE_CONFIG_JVM)
#define fatal                           JvmFatal
JavaVM *Jvm()
{
    static JavaVM *jvm;
    execute_once
    {
#if defined(BASE_CONFIG_ANDROID)
        const char *libjvm = "libdvm.so";
        void *h = dlopen(libjvm, RTLD_LAZY);
#else
        const char *libjvm = 0;
        void *h = dlopen(libjvm, RTLD_LAZY | RTLD_NOLOAD);
#endif
        if (0 == h)
            LOG("error: dlopen: %s", dlerror());
        else
        {
            jint (*JNI_GetCreatedJavaVMs)(JavaVM **, jsize, jsize *);
            JNI_GetCreatedJavaVMs = (jint (*)(JavaVM **, jsize, jsize *))
                dlsym(h, "JNI_GetCreatedJavaVMs");
            if (0 == JNI_GetCreatedJavaVMs)
                LOG("error: dlsym: %s", dlerror());
            else
            {
                jsize njvm;
                if (0 != JNI_GetCreatedJavaVMs(&jvm, 1, &njvm))
                    LOG("error: JNI_GetCreatedJavaVMs");
            }
            dlclose(h);
        }
    }
    return jvm;
}
JNIEnv *JvmEnvironment()
{
    JNIEnv *env = 0;
    JavaVM *jvm = Jvm();
    if (0 == jvm)
        fatal(0, "null JavaVM");
#if defined(BASE_CONFIG_ANDROID)
    if (0 != jvm->AttachCurrentThreadAsDaemon(&env, 0))
#else
    if (0 != jvm->AttachCurrentThreadAsDaemon((void **)&env, 0))
#endif
        fatal(0, "JavaVM.AttachCurrentThreadAsDaemon");
    return env;
}
void JvmRegisterNativeMethods(const char *clsname, JNINativeMethod *methods, size_t nmethods)
{
    JNIEnv *env = JvmEnvironment();
    jclass jcls = env->FindClass(clsname);
    if (0 == jcls)
        fatal(env, "JNIEnv.FindClass: %s", clsname);
    if (0 != env->RegisterNatives(jcls, methods, nmethods))
        fatal(env, "JNIEnv.RegisterNatives: %s", clsname);
    env->DeleteLocalRef(jcls);
    DEBUGLOG("%s", clsname);
}
void JvmThrowException(JNIEnv *env, const char *clsname, const char *msg)
{
    jclass jcls = env->FindClass(clsname);
    if (0 == jcls)
        fatal(env, "JNIEnv.FindClass: %s", clsname);
    if (0 != env->ThrowNew(jcls, msg))
        fatal(env, "JNIEnv.ThrowNew: %s", clsname);
    env->DeleteLocalRef(jcls);
}
jclass JvmClassGlobalRef(JNIEnv *env, const char *clsname)
{
    jclass jcls = env->FindClass(clsname);
    if (0 == jcls)
        return 0;
    jclass gcls = (jclass)env->NewGlobalRef(jcls);
    env->DeleteLocalRef(jcls);
    return gcls;
}
const char *JvmClassName(JNIEnv *env, jobject jobj, bool javaFormat)
{
    static jmethodID mid;
    execute_once
    {
        jclass jcls = env->FindClass("java/lang/Class");
        if (0 == jcls)
            fatal(env, "JNIEnv.FindClass: %s", "java/lang/Class");
        mid = env->GetMethodID(jcls, "getName", "()Ljava/lang/String;");
        if (0 == mid)
            fatal(env, "JNIEnv.GetMethodID: %s,%s", "java/lang/Class", "getName");
        env->DeleteLocalRef(jcls);
    }
    const char *name = "<unknown class>";
    if (jstring jname = (jstring)env->CallObjectMethod(env->GetObjectClass(jobj), mid))
        if (0 != (name = JvmCString(env, jname)))
        {
            if (!javaFormat)
                for (char *p = (char *)name; *p; p++)
                    if ('.' == *p)
                        *p = '/';
        }
    return name;
}
jstring JvmCallToStringMethod(JNIEnv *env, jobject jobj)
{
    static jmethodID mid;
    execute_once
    {
        jclass jcls = env->FindClass("java/lang/String");
        if (0 == jcls)
            fatal(env, "JNIEnv.FindClass: %s", "java/lang/String");
        mid = env->GetMethodID(jcls, "toString", "()Ljava/lang/String;");
        if (0 == mid)
            fatal(env, "JNIEnv.GetMethodID: %s,%s", "java/lang/String", "toString");
        env->DeleteLocalRef(jcls);
    }
    return (jstring)env->CallObjectMethod(jobj, mid);
}
jstring JvmJavaString(JNIEnv *env, const char *str)
{
    if (0 == str)
        return 0;
    return env->NewStringUTF(str);
}
const char *JvmCString(JNIEnv *env, jstring jstr)
{
    if (0 == jstr)
        return 0;
    const char *str = cstring(0, env->GetStringUTFLength(jstr));
    env->GetStringUTFRegion(jstr, 0, env->GetStringLength(jstr), (char *)str);
    return str;
}
void JvmFatal(JNIEnv *env, const char *format, ...)
{
    if (0 != env)
        if (env->ExceptionCheck())
            env->ExceptionDescribe();
    const char *newfmt = cstringf("JvmFatal: %s", format);
    va_list ap;
    va_start(ap, format);
    Logv(newfmt, ap);
    va_end(ap);
    abort();
}
#else
JavaVM *Jvm()
{
    return 0;
}
JNIEnv *JvmEnvironment()
{
    return 0;
}
void JvmRegisterNativeMethods(const char *clsname, JNINativeMethod *methods, size_t nmethods)
{
}
void JvmThrowException(JNIEnv *env, const char *clsname, const char *msg)
{
}
jclass JvmClassGlobalRef(JNIEnv *env, const char *clsname)
{
    return 0;
}
const char *JvmClassName(JNIEnv *env, jobject jobj, bool javaFormat)
{
    return 0;
}
jstring JvmCallToStringMethod(JNIEnv *env, jobject jobj)
{
    return 0;
}
jstring JvmJavaString(JNIEnv *env, const char *str)
{
    return 0;
}
const char *JvmCString(JNIEnv *env, jstring jstr)
{
    return 0;
}
void JvmFatal(JNIEnv *env, const char *format, ...)
{
}
#endif

}
}
