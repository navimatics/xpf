/*
 * Base/impl/IndexMap.cpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#include <Base/impl/IndexMap.hpp>
#include <Base/impl/CArray.hpp>
#include <Base/impl/CString.hpp>
#include <Base/impl/Coroutine.hpp>

namespace Base {
namespace impl {

IndexMap::IndexMap(size_t capacity) : RadixTree(capacity)
{
}
IndexMap::~IndexMap()
{
    removeAllObjects();
}
void IndexMap::autocollectObjects(bool ac)
{
    _nac = !ac;
}
size_t IndexMap::count()
{
    return _count;
}
Object *IndexMap::object(index_t index)
{
    Node *node = lookup(index);
    if (node != nullnode() && node->index == index)
    {
        Object *obj = (Object *)node->value;
        if (0 == obj)
            return 0;
        return _nac ? obj : obj->autocollect();
    }
    else
        return 0;
}
bool IndexMap::getObject(index_t index, Object *&obj)
{
    Node *node = lookup(index);
    if (node != nullnode() && node->index == index)
    {
        obj = (Object *)node->value;
        if (0 == obj)
            return true;
        _nac ? obj : obj->autocollect();
        return true;
    }
    else
    {
        obj = 0;
        return false;
    }
}
void IndexMap::setObject(index_t index, Object *obj)
{
    Node *node = lookup(index);
    if (node != nullnode() && node->index == index)
    {
        if (0 != obj)
            obj->retain();
        Object *oldobj = (Object *)node->value;
        if (0 != oldobj)
            oldobj->release();
        node->value = (index_t)obj;
    }
    else
    {
        if (0 != obj)
            obj->retain();
        insert(node, index, (index_t)obj);
        _count++;
    }
}
void IndexMap::removeObject(index_t index)
{
    Node *node = lookup(index);
    if (node != nullnode() && node->index == index)
    {
        Object *oldobj = (Object *)node->value;
        if (0 != oldobj)
            oldobj->release();
        remove(node);
        _count--;
    }
}
void IndexMap::removeAllObjects()
{
    Node *p = nullnode(), *endp = p + carr_length(p);
    for (p++; endp > p; p++)
        if (0 <= p->bitpos)
        {
            Object *oldobj = (Object *)p->value;
            if (0 != oldobj)
                oldobj->release();
        }
    _count = 0;
    reset();
}
void IndexMap::addObjects(IndexMap *map)
{
    Node *p = map->nullnode(), *endp = p + carr_length(p);
    for (p++; endp > p; p++)
        if (0 <= p->bitpos)
        {
            Object *obj = (Object *)p->value;
            setObject(p->index, obj);
        }
}
index_t *IndexMap::indexes()
{
    index_t *result = (index_t *)carrayWithCapacity(_count, sizeof(index_t));
    carr_obj(result)->setLength(_count);
    size_t i = 0;
    Iterable<index_t> *iter = new (collect) NodeIterator(this);
    foreach (index_t inode, iter)
    {
        Node *node = (Node *)inode;
        result[i++] = node->index;
    }
    return result;
}
Object **IndexMap::objects()
{
    Object **result = (Object **)carrayWithCapacity(_count, sizeof(Object *));
    carr_obj(result)->setLength(_count);
    size_t i = 0;
    Iterable<index_t> *iter = new (collect) NodeIterator(this);
    foreach (index_t inode, iter)
    {
        Node *node = (Node *)inode;
        result[i++] = (Object *)node->value;
    }
    return result;
}
void IndexMap::getIndexesAndObjects(index_t *&idxbuf, Object **&objbuf)
{
    idxbuf = (index_t *)carrayWithCapacity(_count, sizeof(index_t));
    objbuf = (Object **)carrayWithCapacity(_count, sizeof(Object *));
    carr_obj(idxbuf)->setLength(_count);
    carr_obj(objbuf)->setLength(_count);
    size_t i = 0;
    Iterable<index_t> *iter = new (collect) NodeIterator(this);
    foreach (index_t inode, iter)
    {
        Node *node = (Node *)inode;
        idxbuf[i] = node->index;
        objbuf[i] = (Object *)node->value;
        i++;
    }
}
const char *IndexMap::strrepr()
{
    return cstringf("<%s %p; count=%u>", className(), this, count());
}
/* Iterable<index_t> */
void *IndexMap::iterate(void *state, index_t *bufp[2])
{
    if (0 == state)
        state = new (collect) NodeIterator(this);
    state = ((NodeIterator *)state)->iterate(state, bufp);
    index_t *curp = bufp[0], *endp = bufp[1];
    while (curp < endp)
    {
        Node *node = (Node *)*curp;
        *curp = node->index;
        curp++;
    }
    return state;
}
/* ObjectCodec use */
Map *IndexMap::__map()
{
    Map *map = new (collect) Map;
    Iterable<index_t> *iter = new (collect) NodeIterator(this);
    foreach (index_t inode, iter)
    {
        Node *node = (Node *)inode;
        CString *key = CString::createf("%llx", (unsigned long long)node->index);
        map->setObject(key, (Object *)node->value);
        key->release();
    }
    return map;
}
void IndexMap::__setMap(Map *map)
{
    foreach (CString *key, map)
    {
        index_t index = strtoull(key->chars(), 0, 16);
        Object *obj = map->object(key);
        setObject(index, obj);
    }
}
ObjectCodecBegin(IndexMap)
    ObjectCodecProperty("_", &IndexMap::__map, &IndexMap::__setMap)
ObjectCodecEnd(IndexMap)

}
}
