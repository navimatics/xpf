/*
 * Base/impl/CArray.cpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#include <Base/impl/CArray.hpp>

namespace Base {
namespace impl {

CArray *CArray::empty()
{
    static CArray *arr = (CArray *)(new CArray)->linger();
        /* threadsafe: __forcecall_CArray_empty */
    return arr;
}
CArray *CArray::create(const void *p, size_t length, size_t elemsize)
{
    size_t size = length * elemsize;
    if (0 == size)
        return empty();
    CArray *self = new (size) CArray;
    self->_length = length;
    self->_size = size;
    void *elems = (char *)self + sizeof(CArray);
    if (0 != p)
        memcpy(elems, p, size);
    else
        memset(elems, 0, size);
    return self;
}
CArray *CArray::createWithCapacity(size_t capacity, size_t elemsize)
{
    size_t size = capacity * elemsize;
    if (0 == size)
        return empty();
    CArray *self = new (size) CArray;
    //self->_length = 0;
    self->_size = size;
    return self;
}
void CArray::apply(void (*fn)(void *))
{
    for (void **p = (void **)elems(), **endp = p + _length; endp > p; p++)
    {
        void *obj = *p;
        if (0 != obj)
            (*fn)(obj);
    }
}
void CArray::apply(void (Object::*fn)())
{
    for (Object **p = (Object **)elems(), **endp = p + _length; endp > p; p++)
    {
        Object *obj = *p;
        if (0 != obj)
            (obj->*fn)();
    }
}
CArray *CArray::_concat(const void *p, size_t length, size_t elemsize)
{
    if (0 == length)
        return this;
    CArray *self = this;
    char *t = (char *)this + sizeof(CArray);
    size_t curlen = _length;
    size_t newlen = curlen + length;
    if (newlen * elemsize > _size)
    {
        size_t capacity = newlen * 3 / 2;
        if (capacity < 4)
            capacity = 4;
        size_t size = capacity * elemsize;
        self = new (size) CArray;
        self->_size = size;
        char *p = (char *)self + sizeof(CArray);
        memcpy(p, t, curlen * elemsize);
        t = p;
        release();
    }
    if (0 != p)
        memcpy(t + curlen * elemsize, p, length * elemsize);
    else
        memset(t + curlen * elemsize, 0, length * elemsize);
    self->_length = newlen;
    return self;
}
static CArray *__forcecall_CArray_empty = CArray::empty();

void *carray(const void *p, size_t length, size_t elemsize)
{
    CArray *self = CArray::create(p, length, elemsize);
    self->autorelease();
    return (char *)self + sizeof(CArray);
}
void *carrayWithCapacity(size_t capacity, size_t elemsize)
{
    CArray *self = CArray::createWithCapacity(capacity, elemsize);
    self->autorelease();
    return (char *)self + sizeof(CArray);
}
void *carr_new(const void *p, size_t length, size_t elemsize)
{
    CArray *self = CArray::create(p, length, elemsize);
    return (char *)self + sizeof(CArray);
}
void *carr_newWithCapacity(size_t capacity, size_t elemsize)
{
    CArray *self = CArray::createWithCapacity(capacity, elemsize);
    return (char *)self + sizeof(CArray);
}
void *carr_concat(void *p, const void *q, size_t length, size_t elemsize)
{
    if (0 == p)
        return carr_new(q, length, elemsize);
    else
        return ((CArray *)((char *)p - sizeof(CArray)))->_concat(q, length, elemsize)->elems();
}
uintptr_t __carr_inlines_hack()
{
    /* ensure inclusion of the following inline symbols in library */
    return (uintptr_t)&carr_retain + (uintptr_t)&carr_release + (uintptr_t)&carr_autorelease +
        (uintptr_t)&carr_autocollect;
}

void carr_pointer_remove(void **arrp, void *q)
{
    if (0 == arrp)
        return;
    CArray *arr = (CArray *)((char *)arrp - sizeof(CArray));
    size_t len = arr->length();
    for (void **p = arrp, **endp = p + len; endp > p; p++)
    {
        if (q == *p)
        {
            memmove(p, p + 1, (len - (p - arrp) - 1) * sizeof(void *));
            arr->setLength(len - 1);
            break;
        }
    }
}

}
}
