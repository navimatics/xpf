/*
 * Base/impl/CStringPath.cpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#include <Base/impl/CString.hpp>

namespace Base {
namespace impl {

#if defined(BASE_CONFIG_WINDOWS)
#define PATHCOMPSEP                     '\\'
#elif defined(BASE_CONFIG_POSIX)
#define PATHCOMPSEP                     '/'
#else
#error CStringPath.cpp not implemented for this platform
#endif

#if defined(BASE_CONFIG_WINDOWS)
#define CASE_PATHCOMPSEP                case '/': case PATHCOMPSEP
#else
#define CASE_PATHCOMPSEP                case PATHCOMPSEP
#endif

#if defined(BASE_CONFIG_WINDOWS) || defined(BASE_CONFIG_POSIX)
static inline bool ispathcompsep(char c)
{
#if defined(BASE_CONFIG_WINDOWS)
    return PATHCOMPSEP == c || '/' == c;
#else
    return PATHCOMPSEP == c;
#endif
}
static inline bool isabspath(const char *src)
{
#if defined(BASE_CONFIG_WINDOWS)
    return ispathcompsep(src[0]) || ('\0' != src[0] && ':' == src[1]/* && ispathcompsep(src[2])*/);
#else
    return ispathcompsep(src[0]);
#endif
}
static inline const char *pathbegin(const char *src)
{
    const char *srcp = src;
    if (ispathcompsep(srcp[0]) && ispathcompsep(srcp[1]) && !ispathcompsep(srcp[2]))
    {
        srcp += 2;
        while ('\0' != srcp[0] && !ispathcompsep(srcp[0]))
            srcp++;
    }
#if defined(BASE_CONFIG_WINDOWS)
    else if ('\0' != srcp[0] && ':' == srcp[1])
        srcp += 2;
#endif
    return srcp;
}
static char *dstparent(char *dst, char *dstp)
{
    if (dst == dstp)
        goto dotdot;
    if (PATHCOMPSEP == dstp[-1])
    {
        if (dst + 1 == dstp)
            return dstp;
        dstp -= 2;
    }
    while (dst <= dstp && PATHCOMPSEP != *dstp)
        dstp--;
    dstp++;
    if ('.' == dstp[0] && '.' == dstp[1] && PATHCOMPSEP == dstp[2])
    {
        dstp += 3;
        goto dotdot;
    }
    return dstp;
dotdot:
    *dstp++ = '.';
    *dstp++ = '.';
    *dstp++ = PATHCOMPSEP;
    return dstp;
}
static char *normalize(const char *src, char *dst, bool resolveParent)
{
    const char *srcb = pathbegin(src);
    if (src < srcb)
    {
        for (const char *p = src; srcb > p; p++)
            switch (char c = *p)
            {
            CASE_PATHCOMPSEP:
                *dst++ = PATHCOMPSEP;
                break;
            default:
                *dst++ = c;
                break;
            }
    }
    const char *srcp = srcb;
    char *dstp = dst;
    for (;;)
        switch (char c = *srcp++)
        {
        case '\0':
            return dstp;
        CASE_PATHCOMPSEP:
            if (dst == dstp || !ispathcompsep(dstp[-1]))
                *dstp++ = PATHCOMPSEP;
            break;
        case '.':
            if (srcb + 1 == srcp || ispathcompsep(srcp[-2]))
            {
                if (ispathcompsep(srcp[0]))
                {
                    srcp++;
                    break;
                }
                else if ('\0' == srcp[0])
                    break;
                else if (resolveParent && '.' == srcp[0])
                {
                    if (ispathcompsep(srcp[1]))
                    {
                        srcp += 2;
                        dstp = dstparent(dst, dstp);
                        break;
                    }
                    else if ('\0' == srcp[1])
                    {
                        srcp++;
                        dstp = dstparent(dst, dstp);
                        break;
                    }
                }
            }
            /* fall through */
        default:
            *dstp++ = c;
            break;
        }
}

const char *cstr_path_realpath(const char *src)
{
#if defined(BASE_CONFIG_WINDOWS)
    char buf[_MAX_PATH];
    if (0 == _fullpath(buf, src, sizeof buf))
        return 0;
    return cstring(buf);
#elif defined(BASE_CONFIG_POSIX)
    char buf[PATH_MAX];
    if (0 == realpath(src, buf))
        return 0;
    return cstring(buf);
#endif
}
const char *cstr_path_normalize(const char *src, bool resolveParent)
{
    size_t len = strlen(src);
    char *dst = (char *)cstring(0, len + 2);
    char *dstp = normalize(src, dst, resolveParent);
    if (dst == dstp)
    {
        *dstp++ = '.';
        *dstp++ = PATHCOMPSEP;
    }
    assert(dst < dstp && cstr_length(dst) >= (size_t)(dstp - dst));
    *dstp = '\0'; cstr_obj(dst)->setLength(dstp - dst);
    return dst;
}
const char *cstr_path_concat(const char *src1, const char *src2, bool resolveParent)
{
    if (isabspath(src2))
        return cstr_path_normalize(src2, resolveParent);
    size_t len1 = strlen(src1);
    size_t len2 = strlen(src2);
    size_t len = len1 + 1 + len2;
    char *dst = (char *)cstring(0, len + 2);
    char *dstp = dst;
    if (0 < len1)
    {
        memcpy(dstp, src1, len1);
        dstp += len1;
        if (!ispathcompsep(dstp[-1]))
            *dstp++ = PATHCOMPSEP;
    }
    memcpy(dstp, src2, len2);
    dstp = normalize(dst, dst, resolveParent);
    if (dst == dstp)
    {
        *dstp++ = '.';
        *dstp++ = PATHCOMPSEP;
    }
    assert(dst < dstp && cstr_length(dst) >= (size_t)(dstp - dst));
    *dstp = '\0'; cstr_obj(dst)->setLength(dstp - dst);
    return dst;
}
const char *cstr_path_basename(const char *src)
{
    const char *srcb = pathbegin(src);
    size_t len = strlen(srcb);
    const char *srcp = srcb + len;
    while (srcb <= srcp && !ispathcompsep(*srcp))
        srcp--;
    srcp++;
    return cstring(srcp);
}
const char *cstr_path_extension(const char *src)
{
    const char *srcb = pathbegin(src);
    size_t len = strlen(srcb);
    const char *srcp = srcb + len;
    while (srcb <= srcp && !ispathcompsep(*srcp))
        srcp--;
    srcp++;
    if ('.' == *srcp)
        srcp++; /* ignore leading dots */
    while ('\0' != *srcp && '.' != *srcp)
        srcp++;
    return cstring(srcp);
}
const char *cstr_path_setBasename(const char *src1, const char *src2)
{
    const char *srcb = pathbegin(src1);
    size_t len1 = strlen(srcb);
    size_t len2 = strlen(src2);
    const char *srcp = srcb + len1;
    while (srcb <= srcp && !ispathcompsep(*srcp))
        srcp--;
    srcp++;
    len1 = srcp - src1;
    size_t len = len1 + 1 + len2;
    char *dst = (char *)cstring(0, len);
    char *dstp = dst;
    memcpy(dstp, src1, len1);
    dstp += len1;
    if (dst < dstp && !ispathcompsep(dstp[-1]))
        *dstp++ = PATHCOMPSEP;
    memcpy(dstp, src2, len2);
    return dst;
}
const char *cstr_path_setExtension(const char *src1, const char *src2)
{
    const char *srcb = pathbegin(src1);
    size_t len1 = strlen(srcb);
    size_t len2 = strlen(src2);
    const char *srcp = srcb + len1;
    while (srcb <= srcp && !ispathcompsep(*srcp))
        srcp--;
    srcp++;
    if ('.' == *srcp)
        srcp++; /* ignore leading dots */
    while ('\0' != *srcp && '.' != *srcp)
        srcp++;
    len1 = srcp - src1;
    size_t len = len1 + len2;
    char *dst = (char *)cstring(0, len);
    char *dstp = dst;
    memcpy(dstp, src1, len1);
    dstp += len1;
    if (dst < dstp && !ispathcompsep(dstp[-1]))
        if (srcb - src1 != dstp - dst)
            memcpy(dstp, src2, len2);
    return dst;
}
#endif

}
}
