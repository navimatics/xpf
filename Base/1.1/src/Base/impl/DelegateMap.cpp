/*
 * Base/impl/DelegateMap.cpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#include <Base/impl/DelegateMap.hpp>
#include <Base/impl/CArray.hpp>
#include <Base/impl/CString.hpp>

namespace Base {
namespace impl {

DelegateMap::DelegateMap(ObjectComparer *comparer) :
    _comparerRef(0 == comparer ? 0 : comparer->self()),
    _map(Object_hash_t(comparer), Object_equal_t(comparer))
{
}
size_t DelegateMap::count()
{
    return _map.size();
}
DelegateData DelegateMap::delegate(Object *key)
{
    map_t::iterator iter = _map.find(Ref<Object>(key));
    if (iter != _map.end())
        return iter->second;
    else
    {
        DelegateData errdel = { 0, 0 };
        return errdel;
    }
}
bool DelegateMap::getDelegate(Object *key, DelegateData &del)
{
    map_t::iterator iter = _map.find(Ref<Object>(key));
    if (iter != _map.end())
    {
        del = iter->second;
        return true;
    }
    else
    {
        DelegateData errdel = { 0, 0 };
        del = errdel;
        return false;
    }
}
void DelegateMap::setDelegate(Object *key, DelegateData del)
{
    _map[Ref<Object>(key)] = del;
}
void DelegateMap::removeDelegate(Object *key)
{
    _map.erase(Ref<Object>(key));
}
void DelegateMap::removeAllDelegates()
{
    _map.clear();
}
void DelegateMap::addDelegates(DelegateMap *map)
{
    for (map_t::iterator p = map->_map.begin(), q = map->_map.end(); p != q; ++p)
        _map[p->first] = p->second;
}
Object **DelegateMap::keys()
{
    Object **keybuf = (Object **)carrayWithCapacity(_map.size(), sizeof(Object *));
    carr_obj(keybuf)->setLength(_map.size());
    Object **keys = keybuf;
    for (map_t::iterator p = _map.begin(), q = _map.end(); p != q; ++p, ++keys)
        *keys = p->first;
    return keybuf;
}
DelegateData *DelegateMap::delegates()
{
    DelegateData *delbuf = (DelegateData *)carrayWithCapacity(_map.size(), sizeof(DelegateData));
    carr_obj(delbuf)->setLength(_map.size());
    DelegateData *dels = delbuf;
    for (map_t::iterator p = _map.begin(), q = _map.end(); p != q; ++p, ++dels)
        *dels = p->second;
    return delbuf;
}
void DelegateMap::getKeysAndDelegates(Object **&keybuf, DelegateData *&delbuf)
{
    keybuf = (Object **)carrayWithCapacity(_map.size(), sizeof(Object *));
    delbuf = (DelegateData *)carrayWithCapacity(_map.size(), sizeof(DelegateData));
    carr_obj(keybuf)->setLength(_map.size());
    carr_obj(delbuf)->setLength(_map.size());
    Object **keys = keybuf;
    DelegateData *dels = delbuf;
    for (map_t::iterator p = _map.begin(), q = _map.end(); p != q; ++p, ++keys, ++dels)
    {
        *keys = p->first;
        *dels = p->second;
    }
}
const char *DelegateMap::strrepr()
{
    return cstringf("<%s %p; count=%u>", className(), this, count());
}
/* Iterable<Object *> */
void *DelegateMap::iterate(void *state, Object **bufp[2])
{
    if (0 == state)
        state = new (collect) Iterator(_map);
    Iterator *iter = (Iterator *)state;
    Object **curp = bufp[0], **endp = bufp[1];
    while (curp < endp && iter->p != iter->q)
    {
        *curp++ = iter->p->first;
        ++iter->p;
    }
    bufp[1] = curp;
    return state;
}

}
}
