/*
 * Base/impl/Stream.cpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#include <Base/impl/Stream.hpp>

namespace Base {
namespace impl {

ssize_t Stream::read(void *buf, size_t size)
{
    return -1;
}
ssize_t Stream::write(const void *buf, size_t size)
{
    return -1;
}
ssize_t Stream::seek(ssize_t offset, int whence)
{
    return -1;
}
ssize_t Stream::close()
{
    return 0;
}

}
}
