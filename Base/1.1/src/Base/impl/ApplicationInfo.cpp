/*
 * Base/impl/ApplicationInfo.cpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#include <Base/impl/ApplicationInfo.hpp>
#include <Base/impl/CString.hpp>
#include <Base/impl/Synchronization.hpp>
#if defined(BASE_CONFIG_WINDOWS)
#include <shlobj.h>
#include <windows.h>
#elif defined(BASE_CONFIG_DARWIN) || defined(BASE_CONFIG_LINUX)
#include <unistd.h>
#elif defined(BASE_CONFIG_ANDROID)
#include <fcntl.h>
#include <unistd.h>
#else
#error ApplicationInfo.cpp not implemented for this platform
#endif

namespace Base {
namespace impl {

#if defined(BASE_CONFIG_WINDOWS)
const char *_GetApplicationName()
{
    const char *chars = 0;
    char buf[MAX_PATH];
    DWORD len = GetModuleFileNameA(0, buf, MAX_PATH);
    if (0 < len && len < MAX_PATH)
    {
        char *p = strrchr(buf, '\\');
        if (0 != p)
            p++;
        else
            p = buf;
        char *q = strrchr(buf, '.');
        if (0 != q)
            if (('e' == q[1] || 'E' == q[1]) &&
                ('x' == q[2] || 'X' == q[2]) &&
                ('e' == q[3] || 'E' == q[3]) &&
                '\0' == q[4])
                *q = '\0';
        chars = cstr_new(p);
    }
    return chars;
}
const char *_GetApplicationResourceDirectory()
{
    const char *chars = 0;
    char buf[MAX_PATH];
    DWORD len = GetModuleFileNameA(0, buf, MAX_PATH);
    if (0 < len && len < MAX_PATH)
    {
        char *p = strrchr(buf, '\\');
        if (0 != p)
            *++p = '\0';
        else
        {
            /* should not happen */
            buf[0] = '.';
            buf[1] = '\\';
            buf[2] = '\0';
        }
        chars = cstr_new(buf);
    }
    return chars;
}
const char *_GetApplicationDataDirectory()
{
    const char *chars = 0;
    char buf[MAX_PATH + MAX_PATH];
    if (S_OK == SHGetFolderPathA(0, CSIDL_APPDATA, 0, SHGFP_TYPE_CURRENT, buf))
        chars = cstr_newf("%s\\%s\\", buf, GetApplicationName());
    return chars;
}
const char *_GetDocumentDirectory()
{
    const char *chars = 0;
    char buf[MAX_PATH];
    if (S_OK == SHGetFolderPathA(0, CSIDL_PERSONAL, 0, SHGFP_TYPE_CURRENT, buf))
        chars = cstr_newf("%s\\", buf);
    return chars;
}
const char *_GetTemporaryDirectory()
{
    const char *chars = 0;
    char buf[MAX_PATH];
    DWORD len = GetTempPathA(MAX_PATH, buf);
    if (0 < len && len < MAX_PATH)
        /* GetTempPath() returns string that ends with a backslash */
        chars = cstr_new(buf);
    return chars;
}
#elif defined(BASE_CONFIG_DARWIN)
/* implemented in Base/impl/ApplicationInfo.mm */
const char *_GetApplicationName();
const char *_GetApplicationResourceDirectory();
const char *_GetApplicationDataDirectory();
const char *_GetDocumentDirectory();
const char *_GetTemporaryDirectory();
#elif defined(BASE_CONFIG_LINUX)
const char *_GetApplicationName()
{
    const char *chars = 0;
    char buf[PATH_MAX + 1];
    ssize_t len = readlink(0, buf, PATH_MAX);
    if (0 < len && len <= PATH_MAX)
    {
        char *p = strrchr(buf, '/');
        if (0 != p)
            p++;
        else
            p = buf;
        chars = cstr_new(p);
    }
    return chars;
}
const char *_GetApplicationResourceDirectory()
{
    const char *chars = 0;
    char buf[PATH_MAX + 1];
    ssize_t len = readlink(0, buf, PATH_MAX);
    if (0 < len && len <= PATH_MAX)
    {
        char *p = strrchr(buf, '/');
        if (0 != p)
            *++p = '\0';
        else
        {
            /* should not happen */
            buf[0] = '.';
            buf[1] = '/';
            buf[2] = '\0';
        }
        chars = cstr_new(buf);
    }
    return chars;
}
const char *_GetApplicationDataDirectory()
{
    const char *chars = 0;
    const char *path = GetDocumentDirectory();
    const char *name = GetApplicationName();
    if (0 != path && 0 != name)
        chars = cstr_newf("%s.%s/", path, name);
    return chars;
}
const char *_GetDocumentDirectory()
{
    const char *chars = 0;
    const char *path = getenv("HOME");
    if (0 != path)
    {
        ssize_t len = strlen(path);
        if ('/' == path[len - 1])
            chars = cstr_new(path, len);
        else
            chars = cstr_newf("%s/", path);
    }
    else
        mark_and_collect
            chars = cstr_new(GetWorkingDirectory());
    return chars;
}
const char *_GetTemporaryDirectory()
{
    const char *chars = 0;
    const char *path = getenv("TMPDIR");
    if (0 == path || '\0' == path[0])
    {
        path = P_tmpdir;
        if (0 == path)
            path = "/tmp/";
    }
    ssize_t len = strlen(path);
    if ('/' == path[len - 1])
        chars = cstr_new(path, len);
    else
        chars = cstr_newf("%s/", path);
    return chars;
}
#elif defined(BASE_CONFIG_ANDROID)
const char *_GetApplicationName()
{
    const char *chars = 0;
    int fd = open("/proc/self/cmdline", O_RDONLY);
    if (-1 != fd)
    {
        char buf[PATH_MAX + 1];
        if (0 < read(fd, buf, sizeof buf))
            chars = cstr_new(buf);
        close(fd);
    }
    return chars;
}
const char *_GetApplicationResourceDirectory()
{
    return 0;
}
const char *_GetApplicationDataDirectory()
{
    const char *chars = 0;
    const char *name = GetApplicationName();
    if (0 != name)
        chars = cstr_newf("/data/data/%s/shared_prefs/", name);
    return chars;
}
const char *_GetDocumentDirectory()
{
    const char *chars = 0;
    const char *name = GetApplicationName();
    if (0 != name)
        chars = cstr_newf("/data/data/%s/files/", name);
    return chars;
}
const char *_GetTemporaryDirectory()
{
    const char *chars = 0;
    const char *name = GetApplicationName();
    if (0 != name)
        chars = cstr_newf("/data/data/%s/cache/", name);
    return chars;
}
#endif

const char *GetApplicationName()
{
    static const char *result;
    execute_once
        result = _GetApplicationName();
    return result;
}
const char *GetApplicationResourceDirectory()
{
    static const char *result;
    execute_once
        result = _GetApplicationResourceDirectory();
    return result;
}
const char *GetApplicationDataDirectory()
{
    static const char *result;
    execute_once
        result = _GetApplicationDataDirectory();
    return result;
}
const char *GetDocumentDirectory()
{
    static const char *result;
    execute_once
        result = _GetDocumentDirectory();
    return result;
}
const char *GetTemporaryDirectory()
{
    static const char *result;
    execute_once
        result = _GetTemporaryDirectory();
    return result;
}

#if defined(BASE_CONFIG_WINDOWS)
const char *GetWorkingDirectory()
{
    const char *chars = 0;
    char buf[MAX_PATH];
    DWORD len = GetCurrentDirectoryA(MAX_PATH - 1, buf);
    if (0 < len && len < MAX_PATH - 1)
    {
        if ('\\' != buf[len - 1])
        {
            buf[len++] = '\\';
            buf[len] = '\0';
        }
        chars = cstring(buf, len);
    }
    return chars;
}
#elif defined(BASE_CONFIG_POSIX)
const char *GetWorkingDirectory()
{
    const char *chars = 0;
    char buf[PATH_MAX + 1];
    char *path = getcwd(buf, PATH_MAX);
    if (0 != path && '\0' != path[0])
    {
        ssize_t len = strlen(path);
        if ('/' != path[len - 1])
        {
            path[len++] = '/';
            path[len] = '\0';
        }
        chars = cstring(path, len);
    }
    return chars;
}
#endif

}
}
