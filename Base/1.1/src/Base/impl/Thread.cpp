/*
 * Base/impl/Thread.cpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#include <Base/impl/Thread.hpp>
#include <Base/impl/Atomic.hpp>
#include <Base/impl/Log.hpp>
#include <Base/impl/Tls.hpp>
#if defined(BASE_CONFIG_POSIX)
#include <signal.h>
#endif

namespace Base {
namespace impl {

static void _ThreadTlsFree(void *p)
{
    Thread *thread = (Thread *)p;
    thread->release();
}
static tlskey_t _ThreadTlsKey() BASE_FUNCCONST;
static tlskey_t _ThreadTlsKey()
{
    static tlskey_t tlskey = tlsalloc(_ThreadTlsFree);
        /* threadsafe: __forcecall_ThreadTlsKey */
    return tlskey;
}
static tlskey_t __forcecall_ThreadTlsKey = _ThreadTlsKey();
Thread *Thread::currentThread()
{
    tlskey_t tlskey = _ThreadTlsKey();
    Thread *thread = (Thread *)tlsget(tlskey);
    if (0 == thread)
    {
        thread = new Thread;
        thread->_thr = _thrcurrent();
        /*
         * The Windows version of _thrcurrent() returns a new thr_t that needs to be detached.
         * The POSIX version of _thrcurrent() returns the current thr_t that should not be detached.
         * Hence the conditional below.
         */
#if defined(BASE_CONFIG_WINDOWS)
        thread->_detached = false;
#elif defined(BASE_CONFIG_POSIX)
        thread->_detached = true;
#endif
        tlsset(tlskey, thread);
    }
    return thread;
}
Map *Thread::currentThreadMap()
{
    return currentThread()->_map;
}
void Thread::exit()
{
    threxit();
}
void Thread::sleep(wait_timeout_t timeout)
{
    thrsleep(timeout);
}
Thread::Thread()
{
    threadsafe();
    _map = new Map;
}
Thread::Thread(void (*entry)(void *), void *arg)
{
    threadsafe();
    _map = new Map;
    _entry = entry;
    _arg = arg;
}
Thread::~Thread()
{
    if (!_detached)
        if (0 != _thr)
            thrdetach(_thr);
    _map->release();
    for (size_t i = 0; _SlotObjectCount > i; i++)
        if (Object *obj = (Object *)_slots[i])
            obj->release();
}
void Thread::start()
{
    if (0 != _thr)
        return;
    retain();
    if (!thrcreate(_thr, Thread::entry, this))
        release();
}
void Thread::detach()
{
    if (!_detached)
    {
        _detached = true;
        if (0 != _thr)
            thrdetach(_thr);
    }
}
void Thread::wait()
{
    if (!_detached)
    {
        _detached = true;
        if (0 != _thr)
            thrjoin(_thr);
    }
}
void Thread::quit()
{
    _quitting = true;
    atomic_wmb();
}
bool Thread::isQuitting()
{
    bool quitting = _quitting;
    atomic_rmb();
    return quitting;
}
void Thread::run()
{
    _entry(_arg);
}
BASE_IMPL_THRENTRY(Thread::entry, arg)
{
    bool success = true;
    Thread *self = (Thread *)arg;
    tlskey_t tlskey = _ThreadTlsKey();
    tlsset(tlskey, self);
#if defined(BASE_CONFIG_POSIX)
    /* !!!:
     * Under POSIX there is no guarantee that self->_thr will have been assigned
     * before this function has started executing.
     *
     * We therefore assign the current thread handle both in the parent thread and here.
     * Because we write the same value in both cases this is ok.
     */
    self->_thr = _thrcurrent();
#endif
    try
    {
        self->run();
    }
    catch (const std::exception &ex)
    {
        LOG("unexpected exception: %s", ex.what());
        success = false;
    }
    catch (...)
    {
        LOG("unexpected exception");
        success = false;
    }
    if (success)
        Thread::exit();
    abort();
    return 0;
}
#if defined(BASE_CONFIG_WINDOWS)
static VOID CALLBACK _interrupt_apcproc(ULONG_PTR param)
{
}
void Thread::_interrupt(thr_t thr)
{
    QueueUserAPC(_interrupt_apcproc, thr, 0);
}
void Thread::_interrupt()
{
    QueueUserAPC(_interrupt_apcproc, _thr, 0);
}
#elif defined(BASE_CONFIG_POSIX)
static void _interrupt_sighandler(int signo)
{
    /* !!!:
     * This signal handler uses pthread_getspecific() to determine the current thread.
     * Pthread_getspecific() is not guaranteed by the POSIX standard to be async-signal-safe.
     */
    if (Thread *thread = (Thread *)pthread_getspecific(_ThreadTlsKey()))
        if (sigjmp_buf *jmpbufp = (sigjmp_buf *)thread->_slot(Thread::_SlotJmpbuf))
            siglongjmp(*jmpbufp, 1);
}
static void _interrupt_init()
{
    struct sigaction sa;
    sa.sa_handler = _interrupt_sighandler;
    sa.sa_flags = SA_RESTART;
    sigfillset(&sa.sa_mask);
    sigaction(SIGUSR2, &sa, 0);
    sigemptyset(&sa.sa_mask);
    sigaddset(&sa.sa_mask, SIGUSR2);
    pthread_sigmask(SIG_UNBLOCK, &sa.sa_mask, 0);
        /* SIGUSR2 unblocked on main thread and all threads created from it */
}
static bool __forcecall_interrupt_init = (_interrupt_init(), false);
void Thread::_interrupt(thr_t thr)
{
    pthread_kill(thr, SIGUSR2);
}
void Thread::_interrupt()
{
    pthread_kill(_thr, SIGUSR2);
}
#endif

}
}
