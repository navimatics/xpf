/*
 * Base/impl/ObjectWriter.cpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#include <Base/impl/ObjectWriter.hpp>
#include <Base/impl/CArray.hpp>
#include <Base/impl/CString.hpp>
#include <Base/impl/ObjectCodec.hpp>
#include <Base/impl/Value.hpp>

namespace Base {
namespace impl {

ObjectWriter::ObjectWriter(Stream *stream, bool pretty) : JsonWriter(stream, pretty)
{
    _idmap = new Map;
    _idmap->autocollectObjects(false);
    setFlags(ObjectCodecExtensions, 1);
}
ObjectWriter::~ObjectWriter()
{
    _idmap->release();
}
bool ObjectWriter::writeObject(Object *obj)
{
    mark_and_collect
        return writeObjectInternal(obj);
}
bool ObjectWriter::writeObjectInternal(Object *obj)
{
    ObjectCodec *codec = 0;
    if (flags(_ObjectCodecExtension))
    {
        if (0 != obj)
            codec = ObjectCodec::getObjectCodec(obj);
    }
    if (0 != codec)
    {
        CString *id = (CString *)_idmap->object(obj);
        if (0 != id)
            return _writeSpecialText(id->chars(), id->length());
        else
        {
            id = CString::createf("\\//i%i", _idmap->count());
            _idmap->setObject(obj, id);
            id->release();
            writeStartObject();
            _writeSpecialText("\\//t", 4);
            {
                const char *str = codec->name();
                writeText(str, cstr_length(str));
            }
            _writeSpecialText("\\//i", 4);
            _writeSpecialText(id->chars(), id->length());
            ObjectCodec::PropertyDescr *props = codec->getPropertyDescrs();
            for (size_t i = 0, n = carr_length(props); n > i; i++)
            {
                ObjectCodec::PropertyDescr *prop = props + i;
                switch (prop->kind)
                {
                case '@':
                    {
                        Object *objval = ObjectCodec::getObjectProperty(obj, prop);
                        if (objval != prop->deflt.obj)
                        {
                            if (0 == objval || 0 != objval->cmp(prop->deflt.obj))
                            {
                                writeText(prop->name, cstr_length(prop->name));
                                writeObjectInternal(objval);
                            }
                        }
                    }
                    break;
                case '*':
                    {
                        const char *strval = ObjectCodec::getStringProperty(obj, prop);
                        if (strval != prop->deflt.str)
                        {
                            if (0 != strval)
                            {
                                if (0 == prop->deflt.str ||
                                    0 != memcmp(strval, prop->deflt.str, cstr_length(strval)))
                                {
                                    writeText(prop->name, cstr_length(prop->name));
                                    writeText(strval, cstr_length(strval));
                                }
                            }
                            else
                            {
                                writeText(prop->name, cstr_length(prop->name));
                                writeNullValue();
                            }
                        }
                    }
                    break;
                case 'B':
                    {
                        bool Bval = ObjectCodec::getBooleanProperty(obj, prop);
                        if (Bval != prop->deflt.B)
                        {
                            writeText(prop->name, cstr_length(prop->name));
                            writeBooleanValue(Bval);
                        }
                    }
                    break;
                case 'i':
                    {
                        int ival = ObjectCodec::getIntegerProperty(obj, prop);
                        if (ival != prop->deflt.i)
                        {
                            writeText(prop->name, cstr_length(prop->name));
                            writeIntegerValue(ival);
                        }
                    }
                    break;
                case 'l':
                    {
                        long lval = ObjectCodec::getIntegerProperty(obj, prop);
                        if (lval != prop->deflt.l)
                        {
                            writeText(prop->name, cstr_length(prop->name));
                            writeIntegerValue(lval);
                        }
                    }
                    break;
                case 'f':
                    {
                        float fval = ObjectCodec::getRealProperty(obj, prop);
                        if (fval != prop->deflt.f && (!isNone(fval) || !isNone(prop->deflt.f)))
                            /* isNone() test required because floating point NaN != NaN */
                        {
                            writeText(prop->name, cstr_length(prop->name));
                            writeRealValue(fval);
                        }
                    }
                    break;
                case 'd':
                    {
                        double dval = ObjectCodec::getRealProperty(obj, prop);
                        if (dval != prop->deflt.d && (!isNone(dval) || !isNone(prop->deflt.d)))
                            /* isNone() test required because floating point NaN != NaN */
                        {
                            writeText(prop->name, cstr_length(prop->name));
                            writeRealValue(dval);
                        }
                    }
                    break;
                default:
                    break;
                }
            }
            return writeEndObject();
        }
    }
    else if (Map *map = dynamic_cast<Map *>(obj))
    {
        writeStartObject();
        CString **keys = (CString **)map->keys();
        qsort(keys, carr_length(keys), sizeof(Object *), Object::qsort_cmp);
        for (size_t i = 0, n = carr_length(keys); n > i; i++)
        {
            Object *obj = map->object(keys[i]);
            writeText(keys[i]->chars(), keys[i]->length());
            writeObjectInternal(obj);
        }
        return writeEndObject();
    }
    else if (Array *array = dynamic_cast<Array *>(obj))
    {
        writeStartArray();
        foreach (Object *obj, array)
            writeObjectInternal(obj);
        return writeEndArray();
    }
    else if (Value *value = dynamic_cast<Value *>(obj))
    {
        switch (value->kind())
        {
        case 'B':
            return writeBooleanValue(value->booleanValue());
        case 'l':
            return writeIntegerValue(value->integerValue());
        case 'd':
            return writeRealValue(value->realValue());
        default:
            /* should not happen */
            return writeText(value->strrepr());
        }
    }
    else if (0 != obj)
    {
        const char *str = obj->strrepr();
        return writeText(str, cstr_length(str));
    }
    else
        return writeNullValue();
}

}
}
