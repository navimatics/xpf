/*
 * Base/impl/ThreadLoop.cpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#include <Base/impl/ThreadLoop.hpp>
#include <Base/impl/LinkedLists.hpp>
#include <Base/impl/Synchronization.hpp>
#if defined(BASE_CONFIG_WINDOWS)
#include <windows.h>
#elif defined(BASE_CONFIG_DARWIN)
#include <CoreFoundation/CoreFoundation.h>
#else
#error ThreadLoop.cpp not implemented for this platform
#endif

namespace Base {
namespace impl {

bool _ThreadLoopSignal0(void *);
bool _ThreadLoopSignal1(void *);

class ThreadLoopQueue : public Object
{
public:
    ThreadLoopQueue();
    ~ThreadLoopQueue();
    bool enqueInvocation(DelegateData del, Object *obj, bool isdel, void *sigtgt);
    void cancelInvocations(DelegateData del);
    void dequeInvocation();
private:
    struct invocation_listentry_t
    {
        invocation_listentry_t *next;
    };
    struct invocation_t
    {
        invocation_listentry_t entry;
        DelegateData del;
        Object *obj;
        bool isdel;
    };
    mutex_t _mutex;
    invocation_listentry_t _queue, *_back;
};
ThreadLoopQueue::ThreadLoopQueue()
{
    mutex_init(&_mutex);
    slist_back_reset(_back, &_queue);
}
ThreadLoopQueue::~ThreadLoopQueue()
{
    invocation_listentry_t *entry = _queue.next;
    while (&_queue != entry)
    {
        invocation_t *invocation = (invocation_t *)entry;
        entry = entry->next;
        if (0 != invocation->del.data)
            ((Object *)invocation->del.data)->release();
        if (0 != invocation->obj)
            invocation->obj->release();
        free(invocation);
    }
    mutex_fini(&_mutex);
}
bool ThreadLoopQueue::enqueInvocation(DelegateData del, Object *obj, bool isdel, void *sigtgt)
{
    invocation_t *invocation = (invocation_t *)Malloc(sizeof(invocation_t));
    slist_reset(&invocation->entry);
    invocation->del = del;
    invocation->obj = obj;
    invocation->isdel = isdel;
    if (0 != invocation->del.data)
        ((Object *)invocation->del.data)->retain();
    if (0 != invocation->obj)
        invocation->obj->retain();
    mutex_lock(&_mutex);
    bool res = _ThreadLoopSignal1(sigtgt);
    if (res)
        slist_push_back(_back, &invocation->entry);
    mutex_unlock(&_mutex);
    return res;
}
void ThreadLoopQueue::cancelInvocations(DelegateData del)
{
    mutex_lock(&_mutex);
    invocation_listentry_t *entry = _queue.next;
    while (&_queue != entry)
    {
        invocation_t *invocation = (invocation_t *)entry;
        entry = entry->next;
        if ((0 == del.code || invocation->del.code == del.code) && invocation->del.data == del.data)
        {
            invocation->del.code = 0;
            if (0 != invocation->del.data)
            {
                ((Object *)invocation->del.data)->release();
                invocation->del.data = 0;
            }
            if (0 != invocation->obj)
            {
                invocation->obj->release();
                invocation->obj = 0;
            }
        }
    }
    mutex_unlock(&_mutex);
}
void ThreadLoopQueue::dequeInvocation()
{
    mutex_lock(&_mutex);
    invocation_listentry_t *entry = slist_pop_front(_back);
    mutex_unlock(&_mutex);
#if defined(NDEBUG)
    if (&_queue == entry)
        return;
#else
    assert(&_queue != entry);
#endif
    invocation_t *invocation = (invocation_t *)entry;
    if (0 != invocation->del.code)
        mark_and_collect
        {
            if (invocation->isdel)
                ((void (*)(void *, Object *))invocation->del.code)(invocation->del.data, invocation->obj);
            else
                ((void (*)(Object *))invocation->del.code)(invocation->obj);
        }
    if (0 != invocation->del.data)
        ((Object *)invocation->del.data)->release();
    if (0 != invocation->obj)
        invocation->obj->release();
    free(invocation);
}

#if defined(BASE_CONFIG_WINDOWS)
static void _ThreadLoopCollectionInit()
{
    /* !!!: REVISIT: empty */
}
static UINT _ThreadLoopMessage()
{
    static UINT message;
    execute_once
        message = RegisterWindowMessageA("ThreadLoopMessage-AD76B09C-B725-4EC2-A202-BDE1A3E03D6D");
    return message;
}
static LRESULT CALLBACK _ThreadLoopWindowProc(HWND hwnd, UINT message, WPARAM wparam, LPARAM lparam)
{
    if (_ThreadLoopMessage() == message)
    {
        if (1 == wparam)
        {
            Thread *thread = Thread::currentThread();
            ThreadLoopQueue *queue = (ThreadLoopQueue *)thread->_slot(Thread::_SlotObjectThreadLoopQueue);
            queue->dequeInvocation();
        }
        return 0;
    }
    else
        return DefWindowProc(hwnd, message, wparam, lparam);
}
static void *_ThreadLoopSignalTarget()
{
    static ATOM classAtom;
    execute_once
    {
        WNDCLASSA wc = { 0 };
        wc.style = CS_GLOBALCLASS;
        wc.lpfnWndProc = _ThreadLoopWindowProc;
        wc.hInstance = GetModuleHandle(0);
        wc.lpszClassName = "ThreadLoopWindow-AD76B09C-B725-4EC2-A202-BDE1A3E03D6D";
        classAtom = RegisterClassA(&wc);
    }
    /*
     * 1. CreateWindow() forces creation of a message queue.
     *    See http://msdn.microsoft.com/en-us/library/ms644928.aspx
     * 2. When a thread terminates its windows are automatically freed.
     *    See http://msdn.microsoft.com/en-us/library/windows/desktop/ms686724.aspx
     */
    return CreateWindowA((LPSTR)classAtom, "", 0, 0, 0, 0, 0, HWND_MESSAGE, 0, GetModuleHandle(0), 0);
}
bool _ThreadLoopSignal0(void *hwnd)
{
    return FALSE != PostMessage((HWND)hwnd, _ThreadLoopMessage(), 0, 0);
}
bool _ThreadLoopSignal1(void *hwnd)
{
    return FALSE != PostMessage((HWND)hwnd, _ThreadLoopMessage(), 1, 0);
}
#elif defined(BASE_CONFIG_DARWIN)
struct _ThreadLoopCollectionObserverInfo
{
    int refcnt;
    int nested;
    unsigned mask;
    static void *alloc()
    {
        _ThreadLoopCollectionObserverInfo *p =
            (_ThreadLoopCollectionObserverInfo *)Malloc(sizeof(_ThreadLoopCollectionObserverInfo));
        p->refcnt = 0;
        p->nested = -1;
        p->mask = 0;
        return p;
    }
    static const void *retain(const void *p)
    {
        _ThreadLoopCollectionObserverInfo *info = (_ThreadLoopCollectionObserverInfo *)p;
        info->refcnt++;
        return info;
    }
    static void release(const void *p)
    {
        _ThreadLoopCollectionObserverInfo *info = (_ThreadLoopCollectionObserverInfo *)p;
        int rc = --info->refcnt;
        if (0 == rc)
            free(info);
    }
};
static void _ThreadLoopCollectionObserverCallback(
    CFRunLoopObserverRef observer, CFRunLoopActivity activity, void *p)
{
    /* We use two CFRunLoop observers to maintain a collection pool during event dispatching.
     * We set up two observers one for kCFRunLoopEntry with an order of INT_MIN (run before
     * other observers) and another one for kCFRunLoopBeforeTimers | kCFRunLoopExit with an
     * order of INT_MAX (run after other observers). This is particular important on iOS,
     * where observers are used by subsystems such as CoreAnimation; their observers may
     * otherwise get called without a collection pool in place.
     *
     * The CFRunLoop effectively looks like this (see CFRunLoop.c in opensource.apple.com):
     *     DoObservers(kCFRunLoopEntry);
     *     loop
     *         DoObservers(kCFRunLoopBeforeTimers)
     *         wait for and dispatch timers, events, etc.
     *     DoObservers(kCFRunLoopExit)
     *
     * If the runloop enters and exits after a single event, we will execute:
     *     kCFRunLoopEntry:
     *         Object::Mark();
     *     kCFRunLoopBeforeTimers:
     *         // empty -- first BeforeTimers does not do a Collect()/Mark(); doing so would be extraneous
     *     kCFRunLoopExit:
     *         Object::Collect();;
     *
     * If the runloop enters, dispatches multiple events and then exits, we will execute:
     *     kCFRunLoopEntry:
     *         Object::Mark();
     *     kCFRunLoopBeforeTimers:
     *         // empty
     *     kCFRunLoopBeforeTimers:
     *         Object::Collect();
     *         Object::Mark();
     *     kCFRunLoopBeforeTimers:
     *         Object::Collect();
     *         Object::Mark();
     *     ...
     *     kCFRunLoopExit:
     *         Object::Collect();
     *
     * If the runloop enters, dispatches multiple events, gets reentered,
     * then dispatches more events and then exits, we will execute:
     *     kCFRunLoopEntry:
     *         Object::Mark();
     *     kCFRunLoopBeforeTimers:
     *         // empty
     *     kCFRunLoopBeforeTimers:
     *         Object::Collect();
     *         Object::Mark();
     *     ...
     *     loop reentered
     *         kCFRunLoopEntry:
     *             Object::Mark();
     *         kCFRunLoopBeforeTimers:
     *             // empty
     *         kCFRunLoopBeforeTimers:
     *             Object::Collect();
     *             Object::Mark();
     *         ...
     *         kCFRunLoopExit:
     *             Object::Collect();
     *     reentered loop exited
     *     kCFRunLoopBeforeTimers:
     *         Object::Collect();
     *         Object::Mark();
     *     ...
     *     kCFRunLoopExit:
     *         Object::Collect();
     */
    _ThreadLoopCollectionObserverInfo *info = (_ThreadLoopCollectionObserverInfo *)p;
    switch (activity)
    {
    case kCFRunLoopEntry:
        info->nested++;
        Object::Mark();
        break;
    case kCFRunLoopBeforeTimers:
        if (IMAX_BITS(INT_MAX) <= info->nested || (info->mask & (1 << info->nested)))
        {
            Object::Collect();
            Object::Mark();
        }
        else
            info->mask |= (1 << info->nested);
        break;
    case kCFRunLoopExit:
        if (IMAX_BITS(INT_MAX) > info->nested)
            info->mask &= ~(1 << info->nested);
        Object::Collect();
        info->nested--;
        break;
    }
}
static void _ThreadLoopCollectionInit()
{
    CFRunLoopObserverContext context =
    {
        0,
        _ThreadLoopCollectionObserverInfo::alloc(),
        _ThreadLoopCollectionObserverInfo::retain,
        _ThreadLoopCollectionObserverInfo::release,
        0
    };
    {
        CFRunLoopObserverRef observer = CFRunLoopObserverCreate(0,
            kCFRunLoopEntry, true, INT_MIN,
            _ThreadLoopCollectionObserverCallback, &context);
        if (0 == observer)
            MemoryAllocationError();
        CFRunLoopAddObserver(CFRunLoopGetCurrent(), observer, kCFRunLoopCommonModes);
        CFRelease(observer);
    }
    {
        CFRunLoopObserverRef observer = CFRunLoopObserverCreate(0,
            kCFRunLoopBeforeTimers | kCFRunLoopExit, true, INT_MAX,
            _ThreadLoopCollectionObserverCallback, &context);
        if (0 == observer)
            MemoryAllocationError();
        CFRunLoopAddObserver(CFRunLoopGetCurrent(), observer, kCFRunLoopCommonModes);
        CFRelease(observer);
    }
}
/* portions implemented in Base/impl/ThreadLoop.mm */
void *_ThreadLoopSignalTarget();
void _ThreadLoopInvokeOnCurrentThread()
{
    Thread *thread = Thread::currentThread();
    ThreadLoopQueue *queue = (ThreadLoopQueue *)thread->_slot(Thread::_SlotObjectThreadLoopQueue);
    queue->dequeInvocation();
}
#endif
static Thread *_ThreadLoopInit()
{
    Thread *thread = Thread::currentThread();
    if (0 == thread->_slot(Thread::_SlotThreadLoopSignalTarget))
    {
        ThreadLoopQueue *queue = new ThreadLoopQueue;
        thread->_setSlotObject(Thread::_SlotObjectThreadLoopQueue, queue);
        atomic_wmb();
        thread->_setSlot(Thread::_SlotThreadLoopSignalTarget, _ThreadLoopSignalTarget());
        queue->release();
        _ThreadLoopCollectionInit();
    }
    return thread;
}
void ThreadLoopInit()
{
    _ThreadLoopInit();
}
#if defined(BASE_CONFIG_WINDOWS)
void ThreadLoopRun()
{
    Thread *thread = _ThreadLoopInit();
    thread->_setSlot(Thread::_SlotThreadLoopActive, (void *)1);
    while (thread->_slot(Thread::_SlotThreadLoopActive))
        mark_and_collect
        {
            MSG msg;
            BOOL res = GetMessage(&msg, 0, 0, 0);
            assert(-1 != res);
            if (0 == res)
            {
                PostQuitMessage(msg.wParam);
                break;
            }
            TranslateMessage(&msg);
            DispatchMessage(&msg);
        }
}
#elif defined(BASE_CONFIG_DARWIN)
void ThreadLoopRun()
{
    Thread *thread = _ThreadLoopInit();
    thread->_setSlot(Thread::_SlotThreadLoopActive, (void *)1);
    while (thread->_slot(Thread::_SlotThreadLoopActive))
        mark_and_collect
        {
            SInt32 res = CFRunLoopRunInMode(kCFRunLoopDefaultMode, 1e+20, true);
            if (kCFRunLoopRunStopped == res || kCFRunLoopRunFinished == res)
                break;
        }
}
#endif
void ThreadLoopStop()
{
    Thread *thread = Thread::currentThread();
    if (thread->_slot(Thread::_SlotThreadLoopActive))
    {
        thread->_setSlot(Thread::_SlotThreadLoopActive, 0);
        _ThreadLoopSignal0(thread->_slot(Thread::_SlotThreadLoopSignalTarget));
    }
}
static bool _ThreadLoopInvoke(Thread *thread, DelegateData del, Object *obj, bool isdel)
{
    /*
     * ThreadLoopInvoke/ThreadLoopCancelInvoke
     *
     * This function uses the _SlotThreadLoopSignalTarget and _SlotObjectThreadLoopQueue
     * thread slot from a different thread!
     *
     * This works because the slots are written only once for the lifetime of the owner thread.
     * Furthermore the slots are written and read in such an order that with appropriate use
     * of memory barriers they can be safely used from a different thread.
     *
     *     Owner thread                         Invoker thread
     *     ------------                         --------------
     *     WR _SlotObjectThreadLoopQueue
     *     WMB
     *     WR _SlotThreadLoopSignalTarget
     *                                          RD _SlotThreadLoopSignalTarget
     *                                          RMB
     *                                          RD _SlotObjectThreadLoopQueue
     */
    void *sigtgt = thread->_slot(Thread::_SlotThreadLoopSignalTarget);
    atomic_rmb();
    if (0 == sigtgt)
        return false;
    ThreadLoopQueue *queue = (ThreadLoopQueue *)thread->_slot(Thread::_SlotObjectThreadLoopQueue);
    return queue->enqueInvocation(del, obj, isdel, sigtgt);
}
static bool _ThreadLoopCancelInvoke(Thread *thread, DelegateData del)
{
    void *sigtgt = thread->_slot(Thread::_SlotThreadLoopSignalTarget);
    atomic_rmb();
    if (0 == sigtgt)
        return false;
    ThreadLoopQueue *queue = (ThreadLoopQueue *)thread->_slot(Thread::_SlotObjectThreadLoopQueue);
    queue->cancelInvocations(del);
    return true;
}
bool ThreadLoopInvoke(Thread *thread, void (*fn)(Object *), Object *obj)
{
    DelegateData del = { (void (*)(void *))fn, 0 };
    return _ThreadLoopInvoke(thread, del, obj, false);
}
bool ThreadLoopInvoke(Thread *thread, Delegate<void (Object *)> del, Object *obj)
{
    return _ThreadLoopInvoke(thread, del, obj, true);
}
bool ThreadLoopCancelInvoke(Thread *thread, void (*fn)(Object *))
{
    if (0 == fn)
        return false;
    DelegateData del = { (void (*)(void *))fn, 0 };
    return _ThreadLoopCancelInvoke(thread, del);
}
bool ThreadLoopCancelInvoke(Thread *thread, Delegate<void (Object *)> del)
{
    if (0 == del.code && 0 == del.data)
        return false;
    return _ThreadLoopCancelInvoke(thread, del);
}
bool ThreadLoopCancelInvoke(Thread *thread, Object *target)
{
    if (0 == target)
        return false;
    DelegateData del = { 0, target };
    return _ThreadLoopCancelInvoke(thread, del);
}

/* timers */
struct timer_t
{
#if defined(BASE_CONFIG_WINDOWS)
    UINT_PTR handle;
#elif defined(BASE_CONFIG_DARWIN)
    CFRunLoopTimerRef handle;
#endif
    DelegateData del;
    Object *obj;
    bool isdel;
};
static void _ThreadLoopTimerInvoke(timer_t *timer)
{
    if (0 != timer->del.code)
        mark_and_collect
        {
            if (timer->isdel)
                ((void (*)(void *, Object *))timer->del.code)(timer->del.data, timer->obj);
            else
                ((void (*)(Object *))timer->del.code)(timer->obj);
        }
}
#if defined(BASE_CONFIG_WINDOWS)
static VOID CALLBACK _ThreadLoopTimerProc(HWND hwnd, UINT message, UINT_PTR handle, DWORD time)
{
    _ThreadLoopTimerInvoke((timer_t *)handle);
}
#elif defined(BASE_CONFIG_DARWIN)
static void _ThreadLoopTimerProc(CFRunLoopTimerRef handle, void *info)
{
    _ThreadLoopTimerInvoke((timer_t *)info);
}
#endif
static void *_ThreadLoopSetTimer(wait_timeout_t timeout, DelegateData del, Object *obj, bool isdel)
{
    Thread *thread = _ThreadLoopInit();
    timer_t *timer = (timer_t *)Malloc(sizeof(timer_t));
    timer->handle = 0;
    timer->del = del;
    timer->obj = obj;
    timer->isdel = isdel;
    if (0 != timer->del.data)
        ((Object *)timer->del.data)->retain();
    if (0 != timer->obj)
        timer->obj->retain();
#if defined(BASE_CONFIG_WINDOWS)
    HWND hwnd = (HWND)thread->_slot(Thread::_SlotThreadLoopSignalTarget);
    timer->handle = SetTimer(
        hwnd, (UINT_PTR)timer, timeout, _ThreadLoopTimerProc);
    if (0 == timer->handle)
        goto error;
#elif defined(BASE_CONFIG_DARWIN)
    (void)thread;
    CFRunLoopTimerContext context = { 0, timer };
    CFTimeInterval interval = timeout / 1000.0;
    timer->handle = CFRunLoopTimerCreate(0,
        CFAbsoluteTimeGetCurrent() + interval, interval, 0, 0, _ThreadLoopTimerProc, &context);
    if (0 == timer->handle)
        goto error;
    CFRunLoopAddTimer(CFRunLoopGetCurrent(), timer->handle, kCFRunLoopCommonModes);
#endif
    return timer;
error:
    if (0 != timer->del.data)
        ((Object *)timer->del.data)->release();
    if (0 != timer->obj)
        timer->obj->release();
    free(timer);
    return 0;
}
static bool _ThreadLoopCancelTimer(void *timer0)
{
    timer_t *timer = (timer_t *)timer0;
#if defined(BASE_CONFIG_WINDOWS)
    Thread *thread = Thread::currentThread();
    HWND hwnd = (HWND)thread->_slot(Thread::_SlotThreadLoopSignalTarget);
    KillTimer(hwnd, timer->handle);
    MSG msg;
    while (PeekMessage(&msg, hwnd, WM_TIMER, WM_TIMER, PM_REMOVE))
        ;
#elif defined(BASE_CONFIG_DARWIN)
    CFRunLoopTimerInvalidate(timer->handle);
    CFRelease(timer->handle);
#endif
    if (0 != timer->del.data)
        ((Object *)timer->del.data)->release();
    if (0 != timer->obj)
        timer->obj->release();
    free(timer);
    return true;
}
void *ThreadLoopSetTimer(wait_timeout_t timeout, void (*fn)(Object *), Object *obj)
{
    DelegateData del = { (void (*)(void *))fn, 0 };
    return _ThreadLoopSetTimer(timeout, del, obj, false);
}
void *ThreadLoopSetTimer(wait_timeout_t timeout, Delegate<void (Object *)> del, Object *obj)
{
    return _ThreadLoopSetTimer(timeout, del, obj, true);
}
bool ThreadLoopCancelTimer(void *timer)
{
    if (0 == timer)
        return false;
    return _ThreadLoopCancelTimer(timer);
}

}
}
