/*
 * Base/impl/Syn.cpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#include <Base/impl/Syn.hpp>
#include <Base/impl/LinkedLists.hpp>
#include <Base/impl/Tls.hpp>

namespace Base {
namespace impl {

/*
 * Mutexes
 */
#if defined(BASE_CONFIG_POSIX)
static pthread_mutexattr_t *_mutex_defaultattr()
{
    pthread_mutexattr_t *attr = (pthread_mutexattr_t *)Malloc(sizeof(pthread_mutexattr_t));
    memset(attr, 0, sizeof(pthread_mutexattr_t));
    int res = pthread_mutexattr_init(attr);
    if (0 != res)
        MemoryAllocationError();
    pthread_mutexattr_settype(attr, PTHREAD_MUTEX_RECURSIVE);
    return attr;
}
pthread_mutexattr_t *mutex_defaultattr()
{
    static pthread_mutexattr_t *attr = _mutex_defaultattr();
        /* threadsafe: __forcecall_mutex_defaultattr */
    return attr;
}
static pthread_mutexattr_t *__forcecall_mutex_defaultattr = mutex_defaultattr();
#endif

/*
 * Read-write locks
 */
/*
 * This is a cross-platform implementation of a "fair" read-write lock.
 *
 * See http://www.rfc1149.net/blog/2011/01/07/the-third-readers-writers-problem/ for explanation.
 * Inspired by http://locklessinc.com/articles/sleeping_rwlocks/ "Fair Read-Write Locks" section.
 */
void _rwlock_init(_rwlock_t *rwlock, bool fair)
{
    rwlock->fair = fair;
    if (rwlock->fair)
        mutex_init(&rwlock->order_mutex);
    mutex_init(&rwlock->write_mutex);
    rwlock->readers = 0;
}
void _rwlock_fini(_rwlock_t *rwlock)
{
    mutex_fini(&rwlock->write_mutex);
    if (rwlock->fair)
        mutex_fini(&rwlock->order_mutex);
}
void _rwlock_rdlock(_rwlock_t *rwlock)
{
    if (rwlock->fair)
    {
        mutex_lock(&rwlock->order_mutex);
        if (1 == atomic_inc(rwlock->readers))
            /* first reader locks write mutex, thus disallowing writers */
            mutex_lock(&rwlock->write_mutex);
        mutex_unlock(&rwlock->order_mutex);
    }
    else
    {
        if (1 == atomic_inc(rwlock->readers))
            /* first reader locks write mutex, thus disallowing writers */
            mutex_lock(&rwlock->write_mutex);
    }
}
void _rwlock_rdunlock(_rwlock_t *rwlock)
{
    if (0 == atomic_dec(rwlock->readers))
        /* last reader unlocks write mutex, thus allowing writers */
        mutex_unlock(&rwlock->write_mutex);
}
void _rwlock_wrlock(_rwlock_t *rwlock)
{
    if (rwlock->fair)
    {
        mutex_lock(&rwlock->order_mutex);
        mutex_lock(&rwlock->write_mutex);
        mutex_unlock(&rwlock->order_mutex);
    }
    else
        mutex_lock(&rwlock->write_mutex);
}
void _rwlock_wrunlock(_rwlock_t *rwlock)
{
    mutex_unlock(&rwlock->write_mutex);
}

/*
 * Condition variables
 */
#if defined(BASE_CONFIG_WINDOWS)
/*
 * See http://research.microsoft.com/pubs/64242/implementingcvs.pdf "The Sequel—NT and PThreads" section.
 *
 * The described algorithm (which uses a LIFO instead of a FIFO wait list) is as follows:
 *
 * class Thread {
 *     public static Semaphore x; // Global lock; initially x.count = 1; x.limit = 1
 *     public Thread next = null;
 *     public Semaphore s = new Semaphore(); // Initially s.count = 0; s.limit = 1;
 *     public static Thread Self() { ... }
 * }
 * class CV {
 *     Lock m;
 *     Thread waiters = null;
 *     public CV(Lock m) {
 *         this.m = m; }
 *     public void Wait() { // Pre-condition: this thread holds “m”
 *         Thread self = Thread.Self();
 *         Thread.x.P(); {
 *             self.next = waiters;
 *             waiters = self;
 *         } Thread.x.V();
 *         m.Release();
 *         self.s.P();
 *         m.Acquire();
 *     }
 *     public void Signal() {
 *         Thread.x.P(); {
 *             if (waiters != null) {
 *                 waiters.s.V();
 *                 waiters = waiters.next;
 *             }
 *         } Thread.x.V();
 *     }
 *     public void Broadcast() {
 *         Thread.x.P(); {
 *             while (waiters != null) {
 *                 waiters.s.V();
 *                 waiters = waiters.next;
 *             }
 *         } Thread.x.V();
 *     }
 * }
 */
struct _CondvarTls
{
    _condvar_listentry_t entry;
        /* must be first */
    HANDLE waitobj;
};
static void _CondvarTlsFree(void *p)
{
    _CondvarTls *tls = (_CondvarTls *)p;
    CloseHandle(tls->waitobj);
    free(tls);
}
static tlskey_t _CondvarTlsKey() BASE_FUNCCONST;
static tlskey_t _CondvarTlsKey()
{
    static tlskey_t tlskey = tlsalloc(_CondvarTlsFree);
        /* threadsafe: __forcecall_CondvarTlsKey */
    return tlskey;
}
static tlskey_t __forcecall_CondvarTlsKey = _CondvarTlsKey();
void _condvar_init(_condvar_t *condvar)
{
    mutex_init(&condvar->mutex);
    dlist_reset(&condvar->waitq);
}
void _condvar_fini(_condvar_t *condvar)
{
    mutex_fini(&condvar->mutex);
}
bool _condvar_wait(_condvar_t *condvar, mutex_t *mutex, wait_timeout_t timeout)
{
    /* get the thread wait object (if any) */
    tlskey_t tlskey = _CondvarTlsKey();
    _CondvarTls *tls = (_CondvarTls *)tlsget(tlskey);
    if (0 == tls)
    {
        /* allocate thread-local storage; create the thread wait object and place it in the storage */
        tls = (_CondvarTls *)malloc(sizeof(_CondvarTls));
        if (0 == tls)
            MemoryAllocationError();
        tls->waitobj = CreateSemaphore(0, 0, 1, 0);
            /* binary semaphore */
        if (0 == tls->waitobj)
            MemoryAllocationError();
        dlist_reset(&tls->entry);
        tlsset(tlskey, tls);
    }
    /* insert the thread wait object into the waitq */
    mutex_lock(&condvar->mutex);
    dlist_push_back(&condvar->waitq, &tls->entry);
    mutex_unlock(&condvar->mutex);
    /* release external mutex and wait on the thread wait object */
    mutex_unlock(mutex);
    bool result = WAIT_OBJECT_0 == WaitForSingleObject(tls->waitobj, timeout);
    if (!result)
    {
        /* remove thread wait object from waitq */
        mutex_lock(&condvar->mutex);
        dlist_remove(&tls->entry);
        dlist_reset(&tls->entry);
        mutex_unlock(&condvar->mutex);
        /* consume any notification that may have arrived after the timeout */
        WaitForSingleObject(tls->waitobj, 0);
    }
    /* reacquire external mutex */
    mutex_lock(mutex);
    /* report success/failure */
    return result;
}
void _condvar_notify(_condvar_t *condvar)
{
    mutex_lock(&condvar->mutex);
    /* remove a thread wait object from the waitq list */
    _condvar_listentry_t *entry = dlist_pop_back(&condvar->waitq);
    if (&condvar->waitq == entry)
        goto done;
    dlist_reset(entry);
    /* if there was a thread wait object wake up its thread */
    _CondvarTls *tls = (_CondvarTls *)entry;
    ReleaseSemaphore(tls->waitobj, 1, 0);
done:
    mutex_unlock(&condvar->mutex);
}
void _condvar_notify_all(_condvar_t *condvar)
{
    mutex_lock(&condvar->mutex);
    for (;;)
    {
        /* remove a thread wait object from the waitq list */
        _condvar_listentry_t *entry = dlist_pop_back(&condvar->waitq);
        if (&condvar->waitq == entry)
            goto done;
        dlist_reset(entry);
        /* if there was a thread wait object wake up its thread */
        _CondvarTls *tls = (_CondvarTls *)entry;
        ReleaseSemaphore(tls->waitobj, 1, 0);
    }
done:
    mutex_unlock(&condvar->mutex);
}
#elif defined(BASE_CONFIG_POSIX)
bool _condvar_timedwait(_condvar_t *condvar, mutex_t *mutex, wait_timeout_t timeout)
{
    struct timeval tv;
    struct timespec ts;
    gettimeofday(&tv, 0);
    ts.tv_sec = tv.tv_sec + timeout / 1000;
    ts.tv_nsec = tv.tv_usec * 1000 + (timeout % 1000) * 1000000;
    if (1000000000 <= ts.tv_nsec)
    {
        ts.tv_sec++;
        ts.tv_nsec -= 1000000000;
    }
    return 0 == pthread_cond_timedwait(condvar, mutex, &ts);
}
#endif

/*
 * Execute once
 */
static mutex_t *_execute_once_mutex()
{
    mutex_t *mutex = (mutex_t *)Malloc(sizeof(mutex_t));
    mutex_init(mutex);
    return mutex;
}
static mutex_t *execute_once_mutex()
{
    static mutex_t *mutex = _execute_once_mutex();
        /* threadsafe: __forcecall_execute_once_mutex */
    return mutex;
}
static mutex_t *__forcecall_execute_once_mutex = execute_once_mutex();
bool execute_once_enter(execute_once_control_t *i)
{
    bool f = 0 != *i;
    atomic_rmb();
    if (f)
        return false;
    mutex_t *mutex = execute_once_mutex();
    mutex_lock(mutex);
    f = 0 != *i;
    if (f)
    {
        mutex_unlock(mutex);
        return false;
    }
    return true;
}
void execute_once_leave(execute_once_control_t *i)
{
    atomic_wmb();
    *i = 1;
    mutex_t *mutex = execute_once_mutex();
    mutex_unlock(mutex);
}

}
}
