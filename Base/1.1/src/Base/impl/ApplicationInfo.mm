/*
 * Base/impl/ApplicationInfo.mm
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#include <Base/impl/Defines.hpp>
#if defined(BASE_CONFIG_DARWIN)
#include <Base/impl/ApplicationInfo.hpp>
#include <Base/impl/CString.hpp>
#import <Foundation/Foundation.h>

namespace Base {
namespace impl {

const char *_GetApplicationName()
{
    const char *chars = 0;
    NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
    NSBundle *bundle = [NSBundle mainBundle];
    id value = [bundle objectForInfoDictionaryKey:@"CFBundleName"];
    if (![value isKindOfClass:[NSString class]])
        value = [[bundle executablePath] lastPathComponent];
    if (nil != value)
        chars = cstr_new([value UTF8String]);
    [pool release];
    return chars;
}
const char *_GetApplicationResourceDirectory()
{
    const char *chars = 0;
    NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
    NSBundle *bundle = [NSBundle mainBundle];
    id value = [bundle resourcePath];
    if (nil != value)
    {
        const char *path = [value UTF8String];
        ssize_t len = strlen(path);
        if ('/' == path[len - 1])
            chars = cstr_new(path, len);
        else
            chars = cstr_newf("%s/", path);
    }
    [pool release];
    return chars;
}
const char *_GetApplicationDataDirectory()
{
    const char *chars = 0;
    NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
    NSArray *array = NSSearchPathForDirectoriesInDomains(
        NSApplicationSupportDirectory, NSUserDomainMask, YES);
    if (0 < [array count])
    {
        const char *path = [[array objectAtIndex:0] UTF8String];
        NSBundle *bundle = [NSBundle mainBundle];
        id value = [bundle objectForInfoDictionaryKey:@"CFBundleIdentifier"];
        if ([value isKindOfClass:[NSString class]])
            chars = cstr_newf("%s/%s/", path, [value UTF8String]);
        else
            chars = cstr_newf("%s/%s/", path, GetApplicationName());
    }
    [pool release];
    return chars;
}
const char *_GetDocumentDirectory()
{
    const char *chars = 0;
    NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
    NSArray *array = NSSearchPathForDirectoriesInDomains(
        NSDocumentDirectory, NSUserDomainMask, YES);
    if (0 < [array count])
    {
        const char *path = [[array objectAtIndex:0] UTF8String];
        ssize_t len = strlen(path);
        if ('/' == path[len - 1])
            chars = cstr_new(path, len);
        else
            chars = cstr_newf("%s/", path);
    }
    [pool release];
    return chars;
}
const char *_GetTemporaryDirectory()
{
    const char *chars = 0;
    NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
    NSString *value = NSTemporaryDirectory();
    if (0 != value)
    {
        const char *path = [value UTF8String];
        ssize_t len = strlen(path);
        if ('/' == path[len - 1])
            chars = cstr_new(path, len);
        else
            chars = cstr_newf("%s/", path);
    }
    [pool release];
    return chars;
}

}
}
#endif
