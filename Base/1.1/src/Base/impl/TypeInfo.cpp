/*
 * Base/impl/TypeInfo.cpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#include <Base/impl/Defines.hpp>
#include <Base/impl/CString.hpp>
#include <Base/impl/TypeInfo.hpp>
#if defined(BASE_CONFIG_GNUC)
#include <cxxabi.h>
#endif

namespace Base {
namespace impl {

const char *TypeName(const std::type_info &tinfo)
{
#if defined(BASE_CONFIG_MSVC)
    const char *name = tinfo.name();
    if (0 == strncmp("class ", name, 6))
        name += 6;
    return cstring(name);
#elif defined(BASE_CONFIG_GNUC)
    int status = 0;
    char *name = abi::__cxa_demangle(tinfo.name(), 0, 0, &status);
    const char *result;
    if (0 != name && 0 == status)
    {
        result = cstring(name);
        free(name);
    }
    else
        result = cstring("<Unknown>");
    return result;
#endif
}

}
}
