/*
 * Base/impl/Environment.cpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#include <Base/impl/Environment.hpp>
#include <Base/impl/ApplicationInfo.hpp>
#include <Base/impl/CString.hpp>
#include <Base/impl/ObjectUtil.hpp>
#include <Base/impl/Thread.hpp>

#if defined(BASE_CONFIG_WINDOWS)
#elif defined(BASE_CONFIG_DARWIN_MAC)
extern "C" char ***_NSGetEnviron(void);
#elif defined(BASE_CONFIG_DARWIN_IOS)
#elif defined(BASE_CONFIG_POSIX)
#endif

namespace Base {
namespace impl {

static void MapRemoveObjects(Map *target, Map *map)
{
    foreach (Object *key, map)
        target->removeObject(key);
}

inline Map *Environment::_threadMap()
{
    Map *threadMap = Thread::currentThreadMap();
    Map *map = ((Map *)threadMap->object(_threadMapKey));
    if (0 == map)
    {
        map = new Map;
        threadMap->setObject(_threadMapKey, map);
        map->release();
    }
    return map;
}

Environment *Environment::standardEnvironment()
{
    static Environment *env;
    execute_once
        env = new Environment(cstringf("%senvironment.native", GetApplicationDataDirectory()));
    return env;
}
Environment::Environment(const char *path)
{
    threadsafe();
    _path = cstr_new(path);
    _lock = new Lock;
    _threadMapKey = new Object;
    _threadMapKey->threadsafe();
    Map *map = new Map;
    Thread::currentThreadMap()->setObject(_threadMapKey, map);
    map->release();
    _processMap = new Map;
#if defined(BASE_CONFIG_WINDOWS)
    for (char **envp = _environ; 0 != *envp; envp++)
#elif defined(BASE_CONFIG_DARWIN_MAC)
    for (char **envp = *_NSGetEnviron(); 0 != *envp; envp++)
#elif defined(BASE_CONFIG_POSIX) && !defined(BASE_CONFIG_DARWIN_IOS) && !defined(BASE_CONFIG_ANDROID)
    for (char **envp = environ; 0 != *envp; envp++)
#endif
#if defined(BASE_CONFIG_WINDOWS) || \
    defined(BASE_CONFIG_DARWIN_MAC) || \
    (defined(BASE_CONFIG_POSIX) && !defined(BASE_CONFIG_DARWIN_IOS) && !defined(BASE_CONFIG_ANDROID))
        if (const char *p = strchr(*envp, '='))
        {
            CString *key = CString::create(*envp, p - *envp);
            CString *obj = CString::create(p + 1);
            _processMap->setObject(key, obj);
            obj->release();
            key->release();
        }
#endif
    Object *obj = ObjectReadNative(_path);
    if (Map *map = dynamic_cast<Map *>(obj))
        _applicationMap = (Map *)map->retain();
    else
        _applicationMap = new Map;
    _factoryMap = new Map;
}
Environment::~Environment()
{
    commit();
    _factoryMap->release();
    _applicationMap->release();
    _processMap->release();
    Thread::currentThreadMap()->removeObject(_threadMapKey);
    _threadMapKey->release();
    _lock->release();
    cstr_release(_path);
}
Lock *Environment::lock()
{
    return _lock;
}
Object *Environment::object(Object *key, unsigned kind)
{
    kind &= ~Override;
    Object *result = 0;
    if (Thread == kind)
        if (_threadMap()->getObject(key, result))
            return result;
    synchronized (_lock)
        switch (kind)
        {
        case Thread:
        case Process:
            if (_processMap->getObject(key, result))
                break;
        case Application:
            if (_applicationMap->getObject(key, result))
                break;
        case Factory:
            if (_factoryMap->getObject(key, result))
                break;
        default:
            break;
        }
    return result;
}
void Environment::setObject(Object *key, Object *obj, unsigned kind)
{
    unsigned override = kind & Override;
    kind &= ~Override;
    if (override)
        switch (kind)
        {
        case Thread:
            _threadMap()->setObject(key, obj);
            break;
        case Process:
            _threadMap()->removeObject(key);
            synchronized (_lock)
                _processMap->setObject(key, obj);
            break;
        case Application:
            _threadMap()->removeObject(key);
            synchronized (_lock)
            {
                _processMap->removeObject(key);
                _applicationMap->setObject(key, obj);
                _sync = true;
            }
            break;
        case Factory:
            _threadMap()->removeObject(key);
            synchronized (_lock)
            {
                _processMap->removeObject(key);
                _applicationMap->removeObject(key);
                _sync = true;
                _factoryMap->setObject(key, obj);
            }
            break;
        default:
            break;
        }
    else
        switch (kind)
        {
        case Thread:
            _threadMap()->setObject(key, obj);
            break;
        case Process:
            synchronized (_lock)
                _processMap->setObject(key, obj);
            break;
        case Application:
            synchronized (_lock)
            {
                _applicationMap->setObject(key, obj);
                _sync = true;
            }
            break;
        case Factory:
            synchronized (_lock)
                _factoryMap->setObject(key, obj);
            break;
        default:
            break;
        }
}
void Environment::removeObject(Object *key, unsigned kind)
{
    unsigned override = kind & Override;
    kind &= ~Override;
    if (override)
        switch (kind)
        {
        case Thread:
            _threadMap()->removeObject(key);
            break;
        case Process:
            _threadMap()->removeObject(key);
            synchronized (_lock)
                _processMap->removeObject(key);
            break;
        case Application:
            _threadMap()->removeObject(key);
            synchronized (_lock)
            {
                _processMap->removeObject(key);
                _applicationMap->removeObject(key);
                _sync = true;
            }
            break;
        case Factory:
            _threadMap()->removeObject(key);
            synchronized (_lock)
            {
                _processMap->removeObject(key);
                _applicationMap->removeObject(key);
                _sync = true;
                _factoryMap->removeObject(key);
            }
            break;
        default:
            break;
        }
    else
        switch (kind)
        {
        case Thread:
            _threadMap()->removeObject(key);
            break;
        case Process:
            synchronized (_lock)
                _processMap->removeObject(key);
            break;
        case Application:
            synchronized (_lock)
            {
                _applicationMap->removeObject(key);
                _sync = true;
            }
            break;
        case Factory:
            synchronized (_lock)
                _factoryMap->removeObject(key);
            break;
        default:
            break;
        }
}
void Environment::addObjects(Map *map, unsigned kind)
{
    unsigned override = kind & Override;
    kind &= ~Override;
    if (override)
        switch (kind)
        {
        case Thread:
            _threadMap()->addObjects(map);
            break;
        case Process:
            MapRemoveObjects(_threadMap(), map);
            synchronized (_lock)
                _processMap->addObjects(map);
            break;
        case Application:
            MapRemoveObjects(_threadMap(), map);
            synchronized (_lock)
            {
                MapRemoveObjects(_processMap, map);
                _applicationMap->addObjects(map);
                _sync = true;
            }
            break;
        case Factory:
            MapRemoveObjects(_threadMap(), map);
            synchronized (_lock)
            {
                MapRemoveObjects(_processMap, map);
                MapRemoveObjects(_applicationMap, map);
                _sync = true;
                _factoryMap->addObjects(map);
            }
            break;
        default:
            break;
        }
    else
        switch (kind)
        {
        case Thread:
            _threadMap()->addObjects(map);
            break;
        case Process:
            synchronized (_lock)
                _processMap->addObjects(map);
            break;
        case Application:
            synchronized (_lock)
            {
                _applicationMap->addObjects(map);
                _sync = true;
            }
            break;
        case Factory:
            synchronized (_lock)
                _factoryMap->addObjects(map);
            break;
        default:
            break;
        }
}
void Environment::addObjects(const char *path, unsigned kind)
{
    Object *obj = ObjectReadNative(path);
    if (Map *map = dynamic_cast<Map *>(obj))
        addObjects(map, kind);
}
void Environment::markDirty(unsigned kind)
{
    unsigned override = kind & Override;
    kind &= ~Override;
    if (override)
        switch (kind)
        {
        case Application:
        case Factory:
            synchronized (_lock)
                _sync = true;
            break;
        default:
            break;
        }
    else
        switch (kind)
        {
        case Application:
            synchronized (_lock)
                _sync = true;
            break;
        default:
            break;
        }
}
bool Environment::commit()
{
    Map *map;
    synchronized (_lock)
    {
        if (!_sync)
            return true;
        _sync = false;
        map = new Map;
        map->addObjects(_applicationMap);
    }
    bool result = ObjectWriteNative(_path, map);
    map->release();
    return result;
}

}
}
