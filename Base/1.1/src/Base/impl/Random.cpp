/*
 * Base/impl/Random.cpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#include <Base/impl/Random.hpp>
#include <Base/impl/BitHacks.hpp>
#include <Base/impl/Thread.hpp>
extern "C" {
#if ULONG_MAX > 4294967295
#include <tinymt64.h>
#else
#include <tinymt32.h>
#endif
}
#if defined(BASE_CONFIG_WINDOWS)
#include <windows.h>
#elif defined(BASE_CONFIG_POSIX)
#include <sys/time.h>
#endif

namespace Base {
namespace impl {

/*
 * Uses the Tiny Mersenne Twister:
 * http://www.math.sci.hiroshima-u.ac.jp/~m-mat/MT/TINYMT/index.html
 */

static Random *GetThreadRandom()
{
    Thread *thread = Thread::currentThread();
    Random *random = (Random *)thread->_slotObject(Thread::_SlotObjectRandom);
    if (0 == random)
    {
        random = new Random;
        thread->_setSlotObject(Thread::_SlotObjectRandom, random);
    }
    return random;
}

unsigned long Random::integerValue()
{
    return GetThreadRandom()->nextIntegerValue();
}
unsigned long Random::integerValue(unsigned long le, unsigned long gt)
{
    return GetThreadRandom()->nextIntegerValue(le, gt);
}
double Random::realValue()
{
    return GetThreadRandom()->nextRealValue();
}
Random::Random()
{
    unsigned long seed;
#if defined(BASE_CONFIG_WINDOWS)
    FILETIME ft;
    GetSystemTimeAsFileTime(&ft);
    seed =
        bit_rol((unsigned long)ft.dwHighDateTime, (sizeof(unsigned long) * CHAR_BIT) / 2) ^
        (unsigned long)ft.dwLowDateTime ^
        bit_rol((unsigned long)Thread::currentThread(), (sizeof(unsigned long) * CHAR_BIT) / 4);
#elif defined(BASE_CONFIG_POSIX)
    struct timeval tv;
    gettimeofday(&tv, 0);
    seed =
        bit_rol((unsigned long)tv.tv_sec, (sizeof(unsigned long) * CHAR_BIT) / 2) ^
        (unsigned long)tv.tv_usec ^
        bit_rol((unsigned long)Thread::currentThread(), (sizeof(unsigned long) * CHAR_BIT) / 4);
#else
    time_t unixTime = time(0);
    seed =
        bit_rol((unsigned long)unixTime, (sizeof(unsigned long) * CHAR_BIT) / 2) ^
        (unsigned long)Thread::currentThread();
#endif
    _init(seed);
}
Random::Random(unsigned long seed)
{
    _init(seed);
}
unsigned long Random::nextIntegerValue()
{
#if ULONG_MAX > 4294967295
    tinymt64_t *tinymt = (tinymt64_t *)&_state;
    return tinymt64_generate_uint64(tinymt);
#else
    tinymt32_t *tinymt = (tinymt32_t *)&_state;
    return tinymt32_generate_uint32(tinymt);
#endif
}
unsigned long Random::nextIntegerValue(unsigned long le, unsigned long gt)
{
#if ULONG_MAX > 4294967295
    tinymt64_t *tinymt = (tinymt64_t *)&_state;
    return le + tinymt64_generate_uint64(tinymt) % (gt - le);
#else
    tinymt32_t *tinymt = (tinymt32_t *)&_state;
    return le + tinymt32_generate_uint32(tinymt) % (gt - le);
#endif
}
double Random::nextRealValue()
{
#if ULONG_MAX > 4294967295
    tinymt64_t *tinymt = (tinymt64_t *)&_state;
    return tinymt64_generate_double(tinymt);
#else
    tinymt32_t *tinymt = (tinymt32_t *)&_state;
    return tinymt32_generate_32double(tinymt);
#endif
}
void Random::_init(unsigned long seed)
{
#if ULONG_MAX > 4294967295
    BASE_COMPILE_ASSERT(sizeof(DUP_TINYMT64_T) == sizeof(tinymt64_t));
    tinymt64_t *tinymt = (tinymt64_t *)&_state;
    /* values below same as those used for testing of check64.c */
    tinymt->mat1 = 0xfa051f40;
    tinymt->mat2 = 0xffd0fff4;
    tinymt->tmat = 0x58d02ffeffbfffbc;
    tinymt64_init(tinymt, seed);
#else
    BASE_COMPILE_ASSERT(sizeof(DUP_TINYMT32_T) == sizeof(tinymt32_t));
    tinymt32_t *tinymt = (tinymt32_t *)&_state;
    /* values below same as those used for testing of check32.c */
    tinymt->mat1 = 0x8f7011ee;
    tinymt->mat2 = 0xfc78ff1f;
    tinymt->tmat = 0x3793fdff;
    tinymt32_init(tinymt, seed);
#endif
}

}
}
