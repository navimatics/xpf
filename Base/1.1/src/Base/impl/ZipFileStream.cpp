/*
 * Base/impl/ZipFileStream.cpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#include <Base/impl/ZipFileStream.hpp>
#include <unzip.h>
#include <zip.h>

namespace Base {
namespace impl {

ZipFileStream *ZipFileStream::create(const char *path, const char *filename, const char *mode)
{
    int m = 0;
    for (const char *modep = mode; *modep; modep++)
    {
        char c = *modep;
        switch (c)
        {
        case 'r':
        case 'w':
            m = c;
            break;
        case 'b':
        default:
            break;
        }
    }
    void *file = 0;
    if ('r' == m)
    {
        file = unzOpen(path);
        if (0 != file)
            if (UNZ_OK != unzLocateFile(file, filename, 0) || UNZ_OK != unzOpenCurrentFile(file))
            {
                unzClose(file);
                file = 0;
            }
    }
    else if ('w' == m)
    {
        file = zipOpen(path, APPEND_STATUS_ADDINZIP);
        if (0 == file)
            file = zipOpen(path, APPEND_STATUS_CREATE);
        if (0 != file)
            if (ZIP_OK != zipOpenNewFileInZip(file, filename, 0, 0, 0, 0, 0, 0, Z_DEFLATED, 9))
            {
                zipClose(file, 0);
                file = 0;
            }
    }
    if (0 == file)
        return 0;
    return new ZipFileStream(file, mode);
}
ZipFileStream::ZipFileStream(void *file, const char *mode)
{
    int m = 0;
    for (const char *modep = mode; *modep; modep++)
    {
        char c = *modep;
        switch (c)
        {
        case 'r':
        case 'w':
            m = c;
            break;
        case 'b':
        default:
            break;
        }
    }
    _file = file;
    _mode = m;
}
ZipFileStream::~ZipFileStream()
{
    close();
}
ssize_t ZipFileStream::read(void *buf, size_t size)
{
    if (0 == _file)
        return -1;
    if ('r' != _mode)
        return -1;
    ssize_t bytes = unzReadCurrentFile(_file, buf, size);
    if (0 > bytes)
        return -1;
    return bytes;
}
ssize_t ZipFileStream::write(const void *buf, size_t size)
{
    if (0 == _file)
        return -1;
    if ('w' != _mode)
        return -1;
    int err = zipWriteInFileInZip(_file, buf, size);
    if (ZIP_OK != err)
        return -1;
    return size;
}
ssize_t ZipFileStream::seek(ssize_t offset, int whence)
{
    if (0 == _file)
        return -1;
    if ('r' != _mode)
        return -1;
    return unzSeekCurrentFile(_file, offset, whence);
}
ssize_t ZipFileStream::close()
{
    if (0 == _file)
        return -1;
    if ('r' == _mode)
    {
        int result0 = unzCloseCurrentFile(_file);
        int result1 = unzClose(_file);
        _file = 0;
        return UNZ_OK != result0 || UNZ_OK != result1 ? -1 : 0;
    }
    if ('w' == _mode)
    {
        int result0 = zipCloseFileInZip(_file);
        int result1 = zipClose(_file, 0);
        _file = 0;
        return ZIP_OK != result0 || ZIP_OK != result1 ? -1 : 0;
    }
    return -1;
}

}
}
