package com.navimatics.base;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import org.junit.Test;

public class PeerMappingStressTest
{
    @Test
    public void test()
    {
        final int nthreads = 100;
        final int nobjects = 10000;
        final NativeArray array = new NativeArray();
        final Random random = new Random();
        for (int i = 0; i < nobjects; i++)
            array.add(new NativeObject());
        System.gc();
        List<Thread> threads = new ArrayList();
        for (int i = 0; i < nthreads; i++)
            threads.add(new Thread(new Runnable()
            {
                public void run()
                {
                    int gcindex;
                    synchronized (random)
                    {
                        gcindex = random.nextInt(nobjects);
                    }
                    NativeArray local = new NativeArray();
                    for (int i = 0; i < nobjects; i++)
                    {
                        /* We should really protect access to the NativeArray using synchronized.
                         * However we do not modify the NativeArray so this is OK and it allows
                         * us to stress test the peer mapping implementation.
                         */
                        local.add(array.get(i));
                        if (i == gcindex)
                            System.gc();
                    }
                }
            }));
        for (Thread t : threads)
            t.start();
        for (Thread t : threads)
            for (;;)
                try
                {
                    t.join();
                    break;
                }
                catch (InterruptedException ex)
                {
                }
    }
}
