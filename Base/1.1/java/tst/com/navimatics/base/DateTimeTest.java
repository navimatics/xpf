package com.navimatics.base;

import static org.junit.Assert.*;
import org.junit.Test;

public class DateTimeTest
{
    @Test
    public void test()
    {
        double jd = (int)(new DateTime().julianDate() + 0.5);

        DateTime dt0 = new DateTime(jd);
        DateTime.Components c0 = dt0.components();
        double jd0 = (int)(dt0.julianDate() + 0.5);

        DateTime dt1 = new DateTime(c0);
        DateTime.Components c1 = dt1.components();
        double jd1 = (int)(dt1.julianDate() + 0.5);
        assertComponentsEqual(c0, c1);
        assertEquals(jd0, jd1, 0.1);

        DateTime dt2 = new DateTime(jd0);
        DateTime.Components c2 = dt2.components();
        double jd2 = (int)(dt2.julianDate() + 0.5);
        assertComponentsEqual(c0, c2);
        assertEquals(jd0, jd2, 0.1);

        DateTime dt3 = new DateTime(c0.year, c0.month, c0.day, c0.hour, c0.min, c0.sec, c0.msec);
        DateTime.Components c3 = dt3.components();
        double jd3 = (int)(dt3.julianDate() + 0.5);
        assertComponentsEqual(c0, c3);
        assertEquals(jd0, jd3, 0.1);
    }
    private void assertComponentsEqual(DateTime.Components a, DateTime.Components b)
    {
        assertEquals(a.year, b.year);
        assertEquals(a.month, b.month);
        assertEquals(a.day, b.day);
        assertEquals(a.hour, b.hour);
        assertEquals(a.min, b.min);
        assertEquals(a.sec, b.sec);
        assertEquals(a.msec, b.msec);
        assertEquals(a.dayOfWeek, b.dayOfWeek);
    }
}
