package com.navimatics.base;

import static org.junit.Assert.*;
import org.junit.Test;

public class NativeObjectTest
{
    @Test
    public void testNativeObject()
    {
        new NativeObject();
        new NativeObject().dispose();
        System.gc();
    }
    @Test
    public void testHashCode()
    {
        Object obj = new NativeObject();
        assertFalse(0 == obj.hashCode());
    }
    @Test
    public void testEqualsObject()
    {
        Object obj1 = new NativeObject();
        Object obj2 = new NativeObject();
        assertEquals(obj1, obj1);
        assertEquals(obj2, obj2);
        assertFalse(obj1.equals(null));
        assertFalse(obj1.equals(obj2));
        assertFalse(obj1.equals(new Object()));
        assertFalse(new Object().equals(obj1));
    }
    @Test
    public void testToString()
    {
        Object obj = new NativeObject();
        assertNotNull(obj.toString());
    }
}
