package com.navimatics.base;

import static org.junit.Assert.*;
import java.util.Collection;
import java.util.Map;
import java.util.Set;
import org.junit.Test;

public class NativeMapTest
{
    @Test
    public void testNativeMap()
    {
        new NativeMap();
        System.gc();
    }
    @Test
    public void testNativeMapMapOfQextendsNativeObjectQextendsNativeObject()
    {
        // TODO
    }
    @Test
    public void testClear()
    {
        NativeObject obj0 = new NativeObject();
        NativeObject obj1 = new NativeObject();
        Map m = new NativeMap();
        m.put(obj0, obj0);
        m.put(obj1, obj1);
        assertEquals(2, m.size());
        m.clear();
        assertEquals(0, m.size());
    }
    @Test
    public void testContainsKey()
    {
        NativeObject obj0 = new NativeObject();
        NativeObject obj1 = new NativeObject();
        NativeObject obj2 = new NativeObject();
        Map m = new NativeMap();
        try
        {
            m.containsKey(null);
            fail();
        }
        catch (NullPointerException ex)
        {
        }
        m.put(obj0, obj0);
        m.put(obj1, obj1);
        m.put(obj2, null);
        assertEquals(3, m.size());
        assertFalse(m.containsKey(new Object()));
        assertFalse(m.containsKey(new NativeObject()));
        assertTrue(m.containsKey(obj0));
        assertTrue(m.containsKey(obj1));
        assertTrue(m.containsKey(obj2));
        m.put(obj0, obj1);
        assertTrue(m.containsKey(obj0));
    }
    @Test
    public void testContainsValue()
    {
        // TODO
    }
    @Test
    public void testEntrySet()
    {
        // TODO
    }
    @Test
    public void testGet()
    {
        NativeObject obj0 = new NativeObject();
        NativeObject obj1 = new NativeObject();
        NativeObject obj2 = new NativeObject();
        Map m = new NativeMap();
        try
        {
            m.get(null);
            fail();
        }
        catch (NullPointerException ex)
        {
        }
        m.put(obj0, obj0);
        m.put(obj1, obj1);
        m.put(obj2, null);
        assertEquals(3, m.size());
        assertEquals(null, m.get(new Object()));
        assertEquals(null, m.get(new NativeObject()));
        assertEquals(obj0, m.get(obj0));
        assertEquals(obj1, m.get(obj1));
        assertEquals(null, m.get(obj2));
        m.put(obj0, obj1);
        assertEquals(obj1, m.get(obj0));
    }
    @Test
    public void testIsEmpty()
    {
        // TODO
    }
    @Test
    public void testKeySet()
    {
        NativeObject obj0 = new NativeObject();
        NativeObject obj1 = new NativeObject();
        NativeObject obj2 = new NativeObject();
        Map m = new NativeMap();
        m.put(obj0, obj0);
        m.put(obj1, obj1);
        m.put(obj2, null);
        Set s = m.keySet();
        assertEquals(3, s.size());
        assertTrue(s.contains(obj0));
        assertTrue(s.contains(obj1));
        assertTrue(s.contains(obj2));
    }
    @Test
    public void testPut()
    {
        NativeObject obj0 = new NativeObject();
        NativeObject obj1 = new NativeObject();
        NativeObject obj2 = new NativeObject();
        Map m = new NativeMap();
        try
        {
            m.put(null, obj0);
            fail();
        }
        catch (NullPointerException ex)
        {
        }
        assertEquals(0, m.size());
        assertEquals(null, m.put(obj0, obj0));
        assertEquals(null, m.put(obj1, obj1));
        assertEquals(null, m.put(obj2, null));
        assertEquals(3, m.size());
        assertEquals(null, m.get(new Object()));
        assertEquals(null, m.get(new NativeObject()));
        assertEquals(obj0, m.get(obj0));
        assertEquals(obj1, m.get(obj1));
        assertEquals(null, m.get(obj2));
        assertEquals(obj0, m.put(obj0, obj1));
        assertEquals(obj1, m.get(obj0));
    }
    @Test
    public void testPutAll()
    {
        // TODO
    }
    @Test
    public void testRemove()
    {
        NativeObject obj0 = new NativeObject();
        NativeObject obj1 = new NativeObject();
        NativeObject obj2 = new NativeObject();
        Map m = new NativeMap();
        try
        {
            m.remove(null);
            fail();
        }
        catch (NullPointerException ex)
        {
        }
        m.put(obj0, obj0);
        m.put(obj1, obj1);
        m.put(obj2, null);
        assertEquals(3, m.size());
        assertEquals(null, m.remove(new Object()));
        assertEquals(null, m.remove(new NativeObject()));
        assertEquals(obj0, m.remove(obj0));
        assertEquals(obj1, m.remove(obj1));
        assertEquals(null, m.remove(obj2));
        assertEquals(0, m.size());
        m.put(obj0, obj1);
        assertEquals(obj1, m.remove(obj0));
        assertEquals(0, m.size());
    }
    @Test
    public void testSize()
    {
        NativeObject obj0 = new NativeObject();
        NativeObject obj1 = new NativeObject();
        Map m = new NativeMap();
        assertEquals(0, m.size());
        m.put(obj0, obj0);
        m.put(obj1, obj1);
        assertEquals(2, m.size());
    }
    @Test
    public void testValues()
    {
        NativeObject obj0 = new NativeObject();
        NativeObject obj1 = new NativeObject();
        NativeObject obj2 = new NativeObject();
        Map m = new NativeMap();
        m.put(obj0, obj0);
        m.put(obj1, obj1);
        m.put(obj2, null);
        Collection c = m.values();
        assertEquals(3, c.size());
        assertTrue(c.contains(obj0));
        assertTrue(c.contains(obj1));
        assertTrue(c.contains(null));
    }
}
