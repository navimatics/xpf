package com.navimatics.base;

import static org.junit.Assert.*;
import java.util.List;
import org.junit.Test;

public class NativeStringTest
{
    @Test
    public void testNativeString()
    {
        try
        {
            NativeString.create(null);
        }
        catch (NullPointerException ex)
        {
        }
        NativeString s = NativeString.create("test123");
        assertEquals("test123", s.toString());
        NativeString s1 = NativeString.create("");
        NativeString s2 = NativeString.create("");
        assertEquals(System.identityHashCode(s1), System.identityHashCode(s2));
    }
    @Test
    public void testPhantomStrings()
    {
        NativeString str0 = NativeString.create("zero");
        NativeString str1 = NativeString.create("one");
        int id0 = System.identityHashCode(str0);
        int id1 = System.identityHashCode(str1);
        List l = new NativeArray();
        l.add(str0);
        l.add(str1);
        str0 = null;
        str1 = null;
        System.gc();
        str0 = (NativeString)l.get(0);
        str1 = (NativeString)l.get(1);
        assertEquals("zero", str0.toString());
        assertEquals("one", str1.toString());
        /* the following may fail if GC/finalization does not happen in time */
        assertFalse(id0 == System.identityHashCode(str0));
        assertFalse(id1 == System.identityHashCode(str1));
    }
}
