/*
 * com/navimatics/base/Defines.java
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

package com.navimatics.base;

public class Defines
{
    public static int integerNone()
    {
        return Integer.MIN_VALUE;
    }
    public static long longNone()
    {
        return Long.MIN_VALUE;
    }
    public static float floatNone()
    {
        return Float.NaN;
    }
    public static double doubleNone()
    {
        return Double.NaN;
    }
    public static boolean isNone(int value)
    {
        return Integer.MIN_VALUE == value;
    }
    public static boolean isNone(long value)
    {
        return Long.MIN_VALUE == value;
    }
    public static boolean isNone(float value)
    {
        return Float.isNaN(value);
    }
    public static boolean isNone(double value)
    {
        return Double.isNaN(value);
    }
}
