/*
 * com/navimatics/base/NativeSet.java
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

package com.navimatics.base;

import java.util.Collection;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.Set;

public class NativeSet<E extends NativeObject> extends NativeObject implements Set<E>
{
    public NativeSet()
    {
        super(skip_ctor);
        _ctor();
    }
    public NativeSet(Collection<? extends E> collection)
    {
        super(skip_ctor);
        _ctor();
        for (E obj : collection)
            add(obj);
    }
    protected NativeSet(skip_ctor_t s)
    {
        super(s);
    }
    public native boolean add(E obj);
    public boolean addAll(Collection<? extends E> collection)
    {
        boolean result = false;
        for (E obj : collection)
        {
            result = true;
            add(obj);
        }
        return result;
    }
    public native void clear();
    public native boolean contains(Object obj);
    public boolean containsAll(Collection<?> collection)
    {
        for (Object obj : collection)
            if (!contains(obj))
                return false;
        return true;
    }
    public boolean isEmpty()
    {
        return 0 == size();
    }
    public Iterator<E> iterator()
    {
        return new _Iterator<E>(this);
    }
    public native boolean remove(Object obj);
    public boolean removeAll(Collection<?> collection)
    {
        boolean result = false;
        for (Object obj : collection)
        {
            result = true;
            remove(obj);
        }
        return result;
    }
    public boolean retainAll(Collection<?> collection)
    {
        boolean result = false;
        Object[] array = _toArray(null, Object.class);
        for (Object obj : array)
        {
            if (!collection.contains(obj))
            {
                result = true;
                remove(obj);
            }
        }
        return result;
    }
    public native int size();
    public Object[] toArray()
    {
        return _toArray(null, Object.class);
    }
    public <T> T[] toArray(T[] array)
    {
        return (T[])_toArray(array, array.getClass().getComponentType());
    }
    private native Object[] _toArray(Object[] array, Class elemCls);
    private native void _ctor();
    private static class _Iterator<E extends NativeObject> implements Iterator<E>
    {
        public _Iterator(NativeSet<E> set)
        {
            _array = (E[])set._toArray(null, NativeObject.class);
            _index = 0;
        }
        public boolean hasNext()
        {
            return _array.length > _index;
        }
        public E next()
        {
            if (_array.length > _index)
                return _array[_index++];
            else
                throw new NoSuchElementException();
        }
        public void remove()
        {
            throw new UnsupportedOperationException();
        }
        private E[] _array;
        private int _index;
    }
}
