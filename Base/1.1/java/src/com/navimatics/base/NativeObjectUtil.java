/*
 * com/navimatics/base/NativeObjectUtil.java
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

package com.navimatics.base;

public class NativeObjectUtil
{
    public static native NativeObject readNative(String path);
    public static native void writeNative(String path, NativeObject obj);
    public static native NativeObject readJson(String path);
    public static native void writeJson(String path, NativeObject obj);
}
