/*
 * This fixes an issue with Crystax NDK version r7-crystax-5.beta2 and Android 4.2
 *     http://www.crystax.net/trac/ndk/ticket/112
 */

#warning "Crystax NDK version r7-crystax-5.beta2 ticket 112 fix. Do not use with other versions."
void __exidx_start() {}
void __exidx_end() {}
