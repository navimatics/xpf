/*
 * Base/Coroutine.hpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */
/**
 * Simple coroutines
 * Inspired by http://www.chiark.greenend.org.uk/~sgtatham/coroutines.html
 *
 * The following macros provide very rudimentary support for building coroutines.
 * They are primarily used to implement the Iterable interface.
 *
 * Macros
 * All macros take a "state" parameter that must be a struct pointer that has a "lineno" integer
 * field. This field must be initialized to 0 on initial entry to a coroutine block.
 *     coroblock(state)
 *         This macro introduces a coroutine statement block { ... } where the "yield" statement
 *         can be used. There can only be one such block within a function.
 *     coroyield(state)
 *         This macro exits the current coroutine statement block. The coroutine block can be
 *         reentered in which case execution will continue after the "yield" statement. It is
 *         an error to use "yield" outside of a coroutine block.
 *     corofini(state)
 *         This macro exits the current coroutine statement block and marks it as "finished".
 *         If the coroutine block is reentered it exits immediately. It is an error to use "fini"
 *         outside of a coroutine block.
 */

#ifndef BASE_COROUTINE_HPP_INCLUDED
#define BASE_COROUTINE_HPP_INCLUDED

#include <Base/Defines.hpp>
#include <Base/impl/Coroutine.hpp>

namespace Base {
}

#endif // BASE_COROUTINE_HPP_INCLUDED
