/*
 * Base/Value.hpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef BASE_VALUE_HPP_INCLUDED
#define BASE_VALUE_HPP_INCLUDED

#include <Base/Defines.hpp>
#include <Base/Object.hpp>
#include <Base/impl/Value.hpp>

namespace Base {

using Base::impl::Value;

}

#endif // BASE_VALUE_HPP_INCLUDED
