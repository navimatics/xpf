/*
 * Base/ObjectCodec.hpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef BASE_OBJECTCODEC_HPP_INCLUDED
#define BASE_OBJECTCODEC_HPP_INCLUDED

#include <Base/Defines.hpp>
#include <Base/Object.hpp>
#include <Base/impl/ObjectCodec.hpp>

namespace Base {

using Base::impl::ObjectCodec;

}

#endif // BASE_OBJECTCODEC_HPP_INCLUDED
