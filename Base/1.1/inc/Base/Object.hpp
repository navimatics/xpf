/*
 * Base/Object.hpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */
/**
 * Definitions for the base class used throughout these libraries.
 *
 * Base::Object
 *     This is the base class for most other classes in these libraries.
 *
 *     It enables reference counting, autocollection, and comparison and hashing for placement in
 *     containers. The methods retain(), release() can be used to manage the reference count and
 *     the method autorelease() can be used to place a Base::Object into a collection pool. The
 *     method cmp() allows for comparison between two Base::Object's and the method hash() allows
 *     for a hash value to be computed; both methods are useful for placing an object inside a
 *     container.
 *
 *     An important concept for Base::Object's is collection pools and autocollections. Collection
 *     pools are areas of memory where pointers to live Base::Object's are maintained (retained).
 *     Once an object is retained in a pool, it will not be released until the whole pool is
 *     collected. Collection pools are thread-local and nested; a new pool is created with the
 *     Base::Object::Mark() method; an existing pool is collected with the Base::Object::Collect()
 *     method. There is also the mark_and_collect macro which makes use of collection pools easier
 *     and safer.
 *
 *         // create outer collection pool
 *         Base::Object::Mark();
 *         ...
 *         // create inner collection pool
 *         Base::Object::Mark();
 *         ...
 *         // empty and destroy inner collection pool; only objects in inner pool are released
 *         Base::Object::Collect();
 *         ...
 *         // empty and destroy outer collection pool
 *         Base::Object::Collect();
 *
 *         // create collection pool that is active within a scope using mark_and_collect
 *         mark_and_collect
 *         {
 *             ...
 *         }
 *
 *     When a Base::Object gets created it is not placed by default in a collection pool. One can
 *     place it in the current collection pool by using the autorelease() method. Another technique
 *     is to use a special form of the new operator which places the Base::Object in the current
 *     collection pool at creation time.
 *
 *         // create object and place it in the current collection pool
 *         Base::Object *obj = new Base::Object;
 *         obj->autorelease();
 *
 *         // create object and place it in the current collection pool in a single step
 *         Base::Object *obj = new (Base::collect) Base::Object;
 *
 *     Is is an error to try to place an object in a collection pool when none is active (the
 *     collection pool machinery logs an error in this case).
 *
 *     Base::Object are often one-to-one mapped to non-C++ objects. For example, there exists a
 *     one-to-one mapping between Base::Object's and Java objects. This enables use of
 *     Base::Object's from Java code with minimal work.
 */

#ifndef BASE_OBJECT_HPP_INCLUDED
#define BASE_OBJECT_HPP_INCLUDED

#include <Base/Defines.hpp>
#include <Base/impl/Object.hpp>

namespace Base {

using Base::impl::collect_t;
using Base::impl::collect;
using Base::impl::Object;

using Base::impl::obj_assign;

using Base::impl::Interface;

using Base::impl::ObjectComparer;
using Base::impl::Object_hash_t;
using Base::impl::Object_equal_t;
using Base::impl::Object_less_t;

}

#endif // BASE_OBJECT_HPP_INCLUDED
