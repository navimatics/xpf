/*
 * Base/Jvm.hpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef BASE_JVM_HPP_INCLUDED
#define BASE_JVM_HPP_INCLUDED

#include <Base/Defines.hpp>
#include <Base/CString.hpp>
#include <Base/Object.hpp>
#include <Base/impl/Jvm.hpp>

namespace Base {

/*
 * utility
 */
using Base::impl::Jvm;
using Base::impl::JvmEnvironment;
using Base::impl::JvmRegisterNativeMethods;
using Base::impl::JvmThrowException;
using Base::impl::JvmClassGlobalRef;
using Base::impl::JvmClassName;
using Base::impl::JvmCallToStringMethod;
using Base::impl::JvmJavaString;
using Base::impl::JvmCString;
using Base::impl::JvmFatal;

/*
 * peer mapping
 */
using Base::impl::JvmNativeField;
using Base::impl::JvmRegisterPeerTypes;
using Base::impl::JvmJavaObject;
using Base::impl::JvmNativeObject;
using Base::impl::JvmSetNativeObject;
using Base::impl::JvmJavaObjectBaseClass;
using Base::impl::JvmCreateStructMapping;
using Base::impl::JvmStructFromJavaObject;
using Base::impl::JvmJavaObjectFromStruct;

}

#endif // BASE_JVM_HPP_INCLUDED
