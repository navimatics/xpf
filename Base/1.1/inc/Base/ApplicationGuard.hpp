/*
 * Base/ApplicationGuard.hpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */
/**
 * Utility functions to ensure that only a single instance of an application/program runs.
 *
 * _ApplicationGuard()
 *     This function creates or opens a specified file and attempts to exclusively lock a single
 *     byte at a high offset (currently at 1 GiB). It returns true on success and false on failure.
 *     It can also return an stdio FILE pointer to the opened file.
 *
 *     If the function returns success the calling process is the "primary" instance of the
 *     application/program and can use the FILE pointer to write information (e.g. a process id).
 *     If the function returns failure the calling process is a "secondary" instance and can
 *     only use the FILE pointer to read information. Even if a primary instance of a program
 *     always writes to the FILE after a successful call to _ApplicationGuard(), a secondary
 *     instance may still find the file empty due to two instances launching at the same time. A
 *     secondary instance must be able to handle this situation gracefully.
 *
 *     It is the responsibility of the "primary" instance to remove the file when it exits. Even
 *     if the "primary" instance dies unexpectedly, the OS will clear any locks on the file thus
 *     allowing an instance that launches at a later time to become primary again.
 *
 *     The _ApplicationGuard() function may be called multiple times, but it must be called with
 *     different paths every time. Calling it more than once with the same path is unspecified
 *     behavior (this is due to semantic incompatibilities in the locking mechanisms used on
 *     different systems).
 *
 *     The full prototype of this function is as follows:
 *         bool _ApplicationGuard(const char *path, FILE **filep = 0);
 *
 * ApplicationGuard()
 *     This function creates or opens a specified file and acquires a lock on it as described
 *     in _ApplicationGuard() above. It takes the same arguments and has the same returns.
 *
 *     The differences between _ApplicationGuard() and ApplicationGuard() are the following:
 *         1. The ApplicationGuard() function must be called always with the same path argument.
 *            It is an error to make two calls to ApplicationGuard() with a different path argument.
 *         2. If ApplicationGuard() returns true (i.e. if it is the "primary" instance), the file
 *            supplied to it will be automatically removed at process exit (using atexit()),
 *            provided that the instance does not die unexpectedly.
 *
 *     The full prototype of this function is as follows:
 *         bool ApplicationGuard(const char *path, FILE **filep = 0);
 */

#ifndef BASE_APPLICATIONGUARD_HPP_INCLUDED
#define BASE_APPLICATIONGUARD_HPP_INCLUDED

#include <Base/Defines.hpp>
#include <Base/impl/ApplicationGuard.hpp>

namespace Base {

using Base::impl::_ApplicationGuard;
using Base::impl::ApplicationGuard;

}

#endif // BASE_APPLICATIONGUARD_HPP_INCLUDED
