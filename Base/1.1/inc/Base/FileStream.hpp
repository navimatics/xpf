/*
 * Base/FileStream.hpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef BASE_FILESTREAM_HPP_INCLUDED
#define BASE_FILESTREAM_HPP_INCLUDED

#include <Base/Defines.hpp>
#include <Base/Stream.hpp>
#include <Base/impl/FileStream.hpp>

namespace Base {

using Base::impl::FileStream;

}

#endif // BASE_FILESTREAM_HPP_INCLUDED
