/*
 * Base/hash_map.hpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef BASE_HASHMAP_HPP_INCLUDED
#define BASE_HASHMAP_HPP_INCLUDED

#include <Base/Defines.hpp>
#include <Base/impl/hash_map.hpp>

namespace Base {

using Base::impl::hash_map;

}

#endif // BASE_HASHMAP_HPP_INCLUDED
