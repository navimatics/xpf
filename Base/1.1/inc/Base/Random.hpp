/*
 * Base/Random.hpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef BASE_RANDOM_HPP_INCLUDED
#define BASE_RANDOM_HPP_INCLUDED

#include <Base/Defines.hpp>
#include <Base/Object.hpp>
#include <Base/impl/Random.hpp>

namespace Base {

using Base::impl::Random;

}

#endif // BASE_RANDOM_HPP_INCLUDED
