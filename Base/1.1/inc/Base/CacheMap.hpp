/*
 * Base/CacheMap.hpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef BASE_CACHEMAP_HPP_INCLUDED
#define BASE_CACHEMAP_HPP_INCLUDED

#include <Base/Defines.hpp>
#include <Base/Object.hpp>
#include <Base/impl/CacheMap.hpp>

namespace Base {

using Base::impl::CacheMap;
using Base::impl::GenericCacheMap;

}

#endif // BASE_CACHEMAP_HPP_INCLUDED
