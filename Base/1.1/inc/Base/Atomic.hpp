/*
 * Base/Atomic.hpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef BASE_ATOMIC_HPP_INCLUDED
#define BASE_ATOMIC_HPP_INCLUDED

#include <Base/impl/Defines.hpp>
#include <Base/impl/Atomic.hpp>

namespace Base {

using Base::impl::atomic_rmb;
using Base::impl::atomic_wmb;
using Base::impl::atomic_mb;

using Base::impl::atomic_inc;
using Base::impl::atomic_dec;

using Base::impl::atomic_cas;

using Base::impl::atomic_lock_acquire;
using Base::impl::atomic_lock_release;

}

#endif // BASE_ATOMIC_HPP_INCLUDED
