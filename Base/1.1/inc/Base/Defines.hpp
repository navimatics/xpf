/*
 * Base/Defines.hpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef BASE_DEFINES_HPP_INCLUDED
#define BASE_DEFINES_HPP_INCLUDED

#include <Base/impl/Defines.hpp>

namespace Base {

using Base::impl::index_t;
using Base::impl::sindex_t;

using Base::impl::wait_timeout_t;
using Base::impl::wait_timeout_infinite;

using Base::impl::isFinite;
using Base::impl::isInfinite;
using Base::impl::isNan;

using Base::impl::None;
using Base::impl::isNone;

using Base::impl::MemoryAllocationError;
using Base::impl::Malloc;
using Base::impl::Realloc;

}

#endif // BASE_DEFINES_HPP_INCLUDED
