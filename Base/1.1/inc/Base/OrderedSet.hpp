/*
 * Base/OrderedSet.hpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef BASE_ORDEREDSET_HPP_INCLUDED
#define BASE_ORDEREDSET_HPP_INCLUDED

#include <Base/Defines.hpp>
#include <Base/Object.hpp>
#include <Base/impl/OrderedSet.hpp>

namespace Base {

using Base::impl::OrderedSet;
using Base::impl::GenericOrderedSet;

}

#endif // BASE_ORDEREDSET_HPP_INCLUDED
