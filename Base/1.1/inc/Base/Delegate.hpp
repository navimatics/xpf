/*
 * Base/Delegate.hpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef BASE_DELEGATE_HPP_INCLUDED
#define BASE_DELEGATE_HPP_INCLUDED

#include <Base/Defines.hpp>
#include <Base/impl/Delegate.hpp>

namespace Base {

using Base::impl::DelegateData;
using Base::impl::Delegate;

}

#endif // BASE_DELEGATE_HPP_INCLUDED
