/*
 * Base/Environment.hpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef BASE_ENVIRONMENT_HPP_INCLUDED
#define BASE_ENVIRONMENT_HPP_INCLUDED

#include <Base/Defines.hpp>
#include <Base/Object.hpp>
#include <Base/impl/Environment.hpp>

namespace Base {

using Base::impl::Environment;

}

#endif // BASE_ENVIRONMENT_HPP_INCLUDED
