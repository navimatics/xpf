/*
 * Base/Error.hpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef BASE_ERROR_HPP_INCLUDED
#define BASE_ERROR_HPP_INCLUDED

#include <Base/Defines.hpp>
#include <Base/CString.hpp>
#include <Base/Object.hpp>
#include <Base/impl/Error.hpp>

namespace Base {

using Base::impl::Error;

}

#endif // BASE_ERROR_HPP_INCLUDED
