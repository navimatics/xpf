/*
 * Base/IndexSet.hpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef BASE_INDEXSET_HPP_INCLUDED
#define BASE_INDEXSET_HPP_INCLUDED

#include <Base/Defines.hpp>
#include <Base/Object.hpp>
#include <Base/impl/IndexSet.hpp>

namespace Base {

using Base::impl::IndexSet;

}

#endif // BASE_INDEXSET_HPP_INCLUDED
