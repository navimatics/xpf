/*
 * Base/Synchronization.hpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */
/**
 * Definitions for synchronization and one-time initialization of resources.
 *
 * Base::Lock
 *     This class provides a locking mechanism that is useful for mutual exclusion.
 *     It provides two methods: lock() and unlock(). These methods can be used directly to provide
 *     single thread access (mutual exclusion) to a particular resource, but they are easier to use
 *     indirectly through the synchronized macro.
 *
 * synchronized
 *     This macro makes use of Base::Lock's easier and safer. It protects a scope with a call to
 *     lock() before entering it and a call to unlock() immediately after leaving it.
 *
 *         Base::Lock *lock = ...;
 *         synchronized (lock)
 *         {
 *             // mutually exclusive code
 *             ...
 *         }
 *
 * execute_once
 *     This macro is used for one-time initialization of resources. The protected scope will
 *     execute exactly once, the very first time the scope is reached, even in the presence of
 *     multiple threads. This can be useful to lazy initialize expensive resources in a thread-
 *     safe manner.
 *
 *         static ExpensiveResource *resource;
 *         execute_once
 *         {
 *             // initialize resource
 *             ...
 *         }
 *         // use resource
 *         ...
 */

#ifndef BASE_SYNCHRONIZATION_HPP_INCLUDED
#define BASE_SYNCHRONIZATION_HPP_INCLUDED

#include <Base/Defines.hpp>
#include <Base/Object.hpp>
#include <Base/impl/Synchronization.hpp>

namespace Base {

using Base::impl::Lock;
using Base::impl::ReadWriteLock;
using Base::impl::Semaphore;
using Base::impl::ConditionVariable;

}

#endif // BASE_SYNCHRONIZATION_HPP_INCLUDED
