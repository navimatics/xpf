/*
 * Base/TextParser.hpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef BASE_TEXTPARSER_HPP_INCLUDED
#define BASE_TEXTPARSER_HPP_INCLUDED

#include <Base/Defines.hpp>
#include <Base/Object.hpp>
#include <Base/Stream.hpp>
#include <Base/impl/TextParser.hpp>

namespace Base {

using Base::impl::TextParser;

}

#endif // BASE_TEXTPARSER_HPP_INCLUDED
