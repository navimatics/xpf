/*
 * Base/Thread.hpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */
/**
 * Definitions for a cross-platform thread class.
 *
 * Base::Thread
 *     This class encapsulates a thread of execution.
 *
 *     Every system thread has a Base::Thread object attached to it. This includes the main program
 *     thread. To get the current Base::Thread object use the static currentThread() method.
 *
 *     Every system thread has also some storage attached to it (thread local storage). To access
 *     this storage use the currentThreadMap() method. This returns a Base::Map object that can be
 *     used to store thread-local objects under specific keys.
 *
 *     To exit the current thread of execution you can use the static exit() method. This method
 *     will exit the thread immediately without any stack unwinding, etc. Use with caution.
 *
 *     There are two ways to create a new thread: 1) You can create a subclass of Base::Thread and
 *     override the run() method, or 2) you can create a new Base::Thread object and supply a
 *     thread entry function and argument in the constructor call. When the thread starts either
 *     the overriden run() method or the thread entry function will be called, depending on how the
 *     Base::Thread object was created. Execution of the thread will not start until the start()
 *     method gets called.
 *
 *     The detach() method can be used to dissociate a Base::Thread() object from its corresponding
 *     system thread. After a detach() call a Base::Thread can no longer be used to manipulate the
 *     corresponding system thread. However the corresponding Base::Thread object will persist
 *     until the system thread finishes execution and it will be available through a call to the
 *     currentThread() method. Likewise the thread's local storage will persist as well.
 *
 *     The wait() method allows another thread to wait for the system thread associated with a
 *     Base::Thread object to finish execution. This is useful when wanting to ensure that the
 *     task performed by a particular thread is complete.
 *
 *     It should be noted that the start(), detach() and wait() methods must be called AT MOST ONCE
 *     per Base::Thread object. Calling any of these methods more than at most once for a particular
 *     Base::Object is a programmatic error. It is also a programmatic error to call the detach()
 *     or wait() methods from the newly started thread.
 *
 *     The quit() method sets a flag that instructs a particular thread that it should quit. A
 *     thread may test whether the flag is set by using the isQuitting() method. Please note that
 *     a thread may be unable to test this flag, either because it is executing a blocking system
 *     call or because of its design. Thus calling quit() on a thread is only a request and is not
 *     guaranteed to terminate the thread.
 */

#ifndef BASE_THREAD_HPP_INCLUDED
#define BASE_THREAD_HPP_INCLUDED

#include <Base/Defines.hpp>
#include <Base/Object.hpp>
#include <Base/impl/Thread.hpp>

namespace Base {

using Base::impl::Thread;

}

#endif // BASE_THREAD_HPP_INCLUDED
