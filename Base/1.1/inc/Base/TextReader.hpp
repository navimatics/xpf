/*
 * Base/TextReader.hpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef BASE_TEXTREADER_HPP_INCLUDED
#define BASE_TEXTREADER_HPP_INCLUDED

#include <Base/Defines.hpp>
#include <Base/Stream.hpp>
#include <Base/TextParser.hpp>
#include <Base/impl/TextReader.hpp>

namespace Base {

using Base::impl::TextReader;

}

#endif // BASE_TEXTREADER_HPP_INCLUDED
