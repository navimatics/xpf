/*
 * Base/StdioStream.hpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef BASE_STDIOSTREAM_HPP_INCLUDED
#define BASE_STDIOSTREAM_HPP_INCLUDED

#include <Base/Defines.hpp>
#include <Base/Stream.hpp>
#include <Base/impl/StdioStream.hpp>

namespace Base {

using Base::impl::StdioStream;

}

#endif // BASE_STDIOSTREAM_HPP_INCLUDED
