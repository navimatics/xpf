/*
 * Base/Stream.hpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef BASE_STREAM_HPP_INCLUDED
#define BASE_STREAM_HPP_INCLUDED

#include <Base/Defines.hpp>
#include <Base/Object.hpp>
#include <Base/impl/Stream.hpp>

namespace Base {

using Base::impl::Stream;

}

#endif // BASE_STREAM_HPP_INCLUDED
