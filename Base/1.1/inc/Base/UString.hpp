/*
 * Base/UString.hpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef BASE_USTRING_HPP_INCLUDED
#define BASE_USTRING_HPP_INCLUDED

#include <Base/Defines.hpp>
#include <Base/Object.hpp>
#include <Base/impl/UString.hpp>

namespace Base {

using Base::impl::unichar_t;
using Base::impl::UString;
using Base::impl::ustring;
using Base::impl::ustringWithCapacity;
using Base::impl::ustr_new;
using Base::impl::ustr_newWithCapacity;
using Base::impl::ustr_assign;
using Base::impl::ustr_assign_copy;
using Base::impl::ustr_obj;
using Base::impl::ustr_retain;
using Base::impl::ustr_release;
using Base::impl::ustr_autorelease;
using Base::impl::ustr_autocollect;
using Base::impl::ustr_length;
using Base::impl::ustr_slice;
using Base::impl::ustr_concat;

using Base::impl::ustr_from_cstr;
using Base::impl::cstr_from_ustr;

}

#endif // BASE_USTRING_HPP_INCLUDED
