/*
 * Base/hash_set.hpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef BASE_HASHSET_HPP_INCLUDED
#define BASE_HASHSET_HPP_INCLUDED

#include <Base/Defines.hpp>
#include <Base/impl/hash_set.hpp>

namespace Base {

using Base::impl::hash_set;

}

#endif // BASE_HASHSET_HPP_INCLUDED
