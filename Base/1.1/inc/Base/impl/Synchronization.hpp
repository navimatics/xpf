/*
 * Base/impl/Synchronization.hpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef BASE_IMPL_SYNCHRONIZATION_HPP_INCLUDED
#define BASE_IMPL_SYNCHRONIZATION_HPP_INCLUDED

#include <Base/impl/Defines.hpp>
#include <Base/impl/Object.hpp>
#include <Base/impl/Syn.hpp>

#define BASE_IMPL_synchronized(num, l)  \
    if (Base::impl::__LockGuard<Base::impl::Lock> BASE_CONCAT(lockGuard__, num) = l)\
        goto BASE_CONCAT(lockGoto__, num);\
    else\
        BASE_CONCAT(lockGoto__, num):
#if defined(BASE_CONFIG_MSVC)
#define synchronized(l)                 BASE_IMPL_synchronized(__COUNTER__, l)
#elif defined(BASE_CONFIG_GNUC)
#define synchronized(l)                 BASE_IMPL_synchronized(__LINE__, l)
#endif

#define BASE_IMPL_synchronized_read(num, l)  \
    if (Base::impl::__ReadLockGuard<Base::impl::Lock> BASE_CONCAT(lockGuard__, num) = l)\
        goto BASE_CONCAT(lockGoto__, num);\
    else\
        BASE_CONCAT(lockGoto__, num):
#if defined(BASE_CONFIG_MSVC)
#define synchronized_read(l)            BASE_IMPL_synchronized_read(__COUNTER__, l)
#elif defined(BASE_CONFIG_GNUC)
#define synchronized_read(l)            BASE_IMPL_synchronized_read(__LINE__, l)
#endif

#define BASE_IMPL_execute_once(num)     \
    static Base::impl::execute_once_control_t BASE_CONCAT(execute_once_control__, num) = 0;\
    if (BASE_CONCAT(execute_once_control__, num))\
        (void)0;\
    else if (!Base::impl::execute_once_enter(&BASE_CONCAT(execute_once_control__, num)))\
        (void)0;\
    else if (Base::impl::__ExecuteOnceLeaveGuard BASE_CONCAT(executeOnceLeaveGuard__, num) = &BASE_CONCAT(execute_once_control__, num))\
        (void)0;\
    else
#if defined(BASE_CONFIG_MSVC)
#define execute_once                    BASE_IMPL_execute_once(__COUNTER__)
#elif defined(BASE_CONFIG_GNUC)
#define execute_once                    BASE_IMPL_execute_once(__LINE__)
#endif

namespace Base {
namespace impl {

class BASE_APISYM Lock : public Object
{
public:
    Lock();
    ~Lock();
    void lock();
    void unlock();
private:
    friend class ConditionVariable;
    mutex_t _mutex;
};

class BASE_APISYM ReadWriteLock : public Object
{
public:
    ReadWriteLock(bool fair = false);
    ~ReadWriteLock();
    void lock(bool read = false);
    void unlock(bool read = false);
private:
    rwlock_t _rwlock;
};

class BASE_APISYM Semaphore : public Object
{
public:
    Semaphore(int count = 0);
    ~Semaphore();
    void wait();
    void notify();
private:
    semaphore_t _semaphore;
};

class BASE_APISYM ConditionVariable : public Object
{
public:
    ConditionVariable();
    ~ConditionVariable();
    bool wait(Lock *lock, wait_timeout_t timeout = wait_timeout_infinite);
    void notify();
    void notifyAll();
private:
    condvar_t _condvar;
};

template <typename L>
struct __LockGuard
{
    L *_l;
    operator bool()
    {
        return false;
    }
    __LockGuard(L *l) : _l(l)
    {
        if (0 != _l)
            _l->lock();
    }
    ~__LockGuard()
    {
        if (0 != _l)
            _l->unlock();
    }
};

template <typename L>
struct __ReadLockGuard
{
    L *_l;
    operator bool()
    {
        return false;
    }
    __ReadLockGuard(L *l) : _l(l)
    {
        if (0 != _l)
            _l->lock(true);
    }
    ~__ReadLockGuard()
    {
        if (0 != _l)
            _l->unlock(true);
    }
};

struct __ExecuteOnceLeaveGuard
{
    execute_once_control_t *_i;
    operator bool()
    {
        return false;
    }
    __ExecuteOnceLeaveGuard(execute_once_control_t *i) : _i(i)
    {
    }
    ~__ExecuteOnceLeaveGuard()
    {
        execute_once_leave(_i);
    }
};

}
}

#endif // BASE_IMPL_SYNCHRONIZATION_HPP_INCLUDED
