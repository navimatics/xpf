/*
 * Base/impl/IndexMap.hpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef BASE_IMPL_INDEXMAP_HPP_INCLUDED
#define BASE_IMPL_INDEXMAP_HPP_INCLUDED

#include <Base/impl/Defines.hpp>
#include <Base/impl/Iterable.hpp>
#include <Base/impl/Map.hpp>
#include <Base/impl/ObjectCodec.hpp>
#include <Base/impl/RadixTree.hpp>

namespace Base {
namespace impl {

class BASE_APISYM IndexMap : public RadixTree, public Iterable<index_t>
{
public:
    IndexMap(size_t capacity = 0);
    ~IndexMap();
    void autocollectObjects(bool ac);
    size_t count();
    Object *object(index_t index);
    bool getObject(index_t index, Object *&obj);
    void setObject(index_t index, Object *obj);
    void removeObject(index_t index);
    void removeAllObjects();
    void addObjects(IndexMap *map);
    index_t *indexes();
    Object **objects();
    void getIndexesAndObjects(index_t *&idxbuf, Object **&objbuf);
    const char *strrepr();
    /* Iterable<index_t> */
    void *iterate(void *state, index_t *bufp[2]);
private:
    size_t _count;
    bool _nac;
    /* ObjectCodec use */
    Map *__map();
    void __setMap(Map *map);
    ObjectCodecDeclare();
};

template <typename V>
class BASE_APISYM GenericIndexMap : public IndexMap
{
public:
    V *object(index_t index)
    {
        return (V *)inherited::object(index);
    }
    bool getObject(index_t index, V *&obj)
    {
        return inherited::getObject(index, (Object *&)obj);
    }
    void setObject(index_t index, V *obj)
    {
        inherited::setObject(index, obj);
    }
    void removeObject(index_t index)
    {
        inherited::removeObject(index);
    }
    void addObjects(GenericIndexMap<V> *map)
    {
        inherited::addObjects(map);
    }
    V **objects()
    {
        return (V **)inherited::objects();
    }
    void getIndexesAndObjects(index_t *&idxbuf, V **&objbuf)
    {
        inherited::getIndexesAndObjects(idxbuf, (Object **&)objbuf);
    }
private:
    typedef IndexMap inherited;
};

}
}

#endif // BASE_IMPL_INDEXMAP_HPP_INCLUDED
