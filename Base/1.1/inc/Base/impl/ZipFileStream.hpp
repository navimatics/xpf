/*
 * Base/impl/ZipFileStream.hpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef BASE_IMPL_ZIPFILESTREAM_HPP_INCLUDED
#define BASE_IMPL_ZIPFILESTREAM_HPP_INCLUDED

#include <Base/impl/Defines.hpp>
#include <Base/impl/Stream.hpp>

namespace Base {
namespace impl {

class BASE_APISYM ZipFileStream : public Stream
{
public:
    static ZipFileStream *create(const char *path, const char *filename, const char *mode);
        /* mode:
         * 'r': read mode; 'w': write mode
         * 'b': ignored
         */
    ZipFileStream(void *file, const char *mode);
    ~ZipFileStream();
    ssize_t read(void *buf, size_t size);
    ssize_t write(const void *buf, size_t size);
    ssize_t seek(ssize_t offset, int whence = SEEK_SET);
    ssize_t close();
    void *file();
private:
    void *_file;
    int _mode;
};

inline void *ZipFileStream::file()
{
    return _file;
}

}
}

#endif // BASE_IMPL_ZIPFILESTREAM_HPP_INCLUDED
