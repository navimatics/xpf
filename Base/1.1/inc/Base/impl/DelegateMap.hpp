/*
 * Base/impl/DelegateMap.hpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef BASE_IMPL_DELEGATEMAP_HPP_INCLUDED
#define BASE_IMPL_DELEGATEMAP_HPP_INCLUDED

#include <Base/impl/Defines.hpp>
#include <Base/impl/Delegate.hpp>
#include <Base/impl/Iterable.hpp>
#include <Base/impl/Object.hpp>
#include <Base/impl/Ref.hpp>
#include <Base/impl/hash_map.hpp>

namespace Base {
namespace impl {

class BASE_APISYM DelegateMap : public Object, public Iterable<Object *>
{
public:
    DelegateMap(ObjectComparer *comparer = 0);
    size_t count();
    DelegateData delegate(Object *key);
    bool getDelegate(Object *key, DelegateData &del);
    void setDelegate(Object *key, DelegateData del);
    void removeDelegate(Object *key);
    void removeAllDelegates();
    void addDelegates(DelegateMap *map);
    Object **keys();
    DelegateData *delegates();
    void getKeysAndDelegates(Object **&keybuf, DelegateData *&delbuf);
    const char *strrepr();
    /* Iterable<Object *> */
    void *iterate(void *state, Object **bufp[2]);
private:
    typedef hash_map<Ref<Object>, DelegateData, Object_hash_t, Object_equal_t> map_t;
    struct Iterator : public Object
    {
        Iterator(map_t &map) : p(map.begin()), q(map.end())
        {
        }
        map_t::iterator p, q;
    };
private:
    Ref<Object> _comparerRef;
    map_t _map;
};

template <typename K, typename Sig>
class BASE_APISYM GenericDelegateMap : public DelegateMap
{
public:
    Delegate<Sig> delegate(K *key)
    {
        DelegateData del = inherited::delegate(key);
        return *(Delegate<Sig> *)&del;
    }
    bool getDelegate(K *key, Delegate<Sig> &del)
    {
        return inherited::getDelegate(key, del);
    }
    void setDelegate(K *key, Delegate<Sig> del)
    {
        inherited::setDelegate(key, del);
    }
    void removeDelegate(K *key)
    {
        inherited::removeDelegate(key);
    }
    void addDelegates(GenericDelegateMap<K, Sig> *map)
    {
        inherited::addDelegates(map);
    }
    K **keys()
    {
        return (K **)inherited::keys();
    }
    Delegate<Sig> *delegates()
    {
        return (Delegate<Sig> *)inherited::delegates();
    }
    void getKeysAndDelegates(K **&keybuf, Delegate<Sig> *&delbuf)
    {
        inherited::getKeysAndDelegates((Object **&)keybuf, (DelegateData *&)delbuf);
    }
private:
    typedef DelegateMap inherited;
};

}
}

#endif // BASE_IMPL_DELEGATEMAP_HPP_INCLUDED
