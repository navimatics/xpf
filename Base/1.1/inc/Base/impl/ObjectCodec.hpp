/*
 * Base/impl/ObjectCodec.hpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef BASE_IMPL_OBJECTCODEC_HPP_INCLUDED
#define BASE_IMPL_OBJECTCODEC_HPP_INCLUDED

#include <Base/impl/Defines.hpp>
#include <Base/impl/CString.hpp>
#include <Base/impl/Object.hpp>

#define ObjectCodecDeclare()            \
    protected:\
        static Base::impl::Object *__objectCodecCreateObject();\
        static Base::impl::ObjectCodec *__objectCodecPtr, *__objectCodec()
#define ObjectCodecBeginWithCreator(type, create)\
    Base::impl::ObjectCodec *type::__objectCodec()\
    {\
        static Base::impl::ObjectCodec *codec = 0;\
        if (0 == codec)\
        {\
            codec = new Base::impl::ObjectCodec(typeid(type), 0, create);
#define ObjectCodecBeginExWithCreator(type, base, create)\
    Base::impl::ObjectCodec *type::__objectCodec()\
    {\
        static Base::impl::ObjectCodec *codec = 0;\
        if (0 == codec)\
        {\
            Base::impl::ObjectCodec *baseCodec = base::__objectCodec();\
            codec = new Base::impl::ObjectCodec(typeid(type), baseCodec, create);
#define ObjectCodecProperty(name, ...)  \
            codec->addPropertyDescr(name, __VA_ARGS__);
#define ObjectCodecPostCreateHandler(handler)  \
            codec->setPostCreateHandler(handler);
#define ObjectCodecEnd(type)\
            codec->linger();\
            Base::impl::ObjectCodec::registerObjectCodec(codec);\
        }\
        return codec;\
    }\
    Base::impl::ObjectCodec *type::__objectCodecPtr = type::__objectCodec();
#define ObjectCodecBegin(type)          \
    Base::impl::Object *type::__objectCodecCreateObject() { return new type; }\
    ObjectCodecBeginWithCreator(type, __objectCodecCreateObject)
#define ObjectCodecBeginEx(type, base)  \
    Base::impl::Object *type::__objectCodecCreateObject() { return new type; }\
    ObjectCodecBeginExWithCreator(type, base, __objectCodecCreateObject)

namespace Base {
namespace impl {

class BASE_APISYM ObjectCodec : public Object
{
public:
    struct PropertyDescr
    {
        const char *name;
        int kind;
        long (Object::*get)();
        void (Object::*set)(long);
        long Object::*p;
        union DefaultValue
        {
            Object *obj;
            const char *str;
            bool B;
            int i;
            long l;
            float f;
            double d;
        } deflt;
    };
public:
    /* property accessors */
    static Object *getObjectProperty(Object *obj, PropertyDescr *prop);
    static const char *getStringProperty(Object *obj, PropertyDescr *prop);
    static bool getBooleanProperty(Object *obj, PropertyDescr *prop);
    static long getIntegerProperty(Object *obj, PropertyDescr *prop);
    static double getRealProperty(Object *obj, PropertyDescr *prop);
    static void setObjectProperty(Object *obj, PropertyDescr *prop, Object *value);
    static void setStringProperty(Object *obj, PropertyDescr *prop, const char *value);
    static void setBooleanProperty(Object *obj, PropertyDescr *prop, bool value);
    static void setIntegerProperty(Object *obj, PropertyDescr *prop, long value);
    static void setRealProperty(Object *obj, PropertyDescr *prop, double value);
    /* codec registry */
    static void registerObjectCodec(ObjectCodec *codec);
    static ObjectCodec *getObjectCodecByName(CString *name);
    static ObjectCodec *getObjectCodec(Object *obj);
    /* create/destroy */
    ObjectCodec(const char *name, Object *(*create)());
    ObjectCodec(const char *name, ObjectCodec *baseCodec, Object *(*create)());
    ObjectCodec(const std::type_info &info, Object *(*create)());
    ObjectCodec(const std::type_info &info, ObjectCodec *baseCodec, Object *(*create)());
    ~ObjectCodec();
    /* codec description */
    const char *name();
    Object *createObject();
    void postCreateObject(Object *obj);
    PropertyDescr *getPropertyDescrs();
    PropertyDescr *getPropertyDescr(const char *name);
    template <typename T, typename V>
    void addPropertyDescr(const char *name, V *(T::*get)(), void (T::*set)(V *), V *deflt = 0);
    template <typename T, typename V>
    void addPropertyDescr(const char *name, V *T::*p, V *deflt = 0);
    template <typename T>
    void addPropertyDescr(const char *name, const char *(T::*get)(), void (T::*set)(const char *), const char *deflt = 0);
    template <typename T>
    void addPropertyDescr(const char *name, const char *T::*p, const char *deflt = 0);
    template <typename T>
    void addPropertyDescr(const char *name, bool (T::*get)(), void (T::*set)(bool), bool deflt = false);
    template <typename T>
    void addPropertyDescr(const char *name, bool T::*p, bool deflt = false);
    template <typename T>
    void addPropertyDescr(const char *name, int (T::*get)(), void (T::*set)(int), int deflt = 0);
    template <typename T>
    void addPropertyDescr(const char *name, int T::*p, int deflt = 0);
    template <typename T>
    void addPropertyDescr(const char *name, long (T::*get)(), void (T::*set)(long), long deflt = 0);
    template <typename T>
    void addPropertyDescr(const char *name, long T::*p, long deflt = 0);
    template <typename T>
    void addPropertyDescr(const char *name, float (T::*get)(), void (T::*set)(float), float deflt = 0);
    template <typename T>
    void addPropertyDescr(const char *name, float T::*p, float deflt = 0);
    template <typename T>
    void addPropertyDescr(const char *name, double (T::*get)(), void (T::*set)(double), double deflt = 0);
    template <typename T>
    void addPropertyDescr(const char *name, double T::*p, double deflt = 0);
    template <typename T>
    void setPostCreateHandler(void (T::* postCreate)());
private:
    void _addPropertyDescr(const char *name,
        int kind, long (Object::*get)(), void (Object::*set)(long),
        PropertyDescr::DefaultValue deflt);
    void _addPropertyDescr(const char *name,
        int kind, long Object::*p,
        PropertyDescr::DefaultValue deflt);
    void _init(const char *name, ObjectCodec *baseCodec, Object *(*create)());
private:
    const char *_name;
    Object *(*_create)();
    void (Object::* _postCreate)();
    PropertyDescr *_props;
        /* property descriptors are kept sorted by name to enable binary search lookup on them */
};

inline const char *ObjectCodec::name()
{
    return _name;
}
inline Object *ObjectCodec::createObject()
{
    return _create();
}
inline void ObjectCodec::postCreateObject(Object *obj)
{
    if (_postCreate)
        (obj->*_postCreate)();
}
inline ObjectCodec::PropertyDescr *ObjectCodec::getPropertyDescrs()
{
    return _props;
}

template <typename T, typename V>
inline void ObjectCodec::addPropertyDescr(const char *name, V *(T::*get)(), void (T::*set)(V *), V *deflt)
{
    PropertyDescr::DefaultValue u; u.obj = (Object *)deflt;
    _addPropertyDescr(name, '@', (long (Object::*)())get, (void (Object::*)(long))set, u);
}
template <typename T, typename V>
inline void ObjectCodec::addPropertyDescr(const char *name, V *T::*p, V *deflt)
{
    PropertyDescr::DefaultValue u; u.obj = (Object *)deflt;
    _addPropertyDescr(name, '@', (long Object::*)p, u);
}
template <typename T>
inline void ObjectCodec::addPropertyDescr(const char *name, const char *(T::*get)(), void (T::*set)(const char *), const char *deflt)
{
    PropertyDescr::DefaultValue u; u.str = deflt;
    _addPropertyDescr(name, '*', (long (Object::*)())get, (void (Object::*)(long))set, u);
}
template <typename T>
inline void ObjectCodec::addPropertyDescr(const char *name, const char *T::*p, const char *deflt)
{
    PropertyDescr::DefaultValue u; u.str = deflt;
    _addPropertyDescr(name, '*', (long Object::*)p, u);
}
template <typename T>
inline void ObjectCodec::addPropertyDescr(const char *name, bool (T::*get)(), void (T::*set)(bool), bool deflt)
{
    PropertyDescr::DefaultValue u; u.B = deflt;
    _addPropertyDescr(name, 'B', (long (Object::*)())get, (void (Object::*)(long))set, u);
}
template <typename T>
inline void ObjectCodec::addPropertyDescr(const char *name, bool T::*p, bool deflt)
{
    PropertyDescr::DefaultValue u; u.B = deflt;
    _addPropertyDescr(name, 'B', (long Object::*)p, u);
}
template <typename T>
inline void ObjectCodec::addPropertyDescr(const char *name, int (T::*get)(), void (T::*set)(int), int deflt)
{
    PropertyDescr::DefaultValue u; u.i = deflt;
    _addPropertyDescr(name, 'i', (long (Object::*)())get, (void (Object::*)(long))set, u);
}
template <typename T>
inline void ObjectCodec::addPropertyDescr(const char *name, int T::*p, int deflt)
{
    PropertyDescr::DefaultValue u; u.i = deflt;
    _addPropertyDescr(name, 'i', (long Object::*)p, u);
}
template <typename T>
inline void ObjectCodec::addPropertyDescr(const char *name, long (T::*get)(), void (T::*set)(long), long deflt)
{
    PropertyDescr::DefaultValue u; u.l = deflt;
    _addPropertyDescr(name, 'l', (long (Object::*)())get, (void (Object::*)(long))set, u);
}
template <typename T>
inline void ObjectCodec::addPropertyDescr(const char *name, long T::*p, long deflt)
{
    PropertyDescr::DefaultValue u; u.l = deflt;
    _addPropertyDescr(name, 'l', (long Object::*)p, u);
}
template <typename T>
inline void ObjectCodec::addPropertyDescr(const char *name, float (T::*get)(), void (T::*set)(float), float deflt)
{
    PropertyDescr::DefaultValue u; u.f = deflt;
    _addPropertyDescr(name, 'f', (long (Object::*)())get, (void (Object::*)(long))set, u);
}
template <typename T>
inline void ObjectCodec::addPropertyDescr(const char *name, float T::*p, float deflt)
{
    PropertyDescr::DefaultValue u; u.f = deflt;
    _addPropertyDescr(name, 'f', (long Object::*)p, u);
}
template <typename T>
inline void ObjectCodec::addPropertyDescr(const char *name, double (T::*get)(), void (T::*set)(double), double deflt)
{
    PropertyDescr::DefaultValue u; u.d = deflt;
    _addPropertyDescr(name, 'd', (long (Object::*)())get, (void (Object::*)(long))set, u);
}
template <typename T>
inline void ObjectCodec::addPropertyDescr(const char *name, double T::*p, double deflt)
{
    PropertyDescr::DefaultValue u; u.d = deflt;
    _addPropertyDescr(name, 'd', (long Object::*)p, u);
}
template <typename T>
inline void ObjectCodec::setPostCreateHandler(void (T::* postCreate)())
{
    _postCreate = (void (Object::*)())postCreate;
}

}
}

#endif // BASE_IMPL_OBJECTCODEC_HPP_INCLUDED
