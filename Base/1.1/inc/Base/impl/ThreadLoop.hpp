/*
 * Base/impl/ThreadLoop.hpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef BASE_IMPL_THREADLOOP_HPP_INCLUDED
#define BASE_IMPL_THREADLOOP_HPP_INCLUDED

#include <Base/impl/Defines.hpp>
#include <Base/impl/Delegate.hpp>
#include <Base/impl/Thread.hpp>

namespace Base {
namespace impl {

/* loop */
BASE_APISYM void ThreadLoopInit();
BASE_APISYM void ThreadLoopRun();
BASE_APISYM void ThreadLoopStop();

/* invoke */
BASE_APISYM bool ThreadLoopInvoke(Thread *thread, void (*fn)(Object *), Object *obj);
BASE_APISYM bool ThreadLoopInvoke(Thread *thread, Delegate<void (Object *)> del, Object *obj);
    /* ThreadLoopInvoke() will retain/release the Object passed to it to ensure that it remains
     * alive until the invocation is complete. The delegate version of ThreadLoopInvoke() will
     * further assume that the delegate target (i.e. the ''data'' member) is also an Object and
     * will retain/release it appropriately. Do not use ThreadLoopInvoke() with Delegate's that
     * have non Object targets (0 is ok).
     */
BASE_APISYM bool ThreadLoopCancelInvoke(Thread *thread, void (*fn)(Object *));
BASE_APISYM bool ThreadLoopCancelInvoke(Thread *thread, Delegate<void (Object *)> del);
BASE_APISYM bool ThreadLoopCancelInvoke(Thread *thread, Object *target);

/* timers */
BASE_APISYM void *ThreadLoopSetTimer(wait_timeout_t timeout, void (*fn)(Object *), Object *obj);
BASE_APISYM void *ThreadLoopSetTimer(wait_timeout_t timeout, Delegate<void (Object *)> del, Object *obj);
    /* ThreadLoopSetTimer() will retain/release the Object and delegate target passed to it
     * [similarly to ThreadLoopInvoke()]. These will be released at ThreadLoopCancelTimer().
     */
BASE_APISYM bool ThreadLoopCancelTimer(void *timer);

}
}

#endif // BASE_IMPL_THREADLOOP_HPP_INCLUDED
