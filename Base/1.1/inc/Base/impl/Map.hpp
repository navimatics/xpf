/*
 * Base/impl/Map.hpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef BASE_IMPL_MAP_HPP_INCLUDED
#define BASE_IMPL_MAP_HPP_INCLUDED

#include <Base/impl/Defines.hpp>
#include <Base/impl/Iterable.hpp>
#include <Base/impl/Object.hpp>
#include <Base/impl/Ref.hpp>
#include <Base/impl/hash_map.hpp>

namespace Base {
namespace impl {

class BASE_APISYM Map : public Object, public Iterable<Object *>
{
public:
    Map(ObjectComparer *comparer = 0);
    void autocollectObjects(bool ac);
    size_t count();
    Object *object(Object *key);
    bool getObject(Object *key, Object *&obj);
    void setObject(Object *key, Object *obj);
    void removeObject(Object *key);
    void removeAllObjects();
    void addObjects(Map *map);
    Object **keys();
    Object **objects();
    void getKeysAndObjects(Object **&keybuf, Object **&objbuf);
    const char *strrepr();
    /* Iterable<Object *> */
    void *iterate(void *state, Object **bufp[2]);
private:
    typedef hash_map<Ref<Object>, Ref<Object>, Object_hash_t, Object_equal_t> map_t;
    struct Iterator : public Object
    {
        Iterator(map_t &map) : p(map.begin()), q(map.end())
        {
        }
        map_t::iterator p, q;
    };
private:
    Ref<Object> _comparerRef;
    map_t _map;
    bool _nac;
};

template <typename K, typename V>
class BASE_APISYM GenericMap : public Map
{
public:
    V *object(K *key)
    {
        return (V *)inherited::object(key);
    }
    bool getObject(K *key, V *&obj)
    {
        return inherited::getObject(key, (Object *&)obj);
    }
    void setObject(K *key, V *obj)
    {
        inherited::setObject(key, obj);
    }
    void removeObject(K *key)
    {
        inherited::removeObject(key);
    }
    void addObjects(GenericMap<K, V> *map)
    {
        inherited::addObjects(map);
    }
    K **keys()
    {
        return (K **)inherited::keys();
    }
    V **objects()
    {
        return (V **)inherited::objects();
    }
    void getKeysAndObjects(K **&keybuf, V **&objbuf)
    {
        inherited::getKeysAndObjects((Object **&)keybuf, (Object **&)objbuf);
    }
private:
    typedef Map inherited;
};

}
}

#endif // BASE_IMPL_MAP_HPP_INCLUDED
