/*
 * Base/impl/Uuid.hpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef BASE_IMPL_UUID_HPP_INCLUDED
#define BASE_IMPL_UUID_HPP_INCLUDED

#include <Base/impl/Defines.hpp>

namespace Base {
namespace impl {

typedef unsigned char Uuid[16];
void BASE_APISYM UuidCreate(Uuid uu);
void BASE_APISYM UuidCopy(Uuid dst, const Uuid src);
void BASE_APISYM UuidClear(Uuid uu);
bool BASE_APISYM UuidIsNull(const Uuid uu);
bool BASE_APISYM UuidParse(const char *in, Uuid uu);
bool BASE_APISYM UuidUnparse(const Uuid uu, char *out, size_t size);
bool BASE_APISYM UuidUnparseRaw(const Uuid uu, char *out, size_t size);
int BASE_APISYM UuidCompare(const Uuid uu1, const Uuid uu2);

}
}

#endif // BASE_IMPL_UUID_HPP_INCLUDED
