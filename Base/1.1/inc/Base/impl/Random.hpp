/*
 * Base/impl/Random.hpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef BASE_IMPL_RANDOM_HPP_INCLUDED
#define BASE_IMPL_RANDOM_HPP_INCLUDED

#include <Base/impl/Defines.hpp>
#include <Base/impl/Object.hpp>

namespace Base {
namespace impl {

class BASE_APISYM Random : public Object
{
public:
    static unsigned long integerValue();
    static unsigned long integerValue(unsigned long le, unsigned long gt);
        /* le <= returned-value < gt */
    static double realValue();
        /* 0 <= returned-value < 1 */
    Random();
    Random(unsigned long seed);
    unsigned long nextIntegerValue();
    unsigned long nextIntegerValue(unsigned long le, unsigned long gt);
        /* le <= returned-value < gt */
    double nextRealValue();
        /* 0 <= returned-value < 1 */
private:
    void _init(unsigned long seed);
private:
#if ULONG_MAX > 4294967295
    struct DUP_TINYMT64_T {
        uint64_t status[2];
        uint32_t mat1;
        uint32_t mat2;
        uint64_t tmat;
    } _state;
#else
    struct DUP_TINYMT32_T {
        uint32_t status[4];
        uint32_t mat1;
        uint32_t mat2;
        uint32_t tmat;
    } _state;
#endif
};

}
}

#endif // BASE_IMPL_RANDOM_HPP_INCLUDED
