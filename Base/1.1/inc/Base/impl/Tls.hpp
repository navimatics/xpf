/*
 * Base/impl/Tls.hpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef BASE_IMPL_TLS_HPP_INCLUDED
#define BASE_IMPL_TLS_HPP_INCLUDED

#include <Base/impl/Defines.hpp>
#if defined(BASE_CONFIG_WINDOWS)
#include <windows.h>
#elif defined(BASE_CONFIG_POSIX)
#include <pthread.h>
#endif

namespace Base {
namespace impl {

#if defined(BASE_CONFIG_WINDOWS)
typedef DWORD tlskey_t;
tlskey_t tlsalloc(void (*fn)(void *));
inline void *tlsget(tlskey_t key)
{
    return TlsGetValue(key);
}
inline void tlsset(tlskey_t key, void *value)
{
    TlsSetValue(key, value);
}
#elif defined(BASE_CONFIG_POSIX)
typedef pthread_key_t tlskey_t;
inline tlskey_t tlsalloc(void (*fn)(void *))
{
    tlskey_t key = 0;
    pthread_key_create(&key, fn);
    return key;
}
inline void *tlsget(tlskey_t key)
{
    return pthread_getspecific(key);
}
inline void tlsset(tlskey_t key, void *value)
{
    pthread_setspecific(key, value);
}
#endif

}
}

#endif // BASE_IMPL_TLS_HPP_INCLUDED
