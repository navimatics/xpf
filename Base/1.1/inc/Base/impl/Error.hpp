/*
 * Base/impl/Error.hpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef BASE_IMPL_ERROR_HPP_INCLUDED
#define BASE_IMPL_ERROR_HPP_INCLUDED

#include <Base/impl/Defines.hpp>
#include <Base/impl/CString.hpp>
#include <Base/impl/Object.hpp>

namespace Base {
namespace impl {

class BASE_APISYM Error : public Object
{
public:
    /* error logging */
    typedef void (*logger_t)(Error *);
    static logger_t logger();
    static void setLogger(logger_t logger);
    static void standardLogger(Error *);
    static void filteredLogger(Error *);
    static void addFilteredLoggerDomain(const char *domain);
    static void removeFilteredLoggerDomain(const char *domain);
    static void enableLogging(bool flag);
public:
    static Error *lastError();
    static void setLastError(Error *error, bool restore = false);
    static void setLastError(const char *domain, int code, const char *message = 0);
    static void setLastErrorFromCErrno();
    static void setLastErrorFromCErrno(int code);
    static void setLastErrorFromSystem();
    static void setLastErrorFromSystem(int code);
    static Error *createFromCErrno();
    static Error *createFromCErrno(int code);
    static Error *createFromSystem();
    static Error *createFromSystem(int code);
    Error(const char *domain, int code, const char *message = 0);
    Error(CString *domain, int code, CString *message = 0);
    ~Error();
    Object *threadsafe();
    const char *domain();
    int code();
    const char *message();
    const char *strrepr();
private:
    const char *_domain;
    int _code;
    const char *_message;
};

}
}

#endif // BASE_IMPL_ERROR_HPP_INCLUDED
