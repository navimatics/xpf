/*
 * Base/impl/ObjectWriter.hpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef BASE_IMPL_OBJECTWRITER_HPP_INCLUDED
#define BASE_IMPL_OBJECTWRITER_HPP_INCLUDED

#include <Base/impl/Defines.hpp>
#include <Base/impl/JsonWriter.hpp>
#include <Base/impl/Map.hpp>

namespace Base {
namespace impl {

class BASE_APISYM ObjectWriter : public JsonWriter
{
public:
    ObjectWriter(Stream *stream = 0, bool pretty = false);
    ~ObjectWriter();
    bool writeObject(Object *obj);
private:
    bool writeObjectInternal(Object *obj);
private:
    Map *_idmap;
};

}
}

#endif // BASE_IMPL_OBJECTWRITER_HPP_INCLUDED
