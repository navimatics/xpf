/*
 * Base/impl/SystemStream.hpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef BASE_IMPL_SYSTEMSTREAM_HPP_INCLUDED
#define BASE_IMPL_SYSTEMSTREAM_HPP_INCLUDED

#include <Base/impl/Defines.hpp>
#include <Base/impl/Stream.hpp>
#include <Base/impl/Thr.hpp>

namespace Base {
namespace impl {

class BASE_APISYM SystemStream : public Stream
{
public:
#if defined(BASE_CONFIG_WINDOWS)
    enum { InvalidHandle = -1 };
    typedef intptr_t handle_t;
#elif defined(BASE_CONFIG_POSIX)
    enum { InvalidHandle = -1 };
    typedef int handle_t;
#endif
public:
    static SystemStream *create(const char *path, const char *mode);
    SystemStream(handle_t handle);
    ~SystemStream();
    ssize_t read(void *buf, size_t size);
    ssize_t write(const void *buf, size_t size);
    ssize_t seek(ssize_t offset, int whence = SEEK_SET);
    ssize_t close();
    void cancel();
    handle_t handle();
protected:
    virtual ssize_t _close(handle_t handle);
private:
    ssize_t rdwr(void *buf, size_t size, int flag);
private:
    int _objid;
    handle_t _handle;
    thr_t _thr;
    int _canceled;
#if defined(BASE_CONFIG_WINDOWS)
    int64_t _offset;
#endif
};

inline SystemStream::handle_t SystemStream::handle()
{
    return _handle;
}

}
}

#endif // BASE_IMPL_SYSTEMSTREAM_HPP_INCLUDED
