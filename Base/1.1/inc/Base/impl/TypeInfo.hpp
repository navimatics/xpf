/*
 * Base/impl/Typeinfo.hpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef BASE_IMPL_TYPEINFO_HPP_INCLUDED
#define BASE_IMPL_TYPEINFO_HPP_INCLUDED

#include <Base/impl/Defines.hpp>

namespace Base {
namespace impl {

const char *TypeName(const std::type_info &tinfo);

}
}

#endif // BASE_IMPL_TYPEINFO_HPP_INCLUDED
