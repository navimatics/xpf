/*
 * Base/impl/BitHacks.hpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef BASE_IMPL_BITHACKS_HPP_INCLUDED
#define BASE_IMPL_BITHACKS_HPP_INCLUDED

#include <Base/impl/Defines.hpp>
#if defined(BASE_CONFIG_MSVC)
#include <intrin.h>
#endif

namespace Base {
namespace impl {

/* rotate */
inline unsigned bit_rol(unsigned v, unsigned shift)
{
    return (v << shift) | (v >> (sizeof(v) * CHAR_BIT - shift));
}
inline unsigned long bit_rol(unsigned long v, unsigned shift)
{
    return (v << shift) | (v >> (sizeof(v) * CHAR_BIT - shift));
}
inline unsigned bit_ror(unsigned v, unsigned shift)
{
    return (v >> shift) | (v << (sizeof(v) * CHAR_BIT - shift));
}
inline unsigned long bit_ror(unsigned long v, unsigned shift)
{
    return (v >> shift) | (v << (sizeof(v) * CHAR_BIT - shift));
}

/* count leading zeroes */
#define INDEX_BITS                      IMAX_BITS(UINTPTR_MAX)
inline int bit_clz(index_t v)
{
    if (0 == v)
        return INDEX_BITS;
#if defined(BASE_CONFIG_MSVC) && INDEX_BITS <= 32
    unsigned long r = 0;
    _BitScanReverse(&r, v);
    return (INDEX_BITS - 1) - r;
#elif defined(BASE_CONFIG_MSVC) && INDEX_BITS <= 64
    unsigned long r = 0;
    _BitScanReverse64(&r, v);
    return (INDEX_BITS - 1) - r;
#elif defined(BASE_CONFIG_GNUC) && INDEX_BITS <= 32
    return __builtin_clz(v);
#elif defined(BASE_CONFIG_GNUC) && INDEX_BITS <= 64
    return __builtin_clzll(v);
#else
    /* based on a method from http://graphics.stanford.edu/~seander/bithacks.html */
    int r = 0;
#if INDEX_BITS > 32
    if (0 != (v & 0xFFFFFFFF00000000))
    {
        v >>= 32;
        r |= 32;
    }
#endif
    if (0 != (v & 0xFFFF0000))
    {
        v >>= 16;
        r |= 16;
    }
    if (0 != (v & 0xFF00))
    {
        v >>= 8;
        r |= 8;
    }
    if (0 != (v & 0xF0))
    {
        v >>= 4;
        r |= 4;
    }
    if (0 != (v & 0xC))
    {
        v >>= 2;
        r |= 2;
    }
    if (0 != (v & 0x2))
    {
        //v >>= 1;
        r |= 1;
    }
    return (INDEX_BITS - 1) - r;
#endif
}

}
}

#endif // BASE_IMPL_BITHACKS_HPP_INCLUDED
