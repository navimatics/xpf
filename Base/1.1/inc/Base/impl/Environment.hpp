/*
 * Base/impl/Environment.hpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef BASE_IMPL_ENVIRONMENT_HPP_INCLUDED
#define BASE_IMPL_ENVIRONMENT_HPP_INCLUDED

#include <Base/impl/Defines.hpp>
#include <Base/impl/Map.hpp>
#include <Base/impl/Object.hpp>
#include <Base/impl/Synchronization.hpp>

namespace Base {
namespace impl {

class BASE_APISYM Environment : public Object
{
public:
    enum Kind
    {
        Thread,                         /* thread local */
        Process,                        /* process local */
        Application,                    /* persist across process executions */
        Factory,                        /* "factory" defaults */
        Override = 0x80000000,          /* override value in all environments above specified one */
        Default = Thread,
    };
public:
    static Environment *standardEnvironment();
    Environment(const char *path);
    ~Environment();
    Lock *lock();
    Object *object(Object *key, unsigned kind = Default);
    void setObject(Object *key, Object *obj, unsigned kind = Default);
    void removeObject(Object *key, unsigned kind = Default);
    void addObjects(Map *map, unsigned kind = Default);
    void addObjects(const char *path, unsigned kind = Default);
    void markDirty(unsigned kind = Default);
    bool commit();
private:
    Map *_threadMap();
private:
    const char *_path;
    Lock *_lock;
    Object *_threadMapKey;
    Map *_processMap;
    Map *_applicationMap;
    Map *_factoryMap;
    bool _sync;
};

}
}

#endif // BASE_IMPL_ENVIRONMENT_HPP_INCLUDED
