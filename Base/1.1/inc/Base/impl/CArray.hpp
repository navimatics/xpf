/*
 * Base/impl/CArray.hpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef BASE_IMPL_CARRAY_HPP_INCLUDED
#define BASE_IMPL_CARRAY_HPP_INCLUDED

#include <Base/impl/Defines.hpp>
#include <Base/impl/Object.hpp>

namespace Base {
namespace impl {

class BASE_APISYM CArray : public Object
{
public:
    static CArray *empty();
    static CArray *create(const void *p, size_t length, size_t elemsize);
    static CArray *createWithCapacity(size_t capacity, size_t elemsize);
    size_t length();
    void setLength(size_t value);
    void *elems();
    void apply(void (*fn)(void *));
    void apply(void (Object::*fn)());
    CArray *_concat(const void *p, size_t length, size_t elemsize);
private:
    CArray();
private:
    size_t _length;
    size_t _size;
};

BASE_APISYM void *carray(const void *p, size_t length, size_t elemsize);
BASE_APISYM void *carrayWithCapacity(size_t capacity, size_t elemsize);
BASE_APISYM void *carr_new(const void *p, size_t length, size_t elemsize);
BASE_APISYM void *carr_newWithCapacity(size_t capacity, size_t elemsize);
BASE_APISYM void carr_assign(void *&p, void *q);
BASE_APISYM void carr_assign_copy(void *&p, const void *q, size_t length, size_t elemsize);
BASE_APISYM CArray *carr_obj(void *p);
BASE_APISYM void carr_retain(void *p);
BASE_APISYM void carr_release(void *p);
BASE_APISYM void carr_autorelease(void *p);
BASE_APISYM void carr_autocollect(void *p);
BASE_APISYM size_t carr_length(const void *p);
BASE_APISYM void *carr_concat(void *p, const void *q, size_t length, size_t elemsize);

inline size_t CArray::length()
{
    return _length;
}
inline void CArray::setLength(size_t value)
{
    _length = value;
}
inline void *CArray::elems()
{
    return (char *)this + sizeof(CArray);
}
inline CArray::CArray()
{
}

inline void carr_assign(void *&p, void *q)
{
    if (0 != q)
        ((CArray *)((char *)q - sizeof(CArray)))->retain();
    if (0 != p)
        ((CArray *)((char *)p - sizeof(CArray)))->release();
    p = q;
}
inline void carr_assign_copy(void *&p, const void *q0, size_t length, size_t elemsize)
{
    void *q = 0;
    if (0 != q0)
        q = carr_new(q0, length, elemsize);
    if (0 != p)
        ((CArray *)((char *)p - sizeof(CArray)))->release();
    p = q;
}
inline CArray *carr_obj(void *p)
{
    if (0 == p)
        return 0;
    return (CArray *)((char *)p - sizeof(CArray));
}
inline void carr_retain(void *p)
{
    if (0 != p)
        ((CArray *)((char *)p - sizeof(CArray)))->retain();
}
inline void carr_release(void *p)
{
    if (0 != p)
        ((CArray *)((char *)p - sizeof(CArray)))->release();
}
inline void carr_autorelease(void *p)
{
    if (0 != p)
        ((CArray *)((char *)p - sizeof(CArray)))->autorelease();
}
inline void carr_autocollect(void *p)
{
    if (0 != p)
        ((CArray *)((char *)p - sizeof(CArray)))->autocollect();
}
inline size_t carr_length(const void *p)
{
    if (0 == p)
        return 0;
    return ((CArray *)((char *)p - sizeof(CArray)))->length();
}

BASE_APISYM void **carr_pointer_concat(void **p, void *q);
BASE_APISYM void carr_pointer_remove(void **p, void *q);

inline void **carr_pointer_concat(void **p, void *q)
{
    return (void **)carr_concat(p, &q, 1, sizeof(void *));
}

}
}

#endif // BASE_IMPL_CARRAY_HPP_INCLUDED
