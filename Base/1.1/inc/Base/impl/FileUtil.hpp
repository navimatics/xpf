/*
 * Base/impl/FileUtil.hpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef BASE_IMPL_FILEUTIL_HPP_INCLUDED
#define BASE_IMPL_FILEUTIL_HPP_INCLUDED

#include <Base/impl/Defines.hpp>

namespace Base {
namespace impl {

BASE_APISYM const char **listdir(const char *dirname, const char *pattern = 0, int fnmatchFlags = 0);

}
}

#endif // BASE_IMPL_FILEUTIL_HPP_INCLUDED
