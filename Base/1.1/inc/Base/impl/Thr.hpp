/*
 * Base/impl/Thr.hpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef BASE_IMPL_THR_HPP_INCLUDED
#define BASE_IMPL_THR_HPP_INCLUDED

#include <Base/impl/Defines.hpp>
#if defined(BASE_CONFIG_WINDOWS)
#include <windows.h>
#include <process.h>
#elif defined(BASE_CONFIG_POSIX)
#include <pthread.h>
#include <sys/select.h>
#endif

namespace Base {
namespace impl {

#if defined(BASE_CONFIG_WINDOWS)
#define BASE_IMPL_THRENTRY(fn, arg)     unsigned BASE_STDCALL fn(void *arg)
typedef HANDLE thr_t;
inline bool thrcreate(thr_t &thr, unsigned (BASE_STDCALL *entry)(void *), void *arg)
{
    if (0 != (thr = (thr_t)_beginthreadex(0, 0, entry, arg, CREATE_SUSPENDED, 0)))
    {
        ResumeThread(thr);
        return true;
    }
    else
        return false;
}
inline void thrdetach(thr_t thr)
{
    CloseHandle(thr);
}
inline void threxit()
{
    _endthreadex(0);
}
inline void thrsleep(wait_timeout_t timeout = wait_timeout_infinite)
{
    BASE_COMPILE_ASSERT(INFINITE == wait_timeout_infinite);
    SleepEx(timeout, TRUE);
}
inline void thrjoin(thr_t thr)
{
    WaitForSingleObject(thr, INFINITE);
    CloseHandle(thr);
}
inline thr_t _thrcurrent()
{
    /* !!!:
     * The Windows _thrcurrent() returns a new thr_t that needs to be eventually destroyed
     * using thrdetach().
     *
     * This is different from the POSIX version, which simply returns the current thr_t.
     */
    thr_t thr = 0;
    DuplicateHandle(
        GetCurrentProcess(), GetCurrentThread(),
        GetCurrentProcess(), &thr,
        0, false, DUPLICATE_SAME_ACCESS);
    return thr;
}
#elif defined(BASE_CONFIG_POSIX)
#define BASE_IMPL_THRENTRY(fn, arg)     void *fn(void *arg)
typedef pthread_t thr_t;
inline bool thrcreate(thr_t &thr, void *(*entry)(void *), void *arg)
{
    thr_t tmp;
    if (0 == pthread_create(&tmp, 0, entry, arg))
    {
        /* !!!:
         * Under POSIX there is no guarantee that tmp will have been assigned
         * before ``entry'' has started executing.
         */
        thr = tmp;
        return true;
    }
    else
    {
        thr = 0;
        return false;
    }
}
inline void thrdetach(thr_t thr)
{
    pthread_detach(thr);
}
inline void threxit()
{
    pthread_exit(0);
}
inline void thrsleep(wait_timeout_t timeout = wait_timeout_infinite)
{
    if (wait_timeout_infinite == timeout)
        select(0, 0, 0, 0, 0);
    else
    {
        struct timeval tv;
        tv.tv_sec = timeout / 1000;
        tv.tv_usec = (timeout % 1000) * 1000;
        select(0, 0, 0, 0, &tv);
    }
}
inline void thrjoin(thr_t thr)
{
    pthread_join(thr, 0);
}
inline thr_t _thrcurrent()
{
    /* !!!:
     * The POSIX _thrcurrent() returns the current thr_t.
     *
     * This is different from the Windows version, which returns a new thr_t
     * that needs to be eventually destroyed using thrdetach().
     */
    return pthread_self();
}
#endif

}
}

#endif // BASE_IMPL_THR_HPP_INCLUDED
