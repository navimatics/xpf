/*
 * Base/impl/UString.hpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef BASE_IMPL_USTRING_HPP_INCLUDED
#define BASE_IMPL_USTRING_HPP_INCLUDED

#include <Base/impl/Defines.hpp>
#include <Base/impl/Object.hpp>

namespace Base {
namespace impl {

typedef uint16_t unichar_t;

class BASE_APISYM UString : public Object
{
public:
    static UString *empty();
    static UString *create(const unichar_t *s, size_t length = -1);
    static UString *createWithCapacity(size_t capacity);
    size_t hash();
    int cmp(Object *obj);
    size_t length();
    void setLength(size_t value);
    const unichar_t *uchars();
    UString *slice(ssize_t i, ssize_t j = 0);
    UString *_concat(const unichar_t *s, size_t length = -1);
    void makeImmutable();
private:
    UString();
    static UString *__empty();
private:
    size_t _length;
    size_t _metric;
};

inline size_t UString::length()
{
    return _length;
}
inline const unichar_t *UString::uchars()
{
    return (unichar_t *)((char *)this + sizeof(UString));
}
inline UString::UString()
{
}

BASE_APISYM const unichar_t *ustring(const unichar_t *s, size_t length = -1);
BASE_APISYM const unichar_t *ustringWithCapacity(size_t capacity);
BASE_APISYM const unichar_t *ustr_new(const unichar_t *s, size_t length = -1);
BASE_APISYM const unichar_t *ustr_newWithCapacity(size_t capacity);
BASE_APISYM void ustr_assign(const unichar_t *&s, const unichar_t *t);
BASE_APISYM void ustr_assign_copy(const unichar_t *&s, const unichar_t *t, size_t length = -1);
BASE_APISYM UString *ustr_obj(const unichar_t *s);
BASE_APISYM void ustr_retain(const unichar_t *s);
BASE_APISYM void ustr_release(const unichar_t *s);
BASE_APISYM void ustr_autorelease(const unichar_t *s);
BASE_APISYM void ustr_autocollect(const unichar_t *s);
BASE_APISYM size_t ustr_length(const unichar_t *s);
BASE_APISYM const unichar_t *ustr_slice(const unichar_t *s, ssize_t i, ssize_t j = 0);
BASE_APISYM const unichar_t *ustr_concat(const unichar_t *s, const unichar_t *t, size_t length = -1);

inline void ustr_assign(const unichar_t *&s, const unichar_t *t)
{
    if (0 != t)
        ((UString *)((char *)t - sizeof(UString)))->retain();
    if (0 != s)
        ((UString *)((char *)s - sizeof(UString)))->release();
    s = t;
}
inline void ustr_assign_copy(const unichar_t *&s, const unichar_t *t, size_t length)
{
    if (0 != t)
        t = ustr_new(t, length);
    if (0 != s)
        ((UString *)((char *)s - sizeof(UString)))->release();
    s = t;
}
inline UString *ustr_obj(const unichar_t *s)
{
    if (0 == s)
        return 0;
    return (UString *)((char *)s - sizeof(UString));
}
inline void ustr_retain(const unichar_t *s)
{
    if (0 != s)
        ((UString *)((char *)s - sizeof(UString)))->retain();
}
inline void ustr_release(const unichar_t *s)
{
    if (0 != s)
        ((UString *)((char *)s - sizeof(UString)))->release();
}
inline void ustr_autorelease(const unichar_t *s)
{
    if (0 != s)
        ((UString *)((char *)s - sizeof(UString)))->autorelease();
}
inline void ustr_autocollect(const unichar_t *s)
{
    if (0 != s)
        ((UString *)((char *)s - sizeof(UString)))->autocollect();
}
inline size_t ustr_length(const unichar_t *s)
{
    if (0 == s)
        return 0;
    return ((UString *)((char *)s - sizeof(UString)))->length();
}
inline const unichar_t *ustr_slice(const unichar_t *s, ssize_t i, ssize_t j)
{
    if (0 == s)
        return 0;
    return ((UString *)((char *)s - sizeof(UString)))->slice(i, j)->uchars();
}

BASE_APISYM const unichar_t *ustr_from_cstr(const char *s, size_t length = -1);
BASE_APISYM const char *cstr_from_ustr(const unichar_t *s, size_t length = -1);

}
}

#endif // BASE_IMPL_USTRING_HPP_INCLUDED
