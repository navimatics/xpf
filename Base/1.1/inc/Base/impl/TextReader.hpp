/*
 * Base/impl/TextReader.hpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef BASE_IMPL_TEXTREADER_HPP_INCLUDED
#define BASE_IMPL_TEXTREADER_HPP_INCLUDED

#include <Base/impl/Defines.hpp>
#include <Base/impl/Stream.hpp>
#include <Base/impl/TextParser.hpp>

namespace Base {
namespace impl {

class BASE_APISYM TextReader : public TextParser
{
public:
    TextReader(Stream *stream = 0);
    void reset();
    char readChar();
    const char *readLine();
    const char *readUntil(const char *delim);
    const char *readSpan(const char *chars, bool complement = false);
    void skipSpan(const char *chars, bool complement = false);
    long readIntegerValue();
    double readRealValue();
    bool finished();
    void setFinished(bool value);
protected:
    bool _finished;
};

}
}

#endif // BASE_IMPL_TEXTREADER_HPP_INCLUDED
