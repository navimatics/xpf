/*
 * Base/impl/FileStream.hpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef BASE_IMPL_FILESTREAM_HPP_INCLUDED
#define BASE_IMPL_FILESTREAM_HPP_INCLUDED

#include <Base/impl/Defines.hpp>
#include <Base/impl/Stream.hpp>
#include <Base/impl/Uuid.hpp>

namespace Base {
namespace impl {

class BASE_APISYM FileStream : public Stream
{
public:
    static FileStream *create(const char *path, const char *mode);
        /* mode:
         * 'r','w','a','+','b': as in fopen()
         * 'W': atomic write mode
         *      (like 'w' but writes into temp file which gets atomically renamed at close() time)
         * '0': decompress mode; '1'-'9': compress at level
         *      default: no compression/decompression,
         *          unless 'g','z' specified: '0' when 'r', '9' when 'w','W'
         * 'g': gzip mode; 'z': zlib mode
         *      default: no compression/decompression,
         *          unless '0'-'9' specified: gzip mode when '1'-'9', autodetect when '0'
         * '!': archive mode
         *      path may be of the format <archive-path!/file-path>,
         *          where archive-path is a path to an archive file (currently only zip),
         *          file-path is a path to a file within the archive file
         *
         * COMPRESSION MODES ARE ONLY ALLOWED WHEN USING 'r','w','W' WITH 'b'.
         * ARCHIVE MODES ARE ONLY ALLOWED WHEN USING 'r','w' WITH 'b'.
         * COMPRESSION AND ARCHIVE MODES CANNOT BE COMBINED.
         */
    ~FileStream();
    ssize_t read(void *buf, size_t size);
    ssize_t write(const void *buf, size_t size);
    ssize_t seek(ssize_t offset, int whence = SEEK_SET);
    ssize_t close();
    const char *path();
private:
    FileStream();
private:
    const char *_path, *_wpath;
    Stream *_stream, *_iostream;
};

inline FileStream::FileStream()
{
}

}
}

#endif // BASE_IMPL_FILESTREAM_HPP_INCLUDED
