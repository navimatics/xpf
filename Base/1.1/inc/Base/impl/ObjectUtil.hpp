/*
 * Base/impl/ObjectUtil.hpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef BASE_IMPL_OBJECTUTIL_HPP_INCLUDED
#define BASE_IMPL_OBJECTUTIL_HPP_INCLUDED

#include <Base/impl/Defines.hpp>
#include <Base/impl/Object.hpp>

namespace Base {
namespace impl {

BASE_APISYM Object *ObjectReadNative(const char *path, unsigned compat = 0);
BASE_APISYM bool ObjectWriteNative(const char *path, Object *obj, unsigned compat = 0);

BASE_APISYM Object *ObjectReadJson(const char *path);
BASE_APISYM bool ObjectWriteJson(const char *path, Object *obj);

}
}

#endif // BASE_IMPL_OBJECTUTIL_HPP_INCLUDED
