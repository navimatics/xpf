/*
 * Base/impl/RadixTree.hpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef BASE_IMPL_RADIXTREE_HPP_INCLUDED
#define BASE_IMPL_RADIXTREE_HPP_INCLUDED

#include <Base/impl/Defines.hpp>
#include <Base/impl/Iterable.hpp>
#include <Base/impl/Object.hpp>

BASE_ITERABLE_REGISTER_ITEM_TYPE(Base::impl::index_t, 0x696478/*ASCII: idx*/)

namespace Base {
namespace impl {

class BASE_APISYM RadixTree : public Object
{
public:
    struct Node
    {
        index_t index, value;
        int bitpos;
        Node *child[2];
    };
    struct NodeIterator : public Object, public Iterable<index_t>
    {
        NodeIterator(RadixTree *tree)
        {
            stkptr = stkbuf;
            node = tree->_nodes;
        }
        void *iterate(void *state, index_t *bufp[2]);
        int lineno;
        Node *stkbuf[IMAX_BITS(UINTPTR_MAX) + 2], **stkptr;
        Node *node, *left, *right;
        int bitpos;
    };
public:
    RadixTree(size_t capacity = 0);
    ~RadixTree();
    void reset();
    Node *lookup(index_t index);
    void insert(Node *node, index_t index, index_t value);
    void remove(Node *remnode);
    Node *nullnode();
    void __sanityCheck();
private:
    Node *_nodes;
};

inline RadixTree::Node *RadixTree::nullnode()
{
    return _nodes;
}

}
}

#endif // BASE_IMPL_RADIXTREE_HPP_INCLUDED
