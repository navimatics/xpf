/*
 * Base/impl/MemoryStream.hpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef BASE_IMPL_MEMORYSTREAM_HPP_INCLUDED
#define BASE_IMPL_MEMORYSTREAM_HPP_INCLUDED

#include <Base/impl/Defines.hpp>
#include <Base/impl/Stream.hpp>

namespace Base {
namespace impl {

class BASE_APISYM MemoryStream : public Stream
{
public:
    MemoryStream(const void *buf = 0, size_t size = -1);
    ~MemoryStream();
    ssize_t read(void *buf, size_t size);
    ssize_t write(const void *buf, size_t size);
        /*
         * Write() appends to the end of the stream buffer and then sets the stream offset
         * to point to the new end. The behavior is as if the POSIX O_APPEND flag was set
         * on the MemoryStream.
         *
         * This means that a read() after a write() will always return 0.
         */
    ssize_t seek(ssize_t offset, int whence = SEEK_SET);
    ssize_t close();
    const char *buffer();
    size_t size();
private:
    const char *_buf;
    ssize_t _offset;
};

}
}

#endif // BASE_IMPL_MEMORYSTREAM_HPP_INCLUDED
