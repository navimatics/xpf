/*
 * Base/impl/Jvm.hpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef BASE_IMPL_JVM_HPP_INCLUDED
#define BASE_IMPL_JVM_HPP_INCLUDED

#include <Base/impl/Defines.hpp>
#include <Base/impl/CString.hpp>
#include <Base/impl/Object.hpp>
#if defined(BASE_CONFIG_JVM)
#if defined(BASE_CONFIG_DARWIN)
#include <JavaVM/jni.h>
#else
#include <jni.h>
#endif
#endif

#if !defined(JNI_VERSION_1_1)
struct JavaVM;
struct JNIEnv;
struct JNINativeMethod;
typedef void *jobject;
typedef void *jclass;
typedef void *jstring;
#endif

#define BASE_JVM_BASEPACKAGE            "com/navimatics/base"
#define BASE_JVM_JAVAOBJECTBASECLASS    "com/navimatics/base/NativeObject"

#define BASE_JVM_NATIVEMETHOD(n,s)      { (char *)#n, (char *)s, (void *)n }
#define BASE_JVM_NATIVEMETHOD_EX(n,s,p) { (char *)n, (char *)s, (void *)p }

#define BASE_JVM_NATIVEFIELD(n,s,t)     { #n, s, offsetof(t, n) }
#define BASE_JVM_NATIVEFIELD_EX(n,s,o)  { n, s, o }

namespace Base {
namespace impl {

/*
 * utility
 */
BASE_APISYM JavaVM *Jvm();
BASE_APISYM JNIEnv *JvmEnvironment();
BASE_APISYM void JvmRegisterNativeMethods(const char *clsname, JNINativeMethod *methods, size_t nmethods);
BASE_APISYM void JvmThrowException(JNIEnv *env, const char *clsname, const char *msg = 0);
BASE_APISYM jclass JvmClassGlobalRef(JNIEnv *env, const char *clsname);
BASE_APISYM const char *JvmClassName(JNIEnv *env, jobject jobj, bool javaFormat = false);
BASE_APISYM jstring JvmCallToStringMethod(JNIEnv *env, jobject jobj);
BASE_APISYM jstring JvmJavaString(JNIEnv *env, const char *str);
BASE_APISYM const char *JvmCString(JNIEnv *env, jstring jstr);
BASE_APISYM void JvmFatal(JNIEnv *env, const char *format, ...);

/*
 * peer mapping
 */
struct JvmNativeField
{
    const char *name;
    const char *signature;
    size_t offset;
};
BASE_APISYM void _JvmInitializePeerMapping();
BASE_APISYM void JvmRegisterPeerTypes(const char *clsname, const std::type_info &tinfo);
BASE_APISYM jobject JvmJavaObject(JNIEnv *env, Object *obj);
BASE_APISYM Object *JvmNativeObject(JNIEnv *env, jobject jobj);
BASE_APISYM void JvmSetNativeObject(JNIEnv *env, jobject jobj, Object *obj);
BASE_APISYM void _JvmJavaObjectDispose(JNIEnv *env, jobject jobj);
BASE_APISYM void _JvmFinalReferenceDispose(JNIEnv *env, jobject jref);
BASE_APISYM jclass JvmJavaObjectBaseClass();
BASE_APISYM void *JvmCreateStructMapping(const char *clsname, JvmNativeField *fields, size_t nfields);
BASE_APISYM void JvmStructFromJavaObject(JNIEnv *env, void *mapping, jobject jobj, void *buf);
BASE_APISYM jobject JvmJavaObjectFromStruct(JNIEnv *env, void *mapping, const void *buf);

}
}

#endif // BASE_IMPL_JVM_HPP_INCLUDED
