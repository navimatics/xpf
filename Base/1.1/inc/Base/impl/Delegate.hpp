/*
 * Base/impl/Delegate.hpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef BASE_IMPL_DELEGATE_HPP_INCLUDED
#define BASE_IMPL_DELEGATE_HPP_INCLUDED

#include <Base/impl/Defines.hpp>

namespace Base {
namespace impl {

/*
 * Based on Sergey Ryazanov's "Impossibly Fast C++ Delegates".
 * See http://www.codeproject.com/KB/cpp/ImpossiblyFastCppDelegate.aspx
 *
 * DelegateMaker machinery based on "Lightweight Generic C++ Callbacks".
 * See http://www.codeproject.com/KB/cpp/CPPCallback.aspx?display=Mobile
 */
struct BASE_APISYM DelegateData
{
    void (*code)(void *);
    void *data;
};
template <typename Sig>
struct BASE_APISYM Delegate;
template <typename Sig>
struct _FunctionDelegateMaker;
template <typename T, typename Sig>
struct _MethodDelegateMaker;
template <typename T, typename Sig>
struct _ConstMethodDelegateMaker;

#define BASE_IMPL_DEFINE_DELEGATE(ARGTYPS, TYPARGS, ARGS, COMMA) \
    template <typename R COMMA ARGTYPS>\
    struct BASE_APISYM Delegate<R (TYPARGS)> : DelegateData\
    {\
        typedef R (signature)(TYPARGS);\
        Delegate(R (*c)(void * COMMA TYPARGS) = 0, void *d = 0)\
        {\
            BASE_COMPILE_ASSERT(sizeof(DelegateData) == sizeof(*this));\
            code = (void (*)(void *))c;\
            data = d;\
        }\
        R operator()(TYPARGS) const\
        {\
            return ((R (*)(void * COMMA TYPARGS))code)(data COMMA ARGS);\
        }\
        operator bool() const\
        {\
            return 0 != code;\
        }\
        bool operator!() const\
        {\
            return 0 == code;\
        }\
    };\
    template <typename R COMMA ARGTYPS>\
    struct _FunctionDelegateMaker<R (TYPARGS)>\
    {\
        template <R (*Function)(TYPARGS)>\
        static R stub(void *data COMMA TYPARGS)\
        {\
            return (*Function)(ARGS);\
        }\
        template <R (*Function)(TYPARGS)>\
        Delegate<R (TYPARGS)> make()\
        {\
            return Delegate<R (TYPARGS)>(&stub<Function>);\
        }\
    };\
    template <typename T, typename R COMMA ARGTYPS>\
    struct _MethodDelegateMaker<T, R (TYPARGS)>\
    {\
        template <R (T::*Method)(TYPARGS)>\
        static R stub(void *data COMMA TYPARGS)\
        {\
            T *p = (T *)data;\
            return (p->*Method)(ARGS);\
        }\
        template <R (T::*Method)(TYPARGS)>\
        static Delegate<R (TYPARGS)> make(T *p)\
        {\
            return Delegate<R (TYPARGS)>(&stub<Method>, p);\
        }\
    };\
    template <typename T, typename R COMMA ARGTYPS>\
    struct _ConstMethodDelegateMaker<T, R (TYPARGS)>\
    {\
        template <R (T::*Method)(TYPARGS) const>\
        static R stub(void *data COMMA TYPARGS)\
        {\
            const T *p = (const T *)data;\
            return (p->*Method)(ARGS);\
        }\
        template <R (T::*Method)(TYPARGS) const>\
        static Delegate<R (TYPARGS)> make(const T *p)\
        {\
            return Delegate<R (TYPARGS)>(&stub<Method>, (void *)p);\
        }\
    };\
    template <typename R COMMA ARGTYPS>\
    _FunctionDelegateMaker<R (TYPARGS)> _DelegateMaker(R (*)(TYPARGS))\
    {\
        return _FunctionDelegateMaker<R (TYPARGS)>();\
    }\
    template <typename T, typename R COMMA ARGTYPS>\
    _MethodDelegateMaker<T, R (TYPARGS)> _DelegateMaker(R (T::*)(TYPARGS))\
    {\
        return _MethodDelegateMaker<T, R (TYPARGS)>();\
    }\
    template <typename T, typename R COMMA ARGTYPS>\
    _ConstMethodDelegateMaker<T, R (TYPARGS)> _DelegateMaker(R (T::*)(TYPARGS) const)\
    {\
        return _ConstMethodDelegateMaker<T, R (TYPARGS)>();\
    }

BASE_IMPL_DEFINE_DELEGATE(
    BASE_COMMA_CONCAT(),
    BASE_COMMA_CONCAT(),
    BASE_COMMA_CONCAT(),
    BASE_COMMA_CONCAT())
BASE_IMPL_DEFINE_DELEGATE(
    BASE_COMMA_CONCAT(typename A1),
    BASE_COMMA_CONCAT(A1 a1),
    BASE_COMMA_CONCAT(a1),
    BASE_COMMA_CONCAT(,))
BASE_IMPL_DEFINE_DELEGATE(
    BASE_COMMA_CONCAT(typename A1, typename A2),
    BASE_COMMA_CONCAT(A1 a1, A2 a2),
    BASE_COMMA_CONCAT(a1, a2),
    BASE_COMMA_CONCAT(,))
BASE_IMPL_DEFINE_DELEGATE(
    BASE_COMMA_CONCAT(typename A1, typename A2, typename A3),
    BASE_COMMA_CONCAT(A1 a1, A2 a2, A3 a3),
    BASE_COMMA_CONCAT(a1, a2, a3),
    BASE_COMMA_CONCAT(,))
BASE_IMPL_DEFINE_DELEGATE(
    BASE_COMMA_CONCAT(typename A1, typename A2, typename A3, typename A4),
    BASE_COMMA_CONCAT(A1 a1, A2 a2, A3 a3, A4 a4),
    BASE_COMMA_CONCAT(a1, a2, a3, a4),
    BASE_COMMA_CONCAT(,))
BASE_IMPL_DEFINE_DELEGATE(
    BASE_COMMA_CONCAT(typename A1, typename A2, typename A3, typename A4, typename A5),
    BASE_COMMA_CONCAT(A1 a1, A2 a2, A3 a3, A4 a4, A5 a5),
    BASE_COMMA_CONCAT(a1, a2, a3, a4, a5),
    BASE_COMMA_CONCAT(,))
BASE_IMPL_DEFINE_DELEGATE(
    BASE_COMMA_CONCAT(typename A1, typename A2, typename A3, typename A4, typename A5, typename A6),
    BASE_COMMA_CONCAT(A1 a1, A2 a2, A3 a3, A4 a4, A5 a5, A6 a6),
    BASE_COMMA_CONCAT(a1, a2, a3, a4, a5, a6),
    BASE_COMMA_CONCAT(,))
BASE_IMPL_DEFINE_DELEGATE(
    BASE_COMMA_CONCAT(typename A1, typename A2, typename A3, typename A4, typename A5, typename A6, typename A7),
    BASE_COMMA_CONCAT(A1 a1, A2 a2, A3 a3, A4 a4, A5 a5, A6 a6, A7 a7),
    BASE_COMMA_CONCAT(a1, a2, a3, a4, a5, a6, a7),
    BASE_COMMA_CONCAT(,))
BASE_IMPL_DEFINE_DELEGATE(
    BASE_COMMA_CONCAT(typename A1, typename A2, typename A3, typename A4, typename A5, typename A6, typename A7, typename A8),
    BASE_COMMA_CONCAT(A1 a1, A2 a2, A3 a3, A4 a4, A5 a5, A6 a6, A7 a7, A8 a8),
    BASE_COMMA_CONCAT(a1, a2, a3, a4, a5, a6, a7, a8),
    BASE_COMMA_CONCAT(,))

/*
 * Despite what is stated in Sergey Ryazanov's article, Delegate comparison *can* be defined.
 * See http://stackoverflow.com/questions/1526912/fast-c-delegates
 */
template <typename Sig>
inline bool operator==(const Delegate<Sig> &a, const Delegate<Sig> &b)
{
    return a.code == b.code && a.data == b.data;
}
template <typename Sig>
inline bool operator!=(const Delegate<Sig> &a, const Delegate<Sig> &b)
{
    return a.code != b.code || a.data != b.data;
}

#define BASE_IMPL_make_delegate1(fn)    Base::impl::_DelegateMaker(fn).make<fn>()
#define BASE_IMPL_make_delegate2(fn, p) Base::impl::_DelegateMaker(fn).make<fn>(p)
#define BASE_IMPL_make_delegate(n, tuple) BASE_CONCAT(BASE_IMPL_make_delegate, n) tuple
#define make_delegate(...)              BASE_IMPL_make_delegate(BASE_PP_NARGS(__VA_ARGS__), (__VA_ARGS__))

}
}

#endif // BASE_IMPL_DELEGATE_HPP_INCLUDED
