/*
 * Base/impl/Iterable.hpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef BASE_IMPL_ITERABLE_HPP_INCLUDED
#define BASE_IMPL_ITERABLE_HPP_INCLUDED

#include <Base/impl/Defines.hpp>
#include <Base/impl/CArray.hpp>
#include <Base/impl/Object.hpp>

#define BASE_ITERABLE_ITEM_TYPE(expr)   \
    Base::impl::__IterableItemTypeDescriptor\
        <sizeof(Base::impl::__IterableItemTypeSelect(expr).selector)>::ItemType
#define BASE_ITERABLE_REGISTER_ITEM_TYPE(T, SIZ)\
    namespace Base { namespace impl {\
    template<> struct __IterableItemTypeDescriptor<SIZ> { typedef T ItemType; };\
    __IterableBoxedSelector<SIZ> __IterableItemTypeSelect(Iterable<T> *);\
    __IterableBoxedSelector<SIZ> __IterableItemTypeSelect(T *);\
    __IterableBoxedSelector<1> __ForeachStateSelect(Iterable<T> *);\
    __IterableBoxedSelector<2> __ForeachStateSelect(T *);\
    }}

#define BASE_IMPL_foreach(item, iter, num) \
    for (Base::impl::__ForeachState\
            <BASE_ITERABLE_ITEM_TYPE(iter), sizeof(Base::impl::__ForeachStateSelect(iter).selector)>\
        BASE_CONCAT(foreachState__, num)(iter); BASE_CONCAT(foreachState__, num);)\
        if (item = BASE_CONCAT(foreachState__, num).next())\
            goto BASE_CONCAT(foreachGoto__, num);\
        else\
            BASE_CONCAT(foreachGoto__, num):
#if defined(BASE_CONFIG_MSVC)
#define foreach(item, iter)             BASE_IMPL_foreach(item, iter, __COUNTER__)
#elif defined(BASE_CONFIG_GNUC)
#define foreach(item, iter)             BASE_IMPL_foreach(item, iter, __LINE__)
#endif

namespace Base {
namespace impl {

template <typename T>
struct Iterable : Interface
{
    virtual void *iterate(void *state, T *bufp[2]) = 0;
};

/* iterable item type registration */
template<size_t SIZ> struct __IterableBoxedSelector { char selector[SIZ]; };
template<size_t SIZ> struct __IterableItemTypeDescriptor;

/* foreach implementation */
template <typename T> struct __ForeachValue
{
    __ForeachValue(T value) : value(value)
    {
    }
    template <typename V> operator V()
    {
        return static_cast<V>(value);
    }
    T value;
};
template<typename T, size_t SIZ> struct __ForeachState;
template<typename T> struct __ForeachState<T, 1>
{
    __ForeachState(Iterable<T> *iter) : iterable(iter), state(0)
    {
        bufp[0] = bufp[1] = buf;
    }
    operator bool()
    {
        if (bufp[0] < bufp[1])
            return true;
        if (0 == iterable)
            return false;
        bufp[0] = buf;
        bufp[1] = buf + NELEMS(buf);
        state = iterable->iterate(state, bufp);
        return bufp[0] < bufp[1];
    }
    __ForeachValue<T> next()
    {
        return *(bufp[0]++);
    }
    Iterable<T> *iterable;
    void *state;
    T buf[16], *bufp[2];
};
template<typename T> struct __ForeachState<T, 2>
{
    __ForeachState(T *array)
    {
        bufp[0] = array;
        bufp[1] = array + carr_length(array);
    }
    operator bool()
    {
        return bufp[0] < bufp[1];
    }
    __ForeachValue<T> next()
    {
        return *(bufp[0]++);
    }
    T *bufp[2];
};

}
}

BASE_ITERABLE_REGISTER_ITEM_TYPE(void *, 0x766f6964/*ASCII: void*/)
BASE_ITERABLE_REGISTER_ITEM_TYPE(Base::impl::Object *, 0x4f626a/*ASCII: Obj*/)

#endif // BASE_IMPL_ITERABLE_HPP_INCLUDED
