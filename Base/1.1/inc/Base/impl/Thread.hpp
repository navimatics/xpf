/*
 * Base/impl/Thread.hpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef BASE_IMPL_THREAD_HPP_INCLUDED
#define BASE_IMPL_THREAD_HPP_INCLUDED

#include <Base/impl/Defines.hpp>
#include <Base/impl/Map.hpp>
#include <Base/impl/Object.hpp>
#include <Base/impl/Thr.hpp>

namespace Base {
namespace impl {

class BASE_APISYM Thread : public Object
{
public:
    static Thread *currentThread();
    static Map *currentThreadMap();
    static void exit();
    static void sleep(wait_timeout_t timeout = wait_timeout_infinite);
    Thread();
    Thread(void (*entry)(void *), void *arg);
    ~Thread();
    void start();
    void detach();
    void wait();
    virtual void quit();
    virtual bool isQuitting();
    thr_t thr();
    /* low-level slots */
    enum
    {
        _SlotObjectThreadLoopQueue,
        _SlotObjectError,
        _SlotObjectRandom,
        _SlotObjectCount,
        __SlotObjectMax = _SlotObjectCount - 1,
        _SlotErrorLogging,
#if defined(BASE_CONFIG_POSIX)
        _SlotJmpbuf,
#endif
        _SlotThreadLoopSignalTarget,
        _SlotThreadLoopActive,
        _SlotCount,
    };
    void *_slot(unsigned i);
    void _setSlot(unsigned i, void *p);
    Object *_slotObject(unsigned i);
    void _setSlotObject(unsigned i, Object *obj);
    /* low-level thread interruption mechanism */
    static void _interrupt(thr_t thr);
    void _interrupt();
protected:
    virtual void run();
private:
    static BASE_IMPL_THRENTRY(entry, arg);
private:
    Map *_map;
    void (*_entry)(void *);
    void *_arg;
    thr_t _thr;
    bool _detached;
    bool _quitting;
    void *_slots[_SlotCount];
};

inline thr_t Thread::thr()
{
    return _thr;
}
inline void *Thread::_slot(unsigned i)
{
    return _slots[i];
}
inline void Thread::_setSlot(unsigned i, void *p)
{
    _slots[i] = p;
}
inline Object *Thread::_slotObject(unsigned i)
{
    return (Object *)_slots[i];
}
inline void Thread::_setSlotObject(unsigned i, Object *obj)
{
    if (0 != obj)
        obj->retain();
    Object *o = (Object *)_slots[i];
    if (0 != o)
        o->release();
    _slots[i] = obj;
}

}
}

#endif // BASE_IMPL_THREAD_HPP_INCLUDED
