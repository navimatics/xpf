/*
 * Base/impl/Log.hpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef BASE_IMPL_LOG_HPP_INCLUDED
#define BASE_IMPL_LOG_HPP_INCLUDED

#include <Base/impl/Defines.hpp>

/*
 * LOG(format, ...)
 */
#define LOG(...)                        Base::impl::__LogWithTag(BASE_FUNCSIG, __VA_ARGS__)
#define LOG_HERE()                      LOG("%s:%u", Base::impl::__LogLastPathComponent(__FILE__), __LINE__)
#if defined(NDEBUG)
#define DEBUGLOG(...)                   ((void)0)
#define DEBUGLOG_HERE()                 ((void)0)
#else
#define DEBUGLOG(...)                   Base::impl::__LogWithTag(BASE_FUNCSIG, __VA_ARGS__)
#define DEBUGLOG_HERE()                 DEBUGLOG("%s:%u", Base::impl::__LogLastPathComponent(__FILE__), __LINE__)
#endif

/*
 * LOG_CALLSTACK(format, ...)
 */
#define LOG_CALLSTACK(...)              Base::impl::__LogCallStack(BASE_FUNCSIG, __VA_ARGS__)
#if defined(NDEBUG)
#define DEBUGLOG_CALLSTACK(...)         ((void)0)
#else
#define DEBUGLOG_CALLSTACK(...)         Base::impl::__LogCallStack(BASE_FUNCSIG, __VA_ARGS__)
#endif

/*
 * LOG_PERF(name)
 */
#define BASE_IMPL_LOG_PERF_STR(num)     #num
#define BASE_IMPL_LOG_PERF(num, ...)    \
    struct BASE_CONCAT(LogPerfGuard__, num)\
    {\
        const char *lit;\
        const char *tag;\
        BASE_CONCAT(LogPerfGuard__, num)(const char *lit, const char *tag)\
        {\
            this->lit = lit;\
            this->tag = tag;\
            Base::impl::__LogPerfBegin(lit);\
        }\
        ~BASE_CONCAT(LogPerfGuard__, num)()\
        {\
            Base::impl::__LogPerfEnd(lit, tag, __VA_ARGS__);\
        }\
    } BASE_CONCAT(perfBlock__, num)(__FILE__ ":" BASE_IMPL_LOG_PERF_STR(num), BASE_FUNCSIG);
#if defined(BASE_CONFIG_MSVC)
#define LOG_PERF(...)                   BASE_IMPL_LOG_PERF(__COUNTER__, __VA_ARGS__)
#elif defined(BASE_CONFIG_GNUC)
#define LOG_PERF(...)                   BASE_IMPL_LOG_PERF(__LINE__, __VA_ARGS__)
#endif
#if defined(NDEBUG)
#define DEBUGLOG_PERF(...)              ((void)0)
#else
#if defined(BASE_CONFIG_MSVC)
#define DEBUGLOG_PERF(...)              BASE_IMPL_LOG_PERF(__COUNTER__, __VA_ARGS__)
#elif defined(BASE_CONFIG_GNUC)
#define DEBUGLOG_PERF(...)              BASE_IMPL_LOG_PERF(__LINE__, __VA_ARGS__)
#endif
#endif

namespace Base {
namespace impl {

/*
 * LOG()
 */
BASE_APISYM void Log(const char *format, ...);
BASE_APISYM void Logv(const char *format, va_list ap);
BASE_APISYM void __LogWithTag(const char *tag, const char *format, ...);
BASE_APISYM const char *__LogLastPathComponent(const char *path);

/*
 * LOG_CALLSTACK()
 */
BASE_APISYM void __LogCallStack(const char *tag, const char *format, ...);

/*
 * LOG_PERF()
 */
BASE_APISYM void __LogPerfBegin(const char *lit);
BASE_APISYM void __LogPerfEnd(const char *lit, const char *tag, const char *name);

}
}

#endif // BASE_IMPL_LOG_HPP_INCLUDED
