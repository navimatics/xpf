/*
 * Base/impl/Object.hpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef BASE_IMPL_OBJECT_HPP_INCLUDED
#define BASE_IMPL_OBJECT_HPP_INCLUDED

#include <Base/impl/Defines.hpp>

#define BASE_IMPL_mark_and_collect(num) \
    if (Base::impl::__MarkAndCollectGuard BASE_CONCAT(markAndCollectGuard__, num) = 0)\
        goto BASE_CONCAT(markAndCollectGoto__, num);\
    else\
        BASE_CONCAT(markAndCollectGoto__, num):
#if defined(BASE_CONFIG_MSVC)
#define mark_and_collect                BASE_IMPL_mark_and_collect(__COUNTER__)
#elif defined(BASE_CONFIG_GNUC)
#define mark_and_collect                BASE_IMPL_mark_and_collect(__LINE__)
#endif

#define BASE_IMPL_ALLOCA_OBJECT1(T)\
    ((T *)((new (alloca(sizeof(T))) T)->linger()))
#define BASE_IMPL_ALLOCA_OBJECT2(T, a1)\
    ((T *)((new (alloca(sizeof(T))) T(a1))->linger()))
#define BASE_IMPL_ALLOCA_OBJECT3(T, a1, a2)\
    ((T *)((new (alloca(sizeof(T))) T(a1, a2))->linger()))
#define BASE_IMPL_ALLOCA_OBJECT4(T, a1, a2, a3)\
    ((T *)((new (alloca(sizeof(T))) T(a1, a2, a3))->linger()))
#define BASE_IMPL_ALLOCA_OBJECT5(T, a1, a2, a3, a4)\
    ((T *)((new (alloca(sizeof(T))) T(a1, a2, a3, a4)->linger()))
#define BASE_IMPL_ALLOCA_OBJECT6(T, a1, a2, a3, a4, a5)\
    ((T *)((new (alloca(sizeof(T))) T(a1, a2, a3, a4, a5))->linger()))
#define BASE_IMPL_ALLOCA_OBJECT7(T, a1, a2, a3, a4, a5, a6)\
    ((T *)((new (alloca(sizeof(T))) T(a1, a2, a3, a4, a5, a6))->linger()))
#define BASE_IMPL_ALLOCA_OBJECT8(T, a1, a2, a3, a4, a5, a6, a7)\
    ((T *)((new (alloca(sizeof(T))) T(a1, a2, a3, a4, a5, a6, a7))->linger()))
#define BASE_IMPL_ALLOCA_OBJECT9(T, a1, a2, a3, a4, a5, a6, a7, a8)\
    ((T *)((new (alloca(sizeof(T))) T(a1, a2, a3, a4, a5, a6, a7, a8))->linger()))
#define BASE_IMPL_ALLOCA_OBJECT(n, tuple)\
    BASE_CONCAT(BASE_IMPL_ALLOCA_OBJECT, n) tuple
#define BASE_ALLOCA_OBJECT(...)         BASE_IMPL_ALLOCA_OBJECT(BASE_PP_NARGS(__VA_ARGS__), (__VA_ARGS__))

namespace Base {
namespace impl {

struct BASE_APISYM collect_t {};
extern BASE_APISYM collect_t collect;

typedef signed int refcnt_t;

class BASE_APISYM Object
{
public:
    static void Mark();
    static void Collect();
    static void *operator new(size_t size, size_t extra = 0);
    static void *operator new(size_t size, const collect_t &, size_t extra = 0);
    static void *operator new(size_t size, const std::nothrow_t &, size_t extra = 0) throw ();
    static void *operator new(size_t size, void *p) throw ();
    static void operator delete(void *p) throw ();
    static void operator delete(void *p, size_t) throw ();
    static void operator delete(void *p, const collect_t &, size_t) throw ();
    static void operator delete(void *p, const std::nothrow_t &, size_t) throw ();
    static void operator delete(void *p, void *) throw ();
    static int qsort_cmp(const void *p1, const void *p2);
    Object();
    virtual ~Object();
    virtual Object *threadsafe();
    virtual Object *linger();
    virtual Object *retain();
    virtual void release();
    virtual Object *autorelease();
    virtual size_t hash();
    virtual int cmp(Object *obj);
    virtual const char *strrepr();
    const char *className();
    Object *autocollect();
private:
    Object(const Object &);
    Object &operator=(const Object &);
private:
    refcnt_t _refcnt;
};

inline Object::Object()
{
    /* BUGFIX:
     * The _refcnt field was originally initialized by Object::new() and this constructor
     * was empty. However this can break under C++03 value initialization, because the Object
     * may have its fields zero initialized *after* Object::new(). This happens when an Object
     * of type T is constructed using the syntax "new T()" (note the empty parentheses) and
     * the object class has no default constructor.
     *
     * The C++03 standard says:
     *     To value-initialize an object of type T means:
     *         — if T is a class type (clause 9) with a user-declared constructor (12.1),
     *           then the default constructor for T is called (and the initialization is
     *           ill-formed if T has no accessible default constructor);
     *         — if T is a non-union class type without a user-declared constructor,
     *           then every non-static data member and base-class component of T is value-initialized;
     *         — if T is an array type, then each element is value-initialized;
     *         — otherwise, the object is zero-initialized
     *
     * Now a class T derived from Object without a default constructor falls under the clause:
     * "if T is a non-union class type without a user-declared constructor, then every non-static
     * data member and base-class component of T is value-initialized". This means that the base
     * Object component of T should be value initialized and because it has a default constructor,
     * the constructor should be called and the _refcnt field should *not* be zeroed out.
     *
     * However Apple LLVM compiler 3.0 (clang) disagrees. Hence this bugfix.
     */
    _refcnt = 1;
}
inline Object::~Object()
{
}
inline Object *Object::autocollect()
{
    return retain()->autorelease();
}

template <typename T>
BASE_APISYM void obj_assign(T *&lhs, T *rhs);

template <typename T>
inline void obj_assign(T *&lhs, T *rhs)
{
    if (0 != rhs)
        rhs->retain();
    if (0 != lhs)
        lhs->release();
    lhs = rhs;
}

struct BASE_APISYM Interface
{
    virtual Object *self();
};

struct BASE_APISYM ObjectComparer : Interface
{
    virtual size_t object_hash(Object *obj) = 0;
    virtual int object_cmp(Object *lobj, Object *robj) = 0;
};

struct BASE_APISYM Object_hash_t
{
    size_t operator()(Object *a) const
    {
        if (0 == comparer)
            return a->hash();
        else
            return comparer->object_hash(a);
    }
    Object_hash_t(ObjectComparer *comparer) : comparer(comparer)
    {
    }
    ObjectComparer *comparer;
};
struct BASE_APISYM Object_equal_t
{
    bool operator()(Object *a, Object *b) const
    {
        if (0 == comparer)
            return 0 == a->cmp(b);
        else
            return 0 == comparer->object_cmp(a, b);
    }
    Object_equal_t(ObjectComparer *comparer) : comparer(comparer)
    {
    }
    ObjectComparer *comparer;
};
struct BASE_APISYM Object_less_t
{
    bool operator()(Object *a, Object *b) const
    {
        if (0 == comparer)
            return 0 > a->cmp(b);
        else
            return 0 > comparer->object_cmp(a, b);
    }
    Object_less_t(ObjectComparer *comparer) : comparer(comparer)
    {
    }
    ObjectComparer *comparer;
};

struct __MarkAndCollectGuard
{
    operator bool()
    {
        return false;
    }
    __MarkAndCollectGuard(int)
    {
        Object::Mark();
    }
    ~__MarkAndCollectGuard()
    {
        Object::Collect();
    }
};

}
}

#endif // BASE_IMPL_OBJECT_HPP_INCLUDED
