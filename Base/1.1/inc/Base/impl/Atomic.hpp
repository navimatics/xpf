/*
 * Base/impl/Atomic.hpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef BASE_IMPL_ATOMIC_HPP_INCLUDED
#define BASE_IMPL_ATOMIC_HPP_INCLUDED

#include <Base/impl/Defines.hpp>
#if defined(BASE_CONFIG_MSVC)
#include <windows.h>
#include <intrin.h>
#endif

namespace Base {
namespace impl {

/*
 * atomic_rmb()
 * atomic_wmb()
 * atomic_mb()
 * Read, write and read-write memory compiler and processor memory barriers.
 */
#if defined(BASE_CONFIG_GNUC)
/*
 * The __sync_synchronize() intrinsic will issue both a compiler and processor memory barrier.
 * See http://gcc.gnu.org/ml/gcc-help/2011-04/msg00166.html
 */
inline void atomic_rmb()
{
    __sync_synchronize();
}
inline void atomic_wmb()
{
    __sync_synchronize();
}
inline void atomic_mb()
{
    __sync_synchronize();
}
#elif defined(BASE_CONFIG_MSVC)
/*
 * The MemoryBarrier() macro will issue both a compiler and processor memory barrier.
 * See http://blogs.msdn.com/b/itgoestoeleven/archive/2008/03/16/how-does-kememorybarrier-work.aspx
 */
inline void atomic_rmb()
{
    MemoryBarrier();
}
inline void atomic_wmb()
{
    MemoryBarrier();
}
inline void atomic_mb()
{
    MemoryBarrier();
}
#endif

/*
 * atomic_inc()
 * Atomically increment an integer variable.
 *
 * This function also acts as a memory barrier for both compiler and processor.
 */
#if defined(BASE_CONFIG_GNUC)
inline int atomic_inc(int &v)
{
    return __sync_add_and_fetch(&v, 1);
}
inline unsigned int atomic_inc(unsigned int &v)
{
    return __sync_add_and_fetch(&v, 1);
}
inline long atomic_inc(long &v)
{
    return __sync_add_and_fetch(&v, 1);
}
inline unsigned long atomic_inc(unsigned long &v)
{
    return __sync_add_and_fetch(&v, 1);
}
#if defined(BASE_CONFIG_ARCH64)
inline long long atomic_inc(long long &v)
{
    return __sync_add_and_fetch(&v, 1);
}
inline unsigned long long atomic_inc(unsigned long long &v)
{
    return __sync_add_and_fetch(&v, 1);
}
#else
inline long long atomic_inc(long long &v)
{
    abort();
}
inline unsigned long long atomic_inc(unsigned long long &v)
{
    abort();
}
#endif
#elif defined(BASE_CONFIG_MSVC)
inline int atomic_inc(int &v)
{
    BASE_COMPILE_ASSERT(sizeof v == sizeof(long));
    return _InterlockedIncrement((long *)&v);
}
inline unsigned int atomic_inc(unsigned int &v)
{
    BASE_COMPILE_ASSERT(sizeof v == sizeof(long));
    return _InterlockedIncrement((long *)&v);
}
inline long atomic_inc(long &v)
{
    BASE_COMPILE_ASSERT(sizeof v == sizeof(long));
    return _InterlockedIncrement((long *)&v);
}
inline unsigned long atomic_inc(unsigned long &v)
{
    BASE_COMPILE_ASSERT(sizeof v == sizeof(long));
    return _InterlockedIncrement((long *)&v);
}
#if defined(BASE_CONFIG_ARCH64)
inline long long atomic_inc(long long &v)
{
    BASE_COMPILE_ASSERT(sizeof v == sizeof(__int64));
    return _InterlockedIncrement64((__int64 *)&v);
}
inline unsigned long long atomic_inc(unsigned long long &v)
{
    BASE_COMPILE_ASSERT(sizeof v == sizeof(__int64));
    return _InterlockedIncrement64((__int64 *)&v);
}
#else
inline long long atomic_inc(long long &v)
{
    abort();
}
inline unsigned long long atomic_inc(unsigned long long &v)
{
    abort();
}
#endif
#endif

/*
 * atomic_dec()
 * Atomically decrement an integer variable.
 *
 * This function also acts as a memory barrier for both compiler and processor.
 */
#if defined(BASE_CONFIG_GNUC)
inline int atomic_dec(int &v)
{
    return __sync_sub_and_fetch(&v, 1);
}
inline unsigned int atomic_dec(unsigned int &v)
{
    return __sync_sub_and_fetch(&v, 1);
}
inline long atomic_dec(long &v)
{
    return __sync_sub_and_fetch(&v, 1);
}
inline unsigned long atomic_dec(unsigned long &v)
{
    return __sync_sub_and_fetch(&v, 1);
}
#if defined(BASE_CONFIG_ARCH64)
inline long long atomic_dec(long long &v)
{
    return __sync_sub_and_fetch(&v, 1);
}
inline unsigned long long atomic_dec(unsigned long long &v)
{
    return __sync_sub_and_fetch(&v, 1);
}
#else
inline long long atomic_dec(long long &v)
{
    abort();
}
inline unsigned long long atomic_dec(unsigned long long &v)
{
    abort();
}
#endif
#elif defined(BASE_CONFIG_MSVC)
inline int atomic_dec(int &v)
{
    BASE_COMPILE_ASSERT(sizeof v == sizeof(long));
    return _InterlockedDecrement((long *)&v);
}
inline unsigned int atomic_dec(unsigned int &v)
{
    BASE_COMPILE_ASSERT(sizeof v == sizeof(long));
    return _InterlockedDecrement((long *)&v);
}
inline long atomic_dec(long &v)
{
    BASE_COMPILE_ASSERT(sizeof v == sizeof(long));
    return _InterlockedDecrement((long *)&v);
}
inline unsigned long atomic_dec(unsigned long &v)
{
    BASE_COMPILE_ASSERT(sizeof v == sizeof(long));
    return _InterlockedDecrement((long *)&v);
}
#if defined(BASE_CONFIG_ARCH64)
inline long long atomic_dec(long long &v)
{
    BASE_COMPILE_ASSERT(sizeof v == sizeof(__int64));
    return _InterlockedDecrement64((__int64 *)&v);
}
inline unsigned long long atomic_dec(unsigned long long &v)
{
    BASE_COMPILE_ASSERT(sizeof v == sizeof(__int64));
    return _InterlockedDecrement64((__int64 *)&v);
}
#else
inline long long atomic_dec(long long &v)
{
    abort();
}
inline unsigned long long atomic_dec(unsigned long long &v)
{
    abort();
}
#endif
#endif

/*
 * atomic_cas()
 * Atomically compare-and-swap an integer variable.
 *
 * Atomic_cas() implements the following operation atomically:
 *
 * Type cas(Type &v, Type oldv, Type newv)
 * {
 *     Type tmpv = v;
 *     if (v == oldv)
 *         v = newv;
 *     return tmpv;
 * }
 *
 * This function also acts as a memory barrier for both compiler and processor.
 */
#if defined(BASE_CONFIG_GNUC)
inline int atomic_cas(int &v, int oldv, int newv)
{
    return __sync_val_compare_and_swap(&v, oldv, newv);
}
inline unsigned int atomic_cas(unsigned int &v, unsigned int oldv, unsigned int newv)
{
    return __sync_val_compare_and_swap(&v, oldv, newv);
}
inline long atomic_cas(long &v, long oldv, long newv)
{
    return __sync_val_compare_and_swap(&v, oldv, newv);
}
inline unsigned long atomic_cas(unsigned long &v, unsigned long oldv, unsigned long newv)
{
    return __sync_val_compare_and_swap(&v, oldv, newv);
}
#if defined(BASE_CONFIG_ARCH64)
inline long long atomic_cas(long long &v, long long oldv, long long newv)
{
    return __sync_val_compare_and_swap(&v, oldv, newv);
}
inline unsigned long long atomic_cas(unsigned long long &v, unsigned long long oldv, unsigned long long newv)
{
    return __sync_val_compare_and_swap(&v, oldv, newv);
}
#else
inline long long atomic_cas(long long &v, long long oldv, long long newv)
{
    abort();
}
inline unsigned long long atomic_cas(unsigned long long &v, unsigned long long oldv, unsigned long long newv)
{
    abort();
}
#endif
#elif defined(BASE_CONFIG_MSVC)
inline int atomic_cas(int &v, int oldv, int newv)
{
    BASE_COMPILE_ASSERT(sizeof v == sizeof(long));
    return _InterlockedCompareExchange((long *)&v, oldv, newv);
}
inline unsigned int atomic_cas(unsigned int &v, unsigned int oldv, unsigned int newv)
{
    BASE_COMPILE_ASSERT(sizeof v == sizeof(long));
    return _InterlockedCompareExchange((long *)&v, oldv, newv);
}
inline long atomic_cas(long &v, long oldv, long newv)
{
    BASE_COMPILE_ASSERT(sizeof v == sizeof(long));
    return _InterlockedCompareExchange((long *)&v, oldv, newv);
}
inline unsigned long atomic_cas(unsigned long &v, unsigned long oldv, unsigned long newv)
{
    BASE_COMPILE_ASSERT(sizeof v == sizeof(long));
    return _InterlockedCompareExchange((long *)&v, oldv, newv);
}
#if defined(BASE_CONFIG_ARCH64)
inline long long atomic_cas(long long &v, long long oldv, long long newv)
{
    BASE_COMPILE_ASSERT(sizeof v == sizeof(__int64));
    return _InterlockedCompareExchange64((__int64 *)&v, oldv, newv);
}
inline unsigned long long atomic_cas(unsigned long long &v, unsigned long long oldv, unsigned long long newv)
{
    BASE_COMPILE_ASSERT(sizeof v == sizeof(__int64));
    return _InterlockedCompareExchange64((__int64 *)&v, oldv, newv);
}
#else
inline long long atomic_cas(long long &v, long long oldv, long long newv)
{
    abort();
}
inline unsigned long long atomic_cas(unsigned long long &v, unsigned long long oldv, unsigned long long newv)
{
    abort();
}
#endif
#endif

/*
 * atomic_lock_acquire()
 * Atomically write a non-0 value to an integer variable and return its old value.
 *
 * This function is not a full barrier, but rather an acquire barrier. This means that references
 * after the function cannot move to (or be speculated to) before the function, but previous memory
 * stores may not be globally visible yet, and previous memory loads may not yet be satisfied.
 */
#if defined(BASE_CONFIG_GNUC)
inline int atomic_lock_acquire(int &v)
{
    return __sync_lock_test_and_set(&v, 1);
}
inline unsigned int atomic_lock_acquire(unsigned int &v)
{
    return __sync_lock_test_and_set(&v, 1);
}
inline long atomic_lock_acquire(long &v)
{
    return __sync_lock_test_and_set(&v, 1);
}
inline unsigned long atomic_lock_acquire(unsigned long &v)
{
    return __sync_lock_test_and_set(&v, 1);
}
#if defined(BASE_CONFIG_ARCH64)
inline long long atomic_lock_acquire(long long &v)
{
    return __sync_lock_test_and_set(&v, 1);
}
inline unsigned long long atomic_lock_acquire(unsigned long long &v)
{
    return __sync_lock_test_and_set(&v, 1);
}
#else
inline long long atomic_lock_acquire(long long &v)
{
    abort();
}
inline unsigned long long atomic_lock_acquire(unsigned long long &v)
{
    abort();
}
#endif
#elif defined(BASE_CONFIG_MSVC)
inline int atomic_lock_acquire(int &v)
{
    BASE_COMPILE_ASSERT(sizeof v == sizeof(long));
    return _InterlockedExchange((long *)&v, 1);
}
inline unsigned int atomic_lock_acquire(unsigned int &v)
{
    BASE_COMPILE_ASSERT(sizeof v == sizeof(long));
    return _InterlockedExchange((long *)&v, 1);
}
inline long atomic_lock_acquire(long &v)
{
    BASE_COMPILE_ASSERT(sizeof v == sizeof(long));
    return _InterlockedExchange((long *)&v, 1);
}
inline unsigned long atomic_lock_acquire(unsigned long &v)
{
    BASE_COMPILE_ASSERT(sizeof v == sizeof(long));
    return _InterlockedExchange((long *)&v, 1);
}
#if defined(BASE_CONFIG_ARCH64)
inline long long atomic_lock_acquire(long long &v)
{
    BASE_COMPILE_ASSERT(sizeof v == sizeof(__int64));
    return _InterlockedExchange64((__int64 *)&v, 1);
}
inline unsigned long long atomic_lock_acquire(unsigned long long &v)
{
    BASE_COMPILE_ASSERT(sizeof v == sizeof(__int64));
    return _InterlockedExchange64((__int64 *)&v, 1);
}
#else
inline long long atomic_lock_acquire(long long &v)
{
    abort();
}
inline unsigned long long atomic_lock_acquire(unsigned long long &v)
{
    abort();
}
#endif
#endif

/*
 * atomic_lock_release()
 * Atomically write a 0 value to an integer variable.
 *
 * This function is not a full barrier, but rather a release barrier. This means that all previous
 * memory stores are globally visible, and all previous memory loads have been satisfied, but
 * following memory reads are not prevented from being speculated to before the barrier.
 */
#if defined(BASE_CONFIG_GNUC)
inline void atomic_lock_release(int &v)
{
    __sync_lock_release(&v);
}
inline void atomic_lock_release(unsigned int &v)
{
    __sync_lock_release(&v);
}
inline void atomic_lock_release(long &v)
{
    __sync_lock_release(&v);
}
inline void atomic_lock_release(unsigned long &v)
{
    __sync_lock_release(&v);
}
#if defined(BASE_CONFIG_ARCH64)
inline void atomic_lock_release(long long &v)
{
    __sync_lock_release(&v);
}
inline void atomic_lock_release(unsigned long long &v)
{
    __sync_lock_release(&v);
}
#else
inline void atomic_lock_release(long long &v)
{
    abort();
}
inline void atomic_lock_release(unsigned long long &v)
{
    abort();
}
#endif
#elif defined(BASE_CONFIG_MSVC)
inline void atomic_lock_release(int &v)
{
    BASE_COMPILE_ASSERT(sizeof v == sizeof(long));
    _InterlockedExchange((long *)&v, 0);
}
inline void atomic_lock_release(unsigned int &v)
{
    BASE_COMPILE_ASSERT(sizeof v == sizeof(long));
    _InterlockedExchange((long *)&v, 0);
}
inline void atomic_lock_release(long &v)
{
    BASE_COMPILE_ASSERT(sizeof v == sizeof(long));
    _InterlockedExchange((long *)&v, 0);
}
inline void atomic_lock_release(unsigned long &v)
{
    BASE_COMPILE_ASSERT(sizeof v == sizeof(long));
    _InterlockedExchange((long *)&v, 0);
}
#if defined(BASE_CONFIG_ARCH64)
inline void atomic_lock_release(long long &v)
{
    BASE_COMPILE_ASSERT(sizeof v == sizeof(__int64));
    _InterlockedExchange64((__int64 *)&v, 0);
}
inline void atomic_lock_release(unsigned long long &v)
{
    BASE_COMPILE_ASSERT(sizeof v == sizeof(__int64));
    _InterlockedExchange64((__int64 *)&v, 0);
}
#else
inline void atomic_lock_release(long long &v)
{
    abort();
}
inline void atomic_lock_release(unsigned long long &v)
{
    abort();
}
#endif
#endif

}
}

#endif // BASE_IMPL_ATOMIC_HPP_INCLUDED
