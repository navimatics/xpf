/*
 * Base/impl/ApplicationGuard.hpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef BASE_IMPL_APPLICATIONGUARD_HPP_INCLUDED
#define BASE_IMPL_APPLICATIONGUARD_HPP_INCLUDED

#include <Base/impl/Defines.hpp>

namespace Base {
namespace impl {

BASE_APISYM bool _ApplicationGuard(const char *path, FILE **filep = 0);
BASE_APISYM bool ApplicationGuard(const char *path, FILE **filep = 0);

}
}

#endif // BASE_IMPL_APPLICATIONGUARD_HPP_INCLUDED
