/*
 * Base/impl/DateTime.hpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef BASE_IMPL_DATETIME_HPP_INCLUDED
#define BASE_IMPL_DATETIME_HPP_INCLUDED

#include <Base/impl/Defines.hpp>
#include <Base/impl/Object.hpp>

namespace Base {
namespace impl {

class BASE_APISYM DateTime : public Object
{
public:
    enum
    {
        Sunday = 0,
        Monday,
        Tuesday,
        Wednesday,
        Thursday,
        Friday,
        Saturday,
    };
    struct Components
    {
        int year, month, day;
        int hour, min, sec, msec;
        int dayOfWeek;                  /* output only */
    };
public:
    static const char *format(Components c, bool extended = false);
    static Components parse(const char *s, const char **endp = 0);
    static double julianDateFromComponents(Components c);
    static Components componentsFromJulianDate(double j);
    static double currentJulianDate();
    DateTime();
    DateTime(Components c);
    DateTime(int year, int month, int day,
        int hour = 0, int min = 0, int sec = 0, int msec = 0);
    DateTime(double julianDate);
    DateTime(time_t unixTime);
    Components components();
    double julianDate();
    time_t unixTime();
    const char *strrepr();
private:
    void c2j(const Components &components);
    void j2c(Components &components);
private:
    int64_t _j;
};

}
}

#endif // BASE_IMPL_DATETIME_HPP_INCLUDED
