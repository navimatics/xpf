/*
 * Base/impl/Value.hpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef BASE_IMPL_VALUE_HPP_INCLUDED
#define BASE_IMPL_VALUE_HPP_INCLUDED

#include <Base/impl/Defines.hpp>
#include <Base/impl/Object.hpp>

namespace Base {
namespace impl {

class BASE_APISYM Value : public Object
{
public:
    static Value *value(bool B);
    static Value *value(short s);
    static Value *value(int i);
    static Value *value(long l);
    static Value *value(float f);
    static Value *value(double d);
    static Value *create(bool B);
    static Value *create(short s);
    static Value *create(int i);
    static Value *create(long l);
    static Value *create(float f);
    static Value *create(double d);
    int kind();
    bool booleanValue();
    long integerValue();
    double realValue();
    size_t hash();
    int cmp(Object *obj);
    const char *strrepr();
private:
    static Value *_staticValue(bool B);
    static Value *_staticValue(long l);
    static Value *_staticValue(double d);
    Value();
private:
    int _kind;
    union
    {
        bool _b;
        long _l;
        double _d;
    };
};

inline Value *Value::value(bool B)
{
    return (Value *)create(B)->autorelease();
}
inline Value *Value::value(short s)
{
    return (Value *)create(s)->autorelease();
}
inline Value *Value::value(int i)
{
    return (Value *)create(i)->autorelease();
}
inline Value *Value::value(long l)
{
    return (Value *)create(l)->autorelease();
}
inline Value *Value::value(float f)
{
    return (Value *)create(f)->autorelease();
}
inline Value *Value::value(double d)
{
    return (Value *)create(d)->autorelease();
}
inline Value *Value::create(short s)
{
    return create((long)s);
}
inline Value *Value::create(int i)
{
    return create((long)i);
}
inline Value *Value::create(float f)
{
    return create((double)f);
}
inline int Value::kind()
{
    return _kind;
}

inline Value::Value()
{
}

}
}

#endif // BASE_IMPL_VALUE_HPP_INCLUDED
