/*
 * Base/impl/ObjectReader.hpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef BASE_IMPL_OBJECTREADER_HPP_INCLUDED
#define BASE_IMPL_OBJECTREADER_HPP_INCLUDED

#include <Base/impl/Defines.hpp>
#include <Base/impl/Array.hpp>
#include <Base/impl/CString.hpp>
#include <Base/impl/JsonParser.hpp>
#include <Base/impl/Map.hpp>
#include <Base/impl/ObjectCodec.hpp>
#include <Base/impl/Set.hpp>

namespace Base {
namespace impl {

class BASE_APISYM ObjectReader : public JsonParser
{
public:
    ObjectReader(Stream *stream = 0);
    ~ObjectReader();
    void reset();
    void addType(const char *type);
    void removeType(const char *type);
    Object *readObject();
        /* the returned object is valid until the next call to readObject() or ~ObjectReader() */
    bool finished();
    void setFinished(bool value);
    Object *ancestorObject(ssize_t index);
protected:
    virtual void objectComplete(Object *obj);
    void startArray();
    void endArray();
    void startObject();
    void endObject();
    void nullValue();
    void booleanValue(bool B);
    void integerValue(long l);
    void realValue(double d);
    void characterData(const char *s, size_t len, bool isFinal, bool isKey);
private:
    Object *getCurrentObject();
    ObjectCodec::PropertyDescr *getCurrentPropertyDescr();
    void dynamicAddObject(Object *anc, Object *obj);
private:
    bool _finished;
    Set *_typeset;
    Array *_stack;
    Array *_keystack;
    CString *_text;
    Map *_idmap;
    Array *_queue;
    size_t _qindex;
};

}
}

#endif // BASE_IMPL_OBJECTREADER_HPP_INCLUDED
