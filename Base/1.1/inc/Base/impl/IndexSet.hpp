/*
 * Base/impl/IndexSet.hpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef BASE_IMPL_INDEXSET_HPP_INCLUDED
#define BASE_IMPL_INDEXSET_HPP_INCLUDED

#include <Base/impl/Defines.hpp>
#include <Base/impl/Array.hpp>
#include <Base/impl/RadixTree.hpp>
#include <Base/impl/ObjectCodec.hpp>

namespace Base {
namespace impl {

class BASE_APISYM IndexSet : public RadixTree, public Iterable<index_t>
{
public:
    IndexSet(size_t capacity = 0);
    size_t count();
    bool containsIndex(index_t index);
    index_t anyIndex();
    void addIndex(index_t index);
    void removeIndex(index_t index);
    void removeAllIndexes();
    index_t *indexes();
    const char *strrepr();
    /* Iterable<index_t> */
    void *iterate(void *state, index_t *bufp[2]);
private:
    struct Iterator : public NodeIterator
    {
        Iterator(IndexSet *tree) : NodeIterator(tree)
        {
        }
        int lineno;
        index_t buf[16], *bufp[2];
        index_t index, value;
    };
private:
    size_t _count;
    /* ObjectCodec use */
    Array *__array();
    void __setArray(Array *array);
    ObjectCodecDeclare();
};

}
}

#endif // BASE_IMPL_INDEXSET_HPP_INCLUDED
