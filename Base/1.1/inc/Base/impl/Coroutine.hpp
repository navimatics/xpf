/*
 * Base/impl/Coroutine.hpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef BASE_IMPL_COROUTINE_HPP_INCLUDED
#define BASE_IMPL_COROUTINE_HPP_INCLUDED

#include <Base/impl/Defines.hpp>

#define BASE_IMPL_coroyield(state, num) \
    do { (state)->lineno = num; goto coroyield__; case num:; } while (0)
#define coroblock(state)                if (0) coroyield__:; else switch ((state)->lineno) case 0:
#if defined(BASE_CONFIG_MSVC)
#define coroyield(state)                BASE_IMPL_coroyield(state, __COUNTER__)
#elif defined(BASE_CONFIG_GNUC)
#define coroyield(state)                BASE_IMPL_coroyield(state, __LINE__)
#endif
#define corofini(state)                 do { (state)->lineno = -1; goto coroyield__; } while (0)

namespace Base {
namespace impl {

#if defined(BASE_CONFIG_MSVC)
/* use __COUNTER__ once to ensure >0 */
#define BASE_IMPL_COROUTINE_DUMMY_NAME  BASE_CONCAT(BASE_IMPL_COROUTINE_dummy_, __COUNTER__)
struct BASE_IMPL_COROUTINE_DUMMY_NAME;
#endif

}
}

#endif // BASE_IMPL_COROUTINE_HPP_INCLUDED
