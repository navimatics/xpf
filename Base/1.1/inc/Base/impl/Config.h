/*
 * Base/impl/Config.h
 * Can be used from C or C++.
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef BASE_IMPL_CONFIG_H_INCLUDED
#define BASE_IMPL_CONFIG_H_INCLUDED

#if defined(_MSC_VER)
#define BASE_CONFIG_MSVC
#elif defined(__GNUC__)
#define BASE_CONFIG_GNUC
#else
#error Unknown compiler
#endif

#if defined(_WIN64)
#define BASE_CONFIG_WIN64
#define BASE_CONFIG_WINDOWS
#elif defined(_WIN32)
#define BASE_CONFIG_WIN32
#define BASE_CONFIG_WINDOWS
#elif defined(__CYGWIN__)
#define BASE_CONFIG_CYGWIN
#define BASE_CONFIG_POSIX
#elif defined(__APPLE__)
#include <TargetConditionals.h>
#define BASE_CONFIG_DARWIN
#define BASE_CONFIG_POSIX
#if TARGET_OS_IPHONE
#define BASE_CONFIG_DARWIN_IOS
#elif TARGET_OS_MAC
#define BASE_CONFIG_DARWIN_MAC
#endif
#elif defined(ANDROID)
#define BASE_CONFIG_ANDROID
#define BASE_CONFIG_POSIX
#define BASE_CONFIG_JVM
#elif defined(__linux__)
#define BASE_CONFIG_LINUX
#define BASE_CONFIG_POSIX
#else
#error Unknown platform
#endif

#if defined(_WIN64)
#define BASE_CONFIG_ARCH64
#define BASE_CONFIG_P64
#elif defined(_WIN32)
#define BASE_CONFIG_ARCH32
#elif defined(__LP64__)
#define BASE_CONFIG_ARCH64
#define BASE_CONFIG_LP64
#else
#define BASE_CONFIG_ARCH32
#endif

#endif // BASE_IMPL_CONFIG_H_INCLUDED
