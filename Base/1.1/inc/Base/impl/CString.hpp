/*
 * Base/impl/CString.hpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef BASE_IMPL_CSTRING_HPP_INCLUDED
#define BASE_IMPL_CSTRING_HPP_INCLUDED

#include <Base/impl/Defines.hpp>
#include <Base/impl/Object.hpp>

#define CStringConstant(literal)        CString::__constant(literal "", sizeof(literal) - 1)

namespace Base {
namespace impl {

class BASE_APISYM CString : public Object
{
public:
    static CString *empty();
    static CString *create(const char *s, size_t length = -1);
    static CString *createWithCapacity(size_t capacity);
    static CString *createf(const char *format, ...);
    static CString *vcreatef(const char *format, va_list ap);
    static CString *__constant(const char *literal, size_t length);
    size_t hash();
    int cmp(Object *obj);
    const char *strrepr();
    size_t length();
    void setLength(size_t value);
    const char *chars();
    CString *slice(ssize_t i, ssize_t j = 0);
    CString *_concat(const char *s, size_t length = -1);
    void makeImmutable();
    static ObjectComparer *comparer();
    static ObjectComparer *caseInsensitiveComparer();
private:
    CString();
private:
    size_t _length;
    size_t _metric;
};

inline size_t CString::length()
{
    return _length;
}
inline const char *CString::chars()
{
    return (char *)this + sizeof(CString);
}
inline CString::CString()
{
}

BASE_APISYM const char *cstring(const char *s, size_t length = -1);
BASE_APISYM const char *cstringWithCapacity(size_t capacity);
BASE_APISYM const char *cstringf(const char *format, ...);
BASE_APISYM const char *cstr_new(const char *s, size_t length = -1);
BASE_APISYM const char *cstr_newWithCapacity(size_t capacity);
BASE_APISYM const char *cstr_newf(const char *format, ...);
BASE_APISYM void cstr_assign(const char *&s, const char *t);
BASE_APISYM void cstr_assign_copy(const char *&s, const char *t, size_t length = -1);
BASE_APISYM CString *cstr_obj(const char *s);
BASE_APISYM void cstr_retain(const char *s);
BASE_APISYM void cstr_release(const char *s);
BASE_APISYM void cstr_autorelease(const char *s);
BASE_APISYM void cstr_autocollect(const char *s);
BASE_APISYM size_t cstr_length(const char *s);
BASE_APISYM const char *cstr_slice(const char *s, ssize_t i, ssize_t j = 0);
BASE_APISYM const char *cstr_concat(const char *s, const char *t, size_t length = -1);

inline void cstr_assign(const char *&s, const char *t)
{
    if (0 != t)
        ((CString *)(t - sizeof(CString)))->retain();
    if (0 != s)
        ((CString *)(s - sizeof(CString)))->release();
    s = t;
}
inline void cstr_assign_copy(const char *&s, const char *t, size_t length)
{
    if (0 != t)
        t = cstr_new(t, length);
    if (0 != s)
        ((CString *)(s - sizeof(CString)))->release();
    s = t;
}
inline CString *cstr_obj(const char *s)
{
    if (0 == s)
        return 0;
    return (CString *)(s - sizeof(CString));
}
inline void cstr_retain(const char *s)
{
    if (0 != s)
        ((CString *)(s - sizeof(CString)))->retain();
}
inline void cstr_release(const char *s)
{
    if (0 != s)
        ((CString *)(s - sizeof(CString)))->release();
}
inline void cstr_autorelease(const char *s)
{
    if (0 != s)
        ((CString *)(s - sizeof(CString)))->autorelease();
}
inline void cstr_autocollect(const char *s)
{
    if (0 != s)
        ((CString *)(s - sizeof(CString)))->autocollect();
}
inline size_t cstr_length(const char *s)
{
    if (0 == s)
        return 0;
    return ((CString *)(s - sizeof(CString)))->length();
}
inline const char *cstr_slice(const char *s, ssize_t i, ssize_t j)
{
    if (0 == s)
        return 0;
    return ((CString *)(s - sizeof(CString)))->slice(i, j)->chars();
}

enum
{
    FnmNoEscape                         = 0x01, /* disable backslash escaping */
    FnmPathname                         = 0x02, /* slash must be matched by slash (slash/backslash on Windows) */
    FnmPeriod                           = 0x04, /* period must be matched by period */
    FnmLeadingDir                       = 0x08, /* ignore /<tail> after Imatch */
    FnmCaseFold                         = 0x10, /* case insensitive search */
    FnmCurlyBraces                      = 0x20, /* support curly brace expansion */
};
BASE_APISYM bool cstr_fnmatch(const char *pattern, const char *string, int flags = 0);

BASE_APISYM const char *cstr_path_realpath(const char *src);
BASE_APISYM const char *cstr_path_normalize(const char *src, bool resolveParent = false);
BASE_APISYM const char *cstr_path_concat(const char *src1, const char *src2, bool resolveParent = false);
BASE_APISYM const char *cstr_path_basename(const char *src);
BASE_APISYM const char *cstr_path_extension(const char *src);
BASE_APISYM const char *cstr_path_setBasename(const char *src1, const char *src2);
BASE_APISYM const char *cstr_path_setExtension(const char *src1, const char *src2);

struct BASE_APISYM cstr_uri_parts
{
    const char *scheme;
    const char *userinfo;
    const char *host;
    const char *port;
    const char *path;
    const char *query;
    const char *fragment;
};
BASE_APISYM void cstr_uri_parseraw(const char *src, cstr_uri_parts *parts);
BASE_APISYM const char *cstr_uri_composeraw(const cstr_uri_parts *parts);
BASE_APISYM void cstr_uri_parse(const char *src, cstr_uri_parts *parts, bool query_processed = false);
BASE_APISYM const char *cstr_uri_compose(const cstr_uri_parts *parts, bool query_processed = false);
BASE_APISYM const char *cstr_uri_normalize(const char *src, bool query_processed = false);
BASE_APISYM const char *cstr_uri_concat(const char *src1, const char *src2, bool query_processed = false);
BASE_APISYM const char *cstr_uri_escape(const char *src, const char *exclude = 0);
BASE_APISYM const char *cstr_uri_unescape(const char *src);
BASE_APISYM const char *cstr_uri_query_extract(const char *query, const char **keyp, const char **valp);
BASE_APISYM const char *cstr_uri_query_concat(const char *query, const char **components, size_t ncomponents);

class BASE_APISYM TransientCString : public Object
{
public:
    TransientCString(const char *s);
    size_t hash();
    int cmp(Object *obj);
    const char *strrepr();
    size_t length();
    const char *chars();
    void setChars(const char *s);
private:
    size_t _length;
    size_t _metric;
    const char *_chars;
};

inline TransientCString::TransientCString(const char *s)
{
    setChars(s);
}
inline size_t TransientCString::length()
{
    return _length;
}
inline const char *TransientCString::chars()
{
    return _chars;
}

}
}

#endif // BASE_IMPL_CSTRING_HPP_INCLUDED
