/*
 * Base/Iterable.hpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */
/**
 * Definitions for iterating over containers in an easy and efficient manner.
 *
 * Base::Iterable<T>
 *     This interface should be implemented by containers that wish to allow easy iteration
 *     using the foreach macro.
 *
 * foreach(type var, iterable)
 *     This macro can be used to iterate over the values in an iterable (usually a container).
 *     The enclosed scope will be entered once for each value in the iterable with the value
 *     assigned to the variable var.
 *
 *     // iterate over every key in a map
 *     foreach (CString *key, map)
 *     {
 *         // use the key
 *         ...
 *     }
 */

#ifndef BASE_ITERABLE_HPP_INCLUDED
#define BASE_ITERABLE_HPP_INCLUDED

#include <Base/Defines.hpp>
#include <Base/Object.hpp>
#include <Base/impl/Iterable.hpp>

namespace Base {

using Base::impl::Iterable;

}

#endif // BASE_ITERABLE_HPP_INCLUDED
