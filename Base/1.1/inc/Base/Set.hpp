/*
 * Base/Set.hpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef BASE_SET_HPP_INCLUDED
#define BASE_SET_HPP_INCLUDED

#include <Base/Defines.hpp>
#include <Base/Object.hpp>
#include <Base/impl/Set.hpp>

namespace Base {

using Base::impl::Set;
using Base::impl::GenericSet;

}

#endif // BASE_SET_HPP_INCLUDED
