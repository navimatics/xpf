/*
 * Base/ZlibStream.hpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef BASE_ZLIBSTREAM_HPP_INCLUDED
#define BASE_ZLIBSTREAM_HPP_INCLUDED

#include <Base/Defines.hpp>
#include <Base/Stream.hpp>
#include <Base/impl/ZlibStream.hpp>

namespace Base {

using Base::impl::ZlibStream;

}

#endif // BASE_ZLIBSTREAM_HPP_INCLUDED
