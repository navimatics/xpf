/*
 * Base/ObjectWriter.hpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef BASE_OBJECTWRITER_HPP_INCLUDED
#define BASE_OBJECTWRITER_HPP_INCLUDED

#include <Base/Defines.hpp>
#include <Base/JsonWriter.hpp>
#include <Base/impl/ObjectWriter.hpp>

namespace Base {

using Base::impl::ObjectWriter;

}

#endif // BASE_OBJECTWRITER_HPP_INCLUDED
