/*
 * Base/SystemStream.hpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef BASE_SYSTEMSTREAM_HPP_INCLUDED
#define BASE_SYSTEMSTREAM_HPP_INCLUDED

#include <Base/Defines.hpp>
#include <Base/Stream.hpp>
#include <Base/impl/SystemStream.hpp>

namespace Base {

using Base::impl::SystemStream;

}

#endif // BASE_SYSTEMSTREAM_HPP_INCLUDED
