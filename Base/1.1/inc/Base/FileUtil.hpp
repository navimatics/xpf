/*
 * Base/FileUtil.hpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef BASE_FILEUTIL_HPP_INCLUDED
#define BASE_FILEUTIL_HPP_INCLUDED

#include <Base/Defines.hpp>
#include <Base/impl/FileUtil.hpp>

namespace Base {

using Base::impl::listdir;

}

#endif // BASE_FILEUTIL_HPP_INCLUDED
