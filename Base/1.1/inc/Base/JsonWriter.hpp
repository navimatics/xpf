/*
 * Base/JsonWriter.hpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef BASE_JSONWRITER_HPP_INCLUDED
#define BASE_JSONWRITER_HPP_INCLUDED

#include <Base/Defines.hpp>
#include <Base/Object.hpp>
#include <Base/Stream.hpp>
#include <Base/impl/JsonWriter.hpp>

namespace Base {

using Base::impl::JsonWriter;

}

#endif // BASE_JSONWRITER_HPP_INCLUDED
