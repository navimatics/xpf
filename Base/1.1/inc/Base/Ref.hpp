/*
 * Base/Ref.hpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef BASE_REF_HPP_INCLUDED
#define BASE_REF_HPP_INCLUDED

#include <Base/Defines.hpp>
#include <Base/impl/Ref.hpp>

namespace Base {

using Base::impl::Ref;

}

#endif // BASE_REF_HPP_INCLUDED
