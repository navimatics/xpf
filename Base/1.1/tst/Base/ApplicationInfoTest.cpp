/*
 * Base/ApplicationInfoTest.cpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#include <Base/ApplicationInfo.hpp>
#include <Base/Object.hpp>
#include <gtest/gtest.h>

TEST(ApplicationInfoTest, Test)
{
    /* not a real test; used for debugging */
    Base::Object::Mark();
    const char *applicationName = Base::GetApplicationName();
    const char *applicationResourceDirectory = Base::GetApplicationResourceDirectory();
    const char *applicationDataDirectory = Base::GetApplicationDataDirectory();
    const char *documentDirectory = Base::GetDocumentDirectory();
    const char *temporaryDirectory = Base::GetTemporaryDirectory();
    const char *workingDirectory = Base::GetWorkingDirectory();
    ASSERT_STREQ(applicationName, Base::GetApplicationName());
    ASSERT_STREQ(applicationResourceDirectory, Base::GetApplicationResourceDirectory());
    ASSERT_STREQ(applicationDataDirectory, Base::GetApplicationDataDirectory());
    ASSERT_STREQ(documentDirectory, Base::GetDocumentDirectory());
    ASSERT_STREQ(temporaryDirectory, Base::GetTemporaryDirectory());
    ASSERT_STREQ(workingDirectory, Base::GetWorkingDirectory());
    Base::Object::Collect();
}
