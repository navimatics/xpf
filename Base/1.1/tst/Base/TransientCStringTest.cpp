/*
 * Base/TransientCStringTest.cpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#include <Base/CString.hpp>
#include <Base/Map.hpp>
#include <gtest/gtest.h>

TEST(TransientCStringTest, Basic)
{
    static Base::CString *One = Base::CStringConstant("One");
    static Base::CString *FortyTwo = Base::CStringConstant("FortyTwo");
    Base::Object::Mark();
    Base::Map *map = new (Base::collect) Base::Map;
    map->setObject(One, One);
    map->setObject(FortyTwo, FortyTwo);
    ASSERT_STREQ("One", ((Base::CString *)map->object(BASE_ALLOCA_OBJECT(Base::TransientCString, "One")))->chars());
    ASSERT_STREQ("FortyTwo", ((Base::CString *)map->object(BASE_ALLOCA_OBJECT(Base::TransientCString, "FortyTwo")))->chars());
    Base::Object::Collect();
}
static void TestNested(Base::Map *map, const char *s)
{
    const char *t = ((Base::CString *)map->object(BASE_ALLOCA_OBJECT(Base::TransientCString, s)))->chars();
    ASSERT_NE(s, t);
    ASSERT_STREQ(s, t);
}
TEST(TransientCStringTest, Nested)
{
    static Base::CString *One = Base::CStringConstant("One");
    static Base::CString *FortyTwo = Base::CStringConstant("FortyTwo");
    Base::Object::Mark();
    Base::Map *map = new (Base::collect) Base::Map;
    map->setObject(One, One);
    map->setObject(FortyTwo, FortyTwo);
    TestNested(map, "One");
    TestNested(map, "FortyTwo");
    Base::Object::Collect();
}
