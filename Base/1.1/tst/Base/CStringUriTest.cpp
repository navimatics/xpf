/*
 * Base/CStringUriTest.cpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#include <Base/CString.hpp>
#include <gtest/gtest.h>

static void assert_parts_equal(
    const Base::cstr_uri_parts &expected, const Base::cstr_uri_parts &actual)
{
    ASSERT_STREQ(expected.scheme, actual.scheme);
    ASSERT_STREQ(expected.userinfo, actual.userinfo);
    ASSERT_STREQ(expected.host, actual.host);
    ASSERT_STREQ(expected.port, actual.port);
    ASSERT_STREQ(expected.path, actual.path);
    ASSERT_STREQ(expected.query, actual.query);
    ASSERT_STREQ(expected.fragment, actual.fragment);
}

TEST(CStringUriTest, parse_compose_raw)
{
    mark_and_collect
    {
        struct
        {
            const char *uri;
            Base::cstr_uri_parts parts;
        } tests[] =
        {
            { "",                       {   0,   0,   0,   0,   "",   0,   0 } },
            { "p",                      {   0,   0,   0,   0,  "p",   0,   0 } },
            { "/",                      {   0,   0,   0,   0,  "/",   0,   0 } },
            { "/p",                     {   0,   0,   0,   0, "/p",   0,   0 } },
            { "?",                      {   0,   0,   0,   0,   "",  "",   0 } },
            { "#",                      {   0,   0,   0,   0,   "",   0,  "" } },
            { "?q",                     {   0,   0,   0,   0,   "", "q",   0 } },
            { "#f",                     {   0,   0,   0,   0,   "",   0, "f" } },
            { "?q#f",                   {   0,   0,   0,   0,   "", "q", "f" } },
            { "/p?q",                   {   0,   0,   0,   0, "/p", "q",   0 } },
            { "/p?q#f",                 {   0,   0,   0,   0, "/p", "q", "f" } },
            { "p?q",                    {   0,   0,   0,   0,  "p", "q",   0 } },
            { "p?q#f",                  {   0,   0,   0,   0,  "p", "q", "f" } },

            { "s:",                     { "s",   0,   0,   0,   "",   0,   0 } },
            { "s:p",                    { "s",   0,   0,   0,  "p",   0,   0 } },
            { "s:/",                    { "s",   0,   0,   0,  "/",   0,   0 } },
            { "s:/p",                   { "s",   0,   0,   0, "/p",   0,   0 } },
            { "s:?q",                   { "s",   0,   0,   0,   "", "q",   0 } },
            { "s:#f",                   { "s",   0,   0,   0,   "",   0, "f" } },
            { "s:p?q#f",                { "s",   0,   0,   0,  "p", "q", "f" } },
            { "s:/p?q#f",               { "s",   0,   0,   0, "/p", "q", "f" } },
            { "s://",                   { "s",   0,  "",   0,   "",   0,   0 } },
            { "s:///",                  { "s",   0,  "",   0,  "/",   0,   0 } },
            { "s:///p",                 { "s",   0,  "",   0, "/p",   0,   0 } },
            { "s://?q",                 { "s",   0,  "",   0,   "", "q",   0 } },
            { "s://#f",                 { "s",   0,  "",   0,   "",   0, "f" } },

            { "s://@",                  { "s",  "",  "",   0,   "",   0,   0 } },
            { "s://@:",                 { "s",  "",  "",  "",   "",   0,   0 } },
            { "s://@:/",                { "s",  "",  "",  "",  "/",   0,   0 } },
            { "s://@:?q",               { "s",  "",  "",  "",   "", "q",   0 } },
            { "s://@:#f",               { "s",  "",  "",  "",   "",   0, "f" } },
            { "s://@:?q#f",             { "s",  "",  "",  "",   "", "q", "f" } },

            { "s://u@h:p/p?q#f",        { "s", "u", "h", "p", "/p", "q", "f" } },
            { "s://u@[:]:p/p?q#f",      { "s", "u","[:]","p", "/p", "q", "f" } },
        };
        for (size_t i = 0; NELEMS(tests) > i; i++)
        {
            Base::cstr_uri_parts parts;
            const char *uri;
            Base::cstr_uri_parseraw(tests[i].uri, &parts);
            assert_parts_equal(tests[i].parts, parts);
            uri = cstr_uri_composeraw(&parts);
            ASSERT_STREQ(tests[i].uri, uri);
        }
    }
}
TEST(CStringUriTest, escape_unescape)
{
    mark_and_collect
    {
        const char *unesc =
            "\x01\x02\x03\x04\x05\x06\x07"
            "\x08\x09\x0a\x0b\x0c\x0d\x0e\x0f"
            "\x10\x11\x12\x13\x14\x15\x16\x17"
            "\x18\x19\x1a\x1b\x1c\x1d\x1e\x1f"
            " !\"#$%&'"
            "()*+,-./"
            "01234567"
            "89:;<=>?"
            "@ABCDEFG"
            "HIJKLMNO"
            "PQRSTUVW"
            "XYZ[\\]^_"
            "`abcdefg"
            "hijklmno"
            "pqrstuvw"
            "xyz{|}~\x7f"
            "\x80\x81\x82\x83\x84\x85\x86\x87"
            "\x88\x89\x8a\x8b\x8c\x8d\x8e\x8f"
            "\x90\x91\x92\x93\x94\x95\x96\x97"
            "\x98\x99\x9a\x9b\x9c\x9d\x9e\x9f"
            "\xa0\xa1\xa2\xa3\xa4\xa5\xa6\xa7"
            "\xa8\xa9\xaa\xab\xac\xad\xae\xaf"
            "\xb0\xb1\xb2\xb3\xb4\xb5\xb6\xb7"
            "\xb8\xb9\xba\xbb\xbc\xbd\xbe\xbf"
            "\xc0\xc1\xc2\xc3\xc4\xc5\xc6\xc7"
            "\xc8\xc9\xca\xcb\xcc\xcd\xce\xcf"
            "\xd0\xd1\xd2\xd3\xd4\xd5\xd6\xd7"
            "\xd8\xd9\xda\xdb\xdc\xdd\xde\xdf"
            "\xe0\xe1\xe2\xe3\xe4\xe5\xe6\xe7"
            "\xe8\xe9\xea\xeb\xec\xed\xee\xef"
            "\xf0\xf1\xf2\xf3\xf4\xf5\xf6\xf7"
            "\xf8\xf9\xfa\xfb\xfc\xfd\xfe\xff";
        const char *esc =
            "%01%02%03%04%05%06%07"
            "%08%09%0A%0B%0C%0D%0E%0F"
            "%10%11%12%13%14%15%16%17"
            "%18%19%1A%1B%1C%1D%1E%1F"
            "%20%21%22%23%24%25%26%27"
            "%28%29%2A%2B%2C-.%2F"
            "01234567"
            "89%3A%3B%3C%3D%3E%3F"
            "%40ABCDEFG"
            "HIJKLMNO"
            "PQRSTUVW"
            "XYZ%5B%5C%5D%5E_"
            "%60abcdefg"
            "hijklmno"
            "pqrstuvw"
            "xyz%7B%7C%7D~%7F"
            "%80%81%82%83%84%85%86%87"
            "%88%89%8A%8B%8C%8D%8E%8F"
            "%90%91%92%93%94%95%96%97"
            "%98%99%9A%9B%9C%9D%9E%9F"
            "%A0%A1%A2%A3%A4%A5%A6%A7"
            "%A8%A9%AA%AB%AC%AD%AE%AF"
            "%B0%B1%B2%B3%B4%B5%B6%B7"
            "%B8%B9%BA%BB%BC%BD%BE%BF"
            "%C0%C1%C2%C3%C4%C5%C6%C7"
            "%C8%C9%CA%CB%CC%CD%CE%CF"
            "%D0%D1%D2%D3%D4%D5%D6%D7"
            "%D8%D9%DA%DB%DC%DD%DE%DF"
            "%E0%E1%E2%E3%E4%E5%E6%E7"
            "%E8%E9%EA%EB%EC%ED%EE%EF"
            "%F0%F1%F2%F3%F4%F5%F6%F7"
            "%F8%F9%FA%FB%FC%FD%FE%FF";
        ASSERT_STREQ(esc, Base::cstr_uri_escape(unesc));
        ASSERT_STREQ(unesc, Base::cstr_uri_unescape(esc));
    }
}
TEST(CStringUriTest, escape_exclude_unescape)
{
    mark_and_collect
    {
        const char *unesc =
            "\x01\x02\x03\x04\x05\x06\x07"
            "\x08\x09\x0a\x0b\x0c\x0d\x0e\x0f"
            "\x10\x11\x12\x13\x14\x15\x16\x17"
            "\x18\x19\x1a\x1b\x1c\x1d\x1e\x1f"
            " !\"#$%&'"
            "()*+,-./"
            "01234567"
            "89:;<=>?"
            "@ABCDEFG"
            "HIJKLMNO"
            "PQRSTUVW"
            "XYZ[\\]^_"
            "`abcdefg"
            "hijklmno"
            "pqrstuvw"
            "xyz{|}~\x7f"
            "\x80\x81\x82\x83\x84\x85\x86\x87"
            "\x88\x89\x8a\x8b\x8c\x8d\x8e\x8f"
            "\x90\x91\x92\x93\x94\x95\x96\x97"
            "\x98\x99\x9a\x9b\x9c\x9d\x9e\x9f"
            "\xa0\xa1\xa2\xa3\xa4\xa5\xa6\xa7"
            "\xa8\xa9\xaa\xab\xac\xad\xae\xaf"
            "\xb0\xb1\xb2\xb3\xb4\xb5\xb6\xb7"
            "\xb8\xb9\xba\xbb\xbc\xbd\xbe\xbf"
            "\xc0\xc1\xc2\xc3\xc4\xc5\xc6\xc7"
            "\xc8\xc9\xca\xcb\xcc\xcd\xce\xcf"
            "\xd0\xd1\xd2\xd3\xd4\xd5\xd6\xd7"
            "\xd8\xd9\xda\xdb\xdc\xdd\xde\xdf"
            "\xe0\xe1\xe2\xe3\xe4\xe5\xe6\xe7"
            "\xe8\xe9\xea\xeb\xec\xed\xee\xef"
            "\xf0\xf1\xf2\xf3\xf4\xf5\xf6\xf7"
            "\xf8\xf9\xfa\xfb\xfc\xfd\xfe\xff";
        const char *esc =
            "%01%02%03%04%05%06%07"
            "%08%09%0A%0B%0C%0D%0E%0F"
            "%10%11%12%13%14%15%16%17"
            "%18%19%1A%1B%1C%1D%1E%1F"
            "%20%21%22%23%24%25%26%27"
            "%28%29%2A%2B%2C-./"
            "01234567"
            "89:%3B%3C%3D%3E%3F"
            "@ABCDEFG"
            "HIJKLMNO"
            "PQRSTUVW"
            "XYZ%5B%5C%5D%5E_"
            "%60abcdefg"
            "hijklmno"
            "pqrstuvw"
            "xyz%7B%7C%7D~%7F"
            "%80%81%82%83%84%85%86%87"
            "%88%89%8A%8B%8C%8D%8E%8F"
            "%90%91%92%93%94%95%96%97"
            "%98%99%9A%9B%9C%9D%9E%9F"
            "%A0%A1%A2%A3%A4%A5%A6%A7"
            "%A8%A9%AA%AB%AC%AD%AE%AF"
            "%B0%B1%B2%B3%B4%B5%B6%B7"
            "%B8%B9%BA%BB%BC%BD%BE%BF"
            "%C0%C1%C2%C3%C4%C5%C6%C7"
            "%C8%C9%CA%CB%CC%CD%CE%CF"
            "%D0%D1%D2%D3%D4%D5%D6%D7"
            "%D8%D9%DA%DB%DC%DD%DE%DF"
            "%E0%E1%E2%E3%E4%E5%E6%E7"
            "%E8%E9%EA%EB%EC%ED%EE%EF"
            "%F0%F1%F2%F3%F4%F5%F6%F7"
            "%F8%F9%FA%FB%FC%FD%FE%FF";
        ASSERT_STREQ(esc, Base::cstr_uri_escape(unesc, ":@/"));
        ASSERT_STREQ(unesc, Base::cstr_uri_unescape(esc));
    }
}
TEST(CStringUriTest, parse_compose)
{
    mark_and_collect
    {
        struct
        {
            const char *uri;
            Base::cstr_uri_parts parts;
        } tests[] =
        {
            { "",                       {   0,   0,   0,   0,   "",   0,   0 } },
            { "p",                      {   0,   0,   0,   0,  "p",   0,   0 } },
            { "/",                      {   0,   0,   0,   0,  "/",   0,   0 } },
            { "/p",                     {   0,   0,   0,   0, "/p",   0,   0 } },
            { "?",                      {   0,   0,   0,   0,   "",  "",   0 } },
            { "#",                      {   0,   0,   0,   0,   "",   0,  "" } },
            { "?q",                     {   0,   0,   0,   0,   "", "q",   0 } },
            { "#f",                     {   0,   0,   0,   0,   "",   0, "f" } },
            { "?q#f",                   {   0,   0,   0,   0,   "", "q", "f" } },
            { "/p?q",                   {   0,   0,   0,   0, "/p", "q",   0 } },
            { "/p?q#f",                 {   0,   0,   0,   0, "/p", "q", "f" } },
            { "p?q",                    {   0,   0,   0,   0,  "p", "q",   0 } },
            { "p?q#f",                  {   0,   0,   0,   0,  "p", "q", "f" } },

            { "s:",                     { "s",   0,   0,   0,   "",   0,   0 } },
            { "s:p",                    { "s",   0,   0,   0,  "p",   0,   0 } },
            { "s:/",                    { "s",   0,   0,   0,  "/",   0,   0 } },
            { "s:/p",                   { "s",   0,   0,   0, "/p",   0,   0 } },
            { "s:?q",                   { "s",   0,   0,   0,   "", "q",   0 } },
            { "s:#f",                   { "s",   0,   0,   0,   "",   0, "f" } },
            { "s:p?q#f",                { "s",   0,   0,   0,  "p", "q", "f" } },
            { "s:/p?q#f",               { "s",   0,   0,   0, "/p", "q", "f" } },
            { "s://",                   { "s",   0,  "",   0,   "",   0,   0 } },
            { "s:///",                  { "s",   0,  "",   0,  "/",   0,   0 } },
            { "s:///p",                 { "s",   0,  "",   0, "/p",   0,   0 } },
            { "s://?q",                 { "s",   0,  "",   0,   "", "q",   0 } },
            { "s://#f",                 { "s",   0,  "",   0,   "",   0, "f" } },

            { "s://@",                  { "s",  "",  "",   0,   "",   0,   0 } },
            { "s://@:",                 { "s",  "",  "",  "",   "",   0,   0 } },
            { "s://@:/",                { "s",  "",  "",  "",  "/",   0,   0 } },
            { "s://@:?q",               { "s",  "",  "",  "",   "", "q",   0 } },
            { "s://@:#f",               { "s",  "",  "",  "",   "",   0, "f" } },
            { "s://@:?q#f",             { "s",  "",  "",  "",   "", "q", "f" } },

            { "s://u@h:p/p?q#f",        { "s", "u", "h", "p", "/p", "q", "f" } },
            { "s://u@[:]:p/p?q#f",      { "s", "u","[:]","p", "/p", "q", "f" } },

            { "s%20://u%20@h%20:p%20/p%20?q%20#f%20",
                                        { "s ", "u ", "h ", "p ", "/p ", "q ", "f " } },
            { "s%20://u%20@[:%20]:p%20/p%20?q%20#f%20",
                                        { "s ", "u ","[: ]","p ", "/p ", "q ", "f " } },

            {
                "s+s://"
                "u!$&'()*+,;=:@"
                "[!$&'()*+,;=:]:"
                "p"
                "/p!$&'()*+,;=:@/!$&'()*+,;=:@"
                "?q!$&'()*+,;=:@/?"
                "#f!$&'()*+,;=:@/?",
                {
                    "s+s",
                    "u!$&'()*+,;=:",
                    "[!$&'()*+,;=:]",
                    "p",
                    "/p!$&'()*+,;=:@/!$&'()*+,;=:@",
                    "q!$&'()*+,;=:@/?",
                    "f!$&'()*+,;=:@/?"
                }
            },
        };
        for (size_t i = 0; NELEMS(tests) > i; i++)
        {
            Base::cstr_uri_parts parts;
            const char *uri;
            Base::cstr_uri_parse(tests[i].uri, &parts, true);
            assert_parts_equal(tests[i].parts, parts);
            uri = cstr_uri_compose(&parts, true);
            ASSERT_STREQ(tests[i].uri, uri);
        }
        Base::cstr_uri_parts parts;
        Base::cstr_uri_parts expect_parts = { "ss ", "Uu ", "hh ", "Pp ", "/Pp ", "Qq ", "Ff " };
        Base::cstr_uri_parse("Ss%20://Uu%20@Hh%20:Pp%20/Pp%20?Qq%20#Ff%20", &parts, true);
        assert_parts_equal(expect_parts, parts);
        parts.scheme = "Ss ";
        parts.host = "Hh ";
        const char *uri = cstr_uri_compose(&parts, true);
        ASSERT_STREQ("ss%20://Uu%20@hh%20:Pp%20/Pp%20?Qq%20#Ff%20", uri);
    }
}
TEST(CStringUriTest, normalize)
{
    mark_and_collect
    {
        struct
        {
            const char *uri;
            const char *normuri;
        } tests[] =
        {
            {
                "hTtp%2BV%41ri%61nt%5b%5D[]://user%41@HOSTn%41me:%31234/p%41th[]/?query[]#fr%41gment%5b%5d",
                "http+variant%5B%5D%5B%5D://userA@hostname:1234/pAth%5B%5D/?query%5B%5D#frAgment%5B%5D"
            },
            {
                "",
                "",
            },
            {
                ".",
                "",
            },
            {
                "./",
                "",
            },
            {
                "./.",
                "",
            },
            {
                "././",
                "",
            },
            {
                "././a",
                "a",
            },
            {
                "..",
                "",
            },
            {
                "../",
                "",
            },
            {
                "../..",
                "",
            },
            {
                "../../",
                "",
            },
            {
                "../../a",
                "a",
            },
            {
                "/.",
                "/"
            },
            {
                "/./",
                "/"
            },
            {
                "/..",
                "",
            },
            {
                "/../",
                "",
            },
            {
                "a/b/..",
                "a/"
            },
            {
                "a/../b/.",
                "b/"
            },
            {
                "a/.a",
                "a/.a",
            },
            {
                "a/..a",
                "a/..a",
            },
            {
                "a/a..",
                "a/a..",
            },
            {
                "a/a../",
                "a/a../",
            },
            {
                "http://a/..",
                "http://a/"
            },
            {
                "http://a/../",
                "http://a/"
            },
            {
                "http://a/b/c/../g?q",
                "http://a/b/g?q"
            },
            {
                "http://a/b/c/../../../../g?q",
                "http://a/g?q"
            },
        };
        for (size_t i = 0; NELEMS(tests) > i; i++)
        {
            const char *normuri = Base::cstr_uri_normalize(tests[i].uri, true);
            ASSERT_STREQ(tests[i].normuri, normuri);
        }
    }
}
TEST(CStringUriTest, concat)
{
    mark_and_collect
    {
        struct
        {
            const char *uri1, *uri2;
            const char *uri;
        } tests[] =
        {
            {
                "http://a/b/c/d;p?q", "g:h",
                "g:h"
            },
            {
                "http://a/b/c/d;p?q", "g",
                "http://a/b/c/g"
            },
            {
                "http://a/b/c/d;p?q", "./g",
                "http://a/b/c/g"
            },
            {
                "http://a/b/c/d;p?q", "g/",
                "http://a/b/c/g/"
            },
            {
                "http://a/b/c/d;p?q", "/g",
                "http://a/g"
            },
            {
                "http://a/b/c/d;p?q", "//g",
                "http://g"
            },
            {
                "http://a/b/c/d;p?q", "?y",
                "http://a/b/c/d;p?y"
            },
            {
                "http://a/b/c/d;p?q", "g?y",
                "http://a/b/c/g?y"
            },
            {
                "http://a/b/c/d;p?q", "#s",
                "http://a/b/c/d;p?q#s"
            },
            {
                "http://a/b/c/d;p?q", "g#s",
                "http://a/b/c/g#s"
            },
            {
                "http://a/b/c/d;p?q", "g?y#s",
                "http://a/b/c/g?y#s"
            },
            {
                "http://a/b/c/d;p?q", ";x",
                "http://a/b/c/;x"
            },
            {
                "http://a/b/c/d;p?q", "g;x",
                "http://a/b/c/g;x"
            },
            {
                "http://a/b/c/d;p?q", "g;x?y#s",
                "http://a/b/c/g;x?y#s"
            },
            {
                "http://a/b/c/d;p?q", "",
                "http://a/b/c/d;p?q"
            },
            {
                "http://a/b/c/d;p?q", ".",
                "http://a/b/c/"
            },
            {
                "http://a/b/c/d;p?q", "./",
                "http://a/b/c/"
            },
            {
                "http://a/b/c/d;p?q", "..",
                "http://a/b/"
            },
            {
                "http://a/b/c/d;p?q", "../",
                "http://a/b/"
            },
            {
                "http://a/b/c/d;p?q", "../g",
                "http://a/b/g"
            },
            {
                "http://a/b/c/d;p?q", "../..",
                "http://a/"
            },
            {
                "http://a/b/c/d;p?q", "../../",
                "http://a/"
            },
            {
                "http://a/b/c/d;p?q", "../../g",
                "http://a/g"
            },
            {
                "http://a/b/c/d;p?q", "../../../g",
                "http://a/g"
            },
            {
                "http://a/b/c/d;p?q", "../../../../g",
                "http://a/g"
            },
            {
                "http://a/b/c/d;p?q", "/./g",
                "http://a/g"
            },
            {
                "http://a/b/c/d;p?q", "/../g",
                "http://a/g"
            },
            {
                "http://a/b/c/d;p?q", "g.",
                "http://a/b/c/g."
            },
            {
                "http://a/b/c/d;p?q", ".g",
                "http://a/b/c/.g"
            },
            {
                "http://a/b/c/d;p?q", "g..",
                "http://a/b/c/g.."
            },
            {
                "http://a/b/c/d;p?q", "..g",
                "http://a/b/c/..g"
            },
            {
                "http://a/b/c/d;p?q", "./../g",
                "http://a/b/g"
            },
            {
                "http://a/b/c/d;p?q", "./g/.",
                "http://a/b/c/g/"
            },
            {
                "http://a/b/c/d;p?q", "g/./h",
                "http://a/b/c/g/h"
            },
            {
                "http://a/b/c/d;p?q", "g/../h",
                "http://a/b/c/h"
            },
            {
                "http://a/b/c/d;p?q", "g;x=1/./y",
                "http://a/b/c/g;x=1/y"
            },
            {
                "http://a/b/c/d;p?q", "g;x=1/../y",
                "http://a/b/c/y"
            },
            {
                "http://a/b/c/d;p?q", "g?y/./x",
                "http://a/b/c/g?y/./x"
            },
            {
                "http://a/b/c/d;p?q", "g?y/../x",
                "http://a/b/c/g?y/../x"
            },
            {
                "http://a/b/c/d;p?q", "g#s/./x",
                "http://a/b/c/g#s/./x"
            },
            {
                "http://a/b/c/d;p?q", "g#s/../x",
                "http://a/b/c/g#s/../x"
            },
            {
                "http://a/b/c/d;p?q", "http:g",
                "http:g"
            },
            {
                "http://a", "g",
                "http://a/g"
            },
            {
                "http://a/", "g",
                "http://a/g"
            },
            {
                "http://a/", "/g",
                "http://a/g"
            },
            {
                "a", "g",
                "g"
            },
            {
                "a/", "g",
                "a/g"
            },
            {
                "a/b", "g",
                "a/g"
            },
        };
        for (size_t i = 0; NELEMS(tests) > i; i++)
        {
            const char *uri = Base::cstr_uri_concat(tests[i].uri1, tests[i].uri2, true);
            ASSERT_STREQ(tests[i].uri, uri);
        }
    }
}
TEST(CStringUriTest, query_extract_concat)
{
    mark_and_collect
    {
        const char *components1[] = { "a", "" };
        const char *components2[] = { "a", "b" };
        const char *components3[] = { "a", "", "b", "" };
        const char *components4[] = { "a", "", "b", "c" };
        const char *components5[] = { "key 1", "val 1", "key  2", "val &2&", "key +3", "val =3=" };
        const char *components6[] = { "MY KEY", "MY VALUE", "My Key", "My Value" };
        struct
        {
            const char *query;
            const char *resultQuery;
            const char **components; size_t ncomponents;
        } tests[] =
        {
            {
                0,
                "",
                0, 0
            },
            {
                "",
                "",
                0, 0
            },
            {
                "a",
                "a=",
                components1, NELEMS(components1)
            },
            {
                "a=",
                "a=",
                components1, NELEMS(components1)
            },
            {
                "a&",
                "a=",
                components1, NELEMS(components1)
            },
            {
                "a=b",
                "a=b",
                components2, NELEMS(components2)
            },
            {
                "a&b",
                "a=&b=",
                components3, NELEMS(components3)
            },
            {
                "a&b=c&",
                "a=&b=c",
                components4, NELEMS(components4)
            },
            {
                "key+1=val%201&key%20+2=val+%262%26&key+%2b3=val+%3d3%3d",
                "key+1=val+1&key++2=val+%262%26&key+%2B3=val+%3D3%3D",
                components5, NELEMS(components5)
            },
            {
                "MY+KEY=MY+VALUE&My+Key=My+Value",
                "MY+KEY=MY+VALUE&My+Key=My+Value",
                components6, NELEMS(components6)
            },
        };
        for (size_t i = 0; NELEMS(tests) > i; i++)
        {
            size_t j = 0;
            const char *query = tests[i].query;
            const char *key, *val;
            while (0 != (query = Base::cstr_uri_query_extract(query, &key, &val)))
            {
                ASSERT_STREQ(tests[i].components[j++], key);
                ASSERT_STREQ(tests[i].components[j++], val);
            }
            ASSERT_EQ(0, key);
            ASSERT_EQ(0, val);
            ASSERT_EQ(tests[i].ncomponents, j);
            const char *resultQuery = Base::cstr_uri_query_concat(0, tests[i].components, tests[i].ncomponents);
            ASSERT_STREQ(tests[i].resultQuery, resultQuery);
            resultQuery = Base::cstr_uri_query_concat("prefix", tests[i].components, tests[i].ncomponents);
            ASSERT_STREQ(
                Base::cstringf("prefix%s%s", '\0' != tests[i].resultQuery[0] ? "&" : "", tests[i].resultQuery),
                resultQuery);
        }
    }
}
