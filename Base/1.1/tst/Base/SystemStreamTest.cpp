/*
 * Base/SystemStreamTest.cpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#include <Base/SystemStream.hpp>
#include <Base/Thread.hpp>
#include <gtest/gtest.h>

TEST(SystemStreamTest, TestRegular)
{
    Base::Object::Mark();
    char path[L_tmpnam]; tmpnam(path);
    Base::SystemStream *stream;
    char buf[32]; ssize_t bytes;
    memset(buf, 0, sizeof buf);
    stream = Base::SystemStream::create(path, "wb");
    stream->write("1234567890", 10);
    stream->close();
    stream->release();
    stream = Base::SystemStream::create(path, "rb");
    bytes = stream->read(buf, sizeof buf - 1);
    stream->close();
    stream->release();
    ASSERT_EQ(10, bytes);
    ASSERT_STREQ("1234567890", buf);
    memset(buf, 0, sizeof buf);
    stream = Base::SystemStream::create(path, "ab");
    stream->write("0987654321", 10);
    stream->release();
    stream = Base::SystemStream::create(path, "rb");
    bytes = stream->read(buf, sizeof buf - 1);
    stream->release();
    ASSERT_EQ(20, bytes);
    ASSERT_STREQ("12345678900987654321", buf);
    remove(path);
    Base::Object::Collect();
}
TEST(SystemStreamTest, TestNonExistant)
{
    Base::Object::Mark();
    Base::SystemStream *stream = Base::SystemStream::create("/FILE/91328EE22A044FA4AF64A6D81D9154CE/DOES/NOT/EXIST", "rb");
    ASSERT_EQ(0, stream);
    Base::Object::Collect();
}
#if 1
#if defined(BASE_CONFIG_WINDOWS)
#define SLOWDEV                         "\\\\.\\CNCA0"
#elif defined(BASE_CONFIG_POSIX)
#define SLOWDEV                         "/dev/tty.GarminGPS10"
#endif
static void TestCancelationThread(void *arg)
{
    Base::SystemStream *stream = (Base::SystemStream *)arg;
    char buf[1024];
    while (0 < stream->read(buf, sizeof buf))
        ;
    stream->close();
    stream->release();
}
TEST(SystemStreamTest, TestCancelation)
{
    Base::Object::Mark();
    Base::SystemStream *stream = Base::SystemStream::create(SLOWDEV, "rb");
    ASSERT_NE((void *)0, stream);
    Base::Thread *thread = new Base::Thread(TestCancelationThread, stream->retain());
    thread->start();
    Base::Thread::sleep(3000);
    stream->cancel();
    thread->wait();
    thread->release();
    stream->release();
    Base::Object::Collect();
}
#endif
