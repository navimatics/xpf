/*
 * Base/TestObject.hpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#include <Base/Object.hpp>

class TestObject : public Base::Object
{
public:
    TestObject();
    ~TestObject();
    int getObjid();
public:
    static int count;
    int objid;
};
