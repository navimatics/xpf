/*
 * Base/ZipFileStreamTest.cpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#include <Base/ZipFileStream.hpp>
#include <gtest/gtest.h>

TEST(ZipFileStreamTest, TestRegular)
{
    Base::Object::Mark();
    char path[L_tmpnam]; tmpnam(path);
    Base::Stream *stream;
    char buf[32]; ssize_t bytes;
    stream = Base::ZipFileStream::create(path, "path1/file1", "wb");
    stream->write("1234567890", 10);
    stream->close();
    stream->release();
    stream = Base::ZipFileStream::create(path, "path2/file2", "wb");
    stream->write("0987654321", 10);
    stream->release();
    memset(buf, 0, sizeof buf);
    stream = Base::ZipFileStream::create(path, "path1/file1", "rb");
    bytes = stream->read(buf, sizeof buf - 1);
    stream->close();
    stream->release();
    ASSERT_EQ(10, bytes);
    ASSERT_STREQ("1234567890", buf);
    memset(buf, 0, sizeof buf);
    stream = Base::ZipFileStream::create(path, "path2/file2", "rb");
    bytes = stream->read(buf, sizeof buf - 1);
    stream->release();
    ASSERT_EQ(10, bytes);
    ASSERT_STREQ("0987654321", buf);
    remove(path);
    Base::Object::Collect();
}
TEST(ZipFileStreamTest, TestNonExistant)
{
    Base::Object::Mark();
    Base::Stream *stream = Base::ZipFileStream::create("/FILE/91328EE22A044FA4AF64A6D81D9154CE/DOES/NOT/EXIST", "path/does/not/exist", "rb");
    ASSERT_EQ(0, stream);
    Base::Object::Collect();
}
