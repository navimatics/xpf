/*
 * Base/CacheMapTest.cpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#include <Base/CacheMap.hpp>
#include <Base/CArray.hpp>
#include <Base/Value.hpp>
#include <gtest/gtest.h>
#include "TestObject.hpp"

TEST(CacheMapTest, AddRemoveAll)
{
    ASSERT_EQ(0, TestObject::count);
    Base::Object::Mark();
    Base::CacheMap *map = new (Base::collect) Base::CacheMap;
    for (size_t i = 0; i < 100; i++)
    {
        TestObject *o = new (Base::collect) TestObject;
        map->setObject(o, o);
    }
    ASSERT_EQ((size_t)100, map->count());
    map->removeAllObjects();
    ASSERT_EQ((size_t)0, map->count());
    for (size_t i = 0; i < 100; i++)
    {
        TestObject *o = new (Base::collect) TestObject;
        map->setObject(o, o);
    }
    ASSERT_EQ((size_t)100, map->count());
    Base::Object::Collect();
    ASSERT_EQ(0, TestObject::count);
}
TEST(CacheMapTest, ObjectAndGetObject)
{
    ASSERT_EQ(0, TestObject::count);
    Base::Object::Mark();
    Base::CacheMap *map = new (Base::collect) Base::CacheMap;
    for (size_t i = 0; i < 100; i++)
    {
        TestObject *o = new (Base::collect) TestObject;
        map->setObject(o, o);
    }
    ASSERT_EQ((size_t)100, map->count());
    TestObject *o = new (Base::collect) TestObject;
    map->setObject(o, o);
    ASSERT_EQ((size_t)101, map->count());
    ASSERT_TRUE(o == map->object(o));
    Base::Object *go;
    ASSERT_TRUE(map->getObject(o, go));
    ASSERT_TRUE(o == go);
    Base::Object::Collect();
    ASSERT_EQ(0, TestObject::count);
}
TEST(CacheMapTest, AddReplaceRemove)
{
    ASSERT_EQ(0, TestObject::count);
    Base::Object::Mark();
    Base::CacheMap *map = new (Base::collect) Base::CacheMap;
    for (size_t i = 0; i < 100; i++)
    {
        TestObject *o = new (Base::collect) TestObject;
        map->setObject(o, o);
    }
    ASSERT_EQ((size_t)100, map->count());
    TestObject *o = new (Base::collect) TestObject;
    map->setObject(o, o);
    ASSERT_EQ((size_t)101, map->count());
    ASSERT_TRUE(o == map->object(o));
    TestObject *o2 = new (Base::collect) TestObject;
    map->setObject(o, o2);
    ASSERT_EQ((size_t)101, map->count());
    ASSERT_TRUE(o2 == map->object(o));
    map->removeObject(o);
    ASSERT_EQ((size_t)100, map->count());
    ASSERT_TRUE(0 == map->object(o));
    Base::Object *go;
    ASSERT_FALSE(map->getObject(o, go));
    ASSERT_TRUE(0 == go);
    Base::Object::Collect();
    ASSERT_EQ(0, TestObject::count);
}
TEST(CacheMapTest, AddObjects)
{
    ASSERT_EQ(0, TestObject::count);
    Base::Object::Mark();
    Base::Object *objbuf[110];
    for (size_t i = 0; i < 100; i++)
        objbuf[i] = new (Base::collect) TestObject;
    Base::CacheMap *map1 = new (Base::collect) Base::CacheMap;
    for (size_t i = 0; i < 100; i++)
    {
        Base::Object *o = objbuf[i];
        map1->setObject(o, o);
    }
    Base::CacheMap *map2 = new (Base::collect) Base::CacheMap;
    for (size_t i = 0; i < 10; i++)
    {
        Base::Object *o = objbuf[i];
        map2->setObject(o, new (Base::collect) TestObject);
    }
    for (size_t i = 100; i < 110; i++)
    {
        TestObject *o = new (Base::collect) TestObject;
        objbuf[i] = o;
        map2->setObject(o, o);
    }
    ASSERT_EQ((size_t)100, map1->count());
    ASSERT_EQ((size_t)20, map2->count());
    map1->addObjects(map2);
    ASSERT_EQ((size_t)110, map1->count());
    for (size_t i = 0; i < 10; i++)
    {
        Base::Object *o = objbuf[i];
        ASSERT_TRUE(map1->object(o) == map2->object(o));
    }
    for (size_t i = 100; i < 110; i++)
    {
        Base::Object *o = objbuf[i];
        ASSERT_TRUE(map1->object(o) == map2->object(o));
    }
    Base::Object::Collect();
    ASSERT_EQ(0, TestObject::count);
}
TEST(CacheMapTest, Contents)
{
    ASSERT_EQ(0, TestObject::count);
    Base::Object::Mark();
    Base::CacheMap *map = new (Base::collect) Base::CacheMap;
    for (size_t i = 0; i < 100; i++)
    {
        TestObject *o = new (Base::collect) TestObject;
        map->setObject(o, o);
    }
    ASSERT_EQ((size_t)100, map->count());
    Base::Object **keys = map->keys();
    Base::Object **objs = map->objects();
    ASSERT_EQ((size_t)100, carr_length(keys));
    ASSERT_EQ((size_t)100, carr_length(objs));
    for (size_t i = 0; i < 100; i++)
        ASSERT_TRUE(keys[i] == objs[i]);
    map->getKeysAndObjects(keys, objs);
    ASSERT_EQ((size_t)100, carr_length(keys));
    ASSERT_EQ((size_t)100, carr_length(objs));
    for (size_t i = 0; i < 100; i++)
        ASSERT_TRUE(keys[i] == objs[i]);
    Base::Object::Collect();
    ASSERT_EQ(0, TestObject::count);
}
TEST(CacheMapTest, BoundedCache)
{
    Base::Object::Mark();
    Base::CacheMap *map = new (Base::collect) Base::CacheMap;
    map->setMaxCount(10);
    for (ssize_t i = 0; i < 100; i++)
    {
        Base::Value *v = Base::Value::create(i);
        v->autorelease();
        map->setObject(v, v);
    }
    ASSERT_EQ((size_t)10, map->count());
    Base::Object **keys = map->keys();
    Base::Object **objs = map->objects();
    ASSERT_EQ((size_t)10, carr_length(keys));
    ASSERT_EQ((size_t)10, carr_length(objs));
    for (ssize_t i = 0; i < 10; i++)
    {
        ASSERT_TRUE(keys[i] == objs[i]);
        ASSERT_EQ(99 - i, ((Base::Value *)keys[i])->integerValue());
    }
    map->getKeysAndObjects(keys, objs);
    ASSERT_EQ((size_t)10, carr_length(keys));
    ASSERT_EQ((size_t)10, carr_length(objs));
    for (ssize_t i = 0; i < 10; i++)
    {
        ASSERT_TRUE(keys[i] == objs[i]);
        ASSERT_EQ(99 - i, ((Base::Value *)keys[i])->integerValue());
    }
    map->setMaxCount(5);
    map->getKeysAndObjects(keys, objs);
    ASSERT_EQ((size_t)5, carr_length(keys));
    ASSERT_EQ((size_t)5, carr_length(objs));
    for (ssize_t i = 0; i < 5; i++)
    {
        ASSERT_TRUE(keys[i] == objs[i]);
        ASSERT_EQ(99 - i, ((Base::Value *)keys[i])->integerValue());
    }
    Base::Value *v = Base::Value::create(95);
    v->autorelease();
    map->object(v);
    map->getKeysAndObjects(keys, objs);
    ASSERT_EQ((size_t)5, carr_length(keys));
    ASSERT_EQ((size_t)5, carr_length(objs));
    ASSERT_TRUE(keys[0] == objs[0]);
    ASSERT_EQ(95, ((Base::Value *)keys[0])->integerValue());
    for (ssize_t i = 1; i < 5; i++)
    {
        ASSERT_TRUE(keys[i] == objs[i]);
        ASSERT_EQ(99 - i + 1, ((Base::Value *)keys[i])->integerValue());
    }
    Base::Object::Collect();
}

/*
 * GenericCacheMap<T>
 */
TEST(GenericCacheMapTest, AddRemoveAll)
{
    ASSERT_EQ(0, TestObject::count);
    Base::Object::Mark();
    Base::GenericCacheMap<TestObject, TestObject> *map = new (Base::collect) Base::GenericCacheMap<TestObject, TestObject>;
    for (size_t i = 0; i < 100; i++)
    {
        TestObject *o = new (Base::collect) TestObject;
        map->setObject(o, o);
    }
    ASSERT_EQ((size_t)100, map->count());
    map->removeAllObjects();
    ASSERT_EQ((size_t)0, map->count());
    for (size_t i = 0; i < 100; i++)
    {
        TestObject *o = new (Base::collect) TestObject;
        map->setObject(o, o);
    }
    ASSERT_EQ((size_t)100, map->count());
    Base::Object::Collect();
    ASSERT_EQ(0, TestObject::count);
}
TEST(GenericCacheMapTest, ObjectAndGetObject)
{
    ASSERT_EQ(0, TestObject::count);
    Base::Object::Mark();
    Base::GenericCacheMap<TestObject, TestObject> *map = new (Base::collect) Base::GenericCacheMap<TestObject, TestObject>;
    for (size_t i = 0; i < 100; i++)
    {
        TestObject *o = new (Base::collect) TestObject;
        map->setObject(o, o);
    }
    ASSERT_EQ((size_t)100, map->count());
    TestObject *o = new (Base::collect) TestObject;
    map->setObject(o, o);
    ASSERT_EQ((size_t)101, map->count());
    ASSERT_TRUE(o == map->object(o));
    TestObject *go;
    ASSERT_TRUE(map->getObject(o, go));
    ASSERT_TRUE(o == go);
    Base::Object::Collect();
    ASSERT_EQ(0, TestObject::count);
}
TEST(GenericCacheMapTest, AddReplaceRemove)
{
    ASSERT_EQ(0, TestObject::count);
    Base::Object::Mark();
    Base::GenericCacheMap<TestObject, TestObject> *map = new (Base::collect) Base::GenericCacheMap<TestObject, TestObject>;
    for (size_t i = 0; i < 100; i++)
    {
        TestObject *o = new (Base::collect) TestObject;
        map->setObject(o, o);
    }
    ASSERT_EQ((size_t)100, map->count());
    TestObject *o = new (Base::collect) TestObject;
    map->setObject(o, o);
    ASSERT_EQ((size_t)101, map->count());
    ASSERT_TRUE(o == map->object(o));
    TestObject *o2 = new (Base::collect) TestObject;
    map->setObject(o, o2);
    ASSERT_EQ((size_t)101, map->count());
    ASSERT_TRUE(o2 == map->object(o));
    map->removeObject(o);
    ASSERT_EQ((size_t)100, map->count());
    ASSERT_TRUE(0 == map->object(o));
    TestObject *go;
    ASSERT_FALSE(map->getObject(o, go));
    ASSERT_TRUE(0 == go);
    Base::Object::Collect();
    ASSERT_EQ(0, TestObject::count);
}
TEST(GenericCacheMapTest, AddObjects)
{
    ASSERT_EQ(0, TestObject::count);
    Base::Object::Mark();
    TestObject *objbuf[110];
    for (size_t i = 0; i < 100; i++)
        objbuf[i] = new (Base::collect) TestObject;
    Base::GenericCacheMap<TestObject, TestObject> *map1 = new (Base::collect) Base::GenericCacheMap<TestObject, TestObject>;
    for (size_t i = 0; i < 100; i++)
    {
        TestObject *o = objbuf[i];
        map1->setObject(o, o);
    }
    Base::GenericCacheMap<TestObject, TestObject> *map2 = new (Base::collect) Base::GenericCacheMap<TestObject, TestObject>;
    for (size_t i = 0; i < 10; i++)
    {
        TestObject *o = objbuf[i];
        map2->setObject(o, new (Base::collect) TestObject);
    }
    for (size_t i = 100; i < 110; i++)
    {
        TestObject *o = new (Base::collect) TestObject;
        objbuf[i] = o;
        map2->setObject(o, o);
    }
    ASSERT_EQ((size_t)100, map1->count());
    ASSERT_EQ((size_t)20, map2->count());
    map1->addObjects(map2);
    ASSERT_EQ((size_t)110, map1->count());
    for (size_t i = 0; i < 10; i++)
    {
        TestObject *o = objbuf[i];
        ASSERT_TRUE(map1->object(o) == map2->object(o));
    }
    for (size_t i = 100; i < 110; i++)
    {
        TestObject *o = objbuf[i];
        ASSERT_TRUE(map1->object(o) == map2->object(o));
    }
    Base::Object::Collect();
    ASSERT_EQ(0, TestObject::count);
}
TEST(GenericCacheMapTest, Contents)
{
    ASSERT_EQ(0, TestObject::count);
    Base::Object::Mark();
    Base::GenericCacheMap<TestObject, TestObject> *map = new (Base::collect) Base::GenericCacheMap<TestObject, TestObject>;
    for (size_t i = 0; i < 100; i++)
    {
        TestObject *o = new (Base::collect) TestObject;
        map->setObject(o, o);
    }
    ASSERT_EQ((size_t)100, map->count());
    TestObject **keys = map->keys();
    TestObject **objs = map->objects();
    ASSERT_EQ((size_t)100, carr_length(keys));
    ASSERT_EQ((size_t)100, carr_length(objs));
    for (size_t i = 0; i < 100; i++)
        ASSERT_TRUE(keys[i] == objs[i]);
    map->getKeysAndObjects(keys, objs);
    ASSERT_EQ((size_t)100, carr_length(keys));
    ASSERT_EQ((size_t)100, carr_length(objs));
    for (size_t i = 0; i < 100; i++)
        ASSERT_TRUE(keys[i] == objs[i]);
    Base::Object::Collect();
    ASSERT_EQ(0, TestObject::count);
}
