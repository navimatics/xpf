/*
 * Base/IterableTest.cpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#include <Base/Iterable.hpp>
#include <Base/Array.hpp>
#include <Base/CArray.hpp>
#include <Base/CacheMap.hpp>
#include <Base/DelegateMap.hpp>
#include <Base/IndexMap.hpp>
#include <Base/Map.hpp>
#include <Base/OrderedMap.hpp>
#include <Base/OrderedSet.hpp>
#include <Base/Set.hpp>
#include <Base/Value.hpp>
#include <gtest/gtest.h>
#include "TestObject.hpp"

TEST(IterableTest, CArrayIterable)
{
    ASSERT_EQ(0, TestObject::count);
    Base::Object::Mark();
    Base::Object **arr = (Base::Object **)Base::carray(0, 100, sizeof(Base::Object *));
    for (ssize_t i = 0, n = Base::carr_length(arr); i < n; i++)
        arr[i] = new (Base::collect) TestObject;
    ssize_t j = 0;
    foreach (TestObject *obj, arr)
        ASSERT_EQ(j++, obj->objid);
    ASSERT_EQ((ssize_t)Base::carr_length(arr), j);
    Base::Object::Collect();
    ASSERT_EQ(0, TestObject::count);
}
TEST(IterableTest, ArrayIterable)
{
    ASSERT_EQ(0, TestObject::count);
    Base::Object::Mark();
    Base::Array *arr = new (Base::collect) Base::Array;
    for (ssize_t i = 0; i < 100; i++)
        arr->addObject(new (Base::collect) TestObject);
    ssize_t j = 0;
    foreach (TestObject *obj, arr)
        ASSERT_EQ(j++, obj->objid);
    ASSERT_EQ((ssize_t)arr->count(), j);
    j = 0;
    foreach (TestObject *obj, arr->objects())
        ASSERT_EQ(j++, obj->objid);
    ASSERT_EQ((ssize_t)arr->count(), j);
    Base::Object::Collect();
    ASSERT_EQ(0, TestObject::count);
}
TEST(IterableTest, MapIterable)
{
    ASSERT_EQ(0, TestObject::count);
    Base::Object::Mark();
    Base::Map *map = new (Base::collect) Base::Map;
    for (ssize_t i = 0; i < 100; i++)
    {
        TestObject *obj = new (Base::collect) TestObject;
        map->setObject(obj, obj);
    }
    ssize_t j = 0;
    foreach (Base::Object *obj, map)
    {
        ASSERT_EQ(obj, map->object(obj));
        j++;
    }
    ASSERT_EQ((ssize_t)map->count(), j);
    j = 0;
    foreach (Base::Object *obj, map->keys())
    {
        ASSERT_EQ(obj, map->object(obj));
        j++;
    }
    ASSERT_EQ((ssize_t)map->count(), j);
    Base::Object::Collect();
    ASSERT_EQ(0, TestObject::count);
}
TEST(IterableTest, SetIterable)
{
    ASSERT_EQ(0, TestObject::count);
    Base::Object::Mark();
    Base::Set *set = new (Base::collect) Base::Set;
    TestObject *to[100];
    for (size_t i = 0; i < 100; i++)
    {
        to[i] = new (Base::collect) TestObject;
        set->addObject(to[i]);
    }
    ssize_t j = 0;
    foreach (Base::Object *obj, set)
    {
        bool found = false;
        for (size_t k = 0; k < 100; k++)
            if (to[k] == obj)
            {
                found = true;
                break;
            }
        ASSERT_TRUE(found);
        j++;
    }
    ASSERT_EQ((ssize_t)set->count(), j);
    j = 0;
    foreach (Base::Object *obj, set->objects())
    {
        bool found = false;
        for (size_t k = 0; k < 100; k++)
            if (to[k] == obj)
            {
                found = true;
                break;
            }
        ASSERT_TRUE(found);
        j++;
    }
    ASSERT_EQ((ssize_t)set->count(), j);
    Base::Object::Collect();
    ASSERT_EQ(0, TestObject::count);
}
TEST(IterableTest, OrderedMapIterable)
{
    ASSERT_EQ(0, TestObject::count);
    Base::Object::Mark();
    Base::OrderedMap *map = new (Base::collect) Base::OrderedMap;
    for (ssize_t i = 0; i < 100; i++)
    {
        TestObject *obj = new (Base::collect) TestObject;
        map->setObject(obj, obj);
    }
    ssize_t j = 0;
    foreach (Base::Object *obj, map)
    {
        ASSERT_EQ(obj, map->object(obj));
        j++;
    }
    ASSERT_EQ((ssize_t)map->count(), j);
    j = 0;
    foreach (Base::Object *obj, map->keys())
    {
        ASSERT_EQ(obj, map->object(obj));
        j++;
    }
    ASSERT_EQ((ssize_t)map->count(), j);
    Base::Object::Collect();
    ASSERT_EQ(0, TestObject::count);
}
TEST(IterableTest, OrderedSetIterable)
{
    ASSERT_EQ(0, TestObject::count);
    Base::Object::Mark();
    Base::OrderedSet *set = new (Base::collect) Base::OrderedSet;
    for (ssize_t i = 0; i < 100; i++)
    {
        Base::Value *v = Base::Value::create(i);
        v->autorelease();
        set->addObject(v);
    }
    ssize_t j = 0;
    foreach (Base::Object *obj, set)
        ASSERT_EQ(j++, ((Base::Value *)obj)->integerValue());
    ASSERT_EQ((ssize_t)set->count(), j);
    j = 0;
    foreach (Base::Object *obj, set->objects())
        ASSERT_EQ(j++, ((Base::Value *)obj)->integerValue());
    ASSERT_EQ((ssize_t)set->count(), j);
    Base::Object::Collect();
    ASSERT_EQ(0, TestObject::count);
}
TEST(IterableTest, IndexMapIterable)
{
    ASSERT_EQ(0, TestObject::count);
    Base::Object::Mark();
    Base::IndexMap *map = new (Base::collect) Base::IndexMap;
    for (ssize_t i = 0; i < 100; i++)
    {
        TestObject *obj = new (Base::collect) TestObject;
        map->setObject(obj->objid, obj);
    }
    ssize_t j = 0;
    foreach (Base::index_t index, map)
    {
        ASSERT_EQ(index, (Base::index_t)((TestObject *)map->object(index))->objid);
        j++;
    }
    ASSERT_EQ((ssize_t)map->count(), j);
    Base::Object::Collect();
    ASSERT_EQ(0, TestObject::count);
}
TEST(IterableTest, CacheMapIterable)
{
    ASSERT_EQ(0, TestObject::count);
    Base::Object::Mark();
    Base::CacheMap *map = new (Base::collect) Base::CacheMap;
    for (ssize_t i = 0; i < 100; i++)
    {
        TestObject *obj = new (Base::collect) TestObject;
        map->setObject(obj, obj);
    }
    ssize_t j = 0;
    foreach (Base::Object *obj, map)
    {
        ASSERT_EQ(obj, map->object(obj));
        j++;
    }
    ASSERT_EQ((ssize_t)map->count(), j);
    j = 0;
    foreach (Base::Object *obj, map->keys())
    {
        ASSERT_EQ(obj, map->object(obj));
        j++;
    }
    ASSERT_EQ((ssize_t)map->count(), j);
    Base::Object::Collect();
    ASSERT_EQ(0, TestObject::count);
}
TEST(IterableTest, DelegateMapIterable)
{
    ASSERT_EQ(0, TestObject::count);
    Base::Object::Mark();
    Base::DelegateMap *map = new (Base::collect) Base::DelegateMap;
    for (ssize_t i = 0; i < 100; i++)
    {
        TestObject *obj = new (Base::collect) TestObject;
        Base::DelegateData del = { 0, obj };
        map->setDelegate(obj, del);
    }
    ssize_t j = 0;
    foreach (Base::Object *obj, map)
    {
        ASSERT_EQ(obj, map->delegate(obj).data);
        j++;
    }
    ASSERT_EQ((ssize_t)map->count(), j);
    j = 0;
    foreach (Base::Object *obj, map->keys())
    {
        ASSERT_EQ(obj, map->delegate(obj).data);
        j++;
    }
    ASSERT_EQ((ssize_t)map->count(), j);
    Base::Object::Collect();
    ASSERT_EQ(0, TestObject::count);
}
