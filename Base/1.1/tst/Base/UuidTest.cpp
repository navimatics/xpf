/*
 * Base/UuidTest.cpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#include <Base/impl/Uuid.hpp>
#include <gtest/gtest.h>

TEST(UuidTest, ParseUnparse)
{
    Base::impl::Uuid uuid;
    const char *uuid_str = "46FB4F2A-7298-4E25-B98F-A746A8D23067";
    char uuid_buf[36 + 1];
    ASSERT_TRUE(Base::impl::UuidParse(uuid_str, uuid));
    ASSERT_TRUE(Base::impl::UuidUnparse(uuid, uuid_buf, sizeof uuid_buf));
    ASSERT_STRCASEEQ(uuid_str, uuid_buf);
}
TEST(UuidTest, ParseUnparseRaw)
{
    Base::impl::Uuid uuid;
    const char *uuid_str = "46FB4F2A72984E25B98FA746A8D23067";
    char uuid_buf[36 + 1];
    ASSERT_TRUE(Base::impl::UuidParse(uuid_str, uuid));
    ASSERT_TRUE(Base::impl::UuidUnparseRaw(uuid, uuid_buf, sizeof uuid_buf));
    ASSERT_STRCASEEQ(uuid_str, uuid_buf);
}
TEST(UuidTest, CreateUnparseParse)
{
    Base::impl::Uuid uuid, uuid2;
    Base::impl::UuidCreate(uuid);
    char uuid_buf[36 + 1];
    ASSERT_TRUE(Base::impl::UuidUnparse(uuid, uuid_buf, sizeof uuid_buf));
    ASSERT_TRUE(Base::impl::UuidParse(uuid_buf, uuid2));
    ASSERT_TRUE(0 == memcmp(uuid, uuid2, sizeof(uuid)));
}
TEST(UuidTest, CreateUnparseParseRaw)
{
    Base::impl::Uuid uuid, uuid2;
    Base::impl::UuidCreate(uuid);
    char uuid_buf[36 + 1];
    ASSERT_TRUE(Base::impl::UuidUnparseRaw(uuid, uuid_buf, sizeof uuid_buf));
    ASSERT_TRUE(Base::impl::UuidParse(uuid_buf, uuid2));
    ASSERT_TRUE(0 == memcmp(uuid, uuid2, sizeof(uuid)));
}
