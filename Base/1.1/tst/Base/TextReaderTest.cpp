/*
 * Base/TextReaderTest.cpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#include <Base/TextReader.hpp>
#include <Base/MemoryStream.hpp>
#include <gtest/gtest.h>

TEST(TextReaderTest, ReadAll)
{
    static const char text[] =
        "123\n"
        "abc\n"
        "def\r\n"
        "\n"
        "\r\n"
        "abc:--:def-::-ghi \t jkl   mnopqrstu\n"
        " 123 456 12.3 45.6 abc\n"
        ;
    mark_and_collect
    {
        Base::MemoryStream *stream = new (Base::collect) Base::MemoryStream(text);
        Base::TextReader *reader = new (Base::collect) Base::TextReader(stream);
        ASSERT_EQ('1', reader->readChar());
        ASSERT_EQ('2', reader->readChar());
        ASSERT_EQ('3', reader->readChar());
        ASSERT_EQ('\n', reader->readChar());
        ASSERT_STREQ("abc", reader->readLine());
        ASSERT_STREQ("def", reader->readLine());
        ASSERT_STREQ("", reader->readLine());
        ASSERT_STREQ("", reader->readLine());
        ASSERT_STREQ("abc", reader->readUntil(":--:"));
        ASSERT_STREQ("def", reader->readUntil("-::-"));
        ASSERT_STREQ("ghi", reader->readSpan("\t\n\v\f\r ", true));
        ASSERT_STREQ(" \t ", reader->readSpan("\t\n\v\f\r "));
        reader->skipSpan("\t\n\v\f\r ", true);
        reader->skipSpan("\t\n\v\f\r ");
        ASSERT_STREQ("mnopqrstu", reader->readLine());
        ASSERT_EQ(123, reader->readIntegerValue());
        ASSERT_EQ(456, reader->readIntegerValue());
        ASSERT_EQ(12.3, reader->readRealValue());
        ASSERT_EQ(45.6, reader->readRealValue());
        ASSERT_TRUE(Base::isNone(reader->readIntegerValue()));
        ASSERT_TRUE(Base::isNone(reader->readRealValue()));
        ASSERT_STREQ("abc", reader->readLine());
        ASSERT_EQ(0, reader->readChar());
        ASSERT_EQ(0, reader->readChar());
        ASSERT_EQ(0, reader->readLine());
        ASSERT_EQ(0, reader->readLine());
        ASSERT_EQ(0, reader->readUntil(":--:"));
        ASSERT_EQ(0, reader->readUntil(":--:"));
        ASSERT_EQ(0, reader->readSpan("\t\n\v\f\r ", true));
        ASSERT_EQ(0, reader->readSpan("\t\n\v\f\r "));
    }
}
TEST(TextReaderTest, ReadLine)
{
    static const char text[] =
        "abc\n"
        "def\r\n"
        "ghi"
        ;
    mark_and_collect
    {
        Base::MemoryStream *stream = new (Base::collect) Base::MemoryStream(text);
        Base::TextReader *reader = new (Base::collect) Base::TextReader(stream);
        ASSERT_STREQ("abc", reader->readLine());
        ASSERT_STREQ("def", reader->readLine());
        ASSERT_STREQ("ghi", reader->readLine());
        ASSERT_EQ(0, reader->readLine());
        ASSERT_EQ(0, reader->readLine());
    }
}
TEST(TextReaderTest, ReadUntil)
{
    static const char text[] =
    "abc:--:def:--:ghi"
    ;
    mark_and_collect
    {
        Base::MemoryStream *stream = new (Base::collect) Base::MemoryStream(text);
        Base::TextReader *reader = new (Base::collect) Base::TextReader(stream);
        ASSERT_STREQ("abc", reader->readUntil(":--:"));
        ASSERT_STREQ("def", reader->readUntil(":--:"));
        ASSERT_STREQ("ghi", reader->readUntil(":--:"));
        ASSERT_EQ(0, reader->readUntil(":--:"));
        ASSERT_EQ(0, reader->readUntil(":--:"));
    }
}
TEST(TextReaderTest, ReadSpan)
{
    static const char text[] =
    " \t ab\t \tba\t  "
    ;
    mark_and_collect
    {
        Base::MemoryStream *stream = new (Base::collect) Base::MemoryStream(text);
        Base::TextReader *reader = new (Base::collect) Base::TextReader(stream);
        ASSERT_STREQ(" \t ", reader->readSpan(" \t"));
        reader->skipSpan("ab");
        ASSERT_STREQ("\t \t", reader->readSpan(" \t"));
        reader->skipSpan("ab");
        ASSERT_STREQ("\t  ", reader->readSpan(" \t"));
        ASSERT_EQ(0, reader->readSpan(" \t"));
        ASSERT_EQ(0, reader->readSpan(" \t"));
    }
}
TEST(TextReaderTest, ReadSpanComplement)
{
    static const char text[] =
    "abc  def  ghi"
    ;
    mark_and_collect
    {
        Base::MemoryStream *stream = new (Base::collect) Base::MemoryStream(text);
        Base::TextReader *reader = new (Base::collect) Base::TextReader(stream);
        ASSERT_STREQ("abc", reader->readSpan(" ", true));
        reader->skipSpan(" ");
        ASSERT_STREQ("def", reader->readSpan(" ", true));
        reader->skipSpan(" ");
        ASSERT_STREQ("ghi", reader->readSpan(" ", true));
        ASSERT_EQ(0, reader->readSpan(" ", true));
        ASSERT_EQ(0, reader->readSpan(" ", true));
    }
}
TEST(TextReaderTest, ReadInteger)
{
    static const char text[] =
    "123  456  789"
    ;
    mark_and_collect
    {
        Base::MemoryStream *stream = new (Base::collect) Base::MemoryStream(text);
        Base::TextReader *reader = new (Base::collect) Base::TextReader(stream);
        ASSERT_EQ(123, reader->readIntegerValue());
        ASSERT_EQ(456, reader->readIntegerValue());
        ASSERT_EQ(789, reader->readIntegerValue());
        ASSERT_TRUE(Base::isNone(reader->readIntegerValue()));
        ASSERT_TRUE(Base::isNone(reader->readIntegerValue()));
    }
}
