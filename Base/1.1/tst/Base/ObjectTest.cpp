/*
 * Base/ObjectTest.cpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#include <Base/Object.hpp>
#include <gtest/gtest.h>
#include "TestObject.hpp"

TEST(ObjectTest, NewRelease)
{
    TestObject *objbuf[10000];
    for (size_t i = 0; 10000 > i; i++)
        objbuf[i] = new TestObject;
    ASSERT_EQ(10000, TestObject::count);
    for (size_t i = 0; 10000 > i; i++)
        objbuf[i]->release();
    ASSERT_EQ(0, TestObject::count);
}
TEST(ObjectTest, RetainRelease)
{
    TestObject *objbuf[10000];
    for (size_t i = 0; 10000 > i; i++)
        objbuf[i] = new TestObject;
    ASSERT_EQ(10000, TestObject::count);
    for (size_t i = 0; 10000 > i; i++)
        objbuf[i]->retain();
    ASSERT_EQ(10000, TestObject::count);
    for (size_t i = 0; 10000 > i; i++)
        objbuf[i]->release();
    ASSERT_EQ(10000, TestObject::count);
    for (size_t i = 0; 10000 > i; i++)
        objbuf[i]->release();
    ASSERT_EQ(0, TestObject::count);
}
TEST(ObjectTest, Assign)
{
    TestObject *objbuf[10000];
    for (size_t i = 0; 10000 > i; i++)
        objbuf[i] = new TestObject;
    ASSERT_EQ(10000, TestObject::count);
    for (size_t i = 0; 10000 > i; i++)
    {
        TestObject *o = new TestObject;
        Base::obj_assign(objbuf[i], o);
        o->release();
    }
    ASSERT_EQ(10000, TestObject::count);
    for (size_t i = 0; 10000 > i; i++)
        Base::obj_assign(objbuf[i], (TestObject *)0);
    ASSERT_EQ(0, TestObject::count);
}
TEST(ObjectTest, CollectionPool)
{
    Base::Object::Mark();
    Base::Object::Collect();
    Base::Object::Mark();
    for (size_t i = 0; 10000 > i; i++)
        new (Base::collect) TestObject;
    for (size_t i = 0; 10000 > i; i++)
        (new TestObject)->autorelease();
    ASSERT_EQ(20000, TestObject::count);
    Base::Object::Collect();
    ASSERT_EQ(0, TestObject::count);
}
TEST(ObjectTest, NestedCollectionPool)
{
    TestObject *objbuf[10000];
    Base::Object::Mark();
    for (size_t i = 0; 10000 > i; i++)
        objbuf[i] = new (Base::collect) TestObject;
    ASSERT_EQ(10000, TestObject::count);
    Base::Object::Mark();
    for (size_t i = 0; 10000 > i; i++)
        new (Base::collect) TestObject;
    ASSERT_EQ(20000, TestObject::count);
    Base::Object::Mark();
    for (size_t i = 0; 10000 > i; i++)
        new (Base::collect) TestObject;
    ASSERT_EQ(30000, TestObject::count);
    Base::Object::Collect();
    ASSERT_EQ(20000, TestObject::count);
    for (size_t i = 0; 10000 > i; i++)
        new (Base::collect) TestObject;
    ASSERT_EQ(30000, TestObject::count);
    Base::Object::Collect();
    ASSERT_EQ(10000, TestObject::count);
    Base::Object::Mark();
    for (size_t i = 0; 10000 > i; i++)
        new (Base::collect) TestObject;
    ASSERT_EQ(20000, TestObject::count);
    Base::Object::Mark();
    for (size_t i = 0; 10000 > i; i++)
        new (Base::collect) TestObject;
    ASSERT_EQ(30000, TestObject::count);
    Base::Object::Collect();
    ASSERT_EQ(20000, TestObject::count);
    for (size_t i = 0; 10000 > i; i++)
        new (Base::collect) TestObject;
    ASSERT_EQ(30000, TestObject::count);
    Base::Object::Collect();
    ASSERT_EQ(10000, TestObject::count);
    for (size_t i = 0; 10000 > i; i++)
        ASSERT_LT(objbuf[i]->objid, TestObject::count);
    Base::Object::Collect();
    ASSERT_EQ(0, TestObject::count);
}
TEST(ObjectTest, ThreadsafeRetainRelease)
{
    TestObject *objbuf[10000];
    for (size_t i = 0; 10000 > i; i++)
    {
        objbuf[i] = new TestObject;
        objbuf[i]->threadsafe();
    }
    ASSERT_EQ(10000, TestObject::count);
    for (size_t i = 0; 10000 > i; i++)
        objbuf[i]->retain();
    ASSERT_EQ(10000, TestObject::count);
    for (size_t i = 0; 10000 > i; i++)
        objbuf[i]->release();
    ASSERT_EQ(10000, TestObject::count);
    for (size_t i = 0; 10000 > i; i++)
        objbuf[i]->release();
    ASSERT_EQ(0, TestObject::count);
}
TEST(ObjectTest, HashCmp)
{
    Base::Object::Mark();
    Base::Object *o1 = new (Base::collect) Base::Object;
    Base::Object *o2 = new (Base::collect) Base::Object;
    ASSERT_TRUE(o1->hash() != o2->hash());
    ASSERT_TRUE(0 == o1->cmp(o1));
    ASSERT_FALSE(0 == o1->cmp(o2));
    Base::Object::Collect();
}
TEST(ObjectTest, className)
{
    Base::Object::Mark();
    Base::Object *obj = new (Base::collect) Base::Object;
    ASSERT_STREQ("Base::impl::Object", obj->className());
    obj = new (Base::collect) TestObject;
    ASSERT_STREQ("TestObject", obj->className());
    Base::Object::Collect();
}
