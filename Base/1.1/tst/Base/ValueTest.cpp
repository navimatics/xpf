/*
 * Base/ValueTest.cpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#include <Base/Value.hpp>
#include <gtest/gtest.h>

TEST(ValueTest, Test1)
{
    Base::Object::Mark();
    Base::Value *B = Base::Value::create(true); B->autorelease();
    Base::Value *l = Base::Value::create(42); l->autorelease();
    Base::Value *d = Base::Value::create(42.0); d->autorelease();
    ASSERT_EQ(true, B->booleanValue());
    ASSERT_EQ(42, l->integerValue());
    ASSERT_EQ(42.0, d->realValue());
    ASSERT_STREQ("1", B->strrepr());
    ASSERT_STREQ("42", l->strrepr());
    ASSERT_STREQ("42.0", d->strrepr());
    Base::Object::Collect();
}
