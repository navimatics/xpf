/*
 * Base/UStringTest.cpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#include <Base/UString.hpp>
#include <gtest/gtest.h>

TEST(UStringTest, ustr_new)
{
    const Base::unichar_t ulit[] = { '1', '2', '3', '4', '5', 0 };
    const Base::unichar_t *ustr = Base::ustr_new(ulit);
    ASSERT_EQ((size_t)5, Base::ustr_length(ustr));
    ASSERT_EQ((size_t)5, Base::ustr_obj(ustr)->length());
    ASSERT_TRUE(0 == memcmp(ulit, ustr, sizeof ulit));
    ASSERT_TRUE(0 == memcmp(ulit, Base::ustr_obj(ustr)->uchars(), sizeof ulit));
    const Base::unichar_t *ustr2 = Base::ustr_new(ustr, Base::ustr_length(ustr));
    ASSERT_EQ((size_t)5, Base::ustr_length(ustr2));
    ASSERT_EQ((size_t)5, Base::ustr_obj(ustr2)->length());
    ASSERT_TRUE(0 == memcmp(ustr, ustr2, sizeof ulit));
    ASSERT_TRUE(0 == memcmp(Base::ustr_obj(ustr)->uchars(), Base::ustr_obj(ustr2)->uchars(), sizeof ulit));
    Base::ustr_release(ustr2);
    Base::ustr_release(ustr);
}
TEST(UStringTest, ustring)
{
    Base::Object::Mark();
    const Base::unichar_t ulit[] = { '1', '2', '3', '4', '5', 0 };
    const Base::unichar_t *ustr = Base::ustring(ulit);
    ASSERT_EQ((size_t)5, Base::ustr_length(ustr));
    ASSERT_EQ((size_t)5, Base::ustr_obj(ustr)->length());
    ASSERT_TRUE(0 == memcmp(ulit, ustr, sizeof ulit));
    ASSERT_TRUE(0 == memcmp(ulit, Base::ustr_obj(ustr)->uchars(), sizeof ulit));
    const Base::unichar_t *ustr2 = Base::ustring(ustr, Base::ustr_length(ustr));
    ASSERT_EQ((size_t)5, Base::ustr_length(ustr2));
    ASSERT_EQ((size_t)5, Base::ustr_obj(ustr2)->length());
    ASSERT_TRUE(0 == memcmp(ustr, ustr2, sizeof ulit));
    ASSERT_TRUE(0 == memcmp(Base::ustr_obj(ustr)->uchars(), Base::ustr_obj(ustr2)->uchars(), sizeof ulit));
    Base::Object::Collect();
}
TEST(UStringTest, slice)
{
    Base::Object::Mark();
    const Base::unichar_t *ustr = Base::ustring(0);
    ASSERT_EQ((size_t)0, Base::ustr_length(ustr));
    const Base::unichar_t *slice = Base::ustr_slice(ustr, -10, 10);
    ASSERT_EQ((size_t)0, Base::ustr_length(slice));
    const Base::unichar_t ulit[] = { '1', '2', '3', '4', '5', 0 };
    ustr = Base::ustring(ulit);
    const Base::unichar_t ulit12[] = { '1', '2',  0 };
    const Base::unichar_t ulit23[] = { '2', '3', 0 };
    const Base::unichar_t ulit34[] = { '3', '4', 0 };
    const Base::unichar_t ulit345[] = { '3', '4', '5', 0 };
    const Base::unichar_t ulit0[] = { 0 };
    ASSERT_TRUE(0 == memcmp(ulit12, Base::ustr_slice(ustr, 0, 2), sizeof ulit12));
    ASSERT_TRUE(0 == memcmp(ulit23, Base::ustr_slice(ustr, 1, 3), sizeof ulit23));
    ASSERT_TRUE(0 == memcmp(ulit345, Base::ustr_slice(ustr, -3), sizeof ulit345));
    ASSERT_TRUE(0 == memcmp(ulit34, Base::ustr_slice(ustr, -3, -1), sizeof ulit34));
    ASSERT_TRUE(0 == memcmp(ulit345, Base::ustr_slice(ustr, -3, 1000), sizeof ulit345));
    ASSERT_TRUE(0 == memcmp(ulit, Base::ustr_slice(ustr, -1000, 1000), sizeof ulit));
    ASSERT_TRUE(0 == memcmp(ulit0, Base::ustr_slice(ustr, -1000, -1000), sizeof ulit0));
    ASSERT_TRUE(0 == memcmp(ulit0, Base::ustr_slice(ustr, 1000, 1000), sizeof ulit0));
    Base::Object::Collect();
}
TEST(UStringTest, _concat)
{
    Base::Object::Mark();
    const Base::unichar_t ulit0[] = { 'T', 'h', 'e', 0 };
    const Base::unichar_t ulit1[] = { 'T', 'h', 'e', ' ', 'q', 'u', 'i', 'c', 'k', ' ', 'b', 'r', 'o', 'w', 'n', ' ', 'f', 'o', 'x', 0 };
    const Base::unichar_t ulit2[] = { 'T', 'h', 'e', ' ', 'q', 'u', 'i', 'c', 'k', ' ', 'b', 'r', 'o', 'w', 'n', ' ', 'f', 'o', 'x',
        ' ', 'j', 'u', 'm', 'p', 's', 0 };
    const Base::unichar_t ulit3[] = { 'T', 'h', 'e', ' ', 'q', 'u', 'i', 'c', 'k', ' ', 'b', 'r', 'o', 'w', 'n', ' ', 'f', 'o', 'x',
        ' ', 'j', 'u', 'm', 'p', 's', ' ', 'o', 'v', 'e', 'r', ' ', 't', 'h', 'e', ' ', 'l', 'a', 'z', 'y', ' ', 'd', 'o', 'g', '.', 0 };
    const Base::unichar_t *s0 = Base::ustring(ulit0);
    const Base::unichar_t *s1 = Base::ustring(ulit1);
    const Base::unichar_t *s2 = Base::ustring(ulit2);
    const Base::unichar_t *s3 = Base::ustring(ulit3);
    size_t h0 = Base::ustr_obj(s0)->hash();
    size_t h1 = Base::ustr_obj(s1)->hash();
    size_t h2 = Base::ustr_obj(s2)->hash();
    size_t h3 = Base::ustr_obj(s3)->hash();
    const Base::unichar_t flit0[] = { 'T', 'h', 'e', 0 };
    const Base::unichar_t flit1[] = { ' ', 'q', 'u', 'i', 'c', 'k', ' ', 'b', 'r', 'o', 'w', 'n', ' ', 'f', 'o', 'x', 0 };
    const Base::unichar_t flit2[] = { ' ', 'j', 'u', 'm', 'p', 's', 0 };
    const Base::unichar_t flit3[] = { ' ', 'o', 'v', 'e', 'r', ' ', 't', 'h', 'e', 0 };
    const Base::unichar_t flit4[] = { ' ', 'l', 'a', 'z', 'y', ' ', 'd', 'o', 'g', '.', 0 };
    const Base::unichar_t *t = Base::ustr_new(flit0);
    ASSERT_TRUE(h0 == Base::ustr_obj(t)->hash());
    t = Base::ustr_obj(t)->_concat(flit1)->uchars();
    ASSERT_TRUE(h1 == Base::ustr_obj(t)->hash());
    t = Base::ustr_obj(t)->_concat(flit2)->uchars();
    ASSERT_TRUE(h2 == Base::ustr_obj(t)->hash());
    t = Base::ustr_obj(t)->_concat(flit3)->uchars();
    t = Base::ustr_obj(t)->_concat(flit4)->uchars();
    ASSERT_TRUE(h3 == Base::ustr_obj(t)->hash());
    Base::ustr_release(t);
    const Base::unichar_t *t1 = Base::ustr_new(flit0);
    t1 = Base::ustr_obj(t1)->_concat(flit1)->uchars();
    ASSERT_TRUE(h1 == Base::ustr_obj(t1)->hash());
    Base::ustr_release(t1);
    Base::Object::Collect();
}
TEST(UStringTest, assign)
{
    const Base::unichar_t ulit12345[] = { '1', '2', '3', '4', '5', 0 };
    const Base::unichar_t ulit6789[] = { '6', '7', '8', '9', 0 };
    const Base::unichar_t *ustr = Base::ustr_new(ulit12345);
    const Base::unichar_t *ustr2 = Base::ustr_new(ulit6789);
    Base::ustr_assign(ustr, ustr2);
    ASSERT_EQ((size_t)4, Base::ustr_length(ustr));
    ASSERT_TRUE(0 == memcmp(ulit6789, ustr, sizeof ulit6789));
    Base::ustr_assign_copy(ustr, ulit12345);
    ASSERT_EQ((size_t)5, Base::ustr_length(ustr));
    ASSERT_TRUE(0 == memcmp(ulit12345, ustr, sizeof ulit12345));
    Base::ustr_assign(ustr2, 0);
    Base::ustr_assign_copy(ustr, 0);
}
