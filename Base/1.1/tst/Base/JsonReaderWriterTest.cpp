/*
 * Base/JsonReaderWriterTest.cpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#include <Base/JsonReader.hpp>
#include <Base/JsonWriter.hpp>
#include <Base/CString.hpp>
#include <Base/MemoryStream.hpp>
#include <Base/Snprintf.hpp>
#include <gtest/gtest.h>

class JsonTestReader : public Base::JsonReader
{
public:
    JsonTestReader(const char **chunks, size_t length);
    void reset();
protected:
    ssize_t readBuffer(void *buf, size_t size);
private:
    const char **_chunks;
    size_t _clength;
    size_t _cindex;
};
JsonTestReader::JsonTestReader(const char **chunks, size_t length)
{
    _chunks = chunks;
    _clength = length;
}
void JsonTestReader::reset()
{
    _cindex = 0;
    Base::JsonReader::reset();
}
ssize_t JsonTestReader::readBuffer(void *buf, size_t size)
{
    if (_clength <= _cindex)
        return 0;
    size_t len = strlen(_chunks[_cindex]);
    assert(size > len);
    memcpy(buf, _chunks[_cindex], len);
    _cindex++;
    return len;
}

class JsonTestWriter : public Base::JsonWriter
{
public:
    JsonTestWriter(bool pretty = false);
    ~JsonTestWriter();
    const char *chars();
protected:
    ssize_t writeBuffer(const void *buf, size_t size);
private:
    const char *_xmltext;
};
JsonTestWriter::JsonTestWriter(bool pretty) : JsonWriter(0, pretty)
{
    _xmltext = Base::cstr_new(0);
}
JsonTestWriter::~JsonTestWriter()
{
    Base::cstr_obj(_xmltext)->release();
}
const char *JsonTestWriter::chars()
{
    return _xmltext;
}
ssize_t JsonTestWriter::writeBuffer(const void *buf, size_t size)
{
    _xmltext = Base::cstr_obj(_xmltext)->_concat((const char *)buf, size)->chars();
    return size;
}

static const char *JoinChunks(const char **chunks, size_t length)
{
    const char *s = Base::cstr_new(0);
    for (size_t i = 0; length > i; i++)
        s = Base::cstr_obj(s)->_concat(chunks[i])->chars();
    Base::cstr_autorelease(s);
    return s;
}

static const char *chunks1[] =
{
    "{"
    "\"glos","sary\":{"
    "\"","ti","tle","\":\"example glossary\"",","
    "\"GlossDiv\"",":","{",""
    "\"title\":\"S\",",""
    "\"GlossList\":{"
    "\"GlossEntry\":{"
    "\"ID\":\"SGML\","
    "\"SortAs\":\"SGML\","
    "\"GlossTerm\":\"","Standard Generalized Markup Language\","
    "\"Acronym\":\"SGML","\","
    "\"Abbrev\":\"ISO 8879:1986\","
    "\"GlossDef\":{"
    "\"para\":\"A meta-markup language, used to create markup languages such as DocBook.\","
    "\"GlossSeeAlso\":[","\"GML\"",",","\"XML\"","]"
    "},"
    "\"","G","l","o","s","s","S","e","e","\"",":","\"","m","a","r","k","u","p","\"",""
    "}",""
    "}",""
    "}",""
    "}",""
    "}",""
};
static const char *chunks2[] =
{
    "{\"menu\":{"
    "\"id\":\"file\","
    "\"value\":\"File\","
    "\"popup\":{"
    "\"menuitem\":["
    "{\"value\":\"New\",\"onclick\":\"CreateNewDoc()\"},"
    "{\"value\":\"Open\",\"onclick\":\"OpenDoc()\"},"
    "{\"value\":\"Close\",\"onclick\":\"CloseDoc()\"}"
    "]"
    "}"
    "}}"
};
static const char *chunks3[] =
{
    "{\"widget\":{"
    "\"debug\":\"on\","
    "\"window\":{"
    "\"title\":\"Sample Konfabulator Widget\","
    "\"name\":\"main_window\","
    "\"width\":5","00,"
    "\"height\":500",""
    "},"
    "\"image\":{"
    "\"src\":\"Images/Sun.png\","
    "\"name\":\"sun1\","
    "\"hOffset\":250,"
    "\"vOffset\":250,"
    "\"alignment\":\"center\""
    "},"
    "\"text\":{"
    "\"data\":\"Click Here\","
    "\"size\":36,"
    "\"style\":\"bold\","
    "\"name\":\"text1\","
    "\"hOffset\":250,"
    "\"vOffset\":100,"
    "\"alignment\":\"center\","
    "\"onMouseUp\":\"sun1.opacity = (sun1.opacity / 100) * 90;\""
    "}",
    "}}"
};
static const char *chunks4[] =
{
    "{\"web-app\":{"
    "\"servlet\":["
    "{"
    "\"servlet-name\":\"cofaxCDS\","
    "\"servlet-class\":\"org.cofax.cds.CDSServlet\","
    "\"init-param\":{"
    "\"configGlossary:installationAt\":\"Philadelphia, PA\","
    "\"configGlossary:adminEmail\":\"ksm@pobox.com\","
    "\"configGlossary:poweredBy\":\"Cofax\","
    "\"configGlossary:poweredByIcon\":\"/images/cofax.gif\","
    "\"configGlossary:staticPath\":\"/content/static\","
    "\"templateProcessorClass\":\"org.cofax.WysiwygTemplate\","
    "\"templateLoaderClass\":\"org.cofax.FilesTemplateLoader\","
    "\"templatePath\":\"templates\","
    "\"templateOverridePath\":\"\","
    "\"defaultListTemplate\":\"listTemplate.htm\","
    "\"defaultFileTemplate\":\"articleTemplate.htm\","
    "\"useJSP\":fal","se,"
    "\"jspListTemplate\":\"listTemplate.jsp\","
    "\"jspFileTemplate\":\"articleTemplate.jsp\","
    "\"cachePackageTagsTrack\":200,"
    "\"cachePackageTagsStore\":200,"
    "\"cachePackageTagsRefresh\":60,"
    "\"cacheTemplatesTrack\":100,"
    "\"cacheTemplatesStore\":50,"
    "\"cacheTemplatesRefresh\":15,"
    "\"cachePagesTrack\":200,"
    "\"cachePagesStore\":100,"
    "\"cachePagesRefresh\":10,"
    "\"cachePagesDirtyRead\":10,"
    "\"searchEngineListTemplate\":\"forSearchEnginesList.htm\","
    "\"searchEngineFileTemplate\":\"forSearchEngines.htm\","
    "\"searchEngineRobotsDb\":\"WEB-INF/robots.db\","
    "\"useDataStore\":true",","
    "\"dataStoreClass\":\"org.cofax.SqlDataStore\","
    "\"redirectionClass\":\"org.cofax.SqlRedirection\","
    "\"dataStoreName\":\"cofax\","
    "\"dataStoreDriver\":\"com.microsoft.jdbc.sqlserver.SQLServerDriver\","
    "\"dataStoreUrl\":\"jdbc:microsoft:sqlserver://LOCALHOST:1433;DatabaseName=goon\","
    "\"dataStoreUser\":\"sa\","
    "\"dataStorePassword\":\"dataStoreTestQuery\","
    "\"dataStoreTestQuery\":\"SET NOCOUNT ON;select test='test';\","
    "\"dataStoreLogFile\":\"/usr/local/tomcat/logs/datastore.log\","
    "\"dataStoreInitConns\":10,"
    "\"dataStoreMaxConns\":100,"
    "\"dataStoreConnUsageLimit\":100,"
    "\"dataStoreLogLevel\":\"debug\","
    "\"maxUrlLength\":500}},"
    "{"
    "\"servlet-name\":\"cofaxEmail\","
    "\"servlet-class\":\"org.cofax.cds.EmailServlet\","
    "\"init-param\":{"
    "\"mailHost\":\"mail1\","
    "\"mailHostOverride\":\"mail2\"}},"
    "{"
    "\"servlet-name\":\"cofaxAdmin\","
    "\"servlet-class\":\"org.cofax.cds.AdminServlet\"},"
    ""
    "{"
    "\"servlet-name\":\"fileServlet\","
    "\"servlet-class\":\"org.cofax.cds.FileServlet\"},"
    "{"
    "\"servlet-name\":\"cofaxTools\","
    "\"servlet-class\":\"org.cofax.cms.CofaxToolsServlet\","
    "\"init-param\":{"
    "\"templatePath\":\"toolstemplates/\","
    "\"log\":1,"
    "\"logLocation\":\"/usr/local/tomcat/logs/CofaxTools.log\","
    "\"logMaxSize\":\"\","
    "\"dataLog\":1,"
    "\"dataLogLocation\":\"/usr/local/tomcat/logs/dataLog.log\","
    "\"dataLogMaxSize\":\"\","
    "\"removePageCache\":\"/content/admin/remove?cache=pages&id=\","
    "\"removeTemplateCache\":\"/content/admin/remove?cache=templates&id=\","
    "\"fileTransferFolder\":\"/usr/local/tomcat/webapps/content/fileTransferFolder\","
    "\"lookInContext\":1,"
    "\"adminGroupID\":4,"
    "\"betaServer\":true}}],"
    "\"servlet-mapping\":{"
    "\"cofaxCDS\":\"/\","
    "\"cofaxEmail\":\"/cofaxutil/aemail/*\","
    "\"cofaxAdmin\":\"/admin/*\","
    "\"fileServlet\":\"/static/*\","
    "\"cofaxTools\":\"/tools/*\"},"
    ""
    "\"taglib\":{"
    "\"taglib-uri\":\"cofax.tld\","
    "\"taglib-location\":\"/WEB-INF/tlds/cofax.tld\"}}}"
};
static const char *chunks5[] =
{
    "{\"menu\":{"
    "\"header\":\"SVG Viewer\","
    "\"items\":["
    "{\"id\":\"Open\"},"
    "{\"id\":\"OpenNew\",\"label\":\"Open New\"},"
    "nu","ll,"
    "{\"id\":\"ZoomIn\",\"label\":\"Zoom In\"},"
    "{\"id\":\"ZoomOut\",\"label\":\"Zoom Out\"},"
    "{\"id\":\"OriginalView\",\"label\":\"Original View\"},"
    "null,"
    "{\"id\":\"Quality\"},"
    "{\"id\":\"Pause\"},"
    "{\"id\":\"Mute\"},"
    "null,"
    "{\"id\":\"Find\",\"label\":\"Find...\"},"
    "{\"id\":\"FindAgain\",\"label\":\"Find Again\"},"
    "{\"id\":\"Copy\"},"
    "{\"id\":\"CopyAgain\",\"label\":\"Copy Again\"},"
    "{\"id\":\"CopySVG\",\"label\":\"Copy SVG\"},"
    "{\"id\":\"ViewSVG\",\"label\":\"View SVG\"},"
    "{\"id\":\"ViewSource\",\"label\":\"View Source\"},"
    "{\"id\":\"SaveAs\",\"label\":\"Save As\"},"
    "null,"
    "{\"id\":\"Help\"},"
    "{\"id\":\"About\",\"label\":\"About Adobe CVG Viewer...\"}"
    "]"
    "}}"
};
static const char *chunks6[] =
{
    "["
    "\"\\","\"\","
    "\"\\\"\","
    "\"\\","\\\","
    "\"\\\\\","
    "\"\\","t\","
    "\"\\t\","
    "\"\\u0001",
    "\\u0142"
    "\\uaa42"
    "\\","uaa42"
    "\\u","aa42"
    "\\uaa","42\","
    "100,"
    "100",".0,"
    "100.","0,"
    "100.0",","
    "-","100.0,"
    "100.0"
    "]"
};
static const char *chunks7[] =
{
    "["
    "Na","N,"
    "Infinity,"
    "-", "Infinity,",
    "-I", "nfinity",
    "]"
};
static struct
{
    const char **ptr;
    size_t nel;
} chunksArray[] =
{
    { chunks1, NELEMS(chunks1) },
    { chunks2, NELEMS(chunks2) },
    { chunks3, NELEMS(chunks3) },
    { chunks4, NELEMS(chunks4) },
    { chunks5, NELEMS(chunks5) },
    { chunks6, NELEMS(chunks6) },
    { chunks7, NELEMS(chunks7) },
};
static const char *chunksToIndent[] =
{
    "[\"one\",2,{\"foo\":null,\"quux\":true,\"bar\":[1, 2, []]},{},[[[[[[]]]]]]]"
};
static const char *indented =
"[\n"
"  \"one\",\n"
"  2,\n"
"  {\n"
"    \"foo\": null,\n"
"    \"quux\": true,\n"
"    \"bar\": [\n"
"      1,\n"
"      2,\n"
"      []\n"
"    ]\n"
"  },\n"
"  {},\n"
"  [\n"
"    [\n"
"      [\n"
"        [\n"
"          [\n"
"            []\n"
"          ]\n"
"        ]\n"
"      ]\n"
"    ]\n"
"  ]\n"
"]";

TEST(JsonReaderWriterTest, ReadWrite)
{
    Base::Object::Mark();
    for (size_t i = 0; NELEMS(chunksArray) > i; i++)
    {
        const char **chunks = chunksArray[i].ptr;
        size_t nchunks = chunksArray[i].nel;
        JsonTestReader *reader = new (Base::collect) JsonTestReader(chunks, nchunks);
        JsonTestWriter *writer = new (Base::collect) JsonTestWriter;
        for (;;)
        {
            Base::JsonEvent *event = reader->read();
            if (0 == event)
                break;
            writer->write(event);
        }
        const char *s = JoinChunks(chunks, nchunks);
        ASSERT_STREQ(s, writer->chars());
    }
    Base::Object::Collect();
}
TEST(JsonReaderWriterTest, ReadWriteReset)
{
    Base::Object::Mark();
    for (size_t i = 0; NELEMS(chunksArray) > i; i++)
    {
        const char **chunks = chunksArray[i].ptr;
        size_t nchunks = chunksArray[i].nel;
        JsonTestReader *reader = new (Base::collect) JsonTestReader(chunks, nchunks);
        JsonTestWriter *writer = new (Base::collect) JsonTestWriter;
        for (;;)
        {
            Base::JsonEvent *event = reader->read();
            if (0 == event)
                break;
            writer->write(event);
        }
        const char *s = JoinChunks(chunks, nchunks);
        ASSERT_STREQ(s, writer->chars());
        reader->reset();
        writer = new (Base::collect) JsonTestWriter;
        for (;;)
        {
            Base::JsonEvent *event = reader->read();
            if (0 == event)
                break;
            writer->write(event);
        }
        s = JoinChunks(chunks, nchunks);
        ASSERT_STREQ(s, writer->chars());
    }
    Base::Object::Collect();
}
TEST(JsonReaderWriterTest, ReadWriteLarge)
{
    Base::Object::Mark();
    Base::MemoryStream *memstream = new (Base::collect) Base::MemoryStream;
    memstream->write("[0", 2);
    for (size_t i = 1; i < 20000; i++)
    {
        char buf[64];
        portable_snprintf(buf, sizeof buf, ",%li", i);
        memstream->write(buf, strlen(buf));
    }
    memstream->write("]", 1);
    memstream->seek(0, SEEK_SET);
    Base::JsonReader *reader = new (Base::collect) Base::JsonReader(memstream);
    JsonTestWriter *writer = new (Base::collect) JsonTestWriter;
    for (;;)
    {
        Base::JsonEvent *event = reader->read();
        if (0 == event)
            break;
        writer->write(event);
    }
    ASSERT_TRUE(memstream->size() == Base::cstr_length(writer->chars()));
    ASSERT_TRUE(0 == memcmp(memstream->buffer(), writer->chars(), memstream->size()));
    Base::Object::Collect();
}
TEST(JsonReaderWriterTest, ReadPrettyWrite)
{
    Base::Object::Mark();
    JsonTestReader *reader = new (Base::collect) JsonTestReader(chunksToIndent, NELEMS(chunksToIndent));
    JsonTestWriter *writer = new (Base::collect) JsonTestWriter(true);
    for (;;)
    {
        Base::JsonEvent *event = reader->read();
        if (0 == event)
            break;
        writer->write(event);
    }
    ASSERT_STREQ(indented, writer->chars());
    Base::Object::Collect();
}
