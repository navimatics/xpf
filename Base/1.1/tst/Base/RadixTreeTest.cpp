/*
 * Base/RadixTreeTest.cpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#include <Base/Object.hpp>
#include <Base/impl/RadixTree.hpp>
#include <gtest/gtest.h>

class RadixTree : public Base::impl::RadixTree, public Base::impl::Iterable<Base::index_t>
{
public:
    bool getValue(Base::index_t index, Base::index_t &value)
    {
        Node *node = lookup(index);
        if (node != nullnode() && node->index == index)
        {
            value = node->value;
            return true;
        }
        else
        {
            value = 0;
            return false;
        }
    }
    void setValue(Base::index_t index, Base::index_t value)
    {
        Node *node = lookup(index);
        if (node != nullnode() && node->index == index)
            node->value = value;
        else
            insert(node, index, value);
    }
    void removeValue(Base::index_t index)
    {
        Node *node = lookup(index);
        if (node != nullnode() && node->index == index)
            remove(node);
    }
    void removeAllValues()
    {
        reset();
    }
    /* Iterable<index_t> */
    void *iterate(void *state, Base::index_t *bufp[2])
    {
        if (0 == state)
            state = new (Base::collect) NodeIterator(this);
        state = ((NodeIterator *)state)->iterate(state, bufp);
        Base::index_t *curp = bufp[0], *endp = bufp[1];
        while (curp < endp)
        {
            Node *node = (Node *)*curp;
            *curp = node->index;
            curp++;
        }
        return state;
    }
};

TEST(RadixTreeTest, BasicTest)
{
    Base::Object::Mark();

    Base::index_t value;
    RadixTree *tree = new (Base::collect) RadixTree;

    ASSERT_FALSE(tree->getValue(0, value));
    tree->setValue(0, 0);
    tree->__sanityCheck();
    ASSERT_TRUE(tree->getValue(0, value));
    tree->removeAllValues();
    tree->__sanityCheck();
    ASSERT_FALSE(tree->getValue(0, value));

    tree->setValue(42, 0);
    tree->__sanityCheck();
    ASSERT_TRUE(tree->getValue(42, value));
    ASSERT_FALSE(tree->getValue(0, value));
    ASSERT_FALSE(tree->getValue(43, value));
    tree->removeAllValues();
    tree->__sanityCheck();
    ASSERT_FALSE(tree->getValue(42, value));
    ASSERT_FALSE(tree->getValue(0, value));

    tree->setValue(42, 42);
    tree->__sanityCheck();
    ASSERT_TRUE(tree->getValue(42, value));
    ASSERT_EQ(42U, value);
    tree->setValue(42, 43);
    tree->__sanityCheck();
    ASSERT_TRUE(tree->getValue(42, value));
    ASSERT_EQ(43U, value);
    tree->removeAllValues();
    tree->__sanityCheck();
    ASSERT_FALSE(tree->getValue(42, value));

    tree->setValue(43, 0);
    tree->__sanityCheck();
    ASSERT_FALSE(tree->getValue(42, value));
    ASSERT_TRUE(tree->getValue(43, value));
    ASSERT_FALSE(tree->getValue(0, value));
    tree->setValue(42, 0);
    tree->__sanityCheck();
    ASSERT_TRUE(tree->getValue(42, value));
    ASSERT_TRUE(tree->getValue(43, value));
    ASSERT_FALSE(tree->getValue(0, value));
    tree->removeValue(42);
    tree->__sanityCheck();
    ASSERT_FALSE(tree->getValue(42, value));
    ASSERT_TRUE(tree->getValue(43, value));
    ASSERT_FALSE(tree->getValue(0, value));
    tree->removeValue(42);
    tree->__sanityCheck();
    ASSERT_FALSE(tree->getValue(42, value));
    ASSERT_TRUE(tree->getValue(43, value));
    ASSERT_FALSE(tree->getValue(0, value));
    tree->removeValue(43);
    tree->__sanityCheck();
    ASSERT_FALSE(tree->getValue(42, value));
    ASSERT_FALSE(tree->getValue(43, value));
    ASSERT_FALSE(tree->getValue(0, value));
    tree->removeValue(43);
    tree->__sanityCheck();
    ASSERT_FALSE(tree->getValue(42, value));
    ASSERT_FALSE(tree->getValue(43, value));
    ASSERT_FALSE(tree->getValue(0, value));
    tree->setValue(43, 0);
    tree->setValue(42, 0);
    tree->setValue(44, 0);
    tree->__sanityCheck();
    ASSERT_TRUE(tree->getValue(42, value));
    ASSERT_TRUE(tree->getValue(43, value));
    ASSERT_TRUE(tree->getValue(44, value));
    ASSERT_FALSE(tree->getValue(0, value));
    tree->removeAllValues();
    tree->__sanityCheck();
    ASSERT_FALSE(tree->getValue(42, value));
    ASSERT_FALSE(tree->getValue(43, value));
    ASSERT_FALSE(tree->getValue(0, value));

    tree->setValue(271195, 0);
    tree->setValue(67771, 0);
    tree->setValue(421415, 0);
    tree->setValue(174681, 0);
    tree->setValue(612388, 0);
    tree->setValue(588039, 0);
    tree->setValue(813460, 0);
    tree->setValue(989350, 0);
    tree->setValue(868791, 0);
    tree->setValue(910146, 0);
    tree->removeValue(271195);
    tree->removeValue(67771);
    tree->removeValue(421415);
    tree->removeValue(174681);
    tree->removeValue(612388);
    tree->__sanityCheck();
    tree->removeValue(588039);
    tree->__sanityCheck();
    tree->removeAllValues();

    tree->setValue(224302, 0);
    tree->setValue(623415, 0);
    tree->setValue(187996, 0);
    tree->setValue(544305, 0);
    tree->setValue(785640, 0);
    tree->setValue(96301, 0);
    tree->setValue(754888, 0);
    tree->setValue(232080, 0);
    tree->setValue(716747, 0);
    tree->setValue(850099, 0);
    tree->setValue(0, 0);
    tree->removeValue(224302);
    tree->removeValue(623415);
    tree->removeValue(187996);
    tree->__sanityCheck();
    tree->removeValue(544305);
    tree->__sanityCheck();
    tree->removeAllValues();

    {
        Base::index_t N = 1000;
        for (Base::index_t i = 0; i < N; i++)
            tree->setValue(i, i);
        for (Base::index_t i = 0; i < N; i++)
            tree->setValue(UINTPTR_MAX - i, UINTPTR_MAX - i);
        tree->__sanityCheck();
        for (Base::index_t i = 0; i < N; i++)
        {
            ASSERT_TRUE(tree->getValue(i, value));
            ASSERT_EQ(i, value);
        }
        for (Base::index_t i = 0; i < N; i++)
        {
            ASSERT_TRUE(tree->getValue(UINTPTR_MAX - i, value));
            ASSERT_EQ(UINTPTR_MAX - i, value);
        }
        Base::index_t count = 0;
        foreach (Base::index_t index, tree)
        {
            if (count < N)
                ASSERT_EQ(count, index);
            else
                ASSERT_EQ(UINTPTR_MAX - (N * 2 - 1 - count), index);
            count++;
        }
        ASSERT_EQ(N * 2, count);
        tree->removeAllValues();
        ASSERT_FALSE(tree->getValue(0, value));
        tree->__sanityCheck();
    }

    Base::Object::Collect();
}
TEST(RadixTreeTest, RandomTest)
{
    Base::Object::Mark();

    Base::index_t value;
    RadixTree *tree = new (Base::collect) RadixTree;
    srand(time(0));

    for (Base::index_t max = 1000; max < 100000; max += 10000)
    {
        Base::index_t N = 100 + rand() % (max - 100);
        Base::index_t *num = new Base::index_t[N];
        for (Base::index_t i = 0; i < N; i++)
            num[i] = rand() % 1000000;
        for (Base::index_t i = 0; i < N; i++)
            tree->setValue(num[i], num[i]);
        tree->__sanityCheck();
        for (Base::index_t i = 0; i < N; i++)
        {
            /* shuffle array a bit */
            Base::index_t tmp, swapidx;
            swapidx = rand() % N;
            tmp = num[swapidx];
            num[swapidx] = num[i];
            num[i] = tmp;
        }
        for (Base::index_t i = 0; i < N; i++)
        {
            ASSERT_TRUE(tree->getValue(num[i], value));
            ASSERT_EQ(num[i], value);
        }
        foreach (Base::index_t index, tree)
        {
            ASSERT_TRUE(tree->getValue(index, value));
            ASSERT_EQ(index, value);
        }
        for (Base::index_t i = 0; i < N; i++)
        {
            tree->removeValue(num[i]);
            ASSERT_FALSE(tree->getValue(num[i], value));
            if (i == N / 2)
                tree->__sanityCheck();
        }
        tree->__sanityCheck();
        tree->removeAllValues();
        ASSERT_FALSE(tree->getValue(num[0], value));
        delete[] num;
    }

    Base::Object::Collect();
}

#if 0
#include <Base/hash_map.hpp>
#include <map>
struct Hash
{
    size_t operator()(Base::index_t a) const
    {
        return a;
    }
};
struct Pred
{
    bool operator()(Base::index_t a, Base::index_t b) const
    {
        return a == b;
    }
};
TEST(RadixTreeTest, HashMapPerformance)
{
    const size_t N = 100000;
    Base::index_t *num = new Base::index_t[N];
    srand(333);
    for (Base::index_t i = 0; i < N; i++)
        num[i] = rand() % 1000000;
    Base::hash_map<Base::index_t, Base::index_t, Hash, Pred> map;
    for (Base::index_t i = 0; i < N; i++)
        map[num[i]] = i;
#if 1
    for (Base::index_t i = 0; i < N; i++)
        map[num[i]] = i;
    for (Base::index_t i = 0; i < N; i++)
        map[num[i]] = i;
    for (Base::index_t i = 0; i < N; i++)
        map[num[i]] = i;
    for (Base::index_t i = 0; i < N; i++)
        map[num[i]] = i;
#endif
    for (Base::index_t i = 0; i < 100000; i++)
        map.find(num[i]);
#if 1
    for (Base::index_t i = 0; i < 100000; i++)
        map.find(num[i]);
    for (Base::index_t i = 0; i < 100000; i++)
        map.find(num[i]);
    for (Base::index_t i = 0; i < 100000; i++)
        map.find(num[i]);
    for (Base::index_t i = 0; i < 100000; i++)
        map.find(num[i]);
#endif
}
TEST(RadixTreeTest, MapPerformance)
{
    const size_t N = 100000;
    Base::index_t *num = new Base::index_t[N];
    srand(333);
    for (Base::index_t i = 0; i < N; i++)
        num[i] = rand() % 1000000;
    std::map<Base::index_t, Base::index_t> map;
    for (Base::index_t i = 0; i < N; i++)
        map[num[i]] = i;
#if 1
    for (Base::index_t i = 0; i < N; i++)
        map[num[i]] = i;
    for (Base::index_t i = 0; i < N; i++)
        map[num[i]] = i;
    for (Base::index_t i = 0; i < N; i++)
        map[num[i]] = i;
    for (Base::index_t i = 0; i < N; i++)
        map[num[i]] = i;
#endif
    for (Base::index_t i = 0; i < 100000; i++)
        map.find(num[i]);
#if 1
    for (Base::index_t i = 0; i < 100000; i++)
        map.find(num[i]);
    for (Base::index_t i = 0; i < 100000; i++)
        map.find(num[i]);
    for (Base::index_t i = 0; i < 100000; i++)
        map.find(num[i]);
    for (Base::index_t i = 0; i < 100000; i++)
        map.find(num[i]);
#endif
}
TEST(RadixTreeTest, RadixTreePerformance)
{
    const size_t N = 100000;
    Base::index_t *num = new Base::index_t[N];
    srand(333);
    for (Base::index_t i = 0; i < N; i++)
        num[i] = rand() % 1000000;
    RadixTree *map = new RadixTree;
    for (Base::index_t i = 0; i < 100000; i++)
        map->setValue(num[i], i);
#if 1
    for (Base::index_t i = 0; i < 100000; i++)
        map->setValue(num[i], i);
    for (Base::index_t i = 0; i < 100000; i++)
        map->setValue(num[i], i);
    for (Base::index_t i = 0; i < 100000; i++)
        map->setValue(num[i], i);
    for (Base::index_t i = 0; i < 100000; i++)
        map->setValue(num[i], i);
#endif
    for (Base::index_t i = 0; i < 100000; i++)
    {
        Base::index_t j;
        map->getValue(num[i], j);
    }
#if 1
    for (Base::index_t i = 0; i < 100000; i++)
    {
        Base::index_t j;
        map->getValue(num[i], j);
    }
    for (Base::index_t i = 0; i < 100000; i++)
    {
        Base::index_t j;
        map->getValue(num[i], j);
    }
    for (Base::index_t i = 0; i < 100000; i++)
    {
        Base::index_t j;
        map->getValue(num[i], j);
    }
    for (Base::index_t i = 0; i < 100000; i++)
    {
        Base::index_t j;
        map->getValue(num[i], j);
    }
#endif
    map->release();
}
#endif
