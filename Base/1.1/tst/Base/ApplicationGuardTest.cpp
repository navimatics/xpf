/*
 * Base/ApplicationGuardTest.cpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#include <Base/ApplicationGuard.hpp>
#include <gtest/gtest.h>

TEST(ApplicationGuardTest, PrimaryInstance)
{
    char path[L_tmpnam]; tmpnam(path);
    FILE *file1, *file2;
    ASSERT_TRUE(Base::ApplicationGuard(path, &file1));
    ASSERT_TRUE(Base::ApplicationGuard(path, &file2));
    ASSERT_EQ(file1, file2);
    ASSERT_NE((void *)0, file2);
}
#if 0
TEST(ApplicationGuardTest, SecondaryInstance)
{
    /* ??? */
}
#endif
