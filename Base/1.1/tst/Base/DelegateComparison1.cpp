/*
 * Base/DelegateComparison1.cpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#include <Base/Delegate.hpp>
#include <Base/Object.hpp>
#include <gtest/gtest.h>

Base::Delegate<size_t ()> getDelegateComparison1Delegate(Base::Object *obj)
{
    return make_delegate(&Base::Object::hash, obj);
}
