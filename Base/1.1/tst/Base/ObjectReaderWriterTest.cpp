/*
 * Base/ObjectReaderWriterTest.cpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#include <Base/ObjectReader.hpp>
#include <Base/ObjectWriter.hpp>
#include <Base/ObjectCodec.hpp>
#include <Base/CString.hpp>
#include <gtest/gtest.h>

class RWTestObject : public Base::Object
{
public:
    RWTestObject();
    ~RWTestObject();
    RWTestObject *obj();
    void setObj(RWTestObject *value);
    const char *str();
    void setStr(const char *value);
private:
    RWTestObject *_obj;
    const char *_str;
    bool B;
    long l;
    double d;
    ObjectCodecDeclare();
};
RWTestObject *defaultObject = (RWTestObject *)(new RWTestObject)->linger();
RWTestObject::RWTestObject()
{
    _obj = defaultObject;
    _str = Base::cstr_new("<DEFAULT-STRING>");
    B = true;
    l = Base::None();
    d = Base::None();
}
RWTestObject::~RWTestObject()
{
    if (0 != _obj)
        _obj->release();
    if (0 != _str)
        Base::cstr_obj(_str)->release();
}
RWTestObject *RWTestObject::obj()
{
    return _obj;
}
void RWTestObject::setObj(RWTestObject *value)
{
    if (0 != value)
        value->retain();
    if (0 != _obj)
        _obj->release();
    _obj = value;
}
const char *RWTestObject::str()
{
    return _str;
}
void RWTestObject::setStr(const char *value)
{
    if (0 != value)
        value = Base::cstr_new(value);
    if (0 != _str)
        Base::cstr_obj(_str)->release();
    _str = value;
}
ObjectCodecBegin(RWTestObject)
    ObjectCodecProperty("obj", &RWTestObject::obj, &RWTestObject::setObj, defaultObject)
    ObjectCodecProperty("str", &RWTestObject::str, &RWTestObject::setStr, "<DEFAULT-STRING>")
    ObjectCodecProperty("B", &RWTestObject::B, true)
    ObjectCodecProperty("l", &RWTestObject::l, Base::None())
    ObjectCodecProperty("d", &RWTestObject::d, Base::None())
ObjectCodecEnd(RWTestObject)

class DerivedRWTestObject : public RWTestObject
{
public:
    DerivedRWTestObject();
private:
    long anotherLong;
    ObjectCodecDeclare();
};
DerivedRWTestObject::DerivedRWTestObject()
{
    anotherLong = Base::None();
}
ObjectCodecBeginEx(DerivedRWTestObject, RWTestObject)
    ObjectCodecProperty("Al", &DerivedRWTestObject::anotherLong, Base::None())
ObjectCodecEnd(DerivedRWTestObject)


class TestObjectReader : public Base::ObjectReader
{
public:
    TestObjectReader(const char **chunks, size_t length);
    void reset();
protected:
    ssize_t readBuffer(void *buf, size_t size);
private:
    const char **_chunks;
    size_t _clength;
    size_t _cindex;
};
TestObjectReader::TestObjectReader(const char **chunks, size_t length)
{
    _chunks = chunks;
    _clength = length;
}
void TestObjectReader::reset()
{
    _cindex = 0;
    Base::ObjectReader::reset();
}
ssize_t TestObjectReader::readBuffer(void *buf, size_t size)
{
    if (_clength <= _cindex)
        return 0;
    size_t len = strlen(_chunks[_cindex]);
    assert(size > len);
    memcpy(buf, _chunks[_cindex], len);
    _cindex++;
    return len;
}

class TestObjectWriter : public Base::ObjectWriter
{
public:
    TestObjectWriter(bool pretty = false);
    ~TestObjectWriter();
    const char *chars();
protected:
    ssize_t writeBuffer(const void *buf, size_t size);
private:
    const char *_text;
};
TestObjectWriter::TestObjectWriter(bool pretty) : ObjectWriter(0, pretty)
{
    _text = Base::cstr_new(0);
}
TestObjectWriter::~TestObjectWriter()
{
    Base::cstr_obj(_text)->release();
}
const char *TestObjectWriter::chars()
{
    return _text;
}
ssize_t TestObjectWriter::writeBuffer(const void *buf, size_t size)
{
    _text = Base::cstr_obj(_text)->_concat((const char *)buf, size)->chars();
    return size;
}

static const char *JoinChunks(const char **chunks, size_t length)
{
    const char *s = Base::cstr_new(0);
    for (size_t i = 0; length > i; i++)
        s = Base::cstr_obj(s)->_concat(chunks[i])->chars();
    Base::cstr_autorelease(s);
    return s;
}

static const char *chunks[] =
{
    "{",
    "\"A\":[1,{},{\"A\":[],\"k1\":\"v1\",\"k2\":\"v2\"}],",
    "\"B\":true,",
    "\"d\":42.5,",
    "\"l\":42,",
    "\"n\":null,",
    "\"~empty\":{},",
    "\"~obj\":{\"obj2\":{\"k\":\"v\"}},",
    "\"~s","tr\":\"a","bc\",",
    "\"~~\":[",
    "{\"\\//t\":\"RWTestObject\",\"\\","//i\":\"\\/","/i0\",\"B\":false,\"d\":0.0,\"l\":0,\"obj\":null,\"str\":null},"
    "{\"\\//t\":\"RWTestObject\",\"\\//i\":\"\\//i1\",\"d\":42.5,\"l\":42,\"obj\":\"\\//i0\",\"str\":\"fourtytwo\"},"
    "{\"\\//t\":\"RWTestObject\",\"\\//i\":\"\\//i2\",\"B\":false,\"d\":2.0,\"l\":2,\"obj\":"
        "{\"\\//t\":\"RWTestObject\",\"\\//i\":\"\\//i3\",\"B\":false,\"d\":3.0,\"l\":3,\"obj\":null,\"str\":\"three\"}"
    ",\"str\":\"two\"},"
    "{\"\\//t\":\"RWTestObject\",\"\\//i\":\"\\//i4\",\"B\":false,\"d\":4.0,\"l\":4,\"obj\":\"\\//i3\",\"str\":\"four\"}"
    "]"
    "}"
};
TEST(ObjectReaderWriterTest, ReadWrite)
{
    Base::Object::Mark();
    TestObjectReader *reader = new (Base::collect) TestObjectReader(chunks, NELEMS(chunks));
    TestObjectWriter *writer = new (Base::collect) TestObjectWriter;
    for (;;)
    {
        Base::Object *obj = reader->readObject();
        if (0 == obj)
            break;
        writer->writeObject(obj);
    }
    const char *s = JoinChunks(chunks, NELEMS(chunks));
    ASSERT_STREQ(s, writer->chars());
    Base::Object::Collect();
}
TEST(ObjectReaderWriterTest, ReadWriteReset)
{
    Base::Object::Mark();
    TestObjectReader *reader = new (Base::collect) TestObjectReader(chunks, NELEMS(chunks));
    TestObjectWriter *writer = new (Base::collect) TestObjectWriter;
    for (;;)
    {
        Base::Object *obj = reader->readObject();
        if (0 == obj)
            break;
        writer->writeObject(obj);
    }
    const char *s = JoinChunks(chunks, NELEMS(chunks));
    ASSERT_STREQ(s, writer->chars());
    reader->reset();
    writer = new (Base::collect) TestObjectWriter;
    for (;;)
    {
        Base::Object *obj = reader->readObject();
        if (0 == obj)
            break;
        writer->writeObject(obj);
    }
    s = JoinChunks(chunks, NELEMS(chunks));
    ASSERT_STREQ(s, writer->chars());
    Base::Object::Collect();
}

static const char *derivedChunks[] =
{
    "{",
    "\"A\":[1,{},{\"A\":[],\"k1\":\"v1\",\"k2\":\"v2\"}],",
    "\"B\":true,",
    "\"d\":42.5,",
    "\"l\":42,",
    "\"n\":null,",
    "\"~empty\":{},",
    "\"~obj\":{\"obj2\":{\"k\":\"v\"}},",
    "\"~s","tr\":\"a","bc\",",
    "\"~~\":[",
    "{\"\\//t\":\"DerivedRWTestObject\",\"\\","//i\":\"\\/","/i0\",\"Al\":0,\"B\":false,\"d\":0.0,\"l\":0,\"obj\":null,\"str\":null},"
    "{\"\\//t\":\"DerivedRWTestObject\",\"\\//i\":\"\\//i1\",\"Al\":1,\"d\":42.5,\"l\":42,\"obj\":\"\\//i0\",\"str\":\"fourtytwo\"},"
    "{\"\\//t\":\"DerivedRWTestObject\",\"\\//i\":\"\\//i2\",\"Al\":2,\"B\":false,\"d\":2.0,\"l\":2,\"obj\":"
        "{\"\\//t\":\"DerivedRWTestObject\",\"\\//i\":\"\\//i3\",\"Al\":3,\"B\":false,\"d\":3.0,\"l\":3,\"obj\":null,\"str\":\"three\"}"
    ",\"str\":\"two\"},"
    "{\"\\//t\":\"DerivedRWTestObject\",\"\\//i\":\"\\//i4\",\"Al\":4,\"B\":false,\"d\":4.0,\"l\":4,\"obj\":\"\\//i3\",\"str\":\"four\"}"
    "]"
    "}"
};
TEST(ObjectReaderWriterTest, ReadWriteDerived)
{
    Base::Object::Mark();
    TestObjectReader *reader = new (Base::collect) TestObjectReader(derivedChunks, NELEMS(derivedChunks));
    TestObjectWriter *writer = new (Base::collect) TestObjectWriter;
    for (;;)
    {
        Base::Object *obj = reader->readObject();
        if (0 == obj)
            break;
        writer->writeObject(obj);
    }
    const char *s = JoinChunks(derivedChunks, NELEMS(derivedChunks));
    ASSERT_STREQ(s, writer->chars());
    Base::Object::Collect();
}
