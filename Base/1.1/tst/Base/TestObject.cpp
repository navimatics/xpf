/*
 * Base/TestObject.cpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#include "TestObject.hpp"

int TestObject::count = 0;
TestObject::TestObject()
{
    objid = count++;
}
TestObject::~TestObject()
{
    objid = 0;
    count--;
}
int TestObject::getObjid()
{
    return objid;
}
