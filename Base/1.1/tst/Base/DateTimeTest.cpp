/*
 * Base/DateTimeTest.cpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#include <Base/DateTime.hpp>
#include <gtest/gtest.h>

TEST(DateTimeTest, JulianDate)
{
    Base::Object::Mark();
    struct
    {
        int y, m, d, jd, wd;
    } dates[] =
    {
        { -4713,  1,  1,       0, Base::DateTime::Monday    },
        {  -480,  8,  7, 1546322, Base::DateTime::Tuesday   },
        {    -1, 12, 31, 1721423, Base::DateTime::Friday    },
        {    +1,  1,  1, 1721424, Base::DateTime::Saturday  },
        { +1001,  1,  1, 2086674, Base::DateTime::Wednesday },
        { +1582, 10,  4, 2299160, Base::DateTime::Thursday  },
        { +1582, 10, 15, 2299161, Base::DateTime::Friday    },
        { +2001,  1,  1, 2451911, Base::DateTime::Monday    },
    };
    struct
    {
        int h, m, s;
    } times[] =
    {
        {  0,  0,  0 },
        {  0,  0,  1 },
        {  5, 42, 42 },
        { 10, 42, 42 },
        { 15, 42, 42 },
        { 20, 42, 42 },
        { 23, 59, 59 },
    };
    for (size_t j = 0; NELEMS(dates) > j; j++)
        for (size_t i = 0; NELEMS(times) > i; i++)
        {
            Base::DateTime *dt;
            Base::DateTime::Components cs;
            int jd;
            int y = dates[j].y, n = dates[j].m, d = dates[j].d;
            int h = times[i].h, m = times[i].m, s = times[i].s;
            dt = new (Base::collect) Base::DateTime(y, n, d, h, m, s);
            jd = dt->julianDate() + 0.5;
            cs = dt->components();
            ASSERT_EQ(dates[j].jd, jd);
            ASSERT_EQ(y, cs.year);
            ASSERT_EQ(n, cs.month);
            ASSERT_EQ(d, cs.day);
            ASSERT_EQ(h, cs.hour);
            ASSERT_EQ(m, cs.min);
            ASSERT_EQ(s, cs.sec);
            ASSERT_EQ(0, cs.msec);
            ASSERT_EQ(dates[j].wd, cs.dayOfWeek);
            dt = new (Base::collect) Base::DateTime(dt->julianDate());
            jd = dt->julianDate() + 0.5;
            cs = dt->components();
            ASSERT_EQ(dates[j].jd, jd);
            ASSERT_EQ(y, cs.year);
            ASSERT_EQ(n, cs.month);
            ASSERT_EQ(d, cs.day);
            ASSERT_EQ(h, cs.hour);
            ASSERT_EQ(m, cs.min);
            ASSERT_EQ(s, cs.sec);
            //ASSERT_EQ(0, cs.msec);
            ASSERT_EQ(dates[j].wd, cs.dayOfWeek);
            cs.msec = 421;
            dt = new (Base::collect) Base::DateTime(cs);
            jd = dt->julianDate() + 0.5;
            cs = dt->components();
            ASSERT_EQ(dates[j].jd, jd);
            ASSERT_EQ(y, cs.year);
            ASSERT_EQ(n, cs.month);
            ASSERT_EQ(d, cs.day);
            ASSERT_EQ(h, cs.hour);
            ASSERT_EQ(m, cs.min);
            ASSERT_EQ(s, cs.sec);
            ASSERT_EQ(421, cs.msec);
            ASSERT_EQ(dates[j].wd, cs.dayOfWeek);
        }
    for (size_t j = 0; NELEMS(dates) > j; j++)
        for (size_t i = 0; NELEMS(times) > i; i++)
        {
            Base::DateTime::Components cs;
            double JD;
            int jd;
            int y = dates[j].y, n = dates[j].m, d = dates[j].d;
            int h = times[i].h, m = times[i].m, s = times[i].s;
            cs.year = y; cs.month = n; cs.day = d;
            cs.hour = h; cs.min = m; cs.sec = s; cs.msec = 0;
            JD = Base::DateTime::julianDateFromComponents(cs);
            cs = Base::DateTime::componentsFromJulianDate(JD);
            jd = JD + 0.5;
            ASSERT_EQ(dates[j].jd, jd);
            ASSERT_EQ(y, cs.year);
            ASSERT_EQ(n, cs.month);
            ASSERT_EQ(d, cs.day);
            ASSERT_EQ(h, cs.hour);
            ASSERT_EQ(m, cs.min);
            ASSERT_EQ(s, cs.sec);
            ASSERT_EQ(0, cs.msec);
            ASSERT_EQ(dates[j].wd, cs.dayOfWeek);
            cs.msec = 421;
            JD = Base::DateTime::julianDateFromComponents(cs);
            cs = Base::DateTime::componentsFromJulianDate(JD);
            jd = JD + 0.5;
            ASSERT_EQ(dates[j].jd, jd);
            ASSERT_EQ(y, cs.year);
            ASSERT_EQ(n, cs.month);
            ASSERT_EQ(d, cs.day);
            ASSERT_EQ(h, cs.hour);
            ASSERT_EQ(m, cs.min);
            ASSERT_EQ(s, cs.sec);
            ASSERT_EQ(421, cs.msec);
            ASSERT_EQ(dates[j].wd, cs.dayOfWeek);
        }
    Base::Object::Collect();
}
TEST(DateTimeTest, UnixTime)
{
    Base::Object::Mark();
    struct
    {
        int y, m, d;
        time_t ut;
    } dates[] =
    {
        { +1970,  1,  1,          0 },
        { +1971,  1,  1,   31536000 },
        { +2001,  1,  1,  978307200 },
        { +2010,  7, 21, 1279670400 },
        { +2030,  1,  1, 1893456000 },
    };
    struct
    {
        int h, m, s;
    } times[] =
    {
        {  0,  0,  0 },
        {  0,  0,  1 },
        {  5, 42, 42 },
        { 10, 42, 42 },
        { 15, 42, 42 },
        { 20, 42, 42 },
        { 23, 59, 59 },
    };
    for (size_t j = 0; NELEMS(dates) > j; j++)
        for (size_t i = 0; NELEMS(times) > i; i++)
        {
            Base::DateTime *dt;
            Base::DateTime::Components cs;
            time_t ut;
            int y = dates[j].y, n = dates[j].m, d = dates[j].d;
            int h = times[i].h, m = times[i].m, s = times[i].s;
            dt = new (Base::collect) Base::DateTime(y, n, d, h, m, s);
            ut = dt->unixTime();
            ASSERT_EQ(dates[j].ut + h * 3600 + m * 60 + s, ut);
            dt = new (Base::collect) Base::DateTime(ut);
            cs = dt->components();
            ASSERT_EQ(y, cs.year);
            ASSERT_EQ(n, cs.month);
            ASSERT_EQ(d, cs.day);
            ASSERT_EQ(h, cs.hour);
            ASSERT_EQ(m, cs.min);
            ASSERT_EQ(s, cs.sec);
            ASSERT_EQ(0, cs.msec);
        }
    Base::Object::Collect();
}
TEST(DateTimeTest, CurrentTime)
{
    Base::Object::Mark();
    Base::DateTime *dt;
    Base::DateTime::Components cs;
    time_t t0, ut;
    double jd;
    t0 = time(0);
    dt = new (Base::collect) Base::DateTime();
    jd = Base::DateTime::currentJulianDate();
    cs = dt->components();
    ut = dt->unixTime();
    ASSERT_TRUE(1 >= abs((int)(ut - t0)));
    ASSERT_TRUE(1 >= abs((int)(ut - (time_t)(((jd - 2440587.5) * 86400) + 0.5))));
    struct tm *tm = gmtime(&ut);
    ASSERT_EQ(tm->tm_year + 1900, cs.year);
    ASSERT_EQ(tm->tm_mon + 1, cs.month);
    ASSERT_EQ(tm->tm_mday, cs.day);
    ASSERT_EQ(tm->tm_hour, cs.hour);
    ASSERT_EQ(tm->tm_min, cs.min);
    ASSERT_EQ(tm->tm_sec, cs.sec);
    Base::Object::Collect();
}
TEST(DateTimeTest, ISO8601)
{
    mark_and_collect
    {
        struct
        {
            const char *str;
            Base::DateTime::Components components;
        } tests[] =
        {
            { "19850412",               { 1985,  4, 12,  0,  0,  0, 0, Base::DateTime::Friday } },
            { "1985-04-12",             { 1985,  4, 12,  0,  0,  0, 0, Base::DateTime::Friday } },
            { "1985102",                { 1985,  4, 12,  0,  0,  0, 0, Base::DateTime::Friday } },
            { "1985-102",               { 1985,  4, 12,  0,  0,  0, 0, Base::DateTime::Friday } },
            { "1985W155",               { 1985,  4, 12,  0,  0,  0, 0, Base::DateTime::Friday } },
            { "1985-W15-5",             { 1985,  4, 12,  0,  0,  0, 0, Base::DateTime::Friday } },
            { "1985W15",                { 1985,  4,  8,  0,  0,  0, 0, Base::DateTime::Monday } },
            { "1985-W15",               { 1985,  4,  8,  0,  0,  0, 0, Base::DateTime::Monday } },
            { "198504",                 { 1985,  4,  1,  0,  0,  0, 0, Base::DateTime::Monday } },
            { "1985-04",                { 1985,  4,  1,  0,  0,  0, 0, Base::DateTime::Monday } },
            { "1985",                   { 1985,  1,  1,  0,  0,  0, 0, Base::DateTime::Tuesday } },
            { "19",                     { 1900,  1,  1,  0,  0,  0, 0, Base::DateTime::Monday } },

            { "+1985-04-12",            { 1985,  4, 12,  0,  0,  0, 0, Base::DateTime::Friday } },
            { "-1985-04-12",            {-1985,  4, 12,  0,  0,  0, 0, Base::DateTime::Saturday } },

            { "19850412T101530",        { 1985,  4, 12,  10, 15, 30, 0, Base::DateTime::Friday } },
            { "1985-04-12T10:15:30",    { 1985,  4, 12,  10, 15, 30, 0, Base::DateTime::Friday } },
            { "1985102T235030Z",        { 1985,  4, 12,  23, 50, 30, 0, Base::DateTime::Friday } },
            { "1985-102T23:50:30Z",     { 1985,  4, 12,  23, 50, 30, 0, Base::DateTime::Friday } },
            { "1985W155T235030",        { 1985,  4, 12,  23, 50, 30, 0, Base::DateTime::Friday } },
            { "1985-W15-5T23:50:30",    { 1985,  4, 12,  23, 50, 30, 0, Base::DateTime::Friday } },

            { "19850412T152746",        { 1985,  4, 12,  15, 27, 46, 0, Base::DateTime::Friday } },
            { "1985-04-12T15:27:46",    { 1985,  4, 12,  15, 27, 46, 0, Base::DateTime::Friday } },
            { "19850412T1528",          { 1985,  4, 12,  15, 28,  0, 0, Base::DateTime::Friday } },
            { "1985-04-12T15:28",       { 1985,  4, 12,  15, 28,  0, 0, Base::DateTime::Friday } },
            { "19850412T15",            { 1985,  4, 12,  15,  0,  0, 0, Base::DateTime::Friday } },
            { "1985-04-12T15",          { 1985,  4, 12,  15,  0,  0, 0, Base::DateTime::Friday } },

            { "19850412T152735,5",      { 1985,  4, 12,  15, 27, 35, 500, Base::DateTime::Friday } },
            { "1985-04-12T15:27:35,5",  { 1985,  4, 12,  15, 27, 35, 500, Base::DateTime::Friday } },
            { "19850412T1527,5",        { 1985,  4, 12,  15, 27, 30, 0, Base::DateTime::Friday } },
            { "1985-04-12T15:27,5",     { 1985,  4, 12,  15, 27, 30, 0, Base::DateTime::Friday } },
            { "19850412T15,5",          { 1985,  4, 12,  15, 30,  0, 0, Base::DateTime::Friday } },
            { "1985-04-12T15,5",        { 1985,  4, 12,  15, 30,  0, 0, Base::DateTime::Friday } },

            { "19850412T152746+0100",       { 1985,  4, 12,  14, 27, 46, 0, Base::DateTime::Friday } },
            { "1985-04-12T15:27:46+01:00",  { 1985,  4, 12,  14, 27, 46, 0, Base::DateTime::Friday } },
            { "19850412T152746+01",         { 1985,  4, 12,  14, 27, 46, 0, Base::DateTime::Friday } },
            { "1985-04-12T15:27:46+01",     { 1985,  4, 12,  14, 27, 46, 0, Base::DateTime::Friday } },
            { "19850412T152746-0500",       { 1985,  4, 12,  20, 27, 46, 0, Base::DateTime::Friday } },
            { "1985-04-12T15:27:46-05:00",  { 1985,  4, 12,  20, 27, 46, 0, Base::DateTime::Friday } },
            { "19850412T152746-05",         { 1985,  4, 12,  20, 27, 46, 0, Base::DateTime::Friday } },
            { "1985-04-12T15:27:46-05",     { 1985,  4, 12,  20, 27, 46, 0, Base::DateTime::Friday } },

            { "2012-07-18T04:29:24.5+05:30",{ 2012,  7, 17,  22, 59, 24, 500, Base::DateTime::Tuesday } },
        };
        for (size_t i = 0; NELEMS(tests) > i; i++)
        {
            const char *endp;
            Base::DateTime::Components components = Base::DateTime::parse(tests[i].str, &endp);
            ASSERT_EQ('\0', *endp);
            ASSERT_EQ(tests[i].components.year, components.year);
            ASSERT_EQ(tests[i].components.month, components.month);
            ASSERT_EQ(tests[i].components.day, components.day);
            ASSERT_EQ(tests[i].components.hour, components.hour);
            ASSERT_EQ(tests[i].components.min, components.min);
            ASSERT_EQ(tests[i].components.sec, components.sec);
            ASSERT_EQ(tests[i].components.msec, components.msec);
            ASSERT_EQ(tests[i].components.dayOfWeek, components.dayOfWeek);
            Base::DateTime *dt = new Base::DateTime(components);
            ASSERT_STREQ(dt->strrepr(), Base::DateTime::format(components, false));
            dt->release();
            const char *format = Base::DateTime::format(components, false);
            components = Base::DateTime::parse(format, &endp);
            ASSERT_EQ('\0', *endp);
            ASSERT_EQ(tests[i].components.year, components.year);
            ASSERT_EQ(tests[i].components.month, components.month);
            ASSERT_EQ(tests[i].components.day, components.day);
            ASSERT_EQ(tests[i].components.hour, components.hour);
            ASSERT_EQ(tests[i].components.min, components.min);
            ASSERT_EQ(tests[i].components.sec, components.sec);
            ASSERT_EQ(tests[i].components.msec, components.msec);
            ASSERT_EQ(tests[i].components.dayOfWeek, components.dayOfWeek);
            format = Base::DateTime::format(components, true);
            components = Base::DateTime::parse(format, &endp);
            ASSERT_EQ('\0', *endp);
            ASSERT_EQ(tests[i].components.year, components.year);
            ASSERT_EQ(tests[i].components.month, components.month);
            ASSERT_EQ(tests[i].components.day, components.day);
            ASSERT_EQ(tests[i].components.hour, components.hour);
            ASSERT_EQ(tests[i].components.min, components.min);
            ASSERT_EQ(tests[i].components.sec, components.sec);
            ASSERT_EQ(tests[i].components.msec, components.msec);
            ASSERT_EQ(tests[i].components.dayOfWeek, components.dayOfWeek);
        }
    }
}
TEST(DateTimeTest, ExtractISO8601)
{
    mark_and_collect
    {
        struct
        {
            const char *endp;
            const char *str;
            Base::DateTime::Components components;
        } tests[] =
        {
            { "TEST",   "  TEST",                             { Base::None(),  1,  1,   0,  0,  0, 0, Base::DateTime::Sunday } },
            { "2TEST",  "  2TEST",                            { Base::None(),  1,  1,   0,  0,  0, 0, Base::DateTime::Sunday } },
            { "TEST",   "  20TEST",                           { 2000,  1,  1,   0,  0,  0, 0, Base::DateTime::Saturday } },
            { "201TEST","  201TEST",                          { Base::None(),  1,  1,   0,  0,  0, 0, Base::DateTime::Sunday } },
            { "TEST",   "  2012TEST",                         { 2012,  1,  1,   0,  0,  0, 0, Base::DateTime::Sunday } },
            { "0TEST",  "  20120TEST",                        { 2012,  1,  1,   0,  0,  0, 0, Base::DateTime::Sunday } },
            { "WTEST",  "  2012WTEST",                        { 2012,  1,  1,   0,  0,  0, 0, Base::DateTime::Sunday } },
            { "TEST",   "  201207TEST",                       { 2012,  7,  1,   0,  0,  0, 0, Base::DateTime::Sunday } },
            { "TEST",   "  2012-07TEST",                      { 2012,  7,  1,   0,  0,  0, 0, Base::DateTime::Sunday } },
            { "TEST",   "  20120718TEST",                     { 2012,  7, 18,   0,  0,  0, 0, Base::DateTime::Wednesday } },
            { "1TEST",  "  2012-07-1TEST",                    { 2012,  7,  1,   0,  0,  0, 0, Base::DateTime::Sunday } },
            { "TEST",   "  2012-07-18TEST",                   { 2012,  7, 18,   0,  0,  0, 0, Base::DateTime::Wednesday } },
            { "TEST",   "  20120718T04TEST",                  { 2012,  7, 18,   4,  0,  0, 0, Base::DateTime::Wednesday } },
            { "TEST",   "  2012-07-18T04TEST",                { 2012,  7, 18,   4,  0,  0, 0, Base::DateTime::Wednesday } },
            { "2TEST",  "  20120718T042TEST",                 { 2012,  7, 18,   4,  0,  0, 0, Base::DateTime::Wednesday } },
            { "TEST",   "  20120718T0429TEST",                { 2012,  7, 18,   4, 29,  0, 0, Base::DateTime::Wednesday } },
            { "TEST",   "  2012-07-18T04:29TEST",             { 2012,  7, 18,   4, 29,  0, 0, Base::DateTime::Wednesday } },
            { "2TEST",  "  20120718T04292TEST",               { 2012,  7, 18,   4, 29,  0, 0, Base::DateTime::Wednesday } },
            { "TEST",   "  20120718T042924TEST",              { 2012,  7, 18,   4, 29, 24, 0, Base::DateTime::Wednesday } },
            { "TEST",   "  2012-07-18T04:29:24TEST",          { 2012,  7, 18,   4, 29, 24, 0, Base::DateTime::Wednesday } },
            { ".TEST",  "  20120718T042924.TEST",             { 2012,  7, 18,   4, 29, 24, 0, Base::DateTime::Wednesday } },
            { "TEST",   "  20120718T042924.5TEST",            { 2012,  7, 18,   4, 29, 24, 500, Base::DateTime::Wednesday } },
            { "TEST",   "  2012-07-18T04:29:24.5TEST",        { 2012,  7, 18,   4, 29, 24, 500, Base::DateTime::Wednesday } },
            { "+TEST",  "  20120718T042924.5+TEST",           { 2012,  7, 18,   4, 29, 24, 500, Base::DateTime::Wednesday } },
            { "+0TEST", "  20120718T042924.5+0TEST",          { 2012,  7, 18,   4, 29, 24, 500, Base::DateTime::Wednesday } },
            { "TEST",   "  20120718T042924.5+05TEST",         { 2012,  7, 17,  23, 29, 24, 500, Base::DateTime::Tuesday } },
            { "TEST",   "  2012-07-18T04:29:24.5+05TEST",     { 2012,  7, 17,  23, 29, 24, 500, Base::DateTime::Tuesday } },
            { "3TEST",  "  20120718T042924.5+053TEST",        { 2012,  7, 17,  23, 29, 24, 500, Base::DateTime::Tuesday } },
            { "TEST",   "  20120718T042924.5+0530TEST",       { 2012,  7, 17,  22, 59, 24, 500, Base::DateTime::Tuesday } },
            { "TEST",   "  2012-07-18T04:29:24.5+05:30TEST",  { 2012,  7, 17,  22, 59, 24, 500, Base::DateTime::Tuesday } },
        };
        for (size_t i = 0; NELEMS(tests) > i; i++)
        {
            const char *endp;
            Base::DateTime::Components components = Base::DateTime::parse(tests[i].str, &endp);
            ASSERT_STREQ(tests[i].endp, endp);
            ASSERT_EQ(tests[i].components.year, components.year);
            ASSERT_EQ(tests[i].components.month, components.month);
            ASSERT_EQ(tests[i].components.day, components.day);
            ASSERT_EQ(tests[i].components.hour, components.hour);
            ASSERT_EQ(tests[i].components.min, components.min);
            ASSERT_EQ(tests[i].components.sec, components.sec);
            ASSERT_EQ(tests[i].components.msec, components.msec);
            ASSERT_EQ(tests[i].components.dayOfWeek, components.dayOfWeek);
        }
    }
}
