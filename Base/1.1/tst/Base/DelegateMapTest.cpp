/*
 * Base/DelegateMapTest.cpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#include <Base/DelegateMap.hpp>
#include <Base/CArray.hpp>
#include <gtest/gtest.h>
#include "TestObject.hpp"

typedef Base::Delegate<int ()> TestDelegate;
TEST(GenericDelegateMapTest, AddRemoveAll)
{
    mark_and_collect
    {
        Base::GenericDelegateMap<TestObject, int ()> *map = new (Base::collect) Base::GenericDelegateMap<TestObject, int ()>;
        for (size_t i = 0; i < 100; i++)
        {
            TestObject *o = new (Base::collect) TestObject;
            map->setDelegate(o, make_delegate(&TestObject::getObjid, o));
        }
        ASSERT_EQ((size_t)100, map->count());
        map->removeAllDelegates();
        ASSERT_EQ((size_t)0, map->count());
        for (size_t i = 0; i < 100; i++)
        {
            TestObject *o = new (Base::collect) TestObject;
            map->setDelegate(o, make_delegate(&TestObject::getObjid, o));
        }
        ASSERT_EQ((size_t)100, map->count());
    }
}
TEST(GenericDelegateMapTest, DelegateAndGetDelegate)
{
    mark_and_collect
    {
        Base::GenericDelegateMap<TestObject, int ()> *map = new (Base::collect) Base::GenericDelegateMap<TestObject, int ()>;
        for (size_t i = 0; i < 100; i++)
        {
            TestObject *o = new (Base::collect) TestObject;
            map->setDelegate(o, make_delegate(&TestObject::getObjid, o));
        }
        ASSERT_EQ((size_t)100, map->count());
        TestObject *o = new (Base::collect) TestObject;
        map->setDelegate(o, make_delegate(&TestObject::getObjid, o));
        ASSERT_EQ((size_t)101, map->count());
        ASSERT_TRUE(o == map->delegate(o).data);
        TestDelegate del;
        ASSERT_TRUE(map->getDelegate(o, del));
        ASSERT_TRUE(o == del.data);
    }
}
TEST(GenericDelegateMapTest, AddReplaceRemove)
{
    mark_and_collect
    {
        Base::GenericDelegateMap<TestObject, int ()> *map = new (Base::collect) Base::GenericDelegateMap<TestObject, int ()>;
        for (size_t i = 0; i < 100; i++)
        {
            TestObject *o = new (Base::collect) TestObject;
            map->setDelegate(o, make_delegate(&TestObject::getObjid, o));
        }
        ASSERT_EQ((size_t)100, map->count());
        TestObject *o = new (Base::collect) TestObject;
        map->setDelegate(o, make_delegate(&TestObject::getObjid, o));
        ASSERT_EQ((size_t)101, map->count());
        ASSERT_TRUE(o == map->delegate(o).data);
        TestObject *o2 = new (Base::collect) TestObject;
        map->setDelegate(o, make_delegate(&TestObject::getObjid, o2));
        ASSERT_EQ((size_t)101, map->count());
        ASSERT_TRUE(o2 == map->delegate(o).data);
        map->removeDelegate(o);
        ASSERT_EQ((size_t)100, map->count());
        ASSERT_TRUE(0 == map->delegate(o));
        TestDelegate del;
        ASSERT_FALSE(map->getDelegate(o, del));
        ASSERT_TRUE(0 == del.data);
    }
}
TEST(GenericDelegateMapTest, AddDelegates)
{
    mark_and_collect
    {
        TestObject *objbuf[110];
        for (size_t i = 0; i < 100; i++)
            objbuf[i] = new (Base::collect) TestObject;
        Base::GenericDelegateMap<TestObject, int ()> *map1 = new (Base::collect) Base::GenericDelegateMap<TestObject, int ()>;
        for (size_t i = 0; i < 100; i++)
        {
            TestObject *o = objbuf[i];
            map1->setDelegate(o, make_delegate(&TestObject::getObjid, o));
        }
        Base::GenericDelegateMap<TestObject, int ()> *map2 = new (Base::collect) Base::GenericDelegateMap<TestObject, int ()>;
        for (size_t i = 0; i < 10; i++)
        {
            TestObject *o = objbuf[i];
            map2->setDelegate(o, make_delegate(&TestObject::getObjid, o));
        }
        for (size_t i = 100; i < 110; i++)
        {
            TestObject *o = new (Base::collect) TestObject;
            objbuf[i] = o;
            map2->setDelegate(o, make_delegate(&TestObject::getObjid, o));
        }
        ASSERT_EQ((size_t)100, map1->count());
        ASSERT_EQ((size_t)20, map2->count());
        map1->addDelegates(map2);
        ASSERT_EQ((size_t)110, map1->count());
        for (size_t i = 0; i < 10; i++)
        {
            TestObject *o = objbuf[i];
            ASSERT_TRUE(map1->delegate(o) == map2->delegate(o));
        }
        for (size_t i = 100; i < 110; i++)
        {
            TestObject *o = objbuf[i];
            ASSERT_TRUE(map1->delegate(o) == map2->delegate(o));
        }
    }
}
TEST(GenericDelegateMapTest, Contents)
{
    mark_and_collect
    {
        Base::GenericDelegateMap<TestObject, int ()> *map = new (Base::collect) Base::GenericDelegateMap<TestObject, int ()>;
        for (size_t i = 0; i < 100; i++)
        {
            TestObject *o = new (Base::collect) TestObject;
            map->setDelegate(o, make_delegate(&TestObject::getObjid, o));
        }
        ASSERT_EQ((size_t)100, map->count());
        TestObject **keys = map->keys();
        TestDelegate *dels = map->delegates();
        ASSERT_EQ((size_t)100, carr_length(keys));
        ASSERT_EQ((size_t)100, carr_length(dels));
        for (size_t i = 0; i < 100; i++)
            ASSERT_TRUE(keys[i] == dels[i].data);
        map->getKeysAndDelegates(keys, dels);
        ASSERT_EQ((size_t)100, carr_length(keys));
        ASSERT_EQ((size_t)100, carr_length(dels));
        for (size_t i = 0; i < 100; i++)
            ASSERT_TRUE(keys[i] == dels[i].data);
    }
}
