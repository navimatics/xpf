/*
 * Base/EnvironmentTest.cpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#include <Base/Environment.hpp>
#include <Base/CString.hpp>
#include <Base/Map.hpp>
#include <gtest/gtest.h>

TEST(EnvironmentTest, Test)
{
    static Base::CString *AppName = Base::CStringConstant("EnvironmentTest/AppName");
    static Base::CString *Version = Base::CStringConstant("EnvironmentTest/Version");
    static Base::CString *NonExistant = Base::CStringConstant("EnvironmentTest/NonExistant");

    Base::Object::Mark();
    char path[L_tmpnam]; tmpnam(path);
    Base::Map *map = new Base::Map;
    map->setObject(AppName, Base::cstr_obj(Base::cstring("MyApplication")));
    map->setObject(Version, Base::cstr_obj(Base::cstring("1.0")));
    Base::Environment *env = new Base::Environment(path);
    ASSERT_EQ(0, env->object(AppName, Base::Environment::Thread));
    ASSERT_EQ(0, env->object(Version, Base::Environment::Thread));
    ASSERT_EQ(0, env->object(NonExistant, Base::Environment::Thread));
    env->addObjects(map, Base::Environment::Factory);
    map->release();
    ASSERT_EQ(0, env->object(NonExistant, Base::Environment::Thread));
    ASSERT_STREQ("MyApplication", env->object(AppName, Base::Environment::Thread)->strrepr());
    ASSERT_STREQ("1.0", env->object(Version, Base::Environment::Thread)->strrepr());
    env->setObject(AppName, Base::cstr_obj(Base::cstring("Factory#MyApplication")), Base::Environment::Factory);
    ASSERT_STREQ("Factory#MyApplication", env->object(AppName, Base::Environment::Thread)->strrepr());
    env->setObject(AppName, Base::cstr_obj(Base::cstring("Application#MyApplication")), Base::Environment::Application);
    ASSERT_STREQ("Application#MyApplication", env->object(AppName, Base::Environment::Thread)->strrepr());
    env->setObject(AppName, Base::cstr_obj(Base::cstring("Process#MyApplication")), Base::Environment::Process);
    ASSERT_STREQ("Process#MyApplication", env->object(AppName, Base::Environment::Thread)->strrepr());
    env->setObject(AppName, Base::cstr_obj(Base::cstring("Thread#MyApplication")), Base::Environment::Thread);
    ASSERT_STREQ("Thread#MyApplication", env->object(AppName, Base::Environment::Thread)->strrepr());
    env->removeObject(AppName, Base::Environment::Thread);
    ASSERT_STREQ("Process#MyApplication", env->object(AppName, Base::Environment::Thread)->strrepr());
    env->removeObject(AppName, Base::Environment::Process);
    ASSERT_STREQ("Application#MyApplication", env->object(AppName, Base::Environment::Thread)->strrepr());
    env->removeObject(AppName, Base::Environment::Application);
    ASSERT_STREQ("Factory#MyApplication", env->object(AppName, Base::Environment::Thread)->strrepr());
    env->removeObject(AppName, Base::Environment::Factory);
    ASSERT_EQ(0, env->object(AppName, Base::Environment::Thread));
    env->setObject(AppName, Base::cstr_obj(Base::cstring("MyChangedApplication")),
        Base::Environment::Application);
    ASSERT_STREQ("MyChangedApplication", env->object(AppName, Base::Environment::Thread)->strrepr());
    env->commit();
    env->release();
    env = new Base::Environment(path);
    ASSERT_STREQ("MyChangedApplication", env->object(AppName, Base::Environment::Thread)->strrepr());
    ASSERT_EQ(0, env->object(Version, Base::Environment::Thread));
    env->release();
    char path2[L_tmpnam]; tmpnam(path2);
    env = new Base::Environment(path2);
    env->addObjects(path, Base::Environment::Thread);
    ASSERT_EQ(0, env->object(AppName, Base::Environment::Application));
    ASSERT_EQ(0, env->object(Version, Base::Environment::Application));
    ASSERT_STREQ("MyChangedApplication", env->object(AppName, Base::Environment::Thread)->strrepr());
    env->release();
    remove(path2);
    remove(path);
    Base::Object::Collect();
}
