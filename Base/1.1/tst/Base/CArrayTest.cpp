/*
 * Base/CArrayTest.cpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#include <Base/CArray.hpp>
#include <gtest/gtest.h>

TEST(CArrayTest, carr_new)
{
    int arr[] = { 1, 2, 3, 4, 5 };
    void *carr = Base::carr_new(arr, NELEMS(arr), sizeof(int));
    ASSERT_EQ(NELEMS(arr), Base::carr_length(carr));
    ASSERT_EQ(NELEMS(arr), Base::carr_obj(carr)->length());
    ASSERT_TRUE(0 == memcmp(arr, carr, sizeof arr));
    ASSERT_TRUE(0 == memcmp(arr, Base::carr_obj(carr)->elems(), sizeof arr));
    void *carr2 = Base::carr_new(arr, Base::carr_length(carr), sizeof(int));
    ASSERT_EQ(NELEMS(arr), Base::carr_length(carr2));
    ASSERT_EQ(NELEMS(arr), Base::carr_obj(carr2)->length());
    ASSERT_TRUE(0 == memcmp(carr, carr2, sizeof arr));
    ASSERT_TRUE(0 == memcmp(Base::carr_obj(carr)->elems(), Base::carr_obj(carr2)->elems(), sizeof arr));
    Base::carr_obj(carr2)->release();
    Base::carr_obj(carr)->release();
    int zero[] = { 0, 0 };
    void *czero = Base::carr_new(0, 2, sizeof(int));
    ASSERT_TRUE(0 == memcmp(zero, Base::carr_obj(czero)->elems(), sizeof zero));
    Base::carr_obj(czero)->release();
}
TEST(CArrayTest, carray)
{
    Base::Object::Mark();
    int arr[] = { 1, 2, 3, 4, 5 };
    void *carr = Base::carray(arr, NELEMS(arr), sizeof(int));
    ASSERT_EQ(NELEMS(arr), Base::carr_length(carr));
    ASSERT_EQ(NELEMS(arr), Base::carr_obj(carr)->length());
    ASSERT_TRUE(0 == memcmp(arr, carr, sizeof arr));
    ASSERT_TRUE(0 == memcmp(arr, Base::carr_obj(carr)->elems(), sizeof arr));
    void *carr2 = Base::carray(arr, Base::carr_length(carr), sizeof(int));
    ASSERT_EQ(NELEMS(arr), Base::carr_length(carr2));
    ASSERT_EQ(NELEMS(arr), Base::carr_obj(carr2)->length());
    ASSERT_TRUE(0 == memcmp(carr, carr2, sizeof arr));
    ASSERT_TRUE(0 == memcmp(Base::carr_obj(carr)->elems(), Base::carr_obj(carr2)->elems(), sizeof arr));
    int zero[] = { 0, 0 };
    void *czero = Base::carray(0, 2, sizeof(int));
    ASSERT_TRUE(0 == memcmp(zero, Base::carr_obj(czero)->elems(), sizeof zero));
    Base::Object::Collect();
}
TEST(CArrayTest, _concat)
{
#define STRING_AS_PARAMS(s)             (void *)s "", strlen(s), sizeof(char)
    void *a;
    a = Base::carr_new(STRING_AS_PARAMS("The"));
    a = Base::carr_obj(a)->_concat(STRING_AS_PARAMS(" quick brown fox"))->elems();
    a = Base::carr_obj(a)->_concat(STRING_AS_PARAMS(" jumps"))->elems();
    a = Base::carr_obj(a)->_concat(STRING_AS_PARAMS(" over the"))->elems();
    a = Base::carr_obj(a)->_concat(STRING_AS_PARAMS(" lazy dog."))->elems();
    ASSERT_EQ(strlen("The quick brown fox jumps over the lazy dog."), Base::carr_length(a));
    ASSERT_TRUE(0 == memcmp(a, "The quick brown fox jumps over the lazy dog.", Base::carr_length(a)));
    Base::carr_release(a);
    a = Base::carr_new(0, 0, 0);
    a = Base::carr_obj(a)->_concat(STRING_AS_PARAMS("The"))->elems();
    a = Base::carr_obj(a)->_concat(STRING_AS_PARAMS(" quick brown fox"))->elems();
    a = Base::carr_obj(a)->_concat(STRING_AS_PARAMS(" jumps"))->elems();
    a = Base::carr_obj(a)->_concat(STRING_AS_PARAMS(" over the"))->elems();
    a = Base::carr_obj(a)->_concat(STRING_AS_PARAMS(" lazy dog."))->elems();
    ASSERT_EQ(strlen("The quick brown fox jumps over the lazy dog."), Base::carr_length(a));
    ASSERT_TRUE(0 == memcmp(a, "The quick brown fox jumps over the lazy dog.", Base::carr_length(a)));
    Base::carr_release(a);
#undef STRING_AS_PARAMS
}
TEST(CArrayTest, assign)
{
    int arr[] = { 1, 2, 3, 4, 5 };
    int arr2[] = { 6, 7, 8, 9 };
    void *carr = Base::carr_new(arr, NELEMS(arr), sizeof(int));
    void *carr2 = Base::carr_new(arr2, NELEMS(arr2), sizeof(int));
    Base::carr_assign(carr, carr2);
    ASSERT_EQ(NELEMS(arr2), Base::carr_length(carr2));
    ASSERT_TRUE(0 == memcmp(arr2, carr, sizeof arr2));
    Base::carr_assign_copy(carr, arr, NELEMS(arr), sizeof(int));
    ASSERT_EQ(NELEMS(arr), Base::carr_length(carr));
    ASSERT_TRUE(0 == memcmp(arr, carr, sizeof arr));
    Base::carr_assign(carr2, 0);
    Base::carr_assign_copy(carr, 0, 0, 0);
}
