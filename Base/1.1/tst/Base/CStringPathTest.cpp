/*
 * Base/CStringPathTest.cpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#include <Base/CString.hpp>
#include <gtest/gtest.h>

#if defined(BASE_CONFIG_WINDOWS) || defined(BASE_CONFIG_POSIX)
TEST(CStringPathTest, realpath)
{
    mark_and_collect
    {
        const char *current = Base::cstr_path_realpath(".");
        const char *parent = Base::cstr_path_realpath("..");
        ASSERT_TRUE(0 == memcmp(current, parent, strlen(parent)));
    }
}
TEST(CStringPathTest, normalize)
{
    mark_and_collect
    {
#if defined(BASE_CONFIG_WINDOWS)
        const char *ipaths[] =
        {
            "",
            "/",
            "///",
            "/aaa",
            "///aaa",
            "/aaa//bbb",
            "///aaa//bbb",
            "/aaa//bbb/ccc///ddd/././/.///eee",
            ".",
            "./",
            "./.",
            "./././",
            "./././aaa",
            "./././aaa///",
            "aaa//",
            "aaa/bbb///ccc/././//ddd",
            "././aaa/bbb///ccc/././//ddd",
            "/..",
            "///..",
            "..",
            "../",
            "../..",
            "aaa/..",
            "aaa/../..",
            "aaa/bbb/..",
            "aaa/bbb/../..",
            "aaa/.bbb",
            "aaa./bbb",
            "/./",
            "/.///",
            "/.",
            "aaa///./",
            "aaa///.///",
            "aaa///.",
            "//",
            "//aaa",
            "//aaa//bbb",
            "//aaa//../bbb",
            "//..",
            "//.",
            "//aaa/.",
            "//aaa/./.",
            "//aaa/./bbb",
            "a:",
            "a:.",
            "a:.\\",
            "a:a",
            "a:\\",
            "a:\\.",
            "a:\\..",
            "a:\\a",
        };
        const char *opaths[] =
        {
            ".\\",
            "\\",
            "\\",
            "\\aaa",
            "\\aaa",
            "\\aaa\\bbb",
            "\\aaa\\bbb",
            "\\aaa\\bbb\\ccc\\ddd\\eee",
            ".\\",
            ".\\",
            ".\\",
            ".\\",
            "aaa",
            "aaa\\",
            "aaa\\",
            "aaa\\bbb\\ccc\\ddd",
            "aaa\\bbb\\ccc\\ddd",
            "\\..",
            "\\..",
            "..",
            "..\\",
            "..\\..",
            "aaa\\..",
            "aaa\\..\\..",
            "aaa\\bbb\\..",
            "aaa\\bbb\\..\\..",
            "aaa\\.bbb",
            "aaa.\\bbb",
            "\\",
            "\\",
            "\\",
            "aaa\\",
            "aaa\\",
            "aaa\\",
            "\\\\",
            "\\\\aaa",
            "\\\\aaa\\bbb",
            "\\\\aaa\\..\\bbb",
            "\\\\..",
            "\\\\.",
            "\\\\aaa\\",
            "\\\\aaa\\",
            "\\\\aaa\\bbb",
            "a:",
            "a:",
            "a:",
            "a:a",
            "a:\\",
            "a:\\",
            "a:\\..",
            "a:\\a",
        };
#elif defined(BASE_CONFIG_POSIX)
        const char *ipaths[] =
        {
            "",
            "/",
            "///",
            "/aaa",
            "///aaa",
            "/aaa//bbb",
            "///aaa//bbb",
            "/aaa//bbb/ccc///ddd/././/.///eee",
            ".",
            "./",
            "./.",
            "./././",
            "./././aaa",
            "./././aaa///",
            "aaa//",
            "aaa/bbb///ccc/././//ddd",
            "././aaa/bbb///ccc/././//ddd",
            "/..",
            "///..",
            "..",
            "../",
            "../..",
            "aaa/..",
            "aaa/../..",
            "aaa/bbb/..",
            "aaa/bbb/../..",
            "aaa/.bbb",
            "aaa./bbb",
            "/./",
            "/.///",
            "/.",
            "aaa///./",
            "aaa///.///",
            "aaa///.",
            "//",
            "//aaa",
            "//aaa//bbb",
            "//aaa//../bbb",
            "//..",
            "//.",
            "//aaa/.",
            "//aaa/./.",
            "//aaa/./bbb",
        };
        const char *opaths[] =
        {
            "./",
            "/",
            "/",
            "/aaa",
            "/aaa",
            "/aaa/bbb",
            "/aaa/bbb",
            "/aaa/bbb/ccc/ddd/eee",
            "./",
            "./",
            "./",
            "./",
            "aaa",
            "aaa/",
            "aaa/",
            "aaa/bbb/ccc/ddd",
            "aaa/bbb/ccc/ddd",
            "/..",
            "/..",
            "..",
            "../",
            "../..",
            "aaa/..",
            "aaa/../..",
            "aaa/bbb/..",
            "aaa/bbb/../..",
            "aaa/.bbb",
            "aaa./bbb",
            "/",
            "/",
            "/",
            "aaa/",
            "aaa/",
            "aaa/",
            "//",
            "//aaa",
            "//aaa/bbb",
            "//aaa/../bbb",
            "//..",
            "//.",
            "//aaa/",
            "//aaa/",
            "//aaa/bbb",
        };
#endif
        for (size_t i = 0; NELEMS(opaths) > i; i++)
        {
            const char *path = Base::cstr_path_normalize(ipaths[i]);
            ASSERT_EQ(strlen(path), Base::cstr_length(path));
            ASSERT_STREQ(opaths[i], path);
        }
    }
}
TEST(CStringPathTest, normalize_resolveParent)
{
    mark_and_collect
    {
#if defined(BASE_CONFIG_WINDOWS)
        const char *ipaths[] =
        {
            "",
            "/",
            "///",
            "/aaa",
            "///aaa",
            "/aaa//bbb",
            "///aaa//bbb",
            "/aaa//bbb/ccc///ddd/././/.///eee",
            ".",
            "./",
            "./.",
            "./././",
            "./././aaa",
            "./././aaa///",
            "aaa//",
            "aaa/bbb///ccc/././//ddd",
            "././aaa/bbb///ccc/././//ddd",
            "/..",
            "/../..",
            "///..",
            "..",
            "../",
            "../..",
            "aaa/..",
            "aaa/../..",
            "aaa/bbb/..",
            "aaa/bbb/../..",
            "aaa/.bbb",
            "aaa./bbb",
            "/./",
            "/.///",
            "/.",
            "aaa///./",
            "aaa///.///",
            "aaa///.",
            "//",
            "//aaa",
            "//aaa//bbb",
            "//aaa//../bbb",
            "//..",
            "//../..",
            "//../../..",
            "//.",
            "//aaa/.",
            "//aaa/./.",
            "//aaa/./bbb",
            "/aaa/bbb/../ccc/ddd/../eee",
            "//mmm/aaa/bbb/../ccc/ddd/../eee",
            "a:",
            "a:.",
            "a:.\\",
            "a:a",
            "a:\\",
            "a:\\.",
            "a:\\..",
            "a:\\a",
        };
        const char *opaths[] =
        {
            ".\\",
            "\\",
            "\\",
            "\\aaa",
            "\\aaa",
            "\\aaa\\bbb",
            "\\aaa\\bbb",
            "\\aaa\\bbb\\ccc\\ddd\\eee",
            ".\\",
            ".\\",
            ".\\",
            ".\\",
            "aaa",
            "aaa\\",
            "aaa\\",
            "aaa\\bbb\\ccc\\ddd",
            "aaa\\bbb\\ccc\\ddd",
            "\\",
            "\\",
            "\\",
            "..\\",
            "..\\",
            "..\\..\\",
            ".\\",
            "..\\",
            "aaa\\",
            ".\\",
            "aaa\\.bbb",
            "aaa.\\bbb",
            "\\",
            "\\",
            "\\",
            "aaa\\",
            "aaa\\",
            "aaa\\",
            "\\\\",
            "\\\\aaa",
            "\\\\aaa\\bbb",
            "\\\\aaa\\bbb",
            "\\\\..",
            "\\\\..\\",
            "\\\\..\\",
            "\\\\.",
            "\\\\aaa\\",
            "\\\\aaa\\",
            "\\\\aaa\\bbb",
            "\\aaa\\ccc\\eee",
            "\\\\mmm\\aaa\\ccc\\eee",
            "a:",
            "a:",
            "a:",
            "a:a",
            "a:\\",
            "a:\\",
            "a:\\",
            "a:\\a",
        };
#elif defined(BASE_CONFIG_POSIX)
        const char *ipaths[] =
        {
            "",
            "/",
            "///",
            "/aaa",
            "///aaa",
            "/aaa//bbb",
            "///aaa//bbb",
            "/aaa//bbb/ccc///ddd/././/.///eee",
            ".",
            "./",
            "./.",
            "./././",
            "./././aaa",
            "./././aaa///",
            "aaa//",
            "aaa/bbb///ccc/././//ddd",
            "././aaa/bbb///ccc/././//ddd",
            "/..",
            "/../..",
            "///..",
            "..",
            "../",
            "../..",
            "aaa/..",
            "aaa/../..",
            "aaa/bbb/..",
            "aaa/bbb/../..",
            "aaa/.bbb",
            "aaa./bbb",
            "/./",
            "/.///",
            "/.",
            "aaa///./",
            "aaa///.///",
            "aaa///.",
            "//",
            "//aaa",
            "//aaa//bbb",
            "//aaa//../bbb",
            "//..",
            "//../..",
            "//../../..",
            "//.",
            "//aaa/.",
            "//aaa/./.",
            "//aaa/./bbb",
            "/aaa/bbb/../ccc/ddd/../eee",
            "//mmm/aaa/bbb/../ccc/ddd/../eee",
        };
        const char *opaths[] =
        {
            "./",
            "/",
            "/",
            "/aaa",
            "/aaa",
            "/aaa/bbb",
            "/aaa/bbb",
            "/aaa/bbb/ccc/ddd/eee",
            "./",
            "./",
            "./",
            "./",
            "aaa",
            "aaa/",
            "aaa/",
            "aaa/bbb/ccc/ddd",
            "aaa/bbb/ccc/ddd",
            "/",
            "/",
            "/",
            "../",
            "../",
            "../../",
            "./",
            "../",
            "aaa/",
            "./",
            "aaa/.bbb",
            "aaa./bbb",
            "/",
            "/",
            "/",
            "aaa/",
            "aaa/",
            "aaa/",
            "//",
            "//aaa",
            "//aaa/bbb",
            "//aaa/bbb",
            "//..",
            "//../",
            "//../",
            "//.",
            "//aaa/",
            "//aaa/",
            "//aaa/bbb",
            "/aaa/ccc/eee",
            "//mmm/aaa/ccc/eee",
        };
#endif
        for (size_t i = 0; NELEMS(opaths) > i; i++)
        {
            const char *path = Base::cstr_path_normalize(ipaths[i], true);
            ASSERT_EQ(strlen(path), Base::cstr_length(path));
            ASSERT_STREQ(opaths[i], path);
        }
    }
}
TEST(CStringPathTest, concat)
{
    mark_and_collect
    {
#if defined(BASE_CONFIG_WINDOWS)
        const char *ipaths[] =
        {
            "", "",
            "/", "",
            "", "/",
            "aaa", "bbb",
            "aaa/", "bbb",
            "./", "./",
            "aaa", "./",
            "./", "bbb",
            "aaa/.", "./bbb",
            "aaa//", "./bbb",
            "/aaa", "/bbb",
            "/aaa", "//bbb",
            "/aaa", "///bbb",
            "..", "..",
            "./..", "./..",
        };
        const char *opaths[] =
        {
            ".\\",
            "\\",
            "\\",
            "aaa\\bbb",
            "aaa\\bbb",
            ".\\",
            "aaa\\",
            "bbb",
            "aaa\\bbb",
            "aaa\\bbb",
            "\\bbb",
            "\\\\bbb",
            "\\bbb",
            "..\\..",
            "..\\..",
        };
#elif defined(BASE_CONFIG_POSIX)
        const char *ipaths[] =
        {
            "", "",
            "/", "",
            "", "/",
            "aaa", "bbb",
            "aaa/", "bbb",
            "./", "./",
            "aaa", "./",
            "./", "bbb",
            "aaa/.", "./bbb",
            "aaa//", "./bbb",
            "/aaa", "/bbb",
            "/aaa", "//bbb",
            "/aaa", "///bbb",
            "..", "..",
            "./..", "./..",
        };
        const char *opaths[] =
        {
            "./",
            "/",
            "/",
            "aaa/bbb",
            "aaa/bbb",
            "./",
            "aaa/",
            "bbb",
            "aaa/bbb",
            "aaa/bbb",
            "/bbb",
            "//bbb",
            "/bbb",
            "../..",
            "../..",
        };
#endif
        for (size_t i = 0; NELEMS(opaths) > i; i++)
        {
            const char *path = Base::cstr_path_concat(ipaths[i * 2 + 0], ipaths[i * 2 + 1]);
            ASSERT_EQ(strlen(path), Base::cstr_length(path));
            ASSERT_STREQ(opaths[i], path);
        }
    }
}
TEST(CStringPathTest, concat_resolveParent)
{
    mark_and_collect
    {
#if defined(BASE_CONFIG_WINDOWS)
        const char *ipaths[] =
        {
            "", "",
            "/", "",
            "", "/",
            "aaa", "bbb",
            "aaa/", "bbb",
            "./", "./",
            "aaa", "./",
            "./", "bbb",
            "aaa/.", "./bbb",
            "aaa//", "./bbb",
            "/aaa", "/bbb",
            "/aaa", "//bbb",
            "/aaa", "///bbb",
            "..", "..",
            "./..", "./..",
            "aaa", "..",
            "aaa/bbb", "../..",
            "aaa/bbb///ccc", "../..",
            "//mmm", "..",
            "//mmm/aaa", "..",
            "//mmm/aaa/bbb", "..",
        };
        const char *opaths[] =
        {
            ".\\",
            "\\",
            "\\",
            "aaa\\bbb",
            "aaa\\bbb",
            ".\\",
            "aaa\\",
            "bbb",
            "aaa\\bbb",
            "aaa\\bbb",
            "\\bbb",
            "\\\\bbb",
            "\\bbb",
            "..\\..\\",
            "..\\..\\",
            ".\\",
            ".\\",
            "aaa\\",
            "\\\\mmm\\",
            "\\\\mmm\\",
            "\\\\mmm\\aaa\\",
        };
#elif defined(BASE_CONFIG_POSIX)
        const char *ipaths[] =
        {
            "", "",
            "/", "",
            "", "/",
            "aaa", "bbb",
            "aaa/", "bbb",
            "./", "./",
            "aaa", "./",
            "./", "bbb",
            "aaa/.", "./bbb",
            "aaa//", "./bbb",
            "/aaa", "/bbb",
            "/aaa", "//bbb",
            "/aaa", "///bbb",
            "..", "..",
            "./..", "./..",
            "aaa", "..",
            "aaa/bbb", "../..",
            "aaa/bbb///ccc", "../..",
            "//mmm", "..",
            "//mmm/aaa", "..",
            "//mmm/aaa/bbb", "..",
        };
        const char *opaths[] =
        {
            "./",
            "/",
            "/",
            "aaa/bbb",
            "aaa/bbb",
            "./",
            "aaa/",
            "bbb",
            "aaa/bbb",
            "aaa/bbb",
            "/bbb",
            "//bbb",
            "/bbb",
            "../../",
            "../../",
            "./",
            "./",
            "aaa/",
            "//mmm/",
            "//mmm/",
            "//mmm/aaa/",
        };
#endif
        for (size_t i = 0; NELEMS(opaths) > i; i++)
        {
            const char *path = Base::cstr_path_concat(ipaths[i * 2 + 0], ipaths[i * 2 + 1], true);
            ASSERT_EQ(strlen(path), Base::cstr_length(path));
            ASSERT_STREQ(opaths[i], path);
        }
    }
}
TEST(CStringPathTest, concat_misc)
{
    mark_and_collect
    {
        const char *current = Base::cstr_path_realpath(".");
        const char *cconcat = Base::cstr_path_concat(current, current);
        ASSERT_STREQ(current, cconcat);
    }
    mark_and_collect
    {
        const char *current = Base::cstr_path_realpath(".");
        const char *parent = Base::cstr_path_realpath("..");
        const char *pconcat = Base::cstr_path_concat(parent, ".");
        const char *cconcat = Base::cstr_path_concat(current, "..", true);
        ASSERT_STREQ(pconcat, cconcat);
    }
}
TEST(CStringPathTest, basename)
{
    mark_and_collect
    {
        const char *basename = Base::cstr_path_basename("aaa");
        ASSERT_STREQ("aaa", basename);
        basename = Base::cstr_path_basename("/");
        ASSERT_STREQ("", basename);
        basename = Base::cstr_path_basename("/aaa");
        ASSERT_STREQ("aaa", basename);
        basename = Base::cstr_path_basename("/aaa/");
        ASSERT_STREQ("", basename);
        basename = Base::cstr_path_basename("/aaa/bbb");
        ASSERT_STREQ("bbb", basename);
        basename = Base::cstr_path_basename("//mmm");
        ASSERT_STREQ("", basename);
        basename = Base::cstr_path_basename("//mmm/aaa");
        ASSERT_STREQ("aaa", basename);
    }
}
TEST(CStringPathTest, extension)
{
    mark_and_collect
    {
        const char *extension = Base::cstr_path_extension("aaa");
        ASSERT_STREQ("", extension);
        extension = Base::cstr_path_extension(".aaa");
        ASSERT_STREQ("", extension);
        extension = Base::cstr_path_extension("aaa.");
        ASSERT_STREQ(".", extension);
        extension = Base::cstr_path_extension("aaa.ext");
        ASSERT_STREQ(".ext", extension);
        extension = Base::cstr_path_extension(".aaa.ext");
        ASSERT_STREQ(".ext", extension);
        extension = Base::cstr_path_extension("/");
        ASSERT_STREQ("", extension);
        extension = Base::cstr_path_extension("/aaa");
        ASSERT_STREQ("", extension);
        extension = Base::cstr_path_extension("/.aaa");
        ASSERT_STREQ("", extension);
        extension = Base::cstr_path_extension("/aaa.");
        ASSERT_STREQ(".", extension);
        extension = Base::cstr_path_extension("/aaa.ext");
        ASSERT_STREQ(".ext", extension);
        extension = Base::cstr_path_extension("/.aaa.ext");
        ASSERT_STREQ(".ext", extension);
        extension = Base::cstr_path_extension("/aaa.ext/");
        ASSERT_STREQ("", extension);
        extension = Base::cstr_path_extension("/aaa.ext/bbb");
        ASSERT_STREQ("", extension);
        extension = Base::cstr_path_extension("/aaa.ext/.bbb");
        ASSERT_STREQ("", extension);
        extension = Base::cstr_path_extension("/aaa.ext/bbb.");
        ASSERT_STREQ(".", extension);
        extension = Base::cstr_path_extension("/aaa.ext/bbb.txe");
        ASSERT_STREQ(".txe", extension);
        extension = Base::cstr_path_extension("/aaa.ext/.bbb.txe");
        ASSERT_STREQ(".txe", extension);
        extension = Base::cstr_path_extension("//mmm");
        ASSERT_STREQ("", extension);
        extension = Base::cstr_path_extension("//mmm.ext/aaa.txe");
        ASSERT_STREQ(".txe", extension);
    }
}
TEST(CStringPathTest, setBasename)
{
    mark_and_collect
    {
#if defined(BASE_CONFIG_WINDOWS)
        const char *path = Base::cstr_path_setBasename("aaa", "AAA");
        ASSERT_STREQ("AAA", path);
        path = Base::cstr_path_setBasename("\\", "AAA");
        ASSERT_STREQ("\\AAA", path);
        path = Base::cstr_path_setBasename("\\aaa", "AAA");
        ASSERT_STREQ("\\AAA", path);
        path = Base::cstr_path_setBasename("\\aaa\\", "AAA");
        ASSERT_STREQ("\\aaa\\AAA", path);
        path = Base::cstr_path_setBasename("\\aaa\\bbb", "AAA");
        ASSERT_STREQ("\\aaa\\AAA", path);
        path = Base::cstr_path_setBasename("\\\\mmm", "AAA");
        ASSERT_STREQ("\\\\mmm\\AAA", path);
        path = Base::cstr_path_setBasename("\\\\mmm\\", "AAA");
        ASSERT_STREQ("\\\\mmm\\AAA", path);
        path = Base::cstr_path_setBasename("\\\\mmm\\aaa", "AAA");
        ASSERT_STREQ("\\\\mmm\\AAA", path);
#elif defined(BASE_CONFIG_POSIX)
        const char *path = Base::cstr_path_setBasename("aaa", "AAA");
        ASSERT_STREQ("AAA", path);
        path = Base::cstr_path_setBasename("/", "AAA");
        ASSERT_STREQ("/AAA", path);
        path = Base::cstr_path_setBasename("/aaa", "AAA");
        ASSERT_STREQ("/AAA", path);
        path = Base::cstr_path_setBasename("/aaa/", "AAA");
        ASSERT_STREQ("/aaa/AAA", path);
        path = Base::cstr_path_setBasename("/aaa/bbb", "AAA");
        ASSERT_STREQ("/aaa/AAA", path);
        path = Base::cstr_path_setBasename("//mmm", "AAA");
        ASSERT_STREQ("//mmm/AAA", path);
        path = Base::cstr_path_setBasename("//mmm/", "AAA");
        ASSERT_STREQ("//mmm/AAA", path);
        path = Base::cstr_path_setBasename("//mmm/aaa", "AAA");
        ASSERT_STREQ("//mmm/AAA", path);
#endif
    }
}
TEST(CStringPathTest, setExtension)
{
    mark_and_collect
    {
#if defined(BASE_CONFIG_WINDOWS)
        const char *path = Base::cstr_path_setExtension("aaa", ".EXT");
        ASSERT_STREQ("aaa.EXT", path);
        path = Base::cstr_path_setExtension(".aaa", ".EXT");
        ASSERT_STREQ(".aaa.EXT", path);
        path = Base::cstr_path_setExtension("aaa.", ".EXT");
        ASSERT_STREQ("aaa.EXT", path);
        path = Base::cstr_path_setExtension("aaa.ext", ".EXT");
        ASSERT_STREQ("aaa.EXT", path);
        path = Base::cstr_path_setExtension(".aaa.ext", ".EXT");
        ASSERT_STREQ(".aaa.EXT", path);
        path = Base::cstr_path_setExtension("\\", ".EXT");
        ASSERT_STREQ("\\", path);
        path = Base::cstr_path_setExtension("\\aaa", ".EXT");
        ASSERT_STREQ("\\aaa.EXT", path);
        path = Base::cstr_path_setExtension("\\.aaa", ".EXT");
        ASSERT_STREQ("\\.aaa.EXT", path);
        path = Base::cstr_path_setExtension("\\aaa.", ".EXT");
        ASSERT_STREQ("\\aaa.EXT", path);
        path = Base::cstr_path_setExtension("\\aaa.ext", ".EXT");
        ASSERT_STREQ("\\aaa.EXT", path);
        path = Base::cstr_path_setExtension("\\.aaa.ext", ".EXT");
        ASSERT_STREQ("\\.aaa.EXT", path);
        path = Base::cstr_path_setExtension("\\aaa.ext\\", ".EXT");
        ASSERT_STREQ("\\aaa.ext\\", path);
        path = Base::cstr_path_setExtension("\\aaa.ext\\bbb", ".EXT");
        ASSERT_STREQ("\\aaa.ext\\bbb.EXT", path);
        path = Base::cstr_path_setExtension("\\aaa.ext\\.bbb", ".EXT");
        ASSERT_STREQ("\\aaa.ext\\.bbb.EXT", path);
        path = Base::cstr_path_setExtension("\\aaa.ext\\bbb.", ".EXT");
        ASSERT_STREQ("\\aaa.ext\\bbb.EXT", path);
        path = Base::cstr_path_setExtension("\\aaa.ext\\bbb.txe", ".EXT");
        ASSERT_STREQ("\\aaa.ext\\bbb.EXT", path);
        path = Base::cstr_path_setExtension("\\aaa.ext\\.bbb.txe", ".EXT");
        ASSERT_STREQ("\\aaa.ext\\.bbb.EXT", path);
        path = Base::cstr_path_setExtension("\\\\mmm", ".EXT");
        ASSERT_STREQ("\\\\mmm", path);
        path = Base::cstr_path_setExtension("\\\\mmm\\", ".EXT");
        ASSERT_STREQ("\\\\mmm\\", path);
        path = Base::cstr_path_setExtension("\\\\mmm.ext\\aaa.txe", ".EXT");
        ASSERT_STREQ("\\\\mmm.ext\\aaa.EXT", path);
        path = Base::cstr_path_setExtension("", ".EXT");
        ASSERT_STREQ("", path);
#elif defined(BASE_CONFIG_POSIX)
        const char *path = Base::cstr_path_setExtension("aaa", ".EXT");
        ASSERT_STREQ("aaa.EXT", path);
        path = Base::cstr_path_setExtension(".aaa", ".EXT");
        ASSERT_STREQ(".aaa.EXT", path);
        path = Base::cstr_path_setExtension("aaa.", ".EXT");
        ASSERT_STREQ("aaa.EXT", path);
        path = Base::cstr_path_setExtension("aaa.ext", ".EXT");
        ASSERT_STREQ("aaa.EXT", path);
        path = Base::cstr_path_setExtension(".aaa.ext", ".EXT");
        ASSERT_STREQ(".aaa.EXT", path);
        path = Base::cstr_path_setExtension("/", ".EXT");
        ASSERT_STREQ("/", path);
        path = Base::cstr_path_setExtension("/aaa", ".EXT");
        ASSERT_STREQ("/aaa.EXT", path);
        path = Base::cstr_path_setExtension("/.aaa", ".EXT");
        ASSERT_STREQ("/.aaa.EXT", path);
        path = Base::cstr_path_setExtension("/aaa.", ".EXT");
        ASSERT_STREQ("/aaa.EXT", path);
        path = Base::cstr_path_setExtension("/aaa.ext", ".EXT");
        ASSERT_STREQ("/aaa.EXT", path);
        path = Base::cstr_path_setExtension("/.aaa.ext", ".EXT");
        ASSERT_STREQ("/.aaa.EXT", path);
        path = Base::cstr_path_setExtension("/aaa.ext/", ".EXT");
        ASSERT_STREQ("/aaa.ext/", path);
        path = Base::cstr_path_setExtension("/aaa.ext/bbb", ".EXT");
        ASSERT_STREQ("/aaa.ext/bbb.EXT", path);
        path = Base::cstr_path_setExtension("/aaa.ext/.bbb", ".EXT");
        ASSERT_STREQ("/aaa.ext/.bbb.EXT", path);
        path = Base::cstr_path_setExtension("/aaa.ext/bbb.", ".EXT");
        ASSERT_STREQ("/aaa.ext/bbb.EXT", path);
        path = Base::cstr_path_setExtension("/aaa.ext/bbb.txe", ".EXT");
        ASSERT_STREQ("/aaa.ext/bbb.EXT", path);
        path = Base::cstr_path_setExtension("/aaa.ext/.bbb.txe", ".EXT");
        ASSERT_STREQ("/aaa.ext/.bbb.EXT", path);
        path = Base::cstr_path_setExtension("//mmm", ".EXT");
        ASSERT_STREQ("//mmm", path);
        path = Base::cstr_path_setExtension("//mmm/", ".EXT");
        ASSERT_STREQ("//mmm/", path);
        path = Base::cstr_path_setExtension("//mmm.ext/aaa.txe", ".EXT");
        ASSERT_STREQ("//mmm.ext/aaa.EXT", path);
        path = Base::cstr_path_setExtension("", ".EXT");
        ASSERT_STREQ("", path);
#endif
    }
}
#endif
