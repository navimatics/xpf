/*
 * Base/ErrorTest.cpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#include <Base/Error.hpp>
#include <Base/Log.hpp>
#if defined(BASE_CONFIG_WINDOWS)
#include <windows.h>
#endif
#include <gtest/gtest.h>

TEST(ErrorTest, Test)
{
    Base::Object::Mark();
    Base::Error::setLastError("TestErrorDomain", 'E', "error message");
    ASSERT_STREQ("TestErrorDomain", Base::Error::lastError()->domain());
    ASSERT_EQ('E', Base::Error::lastError()->code());
    ASSERT_STREQ("error message", Base::Error::lastError()->message());
    Base::Error::setLastErrorFromCErrno(EINVAL);
    ASSERT_EQ(EINVAL, Base::Error::lastError()->code());
    errno = EACCES;
    Base::Error::setLastErrorFromCErrno();
    ASSERT_EQ(EACCES, Base::Error::lastError()->code());
#if defined(BASE_CONFIG_WINDOWS)
    SetLastError(ERROR_FILE_NOT_FOUND);
    Base::Error::setLastErrorFromSystem();
    ASSERT_EQ(ERROR_FILE_NOT_FOUND, Base::Error::lastError()->code());
#endif
    Base::Object::Collect();
}
TEST(ErrorTest, TestMessage)
{
    /* This test may fail as it relies on error messages that depend on platform/OS. */
    Base::Object::Mark();
    Base::Error::setLastErrorFromCErrno(EINVAL);
    ASSERT_EQ(EINVAL, Base::Error::lastError()->code());
    ASSERT_STREQ("Invalid argument", Base::Error::lastError()->message());
#if defined(BASE_CONFIG_WINDOWS)
    Base::Error::setLastErrorFromSystem(ERROR_FILE_NOT_FOUND);
    ASSERT_EQ(ERROR_FILE_NOT_FOUND, Base::Error::lastError()->code());
    ASSERT_STREQ("The system cannot find the file specified.", Base::Error::lastError()->message());
#endif
    Base::Object::Collect();
}
