/*
 * Base/ZlibStreamTest.cpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#include <Base/FileStream.hpp>
#include <Base/MemoryStream.hpp>
#include <Base/ZlibStream.hpp>
#include <gtest/gtest.h>

TEST(ZlibStreamTest, TestZlib)
{
    Base::Object::Mark();
    char path[L_tmpnam]; tmpnam(path);
    Base::Stream *iostream, *stream;
    char buf[64]; ssize_t bytes;
    memset(buf, 0, sizeof buf);
    iostream = Base::FileStream::create(path, "wb");
    stream = new Base::ZlibStream(iostream, "wbz9");
    stream->write("1234567890", 10);
    stream->release();
    iostream->release();
    iostream = Base::FileStream::create(path, "rb");
    stream = new Base::ZlibStream(iostream, "rbz0");
    bytes = stream->read(buf, sizeof buf - 1);
    stream->release();
    iostream->release();
    ASSERT_EQ(10, bytes);
    ASSERT_STREQ("1234567890", buf);
    iostream = Base::FileStream::create(path, "rb");
    bytes = iostream->read(buf, sizeof buf - 1);
    iostream->release();
    iostream = Base::FileStream::create(path, "wb");
    stream = new Base::ZlibStream(iostream, "wbz0");
    stream->write(buf, bytes);
    stream->release();
    iostream->release();
    char buf2[64]; ssize_t bytes2;
    memset(buf2, 0, sizeof buf2);
    iostream = Base::FileStream::create(path, "rb");
    stream = new Base::ZlibStream(iostream, "rbz9");
    bytes2 = stream->read(buf2, sizeof buf2 - 1);
    stream->release();
    iostream->release();
    ASSERT_TRUE(bytes == bytes2);
    ASSERT_TRUE(0 == memcmp(buf, buf2, bytes));
    remove(path);
    Base::Object::Collect();
}
TEST(ZlibStreamTest, TestGzip)
{
    Base::Object::Mark();
    char path[L_tmpnam]; tmpnam(path);
    Base::Stream *iostream, *stream;
    char buf[64]; ssize_t bytes;
    memset(buf, 0, sizeof buf);
    iostream = Base::FileStream::create(path, "wb");
    stream = new Base::ZlibStream(iostream, "wbg9");
    stream->write("1234567890", 10);
    stream->release();
    iostream->release();
    iostream = Base::FileStream::create(path, "rb");
    stream = new Base::ZlibStream(iostream, "rbg0");
    bytes = stream->read(buf, sizeof buf - 1);
    stream->release();
    iostream->release();
    ASSERT_EQ(10, bytes);
    ASSERT_STREQ("1234567890", buf);
    iostream = Base::FileStream::create(path, "rb");
    bytes = iostream->read(buf, sizeof buf - 1);
    iostream->release();
    iostream = Base::FileStream::create(path, "wb");
    stream = new Base::ZlibStream(iostream, "wbg0");
    stream->write(buf, bytes);
    stream->release();
    iostream->release();
    char buf2[64]; ssize_t bytes2;
    memset(buf2, 0, sizeof buf2);
    iostream = Base::FileStream::create(path, "rb");
    stream = new Base::ZlibStream(iostream, "rbg9");
    bytes2 = stream->read(buf2, sizeof buf2 - 1);
    stream->release();
    iostream->release();
    ASSERT_TRUE(bytes == bytes2);
    ASSERT_TRUE(0 == memcmp(buf, buf2, bytes));
    remove(path);
    Base::Object::Collect();
}
TEST(ZlibStreamTest, TestZlibLarge)
{
    Base::Object::Mark();
    char path[L_tmpnam]; tmpnam(path);
    Base::Stream *iostream, *stream;
    ssize_t bytes;
    ssize_t bufsiz = 100000;
    char *buf = (char *)Base::impl::Malloc(bufsiz);
    char *buf2 = (char *)Base::impl::Malloc(bufsiz);
    for (ssize_t i = 0; bufsiz > i; i++)
        buf[i] = rand();
    iostream = Base::FileStream::create(path, "wb");
    stream = new Base::ZlibStream(iostream, "wb9");
    stream->write(buf, bufsiz);
    stream->release();
    iostream->release();
    iostream = Base::FileStream::create(path, "rb");
    stream = new Base::ZlibStream(iostream, "rb0");
    bytes = stream->read(buf2, bufsiz);
    ASSERT_EQ(bufsiz, bytes);
    bytes = stream->read(buf2, bufsiz);
    ASSERT_EQ(0, bytes);
    stream->release();
    iostream->release();
    ASSERT_TRUE(0 == memcmp(buf, buf2, bufsiz));
    free(buf2);
    free(buf);
    remove(path);
    Base::Object::Collect();
}
TEST(ZlibStreamTest, TestZlibChunks)
{
    Base::Object::Mark();
    char path[L_tmpnam]; tmpnam(path);
    Base::Stream *iostream, *stream;
    ssize_t bytes;
    ssize_t bufsiz = 100000;
    char *buf = (char *)Base::impl::Malloc(bufsiz);
    char *buf2 = (char *)Base::impl::Malloc(bufsiz);
    for (ssize_t i = 0; bufsiz > i; i++)
        buf[i] = rand();
    iostream = Base::FileStream::create(path, "wb");
    stream = new Base::ZlibStream(iostream, "wb9");
    for (ssize_t i = 0; bufsiz > i; i += 1000)
        stream->write(buf + i, 1000);
    stream->release();
    iostream->release();
    iostream = Base::FileStream::create(path, "rb");
    stream = new Base::ZlibStream(iostream, "rb0");
    bytes = 0;
    for (ssize_t i = 0; bufsiz > i; i += 1000)
        bytes += stream->read(buf2 + i, 1000);
    ASSERT_EQ(bufsiz, bytes);
    bytes = stream->read(buf2, bufsiz);
    ASSERT_EQ(0, bytes);
    stream->release();
    iostream->release();
    ASSERT_TRUE(0 == memcmp(buf, buf2, bufsiz));
    free(buf2);
    free(buf);
    remove(path);
    Base::Object::Collect();
}
TEST(ZlibStreamTest, TestEmpty)
{
    Base::Object::Mark();
    Base::Stream *iostream, *stream;
    char buf[64]; ssize_t bytes;
    /* zlib read */
    iostream = new Base::MemoryStream;
    stream = new Base::ZlibStream(iostream, "rb0");
    bytes = stream->read(buf, NELEMS(buf));
    ASSERT_EQ(0, bytes);
    stream->release();
    iostream->release();
    /* gzip read */
    iostream = new Base::MemoryStream;
    stream = new Base::ZlibStream(iostream, "rbg0");
    bytes = stream->read(buf, NELEMS(buf));
    ASSERT_EQ(0, bytes);
    stream->release();
    iostream->release();
    Base::Object::Collect();
}
