/*
 * Base/MemoryTest.cpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#include <Base/MemoryStream.hpp>
#include <gtest/gtest.h>

#define STREAMREAD(s)                   ASSERT_EQ((ssize_t)strlen(s), stream->read(buf, strlen(s))); ASSERT_TRUE(0 == memcmp(s, buf, strlen(s)))
#define STREAMWRITE(s)                  ASSERT_EQ((ssize_t)strlen(s), stream->write(s, strlen(s)))

TEST(MemoryStreamTest, ReadTest)
{
    Base::Object::Mark();
    Base::MemoryStream *stream = new Base::MemoryStream("The quick brown fox jumps over the lazy dog.");
    char buf[128];
    STREAMREAD("The");
    STREAMREAD(" quick brown fox");
    STREAMREAD(" jumps");
    STREAMREAD(" over the");
    STREAMREAD(" lazy dog.");
    ASSERT_EQ(0, stream->read(buf, sizeof buf));
    ASSERT_EQ(strlen("The quick brown fox jumps over the lazy dog."), stream->seek(0, SEEK_CUR));
    stream->close();
    stream->release();
    Base::Object::Collect();
}
TEST(MemoryStreamTest, WriteTest)
{
    Base::Object::Mark();
    Base::MemoryStream *stream = new Base::MemoryStream;
    STREAMWRITE("The");
    STREAMWRITE(" quick brown fox");
    STREAMWRITE(" jumps");
    STREAMWRITE(" over the");
    STREAMWRITE(" lazy dog.");
    ASSERT_EQ(strlen("The quick brown fox jumps over the lazy dog."), stream->seek(0, SEEK_CUR));
    ASSERT_EQ(strlen("The quick brown fox jumps over the lazy dog."), stream->size());
    ASSERT_STREQ("The quick brown fox jumps over the lazy dog.", stream->buffer());
    ASSERT_TRUE(0 == memcmp("The quick brown fox jumps over the lazy dog.", stream->buffer(), stream->size()));
    stream->close();
    stream->release();
    Base::Object::Collect();
}
TEST(MemoryStreamTest, WriteReadTest)
{
    Base::Object::Mark();
    Base::MemoryStream *stream = new Base::MemoryStream;
    STREAMWRITE("The");
    STREAMWRITE(" quick brown fox");
    STREAMWRITE(" jumps");
    STREAMWRITE(" over the");
    STREAMWRITE(" lazy dog.");
    char buf[128];
    ASSERT_EQ(0, stream->seek(0, SEEK_SET));
    STREAMREAD("The");
    STREAMREAD(" quick brown fox");
    STREAMREAD(" jumps");
    STREAMREAD(" over the");
    STREAMREAD(" lazy dog.");
    ASSERT_EQ(0, stream->read(buf, sizeof buf));
    stream->close();
    stream->release();
    Base::Object::Collect();
}
