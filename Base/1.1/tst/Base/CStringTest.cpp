/*
 * Base/CStringTest.cpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#include <Base/CString.hpp>
#include <gtest/gtest.h>

TEST(CStringTest, cstr_new)
{
    const char *cstr = Base::cstr_new("12345");
    ASSERT_EQ((size_t)5, Base::cstr_length(cstr));
    ASSERT_EQ((size_t)5, Base::cstr_obj(cstr)->length());
    ASSERT_STREQ("12345", cstr);
    ASSERT_STREQ("12345", Base::cstr_obj(cstr)->chars());
    const char *cstr2 = Base::cstr_new(cstr, Base::cstr_length(cstr));
    ASSERT_EQ((size_t)5, Base::cstr_length(cstr2));
    ASSERT_EQ((size_t)5, Base::cstr_obj(cstr2)->length());
    ASSERT_TRUE(0 == strcmp(cstr, cstr2));
    ASSERT_TRUE(0 == strcmp(Base::cstr_obj(cstr)->chars(), Base::cstr_obj(cstr2)->chars()));
    Base::cstr_obj(cstr2)->release();
    Base::cstr_obj(cstr)->release();
    cstr = Base::cstr_newf("%i", 424242);
    ASSERT_EQ((size_t)6, Base::cstr_length(cstr));
    ASSERT_EQ((size_t)6, Base::cstr_obj(cstr)->length());
    ASSERT_STREQ("424242", cstr);
    ASSERT_STREQ("424242", Base::cstr_obj(cstr)->chars());
    Base::cstr_obj(cstr)->release();
}
TEST(CStringTest, cstring)
{
    Base::Object::Mark();
    const char *cstr = Base::cstring("12345");
    ASSERT_EQ((size_t)5, Base::cstr_length(cstr));
    ASSERT_EQ((size_t)5, Base::cstr_obj(cstr)->length());
    ASSERT_STREQ("12345", cstr);
    ASSERT_STREQ("12345", Base::cstr_obj(cstr)->chars());
    const char *cstr2 = Base::cstring(cstr, Base::cstr_length(cstr));
    ASSERT_EQ((size_t)5, Base::cstr_length(cstr2));
    ASSERT_EQ((size_t)5, Base::cstr_obj(cstr2)->length());
    ASSERT_TRUE(0 == strcmp(cstr, cstr2));
    ASSERT_TRUE(0 == strcmp(Base::cstr_obj(cstr)->chars(), Base::cstr_obj(cstr2)->chars()));
    cstr = Base::cstringf("%i", 424242);
    ASSERT_EQ((size_t)6, Base::cstr_length(cstr));
    ASSERT_EQ((size_t)6, Base::cstr_obj(cstr)->length());
    ASSERT_STREQ("424242", cstr);
    ASSERT_STREQ("424242", Base::cstr_obj(cstr)->chars());
    Base::Object::Collect();
}
TEST(CStringTest, slice)
{
    Base::Object::Mark();
    const char *cstr = Base::cstring(0);
    ASSERT_EQ((size_t)0, Base::cstr_length(cstr));
    const char *slice = Base::cstr_slice(cstr, -10, 10);
    ASSERT_EQ((size_t)0, Base::cstr_length(slice));
    cstr = Base::cstring("12345");
    ASSERT_STREQ("12", Base::cstr_slice(cstr, 0, 2));
    ASSERT_STREQ("23", Base::cstr_slice(cstr, 1, 3));
    ASSERT_STREQ("345", Base::cstr_slice(cstr, -3));
    ASSERT_STREQ("34", Base::cstr_slice(cstr, -3, -1));
    ASSERT_STREQ("345", Base::cstr_slice(cstr, -3, 1000));
    ASSERT_STREQ("12345", Base::cstr_slice(cstr, -1000, 1000));
    ASSERT_STREQ("", Base::cstr_slice(cstr, -1000, -1000));
    ASSERT_STREQ("", Base::cstr_slice(cstr, 1000, 1000));
    Base::Object::Collect();
}
TEST(CStringTest, _concat)
{
    Base::Object::Mark();
    const char *s0 = Base::cstring("The");
    const char *s1 = Base::cstring("The quick brown fox");
    const char *s2 = Base::cstring("The quick brown fox jumps");
    const char *s3 = Base::cstring("The quick brown fox jumps over the lazy dog.");
    size_t h0 = Base::cstr_obj(s0)->hash();
    size_t h1 = Base::cstr_obj(s1)->hash();
    size_t h2 = Base::cstr_obj(s2)->hash();
    size_t h3 = Base::cstr_obj(s3)->hash();
    const char *t = Base::cstr_new("The");
    ASSERT_TRUE(h0 == Base::cstr_obj(t)->hash());
    t = Base::cstr_obj(t)->_concat(" quick brown fox")->chars();
    ASSERT_TRUE(h1 == Base::cstr_obj(t)->hash());
    t = Base::cstr_obj(t)->_concat(" jumps")->chars();
    ASSERT_TRUE(h2 == Base::cstr_obj(t)->hash());
    t = Base::cstr_obj(t)->_concat(" over the")->chars();
    t = Base::cstr_obj(t)->_concat(" lazy dog.")->chars();
    ASSERT_TRUE(h3 == Base::cstr_obj(t)->hash());
    Base::cstr_obj(t)->release();
    const char *t1 = Base::cstr_new("The");
    t1 = Base::cstr_obj(t1)->_concat(" quick brown fox")->chars();
    ASSERT_TRUE(h1 == Base::cstr_obj(t1)->hash());
    Base::cstr_obj(t1)->release();
    Base::Object::Collect();
}
TEST(CStringTest, constant)
{
    /* this assumes that string literals are interned by the compiler */
    Base::CString *s1 = Base::CStringConstant("abc");
    Base::CString *s2 = Base::CStringConstant("abc");
    Base::CString *s3 = Base::CStringConstant("def");
    Base::CString *s4 = Base::CStringConstant("def");
    ASSERT_EQ(s1, s2);
    ASSERT_NE(s2, s3);
    ASSERT_EQ(s3, s4);
}
TEST(CStringTest, assign)
{
    const char *cstr = Base::cstr_new("12345");
    const char *cstr2 = Base::cstr_new("6789");
    Base::cstr_assign(cstr, cstr2);
    ASSERT_EQ((size_t)4, Base::cstr_length(cstr));
    ASSERT_STREQ("6789", cstr);
    Base::cstr_assign_copy(cstr, "12345");
    ASSERT_EQ((size_t)5, Base::cstr_length(cstr));
    ASSERT_STREQ("12345", cstr);
    Base::cstr_assign(cstr2, 0);
    Base::cstr_assign_copy(cstr, 0);
}
