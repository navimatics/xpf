/*
 * DbKit/zvfsTest.cpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#include <DbKit/Database.hpp>
#include <gtest/gtest.h>

static void zvfsTest(const char *codecName)
{
    char path[L_tmpnam]; tmpnam(path);
    DbKit::Database *db = new DbKit::Database(path, codecName);
    ASSERT_EQ(0, db->open());
    ASSERT_EQ(0, db->exec("CREATE TABLE t(a, b, c)"));
    ASSERT_EQ(0, db->exec("INSERT INTO t VALUES (1, 2, 3)"));
    ASSERT_EQ(0, db->exec("INSERT INTO t VALUES (4, 5, 6)"));
    ASSERT_EQ(0, db->close());
    int count = 0;
    ASSERT_EQ(0, db->open());
    ASSERT_EQ(1, db->exec("SELECT COUNT(*) FROM t", "; i", &count));
    ASSERT_EQ(0, db->close());
    ASSERT_EQ(2, count);
    db->release();
    remove(path);
}

TEST(zvfsTest, Codecs)
{
    zvfsTest("lz4");
    zvfsTest("lz4hc");
    zvfsTest("deflate");
    zvfsTest("deflateRaw");
}
