/*
 * DbKit/DatabaseTest.cpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#include <DbKit/Database.hpp>
#include <gtest/gtest.h>
#include <sqlite3.h>

int dbparams(DbKit::Database *db, int statementIndex, const char *format, ...)
{
    va_list ap;
    va_start(ap, format);
    int result = db->vparams(statementIndex, format, ap);
    va_end(ap);
    return result;
}
int dbcolumns(DbKit::Database *db, int statementIndex, const char *format, ...)
{
    va_list ap;
    va_start(ap, format);
    int result = db->vcolumns(statementIndex, format, ap);
    va_end(ap);
    return result;
}
int dbstep(DbKit::Database *db, int statementIndex, const char *format = 0, ...)
{
    va_list ap;
    va_start(ap, format);
    int result = db->vstep(statementIndex, format, ap);
    va_end(ap);
    return result;
}
int dbexec(DbKit::Database *db, int statementIndex, const char *format = 0, ...)
{
    va_list ap;
    va_start(ap, format);
    int result = db->vexec(statementIndex, format, ap);
    va_end(ap);
    return result;
}
int dbexec(DbKit::Database *db, const char *text, const char *format = 0, ...)
{
    va_list ap;
    va_start(ap, format);
    int result = db->vexec(text, format, ap);
    va_end(ap);
    return result;
}
int dbparams(DbKit::Database *db, DbKit::Database::statement_t statementHandle, const char *format, ...)
{
    va_list ap;
    va_start(ap, format);
    int result = db->vparams(statementHandle, format, ap);
    va_end(ap);
    return result;
}
int dbcolumns(DbKit::Database *db, DbKit::Database::statement_t statementHandle, const char *format, ...)
{
    va_list ap;
    va_start(ap, format);
    int result = db->vcolumns(statementHandle, format, ap);
    va_end(ap);
    return result;
}
int dbstep(DbKit::Database *db, DbKit::Database::statement_t statementHandle, const char *format = 0, ...)
{
    va_list ap;
    va_start(ap, format);
    int result = db->vstep(statementHandle, format, ap);
    va_end(ap);
    return result;
}
int dbexec(DbKit::Database *db, DbKit::Database::statement_t statementHandle, const char *format = 0, ...)
{
    va_list ap;
    va_start(ap, format);
    int result = db->vexec(statementHandle, format, ap);
    va_end(ap);
    return result;
}

static void TestBasic(bool threadsafe)
{
    mark_and_collect
    {
        DbKit::Database *db;

        db = new DbKit::Database("/PATH/DOES/NOT/EXIST");
        if (threadsafe)
            db->threadsafe();
        ASSERT_STREQ("/PATH/DOES/NOT/EXIST", db->path());
        Base::Error::setLastError(0);
        ASSERT_EQ(-1, db->open());
        ASSERT_STREQ(DbKit::Database::errorDomain(), Base::Error::lastError()->domain());
        ASSERT_EQ(0, db->close());
        db->release();

        db = new DbKit::Database;
        if (threadsafe)
            db->threadsafe();
        ASSERT_STREQ(":memory:", db->path());
        ASSERT_EQ(0, db->open());
        ASSERT_EQ(0, db->close());
        ASSERT_EQ(0, db->close());
        ASSERT_EQ(0, db->open());
        ASSERT_EQ(0, db->open());
        db->release();

        db = new DbKit::Database;
        if (threadsafe)
            db->threadsafe();
        ASSERT_STREQ(":memory:", db->path());
        ASSERT_EQ(0, db->open());
        Base::Error::setLastError(0);
        ASSERT_EQ(-1, db->setStatementText(10, "SELECT * FROM TABLE_DOES_NOT_EXIST"));
        ASSERT_STREQ(DbKit::Database::errorDomain(), Base::Error::lastError()->domain());
        ASSERT_EQ(0, db->nextStatementIndex());
        ASSERT_EQ(0, db->setStatementText(0, "SELECT * FRom sqlite_master"));
        ASSERT_EQ(0, db->setStatementText(1, "SELECT * frOM sqlite_master"));
        ASSERT_STREQ("SELECT * FRom sqlite_master", db->statementText(0));
        ASSERT_STREQ("SELECT * frOM sqlite_master", db->statementText(1));
        ASSERT_EQ(0, db->setStatementText(0, 0));
        ASSERT_EQ(0, db->statementText(0));
        ASSERT_STREQ("SELECT * frOM sqlite_master", db->statementText(1));
        ASSERT_EQ(2, db->nextStatementIndex());
        ASSERT_EQ(0, db->setStatementText(42, "SELECT * FROM sqlite_master"));
        ASSERT_STREQ("SELECT * FROM sqlite_master", db->statementText(42));
        ASSERT_EQ(43, db->nextStatementIndex());
        ASSERT_EQ(0, db->setStatementText(42, "SELECT * from sqlite_master"));
        ASSERT_STREQ("SELECT * from sqlite_master", db->statementText(42));
        ASSERT_EQ(43, db->nextStatementIndex());
        db->release();

        db = new DbKit::Database;
        if (threadsafe)
            db->threadsafe();
        ASSERT_EQ(0, db->open());
        ASSERT_EQ(0, db->setStatementText(0, "SELECT * FROM sqlite_master"));
        ASSERT_EQ(
            sqlite3_db_handle((sqlite3_stmt *)db->statementHandle(0)),
            (sqlite3 *)db->connectionHandle());
        db->release();

        db = new DbKit::Database;
        if (threadsafe)
            db->threadsafe();
        ASSERT_EQ(0, db->open());
        DbKit::Database::statement_t statement = db->_prepare("SELECT * FROM sqlite_master");
        ASSERT_NE((void *)0, statement);
        ASSERT_EQ(
            sqlite3_db_handle((sqlite3_stmt *)statement),
            (sqlite3 *)db->connectionHandle());
        db->_unprepare(statement);
        db->release();
    }
}
TEST(DatabaseTest, Basic)
{
    TestBasic(false);
    TestBasic(true);
}

static void TestStatementFormat(bool threadsafe)
{
    mark_and_collect
    {
        DbKit::Database *db;
        int i;
        unsigned u;
        long l;
        int64_t q;
        double f;
        const char *s;
        Base::CString *str;
        const char *buf; size_t bufsiz;

        db = new DbKit::Database;
        if (threadsafe)
            db->threadsafe();
        ASSERT_EQ(0, db->open());
        Base::Error::setLastError(0);
        ASSERT_EQ(-1, db->exec("GARBAGE"));
        ASSERT_STREQ(DbKit::Database::errorDomain(), Base::Error::lastError()->domain());
        Base::Error::setLastError(0);
        ASSERT_EQ(-1, db->exec("SELECT 1", "?"));
        ASSERT_EQ(SQLITE_MISUSE, Base::Error::lastError()->code());
        Base::Error::setLastError(0);
        ASSERT_EQ(-1, db->exec("SELECT 1", ";?"));
        ASSERT_EQ(SQLITE_MISUSE, Base::Error::lastError()->code());
        Base::Error::setLastError(0);
        ASSERT_EQ(-1, db->exec("SELECT 1", "i", 0));
        ASSERT_EQ(SQLITE_RANGE, Base::Error::lastError()->code());
        Base::Error::setLastError(0);
        ASSERT_EQ(+1, db->exec("SELECT 1", ";i", &i));
        ASSERT_EQ(0, Base::Error::lastError());
        ASSERT_EQ(0, db->exec("CREATE TABLE tab (i INTEGER, r REAL, t TEXT, b BLOB)"));
        ASSERT_EQ(0, Base::Error::lastError());
        ASSERT_EQ(0, db->exec("INSERT INTO tab VALUES (?, ?, ?, ?)", "ifs^", 40, 41.0, "42", "43", 3));
        ASSERT_EQ(0, Base::Error::lastError());
        ASSERT_EQ(+1, db->exec("SELECT i, r, t, b FROM tab", ";ifs^", &i, &f, &s, (const void **)&buf, &bufsiz));
        ASSERT_EQ(40, i);
        ASSERT_EQ(41.0, f);
        ASSERT_STREQ("42", s);
        ASSERT_TRUE('4' == buf[0] && '3' == buf[1] && '\0' == buf[2] && bufsiz == 3);
        ASSERT_EQ(+1, db->exec("SELECT i, r, t, b FROM tab WHERE i = ? AND r = ? AND t = ? AND b = ?",
            " ifs^; ifs^;",
            40, 41.0, "42", "43", 3,
            &i, &f, &s, (const void **)&buf, &bufsiz));
        ASSERT_EQ(40, i);
        ASSERT_EQ(41.0, f);
        ASSERT_STREQ("42", s);
        ASSERT_TRUE('4' == buf[0] && '3' == buf[1] && '\0' == buf[2] && bufsiz == 3);
        ASSERT_EQ(+1, db->exec("SELECT i, r, r, t FROM tab WHERE i = ? AND r = ? AND r = ? AND t = ?",
            " ulq*; ulq*;",
            40u, 41l, 41ll, Base::cstr_obj(Base::cstring("42")),
            &u, &l, &q, &str));
        ASSERT_EQ(40u, u);
        ASSERT_EQ(41l, l);
        ASSERT_EQ(41ll, q);
        ASSERT_TRUE(2 == str->length() && 0 == strcmp("42", str->chars()));
        ASSERT_EQ(+1, db->exec("SELECT 1, 90 FROM tab WHERE ? IS NULL AND i = ?",
            ".i; .i",
            40, &i));
        ASSERT_EQ(90, i);
        ASSERT_EQ(+1, db->exec("SELECT i, r, t, b FROM tab WHERE i = ? AND r = ? AND t = ? AND b = ?",
            " 004^ 2fs01i; 004^02fs 1i;",
            "43", 3, 41.0, "42", 40,
            (const void **)&buf, &bufsiz, &f, &s, &i));
        ASSERT_EQ(40, i);
        ASSERT_EQ(41.0, f);
        ASSERT_STREQ("42", s);
        ASSERT_TRUE('4' == buf[0] && '3' == buf[1] && '\0' == buf[2] && bufsiz == 3);
        db->release();
    }
}
static void TestStatementFormatV(bool threadsafe)
{
    mark_and_collect
    {
        DbKit::Database *db;
        int i;
        unsigned u;
        long l;
        int64_t q;
        double f;
        const char *s;
        Base::CString *str;
        const char *buf; size_t bufsiz;

        db = new DbKit::Database;
        if (threadsafe)
            db->threadsafe();
        ASSERT_EQ(0, db->open());
        Base::Error::setLastError(0);
        ASSERT_EQ(-1, dbexec(db, "GARBAGE"));
        ASSERT_STREQ(DbKit::Database::errorDomain(), Base::Error::lastError()->domain());
        Base::Error::setLastError(0);
        ASSERT_EQ(-1, dbexec(db, "SELECT 1", "?"));
        ASSERT_EQ(SQLITE_MISUSE, Base::Error::lastError()->code());
        Base::Error::setLastError(0);
        ASSERT_EQ(-1, dbexec(db, "SELECT 1", ";?"));
        ASSERT_EQ(SQLITE_MISUSE, Base::Error::lastError()->code());
        Base::Error::setLastError(0);
        ASSERT_EQ(-1, dbexec(db, "SELECT 1", "i", 0));
        ASSERT_EQ(SQLITE_RANGE, Base::Error::lastError()->code());
        Base::Error::setLastError(0);
        ASSERT_EQ(+1, dbexec(db, "SELECT 1", ";i", &i));
        ASSERT_EQ(0, Base::Error::lastError());
        ASSERT_EQ(0, dbexec(db, "CREATE TABLE tab (i INTEGER, r REAL, t TEXT, b BLOB)"));
        ASSERT_EQ(0, Base::Error::lastError());
        ASSERT_EQ(0, dbexec(db, "INSERT INTO tab VALUES (?, ?, ?, ?)", "ifs^", 40, 41.0, "42", "43", 3));
        ASSERT_EQ(0, Base::Error::lastError());
        ASSERT_EQ(+1, dbexec(db, "SELECT i, r, t, b FROM tab", ";ifs^", &i, &f, &s, (const void **)&buf, &bufsiz));
        ASSERT_EQ(40, i);
        ASSERT_EQ(41.0, f);
        ASSERT_STREQ("42", s);
        ASSERT_TRUE('4' == buf[0] && '3' == buf[1] && '\0' == buf[2] && bufsiz == 3);
        ASSERT_EQ(+1, dbexec(db, "SELECT i, r, t, b FROM tab WHERE i = ? AND r = ? AND t = ? AND b = ?",
            " ifs^; ifs^;",
            40, 41.0, "42", "43", 3,
            &i, &f, &s, (const void **)&buf, &bufsiz));
        ASSERT_EQ(40, i);
        ASSERT_EQ(41.0, f);
        ASSERT_STREQ("42", s);
        ASSERT_TRUE('4' == buf[0] && '3' == buf[1] && '\0' == buf[2] && bufsiz == 3);
        ASSERT_EQ(+1, dbexec(db, "SELECT i, r, r, t FROM tab WHERE i = ? AND r = ? AND r = ? AND t = ?",
            " ulq*; ulq*;",
            40u, 41l, 41ll, Base::cstr_obj(Base::cstring("42")),
            &u, &l, &q, &str));
        ASSERT_EQ(40u, u);
        ASSERT_EQ(41l, l);
        ASSERT_EQ(41ll, q);
        ASSERT_TRUE(2 == str->length() && 0 == strcmp("42", str->chars()));
        ASSERT_EQ(+1, dbexec(db, "SELECT 1, 90 FROM tab WHERE ? IS NULL AND i = ?",
            ".i; .i",
            40, &i));
        ASSERT_EQ(90, i);
        ASSERT_EQ(+1, dbexec(db, "SELECT i, r, t, b FROM tab WHERE i = ? AND r = ? AND t = ? AND b = ?",
            " 004^ 2fs01i; 004^02fs 1i;",
            "43", 3, 41.0, "42", 40,
            (const void **)&buf, &bufsiz, &f, &s, &i));
        ASSERT_EQ(40, i);
        ASSERT_EQ(41.0, f);
        ASSERT_STREQ("42", s);
        ASSERT_TRUE('4' == buf[0] && '3' == buf[1] && '\0' == buf[2] && bufsiz == 3);
        db->release();
    }
}
TEST(DatabaseTest, StatementFormat)
{
    TestStatementFormat(false);
    TestStatementFormatV(false);
    TestStatementFormat(true);
    TestStatementFormatV(true);
}

static void TestStatements(bool threadsafe)
{
    mark_and_collect
    {
        DbKit::Database *db;
        int i;
        const char *s;
        int nrows;

        Base::Error::setLastError(0);
        db = new DbKit::Database;
        if (threadsafe)
            db->threadsafe();
        ASSERT_EQ(0, db->open());
        ASSERT_EQ(0, db->exec("CREATE TABLE tab (i INTEGER PRIMARY KEY, t TEXT)"));
        ASSERT_EQ(0, db->setStatementText(1, "INSERT INTO tab VALUES(?, ?)"));
        ASSERT_EQ(0, db->setStatementText(2, "SELECT i, t FROM tab WHERE ? <= i AND i < ?"));
        ASSERT_EQ(0, db->exec(1, "is", 40, "forty"));
        ASSERT_EQ(0, db->exec(1, "is", 41, "fortyone"));
        ASSERT_EQ(0, db->exec(1, "is", 42, "fortytwo"));
        ASSERT_EQ(0, db->exec(1, "is", 43, "fortythree"));
        ASSERT_EQ(0, db->exec(1, "is", 44, "fortyfour"));
        ASSERT_EQ(0, db->exec(1, "is", 45, "fortyfive"));
        ASSERT_EQ(0, db->exec(1, "is", 46, "fortysix"));
        ASSERT_EQ(0, db->exec(1, "is", 47, "fortyseven"));
        ASSERT_EQ(47, db->lastInsertRowid());
        ASSERT_EQ(0, db->exec(db->statementHandle(1), "is", 48, "fortyeight"));
        ASSERT_EQ(48, db->lastInsertRowid());
        ASSERT_EQ(0, db->exec(1, "is", 49, "fortynine"));

        ASSERT_EQ(0, db->params(2, "i", 42));
        ASSERT_EQ(0, db->params(2, "2i", 45));
        ASSERT_EQ(-1, db->step(2, "?"));
        ASSERT_EQ(SQLITE_MISUSE, Base::Error::lastError()->code());
        Base::Error::setLastError(0);
        nrows = 0;
        while (0 < db->step(2, "is", &i, &s))
        {
            switch (nrows)
            {
            case 0:
                ASSERT_EQ(42, i);
                ASSERT_STREQ("fortytwo", s);
                break;
            case 1:
                ASSERT_EQ(43, i);
                ASSERT_STREQ("fortythree", s);
                break;
            case 2:
                ASSERT_EQ(44, i);
                ASSERT_STREQ("fortyfour", s);
                break;
            default:
                FAIL();
                break;
            }
            nrows++;
        }
        ASSERT_EQ(3, nrows);
        ASSERT_EQ(0, Base::Error::lastError());
        nrows = 0;
        while (0 < db->step(2, "is", &i, &s))
        {
            switch (nrows)
            {
            case 0:
                ASSERT_EQ(42, i);
                ASSERT_STREQ("fortytwo", s);
                break;
            case 1:
                ASSERT_EQ(43, i);
                ASSERT_STREQ("fortythree", s);
                break;
            case 2:
                db->finish(2);
                goto end1;
            default:
                FAIL();
                break;
            }
            nrows++;
        }
    end1:
        ASSERT_EQ(2, nrows);
        ASSERT_EQ(0, Base::Error::lastError());
        nrows = 0;
        while (0 < db->step(2, "i", &i))
        {
            ASSERT_EQ(0, db->columns(2, "2s", &s));
            switch (nrows)
            {
            case 0:
                ASSERT_EQ(42, i);
                ASSERT_STREQ("fortytwo", s);
                break;
            case 1:
                ASSERT_EQ(43, i);
                ASSERT_STREQ("fortythree", s);
                break;
            case 2:
                ASSERT_EQ(44, i);
                ASSERT_STREQ("fortyfour", s);
                break;
            default:
                FAIL();
                break;
            }
            nrows++;
        }
        ASSERT_EQ(3, nrows);
        ASSERT_EQ(0, Base::Error::lastError());
        ASSERT_EQ(0, db->params(2, "ii", 45, 48));
        nrows = 0;
        while (0 < db->step(2, "is", &i, &s))
        {
            switch (nrows)
            {
            case 0:
                ASSERT_EQ(45, i);
                ASSERT_STREQ("fortyfive", s);
                break;
            case 1:
                ASSERT_EQ(46, i);
                ASSERT_STREQ("fortysix", s);
                break;
            case 2:
                ASSERT_EQ(47, i);
                ASSERT_STREQ("fortyseven", s);
                break;
            default:
                FAIL();
                break;
            }
            nrows++;
        }
        ASSERT_EQ(3, nrows);
        ASSERT_EQ(0, Base::Error::lastError());

        DbKit::Database::statement_t statement = db->statementHandle(2);
        ASSERT_EQ(0, db->params(statement, "i", 42));
        ASSERT_EQ(0, db->params(statement, "2i", 45));
        nrows = 0;
        while (0 < db->step(statement, "is", &i, &s))
        {
            switch (nrows)
            {
            case 0:
                ASSERT_EQ(42, i);
                ASSERT_STREQ("fortytwo", s);
                break;
            case 1:
                ASSERT_EQ(43, i);
                ASSERT_STREQ("fortythree", s);
                break;
            case 2:
                ASSERT_EQ(44, i);
                ASSERT_STREQ("fortyfour", s);
                break;
            default:
                FAIL();
                break;
            }
            nrows++;
        }
        ASSERT_EQ(3, nrows);
        ASSERT_EQ(0, Base::Error::lastError());
        nrows = 0;
        while (0 < db->step(statement, "is", &i, &s))
        {
            switch (nrows)
            {
            case 0:
                ASSERT_EQ(42, i);
                ASSERT_STREQ("fortytwo", s);
                break;
            case 1:
                ASSERT_EQ(43, i);
                ASSERT_STREQ("fortythree", s);
                break;
            case 2:
                db->finish(statement);
                goto end2;
            default:
                FAIL();
                break;
            }
            nrows++;
        }
    end2:
        ASSERT_EQ(2, nrows);
        ASSERT_EQ(0, Base::Error::lastError());
        nrows = 0;
        while (0 < db->step(statement, "i", &i))
        {
            ASSERT_EQ(0, db->columns(statement, "2s", &s));
            switch (nrows)
            {
            case 0:
                ASSERT_EQ(42, i);
                ASSERT_STREQ("fortytwo", s);
                break;
            case 1:
                ASSERT_EQ(43, i);
                ASSERT_STREQ("fortythree", s);
                break;
            case 2:
                ASSERT_EQ(44, i);
                ASSERT_STREQ("fortyfour", s);
                break;
            default:
                FAIL();
                break;
            }
            nrows++;
        }
        ASSERT_EQ(3, nrows);
        ASSERT_EQ(0, Base::Error::lastError());
        ASSERT_EQ(0, db->params(statement, "ii", 45, 48));
        nrows = 0;
        while (0 < db->step(statement, "is", &i, &s))
        {
            switch (nrows)
            {
            case 0:
                ASSERT_EQ(45, i);
                ASSERT_STREQ("fortyfive", s);
                break;
            case 1:
                ASSERT_EQ(46, i);
                ASSERT_STREQ("fortysix", s);
                break;
            case 2:
                ASSERT_EQ(47, i);
                ASSERT_STREQ("fortyseven", s);
                break;
            default:
                FAIL();
                break;
            }
            nrows++;
        }
        ASSERT_EQ(3, nrows);
        ASSERT_EQ(0, Base::Error::lastError());

        db->release();
    }
}
static void TestStatementsV(bool threadsafe)
{
    mark_and_collect
    {
        DbKit::Database *db;
        int i;
        const char *s;
        int nrows;

        Base::Error::setLastError(0);
        db = new DbKit::Database;
        if (threadsafe)
            db->threadsafe();
        ASSERT_EQ(0, db->open());
        ASSERT_EQ(0, dbexec(db, "CREATE TABLE tab (i INTEGER PRIMARY KEY, t TEXT)"));
        ASSERT_EQ(0, db->setStatementText(1, "INSERT INTO tab VALUES(?, ?)"));
        ASSERT_EQ(0, db->setStatementText(2, "SELECT i, t FROM tab WHERE ? <= i AND i < ?"));
        ASSERT_EQ(0, dbexec(db, 1, "is", 40, "forty"));
        ASSERT_EQ(0, dbexec(db, 1, "is", 41, "fortyone"));
        ASSERT_EQ(0, dbexec(db, 1, "is", 42, "fortytwo"));
        ASSERT_EQ(0, dbexec(db, 1, "is", 43, "fortythree"));
        ASSERT_EQ(0, dbexec(db, 1, "is", 44, "fortyfour"));
        ASSERT_EQ(0, dbexec(db, 1, "is", 45, "fortyfive"));
        ASSERT_EQ(0, dbexec(db, 1, "is", 46, "fortysix"));
        ASSERT_EQ(0, dbexec(db, 1, "is", 47, "fortyseven"));
        ASSERT_EQ(47, db->lastInsertRowid());
        ASSERT_EQ(0, dbexec(db, db->statementHandle(1), "is", 48, "fortyeight"));
        ASSERT_EQ(48, db->lastInsertRowid());
        ASSERT_EQ(0, dbexec(db, 1, "is", 49, "fortynine"));

        ASSERT_EQ(0, dbparams(db, 2, "i", 42));
        ASSERT_EQ(0, dbparams(db, 2, "2i", 45));
        ASSERT_EQ(-1, dbstep(db, 2, "?"));
        ASSERT_EQ(SQLITE_MISUSE, Base::Error::lastError()->code());
        Base::Error::setLastError(0);
        nrows = 0;
        while (0 < dbstep(db, 2, "is", &i, &s))
        {
            switch (nrows)
            {
            case 0:
                ASSERT_EQ(42, i);
                ASSERT_STREQ("fortytwo", s);
                break;
            case 1:
                ASSERT_EQ(43, i);
                ASSERT_STREQ("fortythree", s);
                break;
            case 2:
                ASSERT_EQ(44, i);
                ASSERT_STREQ("fortyfour", s);
                break;
            default:
                FAIL();
                break;
            }
            nrows++;
        }
        ASSERT_EQ(3, nrows);
        ASSERT_EQ(0, Base::Error::lastError());
        nrows = 0;
        while (0 < dbstep(db, 2, "is", &i, &s))
        {
            switch (nrows)
            {
            case 0:
                ASSERT_EQ(42, i);
                ASSERT_STREQ("fortytwo", s);
                break;
            case 1:
                ASSERT_EQ(43, i);
                ASSERT_STREQ("fortythree", s);
                break;
            case 2:
                db->finish(2);
                goto end1;
            default:
                FAIL();
                break;
            }
            nrows++;
        }
    end1:
        ASSERT_EQ(2, nrows);
        ASSERT_EQ(0, Base::Error::lastError());
        nrows = 0;
        while (0 < dbstep(db, 2, "i", &i))
        {
            ASSERT_EQ(0, dbcolumns(db, 2, "2s", &s));
            switch (nrows)
            {
            case 0:
                ASSERT_EQ(42, i);
                ASSERT_STREQ("fortytwo", s);
                break;
            case 1:
                ASSERT_EQ(43, i);
                ASSERT_STREQ("fortythree", s);
                break;
            case 2:
                ASSERT_EQ(44, i);
                ASSERT_STREQ("fortyfour", s);
                break;
            default:
                FAIL();
                break;
            }
            nrows++;
        }
        ASSERT_EQ(3, nrows);
        ASSERT_EQ(0, Base::Error::lastError());
        ASSERT_EQ(0, dbparams(db, 2, "ii", 45, 48));
        nrows = 0;
        while (0 < dbstep(db, 2, "is", &i, &s))
        {
            switch (nrows)
            {
            case 0:
                ASSERT_EQ(45, i);
                ASSERT_STREQ("fortyfive", s);
                break;
            case 1:
                ASSERT_EQ(46, i);
                ASSERT_STREQ("fortysix", s);
                break;
            case 2:
                ASSERT_EQ(47, i);
                ASSERT_STREQ("fortyseven", s);
                break;
            default:
                FAIL();
                break;
            }
            nrows++;
        }
        ASSERT_EQ(3, nrows);
        ASSERT_EQ(0, Base::Error::lastError());

        DbKit::Database::statement_t statement = db->statementHandle(2);
        ASSERT_EQ(0, dbparams(db, statement, "i", 42));
        ASSERT_EQ(0, dbparams(db, statement, "2i", 45));
        nrows = 0;
        while (0 < dbstep(db, statement, "is", &i, &s))
        {
            switch (nrows)
            {
            case 0:
                ASSERT_EQ(42, i);
                ASSERT_STREQ("fortytwo", s);
                break;
            case 1:
                ASSERT_EQ(43, i);
                ASSERT_STREQ("fortythree", s);
                break;
            case 2:
                ASSERT_EQ(44, i);
                ASSERT_STREQ("fortyfour", s);
                break;
            default:
                FAIL();
                break;
            }
            nrows++;
        }
        ASSERT_EQ(3, nrows);
        ASSERT_EQ(0, Base::Error::lastError());
        nrows = 0;
        while (0 < dbstep(db, statement, "is", &i, &s))
        {
            switch (nrows)
            {
            case 0:
                ASSERT_EQ(42, i);
                ASSERT_STREQ("fortytwo", s);
                break;
            case 1:
                ASSERT_EQ(43, i);
                ASSERT_STREQ("fortythree", s);
                break;
            case 2:
                db->finish(statement);
                goto end2;
            default:
                FAIL();
                break;
            }
            nrows++;
        }
    end2:
        ASSERT_EQ(2, nrows);
        ASSERT_EQ(0, Base::Error::lastError());
        nrows = 0;
        while (0 < dbstep(db, statement, "i", &i))
        {
            ASSERT_EQ(0, dbcolumns(db, statement, "2s", &s));
            switch (nrows)
            {
            case 0:
                ASSERT_EQ(42, i);
                ASSERT_STREQ("fortytwo", s);
                break;
            case 1:
                ASSERT_EQ(43, i);
                ASSERT_STREQ("fortythree", s);
                break;
            case 2:
                ASSERT_EQ(44, i);
                ASSERT_STREQ("fortyfour", s);
                break;
            default:
                FAIL();
                break;
            }
            nrows++;
        }
        ASSERT_EQ(3, nrows);
        ASSERT_EQ(0, Base::Error::lastError());
        ASSERT_EQ(0, dbparams(db, statement, "ii", 45, 48));
        nrows = 0;
        while (0 < dbstep(db, statement, "is", &i, &s))
        {
            switch (nrows)
            {
            case 0:
                ASSERT_EQ(45, i);
                ASSERT_STREQ("fortyfive", s);
                break;
            case 1:
                ASSERT_EQ(46, i);
                ASSERT_STREQ("fortysix", s);
                break;
            case 2:
                ASSERT_EQ(47, i);
                ASSERT_STREQ("fortyseven", s);
                break;
            default:
                FAIL();
                break;
            }
            nrows++;
        }
        ASSERT_EQ(3, nrows);
        ASSERT_EQ(0, Base::Error::lastError());

        db->release();
    }
}
TEST(DatabaseTest, Statements)
{
    TestStatements(false);
    TestStatements(true);
    TestStatementsV(false);
    TestStatementsV(true);
}
static void TestBackup(bool threadsafe)
{
    mark_and_collect
    {
        DbKit::Database *db;

        char path[L_tmpnam]; tmpnam(path);
        Base::Error::setLastError(0);
        db = new DbKit::Database;
        if (threadsafe)
            db->threadsafe();

        ASSERT_EQ(0, db->open());
        ASSERT_EQ(0, dbexec(db, "CREATE TABLE tab (i INTEGER)"));
        ASSERT_EQ(0, db->setStatementText(1, "INSERT INTO tab VALUES(?)"));
        for (int i = 0; 1000 > i; i++)
            ASSERT_EQ(0, dbexec(db, 1, "i", i + 42));

        ASSERT_EQ(0, db->backup("main", path));
        {
            DbKit::Database *destdb = new DbKit::Database(path);
            ASSERT_EQ(0, destdb->open());
            ASSERT_EQ(0, destdb->setStatementText(1, "SELECT * FROM tab"));
            int j;
            for (int i = 0; 1000 > i; i++)
            {
                ASSERT_EQ(+1, destdb->step(1, "i", &j));
                ASSERT_EQ(i + 42, j);
            }
            ASSERT_EQ(0, destdb->step(1, "i", &j));
            destdb->release();
        }

        db->exec("DELETE FROM tab");
        for (int i = 0; 1000 > i; i++)
            ASSERT_EQ(0, dbexec(db, 1, "i", -(i + 42)));

        while (0 < db->backup("main", path, 1))
            ;
        {
            DbKit::Database *destdb = new DbKit::Database(path);
            ASSERT_EQ(0, destdb->open());
            ASSERT_EQ(0, destdb->setStatementText(1, "SELECT * FROM tab"));
            int j;
            for (int i = 0; 1000 > i; i++)
            {
                ASSERT_EQ(+1, destdb->step(1, "i", &j));
                ASSERT_EQ(-(i + 42), j);
            }
            ASSERT_EQ(0, destdb->step(1, "i", &j));
            destdb->release();
        }

        db->release();
        remove(path);
    }
}
TEST(DatabaseTest, Backup)
{
    TestBackup(false);
    TestBackup(true);
}
