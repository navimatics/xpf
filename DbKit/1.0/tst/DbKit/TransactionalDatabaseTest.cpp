/*
 * DbKit/TransactionalDatabaseTest.cpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#include <DbKit/TransactionalDatabase.hpp>
#include <gtest/gtest.h>
#include <sqlite3.h>

static void TestPersistentTransactionsFragment(DbKit::TransactionalDatabase *db)
{
    ASSERT_EQ(0, db->enterPersistentTransaction("Xa"));
    ASSERT_EQ(0, db->exec("UPDATE main.tab0 SET t0 = 'hello' WHERE i0 = 1"));
    ASSERT_EQ(0, db->exec("UPDATE db1.tab0 SET t0 = 'sweet' WHERE i0 = 1"));
    ASSERT_EQ(0, db->exec("UPDATE db2.tab0 SET t0 = 'world' WHERE i0 = 1"));
    ASSERT_EQ(0, db->leavePersistentTransaction("Xa", true));
    ASSERT_EQ(-1, db->exec("UPDATE main.tab0 SET t0 = 'oops' WHERE i0 = 1"));
    ASSERT_EQ(-1, db->exec("UPDATE db1.tab0 SET t0 = 'oops' WHERE i0 = 1"));
    ASSERT_EQ(-1, db->exec("UPDATE db2.tab0 SET t0 = 'oops' WHERE i0 = 1"));
    ASSERT_EQ(0, db->enterPersistentTransaction("Xa"));
    ASSERT_EQ(0, db->exec("UPDATE db1.tab1 SET t1 = 'foo' WHERE i1 = 11"));
    ASSERT_EQ(0, db->exec("UPDATE db2.tab2 SET t2 = 'bar' WHERE i2 = 21"));
    ASSERT_EQ(0, db->leavePersistentTransaction("Xa", true));
    ASSERT_EQ(-1, db->exec("UPDATE db1.tab1 SET t1 = 'oops' WHERE i1 = 11"));
    ASSERT_EQ(-1, db->exec("UPDATE db2.tab2 SET t2 = 'oops' WHERE i2 = 21"));
    ASSERT_EQ(0, db->enterPersistentTransaction("Xa"));
    ASSERT_EQ(0, db->exec("UPDATE db1.tab1 SET t1 = 'foo' WHERE i1 = 12"));
    ASSERT_EQ(0, db->exec("UPDATE db2.tab2 SET t2 = 'bar' WHERE i2 = 22"));
    ASSERT_EQ(-1, db->leavePersistentTransaction("Xa", false));
    ASSERT_EQ(0, db->exec("UPDATE db1.tab1 SET t1 = 'FOO' WHERE i1 = 12"));
    ASSERT_EQ(0, db->exec("UPDATE db2.tab2 SET t2 = 'BAR' WHERE i2 = 22"));
}
static void TestPersistentTransactions(bool threadsafe)
{
    mark_and_collect
    {
        DbKit::TransactionalDatabase *db;

        Base::Error::setLastError(0);
        //Base::Error::setLogger(Base::Error::standardLogger);
        db = new DbKit::TransactionalDatabase;
        if (threadsafe)
            db->threadsafe();
        ASSERT_EQ(0, db->open());
        ASSERT_EQ(0, db->exec("ATTACH ':memory:' AS db1"));
        ASSERT_EQ(0, db->exec("ATTACH ':memory:' AS db2"));
        ASSERT_EQ(0, db->exec("CREATE TABLE main.tab0 (i0 INTEGER PRIMARY KEY, t0 TEXT)"));
        ASSERT_EQ(0, db->exec("CREATE TABLE db1.tab0 (i0 INTEGER PRIMARY KEY, t0 TEXT)"));
        ASSERT_EQ(0, db->exec("CREATE TABLE db1.tab1 (i1 INTEGER PRIMARY KEY, t1 TEXT)"));
        ASSERT_EQ(0, db->exec("CREATE TABLE db2.tab0 (i0 INTEGER PRIMARY KEY, t0 TEXT)"));
        ASSERT_EQ(0, db->exec("CREATE TABLE db2.tab2 (i2 INTEGER PRIMARY KEY, t2 TEXT)"));
        ASSERT_EQ(0, db->exec("INSERT INTO main.tab0 VALUES (01, '01')"));
        ASSERT_EQ(0, db->exec("INSERT INTO main.tab0 VALUES (02, '02')"));
        ASSERT_EQ(0, db->exec("INSERT INTO main.tab0 VALUES (03, '03')"));
        ASSERT_EQ(0, db->exec("INSERT INTO main.tab0 VALUES (04, '04')"));
        ASSERT_EQ(0, db->exec("INSERT INTO main.tab0 VALUES (05, '05')"));
        ASSERT_EQ(0, db->exec("INSERT INTO db1.tab0 VALUES (01, 'db1.01')"));
        ASSERT_EQ(0, db->exec("INSERT INTO db1.tab0 VALUES (02, 'db1.02')"));
        ASSERT_EQ(0, db->exec("INSERT INTO db1.tab1 VALUES (11, 'db1.11')"));
        ASSERT_EQ(0, db->exec("INSERT INTO db1.tab1 VALUES (12, 'db1.12')"));
        ASSERT_EQ(0, db->exec("INSERT INTO db2.tab0 VALUES (01, 'db2.01')"));
        ASSERT_EQ(0, db->exec("INSERT INTO db2.tab0 VALUES (02, 'db2.02')"));
        ASSERT_EQ(0, db->exec("INSERT INTO db2.tab2 VALUES (21, 'db2.21')"));
        ASSERT_EQ(0, db->exec("INSERT INTO db2.tab2 VALUES (22, 'db2.22')"));

        ASSERT_EQ(0, db->beginPersistentTransaction("Xa", true));
        TestPersistentTransactionsFragment(db);
        ASSERT_EQ(0, db->endPersistentTransaction("Xa", false));

        int count;
        const char *text;
        ASSERT_EQ(1, db->exec("SELECT COUNT(*) FROM main.tab0", ";i", &count));
        ASSERT_EQ(5, count);
        ASSERT_EQ(1, db->exec("SELECT COUNT(*) FROM db1.tab0", ";i", &count));
        ASSERT_EQ(2, count);
        ASSERT_EQ(1, db->exec("SELECT COUNT(*) FROM db1.tab1", ";i", &count));
        ASSERT_EQ(2, count);
        ASSERT_EQ(1, db->exec("SELECT COUNT(*) FROM db2.tab0", ";i", &count));
        ASSERT_EQ(2, count);
        ASSERT_EQ(1, db->exec("SELECT COUNT(*) FROM db2.tab2", ";i", &count));
        ASSERT_EQ(2, count);
        ASSERT_EQ(1, db->exec("SELECT t0 FROM main.tab0 WHERE i0 = 1", ";s", &text));
        ASSERT_STREQ("01", text);
        ASSERT_EQ(1, db->exec("SELECT t0 FROM main.tab0 WHERE i0 = 2", ";s", &text));
        ASSERT_STREQ("02", text);
        ASSERT_EQ(1, db->exec("SELECT t0 FROM main.tab0 WHERE i0 = 3", ";s", &text));
        ASSERT_STREQ("03", text);
        ASSERT_EQ(1, db->exec("SELECT t0 FROM main.tab0 WHERE i0 = 4", ";s", &text));
        ASSERT_STREQ("04", text);
        ASSERT_EQ(1, db->exec("SELECT t0 FROM main.tab0 WHERE i0 = 5", ";s", &text));
        ASSERT_STREQ("05", text);
        ASSERT_EQ(1, db->exec("SELECT t0 FROM db1.tab0 WHERE i0 = 1", ";s", &text));
        ASSERT_STREQ("db1.01", text);
        ASSERT_EQ(1, db->exec("SELECT t0 FROM db1.tab0 WHERE i0 = 2", ";s", &text));
        ASSERT_STREQ("db1.02", text);
        ASSERT_EQ(1, db->exec("SELECT t1 FROM db1.tab1 WHERE i1 = 11", ";s", &text));
        ASSERT_STREQ("db1.11", text);
        ASSERT_EQ(1, db->exec("SELECT t1 FROM db1.tab1 WHERE i1 = 12", ";s", &text));
        ASSERT_STREQ("FOO", text);
        ASSERT_EQ(1, db->exec("SELECT t0 FROM db2.tab0 WHERE i0 = 1", ";s", &text));
        ASSERT_STREQ("db2.01", text);
        ASSERT_EQ(1, db->exec("SELECT t0 FROM db2.tab0 WHERE i0 = 2", ";s", &text));
        ASSERT_STREQ("db2.02", text);
        ASSERT_EQ(1, db->exec("SELECT t2 FROM db2.tab2 WHERE i2 = 21", ";s", &text));
        ASSERT_STREQ("db2.21", text);
        ASSERT_EQ(1, db->exec("SELECT t2 FROM db2.tab2 WHERE i2 = 22", ";s", &text));
        ASSERT_STREQ("BAR", text);

        ASSERT_EQ(0, db->exec("UPDATE main.tab0 SET t0 = 'cando' WHERE i0 = 1"));
        ASSERT_EQ(0, db->exec("UPDATE db1.tab0 SET t0 = 'cando' WHERE i0 = 1"));
        ASSERT_EQ(0, db->exec("UPDATE db2.tab0 SET t0 = 'cando' WHERE i0 = 1"));
        ASSERT_EQ(1, db->exec("SELECT t0 FROM main.tab0 WHERE i0 = 1", ";s", &text));
        ASSERT_STREQ("cando", text);
        ASSERT_EQ(1, db->exec("SELECT t0 FROM db1.tab0 WHERE i0 = 1", ";s", &text));
        ASSERT_STREQ("cando", text);
        ASSERT_EQ(1, db->exec("SELECT t0 FROM db2.tab0 WHERE i0 = 1", ";s", &text));
        ASSERT_STREQ("cando", text);

        ASSERT_EQ(0, db->beginPersistentTransaction("Xa", true));
        TestPersistentTransactionsFragment(db);
        ASSERT_EQ(0, db->endPersistentTransaction("Xa", true));

        ASSERT_EQ(1, db->exec("SELECT COUNT(*) FROM main.tab0", ";i", &count));
        ASSERT_EQ(5, count);
        ASSERT_EQ(1, db->exec("SELECT COUNT(*) FROM db1.tab0", ";i", &count));
        ASSERT_EQ(2, count);
        ASSERT_EQ(1, db->exec("SELECT COUNT(*) FROM db1.tab1", ";i", &count));
        ASSERT_EQ(2, count);
        ASSERT_EQ(1, db->exec("SELECT COUNT(*) FROM db2.tab0", ";i", &count));
        ASSERT_EQ(2, count);
        ASSERT_EQ(1, db->exec("SELECT COUNT(*) FROM db2.tab2", ";i", &count));
        ASSERT_EQ(2, count);
        ASSERT_EQ(1, db->exec("SELECT t0 FROM main.tab0 WHERE i0 = 1", ";s", &text));
        ASSERT_STREQ("hello", text);
        ASSERT_EQ(1, db->exec("SELECT t0 FROM main.tab0 WHERE i0 = 2", ";s", &text));
        ASSERT_STREQ("02", text);
        ASSERT_EQ(1, db->exec("SELECT t0 FROM main.tab0 WHERE i0 = 3", ";s", &text));
        ASSERT_STREQ("03", text);
        ASSERT_EQ(1, db->exec("SELECT t0 FROM main.tab0 WHERE i0 = 4", ";s", &text));
        ASSERT_STREQ("04", text);
        ASSERT_EQ(1, db->exec("SELECT t0 FROM main.tab0 WHERE i0 = 5", ";s", &text));
        ASSERT_STREQ("05", text);
        ASSERT_EQ(1, db->exec("SELECT t0 FROM db1.tab0 WHERE i0 = 1", ";s", &text));
        ASSERT_STREQ("sweet", text);
        ASSERT_EQ(1, db->exec("SELECT t0 FROM db1.tab0 WHERE i0 = 2", ";s", &text));
        ASSERT_STREQ("db1.02", text);
        ASSERT_EQ(1, db->exec("SELECT t1 FROM db1.tab1 WHERE i1 = 11", ";s", &text));
        ASSERT_STREQ("foo", text);
        ASSERT_EQ(1, db->exec("SELECT t1 FROM db1.tab1 WHERE i1 = 12", ";s", &text));
        ASSERT_STREQ("FOO", text);
        ASSERT_EQ(1, db->exec("SELECT t0 FROM db2.tab0 WHERE i0 = 1", ";s", &text));
        ASSERT_STREQ("world", text);
        ASSERT_EQ(1, db->exec("SELECT t0 FROM db2.tab0 WHERE i0 = 2", ";s", &text));
        ASSERT_STREQ("db2.02", text);
        ASSERT_EQ(1, db->exec("SELECT t2 FROM db2.tab2 WHERE i2 = 21", ";s", &text));
        ASSERT_STREQ("bar", text);
        ASSERT_EQ(1, db->exec("SELECT t2 FROM db2.tab2 WHERE i2 = 22", ";s", &text));
        ASSERT_STREQ("BAR", text);

        db->release();
    }
}
static void PersistentTransactionsSingleAttachedDb(bool threadsafe)
{
    mark_and_collect
    {
        DbKit::TransactionalDatabase *db;
        
        Base::Error::setLastError(0);
        //Base::Error::setLogger(Base::Error::standardLogger);
        db = new DbKit::TransactionalDatabase;
        if (threadsafe)
            db->threadsafe();
        ASSERT_EQ(0, db->open());
        ASSERT_EQ(0, db->exec("ATTACH ':memory:' AS db1"));
        ASSERT_EQ(0, db->exec("ATTACH ':memory:' AS db2"));
        ASSERT_EQ(0, db->exec("CREATE TABLE main.tab0 (i0 INTEGER PRIMARY KEY, t0 TEXT)"));
        ASSERT_EQ(0, db->exec("CREATE TABLE db1.tab0 (i0 INTEGER PRIMARY KEY, t0 TEXT)"));
        ASSERT_EQ(0, db->exec("CREATE TABLE db1.tab1 (i1 INTEGER PRIMARY KEY, t1 TEXT)"));
        ASSERT_EQ(0, db->exec("CREATE TABLE db2.tab0 (i0 INTEGER PRIMARY KEY, t0 TEXT)"));
        ASSERT_EQ(0, db->exec("CREATE TABLE db2.tab2 (i2 INTEGER PRIMARY KEY, t2 TEXT)"));
        ASSERT_EQ(0, db->exec("INSERT INTO main.tab0 VALUES (01, '01')"));
        ASSERT_EQ(0, db->exec("INSERT INTO main.tab0 VALUES (02, '02')"));
        ASSERT_EQ(0, db->exec("INSERT INTO main.tab0 VALUES (03, '03')"));
        ASSERT_EQ(0, db->exec("INSERT INTO main.tab0 VALUES (04, '04')"));
        ASSERT_EQ(0, db->exec("INSERT INTO main.tab0 VALUES (05, '05')"));
        ASSERT_EQ(0, db->exec("INSERT INTO db1.tab0 VALUES (01, 'db1.01')"));
        ASSERT_EQ(0, db->exec("INSERT INTO db1.tab0 VALUES (02, 'db1.02')"));
        ASSERT_EQ(0, db->exec("INSERT INTO db1.tab1 VALUES (11, 'db1.11')"));
        ASSERT_EQ(0, db->exec("INSERT INTO db1.tab1 VALUES (12, 'db1.12')"));
        ASSERT_EQ(0, db->exec("INSERT INTO db2.tab0 VALUES (01, 'db2.01')"));
        ASSERT_EQ(0, db->exec("INSERT INTO db2.tab0 VALUES (02, 'db2.02')"));
        ASSERT_EQ(0, db->exec("INSERT INTO db2.tab2 VALUES (21, 'db2.21')"));
        ASSERT_EQ(0, db->exec("INSERT INTO db2.tab2 VALUES (22, 'db2.22')"));

        const char *tabnames[] =
        {
            "db1.*"
        };
        ASSERT_EQ(0, db->beginPersistentTransaction("Xa", true, tabnames, NELEMS(tabnames)));
        ASSERT_EQ(0, db->enterPersistentTransaction("Xa"));
        ASSERT_EQ(0, db->exec("UPDATE main.tab0 SET t0 = 'hello' WHERE i0 = 1"));
        ASSERT_EQ(0, db->exec("UPDATE db1.tab0 SET t0 = 'sweet' WHERE i0 = 1"));
        ASSERT_EQ(0, db->exec("UPDATE db2.tab0 SET t0 = 'world' WHERE i0 = 1"));
        ASSERT_EQ(0, db->leavePersistentTransaction("Xa", true));
        ASSERT_EQ(0, db->exec("UPDATE main.tab0 SET t0 = 'hello2' WHERE i0 = 1"));
        ASSERT_EQ(-1, db->exec("UPDATE db1.tab0 SET t0 = 'oops' WHERE i0 = 1"));
        ASSERT_EQ(0, db->exec("UPDATE db2.tab0 SET t0 = 'world2' WHERE i0 = 1"));
        ASSERT_EQ(0, db->enterPersistentTransaction("Xa"));
        ASSERT_EQ(0, db->exec("UPDATE db1.tab1 SET t1 = 'foo' WHERE i1 = 11"));
        ASSERT_EQ(0, db->exec("UPDATE db2.tab2 SET t2 = 'bar' WHERE i2 = 21"));
        ASSERT_EQ(0, db->leavePersistentTransaction("Xa", true));
        ASSERT_EQ(-1, db->exec("UPDATE db1.tab1 SET t1 = 'oops' WHERE i1 = 11"));
        ASSERT_EQ(0, db->exec("UPDATE db2.tab2 SET t2 = 'bar2' WHERE i2 = 21"));
        ASSERT_EQ(0, db->enterPersistentTransaction("Xa"));
        ASSERT_EQ(0, db->exec("UPDATE db1.tab1 SET t1 = 'foo' WHERE i1 = 12"));
        ASSERT_EQ(0, db->exec("UPDATE db2.tab2 SET t2 = 'bar' WHERE i2 = 22"));
        ASSERT_EQ(-1, db->leavePersistentTransaction("Xa", false));
        ASSERT_EQ(0, db->exec("UPDATE db1.tab1 SET t1 = 'FOO' WHERE i1 = 12"));
        ASSERT_EQ(0, db->exec("UPDATE db2.tab2 SET t2 = 'BAR' WHERE i2 = 22"));
        ASSERT_EQ(0, db->endPersistentTransaction("Xa", true));
        
        int count;
        const char *text;
        ASSERT_EQ(1, db->exec("SELECT COUNT(*) FROM main.tab0", ";i", &count));
        ASSERT_EQ(5, count);
        ASSERT_EQ(1, db->exec("SELECT COUNT(*) FROM db1.tab0", ";i", &count));
        ASSERT_EQ(2, count);
        ASSERT_EQ(1, db->exec("SELECT COUNT(*) FROM db1.tab1", ";i", &count));
        ASSERT_EQ(2, count);
        ASSERT_EQ(1, db->exec("SELECT COUNT(*) FROM db2.tab0", ";i", &count));
        ASSERT_EQ(2, count);
        ASSERT_EQ(1, db->exec("SELECT COUNT(*) FROM db2.tab2", ";i", &count));
        ASSERT_EQ(2, count);
        ASSERT_EQ(1, db->exec("SELECT t0 FROM main.tab0 WHERE i0 = 1", ";s", &text));
        ASSERT_STREQ("hello2", text);
        ASSERT_EQ(1, db->exec("SELECT t0 FROM main.tab0 WHERE i0 = 2", ";s", &text));
        ASSERT_STREQ("02", text);
        ASSERT_EQ(1, db->exec("SELECT t0 FROM main.tab0 WHERE i0 = 3", ";s", &text));
        ASSERT_STREQ("03", text);
        ASSERT_EQ(1, db->exec("SELECT t0 FROM main.tab0 WHERE i0 = 4", ";s", &text));
        ASSERT_STREQ("04", text);
        ASSERT_EQ(1, db->exec("SELECT t0 FROM main.tab0 WHERE i0 = 5", ";s", &text));
        ASSERT_STREQ("05", text);
        ASSERT_EQ(1, db->exec("SELECT t0 FROM db1.tab0 WHERE i0 = 1", ";s", &text));
        ASSERT_STREQ("sweet", text);
        ASSERT_EQ(1, db->exec("SELECT t0 FROM db1.tab0 WHERE i0 = 2", ";s", &text));
        ASSERT_STREQ("db1.02", text);
        ASSERT_EQ(1, db->exec("SELECT t1 FROM db1.tab1 WHERE i1 = 11", ";s", &text));
        ASSERT_STREQ("foo", text);
        ASSERT_EQ(1, db->exec("SELECT t1 FROM db1.tab1 WHERE i1 = 12", ";s", &text));
        ASSERT_STREQ("FOO", text);
        ASSERT_EQ(1, db->exec("SELECT t0 FROM db2.tab0 WHERE i0 = 1", ";s", &text));
        ASSERT_STREQ("world2", text);
        ASSERT_EQ(1, db->exec("SELECT t0 FROM db2.tab0 WHERE i0 = 2", ";s", &text));
        ASSERT_STREQ("db2.02", text);
        ASSERT_EQ(1, db->exec("SELECT t2 FROM db2.tab2 WHERE i2 = 21", ";s", &text));
        ASSERT_STREQ("bar2", text);
        ASSERT_EQ(1, db->exec("SELECT t2 FROM db2.tab2 WHERE i2 = 22", ";s", &text));
        ASSERT_STREQ("BAR", text);
        
        db->release();
    }
}
static void TestPersistentTransactionsRollbackAll(bool threadsafe)
{
    mark_and_collect
    {
        DbKit::TransactionalDatabase *db;
        
        Base::Error::setLastError(0);
        //Base::Error::setLogger(Base::Error::standardLogger);
        db = new DbKit::TransactionalDatabase;
        if (threadsafe)
            db->threadsafe();
        ASSERT_EQ(0, db->open());
        ASSERT_EQ(0, db->exec("ATTACH ':memory:' AS db1"));
        ASSERT_EQ(0, db->exec("ATTACH ':memory:' AS db2"));
        ASSERT_EQ(0, db->exec("CREATE TABLE main.tab0 (i0 INTEGER PRIMARY KEY, t0 TEXT)"));
        ASSERT_EQ(0, db->exec("CREATE TABLE db1.tab0 (i0 INTEGER PRIMARY KEY, t0 TEXT)"));
        ASSERT_EQ(0, db->exec("CREATE TABLE db1.tab1 (i1 INTEGER PRIMARY KEY, t1 TEXT)"));
        ASSERT_EQ(0, db->exec("CREATE TABLE db2.tab0 (i0 INTEGER PRIMARY KEY, t0 TEXT)"));
        ASSERT_EQ(0, db->exec("CREATE TABLE db2.tab2 (i2 INTEGER PRIMARY KEY, t2 TEXT)"));
        ASSERT_EQ(0, db->exec("INSERT INTO main.tab0 VALUES (01, '01')"));
        ASSERT_EQ(0, db->exec("INSERT INTO main.tab0 VALUES (02, '02')"));
        ASSERT_EQ(0, db->exec("INSERT INTO main.tab0 VALUES (03, '03')"));
        ASSERT_EQ(0, db->exec("INSERT INTO main.tab0 VALUES (04, '04')"));
        ASSERT_EQ(0, db->exec("INSERT INTO main.tab0 VALUES (05, '05')"));
        ASSERT_EQ(0, db->exec("INSERT INTO db1.tab0 VALUES (01, 'db1.01')"));
        ASSERT_EQ(0, db->exec("INSERT INTO db1.tab0 VALUES (02, 'db1.02')"));
        ASSERT_EQ(0, db->exec("INSERT INTO db1.tab1 VALUES (11, 'db1.11')"));
        ASSERT_EQ(0, db->exec("INSERT INTO db1.tab1 VALUES (12, 'db1.12')"));
        ASSERT_EQ(0, db->exec("INSERT INTO db2.tab0 VALUES (01, 'db2.01')"));
        ASSERT_EQ(0, db->exec("INSERT INTO db2.tab0 VALUES (02, 'db2.02')"));
        ASSERT_EQ(0, db->exec("INSERT INTO db2.tab2 VALUES (21, 'db2.21')"));
        ASSERT_EQ(0, db->exec("INSERT INTO db2.tab2 VALUES (22, 'db2.22')"));
        
        ASSERT_EQ(0, db->beginPersistentTransaction("Xa", true));
        TestPersistentTransactionsFragment(db);

        ASSERT_EQ(0, db->beginPersistentTransaction("Xb", true));
        ASSERT_EQ(0, db->enterPersistentTransaction("Xb"));
        ASSERT_EQ(0, db->exec("UPDATE db1.tab1 SET t1 = 'zzzFOO' WHERE i1 = 12"));
        ASSERT_EQ(0, db->exec("UPDATE db2.tab2 SET t2 = 'zzzBAR' WHERE i2 = 22"));
        ASSERT_EQ(0, db->exec("UPDATE db1.tab1 SET t1 = 'yyyFOO' WHERE i1 = 12"));
        ASSERT_EQ(0, db->exec("UPDATE db2.tab2 SET t2 = 'yyyBAR' WHERE i2 = 22"));
        ASSERT_EQ(0, db->exec("UPDATE db1.tab1 SET t1 = 'xxxFOO' WHERE i1 = 12"));
        ASSERT_EQ(0, db->exec("UPDATE db2.tab2 SET t2 = 'xxxBAR' WHERE i2 = 22"));
        ASSERT_EQ(0, db->exec("DELETE FROM db1.tab1 WHERE i1 = 12"));
        ASSERT_EQ(0, db->leavePersistentTransaction("Xb", true));
        
        ASSERT_EQ(0, db->enterPersistentTransaction("Xa"));
        ASSERT_EQ(0, db->exec("UPDATE main.tab0 SET t0 = 'h' WHERE i0 = 1"));
        ASSERT_EQ(0, db->exec("UPDATE db1.tab0 SET t0 = 's' WHERE i0 = 1"));
        ASSERT_EQ(0, db->exec("UPDATE db2.tab0 SET t0 = 'w' WHERE i0 = 1"));
        ASSERT_EQ(0, db->exec("INSERT INTO db2.tab0 VALUES (23, 'oops')"));
        ASSERT_EQ(0, db->exec("INSERT INTO db2.tab0 VALUES (24, 'oops')"));
        ASSERT_EQ(0, db->exec("UPDATE db2.tab0 SET t0 = 'double oops' WHERE i0 = 24"));
        ASSERT_EQ(0, db->leavePersistentTransaction("Xa", true));

        ASSERT_EQ(0, db->rollbackAllPersistentTransactions());

        int count;
        const char *text;
        ASSERT_EQ(1, db->exec("SELECT COUNT(*) FROM main.tab0", ";i", &count));
        ASSERT_EQ(5, count);
        ASSERT_EQ(1, db->exec("SELECT COUNT(*) FROM db1.tab0", ";i", &count));
        ASSERT_EQ(2, count);
        ASSERT_EQ(1, db->exec("SELECT COUNT(*) FROM db1.tab1", ";i", &count));
        ASSERT_EQ(2, count);
        ASSERT_EQ(1, db->exec("SELECT COUNT(*) FROM db2.tab0", ";i", &count));
        ASSERT_EQ(2, count);
        ASSERT_EQ(1, db->exec("SELECT COUNT(*) FROM db2.tab2", ";i", &count));
        ASSERT_EQ(2, count);
        ASSERT_EQ(1, db->exec("SELECT t0 FROM main.tab0 WHERE i0 = 1", ";s", &text));
        ASSERT_STREQ("01", text);
        ASSERT_EQ(1, db->exec("SELECT t0 FROM main.tab0 WHERE i0 = 2", ";s", &text));
        ASSERT_STREQ("02", text);
        ASSERT_EQ(1, db->exec("SELECT t0 FROM main.tab0 WHERE i0 = 3", ";s", &text));
        ASSERT_STREQ("03", text);
        ASSERT_EQ(1, db->exec("SELECT t0 FROM main.tab0 WHERE i0 = 4", ";s", &text));
        ASSERT_STREQ("04", text);
        ASSERT_EQ(1, db->exec("SELECT t0 FROM main.tab0 WHERE i0 = 5", ";s", &text));
        ASSERT_STREQ("05", text);
        ASSERT_EQ(1, db->exec("SELECT t0 FROM db1.tab0 WHERE i0 = 1", ";s", &text));
        ASSERT_STREQ("db1.01", text);
        ASSERT_EQ(1, db->exec("SELECT t0 FROM db1.tab0 WHERE i0 = 2", ";s", &text));
        ASSERT_STREQ("db1.02", text);
        ASSERT_EQ(1, db->exec("SELECT t1 FROM db1.tab1 WHERE i1 = 11", ";s", &text));
        ASSERT_STREQ("db1.11", text);
        ASSERT_EQ(1, db->exec("SELECT t1 FROM db1.tab1 WHERE i1 = 12", ";s", &text));
        ASSERT_STREQ("FOO", text);
        ASSERT_EQ(1, db->exec("SELECT t0 FROM db2.tab0 WHERE i0 = 1", ";s", &text));
        ASSERT_STREQ("db2.01", text);
        ASSERT_EQ(1, db->exec("SELECT t0 FROM db2.tab0 WHERE i0 = 2", ";s", &text));
        ASSERT_STREQ("db2.02", text);
        ASSERT_EQ(1, db->exec("SELECT t2 FROM db2.tab2 WHERE i2 = 21", ";s", &text));
        ASSERT_STREQ("db2.21", text);
        ASSERT_EQ(1, db->exec("SELECT t2 FROM db2.tab2 WHERE i2 = 22", ";s", &text));
        ASSERT_STREQ("BAR", text);
        
        const char *tabnames[] =
        {
            "db1.*"
        };
        ASSERT_EQ(0, db->beginPersistentTransaction("Xa", true, tabnames, NELEMS(tabnames)));
        ASSERT_EQ(0, db->enterPersistentTransaction("Xa"));
        ASSERT_EQ(0, db->exec("UPDATE main.tab0 SET t0 = 'hello' WHERE i0 = 1"));
        ASSERT_EQ(0, db->exec("UPDATE db1.tab0 SET t0 = 'sweet' WHERE i0 = 1"));
        ASSERT_EQ(0, db->exec("UPDATE db2.tab0 SET t0 = 'world' WHERE i0 = 1"));
        ASSERT_EQ(0, db->leavePersistentTransaction("Xa", true));
        ASSERT_EQ(0, db->exec("UPDATE main.tab0 SET t0 = 'hello2' WHERE i0 = 1"));
        ASSERT_EQ(-1, db->exec("UPDATE db1.tab0 SET t0 = 'oops' WHERE i0 = 1"));
        ASSERT_EQ(0, db->exec("UPDATE db2.tab0 SET t0 = 'world2' WHERE i0 = 1"));
        ASSERT_EQ(0, db->enterPersistentTransaction("Xa"));
        ASSERT_EQ(0, db->exec("UPDATE db1.tab1 SET t1 = 'foo' WHERE i1 = 11"));
        ASSERT_EQ(0, db->exec("UPDATE db2.tab2 SET t2 = 'bar' WHERE i2 = 21"));
        ASSERT_EQ(0, db->leavePersistentTransaction("Xa", true));
        ASSERT_EQ(-1, db->exec("UPDATE db1.tab1 SET t1 = 'oops' WHERE i1 = 11"));
        ASSERT_EQ(0, db->exec("UPDATE db2.tab2 SET t2 = 'bar2' WHERE i2 = 21"));
        ASSERT_EQ(0, db->enterPersistentTransaction("Xa"));
        ASSERT_EQ(0, db->exec("UPDATE db1.tab1 SET t1 = 'foo' WHERE i1 = 12"));
        ASSERT_EQ(0, db->exec("UPDATE db2.tab2 SET t2 = 'bar' WHERE i2 = 22"));
        ASSERT_EQ(-1, db->leavePersistentTransaction("Xa", false));
        ASSERT_EQ(0, db->exec("UPDATE db1.tab1 SET t1 = 'FOO' WHERE i1 = 12"));
        ASSERT_EQ(0, db->exec("UPDATE db2.tab2 SET t2 = 'BAR' WHERE i2 = 22"));

        ASSERT_EQ(0, db->rollbackAllPersistentTransactions());
        
        ASSERT_EQ(1, db->exec("SELECT COUNT(*) FROM main.tab0", ";i", &count));
        ASSERT_EQ(5, count);
        ASSERT_EQ(1, db->exec("SELECT COUNT(*) FROM db1.tab0", ";i", &count));
        ASSERT_EQ(2, count);
        ASSERT_EQ(1, db->exec("SELECT COUNT(*) FROM db1.tab1", ";i", &count));
        ASSERT_EQ(2, count);
        ASSERT_EQ(1, db->exec("SELECT COUNT(*) FROM db2.tab0", ";i", &count));
        ASSERT_EQ(2, count);
        ASSERT_EQ(1, db->exec("SELECT COUNT(*) FROM db2.tab2", ";i", &count));
        ASSERT_EQ(2, count);
        ASSERT_EQ(1, db->exec("SELECT t0 FROM main.tab0 WHERE i0 = 1", ";s", &text));
        ASSERT_STREQ("hello2", text);
        ASSERT_EQ(1, db->exec("SELECT t0 FROM main.tab0 WHERE i0 = 2", ";s", &text));
        ASSERT_STREQ("02", text);
        ASSERT_EQ(1, db->exec("SELECT t0 FROM main.tab0 WHERE i0 = 3", ";s", &text));
        ASSERT_STREQ("03", text);
        ASSERT_EQ(1, db->exec("SELECT t0 FROM main.tab0 WHERE i0 = 4", ";s", &text));
        ASSERT_STREQ("04", text);
        ASSERT_EQ(1, db->exec("SELECT t0 FROM main.tab0 WHERE i0 = 5", ";s", &text));
        ASSERT_STREQ("05", text);
        ASSERT_EQ(1, db->exec("SELECT t0 FROM db1.tab0 WHERE i0 = 1", ";s", &text));
        ASSERT_STREQ("db1.01", text);
        ASSERT_EQ(1, db->exec("SELECT t0 FROM db1.tab0 WHERE i0 = 2", ";s", &text));
        ASSERT_STREQ("db1.02", text);
        ASSERT_EQ(1, db->exec("SELECT t1 FROM db1.tab1 WHERE i1 = 11", ";s", &text));
        ASSERT_STREQ("db1.11", text);
        ASSERT_EQ(1, db->exec("SELECT t1 FROM db1.tab1 WHERE i1 = 12", ";s", &text));
        ASSERT_STREQ("FOO", text);
        ASSERT_EQ(1, db->exec("SELECT t0 FROM db2.tab0 WHERE i0 = 1", ";s", &text));
        ASSERT_STREQ("world2", text);
        ASSERT_EQ(1, db->exec("SELECT t0 FROM db2.tab0 WHERE i0 = 2", ";s", &text));
        ASSERT_STREQ("db2.02", text);
        ASSERT_EQ(1, db->exec("SELECT t2 FROM db2.tab2 WHERE i2 = 21", ";s", &text));
        ASSERT_STREQ("bar2", text);
        ASSERT_EQ(1, db->exec("SELECT t2 FROM db2.tab2 WHERE i2 = 22", ";s", &text));
        ASSERT_STREQ("BAR", text);
        
        db->release();
    }
}
TEST(TransactionalDatabaseTest, PersistentTransactions)
{
    TestPersistentTransactions(false);
    TestPersistentTransactions(true);
}
TEST(TransactionalDatabaseTest, PersistentTransactionsSingleAttachedDb)
{
    PersistentTransactionsSingleAttachedDb(false);
    PersistentTransactionsSingleAttachedDb(true);
}
TEST(TransactionalDatabaseTest, PersistentTransactionsRollbackAll)
{
    TestPersistentTransactionsRollbackAll(false);
    TestPersistentTransactionsRollbackAll(true);
}
TEST(TransactionalDatabaseTest, PersistentTransactionsRandomUpdatesRollback)
{
    mark_and_collect
    {
        DbKit::TransactionalDatabase *db = new (Base::collect) DbKit::TransactionalDatabase;
        //Base::Error::setLogger(Base::Error::standardLogger);
        ASSERT_EQ(0, db->open());
        ASSERT_EQ(0, db->exec("CREATE TABLE tab (i INTEGER PRIMARY KEY, r INTEGER)"));

        long count;
        const size_t n = 1000;
        unsigned long r[n];
        for (size_t i = 0; i < n; i++)
            r[i] = Base::Random::integerValue(0, 1000000000);

        for (size_t i = 0; i < n; i++)
            ASSERT_EQ(0, db->exec("INSERT INTO tab VALUES (?, ?)", "ll", (long)i, r[i]));
        for (size_t i = 0; i < n; i++)
            ASSERT_EQ(0, db->exec("INSERT INTO tab VALUES (?, ?)", "ll", (long)i + n, r[i]));
        ASSERT_EQ(0, db->beginPersistentTransaction("X", true));
        ASSERT_EQ(0, db->enterPersistentTransaction("X"));
        for (size_t i = 0; i < n; i++)
            ASSERT_EQ(0, db->exec("INSERT INTO tab VALUES (?, ?)", "ll", (long)i + 2 * n, Base::Random::integerValue(0, 1000000000)));
        for (size_t i = 0; i < n; i++)
            ASSERT_EQ(0, db->exec("UPDATE tab SET r = ? WHERE i = ?", "ll", Base::Random::integerValue(0, 1000000000), (long)i));
        for (size_t i = 0; i < n; i++)
            ASSERT_EQ(0, db->exec("DELETE FROM tab WHERE i = ?", "l", (long)i + n));
        for (size_t i = 0; i < n; i++)
            ASSERT_EQ(0, db->exec("UPDATE tab SET r = ? WHERE i = ?", "ll", Base::Random::integerValue(0, 1000000000), (long)i));
        for (size_t i = 0; i < n; i++)
            ASSERT_EQ(0, db->exec("REPLACE INTO tab VALUES (?, ?)", "ll", (long)i + n, Base::Random::integerValue(0, 1000000000)));
        for (size_t i = 0; i < n; i++)
            ASSERT_EQ(0, db->exec("UPDATE tab SET r = ? WHERE i = ?", "ll", Base::Random::integerValue(0, 1000000000), Base::Random::integerValue(0, n * 3)));
        ASSERT_EQ(0, db->leavePersistentTransaction("X", true));
        ASSERT_EQ(1, db->exec("SELECT count(*) FROM tab", ";l", &count));
        ASSERT_EQ(n * 3, count);
        ASSERT_EQ(0, db->endPersistentTransaction("X", false));

        ASSERT_EQ(1, db->exec("SELECT count(*) FROM tab", ";l", &count));
        ASSERT_EQ(n * 2, count);
        for (size_t i = 0; i < n; i++)
        {
            long num;
            ASSERT_EQ(1, db->exec("SELECT r FROM tab WHERE i = ?", "l;l", (long)i, &num));
            ASSERT_EQ(r[i], num);
        }
        for (size_t i = 0; i < n; i++)
        {
            long num;
            ASSERT_EQ(1, db->exec("SELECT r FROM tab WHERE i = ?", "l;l", (long)i + n, &num));
            ASSERT_EQ(r[i], num);
        }
    }
}
