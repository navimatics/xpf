/*
 * DbKit/Defines.hpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef DBKIT_DEFINES_HPP_INCLUDED
#define DBKIT_DEFINES_HPP_INCLUDED

#include <DbKit/impl/Defines.hpp>

namespace DbKit {
}

#endif // DBKIT_DEFINES_HPP_INCLUDED
