/*
 * DbKit/Database.hpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef DBKIT_DATABASE_HPP_INCLUDED
#define DBKIT_DATABASE_HPP_INCLUDED

#include <DbKit/Defines.hpp>
#include <DbKit/impl/Database.hpp>

namespace DbKit {

using DbKit::impl::Database;

}

#endif // DBKIT_DATABASE_HPP_INCLUDED
