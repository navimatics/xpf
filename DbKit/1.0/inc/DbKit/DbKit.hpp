/*
 * DbKit/DbKit.hpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef DBKIT_DBKIT_HPP_INCLUDED
#define DBKIT_DBKIT_HPP_INCLUDED

#include <DbKit/Defines.hpp>
#include <DbKit/Database.hpp>
#include <DbKit/TransactionalDatabase.hpp>

#endif // DBKIT_DBKIT_HPP_INCLUDED
