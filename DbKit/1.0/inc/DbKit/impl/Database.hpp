/*
 * DbKit/impl/Database.hpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef DBKIT_IMPL_DATABASE_HPP_INCLUDED
#define DBKIT_IMPL_DATABASE_HPP_INCLUDED

#include <DbKit/impl/Defines.hpp>

namespace DbKit {
namespace impl {

class DBKIT_APISYM Database : public Base::Object
{
public:
    typedef struct __connection_s *connection_t;
    typedef struct __statement_s *statement_t;
public:
    enum { ErrorMax = 1000 };
    static const char *errorDomain();
    Database(const char *path = 0, const char *codec = 0);
    ~Database();
    Base::Object *threadsafe();
    Base::Lock *lock();
    const char *path();
    virtual int open();
    virtual int close();
    connection_t connectionHandle();
    statement_t statementHandle(int statementIndex);
    int nextStatementIndex();
    const char *statementText(int statementIndex);
    int setStatementText(int statementIndex, const char *text);
    int params(int statementIndex, const char *format, ...);
        /**
         * Bind parameters to a statement.
         *
         * The "format" argument contains the type description of the parameters
         * to be bound; the parameters themselves follow the "format" argument
         * (similar to printf). "Format" is a list of one of the following characters:
         *     !    treat arguments to s, *, ^ as transient and make private copies of the data;
         *          default is to treat arguments to s, *, ^ as non-transient and not make copies;
         *          applies to all arguments
         *     0-9  bind to n-th (1-999) parameter (no argument; applies to next argument)
         *     .    null (no argument), and increment parameter number
         *     i    int
         *     u    unsigned
         *     l    long
         *     q    int64_t
         *     f    double
         *     s    const char *
         *     *    Base::Object * [strrepr()]
         *     ^    const void *, size_t
         *     ;    terminate processing
         *          whitespace ignored
         */
    int vparams(int statementIndex, const char *format, va_list ap);
    int columns(int statementIndex, const char *format, ...);
        /**
         * Extract query results.
         *
         * The "format" argument contains the type description of the query results;
         * storage space for the results is passed as arguments following the "format"
         * argument (similar to scanf). "Format" is a list of one of the following characters:
         *     0-9  extract n-th (1-999) column (no argument; applies to next argument)
         *     .    ignored (no argument), but increment column number
         *     i    int *
         *     u    unsigned *
         *     l    long *
         *     q    int64_t *
         *     f    double *
         *     s    const char **
         *     *    Base::CString **
         *     ^    const void **, size_t *
         *     ;    terminate processing
         *          whitespace ignored
         */
    int vcolumns(int statementIndex, const char *format, va_list ap);
    int step(int statementIndex, const char *format = 0, ...);
        /**
         * Execute statement and extract next query results.
         *
         * The "format" argument is as in columns().
         *
         * The step methods are intended to be executed in the following manner:
         *     while (0 < step(istmt, format, &arg0, &arg1, ...))
         *     {
         *         ...
         *     }
         *
         * The step methods will keep a statement active as long as there are
         * further results to be returned; in this case the methods return true.
         * When there are no further results to be returned (or when an error occurs)
         * they will reset the statement (so that it is ready to be reexecuted)
         * and return false.
         */
    int vstep(int statementIndex, const char *format, va_list ap);
    int finish(int statementIndex);
        /**
         * Finishes a statement (and resets it for reexecution).
         *
         * The step() methods will normally automatically reset a statement
         * when the step() loop finishes. This does not happen if the loop exits
         * prematurely, (e.g. break before the full query resultset is stepped).
         * In this case the finish() method should be called.
         */
    int exec(int statementIndex, const char *format = 0, ...);
        /**
         * Execute statement. Bind parameters, step once and return single row results.
         *
         * The "format" argument is a concatenation of a params() "format" argument,
         * a semicolon ';' and a columns() "format" argument.
         */
    int vexec(int statementIndex, const char *format, va_list ap);
    int exec(const char *text, const char *format = 0, ...);
    int vexec(const char *text, const char *format, va_list ap);
    int params(statement_t statementHandle, const char *format, ...);
    int vparams(statement_t statementHandle, const char *format, va_list ap);
    int columns(statement_t statementHandle, const char *format, ...);
    int vcolumns(statement_t statementHandle, const char *format, va_list ap);
    int step(statement_t statementHandle, const char *format = 0, ...);
    int vstep(statement_t statementHandle, const char *format, va_list ap);
    int finish(statement_t statementHandle);
    int exec(statement_t statementHandle, const char *format = 0, ...);
    int vexec(statement_t statementHandle, const char *format, va_list ap);
    int64_t lastInsertRowid();
    int transactionActive();
    int backup(const char *dbname, const char *path, ssize_t pagecnt = 0);
        /**
         * Backup source to destination database. Source database can be in use (read/write).
         *
         * The attached database specified by "dbname" is backed up onto the database file
         * specified by "path". If there are no attached databases specify "main" for the
         * "dbname" parameter.
         *
         * The "pagecnt" parameter specifies how many database pages to copy:
         *     * == 0: copy the whole database
         *     * <  0: abandon the backup process
         *     * >  0: copy the specified number of pages before returning
         *
         * The backup() method returns -1 on error, 0 when the backup operation is complete,
         * and +1 when there are still more pages to backup.
         *
         * The backup() method is intended to be executed in the following manner:
         *     while (0 < backup(path, dbname, pagecnt))
         *     {
         *         ...
         *     }
         *
         * There can only be one active backup operation. Specifying different backup()
         * "path" and "dbname" parameters while a backup is in progress is an error. As
         * a convenience a 0 is allowed to be passed for the "dbname" and "path" parameters
         * if a backup is already started but not finished.
         */
    statement_t _prepare(const char *text);
        /**
         * Prepare statement.
         *
         * Statements prepared with _prepare() are not tracked by the Database object.
         * They must be unprepared explicitly using _unprepare().
         *
         * Failure to do so will leak system resources. Furthermore the Database object
         * cannot close() cleanly.
         */
    int _unprepare(statement_t statementHandle);
    void __dumpQueryPlan(int statementIndex);
    static void __dumpQueryPlan(statement_t statementHandle);
protected:
    virtual const char *_errorDomain();
    void _setLastError(int sqlres);
private:
    statement_t _statementHandle(int statementIndex);
    int _vparams(statement_t statementHandle, const char *&format, va_list &ap);
    int _vcolumns(statement_t statementHandle, const char *&format, va_list &ap, bool alloc = false);
    int _vstep(statement_t statementHandle, const char *&format, va_list &ap);
    int _finish(statement_t statementHandle);
    int _vexec(statement_t statementHandle, const char *&format, va_list &ap);
    static int _busyHandler(void *, int);
private:
    struct backup_t;
    Base::Lock *_lock;
    const char *_path;
    const char *_zvfs;
    connection_t _connectionHandle;
    statement_t *_statementHandles;
    backup_t *_backup;
};

}
}

#endif // DBKIT_IMPL_DATABASE_HPP_INCLUDED
