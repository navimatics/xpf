/*
 * DbKit/impl/TransactionalDatabase.hpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef DBKIT_IMPL_TRANSACTIONALDATABASE_HPP_INCLUDED
#define DBKIT_IMPL_TRANSACTIONALDATABASE_HPP_INCLUDED

#include <DbKit/impl/Defines.hpp>
#include <DbKit/impl/Database.hpp>

namespace DbKit {
namespace impl {

class DBKIT_APISYM TransactionalDatabase : public Database
{
public:
    enum { ErrorMax = Database::ErrorMax };
    TransactionalDatabase(const char *path = 0, const char *codec = 0);
    ~TransactionalDatabase();
    int open();
    int close();
    virtual int beginTransaction();
        /* Begin transient (i.e. normal) transaction */
    virtual int endTransaction(bool success);
        /* Commit/rollback transient transaction */
    virtual int beginPersistentTransaction(const char *xname, bool rowlock,
        const char **tabnames = 0, size_t tabnamecnt = 0);
        /* Begin persistent transaction:
         *     - The transaction has a name and will persist across database connections.
         *     - There should be no transient transaction active when this function gets called.
         *     - This does *not* start a transient transaction.
         */
    virtual int endPersistentTransaction(const char *xname, bool success,
        const char **dbnames = 0, size_t dbnamecnt = 0);
        /* Commit/rollback persistent transaction
         *     - There should be no transient transaction active when this function gets called.
         */
    virtual int enterPersistentTransaction(const char *xname,
        const char **dbnames = 0, size_t dbnamecnt = 0);
        /* Begin a transient transaction and instruct the named persistent transaction
         * to start tracking modifications to the database.
         */
    virtual int leavePersistentTransaction(const char *xname, bool success,
        const char **dbnames = 0, size_t dbnamecnt = 0);
        /* Commit/rollback transient transaction and instruct the named persistent transaction
         * to stop tracking modifications to the database.
         */
    virtual int rollbackAllPersistentTransactions(
        const char **dbnames = 0, size_t dbnamecnt = 0);
        /* Rollback all persistent transactions:
         *     - There should be no transient transaction active when this function gets called.
         */
private:
    int resetTriggers(const char *xname, const char *ctcol,
        const char **dbnames, size_t dbnamecnt);
    int rollbackLog(const char *dbname, int64_t xactid);
    int resetVirtualTables();
    int getDatabaseList(const char **&results, size_t &count);
    int concatTableList(const char **&results, const char *dbname);
    int getFieldList(const char **&results, size_t &count, const char *dbname, const char *tabname);
    int concatSingleColumnQueryResults(const char **&results, const char *text, const char *colspec);
    int execText(const char *format, ...);
    int execUnboundedText(const char *format, ...);
private:
    statement_t _statementBegin, _statementCommit, _statementRollback;
};

}
}

#endif // DBKIT_IMPL_TRANSACTIONALDATABASE_HPP_INCLUDED
