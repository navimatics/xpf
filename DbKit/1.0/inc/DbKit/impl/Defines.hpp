/*
 * DbKit/impl/Defines.hpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef DBKIT_IMPL_DEFINES_HPP_INCLUDED
#define DBKIT_IMPL_DEFINES_HPP_INCLUDED

#include <Base/Base.hpp>

#if defined(DBKIT_CONFIG_INTERNAL)
#define DBKIT_APISYM                    BASE_EXPORTSYM
#else
#define DBKIT_APISYM                    BASE_IMPORTSYM
#endif

namespace DbKit {
namespace impl {
}
}

#endif // DBKIT_IMPL_DEFINES_HPP_INCLUDED
