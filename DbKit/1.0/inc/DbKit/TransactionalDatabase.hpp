/*
 * DbKit/TransactionalDatabase.hpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef DBKIT_TRANSACTIONALDATABASE_HPP_INCLUDED
#define DBKIT_TRANSACTIONALDATABASE_HPP_INCLUDED

#include <DbKit/Defines.hpp>
#include <DbKit/Database.hpp>
#include <DbKit/impl/TransactionalDatabase.hpp>

namespace DbKit {

using DbKit::impl::TransactionalDatabase;

}

#endif // DBKIT_TRANSACTIONALDATABASE_HPP_INCLUDED
