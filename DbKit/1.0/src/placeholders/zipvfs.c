/*
 * zipvfs.c placeholder
 * included from DbKit/impl/zsqlite3.c when the SQLite ZIPVFS extension is not present
 */

#include <zipvfs.h>

int zipvfs_create_vfs_v3(const char *zName, const char *zParent, void *pCtx,
    int (*xAutoDetect)(void *, const char *zFile, const char *zHdr, ZipvfsMethods *))
{
    (void)sqlite3PagerWalFramesize; /* silence unused function warning */
    return SQLITE_MISUSE;
}
