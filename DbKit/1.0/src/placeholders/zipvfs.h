/*
 * zipvfs.h placeholder
 * included from DbKit/impl/zsqlite3.h when the SQLite ZIPVFS extension is not present
 */

#define DBKIT_IMPL_ZIPVFS_PLACEHOLDER

#ifdef __cplusplus
extern "C" {
#endif

typedef struct ZipvfsMethods ZipvfsMethods;
int zipvfs_create_vfs_v3(const char *zName, const char *zParent, void *pCtx,
    int (*xAutoDetect)(void *, const char *zFile, const char *zHdr, ZipvfsMethods *));
struct ZipvfsMethods
{
    const char *zHdr;
    void *pCtx;
    int (*xCompressBound)(void *, int nSrc);
    int (*xCompress)(void *, char *aDest, int *pnDest, const char *aSrc, int nSrc);
    int (*xUncompress)(void *, char *aDest, int *pnDest, const char *aSrc, int nSrc);
    int (*xCompressClose)(void *);
};

#ifdef __cplusplus
}
#endif
