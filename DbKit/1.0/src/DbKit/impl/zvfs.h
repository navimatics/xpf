/*
 * DbKit/impl/zvfs.h
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef DBKIT_IMPL_ZVFS_H_INCLUDED
#define DBKIT_IMPL_ZVFS_H_INCLUDED

#ifdef __cplusplus
extern "C" {
#endif

int zvfs_create(const char *name, const char *codecName);

#ifdef __cplusplus
}
#endif

#endif // DBKIT_IMPL_ZVFS_H_INCLUDED
