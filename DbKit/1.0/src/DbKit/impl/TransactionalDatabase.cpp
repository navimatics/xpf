/*
 * DbKit/impl/TransactionalDatabase.cpp
 * Persistent transaction mechanism inspired by http://www.sqlite.org/cvstrac/wiki?p=UndoRedo
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#include <DbKit/impl/TransactionalDatabase.hpp>
#include "zsqlite3.h"

namespace DbKit {
namespace impl {

TransactionalDatabase::TransactionalDatabase(const char *path, const char *codec) :
    Database(path, codec)
{
}
TransactionalDatabase::~TransactionalDatabase()
{
    close();
}
int TransactionalDatabase::open()
{
    synchronized (lock())
    {
        if (-1 == Database::open())
            return -1;
        if (0 == (_statementBegin = _prepare("BEGIN DEFERRED")))
            goto error;
        if (0 == (_statementCommit = _prepare("COMMIT")))
            goto error;
        if (0 == (_statementRollback = _prepare("ROLLBACK")))
            goto error;
        return 0;
    error:
        close();
        return -1;
    }
}
int TransactionalDatabase::close()
{
    synchronized (lock())
    {
        if (0 != _statementBegin)
        {
            _unprepare(_statementBegin);
            _statementBegin = 0;
        }
        if (0 != _statementCommit)
        {
            _unprepare(_statementCommit);
            _statementCommit = 0;
        }
        if (0 != _statementRollback)
        {
            _unprepare(_statementRollback);
            _statementRollback = 0;
        }
        return Database::close();
    }
}
int TransactionalDatabase::beginTransaction()
{
    if (-1 != exec(_statementBegin))
        return 0;
    return -1;
}
int TransactionalDatabase::endTransaction(bool success)
{
    /* This function preserves Base::Error::lastError() when success is false. */
    synchronized (lock())
    {
        if (success)
        {
            if (-1 != exec(_statementCommit))
                return 0;
        }
        Base::Error *lastError = Base::Error::lastError();
        if (-1 != exec(_statementRollback))
            goto end;
        if (!transactionActive())
            goto end;
        LOG("warncrit: unable to rollback transaction!");
    end:
        if (0 != lastError && 0 == strcmp(_errorDomain(), lastError->domain()))
            Base::Error::setLastError(lastError, true);
        return -1;
    }
}
/* The persistent transaction mechanism may not work properly if the beginTransaction() and
 * endTransaction() methods have been overriden by any subclasses. To avoid any problems
 * ensure that we call our implementation of these methods.
 */
static int qsort_strcmp(const void *p, const void *q)
{
    return strcmp(*(const char **)p, *(const char **)q);
}
int TransactionalDatabase::beginPersistentTransaction(const char *xname, bool rowlock,
    const char **tabnames0, size_t tabnamecnt0)
{
    mark_and_collect
        synchronized (lock())
        {
            int res = TransactionalDatabase::beginTransaction();
            if (-1 == res)
                return -1;
            const char **tabnames; size_t tabnamecnt;
            const char *lastDbname;
            int64_t xactid;
            const char *ct0sql, *ct1sql, *dtsql;
            ct0sql = ct1sql = dtsql = 0;
            tabnames = 0; tabnamecnt = 0;
            if (0 == tabnames0 || 0 == tabnamecnt0)
            {
                const char **dbnames; size_t dbnamecnt;
                res = getDatabaseList(dbnames, dbnamecnt);
                if (-1 == res)
                    goto end;
                for (size_t i = 0; dbnamecnt > i; i++)
                {
                    if (0 == stricmp("temp", dbnames[i]))
                        continue;
                    res = concatTableList(tabnames, dbnames[i]);
                    if (-1 == res)
                        goto end;
                }
            }
            else
            {
                for (size_t i = 0; tabnamecnt0 > i; i++)
                {
                    const char *p = strchr(tabnames0[i], '.');
                    if (0 != p && '*' == p[1])
                    {
                        char dbname[256];
                        size_t len = p - tabnames0[i];
                        if (len >= sizeof dbname)
                            len = sizeof dbname - 1;
                        memcpy(dbname, tabnames0[i], len);
                        dbname[len] = '\0';
                        res = concatTableList(tabnames, dbname);
                        if (-1 == res)
                            goto end;
                    }
                    else
                    {
                        const char *tabname = 0 != p
                            ? Base::cstring(tabnames0[i])
                            : Base::cstringf("main.%s", tabnames0[i]);
                        tabnames = (const char **)Base::carr_concat(tabnames, &tabname, 1, sizeof(const char *));
                    }
                }
            }
            if (0 != tabnames)
            {
                Base::carr_obj(tabnames)->autorelease();
                tabnamecnt = Base::carr_length(tabnames);
                qsort(tabnames, tabnamecnt, sizeof(const char *), qsort_strcmp);
            }
            lastDbname = 0;
            for (size_t i = 0; tabnamecnt > i; i++)
            {
                char *p = strchr(tabnames[i], '.');
                assert(0 != p);
                *p++ = '\0';
                const char *dbname = tabnames[i];
                const char *tabname = p;
                if (0 == lastDbname || 0 != strcmp(lastDbname, dbname))
                {
                    if (0 != lastDbname)
                    {
                        res = execUnboundedText(
                            "UPDATE [%s].__xactlst__ SET ct0sql = '%q', ct1sql = '%q', dtsql = '%q' WHERE xactid = %lli",
                            lastDbname, ct0sql, ct1sql, dtsql, xactid);
                        if (-1 == res)
                            goto end;
                        Base::cstr_release(ct0sql);
                        Base::cstr_release(ct1sql);
                        Base::cstr_release(dtsql);
                        ct0sql = 0;
                        ct1sql = 0;
                        dtsql = 0;
                    }
                    res = execText(
                        "CREATE TABLE IF NOT EXISTS [%s].__xactlog__ "
                        "("
                        "    xactid INTEGER,"
                        "    tabname TEXT COLLATE NOCASE,"
                        "    tabrowid INTEGER,"
                        "    sql TEXT,"
                        "    PRIMARY KEY(xactid, tabname, tabrowid)"
                        ")",
                        dbname);
                    if (-1 == res)
                        goto end;
                    res = execText(
                        "CREATE TABLE IF NOT EXISTS [%s].__xactlst__ "
                        "("
                        "    xactid INTEGER PRIMARY KEY NOT NULL,"
                        "    xname TEXT UNIQUE COLLATE NOCASE,"
                        "    ct0sql TEXT,"
                        "    ct1sql TEXT,"
                        "    dtsql TEXT"
                        ")",
                        dbname);
                    if (-1 == res)
                        goto end;
                    res = execText(
                        "INSERT INTO [%s].__xactlst__ (xactid, xname) VALUES (NULL, '%q')",
                        dbname, xname);
                    if (-1 == res)
                        goto end;
                    xactid = lastInsertRowid();
                    lastDbname = dbname;
                }
                const char **fldnames; size_t fldnamecnt;
                res = getFieldList(fldnames, fldnamecnt, dbname, tabname);
                if (-1 == res)
                    goto end;
                char text[4096];
                sqlite3_snprintf(sizeof text, text,
                    "CREATE TRIGGER [%s].[__xactitr__%llx_%q] AFTER INSERT ON [%s] "
                    "WHEN (SELECT 1 FROM [%s].__xactlog__ WHERE xactid=%lli AND tabname='%q'%s) "
                    "BEGIN "
                    "SELECT RAISE(ABORT, 'table [%s].[%s] has persistent transaction changes'); "
                    "END;\n",
                    dbname, xactid, tabname, tabname,
                    dbname, xactid, tabname, rowlock ? " AND tabrowid=new._rowid_" : "",
                    dbname, tabname);
                ct0sql = Base::cstr_concat(ct0sql, text);
                sqlite3_snprintf(sizeof text, text,
                    "CREATE TRIGGER [%s].[__xactutr__%llx_%q] AFTER UPDATE ON [%s] "
                    "WHEN (SELECT 1 FROM [%s].__xactlog__ WHERE xactid=%lli AND tabname='%q'%s) "
                    "BEGIN "
                    "SELECT RAISE(ABORT, 'table [%s].[%s] has persistent transaction changes'); "
                    "END;\n",
                    dbname, xactid, tabname, tabname,
                    dbname, xactid, tabname, rowlock ? " AND tabrowid=old._rowid_" : "",
                    dbname, tabname);
                ct0sql = Base::cstr_concat(ct0sql, text);
                sqlite3_snprintf(sizeof text, text,
                    "CREATE TRIGGER [%s].[__xactdtr__%llx_%q] AFTER DELETE ON [%s] "
                    "WHEN (SELECT 1 FROM [%s].__xactlog__ WHERE xactid=%lli AND tabname='%q'%s) "
                    "BEGIN "
                    "SELECT RAISE(ABORT, 'table [%s].[%s] has persistent transaction changes'); "
                    "END;\n",
                    dbname, xactid, tabname, tabname,
                    dbname, xactid, tabname, rowlock ? " AND tabrowid=old._rowid_" : "",
                    dbname, tabname);
                ct0sql = Base::cstr_concat(ct0sql, text);
                sqlite3_snprintf(sizeof text, text,
                    "CREATE TRIGGER [%s].[__xactitr__%llx_%q] AFTER INSERT ON [%s] "
                    "WHEN NOT EXISTS (SELECT 1 FROM [%s].__xactlog__ WHERE xactid=%lli AND tabname='%q' AND tabrowid=new._rowid_) "
                    "BEGIN "
                    "INSERT INTO __xactlog__ VALUES(%lli, '%q', new._rowid_, "
                    "'DELETE FROM [%s].[%s] WHERE _rowid_='||new._rowid_); "
                    "END;\n",
                    dbname, xactid, tabname, tabname,
                    dbname, xactid, tabname,
                    xactid, tabname, dbname, tabname);
                ct1sql = Base::cstr_concat(ct1sql, text);
                sqlite3_snprintf(sizeof text, text,
                    "CREATE TRIGGER [%s].[__xactutr__%llx_%q] AFTER UPDATE ON [%s] "
                    "WHEN NOT EXISTS (SELECT 1 FROM [%s].__xactlog__ WHERE xactid=%lli AND tabname='%q' AND tabrowid=old._rowid_) "
                    "BEGIN "
                    "INSERT INTO __xactlog__ VALUES(%lli, '%q', old._rowid_, "
                    "'REPLACE INTO [%s].[%s] (_rowid_",
                    dbname, xactid, tabname, tabname,
                    dbname, xactid, tabname,
                    xactid, tabname, dbname, tabname);
                ct1sql = Base::cstr_concat(ct1sql, text);
                for (size_t j = 0; fldnamecnt > j; j++)
                {
                    sqlite3_snprintf(sizeof text, text,
                        ",%s", fldnames[j]);
                    ct1sql = Base::cstr_concat(ct1sql, text);
                }
                sqlite3_snprintf(sizeof text, text,
                    ") VALUES('||old._rowid_||'",
                    dbname, xactid, tabname, tabname, tabname, xactid, dbname, tabname);
                ct1sql = Base::cstr_concat(ct1sql, text);
                for (size_t j = 0; fldnamecnt > j; j++)
                {
                    sqlite3_snprintf(sizeof text, text,
                        ",'||quote(old.%s)||'", fldnames[j]);
                    ct1sql = Base::cstr_concat(ct1sql, text);
                }
                ct1sql = Base::cstr_concat(ct1sql, ")'); END;\n");
                sqlite3_snprintf(sizeof text, text,
                    "CREATE TRIGGER [%s].[__xactdtr__%llx_%q] AFTER DELETE ON [%s] "
                    "WHEN NOT EXISTS (SELECT 1 FROM [%s].__xactlog__ WHERE xactid=%lli AND tabname='%q' AND tabrowid=old._rowid_) "
                    "BEGIN "
                    "INSERT INTO __xactlog__ VALUES(%lli, '%q', old._rowid_, "
                    "'REPLACE INTO [%s].[%s] (_rowid_",
                    dbname, xactid, tabname, tabname,
                    dbname, xactid, tabname,
                    xactid, tabname, dbname, tabname);
                ct1sql = Base::cstr_concat(ct1sql, text);
                for (size_t j = 0; fldnamecnt > j; j++)
                {
                    sqlite3_snprintf(sizeof text, text,
                        ",%s", fldnames[j]);
                    ct1sql = Base::cstr_concat(ct1sql, text);
                }
                sqlite3_snprintf(sizeof text, text,
                    ") VALUES('||old._rowid_||'",
                    dbname, xactid, tabname, tabname, tabname, xactid, dbname, tabname);
                ct1sql = Base::cstr_concat(ct1sql, text);
                for (size_t j = 0; fldnamecnt > j; j++)
                {
                    sqlite3_snprintf(sizeof text, text,
                        ",'||quote(old.%s)||'", fldnames[j]);
                    ct1sql = Base::cstr_concat(ct1sql, text);
                }
                ct1sql = Base::cstr_concat(ct1sql, ")'); END;\n");
                sqlite3_snprintf(sizeof text, text,
                    "DROP TRIGGER [%s].[__xactitr__%llx_%q];\n"
                    "DROP TRIGGER [%s].[__xactutr__%llx_%q];\n"
                    "DROP TRIGGER [%s].[__xactdtr__%llx_%q];\n",
                    dbname, xactid, tabname, dbname, xactid, tabname, dbname, xactid, tabname);
                dtsql = Base::cstr_concat(dtsql, text);
            }
            if (0 != lastDbname)
            {
                res = execUnboundedText(
                    "UPDATE [%s].__xactlst__ SET ct0sql = '%q', ct1sql = '%q', dtsql = '%q' WHERE xactid = %lli",
                    lastDbname, ct0sql, ct1sql, dtsql, xactid);
                if (-1 == res)
                    goto end;
                Base::cstr_release(ct0sql);
                Base::cstr_release(ct1sql);
                Base::cstr_release(dtsql);
                ct0sql = 0;
                ct1sql = 0;
                dtsql = 0;
            }
        end:
            Base::cstr_release(ct0sql);
            Base::cstr_release(ct1sql);
            Base::cstr_release(dtsql);
            return TransactionalDatabase::endTransaction(-1 != res);
        }
}
int TransactionalDatabase::endPersistentTransaction(const char *xname, bool success,
    const char **dbnames, size_t dbnamecnt)
{
    mark_and_collect
        synchronized (lock())
        {
            int res = TransactionalDatabase::beginTransaction();
            if (-1 == res)
                return -1;
            if (0 == dbnames || 0 == dbnamecnt)
            {
                res = getDatabaseList(dbnames, dbnamecnt);
                if (-1 == res)
                    goto end;
            }
            for (size_t i = 0; dbnamecnt > i; i++)
            {
                if (0 == stricmp("temp", dbnames[i]))
                    continue;
                char text[4096];
                sqlite3_snprintf(sizeof text, text,
                    "SELECT xactid, dtsql FROM [%s].__xactlst__ WHERE xname = '%q'",
                    dbnames[i], xname);
                int64_t xactid;
                const char *dtsql;
                if (+1 != exec(text, ";qs", &xactid, &dtsql))
                    continue;
                res = execText(
                    "DELETE FROM [%s].__xactlst__ WHERE xactid = %lli",
                    dbnames[i], xactid);
                if (-1 == res)
                    goto end;
                sqlite3_exec((sqlite3 *)connectionHandle(), dtsql, 0, 0, 0);
                if (!success)
                {
                    /*
                     * If the persistent transaction is being rolled back, we must undo all changes
                     * that the triggers have logged. This can potentially be a lengthy operation.
                     */
                    res = rollbackLog(dbnames[i], xactid);
                    if (-1 == res)
                        goto end;
                }
                res = execText(
                    "DELETE FROM [%s].__xactlog__ WHERE xactid = %lli",
                    dbnames[i], xactid);
                if (-1 == res)
                    goto end;
                res = execText(
                    "SELECT 1 FROM [%s].__xactlst__",
                    dbnames[i]);
                if (-1 == res)
                    goto end;
                if (0 == res)
                {
                    /* no more transactions: drop persistent transaction related tables/indexes */
                    res = execText("DROP TABLE [%s].__xactlst__", dbnames[i]);
                    if (-1 == res)
                        goto end;
                    res = execText("DROP TABLE [%s].__xactlog__", dbnames[i]);
                    if (-1 == res)
                        goto end;
                }
            }
        end:
            res = TransactionalDatabase::endTransaction(-1 != res);
            if (-1 != res && !success)
                resetVirtualTables();
            return res;
        }
}
int TransactionalDatabase::enterPersistentTransaction(const char *xname,
    const char **dbnames, size_t dbnamecnt)
{
    mark_and_collect
        synchronized (lock())
        {
            int res = TransactionalDatabase::beginTransaction();
            if (-1 == res)
                return -1;
            res = resetTriggers(xname, "ct1sql", dbnames, dbnamecnt);
            if (-1 == res)
                return TransactionalDatabase::endTransaction(false);
            return 0;
        }
}
int TransactionalDatabase::leavePersistentTransaction(const char *xname, bool success,
    const char **dbnames, size_t dbnamecnt)
{
    mark_and_collect
        synchronized (lock())
        {
            if (!success)
                return TransactionalDatabase::endTransaction(false);
            int res = resetTriggers(xname, "ct0sql", dbnames, dbnamecnt);
            return TransactionalDatabase::endTransaction(-1 != res);
        }
}
int TransactionalDatabase::rollbackAllPersistentTransactions(
    const char **dbnames, size_t dbnamecnt)
{
    mark_and_collect
        synchronized (lock())
        {
            int res = TransactionalDatabase::beginTransaction();
            if (-1 == res)
                return -1;
            if (0 == dbnames || 0 == dbnamecnt)
            {
                res = getDatabaseList(dbnames, dbnamecnt);
                if (-1 == res)
                    goto end;
            }
            for (size_t i = 0; dbnamecnt > i; i++)
            {
                if (0 == stricmp("temp", dbnames[i]))
                    continue;
                char text[4096];
                sqlite3_snprintf(sizeof text, text,
                    "SELECT dtsql FROM [%s].__xactlst__",
                    dbnames[i]);
                Base::Error::enableLogging(false);
                    /* disable error logging as the call below can fail harmlessly */
                statement_t statement = _prepare(text);
                Base::Error::enableLogging(true);
                    /* reenable error logging */
                if (0 != statement)
                {
                    /* drop all triggers */
                    const char *dtsql;
                    while (0 < step(statement, "s", &dtsql))
                        sqlite3_exec((sqlite3 *)connectionHandle(), dtsql, 0, 0, 0);
                    _unprepare(statement);
                }
                else
                    continue;
                /*
                 * We must undo all changes from all persistent transactions that the triggers
                 * have logged. This can potentially be a very lengthy operation.
                 */
                res = rollbackLog(dbnames[i], Base::None());
                if (-1 == res)
                    goto end;
                /* drop persistent transaction related tables/indexes */
                res = execText("DROP TABLE [%s].__xactlst__", dbnames[i]);
                if (-1 == res)
                    goto end;
                res = execText("DROP TABLE [%s].__xactlog__", dbnames[i]);
                if (-1 == res)
                    goto end;
            }
        end:
            res = TransactionalDatabase::endTransaction(-1 != res);
            if (-1 != res)
                resetVirtualTables();
            return res;
        }
}
int TransactionalDatabase::resetTriggers(const char *xname, const char *ctcol,
    const char **dbnames, size_t dbnamecnt)
{
    if (0 == dbnames || 0 == dbnamecnt)
    {
        int res = getDatabaseList(dbnames, dbnamecnt);
        if (-1 == res)
            return -1;
    }
    for (size_t i = 0; dbnamecnt > i; i++)
    {
        if (0 == stricmp("temp", dbnames[i]))
            continue;
        char text[4096];
        sqlite3_snprintf(sizeof text, text,
            "SELECT %s, dtsql FROM [%s].__xactlst__ WHERE xname = '%q'",
            ctcol, dbnames[i], xname);
        const char *ctsql, *dtsql;
        if (+1 != exec(text, ";ss", &ctsql, &dtsql))
            continue;
        sqlite3_exec((sqlite3 *)connectionHandle(), dtsql, 0, 0, 0);
        if (SQLITE_OK != sqlite3_exec((sqlite3 *)connectionHandle(), ctsql, 0, 0, 0))
            return -1;
    }
    return 0;
}
int TransactionalDatabase::rollbackLog(const char *dbname, int64_t xactid)
{
    int res = 0;
    char text[4096];
    /* we enumerate the logged SQL's in reverse entry order to accommodate foreign key constraints */
    if (!Base::isNone(xactid))
        sqlite3_snprintf(sizeof text, text,
            "SELECT sql FROM [%s].__xactlog__ WHERE xactid = %lli ORDER BY _rowid_ DESC",
            dbname, xactid);
    else
        sqlite3_snprintf(sizeof text, text,
            "SELECT sql FROM [%s].__xactlog__ ORDER BY _rowid_ DESC",
            dbname);
    if (statement_t statement = _prepare(text))
    {
        const char *sql;
        while (0 < step(statement, "s", &sql))
            if (SQLITE_OK != sqlite3_exec((sqlite3 *)connectionHandle(), sql, 0, 0, 0))
            {
                res = -1;
                break;
            }
        _unprepare(statement);
    }
    else
        res = -1;
    return res;
}
int TransactionalDatabase::resetVirtualTables()
{
    /* this should force virtual tables that had any shadow tables updated to do a state reset */
    if (-1 == exec(_statementBegin))
        return -1;
    return exec(_statementRollback);
}
int TransactionalDatabase::getDatabaseList(const char **&results, size_t &count)
{
    results = 0; count = 0;
    int res = concatSingleColumnQueryResults(results, "PRAGMA database_list", "2*");
    if (0 != results)
    {
        Base::carr_obj(results)->autorelease();
        count = Base::carr_length(results);
    }
    return res;
}
int TransactionalDatabase::concatTableList(const char **&results, const char *dbname)
{
    char text[4096];
    sqlite3_snprintf(sizeof text, text,
        "SELECT '%s.' || name FROM [%s].sqlite_master"
        " WHERE type = 'table'"
        "   AND name NOT LIKE 'sqlite@_%%' ESCAPE '@'"
        "   AND name NOT LIKE '@_@_xact%%' ESCAPE '@'"
        "   AND sql NOT LIKE 'CREATE VIRTUAL TABLE%%'",
        dbname, dbname);
    return concatSingleColumnQueryResults(results, text, "*");
}
int TransactionalDatabase::getFieldList(const char **&results, size_t &count, const char *dbname, const char *tabname)
{
    char text[4096];
    sqlite3_snprintf(sizeof text, text, "PRAGMA [%s].table_info([%s])", dbname, tabname);
    results = 0; count = 0;
    int res = concatSingleColumnQueryResults(results, text, "2*");
    if (0 != results)
    {
        Base::carr_obj(results)->autorelease();
        count = Base::carr_length(results);
    }
    return res;
}
int TransactionalDatabase::concatSingleColumnQueryResults(const char **&results, const char *text, const char *colspec)
{
    /* This function guarantees that:
     *     When it returns  0 (success), ''results'' != 0 and points to a valid (possibly empty) array.
     *     When it returns -1 (failure), ''results'' == 0 and any preexisting array is released.
     */
    if (statement_t statement = _prepare(text))
    {
        Base::CString *strobj;
        while (0 < step(statement, colspec, &strobj))
        {
            const char *str = strobj->chars();
            results = (const char **)Base::carr_concat(results, &str, 1, sizeof(const char *));
        }
        _unprepare(statement);
        if (0 == results)
            results = (const char **)Base::carr_new(0, 0, sizeof(const char *));
        return 0;
    }
    else
    {
        Base::carr_release(results);
        results = 0;
        return -1;
    }
}
int TransactionalDatabase::execText(const char *format, ...)
{
    va_list ap;
    va_start(ap, format);
    char text[4096];
    sqlite3_vsnprintf(sizeof text, text, format, ap);
    assert(sizeof(text) > strlen(text)); /* guard against overflown buffer */
    int res = exec(text);
    va_end(ap);
    return res;
}
int TransactionalDatabase::execUnboundedText(const char *format, ...)
{
    va_list ap;
    va_start(ap, format);
    char *text = sqlite3_vmprintf(format, ap);
    int res = exec(text);
    sqlite3_free(text);
    va_end(ap);
    return res;
}

}
}
