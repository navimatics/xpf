/*
 * DbKit/impl/zsqlite3.c
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#include <Base/impl/Config.h>

/*
 * SQLite configuration: General
 *
 * SQLITE_DEFAULT_CACHE_SIZE=500
 *     Default size of the page-cache for each attached database in pages.
 * SQLITE_DEFAULT_FOREIGN_KEYS=1
 *     Enforcement of foreign key constraints is enabled by default.
 * SQLITE_DEFAULT_MEMSTATUS=0
 *     Collection of memory allocation statistics disabled.
 * SQLITE_DEFAULT_PAGE_SIZE=4096
 *     Default page-size used when a database is created.
 * SQLITE_DEFAULT_RECURSIVE_TRIGGERS=1
 *     Triggers are recursive by default.
 * SQLITE_DIRECT_OVERFLOW_READ=1
 *     Content contained in overflow pages of the database file is read directly from disk.
 * SQLITE_ENABLE_FTS4=1
 *     Versions 3 and 4 of the full-text search engine is added to the build.
 * SQLITE_ENABLE_FTS3=1
 *     Version 3 of the full-text search engine is added to the build.
 * SQLITE_ENABLE_FTS3_PARENTHESIS=1
 *     Query pattern parser in FTS3 supports operators AND and NOT.
 * SQLITE_ENABLE_RTREE=1
 *     Include support for the R*Tree index extension.
 * SQLITE_ENABLE_ZIPVFS=1
 *     Enable the ZIPVFS extension to SQLite (requires license from Hwaci).
 * SQLITE_THREADSAFE=2
 *     SQLite can be used in a multithreaded program so long as no two threads
 *     attempt to use the same database connection at the same time.
 * ZIPVFS_DEFAULT_CACHE_SIZE=25
 *     Default ZIPVFS pager cache size.
 */
#define SQLITE_DEFAULT_CACHE_SIZE       500
#define SQLITE_DEFAULT_FOREIGN_KEYS     1
#define SQLITE_DEFAULT_MEMSTATUS        0
#define SQLITE_DEFAULT_PAGE_SIZE        4096
#define SQLITE_DEFAULT_RECURSIVE_TRIGGERS 1
//#define SQLITE_DIRECT_OVERFLOW_READ     1
#define SQLITE_ENABLE_FTS4              1
#define SQLITE_ENABLE_FTS3              1
#define SQLITE_ENABLE_FTS3_PARENTHESIS  1
#define SQLITE_ENABLE_RTREE             1
#define SQLITE_ENABLE_ZIPVFS            1
#define SQLITE_THREADSAFE               2
#define ZIPVFS_DEFAULT_CACHE_SIZE       25

/*
 * SQLite configuration: Darwin
 *
 * HAVE_USLEEP=1
 *     Use usleep().
 * SQLITE_ENABLE_LOCKING_STYLE=1
 *     POSIX locking style.
 * USE_PREAD=1
 *     Use pread().
 */
#if defined(BASE_CONFIG_DARWIN)
#define SQLITE_ENABLE_LOCKING_STYLE     1
#define HAVE_USLEEP                     1
#define USE_PREAD                       1
#else
#error zsqlite3.c not implemented for this platform
#endif

#include <sqlite3.c>
#include <zipvfs.c>

#if defined(DBKIT_IMPL_ZIPVFS_PLACEHOLDER)
#if defined(BASE_CONFIG_MSVC)
#warn SQLite ZIPVFS extension is missing; using placeholder
#elif defined(BASE_CONFIG_GNUC)
#warning SQLite ZIPVFS extension is missing; using placeholder
#else
#error unsupported compiler
#endif
#endif
