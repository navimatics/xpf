/*
 * DbKit/impl/zvfs.c
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#include <assert.h>
#include <stdlib.h>
#include <string.h>
#include "zsqlite3.h"
#include "zvfs.h"

#if defined(DBKIT_IMPL_ZIPVFS_PLACEHOLDER)
int zvfs_create(const char *name, const char *codecName)
{
    return SQLITE_MISUSE;
}
#else
/*
 * store codec
 */
static int zvfs_store_createContext(void **ctx)
{
    *ctx = 0;
    return SQLITE_OK;
}
static int zvfs_store_compressBound(void *ctx, int srcsiz)
{
    return srcsiz;
}
static int zvfs_store_compress(void *ctx, char *dst, int *pdstsiz, const char *src, int srcsiz)
{
    assert(*pdstsiz >= srcsiz);
    memcpy(dst, src, srcsiz);
    *pdstsiz = srcsiz;
    return SQLITE_OK;
}
static int zvfs_store_uncompress(void *ctx, char *dst, int *pdstsiz, const char *src, int srcsiz)
{
    assert(*pdstsiz >= srcsiz);
    memcpy(dst, src, srcsiz);
    *pdstsiz = srcsiz;
    return SQLITE_OK;
}
static int zvfs_store_compressClose(void *ctx)
{
    return SQLITE_OK;
}
/*
 * lz4 codec
 */
#include <lz4.h>
#include <lz4hc.h>
static int zvfs_lz4_createContext(void **ctx)
{
    *ctx = 0;
    return SQLITE_OK;
}
static int zvfs_lz4_compressBound(void *ctx, int srcsiz)
{
    return LZ4_COMPRESSBOUND(srcsiz);
}
static int zvfs_lz4_compress(void *ctx, char *dst, int *pdstsiz, const char *src, int srcsiz)
{
    assert(*pdstsiz >= LZ4_COMPRESSBOUND(srcsiz));
    *pdstsiz = LZ4_compress(src, dst, srcsiz);
    return SQLITE_OK;
}
static int zvfs_lz4hc_compress(void *ctx, char *dst, int *pdstsiz, const char *src, int srcsiz)
{
    assert(*pdstsiz >= LZ4_COMPRESSBOUND(srcsiz));
    *pdstsiz = LZ4_compressHC(src, dst, srcsiz);
    return SQLITE_OK;
}
static int zvfs_lz4_uncompress(void *ctx, char *dst, int *pdstsiz, const char *src, int srcsiz)
{
    int retsiz = LZ4_uncompress(src, dst, *pdstsiz);
    assert(0 >= retsiz || retsiz == srcsiz);
    return 0 < retsiz ? SQLITE_OK : SQLITE_ERROR;
}
static int zvfs_lz4_compressClose(void *ctx)
{
    return SQLITE_OK;
}
/*
 * deflate codec
 */
#define MINIZ_NO_STDIO
#define MINIZ_NO_TIME
#define MINIZ_NO_ARCHIVE_APIS
#define MINIZ_NO_ARCHIVE_WRITING_APIS
#define MINIZ_NO_ZLIB_APIS
#define MINIZ_NO_ZLIB_COMPATIBLE_NAMES
#include <miniz.c>
/* from mz_deflateBound() */
#define MZ_DEFLATEBOUND(srcsiz)\
    MZ_MAX(128 + ((srcsiz) * 110) / 100, 128 + (srcsiz) + (((srcsiz) / (31 * 1024)) + 1) * 5)
static int zvfs_deflate_createContext(void **ctx)
{
    *ctx = (void *)TINFL_FLAG_PARSE_ZLIB_HEADER;
    return SQLITE_OK;
}
static int zvfs_deflateRaw_createContext(void **ctx)
{
    *ctx = 0;
    return SQLITE_OK;
}
static int zvfs_deflate_compressBound(void *ctx, int srcsiz)
{
    return MZ_DEFLATEBOUND(srcsiz);
}
static int zvfs_deflate_compress(void *ctx, char *dst, int *pdstsiz, const char *src, int srcsiz)
{
    assert(*pdstsiz >= MZ_DEFLATEBOUND(srcsiz));
    *pdstsiz = tdefl_compress_mem_to_mem(dst, *pdstsiz, src, srcsiz,
        (intptr_t)ctx ? TDEFL_WRITE_ZLIB_HEADER | 128 : 128); /* 128: number of probes for zlib level 6 */
    return 0 != *pdstsiz ? SQLITE_OK : SQLITE_ERROR;
}
static int zvfs_deflate_uncompress(void *ctx, char *dst, int *pdstsiz, const char *src, int srcsiz)
{
    size_t retsiz = tinfl_decompress_mem_to_mem(dst, *pdstsiz, src, srcsiz, (intptr_t)ctx);
    assert(-1 == retsiz || retsiz == *pdstsiz);
    return -1 != retsiz ? SQLITE_OK : SQLITE_ERROR;
}
static int zvfs_deflate_compressClose(void *ctx)
{
    return SQLITE_OK;
}
/*
 * zvfs_create()
 */
struct Codec
{
    const char *name;
    int (*createContext)(void **ctx);
    int (*compressBound)(void *ctx, int srcsiz);
    int (*compress)(void *ctx, char *dst, int *pdstsiz, const char *src, int srcsiz);
    int (*uncompress)(void *ctx, char *dst, int *pdstsiz, const char *src, int srcsiz);
    int (*compressClose)(void *ctx);
};
static struct Codec codecs[] =
{
    {
        "store",
        zvfs_store_createContext,
        zvfs_store_compressBound,
        zvfs_store_compress,
        zvfs_store_uncompress,
        zvfs_store_compressClose,
    },
    {
        "lz4",
        zvfs_lz4_createContext,
        zvfs_lz4_compressBound,
        zvfs_lz4_compress,
        zvfs_lz4_uncompress,
        zvfs_lz4_compressClose,
    },
    {
        "lz4hc",
        zvfs_lz4_createContext,
        zvfs_lz4_compressBound,
        zvfs_lz4hc_compress,
        zvfs_lz4_uncompress,
        zvfs_lz4_compressClose,
    },
    {
        "deflate",
        zvfs_deflate_createContext,
        zvfs_deflate_compressBound,
        zvfs_deflate_compress,
        zvfs_deflate_uncompress,
        zvfs_deflate_compressClose,
    },
    {
        "deflateRaw",
        zvfs_deflateRaw_createContext,
        zvfs_deflate_compressBound,
        zvfs_deflate_compress,
        zvfs_deflate_uncompress,
        zvfs_deflate_compressClose,
    },
};
static int zvfs_autodetect(void *ctx, const char *filename, const char *codecName, ZipvfsMethods *methods)
{
    if (0 == codecName)
    {
        const char *param = sqlite3_uri_parameter(filename, "codec");
        if (0 != param)
            codecName = param;
    }
    struct Codec *codec;
    if (0 == codecName)
        codec = (struct Codec *)ctx;
    else
    {
        codec = 0;
        for (size_t i = 0; (sizeof codecs / sizeof codecs[0]) > i; i++)
            if (0 == strcmp(codecs[i].name, codecName))
            {
                codec = &codecs[i];
                break;
            }
    }
    if (0 != codec)
    {
        void *codecCtx = 0;
        int ret = codec->createContext(&codecCtx);
        if (SQLITE_OK != ret)
            return ret;
        methods->zHdr = codec->name;
        methods->pCtx = codecCtx;
        methods->xCompressBound = codec->compressBound;
        methods->xCompress = codec->compress;
        methods->xUncompress = codec->uncompress;
        methods->xCompressClose = codec->compressClose;
    }
    return SQLITE_OK;
}
int zvfs_create(const char *name, const char *codecName)
{
    for (size_t i = 0; (sizeof codecs / sizeof codecs[0]) > i; i++)
        if (0 == strcmp(codecs[i].name, codecName))
            return zipvfs_create_vfs_v3(name, 0, &codecs[i], zvfs_autodetect);
    return SQLITE_MISUSE;
}
#endif
