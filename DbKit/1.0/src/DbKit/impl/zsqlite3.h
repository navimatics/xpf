/*
 * DbKit/impl/zsqlite3.h
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef DBKIT_IMPL_ZSQLITE3_H_INCLUDED
#define DBKIT_IMPL_ZSQLITE3_H_INCLUDED

#include <sqlite3.h>
#include <zipvfs.h>

#endif // DBKIT_IMPL_ZSQLITE3_H_INCLUDED
