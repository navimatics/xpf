/*
 * DbKit/impl/Database.cpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#include <DbKit/impl/Database.hpp>
#include "zsqlite3.h"
#include "zvfs.h"

#define _connection                     ((sqlite3 *)_connectionHandle)
#define _statements                     ((sqlite3_stmt **)_statementHandles)

namespace DbKit {
namespace impl {

struct Database::backup_t
{
    sqlite3 *destination;
    sqlite3_backup *backup;
    static backup_t *create(sqlite3 *destination, sqlite3_backup *backup)
    {
        backup_t *self = (backup_t *)Base::Malloc(sizeof(backup_t));
        self->destination = destination;
        self->backup = backup;
        return self;
    }
    void destroy()
    {
        sqlite3_backup_finish(backup);
        sqlite3_close(destination);
        free(this);
    }
};

static int zvfs_register()
{
    static int ret = SQLITE_MISUSE;
    execute_once
    {
        if (SQLITE_OK == zvfs_create("ZV-lz4", "lz4") &&
            SQLITE_OK == zvfs_create("ZV-lz4hc", "lz4hc") &&
            SQLITE_OK == zvfs_create("ZV-deflate", "deflate") &&
            SQLITE_OK == zvfs_create("ZV-deflateRaw", "deflateRaw"))
            ret = SQLITE_OK;
    }
    return ret;
}

inline Database::statement_t Database::_statementHandle(int statementIndex)
{
    ssize_t statementCount = Base::carr_length(_statementHandles);
    if (statementIndex >= statementCount)
        return 0;
    return _statementHandles[statementIndex];
}

const char *Database::errorDomain()
{
    static const char *s = Base::CStringConstant("DbKit::impl::Database")->chars();
    return s;
}
Database::Database(const char *path, const char *codec)
{
    if (0 == path)
        path = ":memory:";
    _path = Base::cstr_new(path);
    if (0 != codec && SQLITE_OK == zvfs_register())
        _zvfs = Base::cstr_newf("ZV-%s", codec);
}
Database::~Database()
{
    close();
    Base::cstr_release(_zvfs);
    Base::cstr_release(_path);
    if (0 != _lock)
        _lock->release();
}
Base::Object *Database::threadsafe()
{
    _lock = new Base::Lock;
    return Base::Object::threadsafe();
}
Base::Lock *Database::lock()
{
    return _lock;
}
const char *Database::path()
{
    Base::cstr_obj(_path)->autocollect();
    return _path;
}
int Database::open()
{
    synchronized (_lock)
    {
        if (0 != _connection)
            return 0;
        sqlite3 *connection = 0;
        int sqlres = sqlite3_open_v2(_path, &connection,
            SQLITE_OPEN_READWRITE | SQLITE_OPEN_CREATE | SQLITE_OPEN_NOMUTEX | SQLITE_OPEN_URI,
            _zvfs);
        _connectionHandle = (connection_t)connection;
        if (SQLITE_OK != sqlres)
        {
            _setLastError(sqlres);
            _connectionHandle = 0;
            sqlite3_close(connection);
                /* sqlite3_close(0) is no-op */
            return -1;
        }
        sqlite3_busy_handler(_connection, _busyHandler, 0);
        return 0;
    }
}
int Database::close()
{
    synchronized (_lock)
    {
        if (0 == _connection)
            return 0;
        if (0 != _backup)
        {
            _backup->destroy();
            _backup = 0;
        }
        for (int statementIndex = 0, statementCount = Base::carr_length(_statementHandles);
            statementIndex < statementCount; statementIndex++)
            sqlite3_finalize(_statements[statementIndex]);
                /* sqlite3_finalize(0) is no-op */
        Base::carr_release(_statementHandles);
        _statementHandles = 0;
        int sqlres = sqlite3_close(_connection);
        if (SQLITE_OK != sqlres)
            LOG("warncrit: will leak connection handle due to sqlite3_close(): %s", sqlite3_errmsg(_connection));
        _connectionHandle = 0;
        return 0;
    }
}
Database::connection_t Database::connectionHandle()
{
    return _connectionHandle;
}
Database::statement_t Database::statementHandle(int statementIndex)
{
    synchronized (_lock)
    {
        assert(0 != _connection);
        assert(0 <= statementIndex);
        return _statementHandle(statementIndex);
    }
}
int Database::nextStatementIndex()
{
    synchronized (_lock)
    {
        assert(0 != _connection);
        return Base::carr_length(_statementHandles);
    }
}
const char *Database::statementText(int statementIndex)
{
    synchronized (_lock)
    {
        assert(0 != _connection);
        assert(0 <= statementIndex);
        sqlite3_stmt *statement = (sqlite3_stmt *)_statementHandle(statementIndex);
        return 0 != statement ? Base::cstring(sqlite3_sql(statement)) : 0;
    }
}
int Database::setStatementText(int statementIndex, const char *text)
{
    synchronized (_lock)
    {
        assert(0 != _connection);
        assert(0 <= statementIndex);
        sqlite3_stmt *statement = 0;
        if (0 != text)
        {
            int sqlres = sqlite3_prepare_v2(_connection, text, -1, &statement, 0);
            if (SQLITE_OK != sqlres)
            {
                _setLastError(sqlres);
                return -1;
            }
        }
        ssize_t statementCount = Base::carr_length(_statementHandles);
        if (statementIndex < statementCount)
            sqlite3_finalize(_statements[statementIndex]);
                /* sqlite3_finalize(0) is no-op */
        else
            _statementHandles = (statement_t *)Base::carr_concat(_statementHandles,
                0, statementIndex - statementCount + 1, sizeof(statement_t));
                /* extend statementHandles array to contain statementIndex + 1 statements */
        _statements[statementIndex] = statement;
        return 0;
    }
}
int Database::params(int statementIndex, const char *format, ...)
{
    synchronized (_lock)
    {
        assert(0 != _connection);
        assert(0 <= statementIndex);
        va_list ap;
        va_start(ap, format);
        int result = _vparams(_statementHandle(statementIndex), format, ap);
        va_end(ap);
        return result;
    }
}
int Database::vparams(int statementIndex, const char *format, va_list ap0)
{
    synchronized (_lock)
    {
        assert(0 != _connection);
        assert(0 <= statementIndex);
        va_list ap;
        va_copy(ap, ap0);
        int result = _vparams(_statementHandle(statementIndex), format, ap);
        va_end(ap);
        return result;
    }
}
int Database::columns(int statementIndex, const char *format, ...)
{
    synchronized (_lock)
    {
        assert(0 != _connection);
        assert(0 <= statementIndex);
        va_list ap;
        va_start(ap, format);
        int result = _vcolumns(_statementHandle(statementIndex), format, ap);
        va_end(ap);
        return result;
    }
}
int Database::vcolumns(int statementIndex, const char *format, va_list ap0)
{
    synchronized (_lock)
    {
        assert(0 != _connection);
        assert(0 <= statementIndex);
        va_list ap;
        va_copy(ap, ap0);
        int result = _vcolumns(_statementHandle(statementIndex), format, ap);
        va_end(ap);
        return result;
    }
}
int Database::step(int statementIndex, const char *format, ...)
{
    synchronized (_lock)
    {
        assert(0 != _connection);
        assert(0 <= statementIndex);
        va_list ap;
        va_start(ap, format);
        int result = _vstep(_statementHandle(statementIndex), format, ap);
        va_end(ap);
        return result;
    }
}
int Database::vstep(int statementIndex, const char *format, va_list ap0)
{
    synchronized (_lock)
    {
        assert(0 != _connection);
        assert(0 <= statementIndex);
        va_list ap;
        va_copy(ap, ap0);
        int result = _vstep(_statementHandle(statementIndex), format, ap);
        va_end(ap);
        return result;
    }
}
int Database::finish(int statementIndex)
{
    synchronized (_lock)
    {
        assert(0 != _connection);
        assert(0 <= statementIndex);
        return _finish(_statementHandle(statementIndex));
    }
}
int Database::exec(int statementIndex, const char *format, ...)
{
    synchronized (_lock)
    {
        assert(0 != _connection);
        assert(0 <= statementIndex);
        va_list ap;
        va_start(ap, format);
        int result = _vexec(_statementHandle(statementIndex), format, ap);
        va_end(ap);
        return result;
    }
}
int Database::vexec(int statementIndex, const char *format, va_list ap0)
{
    synchronized (_lock)
    {
        assert(0 != _connection);
        assert(0 <= statementIndex);
        va_list ap;
        va_copy(ap, ap0);
        int result = _vexec(_statementHandle(statementIndex), format, ap);
        va_end(ap);
        return result;
    }
}
int Database::exec(const char *text, const char *format, ...)
{
    synchronized (_lock)
    {
        assert(0 != _connection);
        sqlite3_stmt *statement;
        int sqlres = sqlite3_prepare_v2(_connection, text, -1, &statement, 0);
        if (SQLITE_OK != sqlres)
        {
            _setLastError(sqlres);
            return -1;
        }
        va_list ap;
        va_start(ap, format);
        int result = _vexec((statement_t)statement, format, ap);
        va_end(ap);
        sqlite3_finalize(statement);
        return result;
    }
}
int Database::vexec(const char *text, const char *format, va_list ap0)
{
    synchronized (_lock)
    {
        assert(0 != _connection);
        sqlite3_stmt *statement;
        int sqlres = sqlite3_prepare_v2(_connection, text, -1, &statement, 0);
        if (SQLITE_OK != sqlres)
        {
            _setLastError(sqlres);
            return -1;
        }
        va_list ap;
        va_copy(ap, ap0);
        int result = _vexec((statement_t)statement, format, ap);
        va_end(ap);
        sqlite3_finalize(statement);
        return result;
    }
}
int Database::params(statement_t statementHandle, const char *format, ...)
{
    synchronized (_lock)
    {
        assert(0 != _connection);
        va_list ap;
        va_start(ap, format);
        int result = _vparams(statementHandle, format, ap);
        va_end(ap);
        return result;
    }
}
int Database::vparams(statement_t statementHandle, const char *format, va_list ap0)
{
    synchronized (_lock)
    {
        assert(0 != _connection);
        va_list ap;
        va_copy(ap, ap0);
        int result = _vparams(statementHandle, format, ap);
        va_end(ap);
        return result;
    }
}
int Database::columns(statement_t statementHandle, const char *format, ...)
{
    synchronized (_lock)
    {
        assert(0 != _connection);
        va_list ap;
        va_start(ap, format);
        int result = _vcolumns(statementHandle, format, ap);
        va_end(ap);
        return result;
    }
}
int Database::vcolumns(statement_t statementHandle, const char *format, va_list ap0)
{
    synchronized (_lock)
    {
        assert(0 != _connection);
        va_list ap;
        va_copy(ap, ap0);
        int result = _vcolumns(statementHandle, format, ap);
        va_end(ap);
        return result;
    }
}
int Database::step(statement_t statementHandle, const char *format, ...)
{
    synchronized (_lock)
    {
        assert(0 != _connection);
        va_list ap;
        va_start(ap, format);
        int result = _vstep(statementHandle, format, ap);
        va_end(ap);
        return result;
    }
}
int Database::vstep(statement_t statementHandle, const char *format, va_list ap0)
{
    synchronized (_lock)
    {
        assert(0 != _connection);
        va_list ap;
        va_copy(ap, ap0);
        int result = _vstep(statementHandle, format, ap);
        va_end(ap);
        return result;
    }
}
int Database::finish(statement_t statementHandle)
{
    synchronized (_lock)
    {
        assert(0 != _connection);
        return _finish(statementHandle);
    }
}
int Database::exec(statement_t statementHandle, const char *format, ...)
{
    synchronized (_lock)
    {
        assert(0 != _connection);
        va_list ap;
        va_start(ap, format);
        int result = _vexec(statementHandle, format, ap);
        va_end(ap);
        return result;
    }
}
int Database::vexec(statement_t statementHandle, const char *format, va_list ap0)
{
    synchronized (_lock)
    {
        assert(0 != _connection);
        va_list ap;
        va_copy(ap, ap0);
        int result = _vexec(statementHandle, format, ap);
        va_end(ap);
        return result;
    }
}
int64_t Database::lastInsertRowid()
{
    synchronized (_lock)
    {
        assert(0 != _connection);
        return sqlite3_last_insert_rowid(_connection);
    }
}
int Database::transactionActive()
{
    synchronized (_lock)
    {
        assert(0 != _connection);
        return 0 == sqlite3_get_autocommit(_connection);
    }
}
int Database::backup(const char *dbname, const char *path, ssize_t pagecnt)
{
    synchronized (_lock)
    {
        assert(0 != _connection);
        int sqlres;
        if (0 == _backup)
        {
            if (0 > pagecnt)
                return 0; /* nothing to do */
            sqlite3 *destination = 0;
            sqlres = sqlite3_open_v2(path, &destination,
                SQLITE_OPEN_READWRITE | SQLITE_OPEN_CREATE | SQLITE_OPEN_NOMUTEX,
                _zvfs);
            if (SQLITE_OK != sqlres)
            {
                Base::Error::setLastError(_errorDomain(),
                    sqlres, "cannot open backup destination database");
                sqlite3_close(destination);
                    /* sqlite3_close(0) is no-op */
                return -1;
            }
            sqlite3_backup *backup = sqlite3_backup_init(destination, "main", _connection, dbname);
            if (0 == backup)
            {
                Base::Error::setLastError(_errorDomain(),
                    sqlite3_errcode(destination), sqlite3_errmsg(destination));
                sqlite3_close(destination);
                return -1;
            }
            _backup = backup_t::create(destination, backup);
        }
        else
        {
            /* !!!:
             * Should check that passed in "dbname" and "path" parameters are valid
             * (same as in first backup() call).
             */
            if (0 > pagecnt)
            {
                _backup->destroy();
                _backup = 0;
                return 0;
            }
        }
        if (0 == pagecnt)
        {
            /* backup remaining pages */
            sqlres = sqlite3_backup_step(_backup->backup, -1);
            for (int count = 0;;)
            {
                if (SQLITE_OK == sqlres)
                    ;
                else if (SQLITE_BUSY == sqlres || SQLITE_LOCKED == sqlres)
                    _busyHandler(0, count++);
                else
                    break;
                sqlres = sqlite3_backup_step(_backup->backup, -1);
            }
        }
        else
        {
            /* backup specified number of pages */
            sqlres = sqlite3_backup_step(_backup->backup, pagecnt);
        }
        if (SQLITE_OK == sqlres || SQLITE_BUSY == sqlres || SQLITE_LOCKED == sqlres)
            return +1;
        else if (SQLITE_DONE == sqlres)
        {
            Base::Error::setLastError(0); /* clear last error */
            _backup->destroy();
            _backup = 0;
            return 0;
        }
        else
        {
            /* error */
            Base::Error::setLastError(_errorDomain(), sqlres, "backup() step failed");
                /* SQLite does not appear to remember the proper error message
                 * on neither the source or destination database.
                 */
            _backup->destroy();
            _backup = 0;
            return -1;
        }
    }
}
Database::statement_t Database::_prepare(const char *text)
{
    synchronized (_lock)
    {
        assert(0 != _connection);
        sqlite3_stmt *statement;
        int sqlres = sqlite3_prepare_v2(_connection, text, -1, &statement, 0);
        if (SQLITE_OK != sqlres)
        {
            _setLastError(sqlres);
            return 0;
        }
        return (statement_t)statement;
    }
}
int Database::_unprepare(statement_t statementHandle)
{
    synchronized (_lock)
    {
        assert(0 != _connection);
        sqlite3_finalize((sqlite3_stmt *)statementHandle);
        return 0;
    }
}
void Database::__dumpQueryPlan(int statementIndex)
{
    synchronized (_lock)
    {
        assert(0 != _connection);
        assert(0 <= statementIndex);
        __dumpQueryPlan(_statementHandle(statementIndex));
    }
}
void Database::__dumpQueryPlan(statement_t statementHandle)
{
    sqlite3_stmt *statement = (sqlite3_stmt *)statementHandle;
    const char *sql = sqlite3_sql(statement);
    if (0 == sql)
    {
        LOG("cannot get SQL text for statement %p", statementHandle);
        return;
    }
    sqlite3_stmt *explainStatement;
    const char *explainSql = Base::cstr_newf("EXPLAIN QUERY PLAN %s", sql);
    int sqlres = sqlite3_prepare_v2(sqlite3_db_handle(statement), explainSql, -1, &explainStatement, 0);
    Base::cstr_release(explainSql);
    if (SQLITE_OK != sqlres)
    {
        LOG("cannot create EXPLAIN QUERY PLAN for statement %p", statementHandle);
        return;
    }
    const char *plan = Base::cstr_new(0);
    while (SQLITE_ROW == sqlite3_step(explainStatement))
    {
        int s = sqlite3_column_int(explainStatement, 0);
        int o = sqlite3_column_int(explainStatement, 1);
        int f = sqlite3_column_int(explainStatement, 2);
        const char *d = (const char *)sqlite3_column_text(explainStatement, 3);
        const char *row = Base::cstr_newf("    %-4d %-4d %-4d %s\n", s, o, f, d);
        plan = Base::cstr_concat(plan, row);
        Base::cstr_release(row);
    }
    sqlite3_finalize(explainStatement);
    LOG("\n%s", plan);
    Base::cstr_release(plan);
}
const char *Database::_errorDomain()
{
    return Database::errorDomain();
}
void Database::_setLastError(int sqlres)
{
    switch (sqlres)
    {
    case SQLITE_OK: case SQLITE_ROW: case SQLITE_DONE:
        Base::Error::setLastError(_errorDomain(), sqlres, "unknown error");
        break;
    case SQLITE_MISUSE:
        /* sqlite does not always set its error code when it returns SQLITE_MISUSE */
        Base::Error::setLastError(_errorDomain(), sqlres, "library misuse");
        break;
    default:
        Base::Error::setLastError(_errorDomain(), sqlres,
            0 != _connection ? sqlite3_errmsg(_connection) : 0);
        break;
    }
}
int Database::_vparams(statement_t statementHandle, const char *&format, va_list &ap)
{
    sqlite3_stmt *statement = (sqlite3_stmt *)statementHandle;
    if (0 == format)
        format = "";
    bool transient = false;
    int iparam = 1;
    for (const char *p = format;; p++)
    {
        int sqlres = SQLITE_OK;
        switch (*p)
        {
        case '\0':
            format = p;
            return 0;
        case ';':
            format = p + 1;
            return 0;
        case ' ': case '\t': case '\n': case '\v': case '\f': case '\r':
            continue;
        case '!':
            transient = true;
            continue;
        case '0': case '1': case '2': case '3': case '4':
        case '5': case '6': case '7': case '8': case '9':
            if (format < p && isdigit(p[-1]))
                iparam = iparam * 10 + *p - '0';
            else
                iparam = *p - '0';
            continue;
        case '.':
            sqlres = sqlite3_bind_null(statement, iparam);
            break;
        case 'i':
            sqlres = sqlite3_bind_int(statement, iparam, va_arg(ap, int));
            break;
        case 'u':
            sqlres = sqlite3_bind_int64(statement, iparam, va_arg(ap, unsigned));
            break;
        case 'l':
            sqlres = sqlite3_bind_int64(statement, iparam, va_arg(ap, long));
            break;
        case 'q':
            sqlres = sqlite3_bind_int64(statement, iparam, va_arg(ap, int64_t));
            break;
        case 'f':
            sqlres = sqlite3_bind_double(statement, iparam, va_arg(ap, double));
            break;
        case 's':
            sqlres = sqlite3_bind_text(statement, iparam, va_arg(ap, const char *), -1,
                transient ? SQLITE_TRANSIENT : SQLITE_STATIC);
            break;
        case '*':
            sqlres = sqlite3_bind_text(statement, iparam, va_arg(ap, Base::Object *)->strrepr(), -1,
                transient ? SQLITE_TRANSIENT : SQLITE_STATIC);
            break;
        case '^':
            {
                const void *bufptr = va_arg(ap, const void *);
                size_t bufsiz = va_arg(ap, size_t);
                sqlres = sqlite3_bind_blob(statement, iparam, bufptr, bufsiz,
                    transient ? SQLITE_TRANSIENT : SQLITE_STATIC);
            }
            break;
        default:
            sqlres = SQLITE_MISUSE;
            break;
        }
        if (SQLITE_OK != sqlres)
        {
            _setLastError(sqlres);
            format = p;
            return -1;
        }
        iparam++;
    }
}
int Database::_vcolumns(statement_t statementHandle, const char *&format, va_list &ap, bool alloc)
{
    sqlite3_stmt *statement = (sqlite3_stmt *)statementHandle;
    if (0 == format)
        format = "";
    int icolumn = 1;
    for (const char *p = format;; p++)
    {
        int sqlres = SQLITE_OK;
        switch (*p)
        {
        case '\0':
            format = p;
            return 0;
        case ';':
            format = p + 1;
            return 0;
        case ' ': case '\t': case '\n': case '\v': case '\f': case '\r':
            continue;
        case '0': case '1': case '2': case '3': case '4':
        case '5': case '6': case '7': case '8': case '9':
            if (format < p && isdigit(p[-1]))
                icolumn = icolumn * 10 + *p - '0';
            else
                icolumn = *p - '0';
            continue;
        case '.':
            break;
        case 'i':
            *va_arg(ap, int *) = sqlite3_column_int(statement, icolumn - 1);
            break;
        case 'u':
            *va_arg(ap, unsigned *) = sqlite3_column_int64(statement, icolumn - 1);
            break;
        case 'l':
            *va_arg(ap, long *) = sqlite3_column_int64(statement, icolumn - 1);
            break;
        case 'q':
            *va_arg(ap, int64_t *) = sqlite3_column_int64(statement, icolumn - 1);
            break;
        case 'f':
            *va_arg(ap, double *) = sqlite3_column_double(statement, icolumn - 1);
            break;
        case 's':
            if (!alloc)
                *va_arg(ap, const char **) =
                    (const char *)sqlite3_column_text(statement, icolumn - 1);
            else
                *va_arg(ap, const char **) = Base::cstring(
                    (const char *)sqlite3_column_text(statement, icolumn - 1));
            break;
        case '*':
            *va_arg(ap, Base::CString **) = Base::cstr_obj(Base::cstring(
                (const char *)sqlite3_column_text(statement, icolumn - 1)));
            break;
        case '^':
            {
                const void *bufptr = sqlite3_column_blob(statement, icolumn - 1);
                size_t bufsiz = sqlite3_column_bytes(statement, icolumn - 1);
                if (!alloc)
                    *va_arg(ap, const void **) = bufptr;
                else
                    *va_arg(ap, const void **) = Base::carray(bufptr, bufsiz, 1);
                size_t *sizptr = va_arg(ap, size_t *);
                if (0 != sizptr)
                    *sizptr = bufsiz;
            }
            break;
        default:
            sqlres = SQLITE_MISUSE;
            break;
        }
        if (SQLITE_OK != sqlres)
        {
            _setLastError(sqlres);
            format = p;
            return -1;
        }
        icolumn++;
    }
}
int Database::_vstep(statement_t statementHandle, const char *&format, va_list &ap)
{
    sqlite3_stmt *statement = (sqlite3_stmt *)statementHandle;
    int sqlres = sqlite3_step(statement);
    if (SQLITE_ROW == sqlres)
    {
        if (-1 != _vcolumns(statementHandle, format, ap))
            return +1;
        else
        {
            sqlite3_reset(statement);
            return -1;
        }
    }
    else if (SQLITE_DONE == sqlres)
    {
        Base::Error::setLastError(0); /* clear last error */
        sqlite3_reset(statement);
        return 0;
    }
    else
    {
        _setLastError(sqlres);
        sqlite3_reset(statement);
        return -1;
    }
}
int Database::_finish(statement_t statementHandle)
{
    sqlite3_stmt *statement = (sqlite3_stmt *)statementHandle;
    sqlite3_reset(statement);
    return 0;
}
int Database::_vexec(statement_t statementHandle, const char *&format, va_list &ap)
{
    if (-1 == _vparams(statementHandle, format, ap))
        return -1;
    sqlite3_stmt *statement = (sqlite3_stmt *)statementHandle;
    int sqlres = sqlite3_step(statement);
    if (SQLITE_ROW == sqlres)
    {
        if (-1 != _vcolumns(statementHandle, format, ap, true))
        {
            sqlite3_reset(statement);
            return +1;
        }
        else
        {
            sqlite3_reset(statement);
            return -1;
        }
    }
    else if (SQLITE_DONE == sqlres)
    {
        Base::Error::setLastError(0); /* clear last error */
        sqlite3_reset(statement);
        return 0;
    }
    else
    {
        _setLastError(sqlres);
        sqlite3_reset(statement);
        return -1;
    }
}
int Database::_busyHandler(void *data, int count)
{
    static const uint8_t delays[] = { 1, 2, 5, 10, 15, 20, 25, 25, 25, 50, 50, 100 };
    int delay = (int)NELEMS(delays) > count ? delays[count] : delays[NELEMS(delays) - 1];
    sqlite3_sleep(delay);
    return 1;
}

}
}
