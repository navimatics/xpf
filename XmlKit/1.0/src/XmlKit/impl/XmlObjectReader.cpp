/*
 * XmlKit/impl/XmlObjectReader.cpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#include <XmlKit/impl/XmlObjectReader.hpp>

namespace XmlKit {
namespace impl {

XmlObjectEvent *XmlObjectEvent::create(XmlQName qname, Object *object)
{
    size_t namesize = strlen(qname.lname) + 1 + strlen(qname.xmlns) + 1;
    XmlObjectEvent *self = new (namesize) XmlObjectEvent;
    char *p = (char *)self + sizeof(XmlObjectEvent);
    self->_qname.xmlns = p;
    p = stpcpy(p, qname.xmlns) + 1;
    self->_qname.lname = p;
    p = stpcpy(p, qname.lname) + 1;
    if (0 != object)
        self->_object = object->retain();
    return self;
}
XmlObjectEvent::~XmlObjectEvent()
{
    if (0 != _object)
        _object->release();
}

XmlObjectReader::XmlObjectReader(Base::Stream *stream) : XmlParser(stream)
{
    _stack = new Base::Array;
    _stack->autocollectObjects(false);
    _queue = new Base::Array;
    _queue->autocollectObjects(false);
}
XmlObjectReader::~XmlObjectReader()
{
    _queue->release();
    _stack->release();
    if (0 != _nameset)
        _nameset->release();
}
void XmlObjectReader::reset()
{
    _finished = false;
    _ignoreMixedContentText = false;
    if (0 != _nameset)
        _nameset->removeAllObjects();
    _stack->removeAllObjects();
    _queue->removeAllObjects();
    _qindex = 0;
    XmlParser::reset();
}
void XmlObjectReader::addQName(XmlQName qname)
{
    if (0 == _nameset)
    {
        _nameset = new Base::Set;
        _nameset->autocollectObjects(false);
    }
    Base::CString *str = Base::CString::create(qname.lname);
    _nameset->addObject(str);
    str->release();
}
void XmlObjectReader::removeQName(XmlQName qname)
{
    if (0 != _nameset)
        _nameset->removeObject(BASE_ALLOCA_OBJECT(Base::TransientCString, qname.lname));
}
bool XmlObjectReader::ignoreMixedContentText()
{
    return _ignoreMixedContentText;
}
void XmlObjectReader::setIgnoreMixedContentText(bool value)
{
    _ignoreMixedContentText = value;
}
XmlObjectEvent *XmlObjectReader::readObject()
{
    for (;;)
    {
        if (_qindex < _queue->count())
            return (XmlObjectEvent *)_queue->object(_qindex++);
        _queue->removeAllObjects();
        _qindex = 0;
        if (_finished)
            return 0;
        _finished = 0 >= parse();
    }
}
bool XmlObjectReader::finished()
{
    return _finished;
}
void XmlObjectReader::setFinished(bool value)
{
    _finished = value;
}
Base::Object *XmlObjectReader::ancestorObject(ssize_t index)
{
    return _stack->object(index);
}
void XmlObjectReader::objectComplete(XmlQName qname, Object *obj)
{
    if (0 != _nameset)
    {
        bool contains = _nameset->containsObject(BASE_ALLOCA_OBJECT(Base::TransientCString, qname.lname));
        if (!contains)
            return;
    }
    else if (1 < _stack->count())
        return;
    XmlObjectEvent *event = XmlObjectEvent::create(qname, obj);
    _queue->addObject(event);
    event->release();
}
void XmlObjectReader::startElement(XmlQName qname, XmlAttribute *attributes, size_t attributeCount)
{
    if (0 < attributeCount)
    {
        Base::Map *map = new Base::Map;
        for (size_t i = 0; attributeCount > i; i++)
        {
            Base::CString *keyobj = Base::CString::createf("@%s", attributes[i].qname.lname);
            Base::CString *valobj = Base::CString::create(attributes[i].value);
            map->setObject(keyobj, valobj);
            valobj->release();
            keyobj->release();
        }
        _stack->addObject(map);
        map->release();
    }
    else
        _stack->addObject(0);
}
void XmlObjectReader::endElement(XmlQName qname)
{
    Object *obj = _stack->object(-1);
    size_t count = _stack->count();
    if (1 == count)
        objectComplete(qname, obj);
    else
    {
        Object *anc = _stack->object(-2);
        Base::Map *map = dynamic_cast<Base::Map *>(anc);
        if (0 == map)
        {
            static Base::CString *textkey = Base::CStringConstant("#text");
            map = new Base::Map;
            if (0 != anc && !_ignoreMixedContentText)
                map->setObject(textkey, anc);
            _stack->replaceObject(-2, map);
            map->release();
        }
        Base::CString *key = Base::CString::create(qname.lname);
        Object *val = map->object(key);
        if (0 == val)
            map->setObject(key, obj);
        else
        {
            Base::Array *array = dynamic_cast<Base::Array *>(val);
            if (0 == array)
            {
                array = new Base::Array(2);
                array->addObject(val);
                array->addObject(obj);
                map->setObject(key, array);
                array->release();
            }
            else
                array->addObject(obj);
        }
        key->release();
        objectComplete(qname, obj);
    }
    _stack->removeObject(-1);
}
void XmlObjectReader::characterData(const char *s, size_t len)
{
    Base::Object *obj = _stack->object(-1);
    if (0 == obj)
    {
        Base::CString *newstr = Base::CString::create(s, len);
        _stack->replaceObject(-1, newstr);
        newstr->release();
    }
    else if (Base::CString *str = dynamic_cast<Base::CString *>(obj))
    {
        str->retain();
        str = str->_concat(s, len);
        _stack->replaceObject(-1, str);
        str->release();
    }
    else if (!_ignoreMixedContentText)
    {
        static Base::CString *textkey = Base::CStringConstant("#text");
        Base::Map *map = (Base::Map *)obj;
        Base::CString *str = (Base::CString *)map->object(textkey);
        if (0 == str)
        {
            str = Base::CString::create(s, len);
            map->setObject(textkey, str);
            str->release();
        }
        else
        {
            str->retain();
            str = str->_concat(s, len);
            map->setObject(textkey, str);
            str->release();
        }
    }
}

}
}
