/*
 * XmlKit/impl/XmlObjectWriter.cpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#include <XmlKit/impl/XmlObjectWriter.hpp>

namespace XmlKit {
namespace impl {

XmlObjectWriter::XmlObjectWriter(Base::Stream *stream, bool pretty) : XmlWriter(stream, pretty)
{
}
bool XmlObjectWriter::writeObject(XmlQName qname, Base::Object *obj)
{
    mark_and_collect
        return writeObjectInternal(qname, obj);
}
bool XmlObjectWriter::writeObject(XmlObjectEvent *event)
{
    mark_and_collect
        return writeObjectInternal(event->qname(), event->object());
}
bool XmlObjectWriter::writeObjectInternal(XmlQName qname, Base::Object *obj)
{
    if (Base::Map *map = dynamic_cast<Base::Map *>(obj))
    {
        Base::CString **keys = (Base::CString **)map->keys();
        qsort(keys, Base::carr_length(keys), sizeof(Base::Object *), Base::Object::qsort_cmp);
        size_t attributeCount = 0;
        for (size_t i = 0, n = Base::carr_length(keys); n > i; i++)
        {
            const char *key = keys[i]->chars();
            if ('@' == key[0])
                attributeCount++;
        }
        XmlAttribute *attributes = 0 == attributeCount ?
            0 :
            (XmlAttribute *)alloca(attributeCount * sizeof(XmlAttribute));
        for (size_t i = 0, j = 0, n = Base::carr_length(keys); n > i; i++)
        {
            const char *key = keys[i]->chars();
            if ('@' == key[0])
            {
                attributes[j].qname.lname = key + 1;
                attributes[j].qname.xmlns = "";
                attributes[j].value = ((Base::CString *)map->object(keys[i]))->chars();
                j++;
            }
        }
        writeStart(qname, attributes, attributeCount);
        for (size_t i = 0, n = carr_length(keys); n > i; i++)
        {
            const char *key = keys[i]->chars();
            if ('@' != key[0])
            {
                if (0 != strcmp("#text", key))
                {
                    XmlQName qname = { key, "" };
                    Base::Object *obj = map->object(keys[i]);
                    writeObjectInternal(qname, obj);
                }
                else
                {
                    Base::CString *obj = (Base::CString *)map->object(keys[i]);
                    writeText(obj->chars(), obj->length());
                }
            }
        }
        return writeEnd(qname);
    }
    else if (Base::Array *array = dynamic_cast<Base::Array *>(obj))
    {
        for (size_t i = 0, n = array->count(); n > i; i++)
            writeObjectInternal(qname, array->object(i));
        return !finished();
    }
    else if (0 != obj)
    {
        writeStart(qname, 0, 0);
        const char *str = obj->strrepr();
        writeText(str, Base::cstr_length(str));
        return writeEnd(qname);
    }
    else
    {
        writeStart(qname, 0, 0);
        return writeEnd(qname);
    }
}

}
}
