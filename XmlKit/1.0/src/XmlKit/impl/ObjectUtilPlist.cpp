/*
 * XmlKit/impl/ObjectUtilPlist.cpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#include <XmlKit/impl/ObjectUtil.hpp>
#include <XmlKit/impl/PlistReader.hpp>
#include <XmlKit/impl/PlistWriter.hpp>

namespace XmlKit {
namespace impl {

Base::Object *ObjectReadPlist(const char *path)
{
    Base::Stream *stream = Base::FileStream::create(path, "rb");
    if (0 == stream)
        return 0;
    PlistReader *reader = new PlistReader(stream);
    Base::Object *obj = reader->readObject();
    if (0 != obj)
        obj->autocollect();
    reader->release();
    stream->release();
    return obj;
}
bool ObjectWritePlist(const char *path, Base::Object *obj)
{
    Base::Stream *stream = Base::FileStream::create(path, "Wb");
    if (0 == stream)
        return false;
    PlistWriter *writer = new PlistWriter(stream);
    bool result = writer->writeObject(obj);
    writer->release();
    result = result && 0 == stream->close();
    stream->release();
    return result;
}

}
}
