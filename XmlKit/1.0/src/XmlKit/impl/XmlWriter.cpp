/*
 * XmlKit/impl/XmlWriter.cpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#include <XmlKit/impl/XmlWriter.hpp>

namespace XmlKit {
namespace impl {

static Base::CString *XmlnsStackGetPrefix(Base::Array *stack, Base::CString *xmlns)
{
    Base::Map **pmap = (Base::Map **)stack->objects();
    for (ssize_t i = stack->count() - 1; 0 <= i; i--)
        if (pmap[i])
            if (Base::CString *prefix = (Base::CString *)pmap[i]->object(xmlns))
                return prefix;
    return 0;
}
static void XmlnsStackAddPrefix(Base::Array *stack, Base::CString *xmlns, Base::CString *prefix)
{
    Base::Map *map = (Base::Map *)stack->object(-1);
    if (0 == map)
    {
        map = new Base::Map;
        map->autocollectObjects(false);
        stack->replaceObject(-1, map);
        map->release();
    }
    map->setObject(xmlns, prefix);
}

XmlWriter::XmlWriter(Base::Stream *stream, bool pretty)
{
    if (0 != stream)
        _stream = (Base::Stream *)stream->retain();
    _indent = pretty ? 0 : -1;
    _nsstack = new Base::Array(8);
    _nsstack->autocollectObjects(false);
    _nsstack->addObject(0);
}
XmlWriter::~XmlWriter()
{
    _nsstack->release();
    if (0 != _stream)
        _stream->release();
}
bool XmlWriter::writeRaw(const char *chars, size_t len)
{
    if (_finished)
        return false;
    if ((size_t)-1 == len)
        len = strlen(chars);
    if (0 == len)
        return true;
    _finished = len != (size_t)writeBuffer(chars, len);
    return !_finished;
}
bool XmlWriter::writeStart(XmlQName qname, XmlAttribute *attributes, size_t attributeCount)
{
    _nsstack->addObject(0);
    if (0 <= _indent)
    {
        if (_starttag)
            writeRaw(">", 1);
        writeLine();
        writeRaw("<", 1);
        _indent++;
        _simpelem = true;
    }
    else
    {
        if (_starttag)
            writeRaw("><", 2);
        else
            writeRaw("<", 1);
    }
    _starttag = true;
    writeQName(qname, 'S');
    if (!_rootelem)
    {
        _rootelem = true;
        Base::Map *map = (Base::Map *)_nsstack->object(0);
        if (0 != map)
        {
            Base::Object **keys, **objs;
            map->getKeysAndObjects(keys, objs);
            for (size_t i = 0, n = Base::carr_length(keys); n > i; i++)
            {
                Base::CString *xmlns = (Base::CString *)keys[i];
                Base::CString *prefix = (Base::CString *)objs[i];
                writeRaw(" ", 1);
                writeXmlns(xmlns, prefix);
            }
        }
    }
    for (size_t i = 0; attributeCount > i; i++)
    {
        writeRaw(" ", 1);
        writeQName(attributes[i].qname, 'A');
        writeRaw("=\"", 2);
        writeRaw(attributes[i].value);
        writeRaw("\"", 1);
    }
    return !_finished;
}
bool XmlWriter::writeEnd(XmlQName qname)
{
    if (0 <= _indent)
    {
        _indent--;
        if (!_starttag && !_simpelem)
            writeLine();
        _simpelem = false;
    }
    if (_starttag)
    {
        writeRaw("/>", 2);
        _starttag = false;
    }
    else
    {
        writeRaw("</", 2);
        writeQName(qname, 'E');
        writeRaw(">", 1);
    }
    _nsstack->removeObject(-1);
    return !_finished;
}
bool XmlWriter::writeText(const char *text, size_t len)
{
    if (_starttag)
    {
        writeRaw(">", 1);
        _starttag = false;
    }
    if ((size_t)-1 == len)
        len = strlen(text);
    const char *q = text, *p = q, *endp = q + len;
    while (endp > p)
        switch (*p)
        {
        case '&':
            writeRaw(q, p - q);
            writeRaw("&amp;", 5);
            q = ++p;
            break;
        case '<':
            writeRaw(q, p - q);
            writeRaw("&lt;", 4);
            q = ++p;
            break;
        case '>':
            writeRaw(q, p - q);
            writeRaw("&gt;", 4);
            q = ++p;
            break;
        case '\"':
            writeRaw(q, p - q);
            writeRaw("&quot;", 6);
            q = ++p;
            break;
        default:
            ++p;
            break;
        }
    writeRaw(q, p - q);
    return !_finished;
}
bool XmlWriter::write(XmlEvent *event)
{
    switch (event->kind())
    {
    case 'S':
        return writeStart(event->qname(), event->attributes(), event->attributeCount());
    case 'E':
        return writeEnd(event->qname());
    case 'T':
        return writeText(event->text(), Base::cstr_length(event->text()));
    default:
        return true;
    }
}
bool XmlWriter::finished()
{
    return _finished;
}
void XmlWriter::setRootNamespace(const char *xmlns_, const char *prefix_)
{
    if (_rootelem)
        return;
    Base::CString *xmlns = Base::CString::create(xmlns_);
    Base::CString *prefix = Base::CString::create(prefix_);
    XmlnsStackAddPrefix(_nsstack, xmlns, prefix);
    prefix->release();
    xmlns->release();
}
ssize_t XmlWriter::writeBuffer(const void *buf, size_t size)
{
    if (0 == _stream)
        return 0;
    return _stream->write(buf, size);
}
bool XmlWriter::writeQName(XmlQName qname, char kind)
{
    if (qname.xmlns[0])
    {
        Base::CString *xmlns = Base::CString::create(qname.xmlns);
        Base::CString *prefix = XmlnsStackGetPrefix(_nsstack, xmlns);
        if (0 == prefix)
        {
            if (26 > _nsindex)
                prefix = Base::CString::createf("%c", 'a' + _nsindex++);
            else
                prefix = Base::CString::createf("p%i", _nsindex++);
            XmlnsStackAddPrefix(_nsstack, xmlns, prefix);
            prefix->release();
        }
        else
            kind = 0;
        if ('A' == kind)
        {
            writeXmlns(xmlns, prefix);
            writeRaw(" ", 1);
        }
        if (0 < prefix->length())
        {
            writeRaw(prefix->chars(), prefix->length());
            writeRaw(":", 1);
        }
        writeRaw(qname.lname);
        if ('S' == kind)
        {
            writeRaw(" ", 1);
            writeXmlns(xmlns, prefix);
        }
        xmlns->release();
    }
    else
        writeRaw(qname.lname);
    return !_finished;
}
bool XmlWriter::writeXmlns(Base::CString *xmlns, Base::CString *prefix)
{
    if (0 < prefix->length())
    {
        writeRaw("xmlns:", 6);
        writeRaw(prefix->chars(), prefix->length());
    }
    else
        writeRaw("xmlns", 5);
    writeRaw("=\"", 2);
    writeRaw(xmlns->chars(), xmlns->length());
    writeRaw("\"", 1);
    return !_finished;
}
void XmlWriter::writeLine()
{
    if (!_startdoc)
    {
        _startdoc = true;
        return;
    }
    switch (_indent % 4)
    {
    case 0:
        writeRaw("\n", 1);
        break;
    case 1:
        writeRaw("\n  ", 3);
        break;
    case 2:
        writeRaw("\n    ", 5);
        break;
    case 3:
        writeRaw("\n      ", 7);
        break;
    }
    for (int i = 0, n = _indent / 4; n > i; i++)
        writeRaw("        ", 8);
}

}
}
