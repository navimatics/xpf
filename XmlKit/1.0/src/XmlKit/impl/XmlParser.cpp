/*
 * XmlKit/impl/XmlParser.cpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#include <XmlKit/impl/XmlParser.hpp>
#include <expat.h>

#define NSSEP                           '^'

namespace XmlKit {
namespace impl {

static inline XmlQName MakeXmlQName(const char *name)
{
    XmlQName qname;
    char *sep = (char *)strchr(name, NSSEP);
    if (0 == sep)
    {
        qname.lname = name;
        qname.xmlns = "";
    }
    else
    {
        *sep = '\0';
        qname.lname = sep + 1;
        qname.xmlns = name;
    }
    return qname;
}
static inline void RestoreXmlQName(XmlQName qname)
{
    if (qname.xmlns[0])
    {
        char *sep = (char *)qname.lname - 1;
        *sep = NSSEP;
    }
}

const char *XmlParser::errorDomain()
{
    static const char *s = Base::CStringConstant("XmlKit::impl::XmlParser")->chars();
    return s;
}
XmlParser::XmlParser(Base::Stream *stream)
{
    if (0 != stream)
        _stream = (Base::Stream *)stream->retain();
    _parser = XML_ParserCreateNS(0, NSSEP);
    if (0 == _parser)
        Base::MemoryAllocationError();
    XML_SetUserData((XML_ParserStruct *)_parser, this);
    XML_SetElementHandler((XML_ParserStruct *)_parser, startElementHandler, endElementHandler);
    XML_SetCharacterDataHandler((XML_ParserStruct *)_parser, characterDataHandler);
}
XmlParser::~XmlParser()
{
    XML_ParserFree((XML_ParserStruct *)_parser);
    if (0 != _stream)
        _stream->release();
}
void XmlParser::reset()
{
    XML_ParserReset((XML_ParserStruct *)_parser, 0);
    XML_SetUserData((XML_ParserStruct *)_parser, this);
    XML_SetElementHandler((XML_ParserStruct *)_parser, startElementHandler, endElementHandler);
    XML_SetCharacterDataHandler((XML_ParserStruct *)_parser, characterDataHandler);
}
ssize_t XmlParser::parse(const void *buf, size_t size)
{
    ssize_t bytes = size;
    if (0 == XML_Parse((XML_ParserStruct *)_parser, (const char *)buf, bytes, 0 == bytes))
    {
        Base::Error::setLastError(_errorDomain(),
            'P', XML_ErrorString(XML_GetErrorCode((XML_ParserStruct *)_parser)));
        return -1;
    }
    return bytes;
}
ssize_t XmlParser::parse()
{
    const int BufSize = 16384;
    void *buf = XML_GetBuffer((XML_ParserStruct *)_parser, BufSize);
    if (0 == buf)
    {
        Base::Error::setLastError(_errorDomain(),
            'P', XML_ErrorString(XML_GetErrorCode((XML_ParserStruct *)_parser)));
        return -1;
    }
    ssize_t bytes = readBuffer(buf, BufSize);
    if (-1 == bytes)
    {
        Base::Error::setLastError(_errorDomain(), 'I', 0);
        return -1;
    }
    if (0 == XML_ParseBuffer((XML_ParserStruct *)_parser, bytes, 0 == bytes))
    {
        Base::Error::setLastError(_errorDomain(),
            'P', XML_ErrorString(XML_GetErrorCode((XML_ParserStruct *)_parser)));
        return -1;
    }
    return bytes;
}
const char *XmlParser::_errorDomain()
{
    return XmlParser::errorDomain();
}
ssize_t XmlParser::readBuffer(void *buf, size_t size)
{
    if (0 == _stream)
        return 0;
    return _stream->read(buf, size);
}
void XmlParser::startElement(XmlQName qname, XmlAttribute *attributes, size_t attributeCount)
{
}
void XmlParser::endElement(XmlQName qname)
{
}
void XmlParser::characterData(const char *s, size_t len)
{
}
void XmlParser::startElementHandler(void *self, const char *name, const char **attv)
{
    XmlQName qname = MakeXmlQName(name);
    size_t attributeCount = 0;
    for (const char **pp = attv; 0 != *pp; pp += 2)
        attributeCount++;
    XmlAttribute *attributes = 0 == attributeCount ?
        0 :
        (XmlAttribute *)alloca(attributeCount * sizeof(XmlAttribute));
    for (size_t i = 0; attributeCount > i; i++)
    {
        attributes[i].qname = MakeXmlQName(attv[i * 2 + 0]);
        attributes[i].value = attv[i * 2 + 1];
    }
    ((XmlParser *)self)->startElement(qname, attributes, attributeCount);
    for (size_t i = 0; attributeCount > i; i++)
        RestoreXmlQName(attributes[i].qname);
    RestoreXmlQName(qname);
}
void XmlParser::endElementHandler(void *self, const char *name)
{
    XmlQName qname = MakeXmlQName(name);
    ((XmlParser *)self)->endElement(qname);
    RestoreXmlQName(qname);
}
void XmlParser::characterDataHandler(void *self, const char *s, int len)
{
    ((XmlParser *)self)->characterData(s, len);
}

}
}
