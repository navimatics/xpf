/*
 * XmlKit/impl/XmlReader.cpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#include <XmlKit/impl/XmlReader.hpp>

namespace XmlKit {
namespace impl {

XmlEvent *XmlEvent::create(int kind, XmlQName qname, XmlAttribute *attributes, size_t attributeCount)
{
    assert('S' == kind || 'E' == kind);
    size_t attrsize = 0;
    for (size_t i = 0; attributeCount > i; i++)
    {
        attrsize += strlen(attributes[i].qname.lname) + 1;
        attrsize += strlen(attributes[i].qname.xmlns) + 1;
        attrsize += strlen(attributes[i].value) + 1;
    }
    size_t namesize = strlen(qname.lname) + 1 + strlen(qname.xmlns) + 1;
    XmlEvent *self = new (attributeCount * sizeof(XmlAttribute) + namesize + attrsize) XmlEvent;
    char *p = (char *)self + sizeof(XmlEvent);
    self->_kind = kind;
    self->_s.attributes = (XmlAttribute *)p;
    p = p + attributeCount * sizeof(XmlAttribute);
    self->_s.qname.xmlns = p;
    p = stpcpy(p, qname.xmlns) + 1;
    self->_s.qname.lname = p;
    p = stpcpy(p, qname.lname) + 1;
    for (size_t i = 0; attributeCount > i; i++)
    {
        self->_s.attributes[i].qname.xmlns = p;
        p = stpcpy(p, attributes[i].qname.xmlns) + 1;
        self->_s.attributes[i].qname.lname = p;
        p = stpcpy(p, attributes[i].qname.lname) + 1;
        self->_s.attributes[i].value = p;
        p = stpcpy(p, attributes[i].value) + 1;
    }
    self->_s.attributeCount = attributeCount;
    return self;
}
XmlEvent *XmlEvent::create(int kind, const char *s, size_t len)
{
    assert('T' == kind);
    XmlEvent *self = new XmlEvent;
    self->_kind = kind;
    self->_t.text = Base::cstr_new(s, len);
    return self;
}
XmlEvent::~XmlEvent()
{
    switch (_kind)
    {
    case 'S':
    case 'E':
        break;
    case 'T':
        Base::cstr_release(_t.text);
        break;
    default:
        break;
    }
}
void XmlEvent::_concat(const char *s, size_t len)
{
    assert('T' == _kind);
    _t.text = Base::cstr_concat(_t.text, s, len);
}

XmlReader::XmlReader(Base::Stream *stream) : XmlParser(stream)
{
    _queue = new Base::Array;
    _queue->autocollectObjects(false);
}
XmlReader::~XmlReader()
{
    _queue->release();
    if (0 != _textev)
        _textev->release();
}
void XmlReader::reset()
{
    _finished = false;
    if (0 != _textev)
    {
        _textev->release();
        _textev = 0;
    }
    _queue->removeAllObjects();
    _qindex = 0;
    XmlParser::reset();
}
XmlEvent *XmlReader::read()
{
    for (;;)
    {
        if (_qindex < _queue->count())
            return (XmlEvent *)_queue->object(_qindex++);
        _queue->removeAllObjects();
        _qindex = 0;
        if (_finished)
            return 0;
        _finished = 0 >= parse();
    }
}
bool XmlReader::finished()
{
    return _finished;
}
void XmlReader::setFinished(bool value)
{
    _finished = value;
}
void XmlReader::startElement(XmlQName qname, XmlAttribute *attributes, size_t attributeCount)
{
    if (0 != _textev)
    {
        _queue->addObject(_textev);
        _textev->release();
        _textev = 0;
    }
    XmlEvent *ev = XmlEvent::create('S', qname, attributes, attributeCount);
    _queue->addObject(ev);
    ev->release();
}
void XmlReader::endElement(XmlQName qname)
{
    if (0 != _textev)
    {
        _queue->addObject(_textev);
        _textev->release();
        _textev = 0;
    }
    XmlEvent *ev = XmlEvent::create('E', qname);
    _queue->addObject(ev);
    ev->release();
}
void XmlReader::characterData(const char *s, size_t len)
{
    if (0 == _textev)
        _textev = XmlEvent::create('T', s, len);
    else
        _textev->_concat(s, len);
}

}
}
