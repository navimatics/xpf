/*
 * XmlKit/impl/PlistWriter.cpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#include <XmlKit/impl/PlistWriter.hpp>

namespace XmlKit {
namespace impl {

PlistWriter::PlistWriter(Base::Stream *stream, bool pretty) : XmlWriter(stream, pretty)
{
}
bool PlistWriter::writeObject(Base::Object *obj)
{
    mark_and_collect
    {
        XmlQName qname = { "plist", "" };
        XmlAttribute attributes[1] =
        {
            { { "version", "" }, "1.0" },
        };
        writeStart(qname, attributes, 1);
        writeObjectInternal(obj);
        return writeEnd(qname);
    }
}
bool PlistWriter::writeObjectInternal(Base::Object *obj)
{
    if (Base::Map *map = dynamic_cast<Base::Map *>(obj))
    {
        XmlQName qname = { "dict", "" };
        writeStart(qname, 0, 0);
        Base::CString **keys = (Base::CString **)map->keys();
        qsort(keys, Base::carr_length(keys), sizeof(Base::Object *), Base::Object::qsort_cmp);
        for (size_t i = 0, n = carr_length(keys); n > i; i++)
        {
            XmlQName qname = { "key", "" };
            writeStart(qname, 0, 0);
            writeText(keys[i]->chars(), keys[i]->length());
            writeEnd(qname);
            if (false == writeObjectInternal(map->object(keys[i])))
                return false;
        }
        return writeEnd(qname);
    }
    else if (Base::Array *array = dynamic_cast<Base::Array *>(obj))
    {
        XmlQName qname = { "array", "" };
        writeStart(qname, 0, 0);
        Object **objs = array->objects();
        for (size_t i = 0, n = array->count(); n > i; i++)
            if (false == writeObjectInternal(objs[i]))
                return false;
        return writeEnd(qname);
    }
    else if (Base::CString *str = dynamic_cast<Base::CString *>(obj))
    {
        XmlQName qname = { "string", "" };
        writeStart(qname, 0, 0);
        writeText(str->chars(), str->length());
        return writeEnd(qname);
    }
    else if (Base::Value *value = dynamic_cast<Base::Value *>(obj))
    {
        switch (value->kind())
        {
        case 'B':
            if (value->booleanValue())
            {
                XmlQName qname = { "true", "" };
                writeStart(qname, 0, 0);
                return writeEnd(qname);
            }
            else
            {
                XmlQName qname = { "false", "" };
                writeStart(qname, 0, 0);
                return writeEnd(qname);
            }
        case 'l':
            {
                XmlQName qname = { "integer", "" };
                writeStart(qname, 0, 0);
                writeText(value->strrepr());
                return writeEnd(qname);
            }
        case 'd':
            {
                XmlQName qname = { "real", "" };
                writeStart(qname, 0, 0);
                writeText(value->strrepr());
                return writeEnd(qname);
            }
        default:
            return false;
        }
    }
    else
        return false;
}

}
}
