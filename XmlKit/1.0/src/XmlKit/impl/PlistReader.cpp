/*
 * XmlKit/impl/PlistReader.cpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#include <XmlKit/impl/PlistReader.hpp>

namespace XmlKit {
namespace impl {

PlistReader::PlistReader(Base::Stream *stream) : XmlReader(stream)
{
    _stack = new Base::Array;
    _stack->autocollectObjects(false);
}
PlistReader::~PlistReader()
{
    if (0 != _text)
        _text->release();
    if (0 != _key)
        _key->release();
    if (0 != _root)
        _root->release();
    _stack->release();
}
Base::Object *PlistReader::readObject()
{
    for (;;)
    {
        XmlEvent *event = read();
        if (0 == event)
            break;
        switch (event->kind())
        {
        case 'S':
            {
                if (0 != _text)
                {
                    _text->release();
                    _text = 0;
                }
                XmlQName qname = event->qname();
                if ('\0' == qname.xmlns[0])
                {
                    if (0 == strcmp("plist", qname.lname))
                    {
                    }
                    else if (0 == strcmp("array", qname.lname))
                    {
                        Base::Array *array = new Base::Array;
                        addObject(array);
                        _stack->addObject(array);
                        array->release();
                    }
                    else if (0 == strcmp("dict", qname.lname))
                    {
                        Base::Map *map = new Base::Map;
                        addObject(map);
                        _stack->addObject(map);
                        map->release();
                    }
                }
                break;
            }
        case 'E':
            {
                XmlQName qname = event->qname();
                if ('\0' == qname.xmlns[0])
                {
                    if (0 == strcmp("plist", qname.lname))
                    {
                    }
                    else if (0 == strcmp("array", qname.lname))
                    {
                        _stack->removeObject(-1);
                    }
                    else if (0 == strcmp("dict", qname.lname))
                    {
                        _stack->removeObject(-1);
                    }
                    else if (0 == strcmp("key", qname.lname))
                    {
                        if (0 != _key)
                            _key->release();
                        _key = _text;
                        if (0 != _key)
                            _key->retain();
                    }
                    else if (0 == strcmp("false", qname.lname))
                    {
                        Base::Value *value = Base::Value::create(false);
                        addObject(value);
                        value->release();
                    }
                    else if (0 == strcmp("true", qname.lname))
                    {
                        Base::Value *value = Base::Value::create(true);
                        addObject(value);
                        value->release();
                    }
                    else if (0 == strcmp("integer", qname.lname))
                    {
                        Base::Value *value = Base::Value::create(0 != _text ? strtol(_text->chars(), 0, 0) : 0);
                        addObject(value);
                        value->release();
                    }
                    else if (0 == strcmp("real", qname.lname))
                    {
                        Base::Value *value = Base::Value::create(0 != _text ? strtod(_text->chars(), 0) : 0.0);
                        addObject(value);
                        value->release();
                    }
                    else if (0 == strcmp("string", qname.lname))
                    {
                        addObject(0 != _text ? _text : Base::CString::empty());
                    }
                    else if (0 == strcmp("date", qname.lname))
                    {
                        addObject(0 != _text ? _text : Base::CString::empty());
                    }
                    else if (0 == strcmp("data", qname.lname))
                    {
                        addObject(0 != _text ? _text : Base::CString::empty());
                    }
                }
                if (0 != _text)
                {
                    _text->release();
                    _text = 0;
                }
                break;
            }
        case 'T':
            {
                if (0 != _text)
                    _text->release();
                _text = Base::cstr_obj(event->text());
                if (0 != _text)
                    _text->retain();
                break;
            }
        default:
            break;
        }
    }
    return _root;
}
void PlistReader::addObject(Object *obj)
{
    if (0 == _stack->count())
    {
        if (0 != _root)
        {
            _root->release();
            _root = 0;
        }
        _root = obj->retain();
    }
    else if (0 != _key)
    {
        Base::Map *map = dynamic_cast<Base::Map *>(_stack->object(-1));
        if (0 != map)
            map->setObject(_key, obj);
        _key->release();
        _key = 0;
    }
    else
    {
        Base::Array *array = dynamic_cast<Base::Array *>(_stack->object(-1));
        if (0 != array)
            array->addObject(obj);
    }
}

}
}
