/*
 * XmlKit/XmlReaderWriterTest.cpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#include <XmlKit/XmlReader.hpp>
#include <XmlKit/XmlWriter.hpp>
#include <gtest/gtest.h>

class XmlTestReader : public XmlKit::XmlReader
{
public:
    XmlTestReader(const char **chunks, size_t length);
    void reset();
protected:
    ssize_t readBuffer(void *buf, size_t size);
private:
    const char **_chunks;
    size_t _clength;
    size_t _cindex;
};
XmlTestReader::XmlTestReader(const char **chunks, size_t length)
{
    _chunks = chunks;
    _clength = length;
}
void XmlTestReader::reset()
{
    _cindex = 0;
    XmlKit::XmlReader::reset();
}
ssize_t XmlTestReader::readBuffer(void *buf, size_t size)
{
    if (_clength <= _cindex)
        return 0;
    size_t len = strlen(_chunks[_cindex]);
    assert(size > len);
    memcpy(buf, _chunks[_cindex], len);
    _cindex++;
    return len;
}

class XmlTestWriter : public XmlKit::XmlWriter
{
public:
    XmlTestWriter(bool pretty = false);
    ~XmlTestWriter();
    const char *chars();
protected:
    ssize_t writeBuffer(const void *buf, size_t size);
private:
    const char *_xmltext;
};
XmlTestWriter::XmlTestWriter(bool pretty) : XmlWriter(0, pretty)
{
    _xmltext = Base::cstr_new(0);
}
XmlTestWriter::~XmlTestWriter()
{
    Base::cstr_obj(_xmltext)->release();
}
const char *XmlTestWriter::chars()
{
    return _xmltext;
}
ssize_t XmlTestWriter::writeBuffer(const void *buf, size_t size)
{
    _xmltext = Base::cstr_obj(_xmltext)->_concat((const char *)buf, size)->chars();
    return size;
}

static const char *JoinChunks(const char **chunks, size_t length)
{
    const char *s = Base::cstr_new(0);
    for (size_t i = 0; length > i; i++)
        s = Base::cstr_obj(s)->_concat(chunks[i])->chars();
    Base::cstr_obj(s)->autorelease();
    return s;
}

static const char *chunks[] =
{
    "<ro", "ot><e0/><e1/>",
    "<e>",
    "<v>val", "ue</v>",
    "<e><v>  another  value  </v></e>",
    "<", "/e>",
    "<e a1=\"v1\" a", "2=\"v2\">",
    "<e a0=\"v0\"/", ">"
    "</e>",
    "<t>0 &amp;", " 1 &l", "t; 2 &gt; 3 &quot; 4</t>"
    "<m>mixed0<m>mixed1<m/>mixed2</m>mixed3</m>"
    "<deep><deep><deep><deep><deep><deep><deep>0</deep></deep></deep></deep></deep></deep></deep>"
    "</root>"
};
static const char *indented =
    "<root>\n"
    "  <e0/>\n"
    "  <e1/>\n"
    "  <e>\n"
    "    <v>value</v>\n"
    "    <e>\n"
    "      <v>  another  value  </v>\n"
    "    </e>\n"
    "  </e>\n"
    "  <e a1=\"v1\" a2=\"v2\">\n"
    "    <e a0=\"v0\"/>\n"
    "  </e>\n"
    "  <t>0 &amp; 1 &lt; 2 &gt; 3 &quot; 4</t>\n"
    "  <m>mixed0\n"
    "    <m>mixed1\n"
    "      <m/>mixed2\n"
    "    </m>mixed3\n"
    "  </m>\n"
    "  <deep>\n"
    "    <deep>\n"
    "      <deep>\n"
    "        <deep>\n"
    "          <deep>\n"
    "            <deep>\n"
    "              <deep>0</deep>\n"
    "            </deep>\n"
    "          </deep>\n"
    "        </deep>\n"
    "      </deep>\n"
    "    </deep>\n"
    "  </deep>\n"
    "</root>";

TEST(XmlReaderWriterTest, ReadWrite)
{
    Base::Object::Mark();
    XmlTestReader *reader = new (Base::collect) XmlTestReader(chunks, NELEMS(chunks));
    XmlTestWriter *writer = new (Base::collect) XmlTestWriter;
    for (;;)
    {
        XmlKit::XmlEvent *event = reader->read();
        if (0 == event)
            break;
        writer->write(event);
    }
    const char *s = JoinChunks(chunks, NELEMS(chunks));
    ASSERT_STREQ(s, writer->chars());
    Base::Object::Collect();
}
TEST(XmlReaderWriterTest, ReadWriteReset)
{
    Base::Object::Mark();
    XmlTestReader *reader = new (Base::collect) XmlTestReader(chunks, NELEMS(chunks));
    XmlTestWriter *writer = new (Base::collect) XmlTestWriter;
    for (;;)
    {
        XmlKit::XmlEvent *event = reader->read();
        if (0 == event)
            break;
        writer->write(event);
    }
    const char *s = JoinChunks(chunks, NELEMS(chunks));
    ASSERT_STREQ(s, writer->chars());
    reader->reset();
    writer = new (Base::collect) XmlTestWriter;
    for (;;)
    {
        XmlKit::XmlEvent *event = reader->read();
        if (0 == event)
            break;
        writer->write(event);
    }
    s = JoinChunks(chunks, NELEMS(chunks));
    ASSERT_STREQ(s, writer->chars());
    Base::Object::Collect();
}
TEST(XmlReaderWriterTest, ReadPrettyWrite)
{
    Base::Object::Mark();
    XmlTestReader *reader = new (Base::collect) XmlTestReader(chunks, NELEMS(chunks));
    XmlTestWriter *writer = new (Base::collect) XmlTestWriter(true);
    for (;;)
    {
        XmlKit::XmlEvent *event = reader->read();
        if (0 == event)
            break;
        writer->write(event);
    }
    ASSERT_STREQ(indented, writer->chars());
    Base::Object::Collect();
}
static const char *chunksNS[] =
{
    "<a:root xmlns:a=\"ns-a\">",
    "<a:e>",
    "<b:e xmlns:b=\"ns-b\"/>",
    "<c:e xmlns:c=\"ns-b\"/>",
    "<e/>",
    "<e xmlns:d=\"ns-d\" d:a=\"d\" a:a=\"a\">",
    "<d:e/>",
    "</e>",
    "<e:e xmlns:e=\"ns-d\"/>",
    "</a:e>",
    "</a:root>"
};
TEST(XmlReaderWriterTest, ReadWriteNS)
{
    Base::Object::Mark();
    XmlTestReader *reader = new (Base::collect) XmlTestReader(chunksNS, NELEMS(chunksNS));
    XmlTestWriter *writer = new (Base::collect) XmlTestWriter;
    for (;;)
    {
        XmlKit::XmlEvent *event = reader->read();
        if (0 == event)
            break;
        writer->write(event);
    }
    const char *s = JoinChunks(chunksNS, NELEMS(chunksNS));
    ASSERT_STREQ(s, writer->chars());
    Base::Object::Collect();
}
static const char *chunksRootNS[] =
{
    "<root xmlns=\"ns-root\">",
    "<a:e xmlns:a=\"ns-a\">",
    "<e/>",
    "</a:e>",
    "</root>"
};
TEST(XmlReaderWriterTest, ReadWriteRootNS)
{
    Base::Object::Mark();
    XmlTestReader *reader = new (Base::collect) XmlTestReader(chunksRootNS, NELEMS(chunksRootNS));
    XmlTestWriter *writer = new (Base::collect) XmlTestWriter;
    writer->setRootNamespace("ns-root", "");
    for (;;)
    {
        XmlKit::XmlEvent *event = reader->read();
        if (0 == event)
            break;
        writer->write(event);
    }
    const char *s = JoinChunks(chunksRootNS, NELEMS(chunksRootNS));
    ASSERT_STREQ(s, writer->chars());
    Base::Object::Collect();
}
