/*
 * XmlKit/PlistReaderWriterTest.cpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#include <XmlKit/PlistReader.hpp>
#include <XmlKit/PlistWriter.hpp>
#include <gtest/gtest.h>

class PlistTestReader : public XmlKit::PlistReader
{
public:
    PlistTestReader(const char **chunks, size_t length);
protected:
    ssize_t readBuffer(void *buf, size_t size);
private:
    const char **_chunks;
    size_t _clength;
    size_t _cindex;
};
PlistTestReader::PlistTestReader(const char **chunks, size_t length)
{
    _chunks = chunks;
    _clength = length;
}
ssize_t PlistTestReader::readBuffer(void *buf, size_t size)
{
    if (_clength <= _cindex)
        return 0;
    size_t len = strlen(_chunks[_cindex]);
    assert(size > len);
    memcpy(buf, _chunks[_cindex], len);
    _cindex++;
    return len;
}

class PlistTestWriter : public XmlKit::PlistWriter
{
public:
    PlistTestWriter();
    ~PlistTestWriter();
    const char *chars();
protected:
    ssize_t writeBuffer(const void *buf, size_t size);
private:
    const char *_xmltext;
};
PlistTestWriter::PlistTestWriter() : PlistWriter(0)
{
    _xmltext = Base::cstr_new(0);
}
PlistTestWriter::~PlistTestWriter()
{
    Base::cstr_obj(_xmltext)->release();
}
const char *PlistTestWriter::chars()
{
    return _xmltext;
}
ssize_t PlistTestWriter::writeBuffer(const void *buf, size_t size)
{
    _xmltext = Base::cstr_obj(_xmltext)->_concat((const char *)buf, size)->chars();
    return size;
}

static const char *JoinChunks(const char **chunks, size_t length)
{
    const char *s = Base::cstr_new(0);
    for (size_t i = 0; length > i; i++)
        s = Base::cstr_obj(s)->_concat(chunks[i])->chars();
    Base::cstr_obj(s)->autorelease();
    return s;
}

static const char *plist1[] =
{
    "<plist version=\"1.0\">"
    "<dict>"
    "<key>array</key>"
    "<array>"
    "<string>0</string>"
    "<string>1</string>"
    "</array>"
    "<key>dict</key>"
    "<dict>"
    "<key>empty</key>"
    "<string></string>"
    "<key>k0</key>"
    "<string>v0</string>"
    "<key>k1</key>"
    "<string>v1</string>"
    "</dict>"
    "<key>false</key>"
    "<false/>"
    "<key>integer</key>"
    "<integer>1</integer>"
    "<key>real</key>"
    "<real>1.1</real>"
    "<key>string</key>"
    "<string>1</string>"
    "<key>true</key>"
    "<true/>"
    "</dict>"
    "</plist>"
};
static const char *plist2[] =
{
    "<plist version=\"1.0\">"
    "<array>"
    "<false/>"
    "<true/>"
    "</array>"
    "</plist>"
};
static const char *plist3[] =
{
    "<plist version=\"1.0\">"
    "<string>text</string>"
    "</plist>"
};

static void TestChunks(const char **chunks, size_t length)
{
    Base::Object::Mark();
    PlistTestReader *reader = new (Base::collect) PlistTestReader(chunks, length);
    PlistTestWriter *writer = new (Base::collect) PlistTestWriter;
    Base::Object *obj = reader->readObject();
    bool result = writer->writeObject(obj);
    ASSERT_TRUE(result);
    const char *s = JoinChunks(chunks, NELEMS(chunks));
    ASSERT_STREQ(s, writer->chars());
    Base::Object::Collect();
}

TEST(PlistReaderWriterTest, ReadWrite1)
{
    TestChunks(plist1, NELEMS(plist1));
}
TEST(PlistReaderWriterTest, ReadWrite2)
{
    TestChunks(plist2, NELEMS(plist2));
}
TEST(PlistReaderWriterTest, ReadWrite3)
{
    TestChunks(plist3, NELEMS(plist3));
}
