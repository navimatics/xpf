/*
 * XmlKit/XmlObjectReaderWriterTest.cpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#include <XmlKit/XmlObjectReader.hpp>
#include <XmlKit/XmlObjectWriter.hpp>
#include <gtest/gtest.h>

class XmlTestObjectReader : public XmlKit::XmlObjectReader
{
public:
    XmlTestObjectReader(const char **chunks, size_t length);
    void reset();
protected:
    ssize_t readBuffer(void *buf, size_t size);
private:
    const char **_chunks;
    size_t _clength;
    size_t _cindex;
};
XmlTestObjectReader::XmlTestObjectReader(const char **chunks, size_t length)
{
    _chunks = chunks;
    _clength = length;
}
void XmlTestObjectReader::reset()
{
    _cindex = 0;
    XmlKit::XmlObjectReader::reset();
}
ssize_t XmlTestObjectReader::readBuffer(void *buf, size_t size)
{
    if (_clength <= _cindex)
        return 0;
    size_t len = strlen(_chunks[_cindex]);
    assert(size > len);
    memcpy(buf, _chunks[_cindex], len);
    _cindex++;
    return len;
}

class XmlTestObjectWriter : public XmlKit::XmlObjectWriter
{
public:
    XmlTestObjectWriter(bool pretty = false);
    ~XmlTestObjectWriter();
    const char *chars();
protected:
    ssize_t writeBuffer(const void *buf, size_t size);
private:
    const char *_xmltext;
};
XmlTestObjectWriter::XmlTestObjectWriter(bool pretty) : XmlObjectWriter(0, pretty)
{
    _xmltext = Base::cstr_new(0);
}
XmlTestObjectWriter::~XmlTestObjectWriter()
{
    Base::cstr_obj(_xmltext)->release();
}
const char *XmlTestObjectWriter::chars()
{
    return _xmltext;
}
ssize_t XmlTestObjectWriter::writeBuffer(const void *buf, size_t size)
{
    _xmltext = Base::cstr_obj(_xmltext)->_concat((const char *)buf, size)->chars();
    return size;
}

static const char *JoinChunks(const char **chunks, size_t length)
{
    const char *s = Base::cstr_new(0);
    for (size_t i = 0; length > i; i++)
        s = Base::cstr_obj(s)->_concat(chunks[i])->chars();
    Base::cstr_obj(s)->autorelease();
    return s;
}

static const char *chunks[] =
{
    "<root xmlns=\"ns-root\">",
    "<rec/>",
    "<rec>text</rec>",
    "<rec a1=\"v1\" a2=\"v2\"/>",
    "<rec a1=\"v1\" a2=\"v2\">te", "x", "t</rec>",
    "<rec><a>1</a><b>2</b></rec>",
    "<rec a1=\"v1\" a2=\"v2\">text<a>1</a><b>2</b></rec>",
    "<rec>te", "x", "t<e>1</e><e>", "2</e><e>3</e></rec>",
    "</root>"
};
TEST(XmlObjectReaderWriterTest, ReadWrite)
{
    Base::Object::Mark();
    XmlTestObjectReader *reader = new (Base::collect) XmlTestObjectReader(chunks, NELEMS(chunks));
    XmlKit::XmlQName qname = { "rec", "" };
    reader->addQName(qname);
    XmlTestObjectWriter *writer = new (Base::collect) XmlTestObjectWriter;
    writer->setRootNamespace("ns-root", "");
    XmlKit::XmlQName root = { "root", "" };
    writer->writeStart(root, 0, 0);
    for (;;)
    {
        XmlKit::XmlObjectEvent *event = reader->readObject();
        if (0 == event)
            break;
        if (0 == strcmp("rec", event->qname().lname))
            writer->writeObject(event);
    }
    writer->writeEnd(root);
    const char *s = JoinChunks(chunks, NELEMS(chunks));
    ASSERT_STREQ(s, writer->chars());
    Base::Object::Collect();
}
TEST(XmlObjectReaderWriterTest, ReadWriteReset)
{
    Base::Object::Mark();
    XmlTestObjectReader *reader = new (Base::collect) XmlTestObjectReader(chunks, NELEMS(chunks));
    XmlKit::XmlQName qname = { "rec", "" };
    reader->addQName(qname);
    XmlTestObjectWriter *writer = new (Base::collect) XmlTestObjectWriter;
    writer->setRootNamespace("ns-root", "");
    XmlKit::XmlQName root = { "root", "" };
    writer->writeStart(root, 0, 0);
    for (;;)
    {
        XmlKit::XmlObjectEvent *event = reader->readObject();
        if (0 == event)
            break;
        if (0 == strcmp("rec", event->qname().lname))
            writer->writeObject(event);
    }
    writer->writeEnd(root);
    const char *s = JoinChunks(chunks, NELEMS(chunks));
    ASSERT_STREQ(s, writer->chars());
    reader->reset();
    reader->addQName(qname);
    writer = new (Base::collect) XmlTestObjectWriter;
    writer->setRootNamespace("ns-root", "");
    writer->writeStart(root, 0, 0);
    for (;;)
    {
        XmlKit::XmlObjectEvent *event = reader->readObject();
        if (0 == event)
            break;
        if (0 == strcmp("rec", event->qname().lname))
            writer->writeObject(event);
    }
    writer->writeEnd(root);
    s = JoinChunks(chunks, NELEMS(chunks));
    ASSERT_STREQ(s, writer->chars());
    Base::Object::Collect();
}
static const char *chunks2[] =
{
    "<root>",
    "<rec/>",
    "<rec>ignored<a/></rec>",
    "<rec>a<b>c</b>d<e>f</e>g</rec>",
    "<rec a1=\"v1\">a<b>c</b>d<e>f</e>g</rec>",
    "</root>"
};
static const char *joined2 =
    "<root>"
    "<rec/>"
    "<rec><a/></rec>"
    "<rec><b>c</b><e>f</e></rec>"
    "<rec a1=\"v1\"><b>c</b><e>f</e></rec>"
    "</root>";
TEST(XmlObjectReaderWriterTest, ReadIgnoreWrite)
{
    Base::Object::Mark();
    XmlTestObjectReader *reader = new (Base::collect) XmlTestObjectReader(chunks2, NELEMS(chunks2));
    reader->setIgnoreMixedContentText(true);
    XmlKit::XmlQName qname = { "rec", "" };
    reader->addQName(qname);
    XmlTestObjectWriter *writer = new (Base::collect) XmlTestObjectWriter;
    XmlKit::XmlQName root = { "root", "" };
    writer->writeStart(root, 0, 0);
    for (;;)
    {
        XmlKit::XmlObjectEvent *event = reader->readObject();
        if (0 == event)
            break;
        if (0 == strcmp("rec", event->qname().lname))
            writer->writeObject(event);
    }
    writer->writeEnd(root);
    ASSERT_STREQ(joined2, writer->chars());
    Base::Object::Collect();
}
