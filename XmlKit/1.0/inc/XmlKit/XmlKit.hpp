/*
 * XmlKit/XmlKit.hpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef XMLKIT_XMLKIT_HPP_INCLUDED
#define XMLKIT_XMLKIT_HPP_INCLUDED

#include <XmlKit/Defines.hpp>
#include <XmlKit/ObjectUtil.hpp>
#include <XmlKit/PlistReader.hpp>
#include <XmlKit/PlistWriter.hpp>
#include <XmlKit/XmlObjectReader.hpp>
#include <XmlKit/XmlObjectWriter.hpp>
#include <XmlKit/XmlParser.hpp>
#include <XmlKit/XmlReader.hpp>
#include <XmlKit/XmlWriter.hpp>

#endif // XMLKIT_XMLKIT_HPP_INCLUDED
