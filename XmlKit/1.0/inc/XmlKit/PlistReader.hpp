/*
 * XmlKit/PlistReader.hpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef XMLKIT_PLISTREADER_HPP_INCLUDED
#define XMLKIT_PLISTREADER_HPP_INCLUDED

#include <XmlKit/Defines.hpp>
#include <XmlKit/XmlReader.hpp>
#include <XmlKit/impl/PlistReader.hpp>

namespace XmlKit {

using XmlKit::impl::PlistReader;

}

#endif // XMLKIT_PLISTREADER_HPP_INCLUDED
