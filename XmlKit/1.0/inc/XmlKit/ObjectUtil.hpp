/*
 * XmlKit/ObjectUtil.hpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef XMLKIT_OBJECTUTIL_HPP_INCLUDED
#define XMLKIT_OBJECTUTIL_HPP_INCLUDED

#include <XmlKit/Defines.hpp>
#include <XmlKit/impl/ObjectUtil.hpp>

namespace XmlKit {

using XmlKit::impl::ObjectReadPlist;
using XmlKit::impl::ObjectWritePlist;

}

#endif // XMLKIT_OBJECTUTIL_HPP_INCLUDED
