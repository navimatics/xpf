/*
 * XmlKit/PlistWriter.hpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef XMLKIT_PLISTWRITER_HPP_INCLUDED
#define XMLKIT_PLISTWRITER_HPP_INCLUDED

#include <XmlKit/Defines.hpp>
#include <XmlKit/XmlWriter.hpp>
#include <XmlKit/impl/PlistWriter.hpp>

namespace XmlKit {

using XmlKit::impl::PlistWriter;

}

#endif // XMLKIT_PLISTWRITER_HPP_INCLUDED
