/*
 * XmlKit/XmlObjectReader.hpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef XMLKIT_XMLOBJECTREADER_HPP_INCLUDED
#define XMLKIT_XMLOBJECTREADER_HPP_INCLUDED

#include <XmlKit/Defines.hpp>
#include <XmlKit/XmlParser.hpp>
#include <XmlKit/impl/XmlObjectReader.hpp>

namespace XmlKit {

using XmlKit::impl::XmlObjectEvent;
using XmlKit::impl::XmlObjectReader;

}

#endif // XMLKIT_XMLOBJECTREADER_HPP_INCLUDED
