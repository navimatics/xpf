/*
 * XmlKit/XmlObjectWriter.hpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef XMLKIT_XMLOBJECTWRITER_HPP_INCLUDED
#define XMLKIT_XMLOBJECTWRITER_HPP_INCLUDED

#include <XmlKit/Defines.hpp>
#include <XmlKit/XmlWriter.hpp>
#include <XmlKit/impl/XmlObjectWriter.hpp>

namespace XmlKit {

using XmlKit::impl::XmlObjectWriter;

}

#endif // XMLKIT_XMLOBJECTWRITER_HPP_INCLUDED
