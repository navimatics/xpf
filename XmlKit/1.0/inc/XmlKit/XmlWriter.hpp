/*
 * XmlKit/XmlWriter.hpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef XMLKIT_XMLWRITER_HPP_INCLUDED
#define XMLKIT_XMLWRITER_HPP_INCLUDED

#include <XmlKit/Defines.hpp>
#include <XmlKit/impl/XmlWriter.hpp>

namespace XmlKit {

using XmlKit::impl::XmlWriter;

}

#endif // XMLKIT_XMLWRITER_HPP_INCLUDED
