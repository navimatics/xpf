/*
 * XmlKit/XmlReader.hpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef XMLKIT_XMLREADER_HPP_INCLUDED
#define XMLKIT_XMLREADER_HPP_INCLUDED

#include <XmlKit/Defines.hpp>
#include <XmlKit/XmlParser.hpp>
#include <XmlKit/impl/XmlReader.hpp>

namespace XmlKit {

using XmlKit::impl::XmlEvent;
using XmlKit::impl::XmlReader;

}

#endif // XMLKIT_XMLREADER_HPP_INCLUDED
