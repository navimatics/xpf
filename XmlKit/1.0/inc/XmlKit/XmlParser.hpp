/*
 * XmlKit/XmlParser.hpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef XMLKIT_XMLPARSER_HPP_INCLUDED
#define XMLKIT_XMLPARSER_HPP_INCLUDED

#include <XmlKit/Defines.hpp>
#include <XmlKit/impl/XmlParser.hpp>

namespace XmlKit {

using XmlKit::impl::XmlQName;
using XmlKit::impl::XmlAttribute;
using XmlKit::impl::XmlParser;

}

#endif // XMLKIT_XMLPARSER_HPP_INCLUDED
