/*
 * XmlKit/impl/XmlParser.hpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef XMLKIT_IMPL_XMLPARSER_HPP_INCLUDED
#define XMLKIT_IMPL_XMLPARSER_HPP_INCLUDED

#include <XmlKit/impl/Defines.hpp>

namespace XmlKit {
namespace impl {

struct XMLKIT_APISYM XmlQName
{
    const char *lname;
    const char *xmlns;
};
struct XMLKIT_APISYM XmlAttribute
{
    XmlQName qname;
    const char *value;
};

class XMLKIT_APISYM XmlParser : public Base::Object
{
public:
    static const char *errorDomain();
    XmlParser(Base::Stream *stream = 0);
    ~XmlParser();
    virtual void reset();
    virtual ssize_t parse(const void *buf, size_t size);
    virtual ssize_t parse();
protected:
    virtual const char *_errorDomain();
    virtual ssize_t readBuffer(void *buf, size_t size);
    virtual void startElement(XmlQName qname, XmlAttribute *attributes, size_t attributeCount);
    virtual void endElement(XmlQName qname);
    virtual void characterData(const char *s, size_t len);
private:
    static void startElementHandler(void *self, const char *name, const char **attv);
    static void endElementHandler(void *self, const char *name);
    static void characterDataHandler(void *self, const char *s, int len);
private:
    Base::Stream *_stream;
    void *_parser;
};

}
}

#endif // XMLKIT_IMPL_XMLPARSER_HPP_INCLUDED
