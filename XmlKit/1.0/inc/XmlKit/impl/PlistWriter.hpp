/*
 * XmlKit/impl/PlistWriter.hpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef XMLKIT_IMPL_PLISTWRITER_HPP_INCLUDED
#define XMLKIT_IMPL_PLISTWRITER_HPP_INCLUDED

#include <XmlKit/impl/Defines.hpp>
#include <XmlKit/impl/XmlWriter.hpp>

namespace XmlKit {
namespace impl {

class XMLKIT_APISYM PlistWriter : public XmlWriter
{
public:
    PlistWriter(Base::Stream *stream = 0, bool pretty = false);
    bool writeObject(Base::Object *obj);
private:
    bool writeObjectInternal(Base::Object *obj);
};

}
}

#endif // XMLKIT_IMPL_PLISTWRITER_HPP_INCLUDED
