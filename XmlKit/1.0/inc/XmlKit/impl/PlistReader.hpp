/*
 * XmlKit/impl/PlistReader.hpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef XMLKIT_IMPL_PLISTREADER_HPP_INCLUDED
#define XMLKIT_IMPL_PLISTREADER_HPP_INCLUDED

#include <XmlKit/impl/Defines.hpp>
#include <XmlKit/impl/XmlReader.hpp>

namespace XmlKit {
namespace impl {

class XMLKIT_APISYM PlistReader : public XmlReader
{
public:
    PlistReader(Base::Stream *stream = 0);
    ~PlistReader();
    Base::Object *readObject();
        /* the returned object is valid until the next call to readObject() or ~PlistReader() */
private:
    void addObject(Base::Object *obj);
private:
    Base::Array *_stack;
    Base::Object *_root;
    Base::CString *_key;
    Base::CString *_text;
};

}
}

#endif // XMLKIT_IMPL_PLISTREADER_HPP_INCLUDED
