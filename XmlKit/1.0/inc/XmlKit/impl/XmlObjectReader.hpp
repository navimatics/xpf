/*
 * XmlKit/impl/XmlObjectReader.hpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef XMLKIT_IMPL_XMLOBJECTREADER_HPP_INCLUDED
#define XMLKIT_IMPL_XMLOBJECTREADER_HPP_INCLUDED

#include <XmlKit/impl/Defines.hpp>
#include <XmlKit/impl/XmlParser.hpp>

namespace XmlKit {
namespace impl {

class XMLKIT_APISYM XmlObjectEvent : public Base::Object
{
public:
    static XmlObjectEvent *create(XmlQName qname, Base::Object *object);
    ~XmlObjectEvent();
    XmlQName qname();
    Base::Object *object();
private:
    XmlObjectEvent();
private:
    XmlQName _qname;
    Base::Object *_object;
};

class XMLKIT_APISYM XmlObjectReader : public XmlParser
{
public:
    XmlObjectReader(Base::Stream *stream = 0);
    ~XmlObjectReader();
    void reset();
    void addQName(XmlQName qname);
    void removeQName(XmlQName qname);
    bool ignoreMixedContentText();
    void setIgnoreMixedContentText(bool value);
    XmlObjectEvent *readObject();
        /* the returned object is valid until the next call to readObject() or ~XmlObjectReader() */
    bool finished();
    void setFinished(bool value);
    Base::Object *ancestorObject(ssize_t index);
protected:
    virtual void objectComplete(XmlQName qname, Base::Object *obj);
    void startElement(XmlQName qname, XmlAttribute *attributes, size_t attributeCount);
    void endElement(XmlQName qname);
    void characterData(const char *s, size_t len);
private:
    bool _finished:1;
    bool _ignoreMixedContentText:1;
    Base::Set *_nameset;
    Base::Array *_stack;
    Base::Array *_queue;
    size_t _qindex;
};

inline XmlQName XmlObjectEvent::qname()
{
    return _qname;
}
inline Base::Object *XmlObjectEvent::object()
{
    return _object;
}
inline XmlObjectEvent::XmlObjectEvent()
{
}

}
}

#endif // XMLKIT_IMPL_XMLOBJECTREADER_HPP_INCLUDED
