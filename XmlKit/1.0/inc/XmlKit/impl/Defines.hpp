/*
 * XmlKit/impl/Defines.hpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef XMLKIT_IMPL_DEFINES_HPP_INCLUDED
#define XMLKIT_IMPL_DEFINES_HPP_INCLUDED

#include <Base/Base.hpp>

#if defined(XMLKIT_CONFIG_INTERNAL)
#define XMLKIT_APISYM                   BASE_EXPORTSYM
#else
#define XMLKIT_APISYM                   BASE_IMPORTSYM
#endif

namespace XmlKit {
namespace impl {
}
}

#endif // XMLKIT_IMPL_DEFINES_HPP_INCLUDED
