/*
 * XmlKit/impl/XmlWriter.hpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef XMLKIT_IMPL_XMLWRITER_HPP_INCLUDED
#define XMLKIT_IMPL_XMLWRITER_HPP_INCLUDED

#include <XmlKit/impl/Defines.hpp>
#include <XmlKit/impl/XmlReader.hpp>

namespace XmlKit {
namespace impl {

class XMLKIT_APISYM XmlWriter : public Base::Object
{
public:
    XmlWriter(Base::Stream *stream = 0, bool pretty = false);
    ~XmlWriter();
    bool writeRaw(const char *chars, size_t len = -1);
    bool writeStart(XmlQName qname, XmlAttribute *attributes, size_t attributeCount);
    bool writeEnd(XmlQName qname);
    bool writeText(const char *text, size_t len = -1);
    bool write(XmlEvent *event);
    bool finished();
    void setRootNamespace(const char *xmlns, const char *prefix);
protected:
    virtual ssize_t writeBuffer(const void *buf, size_t size);
private:
    bool writeQName(XmlQName qname, char kind);
    bool writeXmlns(Base::CString *xmlns, Base::CString *prefix);
    void writeLine();
private:
    Base::Stream *_stream;
    bool _finished:1;
    bool _rootelem:1;
    bool _starttag:1;
    bool _startdoc:1;
    bool _simpelem:1;
    int _indent;
    Base::Array *_nsstack;
    int _nsindex;
};

}
}

#endif // XMLKIT_IMPL_XMLWRITER_HPP_INCLUDED
