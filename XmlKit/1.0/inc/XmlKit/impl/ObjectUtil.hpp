/*
 * XmlKit/impl/ObjectUtil.hpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef XMLKIT_IMPL_OBJECTUTIL_HPP_INCLUDED
#define XMLKIT_IMPL_OBJECTUTIL_HPP_INCLUDED

#include <XmlKit/impl/Defines.hpp>

namespace XmlKit {
namespace impl {

XMLKIT_APISYM Base::Object *ObjectReadPlist(const char *path);
XMLKIT_APISYM bool ObjectWritePlist(const char *path, Base::Object *obj);

}
}

#endif // XMLKIT_IMPL_OBJECTUTIL_HPP_INCLUDED
