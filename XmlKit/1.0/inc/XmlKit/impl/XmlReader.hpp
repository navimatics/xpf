/*
 * XmlKit/impl/XmlReader.hpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef XMLKIT_IMPL_XMLREADER_HPP_INCLUDED
#define XMLKIT_IMPL_XMLREADER_HPP_INCLUDED

#include <XmlKit/impl/Defines.hpp>
#include <XmlKit/impl/XmlParser.hpp>

namespace XmlKit {
namespace impl {

class XMLKIT_APISYM XmlEvent : public Base::Object
{
public:
    static XmlEvent *create(int kind, XmlQName qname, XmlAttribute *attributes = 0, size_t attributeCount = 0);
    static XmlEvent *create(int kind, const char *s, size_t len);
    ~XmlEvent();
    int kind();
    XmlQName qname();
    XmlAttribute *attributes();
    size_t attributeCount();
    const char *text();
    void _concat(const char *s, size_t len = -1);
private:
    XmlEvent();
private:
    int _kind;
    union
    {
        struct { XmlQName qname; XmlAttribute *attributes; size_t attributeCount; } _s;
        struct { const char *text; } _t;
    };
};

class XMLKIT_APISYM XmlReader : public XmlParser
{
public:
    XmlReader(Base::Stream *stream = 0);
    ~XmlReader();
    void reset();
    XmlEvent *read();
        /* the returned object is valid until the next call to read() or ~XmlReader() */
    bool finished();
    void setFinished(bool value);
protected:
    void startElement(XmlQName qname, XmlAttribute *attributes, size_t attributeCount);
    void endElement(XmlQName qname);
    void characterData(const char *s, size_t len);
private:
    bool _finished;
    XmlEvent *_textev;
    Base::Array *_queue;
    size_t _qindex;
};

inline int XmlEvent::kind()
{
    return _kind;
}
inline XmlQName XmlEvent::qname()
{
    return _s.qname;
}
inline XmlAttribute *XmlEvent::attributes()
{
    return 0 < _s.attributeCount ? _s.attributes : 0;
}
inline size_t XmlEvent::attributeCount()
{
    return _s.attributeCount;
}
inline const char *XmlEvent::text()
{
    return _t.text;
}
inline XmlEvent::XmlEvent()
{
}

}
}

#endif // XMLKIT_IMPL_XMLREADER_HPP_INCLUDED
