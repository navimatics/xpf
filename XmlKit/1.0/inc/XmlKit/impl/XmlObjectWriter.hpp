/*
 * XmlKit/impl/XmlObjectWriter.hpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef XMLKIT_IMPL_XMLOBJECTWRITER_HPP_INCLUDED
#define XMLKIT_IMPL_XMLOBJECTWRITER_HPP_INCLUDED

#include <XmlKit/impl/Defines.hpp>
#include <XmlKit/impl/XmlObjectReader.hpp>
#include <XmlKit/impl/XmlWriter.hpp>

namespace XmlKit {
namespace impl {

class XMLKIT_APISYM XmlObjectWriter : public XmlWriter
{
public:
    XmlObjectWriter(Base::Stream *stream = 0, bool pretty = false);
    bool writeObject(XmlQName qname, Base::Object *obj);
    bool writeObject(XmlObjectEvent *event);
private:
    bool writeObjectInternal(XmlQName qname, Base::Object *obj);
};

}
}

#endif // XMLKIT_IMPL_XMLOBJECTWRITER_HPP_INCLUDED
