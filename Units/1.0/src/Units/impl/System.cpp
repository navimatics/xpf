/*
 * Units/impl/System.cpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#include <Units/impl/System.hpp>

namespace Units {
namespace impl {

#define CONVERT(src, dst, factor, Type) \
    if (!Base::isNone(factor))\
    {\
        for (Type *p = values, *endp = p + nvalues; endp > p; p++)\
            *p = *p * factor;\
    }\
    else\
    {\
        for (Type *p = values, *endp = p + nvalues; endp > p; p++)\
            if (Base::isFinite(*p))\
                *p = dst->_forward(src->_inverse(*p * src->_factor)) / dst->_factor;\
    }

static struct
{
    int exp;
    const char *name;
    const char *symbol;
} SIPrefixes[] =
{
    {   0, "",      "", },
    {  +1, "deka",  "da", },
    {   0, "deca",  "da", },
    {  +2, "hecto", "h", },
    {  +3, "kilo",  "k", },
    {  +6, "mega",  "M", },
    {  +9, "giga",  "G", },
    { +12, "tera",  "T", },
    { +15, "peta",  "P", },
    { +18, "exa",   "E", },
    { +21, "zetta", "Z", },
    { +24, "yotta", "Y", },
    {  -1, "deci",  "d", },
    {  -2, "centi", "c", },
    {  -3, "milli", "m", },
    {  -6, "micro", "{μ", },
    {  -9, "nano",  "n", },
    { -12, "pico",  "p", },
    { -15, "femto", "f", },
    { -18, "atto",  "a", },
    { -21, "zepto", "z", },
    { -24, "yocto", "y", },
};

static inline bool isnamechar(char c)
{
    /* A-Za-z_{} */
    static char table[] = { 0, 0, 0, 0, 0, 0, 0, 0, 254, 255, 255, 7, 254, 255, 255, 7 };
    return 0 != (table[(c & 0x7f) >> 3] & (1 << (c & 7)));
}

static const char *cstr_concatUtf8Power(const char *s, unsigned power)
{
    if (1 != power)
        for (; 0 != power; power /= 10)
            switch (power % 10)
            {
            case 0:
                s = Base::cstr_concat(s, "⁰");
                break;
            case 1:
                s = Base::cstr_concat(s, "¹");
                break;
            case 2:
                s = Base::cstr_concat(s, "²");
                break;
            case 3:
                s = Base::cstr_concat(s, "³");
                break;
            case 4:
                s = Base::cstr_concat(s, "⁴");
                break;
            case 5:
                s = Base::cstr_concat(s, "⁵");
                break;
            case 6:
                s = Base::cstr_concat(s, "⁶");
                break;
            case 7:
                s = Base::cstr_concat(s, "⁷");
                break;
            case 8:
                s = Base::cstr_concat(s, "⁸");
                break;
            case 9:
                s = Base::cstr_concat(s, "⁹");
                break;
            }
    return s;
}

/*
 * System::_map keeps strong references to every Unit in the System.
 * Unit's cannot be redefined or removed from a System, which
 * makes it safe to keep a weak reference to them in Dimension.
 *
 * We take advantage of this by allowing Unit dimensions to reference themselves.
 * A unit with a single dimension that references itself is a primitive unit.
 */
struct System::Dimension
{
    Unit *unit;
    int power;
};

static double identity(double x)
{
    return x;
}

System::Unit::Unit(Base::CString *nameobj)
{
    _name = nameobj->chars();
    Base::cstr_retain(_name);
    if ('{' == _name[0])
        _displayName = Base::cstr_slice(_name, 1,
            '}' == _name[Base::cstr_length(_name) - 1] ? -1 : 0);
    else
        _displayName = _name;
    Base::cstr_retain(_displayName);
    _dimensions = (Dimension *)Base::carr_new(0, 1, sizeof(Dimension));
    _dimensions[0].unit = this;
    _dimensions[0].power = 1;
    _factor = 1;
    _forward = identity;
    _inverse = identity;
}
System::Unit::Unit(Base::CString *nameobj, Base::CString *dispobj,
    Base::CArray *dimsobj, double factor,
    double (*forward)(double), double (*inverse)(double))
{
    _name = nameobj->chars();
    Base::cstr_retain(_name);
    if (nameobj == dispobj && '{' == _name[0])
        _displayName = Base::cstr_slice(_name, 1,
            '}' == _name[Base::cstr_length(_name) - 1] ? -1 : 0);
    else
        _displayName = dispobj->chars();
    Base::cstr_retain(_displayName);
    _dimensions = (Dimension *)dimsobj->elems();
    Base::carr_retain(_dimensions);
    _factor = factor;
    _forward = 0 != forward ? forward : identity;
    _inverse = 0 != inverse ? inverse : identity;
}
System::Unit::~Unit()
{
    Base::carr_release(_dimensions);
    Base::cstr_release(_displayName);
    Base::cstr_release(_name);
}
bool System::Unit::sameDimensions(Unit *unit)
{
    size_t len = Base::carr_length(_dimensions);
    if (len != Base::carr_length(unit->_dimensions))
        return false;
    for (Dimension *p = _dimensions, *endp = p + len, *q = unit->_dimensions; endp > p; p++, q++)
        if (p->unit != q->unit || p->power != q->power)
            return false;
    return true;
}
const char *System::Unit::name()
{
    return _name;
}
const char *System::Unit::displayName()
{
    return _displayName;
}
const char *System::Unit::definition()
{
    const char *result = Base::cstr_new("");
    if (identity != _forward)
        result = Base::cstr_concat(result, "*", 1);
    if (1 != _factor)
    {
        if (0 < Base::cstr_length(result))
            result = Base::cstr_concat(result, " ", 1);
        char buf[64];
        portable_snprintf(buf, sizeof buf, DBL_PRECISION_G_FMT, _factor);
        result = Base::cstr_concat(result, buf);
    }
    for (Dimension *p = _dimensions, *endp = p + Base::carr_length(_dimensions); endp > p; p++)
    {
        if (0 < Base::cstr_length(result))
            result = Base::cstr_concat(result, " ", 1);
        result = Base::cstr_concat(result, p->unit->_name, Base::cstr_length(p->unit->_name));
        if (1 != p->power)
        {
            char buf[64];
            portable_snprintf(buf, sizeof buf, "%i", p->power);
            result = Base::cstr_concat(result, " ", 1);
            result = Base::cstr_concat(result, buf);
        }
    }
    Base::cstr_obj(result)->autorelease();
    return result;
}
const char *System::Unit::dimensions()
{
    const char *result = Base::cstr_new("");
    for (Dimension *p = _dimensions, *endp = p + Base::carr_length(_dimensions); endp > p; p++)
    {
        if (0 < Base::cstr_length(result))
            result = Base::cstr_concat(result, " ", 1);
        result = Base::cstr_concat(result, p->unit->_name, Base::cstr_length(p->unit->_name));
        if (1 != p->power)
        {
            char buf[64];
            portable_snprintf(buf, sizeof buf, "%i", p->power);
            result = Base::cstr_concat(result, " ", 1);
            result = Base::cstr_concat(result, buf);
        }
    }
    Base::cstr_obj(result)->autorelease();
    return result;
}
const char *System::Unit::strrepr()
{
    return cstringf("<%s %p; defn=(%s)>", className(), this, definition());
}

System::Conversion::Conversion(Unit *src, Unit *dst)
{
    src->retain();
    dst->retain();
    _src = src;
    _dst = dst;
    /*
     * If the source and destination units do not have custom forward and inverse functions,
     * compute a factor that can be applied directly for fast conversion. If the computed
     * factor is positive, positive/negative infinities and nan's on input will be unaffected.
     *
     * If the computed factor is negative (rare event) it has to be discarded, because it can turn
     * positive infinities to negative and vice-versa.
     */
    _factor = Base::None();
    if (src == dst)
        _factor = 1;
    else if (identity == src->_inverse && identity == dst->_forward)
    {
        double f = src->_factor / dst->_factor;
        if (0 < f)
            _factor = f;
    }
}
System::Conversion::~Conversion()
{
    _dst->release();
    _src->release();
}
void System::Conversion::convert(double *values, size_t nvalues)
{
    CONVERT(_src, _dst, _factor, double);
}
void System::Conversion::convert(float *values, size_t nvalues)
{
    CONVERT(_src, _dst, _factor, float);
}
const char *System::Conversion::strrepr()
{
    return cstringf("<%s %p; (%s)->(%s)>", className(), this,
        _src->definition(), _dst->definition());
}

const char *System::errorDomain()
{
    static const char *s = Base::CStringConstant("Units::impl::System")->chars();
    return s;
}
bool System::convert(Unit *src, Unit *dst, double *values, size_t nvalues)
{
    if (0 == src || 0 == dst)
    {
        Base::Error::setLastError(errorDomain(),
            ErrorMisuse, "source or destination unit is null");
        return false;
    }
    if (src == dst)
        return true;
    if (!src->sameDimensions(dst))
    {
        Base::Error::setLastError(errorDomain(),
            ErrorConvert, "source and destination units have incompatible dimensions");
        return false;
    }
    double factor = Base::None();
    if (identity == src->_inverse && identity == dst->_forward)
    {
        double f = src->_factor / dst->_factor;
        if (0 < f)
            factor = f;
    }
    CONVERT(src, dst, factor, double);
    return true;
}
bool System::convert(Unit *src, Unit *dst, float *values, size_t nvalues)
{
    if (0 == src || 0 == dst)
    {
        Base::Error::setLastError(errorDomain(),
            ErrorMisuse, "source or destination unit is null");
        return false;
    }
    if (src == dst)
        return true;
    if (!src->sameDimensions(dst))
    {
        Base::Error::setLastError(errorDomain(),
            ErrorConvert, "source and destination units have incompatible dimensions");
        return false;
    }
    double factor = Base::None();
    if (identity == src->_inverse && identity == dst->_forward)
    {
        double f = src->_factor / dst->_factor;
        if (0 < f)
            factor = f;
    }
    CONVERT(src, dst, factor, float);
    return true;
}
System::Conversion *System::conversion(Unit *src, Unit *dst)
{
    if (0 == src || 0 == dst)
    {
        Base::Error::setLastError(errorDomain(),
            ErrorMisuse, "source or destination unit is null");
        return 0;
    }
    if (!src->sameDimensions(dst))
    {
        Base::Error::setLastError(errorDomain(),
            ErrorConvert, "source and destination units have incompatible dimensions");
        return 0;
    }
    return new (Base::collect) Conversion(src, dst);
}
System::System()
{
    threadsafe();
    _map = new Base::Map;
}
System::~System()
{
    _map->release();
}
bool System::addUnit(const char *name, const char *defn,
    double (*forward)(double), double (*inverse)(double))
{
    bool result = false;
    Base::CString *nameobj = Base::CString::create(name);
    if (0 == _map->object(nameobj))
    {
        /* do not allow redefinitions */
        if (Unit *unit = createUnit(nameobj, defn, forward, inverse))
        {
            _map->setObject(nameobj, unit);
            unit->release();
            result = true;
        }
    }
    else
        Base::Error::setLastError(System::errorDomain(),
            ErrorMisuse, "duplicate unit definition");
    nameobj->release();
    return result;
}
bool System::addAlias(const char *name, const char *alias)
{
    bool result = false;
    if (Unit *unit = (Unit *)_map->object(BASE_ALLOCA_OBJECT(Base::TransientCString, name)))
    {
        Base::CString *aliasobj = Base::CString::create(alias);
        if (0 == _map->object(aliasobj))
        {
            /* do not allow redefinitions */
            _map->setObject(aliasobj, unit);
            result = true;
        }
        else
            Base::Error::setLastError(System::errorDomain(),
                ErrorMisuse, "duplicate alias definition");
        aliasobj->release();
    }
    else
        Base::Error::setLastError(errorDomain(),
            ErrorMisuse, "missing unit definition");
    return result;
}
bool System::addSIPrefixedUnits(const char *name)
{
    if (Unit *unit = (Unit *)_map->object(BASE_ALLOCA_OBJECT(Base::TransientCString, name)))
    {
        if (0 == strcmp(name, "kg"))
        {
            /* special case the kilogram */
            if (!addUnit("g", "1E-3 kg"))
                return false;
            unit = (Unit *)_map->object(BASE_ALLOCA_OBJECT(Base::TransientCString, "g"));
            name = "g";
        }
        for (size_t i = 0; NELEMS(SIPrefixes) > i; i++)
        {
            if (0 == SIPrefixes[i].exp)
                continue;
            char namebuf[128], defnbuf[128];
            if ('{' != SIPrefixes[i].symbol[0])
                portable_snprintf(namebuf, sizeof namebuf, "%s%s", SIPrefixes[i].symbol, name);
            else
                portable_snprintf(namebuf, sizeof namebuf, "{%s%s}", SIPrefixes[i].symbol + 1, name);
            portable_snprintf(defnbuf, sizeof defnbuf, "1E%i %s", SIPrefixes[i].exp, unit->_name);
            if (0 != strcmp(namebuf, "kg"))
                if (!addUnit(namebuf, defnbuf))
                    return false;
        }
        return true;
    }
    else
    {
        Base::Error::setLastError(errorDomain(),
            ErrorMisuse, "missing unit definition");
        return false;
    }
}
bool System::addSIPrefixedAliases(const char *name, const char *alias)
{
    if (_map->object(BASE_ALLOCA_OBJECT(Base::TransientCString, name)))
    {
        if (0 == strcmp(name, "kg"))
        {
            /* special case the kilogram */
            name = "g";
            alias = "gram";
        }
        for (size_t i = 0; NELEMS(SIPrefixes) > i; i++)
        {
            char namebuf[128], aliasbuf[128];
            if ('{' != SIPrefixes[i].symbol[0])
                portable_snprintf(namebuf, sizeof namebuf, "%s%s", SIPrefixes[i].symbol, name);
            else
                portable_snprintf(namebuf, sizeof namebuf, "{%s%s}", SIPrefixes[i].symbol + 1, name);
            portable_snprintf(aliasbuf, sizeof aliasbuf, "%s%s", SIPrefixes[i].name, alias);
            if (!addAlias(namebuf, aliasbuf))
                return false;
        }
        return true;
    }
    else
    {
        Base::Error::setLastError(errorDomain(),
            ErrorMisuse, "missing unit definition");
        return false;
    }
}
System::Unit *System::unit(const char *defn)
{
    if (Unit *unit = (Unit *)_map->object(BASE_ALLOCA_OBJECT(Base::TransientCString, defn)))
        return unit;
    else if (Unit *unit = createUnit(Base::CString::empty(), defn, 0, 0))
    {
        unit->autorelease();
        return unit;
    }
    else
        return 0;
}
System::Unit *System::createUnit(Base::CString *nameobj, const char *defn,
    double (*forward)(double), double (*inverse)(double))
{
    if (0 == defn)
        return new Unit(nameobj);
    else
    {
        Unit *unit = 0;
        const char *displayName = 0 < nameobj->length() ? 0 : Base::cstr_new(0);
        Dimension *displayPositiveDims = (Dimension *)Base::carr_new(0, 0, sizeof(Dimension));
        Dimension *displayNegativeDims = (Dimension *)Base::carr_new(0, 0, sizeof(Dimension));
        Dimension *dims = (Dimension *)Base::carr_new(0, 0, sizeof(Dimension));
        char *p = (char *)defn; double factor = strtod(p, &p);
        if (0 == factor)
            factor = 1;
        double displayFactor = factor;
        for (;;)
        {
            while (' ' == *p)
                p++;
            if ('\0' == *p)
                break;
            char *unamep = p;
            if ('{' == *p)
            {
                p++;
                while ('}' != *p && '\0' != *p)
                    p++;
                if ('}' == *p)
                    p++;
                else
                    unamep = p;
            }
            else
            {
                while (isnamechar(*p))
                    p++;
            }
            if (p == unamep)
            {
                Base::Error::setLastError(errorDomain(),
                    ErrorMisuse, "syntax error");
                goto end;
            }
            Base::CString *uname = Base::CString::create(unamep, p - unamep);
            Unit *u = (Unit *)_map->object(uname);
            if (0 == u)
            {
                char buf[256];
                portable_snprintf(buf, sizeof buf, "missing unit definition: %s", uname->chars());
                Base::Error::setLastError(errorDomain(), ErrorMisuse, buf);
                uname->release();
                goto end;
            }
            uname->release();
            if (identity != u->_forward || identity != u->_inverse)
            {
                Base::Error::setLastError(errorDomain(),
                    ErrorMisuse, "referenced unit has forward/inverse function defined");
                goto end;
            }
            while (' ' == *p)
                p++;
            int power = 0, sign = +1;
            if ('-' == *p)
            {
                sign = -1;
                p++;
            }
            while (isdigit(*p))
            {
                power *= 10;
                power += *p - '0';
                p++;
            }
            if (0 == power)
                power = 1;
            power *= sign;
            factor *= pow(u->_factor, power);
            size_t ndims = Base::carr_length(dims);
            dims = (Dimension *)Base::carr_concat(dims,
                u->_dimensions, Base::carr_length(u->_dimensions), sizeof(Dimension));
            for (Dimension *dimp = dims + ndims, *dimendp = dims + Base::carr_length(dims);
                dimendp > dimp; dimp++)
                dimp->power *= power;
            if (0 != displayName)
            {
                Dimension dim = { u, power };
                if (0 <= power)
                    displayPositiveDims = (Dimension *)Base::carr_concat(displayPositiveDims,
                        &dim, 1, sizeof(Dimension));
                else
                    displayNegativeDims = (Dimension *)Base::carr_concat(displayNegativeDims,
                        &dim, 1, sizeof(Dimension));
            }
        }
        {
            qsort(dims, Base::carr_length(dims), sizeof(Dimension), Dimension_cmp);
            Dimension *dstp = dims;
            for (Dimension *srcp = dims + 1, *srcendp = dims + Base::carr_length(dims);
                srcendp > srcp; srcp++)
            {
                if (dstp->unit == srcp->unit)
                    dstp->power += srcp->power;
                else
                {
                    if (0 != dstp->power)
                        dstp++;
                    *dstp = *srcp;
                }
            }
            if (0 < Base::carr_length(dims) && 0 != dstp->power)
                dstp++;
            Base::carr_obj(dims)->setLength(dstp - dims);
        }
        if (0 != displayName)
        {
            if (1 != displayFactor)
            {
                char buf[64];
                portable_snprintf(buf, sizeof buf, DBL_PRECISION_G_FMT, displayFactor);
                displayName = Base::cstr_concat(displayName, "[", 1);
                displayName = Base::cstr_concat(displayName, buf);
            }
            for (Dimension *srcp = displayPositiveDims, *srcendp = displayPositiveDims + Base::carr_length(displayPositiveDims);
                srcendp > srcp; srcp++)
            {
                if (0 < Base::cstr_length(displayName))
                    displayName = Base::cstr_concat(displayName, "⋅");
                displayName = Base::cstr_concat(displayName, srcp->unit->displayName());
                displayName = cstr_concatUtf8Power(displayName, srcp->power);
            }
            for (Dimension *srcp = displayNegativeDims, *srcendp = displayNegativeDims + Base::carr_length(displayNegativeDims);
                srcendp > srcp; srcp++)
            {
                if (srcp == displayNegativeDims)
                    displayName = Base::cstr_concat(displayName, "/");
                else
                    displayName = Base::cstr_concat(displayName, "⋅");
                displayName = Base::cstr_concat(displayName, srcp->unit->displayName());
                displayName = cstr_concatUtf8Power(displayName, -srcp->power);
            }
            if (1 != displayFactor)
                displayName = Base::cstr_concat(displayName, "]", 1);
            unit = new Unit(nameobj, Base::cstr_obj(displayName),
                Base::carr_obj(dims), factor, forward, inverse);
        }
        else
            unit = new Unit(nameobj, nameobj,
                Base::carr_obj(dims), factor, forward, inverse);
    end:
        Base::carr_release(dims);
        Base::carr_release(displayNegativeDims);
        Base::carr_release(displayPositiveDims);
        Base::cstr_release(displayName);
        return unit;
    }
}
int System::Dimension_cmp(const void *p1, const void *p2)
{
    Dimension *d1 = (Dimension *)p1, *d2 = (Dimension *)p2;
    return strcmp(d1->unit->_name, d2->unit->_name);
}

}
}
