/*
 * Units/impl/Preferences.cpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#include <Units/impl/Preferences.hpp>

namespace Units {
namespace impl {

inline Base::Map *Preferences::_threadMap()
{
    Base::Map *threadMap = Base::Thread::currentThreadMap();
    Base::Map *map = ((Base::Map *)threadMap->object(_threadMapKey));
    if (0 == map)
    {
        map = new Base::Map;
        threadMap->setObject(_threadMapKey, map);
        map->release();
    }
    return map;
}

Preferences *Preferences::standardPreferences()
{
    static Preferences *prefs;
    execute_once
        prefs = new Preferences(System::standardSystem(), Base::Environment::standardEnvironment());
    return prefs;
}
Preferences::Preferences(System *system, Base::Environment *environment)
{
    static Base::CString *envkey = Base::CStringConstant("Units::impl::Preferences");
    threadsafe();
    system->retain();
    environment->retain();
    _system = system;
    _environment = environment;
    _threadMapKey = new Base::Object;
    _threadMapKey->threadsafe();
    Base::Map *threadMap = new Base::Map;
    Base::Thread::currentThreadMap()->setObject(_threadMapKey, threadMap);
    threadMap->release();
    Base::Map *map = new Base::Map;
    synchronized (_environment->lock())
    {
        Base::Map *applicationMap =
            (Base::Map *)_environment->object(envkey, Base::Environment::Application);
        if (0 != applicationMap)
            map->addObjects(applicationMap);
    }
    foreach (Base::CString *srcnameobj, map)
        if (Base::CString *dstnameobj =
            dynamic_cast<Base::CString *>(map->object(srcnameobj)))
        {
            System::Conversion *conversion = System::conversion(
                _system->unit(srcnameobj->chars()),
                _system->unit(dstnameobj->chars()));
            if (0 != conversion)
                threadMap->setObject(srcnameobj, conversion);
        }
    map->release();
}
Preferences::~Preferences()
{
    Base::Thread::currentThreadMap()->removeObject(_threadMapKey);
    _threadMapKey->release();
    _environment->release();
    _system->release();
}
Base::Lock *Preferences::lock()
{
    return _environment->lock();
}
System::Conversion *Preferences::preferredConversion(const char *srcname)
{
    return (System::Conversion *)
        _threadMap()->object(BASE_ALLOCA_OBJECT(Base::TransientCString, srcname));
}
void Preferences::setPreferredConversion(const char *srcname, const char *dstname)
{
    static Base::CString *envkey = Base::CStringConstant("Units::impl::Preferences");
    Base::CString *srcnameobj = Base::CString::create(srcname);
    Base::CString *dstnameobj = Base::CString::create(dstname);
    System::Conversion *conversion = System::conversion(
        _system->unit(srcnameobj->chars()),
        _system->unit(dstnameobj->chars()));
    if (0 != conversion)
    {
        _threadMap()->setObject(srcnameobj, conversion);
        synchronized(_environment->lock())
        {
            Base::Map *applicationMap =
                (Base::Map *)_environment->object(envkey, Base::Environment::Application);
            if (0 == applicationMap)
            {
                applicationMap = new Base::Map;
                _environment->setObject(envkey, applicationMap, Base::Environment::Application);
                applicationMap->release();
            }
            else
                _environment->markDirty(Base::Environment::Application);
            applicationMap->setObject(srcnameobj, dstnameobj);
        }
    }
    dstnameobj->release();
    srcnameobj->release();
}
void Preferences::removePreferredConversion(const char *srcname)
{
    static Base::CString *envkey = Base::CStringConstant("Units::impl::Preferences");
    Base::TransientCString *srcnameobj = BASE_ALLOCA_OBJECT(Base::TransientCString, srcname);
    _threadMap()->removeObject(srcnameobj);
    synchronized(_environment->lock())
    {
        Base::Map *applicationMap =
            (Base::Map *)_environment->object(envkey, Base::Environment::Application);
        if (0 != applicationMap)
        {
            applicationMap->removeObject(srcnameobj);
            _environment->markDirty(Base::Environment::Application);
        }
    }
}
bool Preferences::commit()
{
    return _environment->commit();
}

}
}
