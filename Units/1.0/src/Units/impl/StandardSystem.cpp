/*
 * Units/impl/StandardSystem.cpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#include <Units/impl/System.hpp>

namespace Units {
namespace impl {

static double celsius_from_kelvin(double K)
{
    return K - 273.15;
}
static double kelvin_from_celsius(double C)
{
    return C + 273.15;
}
static double fahrenheit_from_kelvin(double K)
{
    return 1.8 * K - 459.67;
}
static double kelvin_from_fahrenheit(double F)
{
    return (F + 459.67) / 1.8;
}
System *System::standardSystem()
{
    static System *system;
    execute_once
    {
        system = new System;
        /* SI base units; see NIST SP 330 Table 1 */
        system->addUnit("m");
            system->addSIPrefixedUnits("m");
            system->addSIPrefixedAliases("m", "meter");
            system->addSIPrefixedAliases("m", "metre");
        system->addUnit("kg");
            system->addSIPrefixedUnits("kg");
            system->addSIPrefixedAliases("kg", "kilogram");
        system->addUnit("s");
            system->addSIPrefixedUnits("s");
            system->addSIPrefixedAliases("s", "second");
        system->addUnit("A");
            system->addSIPrefixedUnits("A");
            system->addSIPrefixedAliases("A", "ampere");
        system->addUnit("K");
            system->addSIPrefixedUnits("K");
            system->addSIPrefixedAliases("K", "kelvin");
        system->addUnit("mol");
            system->addSIPrefixedUnits("mol");
            system->addSIPrefixedAliases("mol", "mole");
        system->addUnit("cd");
            system->addSIPrefixedUnits("cd");
            system->addSIPrefixedAliases("cd", "candela");
        /* SI derived units with special names and symbols; see NIST SP 330 Table 3 */
        system->addUnit("rad", "m m -1");
            system->addSIPrefixedUnits("rad");
            system->addSIPrefixedAliases("rad", "radian");
        system->addUnit("sr", "m 2 m -2");
            system->addSIPrefixedUnits("sr");
            system->addSIPrefixedAliases("sr", "steradian");
        system->addUnit("Hz", "s -1");
            system->addSIPrefixedUnits("Hz");
            system->addSIPrefixedAliases("Hz", "hertz");
        system->addUnit("N", "m kg s -2");
            system->addSIPrefixedUnits("N");
            system->addSIPrefixedAliases("N", "newton");
        system->addUnit("Pa", "N m -2");
            system->addSIPrefixedUnits("Pa");
            system->addSIPrefixedAliases("Pa", "pascal");
        system->addUnit("J", "N m");
            system->addSIPrefixedUnits("J");
            system->addSIPrefixedAliases("J", "joule");
        system->addUnit("W", "J s-1");
            system->addSIPrefixedUnits("W");
            system->addSIPrefixedAliases("W", "watt");
        system->addUnit("C", "A s");
            system->addSIPrefixedUnits("C");
            system->addSIPrefixedAliases("C", "coulomb");
        system->addUnit("V", "A -1 W");
            system->addSIPrefixedUnits("V");
            system->addSIPrefixedAliases("V", "volt");
        system->addUnit("F", "C V -1");
            system->addSIPrefixedUnits("F");
            system->addSIPrefixedAliases("F", "farad");
        system->addUnit("{Ω}", "A -1 V");
            system->addSIPrefixedUnits("{Ω}");
            system->addSIPrefixedAliases("{Ω}", "ohm");
        system->addUnit("S", "A V -1");
            system->addSIPrefixedUnits("S");
            system->addSIPrefixedAliases("S", "siemens");
        system->addUnit("Wb", "V s");
            system->addSIPrefixedUnits("Wb");
            system->addSIPrefixedAliases("Wb", "weber");
        system->addUnit("T", "Wb m -2");
            system->addSIPrefixedUnits("T");
            system->addSIPrefixedAliases("T", "tesla");
        system->addUnit("H", "A -1 Wb");
            system->addSIPrefixedUnits("H");
            system->addSIPrefixedAliases("H", "henry");
        system->addUnit("{°C}", "K", celsius_from_kelvin, kelvin_from_celsius);
            system->addAlias("{°C}", "{degree Celsius}");
        system->addUnit("lm", "cd sr");
            system->addSIPrefixedUnits("lm");
            system->addSIPrefixedAliases("lm", "lumen");
        system->addUnit("lx", "lm m -2");
            system->addSIPrefixedUnits("lx");
            system->addSIPrefixedAliases("lx", "lux");
        system->addUnit("Bq", "s -1");
            system->addSIPrefixedUnits("Bq");
            system->addSIPrefixedAliases("Bq", "becquerel");
        system->addUnit("Gy", "J kg -1");
            system->addSIPrefixedUnits("Gy");
            system->addSIPrefixedAliases("Gy", "gray");
        system->addUnit("Sv", "J kg -1");
            system->addSIPrefixedUnits("Sv");
            system->addSIPrefixedAliases("Sv", "sievert");
        system->addUnit("kat", "mol s -1");
            system->addSIPrefixedUnits("kat");
            system->addSIPrefixedAliases("kat", "katal");
        /* Non-SI units accepted for use with the SI; see NIST SP 330 Table 6 */
        system->addUnit("min", "60 s");
            system->addAlias("min", "minute");
        system->addUnit("h", "60 min");
            system->addAlias("h", "hour");
        system->addUnit("d", "24 h");
            system->addAlias("d", "day");
        system->addUnit("{°}", "0.0174532925199433 rad");
            system->addAlias("{°}", "degree");
        system->addUnit("ha", "hm 2");
            system->addAlias("ha", "hectare");
        system->addUnit("L", "dm 3");
            system->addAlias("L", "liter");
            system->addUnit("mL", "1E-3 L");
            system->addAlias("mL", "milliliter");
        system->addUnit("t", "1E3 kg");
            system->addAlias("t", "{metric ton}");
            system->addAlias("t", "tonne");
        /* Non-SI units whose values in SI units must be obtained experimentally; see NIST SP 330 Table 7 */
        system->addUnit("eV", "1.60217653E-19 J");
            system->addAlias("eV", "electronvolt");
        system->addUnit("Da", "1.66053886E-27 kg");
            system->addAlias("Da", "u");
            system->addAlias("Da", "dalton");
        system->addUnit("ua", "1.49597870691E+11 m");
            system->addAlias("ua", "AU");
            system->addAlias("ua", "{astronomical unit}");
        /* Other non-SI units; see NIST SP 330 Table 8 or NIST SP 811 Appendix B */
        system->addUnit("bar", "1.0E+05 Pa");
        system->addUnit("mmHg", "1.333224E+02 Pa");
        system->addUnit("M", "1852 m");
            system->addAlias("M", "nmi");
            system->addAlias("M", "nautical mile");
        system->addUnit("kn", "M h -1");
        /* Conversions; see NIST SP 811 Appendix B */
        system->addUnit("{°F}", "K", fahrenheit_from_kelvin, kelvin_from_fahrenheit);
            system->addAlias("{°F}", "{degree Fahrenheit}");
        system->addUnit("fathom", "1.828804E+00 m");
        system->addUnit("ft", "3.048E-01 m");
            system->addAlias("ft", "feet");
        system->addUnit("gal", "3.785412E-03 m 3");
            system->addAlias("gal", "gallon");
        system->addUnit("in", "2.54E-02 m");
            system->addAlias("in", "inch");
        system->addUnit("inHg", "3.386389E+03 Pa");
        system->addUnit("mi", "1.609344E+03 m");
            system->addAlias("mi", "mile");
        system->addUnit("lb", "4.5359237E-01 kg");
            system->addAlias("lb", "pound");
        system->addUnit("{US survey foot}", "3.048006E-01 m");
        system->addUnit("yard", "9.144E-01 m");
    }
    return system;
}

}
}
