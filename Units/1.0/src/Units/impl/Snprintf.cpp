/*
 * Units/impl/Snprintf.cpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#include <Units/impl/Snprintf.hpp>
#include <Units/impl/Preferences.hpp>

/* trio functions */
extern "C" {
typedef void * trio_pointer_t;
typedef int (*trio_callback_t)(trio_pointer_t);
trio_pointer_t trio_register(trio_callback_t callback, const char *name);
const char *trio_get_format(trio_pointer_t ref);
int trio_get_alternative(trio_pointer_t ref);
void trio_set_alternative(trio_pointer_t ref, int is_alternative);
const trio_pointer_t trio_get_argument(trio_pointer_t ref);
void trio_print_double(trio_pointer_t ref, double number);
int trio_print_ref(trio_pointer_t ref, const char *format, ...);
}

namespace Units {
namespace impl {

static int UnitsPrint(void *ref)
{
    void *argp = trio_get_argument(ref);
    if (0 == argp)
        return 0;
    const char *format = trio_get_format(ref);
    int alternative = trio_get_alternative(ref); trio_set_alternative(ref, 0);
    double value = *(double *)argp;
    if (System::Conversion *conversion = Preferences::standardPreferences()->preferredConversion(format))
    {
        conversion->convert(value);
        trio_print_double(ref, value);
        if (alternative)
            trio_print_ref(ref, "%s", conversion->destinationUnit()->displayName());
    }
    else
        trio_print_double(ref, value);
    return 0;
}
void RegisterCustomSnprintf()
{
    execute_once
        trio_register(UnitsPrint, "Units");
}

}
}
