/*
 * Units/Snprintf.hpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef UNITS_SNPRINTF_HPP_INCLUDED
#define UNITS_SNPRINTF_HPP_INCLUDED

#include <Units/Defines.hpp>
#include <Units/impl/Snprintf.hpp>

namespace Units {

using Units::impl::RegisterCustomSnprintf;

}

#endif // UNITS_SNPRINTF_HPP_INCLUDED
