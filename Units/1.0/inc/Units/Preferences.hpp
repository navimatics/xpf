/*
 * Units/Preferences.hpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef UNITS_PREFERENCES_HPP_INCLUDED
#define UNITS_PREFERENCES_HPP_INCLUDED

#include <Units/Defines.hpp>
#include <Units/System.hpp>
#include <Units/impl/Preferences.hpp>

namespace Units {

using Units::impl::Preferences;

}

#endif // UNITS_PREFERENCES_HPP_INCLUDED
