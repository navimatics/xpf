/*
 * Units/System.hpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef UNITS_SYSTEM_HPP_INCLUDED
#define UNITS_SYSTEM_HPP_INCLUDED

#include <Units/Defines.hpp>
#include <Units/impl/System.hpp>

namespace Units {

using Units::impl::System;

}

#endif // UNITS_SYSTEM_HPP_INCLUDED
