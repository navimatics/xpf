/*
 * Units/impl/Preferences.hpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef UNITS_IMPL_PREFERENCES_HPP_INCLUDED
#define UNITS_IMPL_PREFERENCES_HPP_INCLUDED

#include <Units/impl/Defines.hpp>
#include <Units/impl/System.hpp>

namespace Units {
namespace impl {

class UNITS_APISYM Preferences : public Base::Object
{
public:
    static Preferences *standardPreferences();
    Preferences(System *system, Base::Environment *environment);
    ~Preferences();
    Base::Lock *lock();
    System::Conversion *preferredConversion(const char *srcname);
    void setPreferredConversion(const char *srcname, const char *dstname);
    void removePreferredConversion(const char *srcname);
    bool commit();
private:
    Base::Map *_threadMap();
private:
    System *_system;
    Base::Environment *_environment;
    Object *_threadMapKey;
};

}
}

#endif // UNITS_IMPL_PREFERENCES_HPP_INCLUDED
