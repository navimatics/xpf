/*
 * Units/impl/Snprintf.hpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef UNITS_IMPL_SNPRINTF_HPP_INCLUDED
#define UNITS_IMPL_SNPRINTF_HPP_INCLUDED

#include <Units/impl/Defines.hpp>

namespace Units {
namespace impl {

UNITS_APISYM void RegisterCustomSnprintf();

}
}

#endif // UNITS_IMPL_SNPRINTF_HPP_INCLUDED
