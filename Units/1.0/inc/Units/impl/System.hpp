/*
 * Units/impl/System.hpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef UNITS_IMPL_SYSTEM_HPP_INCLUDED
#define UNITS_IMPL_SYSTEM_HPP_INCLUDED

#include <Units/impl/Defines.hpp>

namespace Units {
namespace impl {

class UNITS_APISYM System : public Base::Object
{
private:
    struct Dimension;
public:
    class Unit : public Base::Object
    {
        friend class System;
    private:
        Unit(Base::CString *nameobj);
        Unit(Base::CString *nameobj, Base::CString *dispobj,
            Base::CArray *dimsobj, double factor,
            double (*forward)(double), double (*inverse)(double));
    public:
        ~Unit();
        bool sameDimensions(Unit *unit);
        const char *name();
        const char *displayName();
        const char *definition();
        const char *dimensions();
        const char *strrepr();
    private:
        const char *_name;
        const char *_displayName;
        Dimension *_dimensions;
        double _factor;
        double (*_forward)(double);
            /* convert base units to this unit */
        double (*_inverse)(double);
            /* convert this unit to base units */
    };
    class Conversion : public Base::Object
    {
        friend class System;
    private:
        Conversion(Unit *src, Unit *dst);
    public:
        ~Conversion();
        Unit *sourceUnit();
        Unit *destinationUnit();
        void convert(double *values, size_t nvalues);
        void convert(float *values, size_t nvalues);
        void convert(double &value);
        void convert(float &value);
        const char *strrepr();
    private:
        Unit *_src;
        Unit *_dst;
        double _factor;
    };
public:
    enum
    {
        ErrorMisuse = 1,
        ErrorConvert = 2,
        ErrorMax = 1000,
    };
    static const char *errorDomain();
    static System *standardSystem();
    static bool convert(Unit *src, Unit *dst, double *values, size_t nvalues);
    static bool convert(Unit *src, Unit *dst, float *values, size_t nvalues);
    static bool convert(Unit *src, Unit *dst, double &value);
    static bool convert(Unit *src, Unit *dst, float &value);
    static Conversion *conversion(Unit *src, Unit *dst);
    System();
    ~System();
    bool addUnit(const char *name, const char *defn = 0,
        double (*forward)(double) = 0, double (*inverse)(double) = 0);
    bool addAlias(const char *name, const char *alias);
    bool addSIPrefixedUnits(const char *name);
    bool addSIPrefixedAliases(const char *name, const char *alias);
    Unit *unit(const char *defn);
private:
    Unit *createUnit(Base::CString *nameobj, const char *defn,
        double (*forward)(double), double (*inverse)(double));
    static int Dimension_cmp(const void *p1, const void *p2);
private:
    Base::Map *_map;
};

inline System::Unit *System::Conversion::sourceUnit()
{
    return _src;
}
inline System::Unit *System::Conversion::destinationUnit()
{
    return _dst;
}
inline void System::Conversion::convert(double &value)
{
    convert(&value, 1);
}
inline void System::Conversion::convert(float &value)
{
    convert(&value, 1);
}

inline bool System::convert(Unit *src, Unit *dst, double &value)
{
    return convert(src, dst, &value, 1);
}
inline bool System::convert(Unit *src, Unit *dst, float &value)
{
    return convert(src, dst, &value, 1);
}

}
}

#endif // UNITS_IMPL_SYSTEM_HPP_INCLUDED
