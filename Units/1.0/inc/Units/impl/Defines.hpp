/*
 * Units/impl/Defines.hpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#ifndef UNITS_IMPL_DEFINES_HPP_INCLUDED
#define UNITS_IMPL_DEFINES_HPP_INCLUDED

#include <Base/Base.hpp>

#if defined(UNITS_CONFIG_INTERNAL)
#define UNITS_APISYM                    BASE_EXPORTSYM
#else
#define UNITS_APISYM                    BASE_IMPORTSYM
#endif

namespace Units {
namespace impl {
}
}

#endif // UNITS_IMPL_DEFINES_HPP_INCLUDED
