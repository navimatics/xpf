/*
 * Units/SystemTest.cpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#include <Units/System.hpp>
#include <gtest/gtest.h>

#define EPS                             1e-9

static double A_from_uv(double x)
{
    return x * 2 + 1;
}
static double uv_from_A(double x)
{
    return (x - 1) / 2;
}
static double B_from_uv(double x)
{
    return x * 3 + 2;
}
static double uv_from_B(double x)
{
    return (x - 2) / 3;
}
TEST(SystemTest, Basic)
{
    mark_and_collect
    {
        Units::System *system = new (Base::collect) Units::System;

        ASSERT_EQ(0, system->unit("u"));
        ASSERT_EQ(0, system->unit("U"));
        ASSERT_TRUE(system->addUnit("u"));
        ASSERT_TRUE(system->addAlias("u", "U"));
        ASSERT_NE((void *)0, system->unit("u"));
        ASSERT_NE((void *)0, system->unit("U"));
        ASSERT_EQ(system->unit("u"), system->unit("U"));
        ASSERT_STREQ("u", system->unit("u")->name());
        ASSERT_STREQ("u", system->unit("U")->name());
        ASSERT_STREQ("u", system->unit("u")->displayName());
        ASSERT_STREQ("u", system->unit("U")->displayName());
        ASSERT_STREQ("u", system->unit("u")->definition());
        ASSERT_STREQ("u", system->unit("U")->definition());
        ASSERT_FALSE(system->addUnit("u"));
        ASSERT_FALSE(system->addAlias("u", "U"));
        ASSERT_FALSE(system->addAlias("nonexistant", "nonexistant"));

        ASSERT_EQ(0, system->unit("v"));
        ASSERT_EQ(0, system->unit("V"));
        ASSERT_TRUE(system->addUnit("v"));
        ASSERT_TRUE(system->addAlias("v", "V"));
        ASSERT_NE((void *)0, system->unit("v"));
        ASSERT_NE((void *)0, system->unit("V"));
        ASSERT_EQ(system->unit("v"), system->unit("V"));
        ASSERT_STREQ("v", system->unit("v")->name());
        ASSERT_STREQ("v", system->unit("V")->name());
        ASSERT_STREQ("v", system->unit("v")->displayName());
        ASSERT_STREQ("v", system->unit("V")->displayName());
        ASSERT_STREQ("v", system->unit("v")->definition());
        ASSERT_STREQ("v", system->unit("V")->definition());
        ASSERT_FALSE(system->addUnit("v"));
        ASSERT_FALSE(system->addAlias("v", "V"));
        ASSERT_FALSE(system->addAlias("nonexistant", "nonexistant"));

        ASSERT_FALSE(system->unit("u")->sameDimensions(system->unit("V")));

        Units::System::Unit *uv = system->unit("u v");
        Units::System::Unit *UV = system->unit("U V");
        ASSERT_NE((void *)0, uv);
        ASSERT_NE((void *)0, UV);
        ASSERT_STREQ("", uv->name());
        ASSERT_STREQ("", UV->name());
        ASSERT_STREQ("u⋅v", uv->displayName());
        ASSERT_STREQ("u⋅v", UV->displayName());
        ASSERT_STREQ("u v", uv->definition());
        ASSERT_STREQ("u v", UV->definition());
        ASSERT_TRUE(uv->sameDimensions(UV));
        ASSERT_FALSE(system->unit("u")->sameDimensions(uv));
        ASSERT_FALSE(system->unit("v")->sameDimensions(uv));

        ASSERT_TRUE(system->addUnit("one", "1"));
        ASSERT_NE((void *)0, system->unit("one"));
        ASSERT_STREQ("one", system->unit("one")->name());
        ASSERT_STREQ("one", system->unit("one")->displayName());
        ASSERT_STREQ("", system->unit("one")->definition());

        ASSERT_TRUE(system->addUnit("a", "100 u v"));
        ASSERT_TRUE(system->addUnit("b", "10 u v -1"));
        ASSERT_TRUE(system->addUnit("c", "a 2 b -2"));
        ASSERT_STREQ("100 v 4", system->unit("c")->definition());
        ASSERT_STREQ("v 4", system->unit("c")->dimensions());

        ASSERT_TRUE(system->unit("v4")->sameDimensions(system->unit("c")));
        ASSERT_TRUE(system->unit("v8v-4")->sameDimensions(system->unit("c")));
        ASSERT_TRUE(system->unit("v8c-2")->sameDimensions(system->unit("one")));
        ASSERT_TRUE(system->unit("v2u-2v-2u2")->sameDimensions(system->unit("one")));

        ASSERT_FALSE(system->addUnit("failUnit1", "u & v"));
        ASSERT_FALSE(system->addUnit("failUnit2", "u nonexistant"));

        system->addUnit("x", "100 u v");
        system->addUnit("y", "10 u v");
        Units::System::Unit *xunit = system->unit("x");
        Units::System::Unit *yunit = system->unit("y");
        {
            double values[] = { 1, 10, 100, +INFINITY };
            double valuesCopy[NELEMS(values)];
            memcpy(valuesCopy, values, sizeof values);
            ASSERT_TRUE(Units::System::convert(xunit, xunit, values, NELEMS(values)));
            for (size_t i = 0; NELEMS(values) > i; i++)
                if (+INFINITY != valuesCopy[i])
                    ASSERT_NEAR(valuesCopy[i], values[i], EPS);
                else
                    ASSERT_EQ(valuesCopy[i], values[i]);
            ASSERT_TRUE(Units::System::convert(xunit, yunit, values, NELEMS(values)));
            ASSERT_TRUE(Units::System::convert(yunit, xunit, values, NELEMS(values)));
            for (size_t i = 0; NELEMS(values) > i; i++)
                if (+INFINITY != valuesCopy[i])
                    ASSERT_NEAR(valuesCopy[i], values[i], EPS);
                else
                    ASSERT_EQ(valuesCopy[i], values[i]);
        }
        {
            float values[] = { 1, 10, 100, +INFINITY };
            float valuesCopy[NELEMS(values)];
            memcpy(valuesCopy, values, sizeof values);
            ASSERT_TRUE(Units::System::convert(xunit, xunit, values, NELEMS(values)));
            for (size_t i = 0; NELEMS(values) > i; i++)
                if (+INFINITY != valuesCopy[i])
                    ASSERT_NEAR(valuesCopy[i], values[i], EPS);
                else
                    ASSERT_EQ(valuesCopy[i], values[i]);
            ASSERT_TRUE(Units::System::convert(xunit, yunit, values, NELEMS(values)));
            ASSERT_TRUE(Units::System::convert(yunit, xunit, values, NELEMS(values)));
            for (size_t i = 0; NELEMS(values) > i; i++)
                if (+INFINITY != valuesCopy[i])
                    ASSERT_NEAR(valuesCopy[i], values[i], EPS);
                else
                    ASSERT_EQ(valuesCopy[i], values[i]);
        }
        /* Units::System::conversion() */
        {
            double values[] = { 1, 10, 100, +INFINITY };
            double valuesCopy[NELEMS(values)];
            memcpy(valuesCopy, values, sizeof values);
            Units::System::conversion(xunit, xunit)->convert(values, NELEMS(values));
            for (size_t i = 0; NELEMS(values) > i; i++)
                if (+INFINITY != valuesCopy[i])
                    ASSERT_NEAR(valuesCopy[i], values[i], EPS);
                else
                    ASSERT_EQ(valuesCopy[i], values[i]);
            Units::System::conversion(xunit, yunit)->convert(values, NELEMS(values));
            Units::System::conversion(yunit, xunit)->convert(values, NELEMS(values));
            for (size_t i = 0; NELEMS(values) > i; i++)
                if (+INFINITY != valuesCopy[i])
                    ASSERT_NEAR(valuesCopy[i], values[i], EPS);
                else
                    ASSERT_EQ(valuesCopy[i], values[i]);
        }
        {
            float values[] = { 1, 10, 100, +INFINITY };
            float valuesCopy[NELEMS(values)];
            memcpy(valuesCopy, values, sizeof values);
            Units::System::conversion(xunit, xunit)->convert(values, NELEMS(values));
            for (size_t i = 0; NELEMS(values) > i; i++)
                if (+INFINITY != valuesCopy[i])
                    ASSERT_NEAR(valuesCopy[i], values[i], EPS);
                else
                    ASSERT_EQ(valuesCopy[i], values[i]);
            Units::System::conversion(xunit, yunit)->convert(values, NELEMS(values));
            Units::System::conversion(yunit, xunit)->convert(values, NELEMS(values));
            for (size_t i = 0; NELEMS(values) > i; i++)
                if (+INFINITY != valuesCopy[i])
                    ASSERT_NEAR(valuesCopy[i], values[i], EPS);
                else
                    ASSERT_EQ(valuesCopy[i], values[i]);
        }

        system->addUnit("A", "u v", A_from_uv, uv_from_A);
        system->addUnit("B", "u v", B_from_uv, uv_from_B);
        Units::System::Unit *Aunit = system->unit("A");
        Units::System::Unit *Bunit = system->unit("B");
        {
            double values[] = { 1, 10, 100, +INFINITY };
            double valuesCopy[NELEMS(values)];
            memcpy(valuesCopy, values, sizeof values);
            ASSERT_TRUE(Units::System::convert(Aunit, Bunit, values, NELEMS(values)));
            ASSERT_TRUE(Units::System::convert(Bunit, Aunit, values, NELEMS(values)));
            for (size_t i = 0; NELEMS(values) > i; i++)
                if (+INFINITY != valuesCopy[i])
                    ASSERT_NEAR(valuesCopy[i], values[i], EPS);
                else
                    ASSERT_EQ(valuesCopy[i], values[i]);
            ASSERT_TRUE(Units::System::convert(Bunit, Aunit, values, NELEMS(values)));
            ASSERT_TRUE(Units::System::convert(Aunit, Bunit, values, NELEMS(values)));
            for (size_t i = 0; NELEMS(values) > i; i++)
                if (+INFINITY != valuesCopy[i])
                    ASSERT_NEAR(valuesCopy[i], values[i], EPS);
                else
                    ASSERT_EQ(valuesCopy[i], values[i]);
        }
        {
            float values[] = { 1, 10, 100, +INFINITY };
            float valuesCopy[NELEMS(values)];
            memcpy(valuesCopy, values, sizeof values);
            ASSERT_TRUE(Units::System::convert(Aunit, Bunit, values, NELEMS(values)));
            ASSERT_TRUE(Units::System::convert(Bunit, Aunit, values, NELEMS(values)));
            for (size_t i = 0; NELEMS(values) > i; i++)
                if (+INFINITY != valuesCopy[i])
                    ASSERT_NEAR(valuesCopy[i], values[i], EPS);
                else
                    ASSERT_EQ(valuesCopy[i], values[i]);
            ASSERT_TRUE(Units::System::convert(Bunit, Aunit, values, NELEMS(values)));
            ASSERT_TRUE(Units::System::convert(Aunit, Bunit, values, NELEMS(values)));
            for (size_t i = 0; NELEMS(values) > i; i++)
                if (+INFINITY != valuesCopy[i])
                    ASSERT_NEAR(valuesCopy[i], values[i], EPS);
                else
                    ASSERT_EQ(valuesCopy[i], values[i]);
        }
        /* Units::System::conversion() */
        {
            double values[] = { 1, 10, 100, +INFINITY };
            double valuesCopy[NELEMS(values)];
            memcpy(valuesCopy, values, sizeof values);
            Units::System::conversion(Aunit, Bunit)->convert(values, NELEMS(values));
            Units::System::conversion(Bunit, Aunit)->convert(values, NELEMS(values));
            for (size_t i = 0; NELEMS(values) > i; i++)
                if (+INFINITY != valuesCopy[i])
                    ASSERT_NEAR(valuesCopy[i], values[i], EPS);
                else
                    ASSERT_EQ(valuesCopy[i], values[i]);
            Units::System::conversion(Bunit, Aunit)->convert(values, NELEMS(values));
            Units::System::conversion(Aunit, Bunit)->convert(values, NELEMS(values));
            for (size_t i = 0; NELEMS(values) > i; i++)
                if (+INFINITY != valuesCopy[i])
                    ASSERT_NEAR(valuesCopy[i], values[i], EPS);
                else
                    ASSERT_EQ(valuesCopy[i], values[i]);
        }
        {
            float values[] = { 1, 10, 100, +INFINITY };
            float valuesCopy[NELEMS(values)];
            memcpy(valuesCopy, values, sizeof values);
            Units::System::conversion(Aunit, Bunit)->convert(values, NELEMS(values));
            Units::System::conversion(Bunit, Aunit)->convert(values, NELEMS(values));
            for (size_t i = 0; NELEMS(values) > i; i++)
                if (+INFINITY != valuesCopy[i])
                    ASSERT_NEAR(valuesCopy[i], values[i], EPS);
                else
                    ASSERT_EQ(valuesCopy[i], values[i]);
            Units::System::conversion(Bunit, Aunit)->convert(values, NELEMS(values));
            Units::System::conversion(Aunit, Bunit)->convert(values, NELEMS(values));
            for (size_t i = 0; NELEMS(values) > i; i++)
                if (+INFINITY != valuesCopy[i])
                    ASSERT_NEAR(valuesCopy[i], values[i], EPS);
                else
                    ASSERT_EQ(valuesCopy[i], values[i]);
        }
    }
}
TEST(SystemTest, StandardSystem)
{
    mark_and_collect
    {
        Base::Error::setLogger(Base::Error::standardLogger);
        Units::System *system = Units::System::standardSystem();
        double value;

        value = 270;
        ASSERT_TRUE(Units::System::convert(system->unit("{°}"), system->unit("rad"), value));
        ASSERT_NEAR(270 * M_PI / 180, value, EPS);
        ASSERT_TRUE(Units::System::convert(system->unit("rad"), system->unit("{°}"), value));
        ASSERT_NEAR(270, value, EPS);
        /* Units::System::conversion() */
        value = 270;
        Units::System::conversion(system->unit("{°}"), system->unit("rad"))->convert(value);
        ASSERT_NEAR(270 * M_PI / 180, value, EPS);
        Units::System::conversion(system->unit("rad"), system->unit("{°}"))->convert(value);
        ASSERT_NEAR(270, value, EPS);

        value = 0;
        ASSERT_TRUE(Units::System::convert(system->unit("{°C}"), system->unit("{°F}"), value));
        ASSERT_NEAR(32, value, EPS);
        ASSERT_TRUE(Units::System::convert(system->unit("{°F}"), system->unit("{°C}"), value));
        ASSERT_NEAR(0, value, EPS);
        /* Units::System::conversion() */
        value = 0;
        Units::System::conversion(system->unit("{°C}"), system->unit("{°F}"))->convert(value);
        ASSERT_NEAR(32, value, EPS);
        Units::System::conversion(system->unit("{°F}"), system->unit("{°C}"))->convert(value);
        ASSERT_NEAR(0, value, EPS);

        value = 1;
        ASSERT_TRUE(Units::System::convert(system->unit("m"), system->unit("ft"), value));
        ASSERT_NEAR(3.280839895, value, EPS);
        ASSERT_TRUE(Units::System::convert(system->unit("ft"), system->unit("m"), value));
        ASSERT_NEAR(1, value, EPS);
        /* Units::System::conversion() */
        value = 1;
        Units::System::conversion(system->unit("m"), system->unit("ft"))->convert(value);
        ASSERT_NEAR(3.280839895, value, EPS);
        Units::System::conversion(system->unit("ft"), system->unit("m"))->convert(value);
        ASSERT_NEAR(1, value, EPS);

        value = 10;
        ASSERT_TRUE(Units::System::convert(system->unit("lb"), system->unit("kg"), value));
        ASSERT_NEAR(4.5359237, value, EPS);
        ASSERT_TRUE(Units::System::convert(system->unit("kg"), system->unit("lb"), value));
        ASSERT_NEAR(10, value, EPS);
        /* Units::System::conversion() */
        value = 10;
        Units::System::conversion(system->unit("lb"), system->unit("kg"))->convert(value);
        ASSERT_NEAR(4.5359237, value, EPS);
        Units::System::conversion(system->unit("kg"), system->unit("lb"))->convert(value);
        ASSERT_NEAR(10, value, EPS);
    }
}
