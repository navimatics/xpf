/*
 * Units/PreferencesTest.cpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#include <Units/Preferences.hpp>
#include <gtest/gtest.h>

TEST(PreferencesTest, Basic)
{
    mark_and_collect
    {
        char path[L_tmpnam]; tmpnam(path);
        Base::Environment *environment = new (Base::collect) Base::Environment(path);
        Units::Preferences *prefs = new (Base::collect) Units::Preferences(
            Units::System::standardSystem(), environment);
        Units::System::Conversion *c1 = prefs->preferredConversion("m");
        Units::System::Conversion *c2 = prefs->preferredConversion("m s -1");
        ASSERT_EQ(0, c1);
        ASSERT_EQ(0, c2);
        prefs->setPreferredConversion("m", "ft");
        prefs->setPreferredConversion("m s -1", "kn");
        c1 = prefs->preferredConversion("m");
        c2 = prefs->preferredConversion("m s -1");
        ASSERT_NE((void *)0, c1);
        ASSERT_NE((void *)0, c2);
        ASSERT_STREQ("m", c1->sourceUnit()->definition());
        ASSERT_STREQ("0.3048 m", c1->destinationUnit()->definition());
        ASSERT_STREQ("m s -1", c2->sourceUnit()->definition());
        ASSERT_STREQ("kn", c2->destinationUnit()->name());
        prefs->removePreferredConversion("m");
        c1 = prefs->preferredConversion("m");
        c2 = prefs->preferredConversion("m s -1");
        ASSERT_EQ(0, c1);
        ASSERT_NE((void *)0, c2);
        ASSERT_STREQ("m s -1", c2->sourceUnit()->definition());
        ASSERT_STREQ("kn", c2->destinationUnit()->name());
        prefs->setPreferredConversion("m s -1", "mi h -1");
        c1 = prefs->preferredConversion("m");
        c2 = prefs->preferredConversion("m s -1");
        ASSERT_EQ(0, c1);
        ASSERT_NE((void *)0, c2);
        ASSERT_STREQ("m s -1", c2->destinationUnit()->dimensions());
        environment->commit();
        remove(path);
    }
    mark_and_collect
    {
        char path[L_tmpnam]; tmpnam(path);
        Base::Environment *environment = new (Base::collect) Base::Environment(path);
        Base::Map *map = new Base::Map;
        environment->setObject(Base::CStringConstant("Units::impl::Preferences"), map,
            Base::Environment::Application);
        map->setObject(Base::CStringConstant("m"), Base::CStringConstant("ft"));
        map->setObject(Base::CStringConstant("m s -1"), Base::CStringConstant("kn"));
        map->release();
        Units::Preferences *prefs = new (Base::collect) Units::Preferences(
            Units::System::standardSystem(), environment);
        Units::System::Conversion *c1 = prefs->preferredConversion("m");
        Units::System::Conversion *c2 = prefs->preferredConversion("m s -1");
        ASSERT_NE((void *)0, c1);
        ASSERT_NE((void *)0, c2);
        ASSERT_STREQ("m", c1->sourceUnit()->definition());
        ASSERT_STREQ("0.3048 m", c1->destinationUnit()->definition());
        ASSERT_STREQ("m s -1", c2->sourceUnit()->definition());
        ASSERT_STREQ("kn", c2->destinationUnit()->name());
        prefs->removePreferredConversion("m");
        c1 = prefs->preferredConversion("m");
        c2 = prefs->preferredConversion("m s -1");
        ASSERT_EQ(0, c1);
        ASSERT_NE((void *)0, c2);
        ASSERT_STREQ("m s -1", c2->sourceUnit()->definition());
        ASSERT_STREQ("kn", c2->destinationUnit()->name());
        prefs->setPreferredConversion("m s -1", "mi h -1");
        c1 = prefs->preferredConversion("m");
        c2 = prefs->preferredConversion("m s -1");
        ASSERT_EQ(0, c1);
        ASSERT_NE((void *)0, c2);
        ASSERT_STREQ("m s -1", c2->destinationUnit()->dimensions());
        environment->commit();
        remove(path);
    }
}
