/*
 * Units/SnprintfTest.cpp
 *
 * Copyright 2010-2013 Navimatics Corporation. All rights reserved.
 */

#include <Units/Snprintf.hpp>
#include <Units/Preferences.hpp>
#include <gtest/gtest.h>

TEST(SnprintfTest, Basic)
{
    mark_and_collect
    {
        Units::RegisterCustomSnprintf();
        Units::Preferences *prefs = Units::Preferences::standardPreferences();
        prefs->setPreferredConversion("m", "ft");
        prefs->setPreferredConversion("m s -1", "kn");

        char buf[128];
        double value;

        value = 10;
        portable_snprintf(buf, sizeof buf, "$<Units:m|%p>", &value);
        ASSERT_STREQ("32.808399", buf);
        portable_snprintf(buf, sizeof buf, "$#.1<Units:m|%p>", &value);
        ASSERT_STREQ("32.8ft", buf);

        portable_snprintf(buf, sizeof buf, "$<Units:m s -1|%p>", &value);
        ASSERT_STREQ("19.438445", buf);
        portable_snprintf(buf, sizeof buf, "$#.1<Units:m s -1|%p>", &value);
        ASSERT_STREQ("19.4kn", buf);

        portable_snprintf(buf, sizeof buf, "$<Units:ft|%p>", &value);
        ASSERT_STREQ("10.000000", buf);
        portable_snprintf(buf, sizeof buf, "$#.1<Units:ft|%p>", &value);
        ASSERT_STREQ("10.0", buf);

        prefs->setPreferredConversion("{°C}", "{°F}");
        portable_snprintf(buf, sizeof buf, "$<Units:{°C}|%p>", &value);
        ASSERT_STREQ("50.000000", buf);
        portable_snprintf(buf, sizeof buf, "$#.1<Units:{°C}|%p>", &value);
        ASSERT_STREQ("50.0°F", buf);

        prefs->removePreferredConversion("{°C}");
        portable_snprintf(buf, sizeof buf, "$<Units:{°C}|%p>", &value);
        ASSERT_STREQ("10.000000", buf);
        portable_snprintf(buf, sizeof buf, "$#.1<Units:{°C}|%p>", &value);
        ASSERT_STREQ("10.0", buf);

        prefs->setPreferredConversion("m s -1", "km h -1");
        portable_snprintf(buf, sizeof buf, "$<Units:m s -1|%p>", &value);
        ASSERT_STREQ("36.000000", buf);
        portable_snprintf(buf, sizeof buf, "$#.1<Units:m s -1|%p>", &value);
        ASSERT_STREQ("36.0km/h", buf);

        prefs->setPreferredConversion("m s -2", "m s -2");
        portable_snprintf(buf, sizeof buf, "$<Units:m s -2|%p>", &value);
        ASSERT_STREQ("10.000000", buf);
        portable_snprintf(buf, sizeof buf, "$#.1<Units:m s -2|%p>", &value);
        ASSERT_STREQ("10.0m/s²", buf);

        prefs->setPreferredConversion("m2 kg s-3 A-1", "m2 kg s-3 A-1");
        portable_snprintf(buf, sizeof buf, "$<Units:m2 kg s-3 A-1|%p>", &value);
        ASSERT_STREQ("10.000000", buf);
        portable_snprintf(buf, sizeof buf, "$#.1<Units:m2 kg s-3 A-1|%p>", &value);
        ASSERT_STREQ("10.0m²⋅kg/s³⋅A", buf);

        prefs->setPreferredConversion("3.048E-01 m", "3.048E-01 m");
        portable_snprintf(buf, sizeof buf, "$<Units:3.048E-01 m|%p>", &value);
        ASSERT_STREQ("10.000000", buf);
        portable_snprintf(buf, sizeof buf, "$#.1<Units:3.048E-01 m|%p>", &value);
        ASSERT_STREQ("10.0[0.3048⋅m]", buf);

        prefs->setPreferredConversion("s -1", "s -1");
        portable_snprintf(buf, sizeof buf, "$<Units:s -1|%p>", &value);
        ASSERT_STREQ("10.000000", buf);
        portable_snprintf(buf, sizeof buf, "$#.1<Units:s -1|%p>", &value);
        ASSERT_STREQ("10.0/s", buf);
    }
}
