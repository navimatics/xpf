#ifndef EXPAT_CONFIG_H
#define EXPAT_CONFIG_H

#if defined(_WIN32) || defined(_WIN64)
#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#undef WIN32_LEAN_AND_MEAN
#endif

#include <memory.h>
#include <string.h>
#define HAVE_MEMMOVE

#if !defined(_MSC_VER) && !defined(__GNUC__)
#error Unknown compiler
#endif

#if defined(_WIN32) || defined(_WIN64)
/* assume Windows always little-endian */
#define BYTEORDER 1234
#elif defined (__i386__) || defined(__x86_64__)
#define BYTEORDER 1234
#elif defined (__ppc__) || defined(__ppc64__)
#define BYTEORDER 4321
#elif defined(__arm__)
#if defined(__ARMEL__)
#define BYTEORDER 1234
#else
#define BYTEORDER 4321
#endif
#endif

#define XML_CONTEXT_BYTES 1024
#define XML_DTD 1
#define XML_NS 1

#endif // EXPAT_CONFIG_H
